<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use MailchimpAPI\Mailchimp;

function currentDate()
{
    return date('Y-m-d H:i:s');
}


function print_rm($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}

function dump($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}

function getMonths($language)
{

    if ($language == 'EN') {

        return array('Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    } elseif ($language == 'AR') {
        return array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يولي', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر');
    }
}


function checkRightAccess($module_id, $role_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Module_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['RoleID'] = $role_id;
    $fetch_by[$can] = 1;

    $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}


function getCollectionProductsSubCategories($collection, $language_cod)
{

    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $result = $CI->Collection_model->getCollectionProductsSubCategories($collection, $language_cod);
    //echo $CI->db->last_query();
    if (!empty($result)) {
        return $result;
    } else {
        return false;
    }

}


function getOfferProductsSubCategories($offer, $language_cod)
{

    $CI = &get_Instance();
    $CI->load->model('Offer_model');
    $result = $CI->Offer_model->getOfferProductsSubCategories($offer, $language_cod);
    //echo $CI->db->last_query();
    if (!empty($result)) {
        return $result;
    } else {
        return false;
    }

}


function NullToEmpty($data)
{
    $returnArr = array();
    if (isset($data[0])) // checking if array is a multi-dimensional one, if so then checking for each row
    {
        $i = 0;
        foreach ($data as $row) {
            if (is_object($row)) {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i]->$key = "";
                    } else {
                        $returnArr[$i]->$key = $value;
                    }
                }
            } else {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i][$key] = "";
                    } else {
                        $returnArr[$i][$key] = $value;
                    }
                }
            }
            $i++;
        }
    } else {
        if (is_object($data)) {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr->$key = "";
                } else {
                    $returnArr->$key = $value;
                }
            }
        } else {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr[$key] = "";
                } else {
                    $returnArr[$key] = $value;
                }
            }
        }
    }
    return $returnArr;
}

function checkUserRightAccess($module_id, $user_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Modules_users_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['UserID'] = $user_id;
    $fetch_by[$can] = 1;

    $result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getSystemLanguages()
{
    $CI = &get_Instance();
    $CI->load->model('System_language_model');
    $languages = $CI->System_language_model->getAllLanguages();
    return $languages;
}

function getDefaultLanguage()
{
    $CI = &get_Instance();
    $CI->load->Model('System_language_model');
    $fetch_by = array();
    $fetch_by['IsDefault'] = 1;
    $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
    return $result;
}

function getLanguageBy($fetch_by)
{
    $CI = &get_Instance();
    $CI->load->Model('System_language_model');
    $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
    return $result;
}

function categoryName($category_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

    $result = $CI->Category_model->getJoinedData(false, 'CategoryID', "categories.CategoryID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "'");
    if ($result) {
        return $result[0]->Title;
    } else {
        return '';
    }
    //return $result;
}


function subCategories($category_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

    $result = $CI->Category_model->getJoinedData(false, 'CategoryID', "categories.ParentID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "' AND categories.IsActive = 1 AND categories.Hide = 0",'ASC','categories_text.Title',true);
    //echo $CI->db->last_query();
    if ($result) {
        return $result;
    } else {
        return '';
    }
    //return $result;
}


function getAllActiveModules($role_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Module_rights_model');
    $result = $CI->Module_rights_model->getModulesWithRights($role_id, $system_language_id, $where);
    return $result;
}


function getActiveUserModule($user_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Modules_users_rights_model');
    $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id, $system_language_id, $where);
    return $result;
}

function countStoreUser($store_id)
{
    $CI = &get_Instance();
    $CI->load->Model('User_model');
    $fetch_by = array();
    $fetch_by['StoreID'] = $store_id;
    $result = $CI->User_model->getMultipleRows($fetch_by);
    if ($result) {
        return count($result);
    } else {
        return '0';
    }

}

function checkAdminSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('admin')) {
        return true;

    } else {
        redirect($CI->config->item('base_url') . 'cms');
    }
}

function checkFrontendSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('user')) {

        if($CI->session->userdata['user']->IsMobileVerified == 0){
            redirect($CI->config->item('base_url').'?verify_mobile=true');
        }
        return true;

    } else {
        redirect($CI->config->item('base_url'));
    }
}


function sendEmail($data = array(),$file_name = false)
{
    $CI = &get_Instance();
    $config["protocol"] = "smtp";
    $config["smtp_host"] = "ssl://smtp.gmail.com";
    $config["smtp_port"] = 465;
    $config["smtp_user"] = "info@chocomood.com";
    $config["smtp_pass"] = "Rb<dW%6Y";
    $config["charset"] = "utf-8";
    $config["mailtype"] = "html";
    $CI->load->library('email');
    $CI->email->from('info@chocomood.com', site_title());
    $CI->email->to($data['to']);
    $CI->email->subject($data['subject']);
    $CI->email->message($data['message']);
    if($file_name){
         $CI->email->attach($file_name);
    }
    $CI->email->set_mailtype('html');
    //print_rm($data);
    $send = $CI->email->send();
    if ( $send) {
       /* echo "true";
         print_rm($CI->email->print_debugger());*/
        return true;
    } else {
        /*echo  $CI->email->print_debugger();

        print_rm($send);*/
        return false;
    }
}

function sendSmsOfficial() // This is official function kept here so we can use it anywhere we want. It has all the functionality unifonic supports
{
    require FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $response = $client->Messages->Send('923368809300', 'chocomood sms testing', 'Chocomood'); // send regular massage
        dump($response);
        //$response = $client->Account->GetBalance();
        //$response = $client->Account->getSenderIDStatus('Arabic');
        //$response = $client->Account->getSenderIDs();
        //$response = $client->Account->GetAppDefaultSenderID();
        //$response = $client->Messages->Send('recipient','messageBody','senderName');
        //$response = $client->Messages->SendBulkMessages('96650*******,9665*******','Hello','UNIFONIC');
        //$response = $client->Messages->GetMessageIDStatus('9459*******');
        //$response = $client->Messages->GetMessagesReport();
        //$response = $client->Messages->GetMessagesDetails();
        //$response = $client->Messages->GetScheduled();
        echo '<pre>';
        print_r($response);
    } catch (Exception $e) {
        echo $e->getCode();
    }
}

function sendSms($mobile_no, $msg, $debug = false) // Provide mobile no with country code, AppsID is configured in vendor/Unifonic/config.php
{
    // return true;
    require_once FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $msg = $msg . "\nFrom: Chocomood.";
        $mobile_no = (strlen($mobile_no) == 9)? "+966".$mobile_no : $mobile_no;
        $response = $client->Messages->Send($mobile_no, $msg, 'Chocomood');
        if ($debug) // If this is true and message sent in try block then it will dump response here
        {
            dump($response);
             //echo "dump";
        }
        if (isset($response->Status) && ($response->Status == 'Queued' || $response->Status == 'Sent' || $response->Status == 'Delivered')) {
            //echo "true";
            return true;
        } else {
            //echo "false";
            //print_rm($response);

            return false;
        }
    } catch (Exception $e) {
        $error = $e->getCode();
        if ($debug) // If this is true and message failed to sent in try block then it will echo error message here
        {
            echo $error;
        }
        return false;
    }
}


function RandomString($digit = 4)
{
    $characters = '123456789123456789123456789123456789123456789';
    $randstring = '';
    for ($i = 0; $i < $digit; $i++) {
        $randstring .= $characters[rand(0, 40)];
    }
    return $randstring;
}

function generatePIN($digits = 4)
{
    $i = 0; //counter
    $pin = ""; //our default pin is blank.
    while ($i < $digits) {
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        $i++;
    }
    return $pin;
}

function log_notification($data)
{
    $CI = &get_Instance();
    $CI->load->model('User_notification_model');
    $result = $CI->User_notification_model->save($data);
    if ($result > 0) {
        return true;
    } else {
        return false;
    }
}

function sendPushNotificationToAndroid($title, $message, $registatoin_ids, $data = array())
{
    $android_fcm_key = 'AIzaSyAGAGqUTZ233iG81Tqj6hWFLz6XbJWNJSg';

    $sendData['title'] = $title;
    $sendData['body'] = $message;
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
        "registration_ids" => $registatoin_ids,
        "content_available" => true,
        "priority" => "high",
        "notification" => array
        (
            "title" => $title,
            "body" => $message,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $registatoin_ids,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );

    $headers = array(
        'Authorization:key=' . $android_fcm_key,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function sendPushNotificationToIOS($title, $message, $register_keys, $data = array())
{
    $ios_fcm_key = 'AIzaSyAGAGqUTZ233iG81Tqj6hWFLz6XbJWNJSg';

    $fields = array(
        //"to" => $gcm_ios_mobile_reg_key,
        "registration_ids" => $register_keys, //1000 per request logic is pending
        "content_available" => true,
        "priority" => "high",
        "notification" => array(
            "body" => strip_tags($message),
            "title" => $title,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $register_keys,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );


    $url = 'https://gcm-http.googleapis.com/gcm/send'; //note: its different than android.


    $headers = array(
        'Authorization: key=' . $ios_fcm_key,
        'Content-Type: application/json'
    );


    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        // echo 'abc';
        die('Curl failed: ' . curl_error($ch));
    }
    // echo 'cdf';
    //print_r($result);exit();
    // Close connection
    curl_close($ch);
    return $result;
}

function sendNotification($title, $message, $data, $user_id)
{
    $CI = &get_Instance();
    $CI->load->model('User_model');
    $res = 'Device token not found!';
    $user = $CI->User_model->get($user_id, true, 'UserID');
    if ($user['DeviceToken'] != '') {
        $token = array($user['DeviceToken']);
        if (strtolower($user['DeviceType']) == 'android') {
            $res = sendPushNotificationToAndroid($title, $message, $token, $data);
        } elseif (strtolower($user['DeviceType']) == 'ios') {
            $res = sendPushNotificationToIOS($title, $message, $token, $data);
        }
    }
    return $res;
}

function pusher($data, $channel, $event, $debug = false)
{
    require FCPATH . '/vendor/autoload.php';

    $options = array(
        'cluster' => 'ap2',
        'useTLS' => true
    );

    //$pusher = new Pusher\Pusher(
    // $app_key,
    //$app_secret,
    // $app_id,
    //array('cluster' => $app_cluster) );

    $pusher = new Pusher\Pusher(
        'a796cb54d7c4b4ae4893',
        'd84121cb5083a62950b6',
        '724300',
        $options
    );

    $response = $pusher->trigger($channel, $event, $data);
    return $response;
}

function convertTimestampToLocalDatetime($timestamp)
{
    // first converting timestamp to GMT date time
    $datetime = gmdate("Y-m-d H:i:s", $timestamp);

    // setting default timezone
    if (isset($_COOKIE['system_timezone'])) {
        $current_timezone = $_COOKIE['system_timezone'];
    } else {
        $current_timezone = 'Asia/Riyadh';
    }

    // creating date time object from the date time coming in UTC format
    $utc_date = DateTime::createFromFormat(
        'Y-m-d H:i:s',
        $datetime,
        new DateTimeZone('UTC')
    );
    $acst_date = clone $utc_date;

    // setting timezone to local timezone for UTC time coming above
    $acst_date->setTimeZone(new DateTimeZone($current_timezone));

    // formatting datetime
    $original_time = $utc_date->format('Y-m-d H:i:s');
    $converted_local_time = $acst_date->format('Y-m-d H:i:s');
    return $converted_local_time;
}

function getFormattedDateTime($timestamp, $format)
{
    return date($format, strtotime(convertTimestampToLocalDatetime($timestamp)));
}

function getPageContent($page_id, $lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Page_model');
    $result = $CI->Page_model->getJoinedData(false, 'PageID', "pages.PageID = " . $page_id . " AND system_languages.ShortCode = '" . $lang . "'")[0];
    return $result;
}

function site_settings()
{
    $CI = &get_Instance();
    $CI->load->model('Site_setting_model');
    return $CI->Site_setting_model->get(1, false, 'SiteSettingID');
}

function site_title()
{
    $CI = &get_Instance();
    $CI->load->model('Site_setting_model');
    return $site_setting = $CI->Site_setting_model->get(1, false, 'SiteSettingID')->SiteName;
    // return $site_setting->SiteName;
}

function my_site_url()
{
    $input = base_url();

// in case scheme relative URI is passed, e.g., //www.google.com/
    $input = trim($input, '/');

// If scheme not included, prepend it
    if (!preg_match('#^http(s)?://#', $input)) {
        $input = 'http://' . $input;
    }

    $urlParts = parse_url($input);

// remove www
    $domain = preg_replace('/^www\./', '', $urlParts['host']);

    return $domain;

// output: google.co.uk
}

function generateBarcode($text, $file_name, $type = 'image')
{
    require FCPATH . '/vendor/autoload.php';
    $time = time();
    $file_path = "uploads/barcode/$file_name.png";
    // $generator = new Picqer\Barcode\BarcodeGeneratorSVG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorJPG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
    // return $generator->getBarcode('081231723897', $generator::TYPE_CODE_128);
    if ($type == 'image') {
        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        $barcode = $generator->getBarcode($text, $generator::TYPE_CODE_128);
        file_put_contents($file_path, $barcode);
        return $file_path;
    } elseif ($type == 'html') {
        $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
        return $generator->getBarcode($text, $generator::TYPE_CODE_128);
    }
}

function generate_pdf($html, $filename = 'invoice',$download=0)
{
    require FCPATH . '/vendor/autoload.php';
    $filename = 'Invoice-' . $filename . "-" . date('YmdHis') . ".pdf";
    // $mpdf = new \Mpdf\Mpdf(['autoArabic' => true,'margin_top' => 10,'default_font' => 'calibri']);
    $mpdf = new \Mpdf\Mpdf(['autoArabic' => true,'default_font' => 'calibri']);
    $mpdf->SetTitle('Order Receipt');
    $mpdf->WriteHTML($html);
    if($download == 1)
    {
        $mpdf->Output($filename.'.pdf', 'D');
    }else
    {
        $mpdf->Output();
    }
}

function booking_html($booking_info)
{
    $html = '<table cellspacing="0" width="100%" style="width:100%">';
    $html .= '<tr><td>OrderNumber </td><td>' . $booking_info['OrderNumber'] . '</td></tr>';
    $html .= '<tr><td>Status </td><td>' . $booking_info['BookingStatusEn'] . '</td></tr>';
    $html .= '<tr><td>Address </td><td>' . $booking_info['Address'] . '</td></tr>';
    $html .= '<tr><td>ServiceCost </td><td>' . $booking_info['ServiceCost'] . '</td></tr>';
    $html .= '<tr><td>CategoryTitle </td><td>' . $booking_info['CategoryTitle'] . '</td></tr>';
    $html .= '</table>';
    return $html;
}

function emailTemplate($msg)
{
    $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chocomood</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;  
}

h4{
    
margin-bottom:20px;

    
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<p style="font-family:sans-serif;font-size:14px;">' . $msg . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Chocomood</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">b-u.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>';

    return $html;
}

function custom_encode($str)
{
    return substr(json_encode($str), 1, -1);
}

function custom_decode($str)
{
    return json_decode(sprintf('"%s"', $str));
}

function front_assets($path = "")
{
    return base_url() . 'assets/frontend/' . $path;
}

function captchaVerify($siteKey)
{
    $secret = '6LeqI5EUAAAAAPUXA4Ficz4i2tYwJ96G9N25hLBL';
    $data = array(
        'secret' => $secret,
        'response' => $siteKey
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $captcha_success = json_decode($response);
    return $captcha_success;
}

function validateMobileNumber($number)
{
    // $url = "http://apilayer.net/api/validate?access_key=d64c34a988c8a6224928b5673d59c22f&number=923368809300&country_code=&format=1";
    $data = array(
        'access_key' => 'd64c34a988c8a6224928b5673d59c22f',
        'number' => $number
    );
    $query_str = http_build_query($data);
    $url = "http://apilayer.net/api/validate?" . $query_str;
    echo $url;
    exit();
    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "http://apilayer.net/api/validate");
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $verify_response = json_decode($response);
    dump($verify_response);
    return $verify_response;
}

function getCities($language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('City_model');
    $cities = $CI->City_model->getAllJoinedData(false, 'CityID', $language, "cities.IsActive = 1", 'ASC', 'SortOrder');
    return $cities;
}


function getBranchDistrict($district_ids,$language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('District_model');
    $districts = $CI->District_model->getAllJoinedData(false, 'DistrictID', $language, "districts.DistrictID IN (".$district_ids.")", 'ASC', 'SortOrder');
    //echo $CI->db->last_query();
    return $districts;
}

function productTitle($product_id)
{
    $CI = &get_Instance();
    $CI->load->model('Product_model');
    $result = $CI->Product_model->getJoinedData(false, 'ProductID', "products.ProductID = " . $product_id . " AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        redirect(base_url('product'));
    }
    $title = str_replace(" ", "-", $result[0]->Title);
    return strtolower($title);
}

function productID($product_title)
{
    $CI = &get_Instance();
    $CI->load->model('Product_model');
    $title = str_replace("-", " ", $product_title);
    $result = $CI->Product_model->getJoinedData(false, 'ProductID', "products_text.Title = '" . $product_title . "' AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        $result = $CI->Product_model->getJoinedData(false, 'ProductID', "products_text.Title = '" . $title . "' AND system_languages.ShortCode = 'EN'");
        if(empty($result)){
            redirect(base_url('product'));
        }
        
    }
    return $result[0]->ProductID;
}

function getUserID()
{
    $CI = &get_Instance();
    return $CI->UserID;
}

function collectionTitle($collection_id)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $result = $CI->Collection_model->getJoinedData(false, 'CollectionID', "collections.CollectionID = " . $collection_id . " AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        redirect(base_url());
    }
    $title = str_replace(" ", "-", $result[0]->Title);
    return strtolower($title);
}

function collectionID($collection_title)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $title = str_replace("-", " ", $collection_title);
    $result = $CI->Collection_model->getJoinedData(false, 'CollectionID', "collections_text.Title = '" . $title . "' AND system_languages.ShortCode = 'EN'");
    if (empty($result)) {
        redirect(base_url());
    }
    return $result[0]->CollectionID;
}

function get_images($id, $type = 'product', $multiple = true)
{
    $CI = &get_Instance();
    $CI->load->model('Site_images_model');
    $images = $CI->Site_images_model->getMultipleRows(array('FileID' => $id, 'ImageType' => strtolower($type)));
    if ($images) {
        if ($multiple) {
            return $images;
        } else {
            return $images[0]->ImageName;
        }
    } else {
        return false;
    }
}

function get_images_id($id, $type = 'product', $multiple = true)
{
    $CI = &get_Instance();
    $CI->load->model('Site_images_model');
    $images = $CI->Site_images_model->getMultipleRows(array('SiteImageID' => $id, 'ImageType' => strtolower($type)));
    if ($images) {
        if ($multiple) {
            return $images;
        } else {
            return $images[0]->ImageName;
        }
    } else {
        return false;
    }
}

function whats_inside_collection($id)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_model');
    $CI->load->model('Product_model');
    $collection = $CI->Collection_model->get($id, false, 'CollectionID');
    if ($collection) {
        $ProductID = $collection->ProductID;
        if ($ProductID != '') {
            $products_ids = explode(',', $ProductID);
            $products = $CI->Product_model->getProductsOfCollection($products_ids);
            return $products;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function getOrderItems($order_id)
{
    $CI = &get_Instance();
    $CI->load->model('Order_item_model');
    $items = $CI->Order_item_model->getOrdersItems($order_id);
    return $items;
}

function newsletter_subscribe($email, $debug = false)
{
    require FCPATH . '/vendor/autoload.php';
    $mailchimp = new Mailchimp('510a3c6f572f02e7a006fd0c680140e4-us18');
    $post_params = [
        'email_address' => $email,
        'status' => 'subscribed'
    ];
    $mailchimp = $mailchimp->lists('b2ef677cb5')->members()->post($post_params);
    // $response->deserialize(); // returns a deserialized (to php object) resource returned by API
    // $response->getHttpCode(); // returns an integer representation of the HTTP response code
    // $response->getHeaders(); // returns response headers as an array of key => value pairs
    // $response->getBody(); // return the raw text body of the response
    $response = json_decode($mailchimp->getBody());
    if ($debug) {
        dump($response);
    }
    //print_rm($response);
    $http_code = $mailchimp->getHttpCode();
    $retArr['http_code'] = $http_code;
    $retArr['title'] = (isset($response->title) ? $response->title: '');
    $retArr['detail'] = (isset($response->detail) ? $response->detail: '');
    return $retArr;
}

function get_mailchimp_subscribers($count = 1000, $offset = 0)
{
    require FCPATH . '/vendor/autoload.php';
    $mailchimp = new Mailchimp('510a3c6f572f02e7a006fd0c680140e4-us18');
    $mailchimp = $mailchimp->lists('b2ef677cb5')->members()->get([
        "count" => $count, 
        "offset" => $offset
    ]);
    $response = json_decode($mailchimp->getBody());
    $response->members['total'] = $response->total_items;
    return $response->members;
}

function getTotalProduct($user_id)
{
    $CI = &get_Instance();
    $CI->load->model('Temp_order_model');
    $result = $CI->Temp_order_model->getTotalProduct($user_id);
    return $result[0]->CartProductsCount > 0 ? $result[0]->CartProductsCount : 0;
}

function isLiked($id, $type)
{
    $CI = &get_Instance();
    $CI->load->model('User_wishlist_model');
    if ($CI->session->userdata('user')) {
        $data['UserID'] = $CI->session->userdata['user']->UserID;
        $data['ItemID'] = $id;
        $data['ItemType'] = $type;
        $wishlist = $CI->User_wishlist_model->getWithMultipleFields($data);
        if ($wishlist) {
            return "p_liked";
        } else {
            return "p_unliked";
        }
    } else {
        return "p_unliked";
    }
}

function getLanguage()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('lang')) {
        $language = $CI->session->userdata('lang');
    } else {
        $result = getDefaultLanguage();
        if ($result) {
            $language = $result->ShortCode;
        } else {
            $language = 'EN';
        }
    }

    return $language;
}

function format_message_for_client($message, $user_id)
{
    $message->WhoSaid = ($message->UserID == $user_id) ? "Customer" : "Support";
    $message->SentReceived = ($message->UserID == $user_id) ? "msgsent" : "msgreceive";
    return $message;
}

function format_message_for_admin($message, $user_id)
{
    $message->WhoSaid = ($message->UserID == $user_id) ? "Support" : "Customer";
    $message->EvenOdd = ($message->UserID == $user_id) ? "even" : "odd";
    return $message;
}

function getTaxShipmentCharges($type = 'Shipment', $OnlyDefault = false) // type == Shipment, Tax
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    $lang = getLanguage();
    $tax_shipment = new Tax_shipment_charges_model();
    if ($OnlyDefault) {
        $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.IsDefault = 1";
        $defaultCharge = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($defaultCharge[0]) ? $defaultCharge[0] : false);
    } else {
        $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.IsActive = 1";
        $charges = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return $charges;
    }
}
function getShipmentMaxAmount() 
{
    $CI = &get_Instance();
    $CI->load->model('Shipping_method_model');
    $lang = getLanguage();
    $tax_shipment = new Shipping_method_model();
    $data = $tax_shipment->getShippingMethod(true);
    return isset($data->MaxAmountForFree)? (($data->MaxAmountForFree == '' || $data->MaxAmountForFree == null)? 0 : $data->MaxAmountForFree) : 0;
}

function getBoxCategory($cat_id) 
{
    $CI = &get_Instance();
    $CI->load->model('Box_category_model');
    $lang = getLanguage();
    $Box_Category = new Box_category_model();
    $data = $Box_Category->getAllJoinedData(false, 'BoxCategoryID', $lang, "box_categories.BoxCategoryID = '".$cat_id."' ");
    return $data;
}

function getTaxShipmentChargesByID($type = 'Shipment',$ID ) // type == Shipment, Tax
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    $lang = getLanguage();
    $tax_shipment = new Tax_shipment_charges_model();
    $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.TaxShipmentChargesID = " . $ID . "";
        
    $defaultCharge = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($defaultCharge[0]) ? $defaultCharge[0] : false);
}

function getUserAddress($AddressIDForPaymentCollection){
    $CI = &get_Instance();
    $CI->load->model('User_address_model');
    $payment_address = $this->User_address_model->getAddresses("user_address.AddressID = " . $AddressIDForPaymentCollection);
    return $payment_address;
}

function orderExtraCharges($order_id){
    $CI = &get_Instance();
    $CI->load->model('Order_extra_charges_model');
    $result = $CI->Order_extra_charges_model->getMultipleRows(array('OrderID' => $order_id));
    return $result;
}

function getSelectedShippingMethodDetail($ShipmentMethodID, $lang)
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    if ($ShipmentMethodID > 0) {
        $where = "tax_shipment_charges.TaxShipmentChargesID = " . $ShipmentMethodID;
        $shipment_method = $CI->Tax_shipment_charges_model->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($shipment_method[0]) ? $shipment_method[0] : false);
    } else {
        $shipment_method = false;
    }
    return $shipment_method;
}

function productAverageRating($ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Product_rating_model');
    $rating = $CI->Product_rating_model->getAverageRating($ProductID);
    return round($rating->average_rating, 1);
}

function collectionAverageRating($CollectionID)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_rating_model');
    $rating = $CI->Collection_rating_model->getAverageRating($CollectionID);
    return round($rating->average_rating, 1);
}

function productRatings($ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Product_rating_model');
    $total_ratings_count = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID));
    if ($total_ratings_count > 0) {
        $rating_count_1 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 1));
        $rating_count_2 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 2));
        $rating_count_3 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 3));
        $rating_count_4 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 4));
        $rating_count_5 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 5));
        $response_arr['rating_1'] = round(($rating_count_1 / $total_ratings_count) * 100);

        $response_arr['rating_2'] = round(($rating_count_2 / $total_ratings_count) * 100);
        $response_arr['rating_3'] = round(($rating_count_3 / $total_ratings_count) * 100);
        $response_arr['rating_4'] = round(($rating_count_4 / $total_ratings_count) * 100);
        $response_arr['rating_5'] = round(($rating_count_5 / $total_ratings_count) * 100);

         $response_arr['rating_1_count'] = $rating_count_1;
         $response_arr['rating_2_count'] = $rating_count_2;
         $response_arr['rating_3_count'] = $rating_count_3;
         $response_arr['rating_4_count'] = $rating_count_4;
         $response_arr['rating_5_count'] = $rating_count_5;



        $response_arr['total_ratings_count'] = $total_ratings_count;
    } else {
        $response_arr['rating_1'] = 0;
        $response_arr['rating_2'] = 0;
        $response_arr['rating_3'] = 0;
        $response_arr['rating_4'] = 0;
        $response_arr['rating_5'] = 0;


        $response_arr['rating_1_count']  = 0;
        $response_arr['rating_2_count'] = 0;
        $response_arr['rating_3_count'] = 0;
        $response_arr['rating_4_count'] = 0;
        $response_arr['rating_5_count'] = 0;
        $response_arr['total_ratings_count'] = 0;
    }
    return $response_arr;
}

function collectionRatings($CollectionID)
{
    $CI = &get_Instance();
    $CI->load->model('Collection_rating_model');
    $total_ratings_count = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID));
    if ($total_ratings_count > 0) {
        $rating_count_1 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 1));
        $rating_count_2 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 2));
        $rating_count_3 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 3));
        $rating_count_4 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 4));
        $rating_count_5 = $CI->Collection_rating_model->getRowsCount(array('CollectionID' => $CollectionID, 'Rating' => 5));
        $response_arr['rating_1'] = ($rating_count_1 / $total_ratings_count) * 100;
        $response_arr['rating_2'] = ($rating_count_2 / $total_ratings_count) * 100;
        $response_arr['rating_3'] = ($rating_count_3 / $total_ratings_count) * 100;
        $response_arr['rating_4'] = ($rating_count_4 / $total_ratings_count) * 100;
        $response_arr['rating_5'] = ($rating_count_5 / $total_ratings_count) * 100;
        $response_arr['total_ratings_count'] = $total_ratings_count;
    } else {
        $response_arr['rating_1'] = 0;
        $response_arr['rating_2'] = 0;
        $response_arr['rating_3'] = 0;
        $response_arr['rating_4'] = 0;
        $response_arr['rating_5'] = 0;
        $response_arr['total_ratings_count'] = 0;
    }
    return $response_arr;
}

function uploadImage($file_key, $path)
{
    $extension = array("jpeg", "jpg", "png","svg");
    $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name']);
    $file_tmp = $_FILES[$file_key]["tmp_name"];
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    if (in_array($ext, $extension)) {
        move_uploaded_file($file_tmp, $path . $file_name);
        return $path . $file_name;
    } else {
        return '';
    }
}

function get_order_invoice($order_id, $type = 'invoice',$download="0")
{
    $CI = &get_Instance();
    $CI->load->model('Order_model');
    $CI->load->model('User_address_model');
    $order_details = $CI->Order_model->getOrders("orders.OrderID = $order_id");
    $data['order'] = $order_details[0];
    $data['language'] = getLanguage();
    $data['type'] = $type;
    $data['barcode']  = generateBarcode($order_id, "barcode", $type = 'image');
    //print_rm($data['order']);
   // $data['payment_address'] = $CI->User_address_model->getAddresses("user_address.AddressID = " . $order_details[0]->AddressIDForPaymentCollection)[0];
    //$order_html = $CI->load->view('frontend/invoice', $data, true);
    if($download == 1)
    {
        $order_html = $CI->load->view('frontend/invoice_pdf', $data, true);
    }else{
        $order_html = $CI->load->view('frontend/emails/order_confirmation', $data, true);
    }
    return $order_html;
}

function get_order_invoice_new($OrderID)
    {
       
        $CI = &get_Instance();
        $CI->load->model('Order_model');
        
        $order_details = $CI->Order_model->getOrders("orders.OrderID = $OrderID");
        $data['order'] = $order_details[0];
        //print_rm($data['order']);
       
        $order_html = $CI->load->view('frontend/invoice', $data, true);
        return $order_html;
        
    }

function email_format($content)
{
    $CI = &get_Instance();
    $data['content'] = $content;
    $html = $CI->load->view('frontend/emails/general_email', $data, true);
    return $html;
}

function get_email_template($template_id, $lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Email_template_model');
    $where = "email_templates.Email_templateID = " . $template_id . " AND system_languages.ShortCode = '" . $lang . "'";
    $template = $CI->Email_template_model->getJoinedData(false, 'Email_templateID', $where);
    return $template[0];
}

function get_unread_orders()
{
    $CI = &get_Instance();
    $CI->load->model('Order_model');
    $where = "";
    $notify_by_role = [4,6];
    $notify_by_driver = [3];
    if(in_array($CI->session->userdata['admin']['RoleID'], $notify_by_role)){
        $where = ['IsRead' => 0, 'StoreID' => $CI->session->userdata['admin']['StoreID']];   
    }else if(in_array($CI->session->userdata['admin']['RoleID'], $notify_by_driver)){
        $where = ['IsRead' => 0, 'DriverID' => $CI->session->userdata['admin']['UserID']];   
    }else{
     $where = ['IsRead' => 0];   
    }
    $result = $CI->Order_model->getMultipleRowsWithSort($where, 'orders.OrderID', 'DESC');
    $retArr['result'] = $result;
    $retArr['result_count'] = $result ? count($result) : 0;
    return $retArr;
}

function getCategories($language)
{
    $CI = &get_Instance();
    $CI->load->model('Category_model');
    return $CI->Category_model->getAllJoinedData(false, 'CategoryID', $language, 'categories.ParentID = 0 AND categories.IsActive = 1');
}

function getAddressDetail($AddressID)
{
    $CI = &get_Instance();
    $CI->load->model('User_address_model');
    $address = $CI->User_address_model->getAddresses("user_address.AddressID = " . $AddressID);
    return $address[0];
}

function getUserOffers($language)
{
    $CI = &get_Instance();
    $CI->load->model('Offer_user_notification_model');
    if ($CI->session->userdata('user')) {
        $UserID = $CI->session->userdata['user']->UserID;
        $user_offers = $CI->Offer_user_notification_model->getUserOffers($UserID, $language);
        return $user_offers;
    } else {
        return false;
    }
}

function IsProductUnderOffer($product_id)
{
    // this function is not final yet
    $CI = &get_Instance();
    $CI->load->model('Offer_user_notification_model');
    $html = '';
    if ($CI->session->userdata('user')) {
        $UserID = $CI->session->userdata['user']->UserID;
        $user_offers = $CI->Offer_user_notification_model->getAllUserOffers($UserID);
        if ($user_offers and count($user_offers) > 0) {
            foreach ($user_offers as $user_offer) {
                $ProductIDs = explode(',', $user_offer->ProductID);
                if (in_array($product_id, $ProductIDs)) {
                    $html = '<h4 class="offered_product" title="' . $user_offer->Description . '">' . $user_offer->Title . '</h4>';
                    // $offer_title = $user_offer->Title;
                    // $offer_description = $user_offer->Description;
                    break;
                }
            }
        }
    }
    return $html;
}

function IsProductPurchased($UserID, $ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Order_item_model');
    $items = $CI->Order_item_model->getOrdersItemsWhere($UserID, $ProductID);
    if (count($items) > 0) {
        return true;
    } else {
        return false;
    }
}

function getNumberFromString($string)
{
    return preg_replace('/[^0-9]/', '', $string);
}

function TermsAcceptedByUser()
{
    $CI = &get_Instance();
    $CI->load->model('User_model');
    if ($CI->session->userdata('user')) {
        $data['UserID'] = $CI->session->userdata['user']->UserID;
        $data['TermsAccepted'] = 1; // 1 means terms accepted
        $termsAccepted = $CI->User_model->getWithMultipleFields($data);
        if ($termsAccepted) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function get_current_url()
{
    $CI =& get_instance();
    $url = $CI->config->site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}

function getURLIds()
{
    $category_ids = array();
    $collection_ids = array();
    if (isset($_GET['q']) && $_GET['q'] != '')
    {
        $url_sub_categories = explode(' ', $_GET['q']);
        if (count($url_sub_categories) > 0) {
            foreach ($url_sub_categories as $url_sub_category) {
                $sub_str = substr($url_sub_category, strrpos($url_sub_category, '-') + 1);
                if (strpos($sub_str, 's') !== false) {
                    $category_ids[] = getNumberFromString($sub_str);
                } elseif (strpos($sub_str, 'c') !== false) {
                    $collection_ids[] = getNumberFromString($sub_str);
                }
            }
        }
    }

    $myArr['category_ids'] = $category_ids;
    $myArr['collection_ids'] = $collection_ids;
    return $myArr;
}

function product_html($products, $ul_cls = 'items_grid')
{

    
    $html = "";
    $return_no_product = TRUE;
    
    $html .= '<div class="row">
                        <div class="col-md-12">
                        <ul class="'.$ul_cls.'" id="productsContainer">';
    $type_of_item = "'Product'";
    $CI = &get_Instance();
    $CI->load->model('Packages_product_model');
    foreach ($products as $key => $p) {
        // echo "<script>console.log('".$p->Price."')</script>";
        $default_package = 0;
        

        $product_packages = $CI->Packages_product_model->productPackages($p->ProductID, 'EN');
        

        $offer_product = checkProductIsInAnyOffer($p->ProductID);
        $ratings = productRatings($p->ProductID);
        $ProductDiscountedPrice = $p->Price;
        $Price = $p->Price;
        if($p->PriceType == 'kg')
        {
            $package = array();
            foreach(@$product_packages as $k => $v) {
                if(@$v['DefaultPackagesID'] == @$v['PackagesID'])
                {
                    $default_package = @$v['PackagesProductID'];
                    array_push($package,$v);
                }
            }
            if(isset($package[0]))
            {
                $gram_price =  @$package[0]['PerGramPrice']; 
                $piece_weight =  @$package[0]['PerPiecePrice']; 
                $weight = @$package[0]['quantity']; 
                $Price = ($gram_price/$piece_weight)*$weight;
            }
            
        }
        $DiscountType = '';
        $DiscountFactor = '';
        $IsOnOffer = false;
        $discountHtml ="";
        if(!empty($offer_product)){
            
            $DiscountType = $offer_product['DiscountType'];
            $DiscountFactor = $offer_product['Discount'];
            if ($DiscountType == 'percentage') {
                $IsOnOffer = true;
                $Discount = ($DiscountFactor / 100) * $Price;
                if ($Discount > $Price) {
                    $ProductDiscountedPrice = 0;
                } else {
                    $ProductDiscountedPrice = $Price - $Discount;
                }
                $discountHtml = '<span class="in-stk-status">Off '.$Discount.'%</span>';

            } elseif ($DiscountType == 'per item') {
                $IsOnOffer = true;
                $Discount = $DiscountFactor;
                if ($Discount > $Price) {
                    $ProductDiscountedPrice = 0;
                } else {
                    $ProductDiscountedPrice = $Price - $DiscountFactor;
                }
                $discountHtml = '<span class="in-stk-status">Off '.$Discount.' SAR</span>';
            } else {
                $Discount = 0;
                if ($Discount > $Price) {
                    $ProductDiscountedPrice = 0;
                } else {
                    $ProductDiscountedPrice = $Price;
                }
            }

        }else
        {
            $ProductDiscountedPrice = $Price;
        }
        
        
        
        $return_no_product = FALSE;    

        if ($p->IsCorporateProduct == 1 && $p->CorporateMinQuantity > 0) {
            $IsCorporateItem = 1;
        } else {
            $IsCorporateItem = 0;
        }

        if ($p->PriceType == 'kg') {
            $PriceType = lang('price_type_kg');
        } else {
            $PriceType = lang('price_type_item');
        }
        $Price = $Price+get_taxt_amount($Price);
        $html .= '<li class="single_product">';
        $html .= '<div class="inbox">';
        
        if ($p->OutOfStock == 1) {
            $html .= '<span class="out-stk-status" >Out of stock</span>';
        }else
        {
            $html .= $discountHtml;
            // $html .= '<span class="in-stk-status" >Out of stock</span>';
        }
        $html .= '<a href="' . base_url() . 'product/detail/' . productTitle($p->ProductID) . '"><div class="imgbox" style="background-image:url('. base_url(get_images($p->ProductID, 'product', false)) .')">';
        
        $html .= '<img src="' . base_url(get_images($p->ProductID, 'product', false)) . '">';
        $html .= '</div></a>';
        $html .= '<a href="' . base_url() . 'product/detail/' . productTitle($p->ProductID) . '">';
        
        if($IsOnOffer)
        {
            $html .= '<h4 class="mb-0 arabic-rtl" style="font-size: 30px;"><strong style="color: #3d3d3d;" data-price="' . number_format($ProductDiscountedPrice, 2). '" data-pid="'.$p->ProductID.'">' . number_format($ProductDiscountedPrice + get_taxt_amount($ProductDiscountedPrice), 2) . '<span style="font-size:18px">'.lang('SAR').'</span></strong> '.($ProductDiscountedPrice != $Price ? '<del style="font-size:14px;margin-bottom: 5px;">'.number_format($Price,2).' '.lang('SAR').'</del>' : '').'</h6><h6 style="font-size:14px;margin-bottom: 5px;">'.$PriceType.'</h6>';
        }
        else
        {
            $html .= '<h4 class="mb-0 arabic-rtl" style="font-size: 30px;"><strong style="color: #3d3d3d;" data-price="' . number_format($ProductDiscountedPrice, 2). '" data-pid="'.$p->ProductID.'">' . number_format($ProductDiscountedPrice + get_taxt_amount($ProductDiscountedPrice), 2) . ' <span style="font-size:10px">'.lang('SAR').'</span></strong></h4><h6 style="font-size:14px;margin-bottom: 5px;">'.$PriceType.'</h6>';
        }
        $html .= '<h4 class="product_title">' . $p->Title . ' </h4>';
        
        $html .= '<div class="description">'.$p->Description.'</div>';
        $html .= '</a>';
        $html .= '<a class="" title="' . lang('click_to_add_to_wishlist') . '" href="javascript:void(0);"
                                   onclick="addToWishlist(' . $p->ProductID . ', ' . $type_of_item . ');"><i
                                            class="listing_heart bi bi-heart ' . isLiked($p->ProductID, 'product') . '" id="item' . $p->ProductID . '" aria-hidden="true"></i></a>';
        $html .= '<a href="javascript:void(0);" title="' . lang('click_to_add_to_cart') . '"
        onclick="addWishlistToCart(' . $p->ProductID . ', ' . $type_of_item . ', ' . $p->Price . ', ' . $IsCorporateItem . ',`'.$DiscountType.'`,`'.$DiscountFactor.'`,`'.@$default_package.'`,`'.@$p->PriceType.'`);">
                                        <i class="bi bi-cart3"></i>
                                </a>';
        $html .= '<p class="edStarts text-left margin-zero"><span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span><span>('.$ratings['total_ratings_count'].')</span></p></div></li>';
    }
    if($return_no_product){
        $html ='<p class="alert alert-danger">'.lang('no_product_available').'</p>';
        return $html;
    }

    $html .= '</ul>
                            </div>
                        </div>';
    return $html;
}

function brand_html($brands, $ul_cls = 'items_grid')
{
    $html = "";
    $return_no_product = TRUE;
    $html .= '<div class="row">
                        <div class="col-md-12">
                        <ul class="'.$ul_cls.'" id="productsContainer">';
    $type_of_item = "'Product'";
    $html .= '<li class="single_product">';
    $html .= '<div class="inbox">';
    $html .= '<a href="' . base_url() . 'product/brand_detail?q=' . strtolower(str_replace(' ','-',lang('chocomood'))).'-s0'. '"><div class="imgbox" style="background-image:url('. front_assets('images/logoin.png') . ')">';
    $html .= '<img src="' . front_assets() . 'images/logoin.png">';
    $html .= '</div></a>';
    $html .= '<a href="' . base_url()  . 'product/brand_detail?q=' . strtolower(str_replace(' ','-',lang('chocomood'))).'-s0'. '">';
    $html .= '<h4>' . lang('chocomood') . ' </h4>';
    $html .= '</a>';
    
    $html .= '</div></li>';
    foreach ($brands as $key => $p) {
        $html .= '<li class="single_product">';
        $html .= '<div class="inbox">';
        $html .= '<a href="' . base_url() . 'product/brand_detail?q=' . strtolower(str_replace(' ','-',$p->brand_name)).'-s'.$p->brand_id. '"><div class="imgbox" style="background-image:url('. base_url($p->Image) .')">';
        $html .= '<img src="' . base_url($p->Image) . '">';
        $html .= '</div></a>';
        $html .= '<a href="' . base_url()  . 'product/brand_detail?q=' . strtolower(str_replace(' ','-',$p->brand_name)).'-s'.$p->brand_id. '">';
        $html .= '<h4>' . $p->brand_name . ' </h4>';
        $html .= '</a>';
        
        $html .= '</div></li>';
    }
    $html .= '</ul>
                            </div>
                        </div>';
    return $html;
}
//      <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
function count_product($id, $type) //type= from brand or category
{
    $where = "";
    if($type=="brand")
    {
        $where = "( FIND_IN_SET(".$id.", products.brand_id) ) AND `products`.`IsActive` = 1";
    }elseif($type=="subcat")
    {
        $where = " ( FIND_IN_SET(".$id.", products.SubCategoryID) ) AND `products`.`IsActive` = 1";
    }
    $query = "SELECT count(*) as count FROM products WHERE ".$where;
    $count = getCustomRow($query)['count'];
    return $count;
}

function checkProductIsInAnyOffer($product_id){
    $CI = &get_Instance();
    $CI->load->model('Offer_model');
    $offer = $CI->Offer_model->getAllJoinedData(true, 'OfferID', 'EN', 'offers.IsActive = 1 AND DATE(ValidTo) > "' . Date('Y-m-d') . '" AND IsForAll = 1 AND ProductID LIKE "%'.$product_id.'%"');
    $return_offer = array();
    if($offer){
        $return_offer = $offer[0]; 
        
    }else{

            if ($CI->session->userdata('user')) {
            
                $offer_for_you = $CI->Offer_model->getOfferForUser($CI->session->userdata['user']->UserID);
                if(!empty($offer_for_you)){
                    $offer_ids = array_column($offer_for_you, 'OfferID');
                    $offer = $CI->Offer_model->getAllJoinedData(true, 'OfferID', 'EN', 'offers.IsActive = 1 AND DATE(ValidTo) > "' . Date('Y-m-d') . '" AND offers.OfferID IN ('.implode(',',$offer_ids).') AND ProductID LIKE "%'.$product_id.'%"');
                    if($offer){
                        $return_offer = $offer[0];
                    }

                }
            }
    }

    return $return_offer;

}

function get_product_packages($product_id,$language_id)
{
    $CI = &get_Instance();
    $CI->load->model('Packages_product_model');
    $data = $CI->Packages_product_model->productPackages($product_id,$language_id, 1);
    if ($data) {
        
            return $data;
        
    } else {
        return false;
    }
}

function get_taxt_amount($total){
    $total_tax = 0;
    $taxes = getTaxShipmentCharges('Tax');
    foreach ($taxes as $tax) {
        $tax_title = $tax->Title;
        $tax_factor = $tax->Type == 'Fixed' ? '' : $tax->Amount . '%';
        if ($tax->Type == 'Fixed') {
            $tax_amount = $tax->Amount;
        } elseif ($tax->Type == 'Percentage') {
           // $tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
             $tax_amount = ($tax->Amount / 100) * ($total);
        }
        $total_tax += $tax_amount;
    }
    // return  number_format($total_tax, 2);
    return $total_tax;

}

function getUserStores($table,$user_id) {
   $data = getCustomRows("Select stores_text.Title from ".$table." s Left join stores_text ON stores_text.StoreID = s.store_id where s.user_id= ".$user_id." AND stores_text.SystemLanguageID = 1");
    return implode(", ",array_column($data,'Title'));
}
function getCustomRow($query) {
    $ci =& get_instance();
    $ci->load->database(); 
    $result = $ci->db->query($query);
    return $result->row_array();
}
function getCustomRows($query) {
    $ci =& get_instance();
    $ci->load->database(); 
    $result = $ci->db->query($query);
    return $result->result_array();
}

function findTotalCartWeight()
{

    $CI = &get_Instance();
    $CI->load->model('Temp_order_model');
    $lang = getLanguage();
    $Temp_order_model = new Temp_order_model();
    $order = $Temp_order_model->getCartItems($CI->session->userdata['user']->UserID, $lang);
    $weight = 0;
    foreach ($order as $k => $v) {
       if($v->PriceType == 'kg'){
            $weight = floatval($weight) + (floatval($v->Quantity) * floatval($v->quantity));
       }else{
            $weight = floatval($weight) + floatval($v->Weight);

       }
    }
    
    return ceil(floatval($weight/1000));
}

function findSemsaShippingCharges()
{

    $data = getCustomRow('Select * from semsa_shipment');
    $weight = findTotalCartWeight();
    $additional = 0;
    if($weight > 15){
        $additional = $weight - 15;
    }
    // echo "<script>console.log('$weight')</script>";
    $price = $data['FifteenKgPrice'];
    $additional_amount = $data['AdditionalKgPrice'] * $additional;
    $total = floatval($price) + floatval($additional_amount);
    return $total;
}

function freeShippingTitle() 
{
    return 'Free Shipping';
}

function limit_string($str, $limit)
{
    if (strlen($str) > $limit)
    {
        $str = substr($str, 0, $limit) . '...';
    }
    return $str;
}

