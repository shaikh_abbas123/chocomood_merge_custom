<?php

Class Shipping_method_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("shipping_methods");

    }

    public function getShippingMethod($active = false, $id=0){
            
        $this->db->select('shipping_methods.*');
        $this->db->from('shipping_methods');

		if($id != 0){
			$this->db->where('shipping_methods.ShippingMethodID', $id);
		}else{
        	$this->db->where('shipping_methods.ShippingMethodID', 3);
		}
        

        if($active == true){
         $this->db->where('shipping_methods.is_active', 1);
        }
        
        return $this->db->get()->row();
        
        
     }
}