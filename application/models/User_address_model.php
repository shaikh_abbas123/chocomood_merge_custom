<?php
Class User_address_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_address");

    }

    public function getAddresses($where = false, $system_language_code = false)
    {
        $this->db->select('user_address.*,users_text.FullName,cities.latitude,cities.longitude,cities_text.Title as CityTitle,districts_text.Title as DistrictTitle');
        $this->db->from('user_address');
        $this->db->join('users', 'user_address.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1', 'LEFT');
        $this->db->join('cities', 'user_address.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'LEFT');
        $this->db->join('districts', 'user_address.DistrictID = districts.DistrictID', 'LEFT');
        $this->db->join('districts_text', 'districts.DistrictID = districts_text.DistrictID', 'LEFT');
        $this->db->join('system_languages slct', 'slct.SystemLanguageID = cities_text.SystemLanguageID', 'LEFT');
        $this->db->join('system_languages sldt', 'sldt.SystemLanguageID = districts_text.SystemLanguageID', 'LEFT');
        if ($where) {
            $this->db->where($where);
        }
        if ($system_language_code) {
            $this->db->where('slct.ShortCode', $system_language_code);
            $this->db->where('sldt.ShortCode', $system_language_code);
        } else {
            $this->db->where('slct.IsDefault', '1');
            $this->db->where('sldt.IsDefault', '1');
        }
        $this->db->order_by('user_address.IsDefault','DESC');
        $this->db->group_by('user_address.AddressID');
        $result = $this->db->get();
        return $result->result();
    }


}