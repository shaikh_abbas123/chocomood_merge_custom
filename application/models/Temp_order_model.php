<?php

Class Temp_order_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("temp_orders");

    }

    public function getTotalProduct($user_id)
    {
        $this->db->select('COUNT(ProductID) as CartProductsCount, SUM(Quantity) as CartQuantityCount');
        $this->db->from('temp_orders');
        $this->db->where('UserID', $user_id);
        return $this->db->get()->result();
    }

    public function getCartItems($user_id, $system_language_code = 'EN')
    {
        if($system_language_code == 'EN'){
            $lang_id = 1;
        }else{
            $lang_id = 2;
        }
        $this->db->select("temp_orders.content_type_sub_image_id,temp_orders.content_text,temp_orders.content_type_id,temp_orders.content_type_sub_id,temp_orders.Wrapping,temp_orders.Fillings,temp_orders.FillColor,temp_orders.PowderColor,temp_orders.BoxPrintable,packages_text.Quantity as quantity, product_packages.PerPiecePrice as package_weight, product_packages.MaximumPackage,product_packages.MinimumPackage,temp_orders.Package,temp_orders.TempOrderID, temp_orders.Weight,temp_orders.Quantity,temp_orders.TempItemPrice,temp_orders.ItemType,temp_orders.ProductID,temp_orders.CustomizedOrderProductIDs,products_text.Title,products.Price,products.PriceType,temp_orders.CustomizedShapeImage,temp_orders.CustomizedBoxID,temp_orders.DiscountType,temp_orders.Discount,products.IsCorporateProduct,products.CorporateMinQuantity,temp_orders.IsCorporateItem,temp_orders.Ribbon, users.*, users_text.*, user_address.*, cities_text.Title as UserCity");
        $this->db->from('temp_orders');
        $this->db->join('products', 'temp_orders.ProductID = products.ProductID', 'LEFT');
        $this->db->join('product_packages', 'temp_orders.Package = product_packages.PackagesProductID', 'LEFT');
        $this->db->join('packages_text', 'packages_text.PackagesID = product_packages.PackagesID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = '.$lang_id .'', 'LEFT');
        $this->db->join('users', 'temp_orders.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'temp_orders.UserID = users_text.UserID', 'LEFT');
        $this->db->join('user_address', 'temp_orders.UserID = user_address.UserID', 'LEFT');
        $this->db->join('cities_text', 'user_address.CityID = cities_text.CityID', 'LEFT');


       // $this->db->join("system_languages slpt", "products_text.SystemLanguageID = slpt.SystemLanguageID AND slpt.ShortCode = '$system_language_code'", "LEFT");
        $this->db->where('temp_orders.UserID', $user_id);
        $this->db->group_by('temp_orders.TempOrderID');
        /*if ($system_language_code) {
            $this->db->where('slpt.ShortCode', $system_language_code);
        } else {
            $this->db->where('slpt.IsDefault', '1');
        }*/
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getAbandonedCart()
    {
        $this->db->select("users.UserID,users.OnlineStatus,users.Mobile,users.Email,users_text.FullName,COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.Quantity) as CartQuantityCount, SUM(temp_orders.Quantity * temp_orders.TempItemPrice) as SubTotal");
        $this->db->from('temp_orders');
        $this->db->join('users', 'temp_orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->group_by('temp_orders.UserID');
        $this->db->order_by('CartQuantityCount', 'DESC');
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getAbandonedCartAnonymous()
    {
        $sql = "SELECT temp_orders.UserID, COUNT(temp_orders.ProductID) as CartProductsCount, SUM(temp_orders.Quantity) as CartQuantityCount FROM temp_orders WHERE UserID NOT IN (SELECT UserID FROM users) HAVING CartProductsCount > 0 AND CartQuantityCount > 0";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getCartByID($TempOrderID)
    {
        $this->db->select("*");
        $this->db->from('temp_orders');
        $this->db->where('temp_orders.TempOrderID', $TempOrderID);
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->row();
    }

}
