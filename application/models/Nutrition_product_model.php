<?php
    Class Nutrition_product_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("product_nutritions");

        }



        public function productNutrition($product_id,$language){
        	$this->db->select('nutritions_text.Title,nutritions.NutritionID,product_nutritions.Quantity');
        	$this->db->from('nutritions');
        	$this->db->join('nutritions_text','nutritions_text.NutritionID = nutritions.NutritionID');
        	$this->db->join('system_languages', 'system_languages.SystemLanguageID = nutritions_text.SystemLanguageID');
        	$this->db->join('product_nutritions','product_nutritions.NutritionID = nutritions.NutritionID');
        	$this->db->where('product_nutritions.ProductID',$product_id);
            $this->db->where('system_languages.ShortCode', $language);

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();

        }


    }