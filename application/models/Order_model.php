<?php

Class Order_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("orders");

    }


    public function getOrders($where = false, $limit = false, $start = 0, $system_language_code = false, $sort = 'DESC')
    {

        $this->db->select('orders.CreatedAt as OrderCreateDate,order_statuses.*,users.Email,users.OnlineStatus,users.PreferredLang as UPreferredLang,users.Mobile,users_text.FullName, cities_text.Title as UserCity, user_address.*,orders.*,act.Title as AddressCity, districts_text.Title as AddressDistrict, IF(tickets.TicketID > 0,1,0) as HasTicket, tickets.TicketID, tickets.TicketNumber, tickets.IsClosed, tickets.CreatedAt as TicketCreatedAt, dut.FullName as AssignedDriverName, du.PreferredLang as DPreferredLang, du.Email as AssignedDriverEmail, du.Mobile as AssignedDriverMobile, tax_shipment_charges_text.Title as ShipmentMethodTitle,stores.VatNo,store_admin.PreferredLang as SPreferredLang,stores_text.Title as StoreTitle,stores_text.Address as StoreAddress,store_city_text.Title as StoreCityTitle,delivery_districts_text.Title as DistrictTitle,store_admin_text.FullName as StoreAdminFullName,store_admin.Email as StoreAdminEmail');
        $this->db->from('orders');
        $this->db->join('order_statuses', 'orders.Status = order_statuses.OrderStatusID', 'LEFT');
        $this->db->join('user_address', 'orders.AddressID = user_address.AddressID', 'LEFT');
        $this->db->join('users', 'orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'left');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'left');
        $this->db->join('order_items', 'order_items.OrderID = orders.OrderID', 'left');
        //store data
        $this->db->join('stores', 'stores.StoreID = orders.StoreID', 'left');
        $this->db->join('stores_text', 'stores_text.StoreID = stores.StoreID AND stores_text.SystemLanguageID = 1', 'left');
        $this->db->join('cities store_city', 'stores.CityID = store_city.CityID', 'left');
        $this->db->join('cities_text store_city_text', 'store_city.CityID = store_city_text.CityID AND store_city_text.SystemLanguageID = 1', 'left');




        $this->db->join('districts delivery_districts', 'delivery_districts.DistrictID = orders.BranchDeliveryDistrictID', 'left');
        $this->db->join('districts_text delivery_districts_text', 'delivery_districts_text.DistrictID = delivery_districts.DistrictID  AND delivery_districts_text.SystemLanguageID = 1', 'left');


        // store admin

        $this->db->join('users store_admin', 'stores.StoreID = store_admin.StoreID AND store_admin.RoleID = 4', 'left');
        $this->db->join('users_text store_admin_text', 'store_admin.UserID = store_admin_text.UserID AND users_text.SystemLanguageID = 1', 'left');



        // address city
        $this->db->join('cities ac', 'user_address.CityID = ac.CityID', 'left');
        $this->db->join('cities_text act', 'ac.CityID = act.CityID', 'left');
        // address district
        $this->db->join('districts', 'user_address.DistrictID = districts.DistrictID', 'left');
        $this->db->join('districts_text', 'districts.DistrictID = districts_text.DistrictID', 'left');

        $this->db->join('tickets', 'orders.OrderID = tickets.OrderID', 'left');

        // driver details
        $this->db->join('users du', 'orders.DriverID = du.UserID', 'LEFT');
        $this->db->join('users_text dut', 'du.UserID = dut.UserID AND dut.SystemLanguageID = 1', 'LEFT');




        // Shipment Method
        $this->db->join('tax_shipment_charges', 'orders.ShipmentMethodID = tax_shipment_charges.TaxShipmentChargesID', 'LEFT');
        $this->db->join('tax_shipment_charges_text', 'tax_shipment_charges.TaxShipmentChargesID = tax_shipment_charges_text.TaxShipmentChargesID', 'LEFT');

       /* $this->db->join('system_languages as slct', 'slct.SystemLanguageID = cities_text.SystemLanguageID', 'Left');
        $this->db->join('system_languages as slact', 'slact.SystemLanguageID = act.SystemLanguageID', 'Left');
        $this->db->join('system_languages as sladt', 'sladt.SystemLanguageID = districts_text.SystemLanguageID', 'Left');
        $this->db->join('system_languages as sltsct', 'sltsct.SystemLanguageID = tax_shipment_charges_text.SystemLanguageID', 'Left');*

        /*if ($system_language_code) {
            $this->db->where('slct.ShortCode', $system_language_code);
            $this->db->where('slact.ShortCode', $system_language_code);
            $this->db->where('sladt.ShortCode', $system_language_code);
            $this->db->where('sltsct.ShortCode', $system_language_code);
        } else {
            $this->db->where('slct.IsDefault', '1');
            $this->db->where('slact.IsDefault', '1');
            $this->db->where('sladt.IsDefault', '1');
            $this->db->where('sltsct.IsDefault', '1');
        }*/

        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('orders.OrderID');
        $this->db->order_by('orders.CreatedAt', $sort);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();

        //echo $this->db->last_query();exit();
        return $result->result();


    }



    public function getOrdersForCronJob($where = false, $limit = false, $start = 0, $system_language_code = false, $sort = 'DESC')
    {

        $this->db->select('orders.*,users.Email,users.Mobile,users_text.FullName, stores.VatNo,stores_text.Title as StoreTitle,stores_text.Address as StoreAddress,store_city_text.Title as StoreCityTitle,store_admin_text.FullName as StoreAdminFullName,store_admin.Email as StoreAdminEmail');
        $this->db->from('orders');
        $this->db->join('users', 'orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
       

        //store data
        $this->db->join('stores', 'stores.StoreID = orders.StoreID', 'left');
        $this->db->join('stores_text', 'stores_text.StoreID = stores.StoreID AND stores_text.SystemLanguageID = 1', 'left');
        $this->db->join('cities store_city', 'stores.CityID = store_city.CityID', 'left');
        $this->db->join('cities_text store_city_text', 'store_city.CityID = store_city_text.CityID AND store_city_text.SystemLanguageID = 1', 'left');

        $this->db->join('users store_admin', 'stores.StoreID = store_admin.StoreID AND store_admin.RoleID = 4', 'left');
        $this->db->join('users_text store_admin_text', 'store_admin.UserID = store_admin_text.UserID AND users_text.SystemLanguageID = 1', 'left');


        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('orders.OrderID');
        $this->db->order_by('orders.CreatedAt', $sort);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();

        //echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getUserLastOrderDetails($UserID, $system_language_code = "EN")
    {
        $this->db->select('orders.*,order_statuses.*,users.Email,users.Mobile,users_text.FullName, cities_text.Title as UserCity, user_address.*,act.Title as AddressCity, districts_text.Title as AddressDistrict, IF(tickets.TicketID > 0,1,0) as HasTicket, tickets.TicketID, tickets.TicketNumber, tickets.CreatedAt as TicketCreatedAt');
        $this->db->from('orders');
        $this->db->join('order_statuses', 'orders.Status = order_statuses.OrderStatusID', 'LEFT');
        $this->db->join('user_address', 'orders.AddressID = user_address.AddressID', 'LEFT');
        $this->db->join('users', 'orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'left');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'left');
        // address city
        $this->db->join('cities ac', 'user_address.CityID = ac.CityID', 'left');
        $this->db->join('cities_text act', 'ac.CityID = act.CityID', 'left');
        // address district
        $this->db->join('districts', 'user_address.DistrictID = districts.DistrictID', 'left');
        $this->db->join('districts_text', 'districts.DistrictID = districts_text.DistrictID', 'left');

        $this->db->join('tickets', 'orders.OrderID = tickets.OrderID', 'left');

        $this->db->join('system_languages as slut', 'slut.SystemLanguageID = users_text.SystemLanguageID', 'Left');
        $this->db->join('system_languages as slct', 'slct.SystemLanguageID = cities_text.SystemLanguageID', 'Left');
        $this->db->join('system_languages as slact', 'slact.SystemLanguageID = act.SystemLanguageID', 'Left');
        $this->db->join('system_languages as sladt', 'sladt.SystemLanguageID = districts_text.SystemLanguageID', 'Left');

        if ($system_language_code) {
            $this->db->where('slut.ShortCode', $system_language_code);
            $this->db->where('slct.ShortCode', $system_language_code);
            $this->db->where('slact.ShortCode', $system_language_code);
            $this->db->where('sladt.ShortCode', $system_language_code);
        } else {
            $this->db->where('slut.IsDefault', '1');
            $this->db->where('slct.IsDefault', '1');
            $this->db->where('slact.IsDefault', '1');
            $this->db->where('sladt.IsDefault', '1');
        }
        $this->db->where('orders.UserID', $UserID);
        $this->db->group_by('orders.OrderID');
        $this->db->order_by('orders.OrderID', 'DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->row();
    }


    public function getTotalsales($year)
    {

        $this->db->select("YEAR(CreatedAt) as SalesYear,MONTH(CreatedAt) as SalesMonth,SUM(TotalAmount) As TotalSales");
        $this->db->from("orders");
        $this->db->where("YEAR(CreatedAt)", $year);
        $this->db->group_by("YEAR(CreatedAt)");
        $this->db->group_by("MONTH(CreatedAt)");
        $this->db->order_by("YEAR(CreatedAt)");
        $this->db->order_by("MONTH(CreatedAt)");

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();


    }


    public function getTotalOrders($year)
    {

        $this->db->select("YEAR(CreatedAt) as OrdersYear,MONTH(CreatedAt) as OrderMonth,COUNT(OrderID) As TotalOrders");
        $this->db->from("orders");
        $this->db->where("YEAR(CreatedAt)", $year);
        // $this->db->where("orders.Status", 4);
        $this->db->group_by("YEAR(CreatedAt)");
        $this->db->group_by("MONTH(CreatedAt)");
        $this->db->order_by("YEAR(CreatedAt)");
        $this->db->order_by("MONTH(CreatedAt)");

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();


    }

    public function getUsersWithOrdersRange($MinValue, $MaxValue, $FromDate, $ToDate)
    {
        $date_where = '';
        if ($FromDate != '' && $ToDate != '')
        {
            $date_where = " AND DATE(orders.CreatedAt) >= '$FromDate' AND DATE(orders.CreatedAt) <= '$ToDate' ";
        }
        $sql = "SELECT users.UserID, users_text.FullName, users.Email, users.Mobile, COUNT(orders.OrderID) AS count_sum FROM users LEFT JOIN users_text ON users.UserID = users_text.UserID LEFT JOIN orders ON users.UserID = orders.UserID WHERE users.RoleID = 5 AND users_text.SystemLanguageID = 1 $date_where HAVING count_sum BETWEEN $MinValue AND $MaxValue  GROUP BY users.UserID ORDER BY count_sum DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    public function getCustomeUsers()
    {
        
        $sql = "SELECT users.UserID, users_text.FullName, users.Email, users.Mobile, COUNT(orders.OrderID) AS count_sum FROM users LEFT JOIN users_text ON users.UserID = users_text.UserID LEFT JOIN orders ON users.UserID = orders.UserID WHERE users.RoleID = 5 AND users_text.SystemLanguageID = 1 GROUP BY users.UserID ORDER BY count_sum DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getUsersWithPurchasesRange($MinValue, $MaxValue, $FromDate, $ToDate)
    {
        $date_where = '';
        if ($FromDate != '' && $ToDate != '')
        {
            $date_where = " AND DATE(orders.CreatedAt) BETWEEN '$FromDate' AND '$ToDate' ";
        }
        $sql = "SELECT users.UserID, users_text.FullName, users.Email, users.Mobile, SUM(orders.TotalAmount) AS count_sum FROM users LEFT JOIN users_text ON users.UserID = users_text.UserID LEFT JOIN orders ON users.UserID = orders.UserID WHERE users.RoleID = 5 AND users_text.SystemLanguageID = 1 $date_where HAVING count_sum BETWEEN $MinValue AND $MaxValue ORDER BY count_sum DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    public function getTotalOrdersOfCustomer($where = false,$InterVal = 'Year')
    {

        $this->db->select("Year(orders.CreatedAt) as Year,MONTH(orders.CreatedAt) as Month,DAY(orders.CreatedAt) as Day,COUNT(OrderID) As TotalOrders,users_text.FullName,SUM(orders.TotalAmount) as TotalSum");
        $this->db->from("orders");
        
        $this->db->join('users', 'orders.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND SystemLanguageID = 1');
        if($where){
            $this->db->where($where);
        }
        
         
         if($InterVal == 'Year'){
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Month'){
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Day'){
            $this->db->group_by('DAY(orders.CreatedAt)');
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }
        
        $this->db->group_by("orders.UserID");


        $this->db->order_by("orders.CreatedAt",'ASC');
       /* $this->db->group_by("DAY(orders.CreatedAt)");
        $this->db->group_by("YEAR(orders.CreatedAt)");
        $this->db->group_by("MONTH(orders.CreatedAt)");
        $this->db->order_by("YEAR(orders.CreatedAt)");
        $this->db->order_by("MONTH(orders.CreatedAt)");*/

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }


    public function getTotalNewAccountOrdersOfCustomers($where = '',$group_by = '')
    {


        $sql ="select Year(orders.CreatedAt) as Year,MONTH(orders.CreatedAt) as Month,DAY(orders.CreatedAt) as Day,count(orders.UserID) as TotalOrders from orders inner join (
                select UserID, min(OrderID) as firstOrderID from orders group by UserID
                
            ) as firstOrders on orders.OrderID=firstOrders.firstOrderID $where $group_by ORDER BY orders.CreatedAt ASC";
        

         $query = $this->db->query($sql);
         $result = array();
        if ($query->num_rows() > 0) {
            $result =  $query->result();
        } 

        return $result;


    }


    public function getOrdersReport($where = false,$InterVal = 'Year')
    {

        $this->db->select("Year(orders.CreatedAt) as Year,MONTH(orders.CreatedAt) as Month,DAY(orders.CreatedAt) as Day,COUNT(orders.OrderID) As TotalOrders,SUM(orders.TotalAmount) as TotalSum,SUM(orders.TotalTaxAmount) as TotalTaxAmount,SUM(orders.TotalShippingCharges) as TotalShippingCharges,SUM(orders.DiscountAvailed) as Discount,COUNT(IF(status = 5, 1, NULL)) 'Cancelled'");
        $this->db->from("orders");
        
        //$this->db->join('order_items', 'order_items.OrderID = orders.OrderID','left');
       // $this->db->join('users_text', 'users.UserID = users_text.UserID AND SystemLanguageID = 1');
        if($where){
            $this->db->where($where);
        }
        
         
         if($InterVal == 'Year'){
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Month'){
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Day'){
            $this->db->group_by('DAY(orders.CreatedAt)');
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }
        
        //$this->db->group_by("orders.UserID");


        $this->db->order_by("orders.CreatedAt",'ASC');
       /* $this->db->group_by("DAY(orders.CreatedAt)");
        $this->db->group_by("YEAR(orders.CreatedAt)");
        $this->db->group_by("MONTH(orders.CreatedAt)");
        $this->db->order_by("YEAR(orders.CreatedAt)");
        $this->db->order_by("MONTH(orders.CreatedAt)");*/

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }


    public function getCouponReport($where = false,$InterVal = 'Year')
    {

        $this->db->select("Year(orders.CreatedAt) as Year,MONTH(orders.CreatedAt) as Month,DAY(orders.CreatedAt) as Day,COUNT(orders.OrderID) As TotalOrders,COUNT(orders.CouponCodeUsed) as Uses,SUM(orders.DiscountAvailed) as Discount,coupons.CouponCode,coupons.DiscountPercentage");
        $this->db->from("coupons");
        
        $this->db->join('orders', 'orders.CouponCodeUsed = coupons.CouponCode');
       
        if($where){
            $this->db->where($where);
        }
        
        $this->db->group_by("coupons.CouponID");
         
         if($InterVal == 'Year'){
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Month'){
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Day'){
            $this->db->group_by('DAY(orders.CreatedAt)');
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }
        
        


        $this->db->order_by("orders.CreatedAt",'ASC');
       /* $this->db->group_by("DAY(orders.CreatedAt)");
        $this->db->group_by("YEAR(orders.CreatedAt)");
        $this->db->group_by("MONTH(orders.CreatedAt)");
        $this->db->order_by("YEAR(orders.CreatedAt)");
        $this->db->order_by("MONTH(orders.CreatedAt)");*/

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getProductOrderCount($where = false,$InterVal = 'Year')
    {

       $this->db->select("Year(orders.CreatedAt) as Year,MONTH(orders.CreatedAt) as Month,DAY(orders.CreatedAt) as Day,products_text.Title,products.SKU,SUM(order_items.Quantity) as Quantity");
        $this->db->from("products");
        $this->db->join("products_text",'products.ProductID = products_text.ProductID AND SystemLanguageID = 1');
        
        
        $this->db->join('order_items', 'products.ProductID = order_items.ProductID');
        $this->db->join('orders', 'order_items.OrderID = orders.OrderID');
       
        if($where){
            $this->db->where($where);
        }
        
        $this->db->group_by("products.ProductID");
         
         if($InterVal == 'Year'){
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Month'){
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }elseif($InterVal == 'Day'){
            $this->db->group_by('DAY(orders.CreatedAt)');
            $this->db->group_by('Month(orders.CreatedAt)');
            $this->db->group_by('Year(orders.CreatedAt)');
         }
        
        


        $this->db->order_by("orders.CreatedAt",'ASC');
       /* $this->db->group_by("DAY(orders.CreatedAt)");
        $this->db->group_by("YEAR(orders.CreatedAt)");
        $this->db->group_by("MONTH(orders.CreatedAt)");
        $this->db->order_by("YEAR(orders.CreatedAt)");
        $this->db->order_by("MONTH(orders.CreatedAt)");*/

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();


    }

    /*
    $this->db->query("SELECT YEAR(CreatedAt) as SalesYear,
             MONTH(CreatedAt) as SalesMonth,
             SUM(TotalAmount) AS TotalSales
        FROM orders
        Where YEAR(CreatedAt) = 2019
    GROUP BY YEAR(CreatedAt), MONTH(CreatedAt)
    ORDER BY YEAR(CreatedAt), MONTH(CreatedAt)");
    */


}