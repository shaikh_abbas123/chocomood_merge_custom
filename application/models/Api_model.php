<?php

class Api_model extends CI_Model
{

    function __construct()
    {


        parent::__construct();

    }

    //Login
    function login($data)
    {

        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("`email` = '" . $data['email'] . "' AND  `password` = '" . $data['password'] . "'");
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    //get
    function get($table_name, $id, $isArr = false)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            if ($isArr) {
                return $query->row_array();
            } else {
                return $query->row();
            }
        } else {
            return false;
        }
    }


    //getAll
    function getAll($table_name)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //getMutiple
    function getMutipleRows($table_name, $field, $val)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($field, $val);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //countRows
    function getRowsCount($table_name, $search)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($search);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /* Insert user */

    function insert($table, $data)
    {
        $this->db->set($data);
        $this->db->insert($table);
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {

            return $insertId;
        } else {
            return false;
        }
    }

    /* update */

    public function update($table_name, $data, $search)
    {
        $this->db->update($table_name, $data, $search);
        if ($this->db->affected_rows() == '1') {
            return true;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    public function updateRow($table_name, $data, $updateBy, $val)
    {
        $this->db->where($updateBy, $val);
        $this->db->update($table_name, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function deleteRow($table_name, $id)
    {
        $this->db->where('id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    public function deleteRowWhere($table_name, $where)
    {
        $this->db->where($where);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;

        } else {
            return false;
        }
    }

    public function deleteRowProject($table_name, $id)
    {
        $this->db->where('project_id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }


    public function deleteRowUser($table_name, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    public function deleteRowFriend($table_name, $id)
    {
        $this->db->where('friend_id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    public function deleteRowSend($table_name, $id)
    {
        $this->db->where('sender_id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    public function deleteRowReceive($table_name, $id)
    {
        $this->db->where('receiver_id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }


    public function deleteRowCommentDetails($id)
    {

        $query = $this->db->query("DELETE project_comments, comment_images, comment_post FROM project_comments LEFT JOIN comment_images ON project_comments.id = comment_images.comment_id LEFT JOIN comment_post ON project_comments.id = comment_post.comment_id
WHERE project_comments.user_id = '" . $id . "' ");

        return true;


    }


    public function deleteRowComment($table_name, $id)
    {
        $this->db->where('comment_id', $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    //get
    function getSingleRow($table_name, $search, $isArr = false)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($search);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            if ($isArr) {
                return $query->row_array();
            } else {
                return $query->row();
            }
        } else {
            return false;
        }
    }

    //get
    function getMultipleRows($table_name, $search, $isArr = false)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($search);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            if ($isArr) {
                return $query->result_array();
            } else {
                return $query->result();
            }
        } else {
            return false;
        }
    }

    //getInterestByUserId
    function getInterestByUserId($user_id)
    {
        $query = $this->db->query("SELECT i.* FROM interests i LEFT OUTER JOIN user_interests ui ON i.id=ui.interest_id WHERE ui.user_id=" . $user_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //getLanguageByProjectId
    function getProjectLanguages($project_id)
    {
        $query = $this->db->query("SELECT l.* FROM languages l LEFT OUTER JOIN project_languages pl ON l.id=pl.language_id WHERE pl.project_id=" . $project_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //getSkillsByProjectId
    function getProjectSkills($project_id)
    {
        $query = $this->db->query("SELECT s.* FROM skills s LEFT OUTER JOIN project_skills ps ON s.id=ps.skill_id WHERE ps.project_id=" . $project_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    //getActivities&CommentsByProjectId
    function getProjectComments($project_id, $user_id)
    {
        $query = $this->db->query("
		SELECT q1.*,
case when q2.counts is null
then 0
else q2.counts 
end as counts , case when q3.counts_posts is null
then 0
else q3.counts_posts 
end as counts_posts
FROM (

SELECT p.user_id AS user_id, p.id AS comment_id, u.full_name AS name,u.image_url, p.comment AS activity, p.created_at AS date_time,p.show_buttons,
CASE WHEN c.status =1
THEN 1 
ELSE 0 
END AS status_like
FROM project_comments p
INNER JOIN users u ON u.id = p.user_id
LEFT JOIN comment_likes c ON c.comment_id = p.id
AND c.user_id =  '" . $user_id . "'
AND c.project_id = '" . $project_id . "'
WHERE p.project_id = '" . $project_id . "'
) AS q1
LEFT JOIN (

SELECT comment_id, COUNT( comment_id ) AS counts
FROM comment_likes
GROUP BY comment_id
) AS q2

ON q1.comment_id = q2.comment_id

LEFT JOIN (

SELECT comment_id, COUNT( comment_id ) AS counts_posts
FROM comment_post
GROUP BY comment_id
) AS q3

ON q1.comment_id = q3.comment_id


 ORDER BY q1.comment_id 
		
		");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    function updateproject($tableName, $arr, $where = '')
    {

        $str = $this->db->update_string($tableName, $arr, $where);


        $rs = $this->db->query($str);
        return $rs;
    }


    function getProjectActivities($project_id)
    {
        $query = $this->db->query("SELECT pa.user_id as user_id, u.full_name as name, a.activity_type as activity, pa.created_at as date_time  FROM project_activities pa LEFT OUTER JOIN activity_type a ON a.id = pa.activity_type_id LEFT OUTER JOIN users u ON u.id = pa.user_id WHERE pa.project_id = " . $project_id . " ORDER BY pa.created_at asc");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    //getProjectUser
    function getProjectUser($project_id, $user_id)
    {
        $query = $this->db->query("SELECT u.* FROM users u LEFT OUTER JOIN project_users pu ON u.id=pu.user_id WHERE pu.project_id=" . $project_id . " AND pu.user_id = " . $user_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //getAllProjectUser
    function getAllProjectUser($project_id)
    {
        $query = $this->db->query("SELECT u.* FROM users u LEFT OUTER JOIN project_users pu ON u.id=pu.user_id WHERE pu.project_id=" . $project_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    function getApprovalUser($project_id)
    {
        $query = $this->db->query("SELECT u.*,pu.project_id FROM users u LEFT OUTER JOIN project_users pu ON u.id=pu.user_id WHERE pu.project_id= '" . $project_id . "' AND pu.status = 0 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }


    function getdisApprovalUser($project_id)
    {
        $query = $this->db->query("SELECT u.*,pu.project_id FROM users u LEFT OUTER JOIN project_users pu ON u.id=pu.user_id WHERE pu.project_id= '" . $project_id . "' AND pu.status = 2 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function ApprovedUser($project_id)
    {
        $query = $this->db->query("SELECT u.*,pu.project_id FROM users u LEFT OUTER JOIN project_users pu ON u.id=pu.user_id WHERE pu.project_id= '" . $project_id . "' AND pu.status = 1 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    /*function Projectinterestcity($cityname)
    {
        $query = $this->db->query("SELECT u. * From users u where u.cityname = '".$cityname."' GROUP BY u.user_name");



        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }*/


    function Projectinterestcity($skill_ids, $cityname)
    {
        $query = $this->db->query("SELECT u. * , us.skill_id
FROM users u
LEFT JOIN user_skills us ON u.id = us.user_id
WHERE us.skill_id IN ('" . $skill_ids . "')
OR u.cityname =  '" . $cityname . "'
GROUP BY u.user_name");


        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }


    function Projectcity($cityname, $user_id)
    {
        $query = $this->db->query("SELECT * FROM users where cityname = '" . $cityname . "' AND id != '" . $user_id . "' ");


        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }


    function updateApprovalStatus($project_id, $user_id)
    {
        $query = $this->db->query("UPDATE project_users SET status = 1 WHERE user_id = '" . $user_id . "' AND project_id = '" . $project_id . "'");

        return true;

    }


    function getupdatedtoken($user_id, $device_id)
    {
        $query = $this->db->query("UPDATE users SET device_UDID = '" . $device_id . "' WHERE id = '" . $user_id . "' ");

        return true;

    }


    function updateDisApprovalStatus($project_id, $user_id)
    {
        $query = $this->db->query("UPDATE project_users SET status = 2 WHERE user_id = '" . $user_id . "' AND project_id = '" . $project_id . "'");

        return true;

    }


    function updateActiveUsersStatus($project_id, $user_id)
    {
        $query = $this->db->query("UPDATE project_users SET status = 0 WHERE user_id = '" . $user_id . "' AND project_id = '" . $project_id . "'");

        return true;

    }


    function updateRejectStatus($project_id, $user_id)
    {
        $query = $this->db->query("DELETE FROM project_users WHERE user_id = '" . $user_id . "' AND project_id = '" . $project_id . "'");

        return true;

    }

    function updateUnjoinRejectStatus($project_id, $user_id)
    {
        $query = $this->db->query("UPDATE project_users SET status = 1 WHERE user_id = '" . $user_id . "' AND project_id = '" . $project_id . "'");

        return true;

    }


    public function updateDraftStatus($project_id)
    {

        $query = $this->db->query("UPDATE projects SET active_status = 4 WHERE  id = '" . $project_id . "'");

        return true;


    }


    function FriendRequest($user_id)
    {
        $query = $this->db->query("SELECT u.*,uf.datetime,uf.friend_id as user_id FROM users u LEFT OUTER JOIN user_friends uf ON u.id=uf.user_id WHERE uf.friend_id= '" . $user_id . "' AND uf.status = 0 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }


    function updateApprovalFriendStatus($user_id, $friend_id)
    {
        $query = $this->db->query("UPDATE user_friends SET status = 1 WHERE user_id = '" . $user_id . "' AND friend_id = '" . $friend_id . "'");

        return true;

    }

    function DisapproveFriends($user_id, $friend_id)
    {
        $query = $this->db->query("DELETE FROM user_friends WHERE user_id = '" . $user_id . "' AND friend_id = '" . $friend_id . "'");

        return true;

    }


    function getPostComment($project_id)
    {
        $query = $this->db->query("SELECT c.id AS commentId, c.comment, c.project_id, c.user_id, u.full_name,u.image_url, c.created_at, COUNT( 
CASE WHEN cl.STATUS =  '1'
THEN 1 
ELSE NULL 
END ) AS like_count
FROM project_comments c
INNER JOIN users u ON u.id = c.user_id
LEFT JOIN comment_likes cl ON cl.comment_id = c.id
WHERE c.project_id = '" . $project_id . "'
GROUP BY c.id, c.comment, c.project_id, c.user_id, u.full_name, c.created_at");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    function deleteComment($comment_id)
    {
        $query = $this->db->query("DELETE FROM project_comments WHERE id = '" . $comment_id . "'");
        $query2 = $this->db->query("DELETE FROM comment_images WHERE comment_id = '" . $comment_id . "'");
        $query3 = $this->db->query("DELETE FROM comment_likes WHERE comment_id = '" . $comment_id . "'");
        $query4 = $this->db->query("DELETE FROM comment_post WHERE comment_id = '" . $comment_id . "'");
        //return $query->result_array();
        /* if ($query->num_rows() > 0) {
              return true;
          } else {
              return false;
          }*/
    }

    function deleteImage($image_id)
    {
        $query = $this->db->query("DELETE FROM comment_images WHERE id = '" . $image_id . "'");
    }


    function updateCommentUnlike($project_id, $user_id, $comment_id)
    {
        $query = $this->db->query("DELETE FROM comment_likes WHERE user_id = '" . $user_id . "' AND project_id = '" . $project_id . "' AND comment_id = '" . $comment_id . "'");

        return true;

    }


    function getvolunteers($project_id)
    {

        $query = $this->db->query("Select * FROM project_users WHERE project_id = '" . $project_id . "' AND Status = 1");
        return $query->num_rows();
    }


    function getLikeCount($project_id)
    {

        $query = $this->db->query("Select * FROM project_likes WHERE project_id = '" . $project_id . "' AND like_status = 1");
        return $query->num_rows();
    }


    function getvolunteercount($project_id)
    {

        $query = $this->db->query("Select no_of_volunteers FROM projects WHERE id = '" . $project_id . "'");
        return $query->row_array();
    }


    function getcommentuser($user_id, $comment_id)
    {

        $query = $this->db->query("SELECT u.id as user_id,u.full_name,u.user_name,u.image_url,c.id as comment_id,c.comment,c.created_at FROM users u LEFT OUTER JOIN project_comments c ON u.id=c.user_id WHERE c.user_id= '" . $user_id . "' AND c.id = '" . $comment_id . "' ");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return $query->row_array();
        }


    }

    function getProjectComments_Posts($comment_id)
    {
        $query = $this->db->query("SELECT * from comment_images where comment_id = '" . $comment_id . "' ");
        return $query->result_array();
    }


    function getcommentposts($comment_id)
    {

        $query = $this->db->query("SELECT u.id as user_id,u.full_name,u.user_name,u.image_url,c.id as postcomment_id,c.post_comment,c.created_at as post_created_at FROM users u LEFT OUTER JOIN comment_post c ON u.id=c.user_id WHERE c.comment_id = '" . $comment_id . "' ORDER BY c.comment_id ASC ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }


    }

    function getcountcommentposts($comment_id)
    {

        $query = $this->db->query("SELECT COUNT(comment_id) AS posts_count FROM comment_post WHERE comment_id = '" . $comment_id . "' ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }


    }


    function getProjectStatus($project_id, $user_id)
    {

        $query = $this->db->query("Select status FROM project_users WHERE project_id = '" . $project_id . "' AND user_id = '" . $user_id . "'");
        return $query->result_array();
    }


    //project list based on filter
    function getProjectList($search)
    {


        $where = "WHERE";
        if ($search['interest_id'] && $search['interest_id'] != '') {
            $where .= " ui.interest_id = " . $search['interest_id'];
        }
        if ($search['city_id'] && $search['city_id'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.city_id = " . $search['city_id'];
        }

        if ($search['skill_id'] && $search['skill_id'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " ps.skill_id = " . $search['skill_id'];
        }
        if ($search['language_id'] && $search['language_id'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " pl.language_id = " . $search['language_id'];
        }
        if ($search['status'] && $search['status'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.active_status = " . $search['status'];
        }
        if ($search['status'] && $search['status'] == '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.active_status = 1 ";
        }

        /*if($search['cityname'] && $search['cityname'] != ''){
              if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.cityname = " . $search['cityname'];
            }

            if($search['countryname'] && $search['countryname'] != ''){
              if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.countryname = " . $search['countryname'];
            }*/

        /*if($search['user_id'] && $search['user_id'] != '')
        {
            if($where != 'WHERE')
            {
                $where .= ' AND';
            }
            $where .= " pu.user_id = ".$search['user_id'];
        }*/


        if ($where == 'WHERE') {
            $where .= " p.is_closed = 0 ";
        } else {
            $where .= " AND p.is_closed = 0 ";
        }

        /*echo "SELECT p.*, COUNT(plk.id) as like_count, u.full_name as user_name, c.eng_name as city_name, co.eng_country_name as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN project_likes plk ON plk.project_id = p.id LEFT OUTER JOIN city c ON p.city_id=c.id LEFT OUTER JOIN users u ON u.id = p.user_id LEFT OUTER JOIN countries co ON co.id = p.country_id ".$where." GROUP BY p.id";
        exit;*/

        //$query = $this->db->query("SELECT p.*, u.full_name as user_name, u.image_url as user_image FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id  LEFT OUTER JOIN users u ON u.id = p.user_id  " . $where . " AND p.active_status = 1 GROUP BY p.id");
        $query = $this->db->query("SELECT p.*, u.full_name as user_name, u.image_url as user_image, c.eng_name as city_name, co.eng_country_name as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN city c ON p.city_id=c.id LEFT OUTER JOIN users u ON u.id = p.user_id LEFT OUTER JOIN countries co ON co.id = p.country_id " . $where . " AND p.active_status = 1 GROUP BY p.id");
        //echo $this->db->last_query();exit();


        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////

    function getProjectList2($search)
    {


        $where = "WHERE";
        if ($search['interest_id'] && $search['interest_id'] != '') {
            $where .= " ui.interest_id = " . $search['interest_id'];
        }


        if ($search['skill_id'] && $search['skill_id'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " ps.skill_id = " . $search['skill_id'];
        }
        if ($search['language_id'] && $search['language_id'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " pl.language_id = " . $search['language_id'];
        }
        if ($search['status'] && $search['status'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.active_status = " . $search['status'];
        } else {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.active_status = '1' ";
        }

        if ($search['cityname'] && $search['cityname'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.cityname = '" . $search['cityname'] . "'";
        }

        if ($search['countryname'] && $search['countryname'] != '') {
            if ($where != 'WHERE') {
                $where .= ' AND';
            }
            $where .= " p.countryname = '" . $search['countryname'] . "'";
        }

        /*if($search['user_id'] && $search['user_id'] != '')
        {
            if($where != 'WHERE')
            {
                $where .= ' AND';
            }
            $where .= " pu.user_id = ".$search['user_id'];
        }*/


        if ($where == 'WHERE') {
            $where .= " p.is_closed = 0 ";
        } else {
            $where .= " AND p.is_closed = 0 ";
        }
//$temp = "SELECT p.*, u.full_name as user_name, u.image_url as user_image, c.eng_name as city_name, co.eng_country_name as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN city c ON p.city_id=c.id LEFT OUTER JOIN users u ON u.id = p.user_id LEFT OUTER JOIN countries co ON co.id = p.country_id " . $where . "  GROUP BY p.id";

//print_r($temp);

//print_r($where);

//exit;

        /*echo "SELECT p.*, COUNT(plk.id) as like_count, u.full_name as user_name, c.eng_name as city_name, co.eng_country_name as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN project_likes plk ON plk.project_id = p.id LEFT OUTER JOIN city c ON p.city_id=c.id LEFT OUTER JOIN users u ON u.id = p.user_id LEFT OUTER JOIN countries co ON co.id = p.country_id ".$where." GROUP BY p.id";
        exit;*/

        //$query = $this->db->query("SELECT p.*, u.full_name as user_name, u.image_url as user_image FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id  LEFT OUTER JOIN users u ON u.id = p.user_id  " . $where . " AND p.active_status = 1 GROUP BY p.id");
        $query = $this->db->query("SELECT p.*, u.full_name as user_name, u.image_url as user_image,p.cityname,p.countryname FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id  LEFT OUTER JOIN users u ON u.id = p.user_id " . $where . " GROUP BY p.id");
        //echo $this->db->last_query();exit();


        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /////////////////////////////////////////////////////////////////


    //project list based on filter
    function getSearchProject($search)
    {


        if (($search['interest_id'] != '') || ($search['query'] != '') || ($search['cityname'] != '') || ($search['skill_id'] != '') || ($search['gender'] != '') || ($search['age'] != '') || ($search['language_id'] != '') || ($search['status'] != '')) {


            $where = "WHERE";

            if ($search['interest_id'] && $search['interest_id'] != '') {
                $where .= " ui.interest_id = " . $search['interest_id'];
            }
            if ($search['cityname'] && $search['cityname'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " p.cityname = '" . $search['cityname'] . "'";
            }
            if ($search['skill_id'] && $search['skill_id'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " ps.skill_id = " . $search['skill_id'];
            }
            if ($search['age'] && $search['age'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " p.age = " . $search['age'];
            }
            if ($search['gender'] && $search['gender'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " p.gender = " . $search['gender'];
            }


            if ($search['language_id'] && $search['language_id'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " pl.language_id = " . $search['language_id'];
            }

            if ($search['status'] && $search['status'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " p.active_status = '" . $search['status'] . "'";
            } else {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " p.active_status = '1' OR p.active_status = '2' ";
            }

            if ($search['query'] && $search['query'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " (p.title LIKE '%" . $search['query'] . "%' OR p.project_description LIKE '%" . $search['query'] . "%')";
            }

            if ($where == 'WHERE') {
                $where .= " p.is_closed = 0 ";
            } else {
                $where .= " AND p.is_closed = 0 ";
            }


        } else {


            $where = "WHERE";

            $where .= " p.active_status = '1' OR p.active_status = '2' ";

        }

        /*echo "SELECT p.*, u.full_name as user_name, c.eng_name as city_name, co.eng_country_name as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN project_likes plk ON plk.project_id = p.id LEFT OUTER JOIN city c ON p.city_id=c.id LEFT OUTER JOIN users u ON u.id = p.user_id LEFT OUTER JOIN countries co ON co.id = p.country_id ".$where." GROUP BY p.id";
        exit;*/

//$temp = "SELECT p.*, u.full_name as user_name, p.cityname as city_name, p.countryname as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN project_likes plk ON plk.project_id = p.id  LEFT OUTER JOIN users u ON u.id = p.user_id " . $where . " GROUP BY p.id";

//print_r($temp);

//exit;


        $query = $this->db->query("SELECT p.*, u.full_name as user_name, p.cityname as city_name, p.countryname as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN project_likes plk ON plk.project_id = p.id  LEFT OUTER JOIN users u ON u.id = p.user_id " . $where . " GROUP BY p.id");


        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
    //SELECT p.*, u.full_name as user_name, c.eng_name as city_name, co.eng_country_name as country_name FROM projects p LEFT OUTER JOIN project_users pu ON p.id = pu.project_id LEFT OUTER JOIN project_languages pl ON p.id = pl.project_id LEFT OUTER JOIN project_skills ps ON p.id = ps.project_id LEFT OUTER JOIN user_interests ui ON pu.user_id = ui.user_id LEFT OUTER JOIN project_likes plk ON plk.project_id = p.id LEFT OUTER JOIN city c ON p.city_id=c.id LEFT OUTER JOIN users u ON u.id = p.user_id LEFT OUTER JOIN countries co ON co.id = p.country_id WHERE ui.interest_id = 2 AND p.city_id = 1 AND ui.interest_id = 2 AND ps.skill_id = 2 AND p.active_status = 1 AND u.age = 28 AND u.gender = 1 GROUP BY p.id

    //user list based on filter
    function getUsersList($search)
    {

        if (($search['interest_id'] != '') || ($search['query'] != '') || ($search['cityname'] != '') || ($search['skill_id'] != '') || ($search['gender'] != '') || ($search['project_id'] != '') || ($search['language_id'] != '')) {

            $where = "WHERE";


            if ($search['interest_id'] && $search['interest_id'] != '') {

                $where .= " ui.interest_id = " . $search['interest_id'];
            }


            if ($search['query'] && $search['query'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }

                $where .= " u.full_name LIKE '%" . $search['query'] . "%'";
            }


            if ($search['cityname'] && $search['cityname'] != '') {

                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " u.cityname = '" . $search['cityname'] . "'";
            }


            if ($search['skill_id'] && $search['skill_id'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " us.skill_id = " . $search['skill_id'];
            }


            if ($search['language_id'] && $search['language_id'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " ul.language_id = " . $search['language_id'];
            }


            if ($search['gender'] && $search['gender'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= "  u.gender = " . $search['gender'];
            }
            if ($search['project_id'] && $search['project_id'] != '') {
                if ($where != 'WHERE') {
                    $where .= ' AND';
                }
                $where .= " (p.id=" . $search['project_id'] . " OR pu.project_id=" . $search['project_id'] . ")";
            }

        } else {

            $where = "";

        }


        //	$temp = "SELECT u.* FROM users u LEFT OUTER JOIN user_interests ui ON u.id = ui.user_id Left OUTER JOIN user_skills us ON u.id = us.user_id LEFT OUTER JOIN project_users pu ON u.id = pu.user_id LEFT OUTER JOIN projects p ON u.id = p.user_id " . $where . " GROUP BY u.id ORDER BY u.full_name ASC";

        //	print_r($temp);

        //	exit;

        $query = $this->db->query("SELECT u.* FROM users u LEFT OUTER JOIN user_interests ui ON u.id = ui.user_id Left OUTER JOIN user_skills us ON u.id = us.user_id LEFT OUTER JOIN project_users pu ON u.id = pu.user_id LEFT OUTER JOIN user_languages ul ON u.id = ul.user_id LEFT OUTER JOIN projects p ON u.id = p.user_id " . $where . " GROUP BY u.id ORDER BY u.full_name ASC");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //getLanguageByUserId
    function getUserLanguages($user_id)
    {
        $query = $this->db->query("SELECT l.* FROM languages l LEFT OUTER JOIN user_languages ul ON l.id=ul.language_id WHERE ul.user_id=" . $user_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //getInterestByUserId
    function getUserInterests($user_id)
    {
        $query = $this->db->query("SELECT i.* FROM interests i LEFT OUTER JOIN user_interests ui ON i.id=ui.interest_id WHERE ui.user_id=" . $user_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //getSkillByUserId
    function getUserSkills($user_id)
    {
        $query = $this->db->query("SELECT s.* FROM skills s LEFT OUTER JOIN user_skills us ON s.id=us.skill_id WHERE us.user_id=" . $user_id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //getDraftedProjectDetail
    function getDraftedProjectDetail()
    {
        $query = $this->db->query("SELECT p.*, c.eng_name as city_name FROM projects p LEFT OUTER JOIN city c ON p.city_id=c.id WHERE p.active_status=0");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    //getProjectDetail
    function getProjectDetail($project_id)
    {
        $query = $this->db->query("SELECT p.*,ct.eng_name as eng_categoryname,ct.arb_name as arb_categoryname,u.id as user_id, u.full_name as user_name, u.image_url as user_image, c.eng_name as city_name, co.eng_country_name as country_name FROM projects p LEFT OUTER JOIN city c ON p.city_id=c.id LEFT OUTER JOIN categories ct ON ct.id= p.category_id LEFT OUTER JOIN users u ON u.id = p.user_id LEFT OUTER JOIN countries co ON co.id = p.country_id WHERE p.id=" . $project_id);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    //getProjectDetail
    function getProjectDetailImage($project_id)
    {
        $query = $this->db->query("SELECT u.image_url AS image_url
FROM users u
LEFT OUTER JOIN project_users pu ON u.id = pu.user_id
WHERE pu.project_id =  '" . $project_id . "' AND pu.status =1");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //getProjectUserImages
    function getProjectUserImages($project_id)
    {
        $query = $this->db->query("SELECT u.image_url FROM users u LEFT OUTER JOIN project_users pu ON u.id = pu.user_id WHERE pu.project_id= '" . $project_id . "' AND pu.status = 1 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //getCurrentUserProfile
    /*function getCurrentUserProfile($user_id)
    {
        $query = "SELECT u.full_name, CONCAT(city.eng_name,', ', countries.eng_country_name) as address, u.age, u.image_url as profile_pic, p.title as project_joined, p.project_description as joined_project_description, proj.title as project_created, proj.project_description as created_project_descriptions FROM users u LEFT OUTER JOIN city ON u.city=city.id LEFT OUTER JOIN countries ON u.country=countries.id LEFT OUTER JOIN project_users pu ON u.id = pu.user_id LEFT OUTER JOIN projects p ON pu.project_id = p.id LEFT OUTER JOIN projects proj ON u.id = proj.user_id WHERE u.id=".$user_id;
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }*/


    public function deleteProject($id)
    {

        $query = $this->db->query("UPDATE projects SET active_status = 5 WHERE  id = '" . $id . "'");

        return true;


    }


    public function updatecode($email, $string)
    {

        $query = $this->db->query("UPDATE users SET verificationcode = '" . $string . "' WHERE  email = '" . $email . "'");

        return true;


    }

    public function updateNotificationStatus($notification)
    {

        $query = $this->db->query("UPDATE user_notifications SET status = 1 WHERE id  = '" . $notification . "' ");

        return true;


    }


    public function updateNotificationInnerStatusApprove($sender_id, $receiver_id, $project_id)
    {

        $query = $this->db->query("UPDATE user_notifications SET status = 1 WHERE sender_id  = '" . $sender_id . "' AND receiver_id  = '" . $receiver_id . "' AND project_id = '" . $project_id . "' AND type = 'Join Request List'");

        return true;


    }


    public function updateNotificationInnerStatusDisApprove($sender_id, $receiver_id, $project_id)
    {

        $query = $this->db->query("UPDATE user_notifications SET status = 1 WHERE sender_id  = '" . $sender_id . "' AND receiver_id  = '" . $receiver_id . "' AND project_id = '" . $project_id . "' AND type = 'Unjoin Request List'");

        return true;


    }


    public function updatequickid($user_id, $quickblox)
    {

        $query = $this->db->query("UPDATE users SET quickblox_id = '" . $quickblox . "' WHERE  id = '" . $user_id . "'");

        return true;


    }


    public function userbyquickblox($quickblox)
    {

        $query = $this->db->query("Select user_name from users WHERE quickblox_id = '" . $quickblox . "'");


        return $query->row_array();


    }


    function getUserInfo($user_id)
    {
        $query = "SELECT u.full_name,u.user_name, u.cityname as city, u.countryname as country,u.quickblox_id, u.age,u.email, u.image_url as profile_pic, u.description, u.cover_url as cover_pic , u.account_type,u.countryname,u.cityname , u.is_verified FROM users u LEFT OUTER JOIN city ON u.city=city.id LEFT OUTER JOIN countries ON u.country=countries.id WHERE u.id=" . $user_id;
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            $record = $query->result_array();
            return $record[0];
        } else {
            return false;
        }
    }

    // getCurrentUserCreatedProjects
    function getUserCreatedProjects($user_id)
    {
        $query = "SELECT p.id, p.title, p.project_description,p.cityname,p.countryname,p.to_date,p.from_date,u.full_name,p.active_status,
					 CASE
					 WHEN p.is_closed = 0 THEN 0
					 WHEN p.is_closed = 1 THEN 1
					 END AS is_closed
 				  from projects p left outer join users u on p.user_id=u.id WHERE p.user_id =" . $user_id;
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            $record = $query->result_array();
            return $record;
        } else {
            return false;
        }
    }

    // getUserJoinedProjects
    function getUserJoinedProjects($user_id)
    {
        $query = "SELECT p.id, p.title, p.project_description, p.cityname, p.countryname, p.to_date, p.from_date, u.full_name, 
CASE 
WHEN p.is_closed =0
THEN 0 
WHEN p.is_closed =1
THEN 1 
END AS is_closed
FROM projects p
LEFT OUTER JOIN project_users pu ON p.id = pu.project_id
AND pu.user_id = '" . $user_id . "'
LEFT OUTER JOIN users u ON u.id = p.user_id
WHERE pu.status =1";

        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            $record = $query->result_array();
            return $record;
        } else {
            return false;
        }
    }

    function getAllMessages($sender_id, $receiver_id)
    {
        $query = "SELECT * FROM user_messages WHERE (sender_id = $sender_id AND receiver_id = $receiver_id) OR (sender_id = $receiver_id AND receiver_id = $sender_id) ORDER BY created_at ASC";
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            $record = $query->result_array();
            return $record;
        } else {
            return array();
        }
    }

    function getNotificationsForUser($user_id)
    {
        $query = "SELECT u.id AS user_id, u.full_name, u.user_name, u.image_url, p.id AS project_id, p.title AS project_title, (

SELECT image_name
FROM project_images
WHERE project_id = un.project_id
LIMIT 1
) AS project_image,un.id AS notification_id, un.notification AS notification_desc,un.notification_ar AS notification_desc_ar,un.status, un.title AS notification_title, un.title_ar AS notification_title_ar, un.type,un.is_read, un.date AS dated
FROM users u
LEFT OUTER JOIN user_notifications un ON u.id = un.sender_id
LEFT OUTER JOIN projects p ON un.project_id = p.id
WHERE un.receiver_id =  '" . $user_id . "'
ORDER BY un.date DESC";
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            $record = $query->result_array();
            return $record;
        } else {
            return array();
        }
    }

    function checkIfFriends($user_id, $friend_id)
    {
        $query = "SELECT * FROM user_friends WHERE (user_id = $user_id AND friend_id = $friend_id) OR (user_id = $friend_id AND friend_id = $user_id) ";
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }


    function deleteFriend($user_id, $friend_id)
    {
        $query = "DELETE FROM user_friends WHERE (user_id = $user_id AND friend_id = $friend_id) OR (user_id = $friend_id AND friend_id = $user_id) ";
        $query = $this->db->query($query);

        return true;

    }


    function checkIfInvited($user_id, $invitee_id, $project_id)
    {

        $query = "SELECT u.* FROM users u LEFT JOIN project_invitees pi ON (u.id = pi.invitee_id) WHERE pi.project_id = '" . $project_id . "' AND pi.user_id = '" . $user_id . "' AND pi.invitee_id = '" . $invitee_id . "' ";
        $query = $this->db->query($query);
        return $query->num_rows();
    }


    function listfrienddetails($user_id)
    {
        $query = "SELECT u1.id, u1.user_name, u1.full_name , u1.image_url
FROM  user_friends uf
INNER JOIN users u1 ON ( u1.id = uf.user_id
OR u1.id = uf.friend_id ) 
AND (
uf.user_id != '" . $user_id . "'
OR uf.friend_id != '" . $user_id . "'
)
WHERE uf.status =1
AND (
uf.friend_id = '" . $user_id . "'
OR uf.user_id = '" . $user_id . "'
)
AND u1.id != '" . $user_id . "'
GROUP BY u1.user_name, u1.full_name, u1.id";
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return $query->result_array();

        }
    }


    function countFriends($user_id)
    {
        $query = "SELECT * FROM user_friends WHERE (user_id = '" . $user_id . "' OR friend_id = '" . $user_id . "') AND status = 1";
        $query = $this->db->query($query);
        if ($query->num_rows() >= 0) {
            return $query->num_rows();
        }
    }

    //


    public function getsentid($user_id)
    {

        $query = $this->db->query("SELECT * FROM user_friends WHERE  user_id = '" . $user_id . "' AND status = 1 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }


    public function getreceiveid($user_id)
    {

        $query = $this->db->query("SELECT * FROM user_friends WHERE  friend_id = '" . $user_id . "' AND status = 1 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }


    public function updatereadstatus($user_id)
    {

        $query = $this->db->query("UPDATE user_notifications SET is_read = '1' WHERE receiver_id = '" . $user_id . "' AND is_read = 0 ");

        return true;

    }


    public function getreportproject()
    {

        $query = $this->db->query("SELECT * FROM report_project GROUP BY project_id");
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;

        }


    }


    public function getcommentcounts()
    {

        $query = $this->db->query("SELECT * FROM report_comment GROUP BY comment_id");
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;

        }


    }


    public function getapprovecount($user_id, $project_id)
    {

        $query = $this->db->query("SELECT * FROM project_users WHERE user_id = '" . $user_id . "' AND project_id = '" . $project_id . "' AND status = 1");
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }

    }


    public function getapprovalprojects()
    {

        $query = $this->db->query("SELECT * FROM projects WHERE active_status = 4 OR active_status = 5 OR active_status = 6 ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;

        }


    }

    public function getcancelledprojects()
    {

        $query = $this->db->query("SELECT * FROM projects WHERE active_status = 3 ");
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;

        }


    }


    public function getallreportprojects()
    {

        $query = $this->db->query("SELECT p. * , COUNT( r.project_id ) AS reportproject
FROM projects p
INNER JOIN report_project r ON p.id = r.project_id
GROUP BY p.id");
        if ($query->num_rows() > 0) {

            if ($as_array) {
                return $query->result_array();
            }
            return $query->result();
        } else {

            return 0;

        }


    }


    public function getallreportcomments()
    {

        $query = $this->db->query("SELECT c. * , p.title, u.full_name, COUNT( r.project_id ) AS reportcomment
                                    FROM project_comments c
                                    INNER JOIN report_comment r ON c.id = r.comment_id
                                    INNER JOIN projects p ON c.project_id = p.id
                                    INNER JOIN users u ON c.user_id = u.id
                                    GROUP BY c.id");
        if ($query->num_rows() > 0) {

            if ($as_array) {
                return $query->result_array();
            }
            return $query->result();
        } else {

            return 0;

        }


    }


    public function getUDID($id)
    {

        $query = $this->db->query("select udid from users where id = '" . $id . "'");
        return $query->row_array();
    }

    public function getAllTimelogsForUser($employee_id)
    {
        $sql = "SELECT *, DATE_FORMAT(checkin_time, '%l:%i:%s %p') as checkin_time, DATE_FORMAT(checkout_time, '%l:%i:%s %p') as checkout_time, DATE_FORMAT(timelog_date, '%a, %D %M %Y') as timelog_date FROM timelogs where employee_id = $employee_id ORDER BY created_at DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getTodaysTimelogDetailForEmployee($employee_id, $timelog_date)
    {
        $sql = "SELECT *,
                    CASE WHEN checkin_time IS NOT NULL THEN checkin_time ELSE '' END AS checkin_time,
                    CASE WHEN checkout_time IS NOT NULL THEN checkout_time ELSE '' END AS checkout_time,
                    CASE WHEN checkin_time IS NOT NULL THEN 1 ELSE 0 END AS checked_in,
                    CASE WHEN checkout_time IS NOT NULL THEN 1 ELSE 0 END AS checked_out,
                    CASE WHEN seating_hours IS NOT NULL THEN seating_hours ELSE '' END AS seating_hours,
                DATE_FORMAT(timelog_date, '%a, %D %M %Y') as timelog_date
                FROM timelogs where employee_id = $employee_id AND timelog_date = '$timelog_date'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getTimelogDetail($timelog_id)
    {
            $sql = "SELECT *,
                    CASE WHEN checkin_time IS NOT NULL THEN checkin_time ELSE '' END AS checkin_time,
                    CASE WHEN checkout_time IS NOT NULL THEN checkout_time ELSE '' END AS checkout_time,
                    CASE WHEN checkin_time IS NOT NULL THEN 1 ELSE 0 END AS checked_in,
                    CASE WHEN checkout_time IS NOT NULL THEN 1 ELSE 0 END AS checked_out,
                    CASE WHEN seating_hours IS NOT NULL THEN seating_hours ELSE '' END AS seating_hours
                    FROM timelogs where id = $timelog_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getFilteredTimelogsForUser($employee_id, $month, $year)
    {
        $sql = "SELECT *,
                CASE WHEN checkin_time IS NOT NULL THEN checkin_time ELSE '' END AS checkin_time,
                    CASE WHEN checkout_time IS NOT NULL THEN checkout_time ELSE '' END AS checkout_time,
                    CASE WHEN checkin_time IS NOT NULL THEN 1 ELSE 0 END AS checked_in,
                    CASE WHEN checkout_time IS NOT NULL THEN 1 ELSE 0 END AS checked_out,
                    CASE WHEN seating_hours IS NOT NULL THEN seating_hours ELSE '' END AS seating_hours,
                DATE_FORMAT(timelog_date, '%a, %D %M %Y') as timelog_date
                FROM timelogs where employee_id = $employee_id AND MONTH(timelog_date) = $month AND YEAR(timelog_date) = $year";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getDeductionsForEmployee($employee_id)
    {
        $sql = "SELECT CONCAT(e.first_name,' ',e.last_name) as employee_name, t.checkin_time, t.checkout_time, t.seating_hours, t.timelog_date, d.deducted_amount as deduction FROM deductions d JOIN timelogs t ON d.employee_id = t.id JOIN employees e ON t.employee_id = e.id AND e.id = $employee_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllProjects($employee_id, $all_projects = 0, $view_as_admin = 0)
    {
        if ($view_as_admin == 1) // for admin, see all projects
        {
            $sql = "SELECT p.* FROM projects p JOIN project_assignees pa ON p.id = pa.project_id GROUP BY p.id ORDER BY p.end_date ASC";
        }else{ // for employee/developer, show only him specific projects of his
            $sql = "SELECT p.* FROM projects p JOIN project_assignees pa ON p.id = pa.project_id AND pa.employee_id = $employee_id GROUP BY p.id ORDER BY p.end_date ASC";
        }
        if ($all_projects == 0) {
            $sql .= " LIMIT 5"; // to only get last two tasks
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getInactiveProjects($employee_id)
    {
        $sql = "SELECT p.* FROM projects p JOIN project_assignees pa ON p.id = pa.project_id AND pa.employee_id = $employee_id AND p.status = 'inactive' GROUP BY p.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getActiveProjects($employee_id)
    {
        $sql = "SELECT p.*  FROM projects p JOIN project_assignees pa ON p.id = pa.project_id AND pa.employee_id = $employee_id AND p.status = 'active' GROUP BY p.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCompletedProjects($employee_id)
    {
        $sql = "SELECT p.*  FROM projects p JOIN project_assignees pa ON p.id = pa.project_id AND pa.employee_id = $employee_id AND p.status = 'completed' GROUP BY p.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getLimitedTasks($employee_id)
    {
        $sql = "SELECT p.title as project_title, t.* FROM tasks t JOIN projects p ON t.project_id = p.id JOIN task_assignees ta ON t.id = ta.task_id AND ta.employee_id = $employee_id GROUP BY t.id ORDER BY t.status DESC LIMIT 2";
        // $sql = "SELECT * FROM ( SELECT p.title as project_title, t.* FROM tasks t JOIN projects p ON t.project_id = p.id JOIN task_assignees ta ON t.id = ta.task_id AND ta.employee_id = $employee_id GROUP BY t.id ORDER BY t.status DESC LIMIT 0, 2) td ORDER BY td.status ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getTasks($employee_id)
    {
        $base_url = base_url();
        $sql = "SELECT p.title as project_title, t.*, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, e.id as responsible_person_id, (SELECT count(*) FROM comments c JOIN tasks ta1 ON c.task_id = ta1.id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta2 ON tc.task_id = ta2.id) as checklist_count FROM tasks t JOIN projects p ON t.project_id = p.id JOIN task_assignees ta ON t.id = ta.task_id JOIN employees e ON ta.employee_id = e.id AND ta.employee_id = $employee_id GROUP BY t.id ORDER BY t.deadline ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getOtherTasks($employee_id)
    {
        $base_url = base_url();
        $sql = "SELECT p.title as project_title, t.*, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, e.id as responsible_person_id, (SELECT count(*) FROM comments c JOIN tasks ta1 ON c.task_id = ta1.id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta2 ON tc.task_id = ta2.id) as checklist_count FROM tasks t JOIN projects p ON t.project_id = p.id JOIN task_assignees ta ON t.id = ta.task_id JOIN employees e ON ta.employee_id = e.id AND ta.employee_id != $employee_id GROUP BY t.id ORDER BY t.deadline ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getMyTasksForProject($employee_id, $project_id)
    {
        $base_url = base_url();
        $sql = "SELECT t.id as task_id, p.id as project_id, p.title as project_title, t.deadline as task_deadline, t.title as task_title, t.status as task_status, t.description as task_description, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, e.id as responsible_person_id, (SELECT count(*) FROM comments c JOIN tasks ta1 ON c.task_id = ta1.id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta2 ON tc.task_id = ta2.id) as checklist_count FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id JOIN projects p ON t.project_id = p.id JOIN employees e ON ta.employee_id = e.id AND ta.employee_id = $employee_id AND t.project_id = $project_id GROUP BY t.id ORDER BY t.deadline ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function getOthersTasksForProject($employee_id, $project_id)
    {
        $base_url = base_url();
        $sql = "SELECT t.id as task_id, p.id as project_id, p.title as project_title, t.deadline as task_deadline, t.title as task_title, t.status as task_status, t.description as task_description, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, e.id as responsible_person_id, (SELECT count(*) FROM comments c JOIN tasks ta1 ON c.task_id = ta1.id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta2 ON tc.task_id = ta2.id) as checklist_count FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id JOIN projects p ON t.project_id = p.id JOIN employees e ON ta.employee_id = e.id AND ta.employee_id != $employee_id AND t.project_id = $project_id GROUP BY t.id ORDER BY t.deadline ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function getOthersTasksForProjectBK($employee_id, $project_id)
    {
        $base_url = base_url();
        $sql = "SELECT t.id as task_id, p.id as project_id, p.title as project_title, t.deadline as task_deadline, t.title as task_title, t.status as task_status, t.description as task_description, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, (SELECT count(*) FROM comments c JOIN tasks ta ON c.task_id = ta.id AND c.employee_id != $employee_id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta1 ON tc.task_id = ta1.id AND tc.employee_id != $employee_id) as checklist_count FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id JOIN projects p ON t.project_id = p.id JOIN employees e ON ta.employee_id = e.id AND ta.employee_id != $employee_id AND t.project_id = $project_id GROUP BY t.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function getAllOpenTasks($employee_id, $project_id)
    {
        $sql = "SELECT t.* FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id AND ta.employee_id = $employee_id AND t.project_id = $project_id AND t.status != 'completed' GROUP BY t.id ORDER BY t.deadline ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getInactiveTasks($employee_id, $project_id)
    {
        $sql = "SELECT t.* FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id AND ta.employee_id = $employee_id AND t.project_id = $project_id AND t.status = 'inactive' GROUP BY t.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getActiveTasks($employee_id, $project_id)
    {
        $sql = "SELECT t.* FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id AND ta.employee_id = $employee_id AND t.project_id = $project_id AND t.status = 'active' GROUP BY t.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCompletedTasks($employee_id, $project_id)
    {
        $sql = "SELECT t.* FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id AND ta.employee_id = $employee_id AND t.project_id = $project_id AND t.status = 'completed' GROUP BY t.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getProjectAssignees($project_id)
    {
        $base_url = base_url();
        $sql = "SELECT e.id as employee_id, CONCAT(e.first_name,' ',e.last_name) as employee_name, e.designation as designation, CONCAT('$base_url',e.image) as employee_image FROM employees e JOIN project_assignees pa ON e.id = pa.employee_id AND pa.project_id = $project_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function getTaskAssignees($task_id)
    {
        $base_url = base_url();
        $sql = "SELECT e.*, CONCAT('$base_url',e.image) as employee_image FROM employees e JOIN task_assignees ta ON e.id = ta.employee_id AND ta.task_id = $task_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function getTaskDetail($task_id, $employee_id)
    {
        $base_url = base_url();
        $sql = "SELECT t.id as task_id, p.id as project_id, p.title as project_title, t.deadline as task_deadline, t.title as task_title, t.status as task_status, t.description as task_description, e.id as responsible_person_id, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, (SELECT count(*) FROM comments c JOIN tasks ta ON c.task_id = ta.id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta1 ON tc.task_id = ta1.id AND tc.employee_id = $employee_id) as checklist_count FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id JOIN projects p ON t.project_id = p.id JOIN employees e ON ta.employee_id = e.id AND ta.task_id = $task_id GROUP BY t.id";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getAllTaskAssignees()
    {
        $base_url = base_url();
        $sql = "SELECT e.*, CONCAT('$base_url',e.image) as employee_image FROM employees e JOIN task_assignees ta ON e.id = ta.employee_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function getAllLeaveRequests($employee_id)
    {
        $sql = "SELECT lr.*, lt.leave_type as leave_type_title FROM leave_requests lr JOIN leave_types lt ON lr.leave_type_id = lt.id AND lr.employee_id = $employee_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getApprovedLeaves($employee_id)
    {
        $sql = "SELECT lr.*, lt.leave_type as leave_type_title FROM leave_requests lr JOIN leave_types lt ON lr.leave_type_id = lt.id AND lr.employee_id = $employee_id AND lr.status = 'approved'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getApprovedLeavesCount($employee_id)
    {
        $sql = "SELECT SUM(lr.days_count) as leaves_count FROM leave_requests lr WHERE lr.employee_id = $employee_id AND lr.status = 'approved'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            if ($result['leaves_count'] > 0) {
                $leaves_count = $result['leaves_count'];
            } else {
                $leaves_count = 0;
            }
            return $leaves_count;
        } else {
            return 0;
        }
    }

    public function getTaskChecklist($task_id, $employee_id)
    {
        $sql = "SELECT id, description, status FROM task_checklist WHERE task_id = $task_id AND employee_id = $employee_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAllCommentsForTask($task_id)
    {
        $base_url = base_url();
        $sql = "SELECT e.id as employee_id, CONCAT( e.first_name,  ' ', e.last_name ) AS employee_name, CONCAT('$base_url',e.image) as employee_image, e.designation as designation, c.id as comment_id, c.comment as comment, c.has_attachment as has_attachment, c.created_at as created_at, ca.type AS file_type, CONCAT('$base_url',ca.file) AS file FROM comments c LEFT JOIN employees e ON c.employee_id = e.id LEFT JOIN comment_attachments ca ON c.id = ca.comment_id WHERE c.task_id = $task_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAllTimelogDetailsForToday($date)
    {
        $base_url = base_url();
        $sql = "SELECT e.id as employee_id, CONCAT( e.first_name,  ' ', e.last_name ) AS employee_name, e.designation as designation, CONCAT('$base_url',e.image) as image,
                        CONCAT('$base_url',e.compressed_image) as compressed_image,
                        CASE WHEN t.checkin_time IS NOT NULL THEN t.checkin_time ELSE '' END AS checkin_time,
                        CASE WHEN t.checkout_time IS NOT NULL THEN t.checkout_time ELSE '' END AS checkout_time,
                        CASE WHEN t.seating_hours IS NOT NULL THEN t.seating_hours ELSE '' END AS seating_hours,
                        CASE WHEN t.checkin_time IS NOT NULL THEN 1 ELSE 0 END AS checked_in,
                        CASE WHEN t.checkout_time IS NOT NULL THEN 1 ELSE 0 END AS checked_out,
                        CASE WHEN t.check_in_lat IS NOT NULL THEN t.check_in_lat ELSE 'N/A' END AS check_in_lat,
                        CASE WHEN t.check_in_lng IS NOT NULL THEN t.check_in_lng ELSE 'N/A' END AS check_in_lng,
                        CASE WHEN t.check_in_location IS NOT NULL THEN t.check_in_location ELSE 'N/A' END AS check_in_location,
                        CASE WHEN t.check_out_lat IS NOT NULL THEN t.check_out_lat ELSE 'N/A' END AS check_out_lat,
                        CASE WHEN t.check_out_lng IS NOT NULL THEN t.check_out_lng ELSE 'N/A' END AS check_out_lng,
                        CASE WHEN t.check_out_location IS NOT NULL THEN t.check_out_location ELSE 'N/A' END AS check_out_location
                        FROM employees e
                        LEFT JOIN timelogs t ON e.id = t.employee_id
                        AND t.timelog_date =  '$date' ORDER BY employee_name";
        //echo $sql;exit();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getMonthlyTimelogsForEmployee($employee_id, $date)
    {
        $sql = "SELECT *,
                CASE WHEN checkin_time IS NOT NULL THEN checkin_time ELSE '' END AS checkin_time,
                    CASE WHEN checkout_time IS NOT NULL THEN checkout_time ELSE '' END AS checkout_time,
                    CASE WHEN checkin_time IS NOT NULL THEN 1 ELSE 0 END AS checked_in,
                    CASE WHEN checkout_time IS NOT NULL THEN 1 ELSE 0 END AS checked_out,
                    CASE WHEN seating_hours IS NOT NULL THEN seating_hours ELSE '' END AS seating_hours,
                DATE_FORMAT(timelog_date, '%a, %D %M %Y') as timelog_date
                FROM timelogs where employee_id = $employee_id AND timelog_date = '$date'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkIfOnLeave($employee_id, $date)
    {
        //$sql = "SELECT * FROM leave_requests where employee_id = $employee_id AND status = 'approved' AND '$date' BETWEEN from_date AND to_date";
        $sql = "SELECT * FROM leave_requests where employee_id = $employee_id AND status = 'approved' AND '$date' BETWEEN DATE_FORMAT(FROM_UNIXTIME(from_date), '%Y-%m-%d') AND DATE_FORMAT(FROM_UNIXTIME(to_date), '%Y-%m-%d')";
        //echo $sql;echo "<br>";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkLeaveRequestStatus($employee_id, $date)
    {
        $sql = "SELECT * FROM leave_requests where employee_id = $employee_id AND '$date' BETWEEN from_date AND to_date";
        // echo $sql;echo "<br>";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkShortLeaveRequestStatus($employee_id, $date)
    {
        $date = date('Y-m-d', strtotime($date));
        $sql = "SELECT * FROM short_leave_requests where employee_id = $employee_id AND for_date = '$date'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkTimelogCorrectionRequestStatus($timelog_id)
    {
        $sql = "SELECT * FROM timelog_correction_requests where timelog_id = $timelog_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getAllEmployees()
    {
        $base_url = base_url();
        $sql = "SELECT e.id as employee_id, CONCAT( e.first_name,  ' ', e.last_name ) AS employee_name, e.designation as designation, CONCAT('$base_url',e.image) as image, s.status as status FROM employees e JOIN statuses s ON e.status_id = s.id ORDER BY employee_name ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function getTaskLastActivityId($task_id)
    {
        $sql = "SELECT * FROM task_activity WHERE task_id = $task_id ORDER BY updated_at DESC LIMIT 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return (int)$result->id;
        } else {
            return 0;
        }
    }

}