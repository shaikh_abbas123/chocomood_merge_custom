<?php
    Class Store_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("stores");

        }


        public function getAllStores($language_code, $active = false){
            if($language_code == 'EN'){
                $SystemLanguageID = 1;
            }else{
                $SystemLanguageID = 2;
            }
        $this->db->select('stores.*,stores_text.*,cities_text.Title as CityTitle');
        $this->db->from('stores');

        $this->db->join('stores_text','stores_text.StoreID = stores.StoreID AND stores_text.SystemLanguageID = '.$SystemLanguageID);
        $this->db->join('cities','cities.CityID = stores.CityID');
        $this->db->join('cities_text','cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = '.$SystemLanguageID);
       // $this->db->join('system_languages','system_languages.SystemLanguageID = cities_text.SystemLanguageID');
        
        
        //$this->db->where('system_languages.ShortCode',$language_code);
        
        
        $this->db->where('cities.Hide',0);
        if($active == true){
         $this->db->where('stores.IsActive',1);
        }
        
        return $this->db->get()->result();
        
        
    }


    }