<?php

Class Product_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("products");

    }

    public function getProducts($where = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'ASC',$rating = array())
    {
        if(!empty($rating)){
            $having = '(';
            $this->db->select('AVG(product_ratings.Rating) as Rating');
            $this->db->join('product_ratings', 'products.ProductID = product_ratings.ProductID','left');
            foreach ($rating as $key => $value) {
               $having .= 'Rating = '.$value.' OR ';
            }
            $having  = rtrim($having,' OR ');
            $having  .= ')';

            $this->db->having($having);
        }
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by($sort_by, $sort_as);
        $this->db->group_by('products.ProductID');


        $result = $this->db->get();
        return $result->result();
    }


    public function getStoreAvailability($where = false, $system_language_code = 'EN', $kg_product = 0)
    {
        if($kg_product == 1){
            $this->db->select('products.*,products_text.*,product_store_availability.StoreID,product_store_availability.GramQuantity as AvailableQuantity');
        }else{
          $this->db->select('products.*,products_text.*,product_store_availability.StoreID,product_store_availability.Quantity as AvailableQuantity');  
        }
        
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('product_store_availability', 'products.ProductID = product_store_availability.ProductID','left');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        


        $result = $this->db->get();
        return $result->row_array();
    }


    public function getCountProducts($where = false, $system_language_code = 'EN', $limit = false, $start = 0)
    {
        $this->db->select('COUNT(products.ProductID) as Total');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.ShortCode', $system_language_code);

        if ($limit) {
            $this->db->limit($limit, $start);
        }


        $result = $this->db->get();
        return $result->row();
    }

    public function getProductsOfCollection($product_ids, $system_language_code = 'EN', $limit = false, $start = 0, $count = false)
    {
        if ($count) {
            $this->db->select('COUNT(products.ProductID) as Total');

        } else {
            $this->db->select('products.*,products_text.*');

        }


        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where_in('products.ProductID', $product_ids);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        return $result->result();
    }

    public function getWishlistItems($UserID, $system_language_code = 'EN')
    {
        $this->db->select("user_wishlist.*,products_text.Title,products.ProductID,products.OutOfStock,products.Price,products.IsCorporateProduct,products.PriceType");
        $this->db->from('user_wishlist');
        $this->db->join('products', 'user_wishlist.ItemID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('user_wishlist.UserID', $UserID);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->result();
    }

    public function searchProducts($value, $system_language_code = 'EN')
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->like('products_text.Title', $value, 'both');
        $this->db->where('products.IsCustomizedProduct', 0);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->result();
    }

    public function getProductDetail($product_id, $system_language_code = 'EN')
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');
        $this->db->where('products.ProductID', $product_id);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        $result = $this->db->get();
        return $result->row();
    }

    public function getProductsWhereIn($category_ids = false, $system_language_code = 'EN', $limit = false, $start = 0, $sort_by = 'products_text.Title', $sort_as = 'ASC')
    {
        $this->db->select('products.*,products_text.*');
        $this->db->from('products');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages', 'products_text.SystemLanguageID = system_languages.SystemLanguageID');

        //$this->db->where_in('products.SubCategoryID', $category_ids);
        $find_in = '';
        foreach ($category_ids as $key => $value){
             if($key == 0){
                $find_in .= '( ';
             }else{
                $find_in .= ' OR ';
             }
            $find_in .= 'FIND_IN_SET('.$value.', products.SubCategoryID)';

            
        }
        if($find_in  != ''){
            $find_in .= ' )';
            $this->db->where($find_in);
        }
        
        //$this->db->where('FIND_IN_SET($category_ids, products.SubCategoryID) > 0');

        $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where('products.IsActive', 1);

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by($sort_by, $sort_as);


        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        
        return $result->result();
    }

    public function getProductsInCart()
    {
        $this->db->select("Count(order_items.ProductID) as TotalCart,order_items.*,products_text.Title,products.SKU,products.Price");
        $this->db->from('order_items');
        
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = 1', 'LEFT');
        
        $this->db->group_by('order_items.ProductID');
        $result = $this->db->get();
        return $result->result();


    }

    public function getProductsReviews()
    {
        $this->db->select("Count(product_reviews.ProductID) as TotalReviews,products_text.Title,products.SKU,products.ProductID");
        $this->db->from('product_reviews');
        
        $this->db->join('products', 'product_reviews.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = 1', 'LEFT');
        
        $this->db->group_by('product_reviews.ProductID');
        $result = $this->db->get();
        return $result->result();


    }

    public function getReviews($where = false)
    {
        $this->db->select("product_reviews.*,product_reviews.Title As ReviewTitle,products_text.Title,products.SKU,products.ProductID");
        $this->db->from('product_reviews');
        
        $this->db->join('products', 'product_reviews.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID AND products_text.SystemLanguageID = 1', 'LEFT');
        if($where){
            $this->db->where($where);
        }
        
        
        $result = $this->db->get();
        return $result->result();


    }


    public function getCities($where = false,$language = 'EN',$isCustom = 0)
    {
        $selected_lang = getLanguageBy(array('ShortCode' => $language));
        if($isCustom != 1)
        {
            $this->db->select("cities.latitude,cities.longitude,cities.CityID,cities_text.Title as CityTitle,stores.ShippingID,stores.DistrictID,stores.StoreID,stores.Latitude,stores.Longitude,stores_text.Address,stores_text.Title as StoreTitle,product_store_availability.Quantity, product_store_availability.GramQuantity");
        }
        else
        {
            $this->db->select("cities.latitude,cities.longitude,cities.CityID,cities_text.Title as CityTitle,stores.ShippingID,stores.DistrictID,stores.StoreID,stores.Latitude,stores.Longitude,stores_text.Address,stores_text.Title as StoreTitle");    
        }
        $this->db->from('cities');
        
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = '.$selected_lang->SystemLanguageID);
        $this->db->join('stores', 'stores.CityID = cities.CityID');
        $this->db->join('stores_text', 'stores.StoreID = stores_text.StoreID AND stores_text.SystemLanguageID = '.$selected_lang->SystemLanguageID);
        if($isCustom != 1)
        {
            $this->db->join('product_store_availability', 'product_store_availability.StoreID = stores.StoreID','left');
        }
        
        if($where){
            $this->db->where($where);
        }


        $this->db->order_by('cities_text.Title','ASC');
        $this->db->group_by('stores.StoreID');
        
        
        $result = $this->db->get();
        

        return $result->result();


    }

    public function getCountCustomerReviews()
    {
        $this->db->select("Count(product_reviews.UserID) as TotalReviews,users.UserID,users_text.FullName");
        $this->db->from('product_reviews');
        
        $this->db->join('users', 'product_reviews.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1', 'LEFT');
        
        $this->db->group_by('product_reviews.UserID');
        $result = $this->db->get();
        return $result->result();


    }


}