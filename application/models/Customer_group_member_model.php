<?php

Class Customer_group_member_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("customer_group_members");

    }

    public function getMembersWithOrders($id)
    {
        $sql = "SELECT users.UserID, users_text.FullName, users.Email, users.Mobile, COUNT(orders.OrderID) AS count_sum FROM customer_group_members LEFT JOIN users ON customer_group_members.UserID = users.UserID LEFT JOIN users_text ON users.UserID = users_text.UserID LEFT JOIN orders ON users.UserID = orders.UserID WHERE customer_group_members.CustomerGroupID = $id AND users_text.SystemLanguageID = 1 GROUP BY users.UserID ORDER BY count_sum DESC";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getMembersWithPurchases($id)
    {
        $sql = "SELECT users.UserID, users_text.FullName, users.Email, users.Mobile, SUM(orders.TotalAmount) AS count_sum FROM customer_group_members LEFT JOIN users ON customer_group_members.UserID = users.UserID LEFT JOIN users_text ON users.UserID = users_text.UserID LEFT JOIN orders ON users.UserID = orders.UserID WHERE customer_group_members.CustomerGroupID = $id AND users_text.SystemLanguageID = 1 GROUP BY users.UserID ORDER BY count_sum DESC";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


}