<?php
Class Order_item_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("order_items");

    }

    public function getOrdersItemsOld($order_id){
        $this->db->select('orders.*, order_items.*, products.*, products_text.*');
        $this->db->from('orders');
        $this->db->join('order_items', 'orders.OrderID = order_items.OrderID', 'LEFT');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID', 'LEFT');
        $this->db->join('system_languages as slpt', 'slpt.SystemLanguageID = products_text.SystemLanguageID', 'Left');
        $this->db->where('slpt.IsDefault', '1');
        $this->db->where('orders.OrderID', $order_id);
        $this->db->group_by('order_items.OrderItemID');
        $result = $this->db->get();
        return $result->result();
    }

    public function getOrdersItems($order_id, $system_language_code = 'EN'){
        $this->db->select("orders.*, order_items.*,products_text.Title,products.SKU,products_text.Description,products.Price,products.PriceType,products.IsCorporateProduct");
        $this->db->from('order_items');
        $this->db->join('orders', 'order_items.OrderID = orders.OrderID', 'LEFT');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID', 'LEFT');
        $this->db->join("system_languages slpt", "products_text.SystemLanguageID = slpt.SystemLanguageID AND slpt.ShortCode = '$system_language_code'", "LEFT");
        $this->db->where('orders.OrderID', $order_id);
        $this->db->group_by('order_items.OrderItemID');
        $result = $this->db->get();
        return $result->result();
    }

    public function getOrdersItemsWhere($UserID, $ProductID, $system_language_code = 'EN'){
        $this->db->select("orders.*, order_items.*,products_text.Title,products.Price,products.IsCorporateProduct");
        $this->db->from('order_items');
        $this->db->join('orders', 'order_items.OrderID = orders.OrderID', 'LEFT');
        $this->db->join('products', 'order_items.ProductID = products.ProductID', 'LEFT');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID', 'LEFT');
        $this->db->join("system_languages slpt", "products_text.SystemLanguageID = slpt.SystemLanguageID AND slpt.ShortCode = '$system_language_code'", "LEFT");
        $this->db->where('order_items.ProductID', $ProductID);
        $this->db->where('order_items.ItemType', 'Product');
        $this->db->where('orders.UserID', $UserID);
        $this->db->group_by('order_items.OrderItemID');
        $result = $this->db->get();
        return $result->result();
    }

    public function getSubItem($itemId){
        $this->db->select("*");
        $this->db->from('order_items');
        $this->db->where('OrderItemID ', $itemId);
        $result = $this->db->get();
        return $result->result();
    }

    public function getSubItemDetial($itemId, $itemType){
        $this->db->select("*");
        if($itemType=="Choco Message"){
            $this->db->from('customization_characters');
            $this->db->where('CharacterID', $itemId);
        }elseif($itemType=="Choco Box"){
            $this->db->from('products');
            $this->db->join('site_images', 'products.ProductID = site_images.FileID', 'LEFT');
            $this->db->where('ProductID', $itemId);
            $result = $this->db->get();
            $result = $result->result();
            $result[0]->CharacterImage = $result[0]->ImageName;
            return $result;//$result->result();
        }elseif ($itemType=="Choco Shape") {
            $this->db->from('shapes');
            $this->db->where('ShapeID', $itemId);
            $result = $this->db->get();
            $result = $result->result();
            $result[0]->CharacterImage = $result[0]->ShapeImage;
            return $result;//$result->result();
        }elseif ($itemType=="ribbons") {
            $this->db->from('ribbons');
            $this->db->where('RibbonID', $itemId);
        }
        $result = $this->db->get();
        return $result->result();
    }
    public function getSideItemDetail($sideItemId, $sideItemType, $sideItemTypeCol, $languageCode="EN"){
        $this->db->select("*");
        $this->db->from($sideItemType);
        if($sideItemType == "boxes"){
            $this->db->join('boxes_text', 'boxes.BoxID = boxes_text.BoxID ', 'LEFT');
            $this->db->where("SystemLanguageID", $languageCode=="EN"?1:2);
        }
        $this->db->where_in($sideItemType.".".$sideItemTypeCol, explode(",",$sideItemId));
        $result = $this->db->get();
        return $result->result();
    }

    public function getContentTypeWithTextnImage($id, $tableName, $colName, $languageCode){
        $this->db->select("*");
        $this->db->from($tableName);
        $this->db->where($colName, $id);
        $this->db->where("SystemLanguageID", $languageCode=="EN"?1:2);
        $result = $this->db->get();
        return $result->result();
    }

    public function getContentSubTypeWithImage($subId, $imageId, $languageCode){
        $this->db->select("*");
        $this->db->from("shape_content_type_sub_text");
        $this->db->join('site_images', 'shape_content_type_sub_text.ContentTypeSubID = site_images.FileID', 'LEFT');
        $this->db->where("SiteImageID", $imageId);
        $this->db->where("ContentTypeSubID", $subId);
        $this->db->where("SystemLanguageID", $languageCode=="EN"?1:2);
        $result = $this->db->get();
        return $result->result();
    }
}