<?php

class Base_Controller extends CI_Controller
{
    protected $language;
    protected $UserID;

    public function __construct()
    {
        parent::__construct();
        $this->load->Model('Category_model');
        $this->load->helper('cookie');
        $this->load->model('User_model');
        if ($this->session->userdata('lang')) {
            $this->language = $this->session->userdata('lang');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }
        if ($this->session->userdata('user')) {
            $this->UserID = $this->session->userdata['user']->UserID;
            $update['UpdatedAt'] =  date('Y-m-d H:i:s');
            $update['OnlineStatus'] =  'Online';
            $update_by['UserID'] = $this->UserID;
            $this->User_model->update($update, $update_by);
            $this->checkUsersStatus($this->User_model);
        }else if($this->session->userdata('admin')){
            $this->UserID = $this->session->userdata['admin']['UserID'];
            $update['UpdatedAt'] =  date('Y-m-d H:i:s');
            $update['OnlineStatus'] =  'Online';
            $update_by['UserID'] = $this->UserID;
            $this->User_model->update($update, $update_by);
            $this->checkUsersStatus($this->User_model);
        } else {
            if (!get_cookie('temp_user_key')) {
                $this->UserID = time();
                $cookie = array(
                    'name' => 'temp_user_key',
                    'value' => $this->UserID,
                    'expire' => time() + 86500,
                );
                set_cookie($cookie);
            } else {
                $this->UserID = get_cookie('temp_user_key');
            }
        }
        
        $this->data['site_setting'] = $this->getSiteSetting();
        if($this->uri->segment(1) == 'cms'){
            $this->language = 'EN';
        }

    }


    public function checkUsersStatus($User_model){
        $data = $User_model->getMultipleRows(array('OnlineStatus' => 'Online'));
        if(!empty($data)){
            foreach($data as $k => $v){
                $hours = $this->time_difference($v->UpdatedAt);
                if($hours >= 3){
                    $update['OnlineStatus'] =  'Offline';
                    $update_by['UserID'] = $v->UserID;
                    $User_model->update($update, $update_by);
                }

            }
        }
       
    }

    public function time_difference($time){
        $datetime1 = new DateTime();
        $datetime2 = new DateTime($time);
        $diff = $datetime1->diff($datetime2);
        $hours = $diff->h;
        $hours = $hours + ($diff->days*24);
        if($time == ''){
            $hours = 5;
        }
        return $hours;
    }

    public function changeLanguage($language)
    {
        $this->load->Model('System_language_model');
        $fetch_by['ShortCode'] = $language;
        $result = $this->System_language_model->getWithMultipleFields($fetch_by);
        if (!$result) {

            $default_lang = getDefaultLanguage();
            $language = $default_lang->ShortCode;
        }
        $this->session->set_userdata('lang', $language);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getSiteSetting()
    {

        $this->load->model('Site_setting_model');
        return $this->Site_setting_model->get(1, false, 'SiteSettingID');
    }

    public function uploadImage($file_key, $path, $id = false, $type = false, $multiple = false)
    {
        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif","svg","webp");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {


                $originalImage = $_FILES[$file_key]["tmp_name"][$key];
                if (preg_match('/jpg|jpeg/i',$ext))
                {
                    $imageTmp = imagecreatefromjpeg($originalImage);
                    $w=imagesx($imageTmp);
                    $h=imagesy($imageTmp);
                    $webp = imagecreatetruecolor($w,$h);
                    imagecopy($webp,$imageTmp,0,0,0,0,$w,$h);
                    $file_name = rand(9999, 99999999999) . date('Ymdhsi').'.webp';
                    ini_set('memory_limit', '-1');
                    imagepalettetotruecolor($webp);
                    imagewebp($webp,$path.$file_name, 75);
                    imagedestroy($imageTmp);
                    imagedestroy($webp);

                    
                }
                else if (preg_match('/png/i',$ext))
                {
                    
                    $imageTmp = imagecreatefrompng($originalImage);
                    $file_name = str_replace("png", "webp", $file_name);
                    imagepalettetotruecolor($imageTmp);
                    imagewebp($imageTmp,$path . $file_name,75);
                }
                else if (preg_match('/gif/i',$ext))
                {   
                    $imageTmp=imagecreatefromgif($originalImage);
                    $file_name = str_replace("gif", "webp", $file_name);
                    imagepalettetotruecolor($imageTmp);
                    imagewebp($imageTmp,$path . $file_name,75);
                }
                else if (preg_match('/svg/i',$ext))
                {
                    move_uploaded_file($file_tmp, $path . $file_name);    
                }
                else if (preg_match('/webp/i',$ext))
                {
                    move_uploaded_file($file_tmp, $path . $file_name);    
                }
                

                 
                
                // echo $file_name;die;
                if (!$multiple) {
                    return $path . $file_name;
                } else {
                    $this->load->model('Site_images_model');
                    $data['FileID'] = $id;
                    $data['ImageType'] = $type;
                    $data['ImageName'] = $path . $file_name;
                    $this->Site_images_model->save($data);
                }
                /* $data['DestinationID'] = $id; 
                  $data['ImagePath'] = $path.$file_name;
                  $this->Site_images_model->save($data); */
            }
        }
        return true;
    }
    public function uploadImageSingle($file_key, $path, $id = false, $type = false)
    {
        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]["name"]);
        $file_tmp = $_FILES[$file_key]["tmp_name"];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        if (in_array($ext, $extension)) {
            move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"], $path . $file_name);
            $this->load->model('Site_images_model');
            $data['FileID'] = $id;
            $data['ImageType'] = $type;
            $data['ImageName'] = $path . $file_name;
            $insertedId = $this->Site_images_model->save($data);
            return $insertedId;
        }
        return 0;
    }

    public function DeleteImage()
    {
        $deleted_by = array();
        $ImagePath = $this->input->post('image_path');
        $deleted_by['SiteImageID'] = $this->input->post('image_id');
        if (file_exists($ImagePath)) {
            unlink($ImagePath);
        }
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


    public function deleteImage2(){
        
        $this->load->Model('Site_images_model');
        

        $deleted_by = array();
        $deleted_by['SiteImageID'] = $this->input->post('id');
        
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;

    }

}
