<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use SmsaSDK\Smsa;
class Order extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');
        $this->load->model('Temp_order_model');
        $this->load->model('User_address_model');
        $this->load->model('Order_extra_charges_model');
        $this->load->model('Coupon_model');
        $this->load->model('Product_model');
        $this->load->model('Store_model');
        $this->load->model('Product_availability_model');
        $this->load->model('Character_model');
        $this->load->model('Shape_model');
        $this->load->model('Box_model');
        $this->load->model('Ribbon_model');
        $this->load->model('Filling_model');
        $this->load->model('Fill_Color_model');
        $this->load->model('Wrapping_model');
        $this->load->model('Powder_Color_model');
        $this->load->model('Content_type_sub_model');
        $this->load->model('Content_type_model');
        $this->data['language'] = $this->language;

    }

    public function placeOrder()
    {
        $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($data['cart_items'])) {
            $response['message'] = lang('no_items_in_basket');
            $response['redirect'] = true;
            $response['url'] = 'cart';
            echo json_encode($response);
            exit();
        }
        $order_data['UserID'] = $this->UserID;
        $show_error = false;
        $error_message = '';
        $model_html = '';
        $show_model = false;

        foreach($data['cart_items'] as $product){
            if($product->PriceType == 'kg'){
                $product_data = $this->Product_model->getStoreAvailability('product_store_availability.StoreID = '.$_GET['StoreID'].' AND products.ProductID = '.$product->ProductID, $this->language, 1);

            }else{
             $product_data = $this->Product_model->getStoreAvailability('product_store_availability.StoreID = '.$_GET['StoreID'].' AND products.ProductID = '.$product->ProductID);
            }
            //echo $this->db->last_query();
            
            if($product->PriceType == 'kg'){
                    $quantity = $product->Quantity * 1000;
                    $unit = ($this->language == 'AR' ? 'غرام' : 'Grams');
                }else{
                    $quantity = $product->Quantity;
                    $unit = lang('pcs');
                }

            $product_image = get_images($product->ProductID, 'product',false);
            if($product_data && $product->Quantity <= $product_data['AvailableQuantity']){

                
                
                $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong> '.$product->Title.'<br> '.$quantity.' '.$unit.'</strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p>'.($product->Quantity * $product->TempItemPrice).' '.lang('SR').'</p>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif($product_data && $product_data['AvailableQuantity'] <  $product->Quantity && $product_data['AvailableQuantity'] > 0){
                $show_model = true;
                $this->Temp_order_model->update(array('Quantity' => $product_data['AvailableQuantity']),array('TempOrderID' => $product->TempOrderID));
                $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> <span class="green">'.$product_data['AvailableQuantity'].' '.lang('pcs').' '.lang('only').' </span></strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="red">'.($product->Quantity * $product->TempItemPrice).' SR</span></p>
                                            <p><span class="green">'.($product_data['AvailableQuantity'] * $product->TempItemPrice).' SR</span></p>
                                            <button href="javascript:void(0);" onclick="removeIt(\'cart/removeFromCart\', \'TempOrderID\', '.$product->TempOrderID.');" class="crs-btn btn-dark"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif($product_data && $product_data['AvailableQuantity'] == 0){
                $show_model = true;
                $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));
                 $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> </strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="green">'.lang('Sold_out').'</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif(!$product_data){
                $show_model = true;
                $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));
                 $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> </strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="green">'.lang('Sold_out').'</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>';

            }


            

        }

        if($show_model){
            $response['model_html'] = $model_html;
            $response['show_model'] = true;
            echo json_encode($response);
            exit();

        }

        $paymentMethod = $_GET['paymentMethod'];
        $online_pay = ($paymentMethod == "cod")? 0 : 1;
        /*$order = $this->orderEntry($_GET, $online_pay);
        if($order == false){
            $response['message'] = lang('something_went_wrong');
            $response['redirect'] = false;
            $response['show_model'] = false;
            echo json_encode($response);
            exit();
        }else{
            if($paymentMethod == "cod"){
                 $order['url'] = 'checkout/thank_you/'.base64_encode($order['insert_id']);
                 echo json_encode($order);
                 exit();
            }else if($paymentMethod == 'paytab'){
                $order['url'] = 'checkout/pay_tab/'.base64_encode($order['insert_id']);
                echo json_encode($order);
                exit();
            }
        }*/

        if($paymentMethod == "cod"){
            $order = $this->orderEntry($_GET, $online_pay);
            if($order == false){
                $response['message'] = lang('something_went_wrong');
                $response['redirect'] = false;
                $response['show_model'] = false;
                echo json_encode($response);
                exit();
            }else{
                $order['url'] = 'checkout/thank_you/'.base64_encode($order['insert_id']);
                echo json_encode($order);
                exit(); 
            }

        }else if($paymentMethod == 'paytab'){
            $order['url'] = 'checkout/pay_tab/'.base64_encode($_GET['total_price_val']);
            $this->session->set_userdata('StoreIDOrder', $_GET['StoreID']);
            $this->session->set_userdata('customizeOrder', 0);
            $order['message'] = lang('thank_you_for_order');
            $order['redirect'] = true;
            $order['show_model'] = false;
            echo json_encode($order);
            exit();
        }
    }

    public function placeOrderCustomize()
    {
        $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        // exit(var_dump($data['cart_items']));
        if (empty($data['cart_items'])) {
            $response['message'] = lang('no_items_in_basket');
            $response['redirect'] = true;
            $response['url'] = 'cart';
            echo json_encode($response);
            exit();
        }
        $order_data['UserID'] = $this->UserID;
        $show_error = false;
        $error_message = '';
        $model_html = '';
        $show_model = false;

        //if check Inventory Availability

        //after check show Message
        if($show_model){
            $response['model_html'] = $model_html;
            $response['show_model'] = true;
            echo json_encode($response);
            exit();
        }

        //check payment method
        $paymentMethod = $_GET['paymentMethod'];
        $online_pay = ($paymentMethod == "cod")? 0 : 1;

        if($paymentMethod == "cod"){
            $order = $this->orderEntryCustomize($_GET, $online_pay);
            if($order == false){
                $response['message'] = lang('something_went_wrong');
                $response['redirect'] = false;
                $response['show_model'] = false;
                echo json_encode($response);
                exit();
            }else{
                $order['url'] = 'checkout/thank_you/'.base64_encode($order['insert_id']);
                echo json_encode($order);
                exit(); 
            }

        }else if($paymentMethod == 'paytab'){
            $order['url'] = 'checkout/pay_tab/'.base64_encode($_GET['total_price_val']);
            $this->session->set_userdata('StoreIDOrder', $_GET['StoreID']);
            $this->session->set_userdata('customizeOrder', 1);
            $order['message'] = lang('thank_you_for_order');
            $order['redirect'] = true;
            $order['show_model'] = false;
            echo json_encode($order);
            exit();
        }
    }

    public function orderEntry($GET, $online_pay = 0){
         $order_data['UserID'] = $this->UserID;
         $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
         if (isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) {
            $order_data['CollectFromStore'] = 1;
            //$user_city_id = $this->session->userdata['admin']['CityID'];
        } else {
            $order_data['CollectFromStore'] = 0;
            $fetch_address_by['UserID'] = $this->UserID;
            $fetch_address_by['IsDefault'] = 1;
            $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
            $order_data['AddressID'] = $address->AddressID;
            $user_city_id = $address->CityID;

            // saving address to collect payment
            /*$fetch_payment_address_by['UserID'] = $this->UserID;
            $fetch_payment_address_by['UseForPaymentCollection'] = 1;
            $address_for_payment = $this->User_address_model->getWithMultipleFields($fetch_payment_address_by);
            if ($address_for_payment) {
                $order_data['AddressIDForPaymentCollection'] = $address_for_payment->AddressID;
            } else {
                $order_data['AddressIDForPaymentCollection'] = $address->AddressID;
            }*/

            $order_data['ShipmentMethodID'] = ($this->session->userdata('ShipmentMethodIDForBooking'))? $this->session->userdata('ShipmentMethodIDForBooking') : 0;
        }
        //$order_data['BranchDeliveryDistrictID'] = $_GET['BranchDeliveryDistrictID'];
        $order_data['StoreID'] = $GET['StoreID'];
        $order_data['CreatedAt'] = date('Y-m-d H:i:s');
        $order_data['OrderNumber'] = time() . $this->UserID;
        $order_data['PaymentMethod'] = $this->session->userdata('PaymentMethodForBooking');
        $insert_id = $this->Order_model->save($order_data);
        if ($insert_id > 0) {
            $response['insert_id'] = $insert_id;
            // $this->sendThankyouEmailToCustomer($insert_id);
            $get_store_data = $this->Store_model->get($GET['StoreID'],false,'StoreID');
            $order_item_data = array();
            $total_amount = 0;
            foreach ($data['cart_items'] as $product) {
                $order_item_data[] = [
                    'OrderID' => $insert_id,
                    'ProductID' => $product->ProductID,
                    'Quantity' => $product->Quantity,
                    'Amount' => $product->TempItemPrice,
                    'ItemType' => $product->ItemType,
                    'DiscountType' => $product->DiscountType,
                    'Discount' => $product->Discount,
                    'CustomizedOrderProductIDs' => $product->CustomizedOrderProductIDs,
                    'CustomizedShapeImage' => $product->CustomizedShapeImage,
                    'CustomizedBoxID' => $product->CustomizedBoxID,
                    'IsCorporateItem' => $product->IsCorporateItem,
                    'Ribbon' => $product->Ribbon,
                    'package_id' => $product->Package
                ];
                $IsOnOffer = false;
                $Price = $product->TempItemPrice;
                $DiscountType = $product->DiscountType;
                $DiscountFactor = $product->Discount;
                if ($DiscountType == 'percentage') {
                    $IsOnOffer = true;
                    $Discount = ($DiscountFactor / 100) * $Price;
                    if ($Discount > $Price) {
                        $ProductDiscountedPrice = 0;
                    } else {
                        $ProductDiscountedPrice = $Price - $Discount;
                    }
                } elseif ($DiscountType == 'per item') {
                    $IsOnOffer = true;
                    $Discount = $DiscountFactor;
                    if ($Discount > $Price) {
                        $ProductDiscountedPrice = 0;
                    } else {
                        $ProductDiscountedPrice = $Price - $DiscountFactor;
                    }
                } else {
                    $Discount = 0;
                    if ($Discount > $Price) {
                        $ProductDiscountedPrice = 0;
                    } else {
                        $ProductDiscountedPrice = $Price;
                    }
                }
                $total_amount += $product->Quantity * $ProductDiscountedPrice;

                // increase product purchase count here
                $product_dt_for_purchase_count = $this->Product_model->get($product->ProductID, false, 'ProductID');
                $PurchaseCount = (int)$product_dt_for_purchase_count->PurchaseCount + (int)$product->Quantity;
                $this->Product_model->update(array('PurchaseCount' => $PurchaseCount), array('ProductID' => $product->ProductID));


                //update product quantity

                if($get_store_data){

                    $availability_product = $this->Product_availability_model->getWithMultipleFields(array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                    if($availability_product){
                        if($product->PriceType == 'kg'){
                            $quantity = $availability_product->GramQuantity - ($product->Quantity * $product->package_weight);
                            $this->Product_availability_model->update(array('GramQuantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                        }else{
                            $quantity = $availability_product->Quantity - $product->Quantity;
                            $this->Product_availability_model->update(array('Quantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                        }

                    }

                }


            }

            if ($this->session->userdata('order_coupon')) {
                $order_coupon = $this->session->userdata('order_coupon');
                $coupon_code = $order_coupon['CouponCode'];
                $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total_amount;
                $total_amount = $total_amount - $coupon_discount_availed;

                // reducing coupon usage count
                $coupon_detail = $this->Coupon_model->getWithMultipleFields(array('CouponID' => $order_coupon['CouponID']), true);
                $update['UsageCount'] = $coupon_detail['UsageCount'] - 1;
                $update_by['CouponID'] = $order_coupon['CouponID'];
                $this->Coupon_model->update($update, $update_by);
            } else {
                $coupon_code = '';
                $coupon_discount_percentage = 0;
                $coupon_discount_availed = 0;
            }

            $ShipmentMethodIDForBooking = $this->session->userdata('ShipmentMethodIDForBooking');
            $shipping_amount = 0;
            $semsa_shipping_amount = 0;
            $freeshipment = false;
            
            $total_tax = 0;
            $taxes = getTaxShipmentCharges('Tax');
            foreach ($taxes as $tax) {
                $tax_id = $tax->TaxShipmentChargesID;
                $tax_title = $tax->Title;
                $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                if ($tax->Type == 'Fixed') {
                    $tax_amount = $tax->Amount;
                } elseif ($tax->Type == 'Percentage') {
                    // $tax_amount = ($tax->Amount / 100) * ($total_amount + $shipping_amount + $semsa_shipping_amount);
                    $tax_amount = ($tax->Amount / 100) * ($total_amount);
                }
                $total_tax += $tax_amount;
                $order_extra_charges['OrderID'] = $insert_id;
                $order_extra_charges['TaxShipmentChargesID'] = $tax_id;
                $order_extra_charges['Title'] = $tax_title;
                $order_extra_charges['Factor'] = $tax_factor;
                $order_extra_charges['Amount'] = $tax_amount;
                $this->Order_extra_charges_model->save($order_extra_charges);
            }
            $get_total =  $total_amount + $total_tax;
            if($order_data['CollectFromStore'] != 1){
                $shipment_method = getSelectedShippingMethodDetail($ShipmentMethodIDForBooking, $this->language);
                if ($shipment_method) {
                    $freeshipment = true;
                    $shipping_id = $shipment_method->TaxShipmentChargesID;
                    $shipping_title = $shipment_method->Title;
                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                    if ($shipment_method->Type == 'Fixed') {
                        $shipping_amount = $shipment_method->Amount;
                    } elseif ($shipment_method->Type == 'Percentage') {
                        $shipping_amount = ($shipment_method->Amount / 100) * ($total_amount);
                    }
                    $apply_free_shipping = false;
                    if ($get_total >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                        $shipping_factor = freeShippingTitle();
                        $shipping_amount = 0.00;
                    }
                    $order_extra_charges['OrderID'] = $insert_id;
                    $order_extra_charges['TaxShipmentChargesID'] = $shipping_id;
                    $order_extra_charges['Title'] = $shipping_title;
                    $order_extra_charges['Factor'] = $shipping_factor;
                    $order_extra_charges['Amount'] = $shipping_amount;
                    $this->Order_extra_charges_model->save($order_extra_charges);
                }
               
            }

            if($freeshipment && $get_total >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                $shipping_amount = 0; 
            }
            $total_amount = $total_amount + $shipping_amount + $total_tax + $semsa_shipping_amount;

            $order_data['CouponCodeUsed'] = $coupon_code;
            $order_data['CouponCodeDiscountPercentage'] = $coupon_discount_percentage;
            $order_data['DiscountAvailed'] = $coupon_discount_availed;
            $order_data['TotalShippingCharges'] = $shipping_amount;
            $order_data['TotalTaxAmount'] = $total_tax;
            $order_data['TotalAmount'] = $total_amount;
            $order_data['OrderNumber'] = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
            $this->Order_item_model->insert_batch($order_item_data);
            $this->Order_model->update($order_data, array('OrderID' => $insert_id));

            //semsa
            $awb = "";
            if($order_data['CollectFromStore'] != 1){
             if($this->session->userdata('SemsaShipmentID') && $this->session->userdata('SemsaShipmentID') == 'semsa'){
                    $semsa_shipping_amount = findSemsaShippingCharges();
                    $total_amount =  number_format(($total_amount + $semsa_shipping_amount), 2);
                    $amount = ($online_pay != 1)? $total_amount : 0;
                    $semsa = $this->semsa_shipment(['amount' => $amount],$get_store_data);
                    if($semsa['status'] == 'DATA RECEIVED'){
                        $this_data['TotalAmount'] = $total_amount;
                        $this_data['TotalShippingCharges'] = $semsa_shipping_amount;
                        $this_data['AWBNumber'] = $semsa['awbNumber'];
                        $this_data['ShippedThroughApi'] = 1;
                        $awb = $semsa['awbNumber'];
                        $this_data['SemsaShippingAmount'] = $semsa_shipping_amount;
                        $this->Order_model->update($this_data, array('OrderID' => $insert_id));
                        $this->session->unset_userdata('SemsaShipmentID');
                    }
                }
            }

            //if($online_pay != 1){
                $deleted_by['UserID'] = $this->UserID;
                $this->Temp_order_model->delete($deleted_by);
                $this->sendOrderConfirmationToCustomer($insert_id, $awb);
           // }
            // $order_data_pusher = $this->Order_model->get($insert_id, true, 'OrderID');
            // pusher($order_data_pusher, "Chocomood_Order_Channel", "Chocomood_Order_Event");
            $response['message'] = lang('thank_you_for_order');
            $response['redirect'] = true;
            $response['show_model'] = false;
            $response['insert_id'] = $insert_id;
            
            //$response['url'] = 'order/paymentMethod/'.base64_encode($insert_id);
           
            //$response['url'] = 'checkout/pay_tab/'.base64_encode($insert_id);
           // $response['url'] = 'checkout/thank_you/'.base64_encode($insert_id);
            return $response;
           
        } else {
            return false;
            
        }
    }

    public function orderEntryCustomize($GET, $online_pay = 0){
        $order_data['UserID'] = $this->UserID;
        $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        // print_rm($data['cart_items']);
        // var_dump($data['cart_items']);
        if (isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) {
            $order_data['CollectFromStore'] = 1;
        }
        else 
        {
            $order_data['CollectFromStore'] = 0;
            $fetch_address_by['UserID'] = $this->UserID;
            $fetch_address_by['IsDefault'] = 1;
            $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
            $order_data['AddressID'] = $address->AddressID;
            $user_city_id = $address->CityID;
            $order_data['ShipmentMethodID'] = ($this->session->userdata('ShipmentMethodIDForBooking'))? $this->session->userdata('ShipmentMethodIDForBooking') : 0;
        }

        $order_data['StoreID'] = $GET['StoreID'];
        $order_data['CreatedAt'] = date('Y-m-d H:i:s');
        $order_data['OrderNumber'] = time() . $this->UserID;
        $order_data['PaymentMethod'] = $this->session->userdata('PaymentMethodForBooking');
        $order_data['isCustomize'] = 1;
        $insert_id = $this->Order_model->save($order_data);

        if ($insert_id > 0) {
            $response['insert_id'] = $insert_id;
            // $this->sendThankyouEmailToCustomer($insert_id);
            $get_store_data = $this->Store_model->get($GET['StoreID'],false,'StoreID');
            $order_item_data = array();
            $total_amount = 0;
            foreach ($data['cart_items'] as $product) {

                $price = array();
                $price_list= '';
                $boxPrice = 0;
                $ribbonPrice = 0;
                $wrapPrice = array();
                $wrappingPrice = '';
                $fillPrice = array();
                $fillingPrice = '';
                $fillColorPrice = array();
                $fillColorPrices = '';
                $PowderColorPrice =array();
                $PowderColorPrice_list='';
                if ($product->ItemType == 'Choco Message' || $product->ItemType == 'Choco Box') {
                    $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $product->CustomizedBoxID . " AND system_languages.ShortCode = '" . $this->language . "'");
                    $boxPrice = $box[0]->BoxPrice;
                    $ProductIDs = $product->CustomizedOrderProductIDs;
                    $ProductIDs = explode(',', $ProductIDs);
                    foreach ($ProductIDs as $ProductID) {
                        if ($product->ItemType == 'Choco Box') {
                            $product_temp = $this->Product_model->getProductDetail($ProductID, $this->language);
                            array_push($price, number_format($product_temp->Price, 2));
                        } elseif ($product->ItemType == 'Choco Message') {
                            $product_temp = $this->Character_model->getData('CharacterID = ' . $ProductID);
                            array_push($price, number_format($product_temp[0]->price, 2));
                        }
                    }
                    
                    $price_list = implode(',', $price);
                }
                else if($product->ItemType == 'Choco Shape')
                {
                    $ProductIDs = $product->CustomizedOrderProductIDs;
                    $ProductIDs = explode(',', $ProductIDs);
                    
                    $shapeData = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = ".$ProductIDs[0]." AND system_languages.ShortCode = '" . $this->language . "'");
                    $price = $shapeData[0]->PricePerPcs;
                    $price_list =  $price;
                }

                if($product->Ribbon != 0 && $product->Ribbon > 0)
                {
                    $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $product->Ribbon . " AND system_languages.ShortCode = '" . $this->language . "'");
                    $ribbonPrice = $ribbon[0]->RibbonPrice;
                }

                if ($product->Wrapping != 0) {
                     
                    $wrappingIDs = $product->Wrapping;
                    $wrappingIDs = explode(',', $wrappingIDs);
                    foreach ($wrappingIDs as $wrappingID) 
                    {
                        $wrapping = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID = " . $wrappingID . " AND system_languages.ShortCode = '" . $this->language . "'");
                        array_push($wrapPrice, number_format($wrapping[0]->WrappingPrice, 2));
                     
                    }
                    $wrappingPrice = implode(',', $wrapPrice);
                }

                if ($product->Fillings != 0) {
                     
                    $FillingIDs = $product->Fillings;
                    $FillingIDs = explode(',', $FillingIDs);
                    foreach ($FillingIDs as $FillingID) 
                    {
                        $filling = $this->Filling_model->getJoinedData(false, 'FillingId', "fillings.FillingId = " . $FillingID . " AND system_languages.ShortCode = '" . $this->language . "'");
                        array_push($fillPrice, number_format($filling[0]->price, 2));
                     
                    }
                    $fillingPrice = implode(',', $fillPrice);
                }

                if ($product->FillColor != 0) {
                     
                    $FillColorsIDs = $product->FillColor;
                    $FillColorsIDs = explode(',', $FillColorsIDs);
                    foreach ($FillColorsIDs as $FillColorsID) 
                    {
                        $fill_color = $this->Filling_model->getJoinedData(false, 'FillingId', "fillings.FillingId = " . $FillingID . " AND system_languages.ShortCode = '" . $this->language . "'");
                        array_push($fillColorPrice, number_format($fill_color[0]->price, 2));
                     
                    }
                    $fillColorPrices = implode(',', $fillColorPrice);
                }

                if ($product->PowderColor != 0) {
                     
                    $PowderColorIDs = $product->PowderColor;
                    $PowderColorIDs = explode(',', $PowderColorIDs);
                    foreach ($PowderColorIDs as $PowderColorID) 
                    {
                        $powder_color = $this->Powder_Color_model->getJoinedData(false, 'PowderColorId', "powder_colors.PowderColorId = " . $PowderColorID . " AND system_languages.ShortCode = '" . $this->language . "'");
                        array_push($PowderColorPrice, number_format($powder_color[0]->price, 2));
                     
                    }
                    $PowderColorPrice_list = implode(',', $PowderColorPrice);
                }

                if($product->content_type_id != '')
                {
                    $content_type = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = " . $product->content_type_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                    $content_price = $content_type[0]->Price;
                      
                }

                if($product->content_type_sub_id != '')
                {
                    $content_type_sub = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = " . $product->content_type_sub_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                    $content_sub_price = $content_type_sub[0]->Price;
                      
                }
                $order_item_data[] = [
                    'OrderID' => $insert_id,
                    'ProductID' => $product->ProductID,
                    'Quantity' => $product->Quantity,
                    'Amount' => $product->TempItemPrice,
                    'ItemType' => $product->ItemType,
                    'CustomizedOrderProductIDs' => $product->CustomizedOrderProductIDs,
                    'CustomizedOrderProductPrice' => $price_list,
                    'CustomizedShapeImage' => $product->CustomizedShapeImage,
                    'CustomizedBoxID' => $product->CustomizedBoxID,
                    'CustomizedBoxPrice' => @$boxPrice,
                    'IsCorporateItem' => $product->IsCorporateItem,
                    'Ribbon' => $product->Ribbon,
                    'RibbonPrice' => @$ribbonPrice,
                    'package_id' => $product->Package,
                    'Wrapping' => $product->Wrapping,
                    'WrappingPrice' => @$wrappingPrice,
                    'Fillings' =>$product->Fillings,
                    'FillingsPrice' => @$fillingPrice,
                    'FillColor' =>$product->FillColor,
                    'FillColorPrice' => @$fillColorPrices,
                    'PowderColor' =>$product->PowderColor,
                    'PowderColorPrice' => @$PowderColorPrice_list,
                    'content_type_id' =>$product->content_type_id,
                    'content_type_price' => @$$content_price,
                    'content_type_sub_id' =>$product->content_type_sub_id,
                    'content_type_sub_price' => @$content_sub_price,
                    'content_text' =>$product->content_text,
                    'content_type_sub_image_id' =>$product->content_type_sub_image_id,
                ];
                $total_amount += $product->Quantity * $product->TempItemPrice;
                

                // increase product purchase count here
                // $product_dt_for_purchase_count = $this->Product_model->get($product->ProductID, false, 'ProductID');
                // $PurchaseCount = (int)$product_dt_for_purchase_count->PurchaseCount + (int)$product->Quantity;
                // $this->Product_model->update(array('PurchaseCount' => $PurchaseCount), array('ProductID' => $product->ProductID));


                // //update product quantity

                // if($get_store_data){

                //     $availability_product = $this->Product_availability_model->getWithMultipleFields(array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                //     if($availability_product){
                //         if($product->PriceType == 'kg'){
                //             $quantity = $availability_product->GramQuantity - ($product->Quantity * $product->package_weight);
                //             $this->Product_availability_model->update(array('GramQuantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                //         }else{
                //             $quantity = $availability_product->Quantity - $product->Quantity;
                //             $this->Product_availability_model->update(array('Quantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                //         }

                //     }

                // }
            }
            

            if ($this->session->userdata('order_coupon')) {
                $order_coupon = $this->session->userdata('order_coupon');
                $coupon_code = $order_coupon['CouponCode'];
                $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total_amount;
                $total_amount = $total_amount - $coupon_discount_availed;

                // reducing coupon usage count
                $coupon_detail = $this->Coupon_model->getWithMultipleFields(array('CouponID' => $order_coupon['CouponID']), true);
                $update['UsageCount'] = $coupon_detail['UsageCount'] - 1;
                $update_by['CouponID'] = $order_coupon['CouponID'];
                $this->Coupon_model->update($update, $update_by);
            } else {
                $coupon_code = '';
                $coupon_discount_percentage = 0;
                $coupon_discount_availed = 0;
            }

            $ShipmentMethodIDForBooking = $this->session->userdata('ShipmentMethodIDForBooking');
            $shipping_amount = 0;
            $semsa_shipping_amount = 0;
            $freeshipment = false;
            
            $total_tax = 0;
            $taxes = getTaxShipmentCharges('Tax');
            foreach ($taxes as $tax) {
                $tax_id = $tax->TaxShipmentChargesID;
                $tax_title = $tax->Title;
                $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                if ($tax->Type == 'Fixed') {
                    $tax_amount = $tax->Amount;
                } elseif ($tax->Type == 'Percentage') {
                    // $tax_amount = ($tax->Amount / 100) * ($total_amount + $shipping_amount + $semsa_shipping_amount);
                    $tax_amount = ($tax->Amount / 100) * ($total_amount);
                }
                $total_tax += $tax_amount;
                $order_extra_charges['OrderID'] = $insert_id;
                $order_extra_charges['TaxShipmentChargesID'] = $tax_id;
                $order_extra_charges['Title'] = $tax_title;
                $order_extra_charges['Factor'] = $tax_factor;
                $order_extra_charges['Amount'] = $tax_amount;
                $this->Order_extra_charges_model->save($order_extra_charges);
            }
            $get_total =  $total_amount + $total_tax;
            if($order_data['CollectFromStore'] != 1){
                $shipment_method = getSelectedShippingMethodDetail($ShipmentMethodIDForBooking, $this->language);
                if ($shipment_method) {
                    $freeshipment = true;
                    $shipping_id = $shipment_method->TaxShipmentChargesID;
                    $shipping_title = $shipment_method->Title;
                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                    if ($shipment_method->Type == 'Fixed') {
                        $shipping_amount = $shipment_method->Amount;
                    } elseif ($shipment_method->Type == 'Percentage') {
                        $shipping_amount = ($shipment_method->Amount / 100) * ($total_amount);
                    }
                    $apply_free_shipping = false;
                    if ($get_total >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                        $shipping_factor = freeShippingTitle();
                        $shipping_amount = 0.00;
                    }
                    $order_extra_charges['OrderID'] = $insert_id;
                    $order_extra_charges['TaxShipmentChargesID'] = $shipping_id;
                    $order_extra_charges['Title'] = $shipping_title;
                    $order_extra_charges['Factor'] = $shipping_factor;
                    $order_extra_charges['Amount'] = $shipping_amount;
                    $this->Order_extra_charges_model->save($order_extra_charges);
                }
               
            }

            if($freeshipment && $get_total >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                $shipping_amount = 0; 
            }
            $total_amount = $total_amount + $shipping_amount + $total_tax + $semsa_shipping_amount;

            $order_data['CouponCodeUsed'] = $coupon_code;
            $order_data['CouponCodeDiscountPercentage'] = $coupon_discount_percentage;
            $order_data['DiscountAvailed'] = $coupon_discount_availed;
            $order_data['TotalShippingCharges'] = $shipping_amount;
            $order_data['TotalTaxAmount'] = $total_tax;
            $order_data['TotalAmount'] = $total_amount;
            $order_data['OrderNumber'] = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
            $this->Order_item_model->insert_batch($order_item_data);
            $this->Order_model->update($order_data, array('OrderID' => $insert_id));
            // print_rm($order_data['TotalAmount']);
            //semsa
            $awb = "";
            if($order_data['CollectFromStore'] != 1){
             if($this->session->userdata('SemsaShipmentID') && $this->session->userdata('SemsaShipmentID') == 'semsa'){
                    $semsa_shipping_amount = findSemsaShippingCharges();
                    $total_amount =  number_format(($total_amount + $semsa_shipping_amount), 2);
                    $amount = ($online_pay != 1)? $total_amount : 0;
                    
                    $semsa = $this->semsa_shipment(['amount' => $amount],$get_store_data);
                    if($semsa['status'] == 'DATA RECEIVED'){
                        $total_amount = str_replace(",", "", $total_amount);
                        $this_data['TotalAmount'] = $total_amount;
                        $this_data['TotalShippingCharges'] = $semsa_shipping_amount;
                        $this_data['AWBNumber'] = $semsa['awbNumber'];
                        $this_data['ShippedThroughApi'] = 1;
                        $awb = $semsa['awbNumber'];
                        $this_data['SemsaShippingAmount'] = $semsa_shipping_amount;
                        $this->Order_model->update($this_data, array('OrderID' => $insert_id));
                        
                        $this->session->unset_userdata('SemsaShipmentID');
                    }
                }
            }

           
                $deleted_by['UserID'] = $this->UserID;
                $this->Temp_order_model->delete($deleted_by);
                
                // $this->sendOrderConfirmationToCustomer($insert_id, $awb);
           
            $order_data_pusher = $this->Order_model->get($insert_id, true, 'OrderID');
            // pusher($order_data_pusher, "Chocomood_Order_Channel", "Chocomood_Order_Event");
   
            $response['message'] = lang('thank_you_for_order');
            $response['redirect'] = true;
            $response['show_model'] = false;
            $response['insert_id'] = $insert_id;
            
           
            return $response;
           
        } else {
            return false;
            
        }
    }

    public function paymentMethod($id){
        $this->data['id'] = $id;
        $this->data['paytab'] = 'checkout/pay_tab/'.$id;
        $this->data['cod'] = 'checkout/thank_you/'.$id;
        $this->data['view'] = 'frontend/payment_method';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function updatePaymentStatus(){
        // print_rm($_POST);
        if($_POST['respStatus'] == 'A')
        {   
            $checkCustomize = $this->session->userdata('customizeOrder');
            $GET['StoreID'] = $this->session->userdata('StoreIDOrder');
            if($checkCustomize == 1)
            {
                $response = $this->orderEntryCustomize($GET, 1);
            }else
            {
                $response = $this->orderEntry($GET, 1);
            }
            
            
            if($response == false){
                $response['message'] = lang('something_went_wrong');
                $response['redirect'] = false;
                $response['show_model'] = false;
                echo json_encode($response);
                return false;
            }
    
            //if(isset($_REQUEST['transaction_id']) && isset($_REQUEST['order_id'])){
            if(isset($_POST['tranRef']) && isset($response['insert_id'])){
                $this->Order_model->update(array('TransactionID' => $_POST['tranRef'],'Hide' => 0, 'PaymentMethod' => 'PayTab'),array('OrderID' => $response['insert_id']));
                $deleted_by['UserID'] = $this->UserID;
                $this->Temp_order_model->delete($deleted_by);
                //$this->sendOrderConfirmationToCustomer($insert_id);
                $this->sendOrderConfirmationToCustomer($response['insert_id']);
                $order_data_pusher = $this->Order_model->get($response['insert_id'], true, 'OrderID');
                pusher($order_data_pusher, "Choco0mood_Order_Channel", "Chocomood_Order_Event");
               
                $this->session->set_flashdata('message', lang('thank_you_for_order'));
                $this->session->set_flashdata('message_type','info');
                redirect(base_url('checkout/thank_you/'.base64_encode($response['insert_id'])));
    
            }
        }else
        {
            $this->CancelPaymentPaymentOfOrder();
        }
        
    }

    public function semsa_shipment_old($data){
        $result = Smsa::key('Testing1');
        Smsa::nullValues(''); 
        $user = $this->session->userdata('user');
        $fetch_address_by['UserID'] = $this->UserID;
        $fetch_address_by['IsDefault'] = 1;
        $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
        if($this->language == 'EN'){
            $lang_id = 1;
        }else{
            $lang_id = 2;
        }
        $city_data = getCustomRow("Select * from cities_text where CityID = ".$address->CityID." AND SystemLanguageID = ".$lang_id);
        $shipmentData = [
                'refNo' => 'my_app_name' . time(), // shipment reference in your application
                'cName' =>  $user->FullName , // customer name
                'cntry' => 'SA', // shipment country
                'cCity' => $city_data['Title'], // shipment city, try: Smsa::getRTLCities() to get the supported cities
                'cMobile' =>  $user->Mobile, // customer mobile
                'cAddr1'  =>  $address->Street, // customer address
                'cAddr2'  =>  $address->Street, // customer address 2
                'shipType' => 'DLV', // shipment type
                'PCs' => 1, // quantity of the shipped pieces
                'cEmail' => $user->Email, // customer email
                'codAmt' => $data['amount'], // payment amount if it's cash on delivery, 0 if not cash on delivery
                'weight' => findTotalCartWeight(), // pieces weight
                'itemDesc' => 'Thank You for choosing our product.', // extra description will be printed
            ];

        /** @var SmsaSDK\Methods\addShipmentResponse $shipment */
        $shipment = Smsa::addShipment($shipmentData);
        $awbNumber = $shipment->getAddShipmentResult();

        //echo "shipment AWB: " . $awbNumber ;
        //echo "\r\n";

        $status = Smsa::getStatus(['awbNo' => $awbNumber])->getGetStatusResult();
        $data['status'] = $status;
        $data['awbNumber'] = $awbNumber;
        //echo "shipment Status: " . $status;
        //die();
        return $data;
    }

    public function semsa_shipment($data,$store_data){
        $result = Smsa::key('Testing1');
        Smsa::nullValues(''); 
        $user = $this->session->userdata('user');
        $fetch_address_by['UserID'] = $this->UserID;
        $fetch_address_by['IsDefault'] = 1;
        $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
        if($this->language == 'EN'){
            $lang_id = 1;
        }else{
            $lang_id = 2;
        }
        $city_data = getCustomRow("Select * from cities_text where CityID = ".$address->CityID." AND SystemLanguageID = ".$lang_id);
        $store_city_data = getCustomRow("Select * from cities_text where CityID = ".$store_data->CityID." AND SystemLanguageID = ".$lang_id);
        $store_text = getCustomRow("Select * from stores_text where StoreID = ".$store_data->StoreID." AND SystemLanguageID = ".$lang_id);
        $shipmentData = [
                'refNo' => 'my_app_name' . time(), // shipment reference in your application
                'sentDate' => date('Y-m-d H:i:s'),
                'cName' =>  $user->FullName , // customer name
                'cntry' => 'SA', // shipment country
                'cCity' => $city_data['Title'], // shipment city, try: Smsa::getRTLCities() to get the supported cities
                'cMobile' =>  $user->Mobile, // customer mobile
                'cAddr1'  =>  $address->Street, // customer address
                'cAddr2'  =>  $address->Street, // customer address 2
                'shipType' => 'DLV', // shipment type
                'PCs' => 1, // quantity of the shipped pieces
                'cEmail' => $user->Email, // customer email
                'codAmt' => $data['amount'], // payment amount if it's cash on delivery, 0 if not cash on delivery
                'weight' => findTotalCartWeight(), // pieces weight
                'itemDesc' => 'Thank You for choosing our product.', // extra description will be printed
                'sName' => $store_text['Title'], //Store Name
                'sContact' => $store_data->phone,
                'sAddr1' => $store_text['Address'],
                'sCity'  => $store_city_data['Title'],
                'sPhone' => $store_data->phone,
                'sCntry' => 'SA'
            ];

        /** @var SmsaSDK\Methods\addShipmentResponse $shipment */
        $shipment = Smsa::addShip($shipmentData);
        $awbNumber = $shipment->getAddShipResult();

        //echo "shipment AWB: " . $awbNumber ;
        //echo "\r\n";

        $status = Smsa::getStatus(['awbNo' => $awbNumber])->getGetStatusResult();
        $data['status'] = $status;
        $data['awbNumber'] = $awbNumber;
        //echo "shipment Status: " . $status;
        //die();
        return $data;
    }

    public function CancelPaymentPaymentOfOrder(){
       /* if(isset($_REQUEST['order_id'])){

            $this->Order_item_model->delete(array('OrderID' => $_REQUEST['order_id']));
            $this->Order_model->delete(array('OrderID' => $_REQUEST['order_id']));           

        }*/
        $this->session->set_flashdata('message', lang('Transaction_Failed'));
        $this->session->set_flashdata('message_type','danger');
        redirect(base_url('cart'));
    }

    public function sendOrderConfirmationToCustomer($OrderID, $awb = "")
    {
        $user = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID);
        //print_rm($user);
        $order = $this->Order_model->get($OrderID, false, 'OrderID');
        if ($user) {
            // sending email
            if ($user[0]->Email != '') {
                $data['to'] = $user[0]->Email;
                $data['subject'] = ($user[0]->PreferredLang == 0)? 'Order received at chocomood' : 'تم استلام الطلب في chocomood';
                $data['message'] = get_order_invoice($OrderID, 'print');
                //print_rm($data);
               // $data['message'] = get_order_invoice_new($OrderID);
                sendEmail($data);
            }

            // sending sms
            if ($user[0]->Mobile != '') {
                $msg = ($user[0]->PreferredLang == 0)? "Dear " : "العزيز ";
                $tag = ($user[0]->PreferredLang == 0)? "Your order is placed successfully with order" : "تم وضع طلبك بنجاح مع الطلب";
                $msg .=  $user[0]->FullName . ", ".$tag." # " . $order->OrderNumber . "\n";
                if($awb != ""){
                     $msg .=  "Tracking ID # " . $awb . "\n";

                }
                //$msg .= ($user[0]->PreferredLang == 0)? "Invoice" : "فاتورة";
                //$msg .= ": " . base_url() . "page/invoice/" . base64_encode($OrderID);
                sendSms($user[0]->Mobile, $msg);
            }
            $admin = $this->User_model->getBackendUsers('users.StoreID = 1 AND users.RoleID = 4 ');
            if (!empty($admin)) {
               foreach($admin as $v){
                    if ($v->Mobile != '') {
                        $msg = ($v->PreferredLang == 0)? "Dear Admin" : "عزيزي المشرف";
                        $tag = ($v->PreferredLang == 0)? "an order is placed at your " : "يتم وضع الطلب على";
                        $with = ($v->PreferredLang == 0)? " with" : "مع";
                        $msg .=  /*$v->FullName*/ ", ".$tag." ".$v->StoreTitle.$with." # " . $order->OrderNumber . "\n";
                        //$msg .= ($v->PreferredLang == 0)? "Invoice" : "فاتورة";
                        //$msg .= ": " . base_url() . "page/invoice/" . base64_encode($OrderID);
                        sendSms($v->Mobile, $msg);
                    }
               } 
            }
        }
    }

    public function cancelOrder()
    {
        $OrderID = $this->input->post('OrderID');
        $this->Order_model->update(array('Status' => 5), array('OrderID' => $OrderID));
        $this->sendOrderCancellationToAdmin($OrderID);
        $response['message'] = lang('order_cancelled');
        $response['redirect'] = true;
        $response['url'] = 'account/profile?p=orders';
        echo json_encode($response);
        exit();
    }

    private function sendOrderCancellationToAdmin($OrderID)
    {
        $admin_users = $this->User_model->getJoinedData(false, 'UserID', "users.RoleID = 1");
        $order = $this->Order_model->get($OrderID, false, 'OrderID');
        if ($admin_users) {
            foreach ($admin_users as $user) {
                // sending email
                if ($user->Email != '') {
                    $data['to'] = $user->Email;
                    $data['subject'] = 'Order cancelled at chocomood';
                    $data['message'] = email_format("Dear " . $user->FullName . ", an order is cancelled by user with order # " . $order->OrderNumber);
                    sendEmail($data);
                }

                // sending sms
                if ($user->Mobile != '') {
                    $msg = "Dear " . $user->FullName . ", an order is cancelled by user with order # " . $order->OrderNumber;
                    sendSms($user[0]->Mobile, $msg);
                }
            }
        }
    }

    public function getChocoboxDetail()
    {
        $TempOrderID = $this->input->post('id');
        $TempOrder = $this->Order_model->getOrders("orders.OrderID = $TempOrderID");
        $order_items = getOrderItems($TempOrder[0]->OrderID);
        
        $box_id = $this->input->post('box_id');
        $box_type = $this->input->post('box_type');
        $ribbon = $this->input->post('ribbon');
        // var_dump($TempOrder);
        $html = '<div class="row">
                        <h4>' . $box_type . ' Detail</h4>';
        if ($box_type == 'Choco Box') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            foreach ($ProductIDs as $ProductID) {
                $product = $this->Product_model->getProductDetail($ProductID, $this->language);
                $html .= '<div class="col-md-2 chocobox_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . base_url(get_images($product->ProductID, 'product', false)) . '" style="width: 61px;height: 59px;">
                                </div>
                                    <h4>' . $product->Title . '</h4>
                                    <h5><strong>' . number_format($product->Price, 2) . '</strong> ' . lang('sar') . '</h5>';
                if ($product->OutOfStock == 1) {
                    $html .= '<small style="font-weight: bold;color: red;">' . lang('out_of_stock') . '</small>';
                }
                $html .= '</div>
                        </div>';
            }
        } elseif ($box_type == 'Choco Message') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            $html .= "<div class='row'>";
            $productForPrice = array();
            foreach ($ProductIDs as $ProductID) {
                $product = $this->Character_model->getData('CharacterID = ' . $ProductID);
                if (empty($productForPrice)) {
                    array_push($productForPrice, $product[0]);
                } else {
                    $check = 0;
                    foreach ($productForPrice as $v) {
                        if ($v->CharacterID == $ProductID) {
                            $check = 1;
                        }
                    }
                    if ($check == 0) {
                        array_push($productForPrice, $product[0]);
                    }
                }

                $html .= '<div class="col-md-2 chocomsg_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . base_url($product[0]->CharacterImage) . '" style="width: 61px;height: 59px;">
                                </div>';
                $html .= '</div>
                        </div>';
            }
            $html .= "</div>";
        }
        elseif($box_type == 'Choco Shape'){
            $ProductIDs = $this->input->post('ProductIDs');
            $shapeData = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = ".$ProductIDs." AND system_languages.ShortCode = '" . $this->language . "'")[0];
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="col-md-12"><img src="' . base_url($order_items[0]->CustomizedShapeImage) . '" alt="" style="width: 400px;"></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_title') . ': <b>'.$shapeData->Title.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_order_processing_time') . ': <b>'.$shapeData->OrderProcessingTime.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_price_per_pcs') . ': <b>'.$shapeData->PricePerPcs.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_individual_size') . ': <b>'.$shapeData->IndividualSize.'</b></h5></div>';
            $html .= '<div class="col-md-12"><h5>' . lang('shape_Description') . ': <b>'.$shapeData->Description.'</b></h5></div>';
            $html .= '<div class="col-md-12"><h5></b></h5></div>';
            $html .= '<div class="col-md-12"><a href="" target="_blank"></a></div>';
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<table> </table>';
            if($order_items[0]->content_type_id != '')
                    {
                        $content_type = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = " . $order_items[0]->content_type_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                            <div class="col-md-12">
                                <h4>Content Type: </h4>
                                <p>'.$content_type[0]->Title.'</p>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $content_type[0]->Price . '</b>
                            </div>
                           
                        ';
                    }

                    if($order_items[0]->content_type_sub_id != '')
                    {
                        $content_type_sub = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = " . $order_items[0]->content_type_sub_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $content_type_sub_image_id = $order_items[0]->content_type_sub_image_id;
                        
                        $html .= '
                            <div class="col-md-12">
                                <h4>Content Type Sub: </h4>
                                <p>'.$content_type_sub[0]->Title.'</p>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $content_type_sub[0]->Price . '</b>
                            </div>';
                        if($content_type_sub_image_id != '')
                        {
                            $content_type_sub_image = get_images_id($content_type_sub_image_id,'choco_box_printable_upload_image',false);
                            $html .= '<div class="col-md-12">
                                    <img src="' . base_url($content_type_sub_image) . '">
                                </div>
                            ';
                        }
                        
                    }

                    if($order_items[0]->content_text != '')
                    {
                        $html .= '
                            <div class="col-md-12">
                                <h4>Custom Text: </h4>
                                <p>'.$order_items[0]->content_text.'</p>
                            </div>';
                    }
                    
                    if($order_items[0]->Wrapping != '')
                    {
                        $wrappingIDs = $order_items[0]->Wrapping;
                        $wrappingIDs = explode(',', $wrappingIDs);
                        $wrap_count = 0;
                        foreach ($wrappingIDs as $wrappingID) 
                        {
                            $wrap_count++;
                            $wrapping = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID = " . $wrappingID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Wrapping '.$wrap_count.': </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $wrapping[0]->WrappingPrice . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <img src="' . base_url($wrapping[0]->WrappingImage) . '">
                                    </div>
                                ';
                        }
                    }
                    if($order_items[0]->Fillings != '')
                    {
                        $fillingIDs = $order_items[0]->Fillings;
                        $fillingIDs = explode(',', $fillingIDs);
                        $filling_count = 0;
                        foreach ($fillingIDs as $fillingID) 
                        {
                            $filling_count++;
                            $filling = $this->Filling_model->getJoinedData(false, 'FillingId', "fillings.FillingId = " . $fillingID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Filling '.$filling_count.': </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $filling[0]->price . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <img src="' . base_url($filling[0]->image) . '">
                                    </div>
                                ';
                        }
                    }

                    if($order_items[0]->FillColor != '')
                    {
                        $fillColorIDs = $order_items[0]->FillColor;
                        $fillColorIDs = explode(',', $fillColorIDs);
                        $fillColor_count = 0;
                        foreach ($fillColorIDs as $fillColorID) 
                        {
                            $fillColor_count++;
                            $fillColor = $this->Fill_Color_model->getJoinedData(false, 'FillColorId', "fill_colors.FillColorId = " . $fillColorID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Fill Color: </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $fillColor[0]->price . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="color" readonly value="' . $fillColor[0]->code . '">
                                    </div>
                                ';
                        }
                    }

                    if($order_items[0]->PowderColor != '')
                    {
                        $powderColorIDs = $order_items[0]->PowderColor;
                        $powderColorIDs = explode(',', $powderColorIDs);
                        $powderColor_count = 0;
                        foreach ($powderColorIDs as $powderColorID) 
                        {
                            $powderColor_count++;
                            $powderColor = $this->Powder_Color_model->getJoinedData(false, 'PowderColorId', "powder_colors.PowderColorId = " . $powderColorID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Powder Color: </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $powderColor[0]->price . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <img src="' . base_url($powderColor[0]->image) . '">
                                    </div>
                                ';
                        }
                    }
        }
        $html .= '</div>';

        if ($box_type == 'Choco Message') {
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="row"><h4>' . lang('box_packaging_detail') . '</h4>';
            if (isset($box_id) && $box_id > 0) {
                $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $box_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                if ($box) {
                    $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box[0]->Title . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box[0]->BoxPrice . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box[0]->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><img src="' . base_url($box[0]->BoxImage) . '" alt="' . $box[0]->Title . '" style="width: 400px;"></div>';
                    $html .= '<div class="col-md-12"><a href="' . base_url($box[0]->BoxImage) . '" target="_blank">' . lang('enlarge_image') . '</a></div>';
                }
            }
            $html .= '</div><br><br>';
            $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $ribbon . " AND system_languages.ShortCode = '" . $this->language . "'");
            $html .= '<div class="row">
                    <div class="col-md-12">
                        <h4>Ribbon: </h4>
                    </div>
                    <div class="col-md-12">
                        Price: <b>' . $ribbon[0]->RibbonPrice . '</b>
                    </div>
                    <div class="col-md-12">
                        <img src="' . base_url($ribbon[0]->RibbonImage) . '">
                    </div>
                   </div>';
            $html .= '<div class="row">
                    <div class="col-md-12">
                        <h4>Character: </h4>
                    </div>
                    ';
            foreach ($productForPrice as $v) {
                $html .= '  
                        <div class="col-md-12 ">
                            
                                    <img src="' . base_url($v->CharacterImage) . '" style="width: 61px;height: 59px;">
                                    Price: ' . $v->price . '
                </div>';
            }
            $html .= '</div>';
        }
        elseif($box_type == 'Choco Box')
        {
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="row"><h4>' . lang('box_packaging_detail') . '</h4>';
            if (isset($box_id) && $box_id > 0) {
                $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $box_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                if ($box) {
                    $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box[0]->Title . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box[0]->BoxPrice . '</b></h5></div>';
                    if($box[0]->PriceType == 'item')
                    {
                        $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box[0]->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
                    }else
                    {
                        $html .= '<div class="col-md-12"><h5>Weight: <b>' . $box[0]->weight . ' Gram</b></h5></div>';
                    }
                    
                    if($box[0])
                    $html .= '<div class="col-md-12"><img src="' . base_url($box[0]->BoxImage) . '" alt="' . $box[0]->Title . '" style="width: 400px;"></div>';
                    $html .= '<div class="col-md-12"><a href="' . base_url($box[0]->BoxImage) . '" target="_blank">' . lang('enlarge_image') . '</a></div>';
                }
                if($order_items[0]->Ribbon != '0')
                {
                    if($order_items[0]->content_type_id != '')
                    {
                        $content_type = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = " . $order_items[0]->content_type_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                            <div class="col-md-12">
                                <h4>Content Type: </h4>
                                <p>'.$content_type[0]->Title.'</p>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $content_type[0]->Price . '</b>
                            </div>
                           
                        ';
                    }

                    // if($order_items[0]->content_type_id == 3) //only if content type text with images
                    // {
                        if($order_items[0]->content_type_sub_id != '')
                        {
                            $content_type_sub = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = " . $order_items[0]->content_type_sub_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                            
                            
                            $html .= '
                                <div class="col-md-12">
                                    <h4>Content Type Sub: </h4>
                                    <p>'.$content_type_sub[0]->Title.'</p>
                                </div>
                                <div class="col-md-12">
                                    Price: <b>' . $content_type_sub[0]->Price . '</b>
                                </div>';
                        }
                    // }
                    

                    $content_type_sub_image_id = $order_items[0]->content_type_sub_image_id;    
                    if($content_type_sub_image_id != '') //custom uploaded image
                    {
                        $content_type_sub_image = get_images_id($content_type_sub_image_id,'choco_box_printable_upload_image',false);
                        $html .= '<div class="col-md-12">
                                <img src="' . base_url($content_type_sub_image) . '">
                            </div>
                        ';
                    }

                    if($order_items[0]->content_text != '')
                    {
                        $html .= '
                            <div class="col-md-12">
                                <h4>Custom Text: </h4>
                                <p>'.$order_items[0]->content_text.'</p>
                            </div>';
                    }
                    
                    if($ribbon != '')
                    {
                        $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $ribbon . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                                <div class="col-md-12">
                                    <h4>Ribbon: </h4>
                                </div>
                                <div class="col-md-12">
                                    Price: <b>' . $ribbon[0]->RibbonPrice . '</b>
                                </div>
                                <div class="col-md-12">
                                    <img src="' . base_url($ribbon[0]->RibbonImage) . '">
                                </div>
                            ';
                    }
                    
                    if($order_items[0]->Wrapping != '')
                    {
                        $wrapping = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID = " . $order_items[0]->Wrapping . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                                <div class="col-md-12">
                                    <h4>Wrapping: </h4>
                                </div>
                                <div class="col-md-12">
                                    Price: <b>' . $wrapping[0]->WrappingPrice . '</b>
                                </div>
                                <div class="col-md-12">
                                    <img src="' . base_url($wrapping[0]->WrappingImage) . '">
                                </div>
                            ';
                    }
                }
                $html .= '<h4>Total Price</h4>';
                        $html .= '<p>'.$TempOrder[0]->TotalAmount.'</p>';
            }
            $html .= '</div><br><br>';
        }
        elseif($box_type == 'Choco Shape'){

        }
        echo $html;
        exit();
    }

    
}