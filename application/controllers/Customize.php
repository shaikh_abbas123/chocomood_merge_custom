<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customize extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Shape_model');
        $this->load->model('Fill_Color_model');
        $this->load->model('Wrapping_model');
        $this->load->model('Content_type_sub_model');
        $this->load->model('Content_type_model');
        $this->load->model('Powder_Color_model');
        $this->load->model('Filling_model');
        $this->load->model('Box_category_model');
        $this->load->model('Box_model');
        $this->load->model('Home_slider_image_model');
        $this->load->model('Product_model');
        $this->load->model('Contact_request_model');
        $this->load->model('Box_model');
        $this->load->model('Ribbon_model');
        $this->load->model('Customize_model');
        $this->load->model('Character_model');
        $this->load->model('Temp_order_model');
        $this->load->model('Brand_model');
        $this->load->model('Nutrition_shape_model');
        
        
        
    }

    public function index()
    {
        $data['menu'] = 'customize';
        //$data['products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, "products.PriceType = 'kg'");
        $data['customizations'] = $this->Customize_model->getAllJoinedData(false, 'CustomizeID', $this->language, 'customizes.IsActive = 1');
        // print_rm($data['customizations']);
        $data['view'] = 'frontend/customize';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function boxes($type = '')
    {
        if ($type == 'choco_box' || $type == 'choco_message') {
            $data['menu'] = 'customize';
            $data['type'] = $type;
            $data['boxes'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1 AND boxFor='Choco Message'");;
            // $data['ribbons'] = $this->Ribbon_model->getAllJoinedData(false, 'RibbonID', $this->language, "ribbons.IsActive = 1");
            $data['view'] = 'frontend/customize-boxes';
            $this->load->view('frontend/layouts/default', $data);
        } else {
            redirect(base_url('customize'));
        }
    }

    public function mixmax(){
        $data['view'] = 'frontend/customize-mixmax-boxes';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_box($BoxID = '', $RibbonColor = '')
    {
        $BoxID = base64_decode($BoxID);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        
        $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $BoxID . " AND system_languages.ShortCode = '" . $this->language . "'");
        if (!$box) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        $data['box'] = $box[0];
        $all_products = $this->Product_model->getProducts('products.IsCustomizedProduct = 1', $this->language);
        $products = array();
        foreach ($all_products as $product) {
            $BoxIDs = explode(',', $product->BoxIDs);
            if (in_array($BoxID, $BoxIDs)) {
                $products[] = $product;
            }
        }

        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 11 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['RibbonColor'] = base64_decode($RibbonColor);
        $data['products'] = $products;
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-box';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_message($BoxID = '')
    {
        $data['ribbons'] = $this->Ribbon_model->getAllJoinedData(false, 'RibbonID', $this->language, "ribbons.IsActive = 1");
        $BoxID = base64_decode($BoxID);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }

        $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $BoxID . " AND system_languages.ShortCode = '" . $this->language . "'");
        if (!$box) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }

        // $RibbonID = base64_decode($RibbonColor);
        // if (!is_numeric($RibbonID)) {
        //     $this->session->set_flashdata('message', 'Ribbon Not selected');
        //     redirect(base_url('customize'));
        // }
        // $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $RibbonID . " AND system_languages.ShortCode = '" . $this->language . "'");

        // if (!$ribbon) {
        //     $this->session->set_flashdata('message', 'Ribbon Not selected');
        //     redirect(base_url('customize'));
        // }

        $data['characters'] = $this->Character_model->getData("IsActive = 1");
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 10 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        // $data['RibbonColor'] = base64_decode($RibbonColor);
        $data['box'] = $box[0];
        // $data['ribbon'] = $ribbon[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-message';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_message_new_design($BoxID = '')
    {
        $data['ribbons'] = $this->Ribbon_model->getAllJoinedData(false, 'RibbonID', $this->language, "ribbons.IsActive = 1");
        $BoxID = base64_decode($BoxID);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }

        $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $BoxID . " AND system_languages.ShortCode = '" . $this->language . "'");
        if (!$box) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }

        // $RibbonID = base64_decode($RibbonColor);
        // if (!is_numeric($RibbonID)) {
        //     $this->session->set_flashdata('message', 'Ribbon Not selected');
        //     redirect(base_url('customize'));
        // }
        // $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $RibbonID . " AND system_languages.ShortCode = '" . $this->language . "'");

        // if (!$ribbon) {
        //     $this->session->set_flashdata('message', 'Ribbon Not selected');
        //     redirect(base_url('customize'));
        // }

        $data['characters'] = $this->Character_model->getData("IsActive = 1");
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 10 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        // $data['RibbonColor'] = base64_decode($RibbonColor);
        $data['box'] = $box[0];
        // $data['ribbon'] = $ribbon[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-message-new-design';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_shape()
    {
        $sort_by = 'shapes_text.Title';
        $sort_as = 'ASC';
        if (isset($_GET['sort']) && $_GET['sort'] != '') {
            if ($_GET['sort'] == 'price_highest_to_lowest') {
                $sort_by = 'shapes.PricePerKG';
                $sort_as = 'DESC';
            }

            if ($_GET['sort'] == 'price_lowest_to_higher') {
                $sort_by = 'shapes.PricePerKG';
                $sort_as = 'ASC';
            }
        }
        $data['shapes'] = $this->Shape_model->getJoinedData(false, "ShapeID", "shapes.IsActive = 1 AND system_languages.ShortCode = '" . $this->language . "'",$sort_as,$sort_by,true);
        // print_rm($data['shapes']);
        $data['customizations'] = $this->Customize_model->getAllJoinedData(false, 'CustomizeID', $this->language, 'customizes.CustomizeID = 3');
        $data['brands'] = $this->Brand_model->getAllJoinedData(false, 'brand_id', $this->language, 'brands.IsActive = 1');
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-shape';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function getChocoboxDetail()
    {
        $TempOrderID = $this->input->post('id');
        $TempOrder = $this->Temp_order_model->getCartByID($TempOrderID);
        $box_id = $this->input->post('box_id');
        $box_type = $this->input->post('box_type');
        $ribbon = $this->input->post('ribbon');
        // var_dump($TempOrder);
        $html = '<div class="row">
                        <h4>' . $box_type . ' Detail</h4>';
        if ($box_type == 'Choco Box') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            foreach ($ProductIDs as $ProductID) {
                $product = $this->Product_model->getProductDetail($ProductID, $this->language);
                $html .= '<div class="col-md-2 chocobox_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . base_url(get_images($product->ProductID, 'product', false)) . '" style="width: 61px;height: 59px;">
                                </div>
                                    <h4>' . $product->Title . '</h4>
                                    <h5><strong>' . number_format($product->Price, 2) . '</strong> ' . lang('sar') . '</h5>';
                if ($product->OutOfStock == 1) {
                    $html .= '<small style="font-weight: bold;color: red;">' . lang('out_of_stock') . '</small>';
                }
                $html .= '</div>
                        </div>';
            }
        } elseif ($box_type == 'Choco Message') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            $html .= "<div class='row'>";
            $productForPrice = array();
            foreach ($ProductIDs as $ProductID) {
                $product = $this->Character_model->getData('CharacterID = ' . $ProductID);
                if (empty($productForPrice)) {
                    array_push($productForPrice, $product[0]);
                } else {
                    $check = 0;
                    foreach ($productForPrice as $v) {
                        if ($v->CharacterID == $ProductID) {
                            $check = 1;
                        }
                    }
                    if ($check == 0) {
                        array_push($productForPrice, $product[0]);
                    }
                }

                $html .= '<div class="col-md-2 chocomsg_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . base_url($product[0]->CharacterImage) . '" style="width: 61px;height: 59px;">
                                </div>';
                $html .= '</div>
                        </div>';
            }
            $html .= "</div>";
        }
        elseif($box_type == 'Choco Shape'){
            $shapeData = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = $TempOrder->CustomizedOrderProductIDs AND system_languages.ShortCode = '" . $this->language . "'")[0];
            $content_types = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = $TempOrder->content_type_id AND shape_content_type.IsActive = 1 AND shape_content_type.ContentTypeFor = 'Choco Shape' AND system_languages.ShortCode = '" . $this->language . "'")[0];
            if($TempOrder->content_type_id)
            {
                $subcontentType = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.IsActive = 1 AND shape_content_type_sub.ContentTypeSubFor = 'Choco Shape' AND shape_content_type_sub.ContentTypeID = 4 AND system_languages.ShortCode = '" . $this->language . "'")[0];    
            }
            
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="col-md-12"><img src="' . base_url($TempOrder->CustomizedShapeImage) . '" alt="" style="width: 400px;"></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_title') . ': <b>'.$shapeData->Title.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_order_processing_time') . ': <b>'.$shapeData->OrderProcessingTime.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_price_per_kg') . ': <b>'.$shapeData->PricePerKG.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_individual_size') . ': <b>'.$shapeData->IndividualSize.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>Content-Type:<b>'.$content_types->Title.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>Price:<b>'.$content_types->Price.'</b></h5></div>';
            if(isset($subcontentType))
            {
                $html .= '<div class="col-md-6"><h5>Content-Type:<b>'.$subcontentType->Title.'</b></h5></div>';
                $html .= '<div class="col-md-6"><h5>Price:<b>'.$subcontentType->Price.'</b></h5></div>';
            }
            $html .= '<div class="col-md-12"><h5>' . lang('shape_Description') . ': <b>'.$shapeData->Description.'</b></h5></div>';
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<table> </table>';
        }
        $html .= '</div>';

        if ($box_type == 'Choco Message') {
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="row"><h4>' . lang('box_packaging_detail') . '</h4>';
            if (isset($box_id) && $box_id > 0) {
                $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $box_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                if ($box) {
                    $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box[0]->Title . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box[0]->BoxPrice . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box[0]->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><img src="' . base_url($box[0]->BoxImage) . '" alt="' . $box[0]->Title . '" style="width: 400px;"></div>';
                    $html .= '<div class="col-md-12"><a href="' . base_url($box[0]->BoxImage) . '" target="_blank">' . lang('enlarge_image') . '</a></div>';
                }
            }
            $html .= '</div><br><br>';
            $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $ribbon . " AND system_languages.ShortCode = '" . $this->language . "'");
            $html .= '<div class="row">
                    <div class="col-md-12">
                        <h4>Ribbon: </h4>
                    </div>
                    <div class="col-md-12">
                        Price: <b>' . $ribbon[0]->RibbonPrice . '</b>
                    </div>
                    <div class="col-md-12">
                        <img src="' . base_url($ribbon[0]->RibbonImage) . '">
                    </div>
                   </div>';
            $html .= '<div class="row">
                    <div class="col-md-12">
                        <h4>Character: </h4>
                    </div>
                    ';
            foreach ($productForPrice as $v) {
                $html .= '  
                        <div class="col-md-12 ">
                            
                                    <img src="' . base_url($v->CharacterImage) . '" style="width: 61px;height: 59px;">
                                    Price: ' . $v->price . '
                </div>';
            }
            $html .= '</div>';
        }
        elseif($box_type == 'Choco Box')
        {
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="row"><h4>' . lang('box_packaging_detail') . '</h4>';
            if (isset($box_id) && $box_id > 0) {
                $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $box_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                if ($box) {
                    $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box[0]->Title . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box[0]->BoxPrice . '</b></h5></div>';
                    if($box[0]->PriceType == 'item')
                    {
                        $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box[0]->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
                    }else
                    {
                        $html .= '<div class="col-md-12"><h5>Weight: <b>' . $box[0]->weight . ' Gram</b></h5></div>';
                    }
                    
                    if($box[0])
                    $html .= '<div class="col-md-12"><img src="' . base_url($box[0]->BoxImage) . '" alt="' . $box[0]->Title . '" style="width: 400px;"></div>';
                    $html .= '<div class="col-md-12"><a href="' . base_url($box[0]->BoxImage) . '" target="_blank">' . lang('enlarge_image') . '</a></div>';
                }
                if($TempOrder->BoxPrintable == '1')
                {
                    if($TempOrder->content_type_id != '')
                    {
                        $content_type = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = " . $TempOrder->content_type_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                            <div class="col-md-12">
                                <h4>Content Type: </h4>
                                <p>'.$content_type[0]->Title.'</p>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $content_type[0]->Price . '</b>
                            </div>
                           
                        ';
                    }

                    if($TempOrder->content_type_id == 3)
                    {
                        if($TempOrder->content_type_sub_id != '')
                        {
                            $content_type_sub = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = " . $TempOrder->content_type_sub_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $content_type_sub_image_id = $TempOrder->content_type_sub_image_id;
                            
                            $html .= '
                                <div class="col-md-12">
                                    <h4>Content Type Sub: </h4>
                                    <p>'.$content_type_sub[0]->Title.'</p>
                                </div>
                                <div class="col-md-12">
                                    Price: <b>' . $content_type_sub[0]->Price . '</b>
                                </div>';
                            if($content_type_sub_image_id != '')
                            {
                                $content_type_sub_image = get_images_id($content_type_sub_image_id,'content_type_sub_image',false);
                                $html .= '<div class="col-md-12">
                                        <img src="' . base_url($content_type_sub_image) . '">
                                    </div>
                                ';
                            }
                            
                        }
                    }
                    

                    if($TempOrder->content_text != '')
                    {
                        $html .= '
                            <div class="col-md-12">
                                <h4>Custom Text: </h4>
                                <p>'.$TempOrder->content_text.'</p>
                            </div>';
                    }
                    
                    $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $ribbon . " AND system_languages.ShortCode = '" . $this->language . "'");
                    $html .= '
                            <div class="col-md-12">
                                <h4>Ribbon: </h4>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $ribbon[0]->RibbonPrice . '</b>
                            </div>
                            <div class="col-md-12">
                                <img src="' . base_url($ribbon[0]->RibbonImage) . '">
                            </div>
                        ';
                    $wrapping = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID = " . $TempOrder->Wrapping . " AND system_languages.ShortCode = '" . $this->language . "'");
                    $html .= '
                            <div class="col-md-12">
                                <h4>Wrapping: </h4>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $wrapping[0]->WrappingPrice . '</b>
                            </div>
                            <div class="col-md-12">
                                <img width="100px" height="100px" src="' . base_url($wrapping[0]->WrappingImage) . '">
                            </div>
                        ';
                        $html .= '<h4>Total Price</h4>';
                        $html .= '<p>'.$TempOrder->TempItemPrice.'</p>';
                }
            }
            $html .= '</div><br><br>';
        }
        elseif($box_type == 'Choco Shape'){

        }
        echo $html;
        exit();
    }

    public function getChocoboxes()
    {
        $html = '<h4>' . lang('box_packaging_detail') . '</h4>';
        $html .= '<p>' . lang('select_box_to_continue') . '</p><br>';
        $type = $this->input->post('type');
        $boxes = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1");
        foreach ($boxes as $box) {
            $html .= '<div class="row">';
            $html .= '<div class="col-md-4">
                                    <div class="row">';
            $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box->Title . '</b></h5></div>';
            $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box->BoxPrice . '</b></h5></div>';
            $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
            $html .= '</div>
                        </div>';
            $html .= '<a title="' . lang('click_to_proceed_with_box') . '" href="' . base_url('customize') . '/' . $type . '/' . base64_encode($box->BoxID) . '"><div class="col-md-8"><img src="' . base_url($box->BoxImage) . '" alt="' . $box->Title . '" style="width: 400px;"></div></a>';
            $html .= '</div>';
            $html .= '<hr>';
        }
        echo $html;
        exit();
    }

    public function box_type()
    {
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/customize/box-type';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function box_list()
    {
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/customize/box-list';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function box_detail()
    {
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/customize/box-detail';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function box_category()
    {
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/customize/box-category';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function choco_shape_details($id)
    {
        $shapeId = base64_decode($id);
        $data['shapeDetail'] = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = $shapeId AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['product_nutritions'] = $this->Nutrition_shape_model->shapeNutrition($shapeId, $this->language);
        // print_rm($data['product_nutritions']);
        $data['shape_images'] = get_images($shapeId, 'shape');
        $data['featured_products'] = $this->Shape_model->getJoinedData(false, "ShapeID", "shapes.IsActive = 1 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['available_cities'] = $this->Product_model->getCities('stores.isCustomize = 1 AND stores.IsActive=1 ' , $this->language,1);
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-shape-details';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function choco_shape_engraved($id)
    {
        $shapeId = base64_decode($id);
        $data['customizations'] = $this->Customize_model->getAllJoinedData(false, 'CustomizeID', $this->language, 'customizes.CustomizeID = 3');
        $data['shapeDetail'] = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = $shapeId AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['shapeDetail_en'] = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = $shapeId AND system_languages.ShortCode = 'en'")[0];
        $data['fillColor'] = $this->Fill_Color_model->getJoinedData(false, 'FillColorId', "fill_colors.IsActive = 1 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['fillings'] = $this->Filling_model->getJoinedData(false, 'FillingId', "fillings.IsActive = 1 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['wrappings'] = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.IsActive = 1 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['wrappings_en'] = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.IsActive = 1 AND system_languages.ShortCode = 'en'");
        $data['powder_colors'] = $this->Powder_Color_model->getJoinedData(false, 'PowderColorId', "powder_colors.IsActive = 1 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['content_types'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.IsActive = 1 AND shape_content_type.ContentTypeFor = 'Choco Shape' AND system_languages.ShortCode = '" . $this->language . "'");
        $data['text_types'] = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.IsActive = 1 AND shape_content_type_sub.ContentTypeSubFor = 'Choco Shape' AND shape_content_type_sub.ContentTypeID = 4 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['customize_text_types'] = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.IsActive = 1 AND shape_content_type_sub.ContentTypeSubFor = 'Choco Shape' AND shape_content_type_sub.ContentTypeID = 5 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-shape-engraved';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function choco_shape_order_preview()
    {
        $contentType = $this->session->userdata('content-type');
        if ($this->session->userdata('content-type') == null) {
            // $this->session->set_flashdata('message', lang('a_shape_must_be_selected'));
            redirect(base_url('customize/choco_shape'));
        }
        $formdata = [];
        $formdata['contentTypeId'] = $contentType;
        if ($contentType == 4) {
            $formdata['customerContent'] = $data['customerContent'] =  $this->session->userdata('customer-content');
            $formdata['textTypeId'] = $textType =  $this->session->userdata('textType');
            $data['subcontentType'] = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = $textType AND system_languages.ShortCode = '" . $this->language . "'")[0];
            $data['contentType'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = $contentType AND system_languages.ShortCode = '" . $this->language . "'")[0];
            

        } else if ($contentType == 5) {
            $formdata['customerContent'] = $data['customerContent'] =  $this->session->userdata('customer-content');
            $drawing =  $this->session->userdata('drawing');
            $drawingExplode = explode(',', $drawing);
            $formdata['textTypeId'] = $drawingExplode[0];
            $formdata['subcontentTypeImageId'] = $drawingExplode[1];
            $data['subcontentType'] = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = $drawingExplode[0] AND system_languages.ShortCode = '" . $this->language . "'")[0];
            // var_dump($formdata['textTypeId']);
            $data['contentType'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = $contentType AND system_languages.ShortCode = '" . $this->language . "'")[0];
            
        } else if ($contentType == 6) {
            $formdata['shapecustomImageId'] = $this->session->userdata('imageFile');
            $data['imageFile'] = get_images_id($formdata['shapecustomImageId'],'choco_shape_upload_image',false);
            $data['contentType'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = $contentType AND system_languages.ShortCode = '" . $this->language . "'")[0];
        }
        $formdata['fill_color'] = $fill_color =  $this->session->userdata('fill_color');
        $data['fillColor'] = $this->Fill_Color_model->getJoinedData(false, 'FillColorId', "fill_colors.FillColorId = $fill_color AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $formdata['filling'] = $fillings = $this->session->userdata('fillings');
        $data['fillingsList'] = $this->Filling_model->getJoinedData(false, 'FillingId', "fillings.FillingId in ($fillings) AND system_languages.ShortCode = '" . $this->language . "'");
        $formdata['wrapping'] = $wrappings = $this->session->userdata('wrappings');
        
        if($wrappings)
        {
            $data['wrappingList'] = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID in ($wrappings) AND system_languages.ShortCode = '" . $this->language . "'");
        }
        
        $formdata['powderColor'] = $powderColor = $this->session->userdata('powderColor');
        $data['powderColor'] = $this->Powder_Color_model->getJoinedData(false, 'PowderColorId', "powder_colors.PowderColorId = $powderColor AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $formdata['quantity'] = $data['Quantity'] = $this->session->userdata('Quantity');
        $formdata['shapeId'] = $shapeId = $this->session->userdata('shapeID');
        $data['shapeDetail'] = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = $shapeId AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['shapeDetail_en'] = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = $shapeId AND system_languages.ShortCode = 'en'")[0];
        $data['formdata'] = $formdata;
        
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco-shape-order-preview';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function customize_box()
    {
        $this->session->unset_userdata(['printable','contentType','contentTypeSub','contentTypeSubImage','customer-content','ribbon','wrapping','customImage']);
        $data['Boxes'] = $this->Box_category_model->getAllJoinedData(false, 'BoxCategoryID', $this->language, "box_categories.IsActive = 1 and box_categories.parentID = 0 AND box_categories.BoxCategoryFor= 'Choco Box'");
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco_box/unprintable/customize_box_category';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function choose_box_category($id = '')
    {
        $BoxCategoryId = base64_decode($id);
        // if (!is_numeric($BoxCategoryId)) {
        //     $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
        //     redirect(base_url('customize'));
        // }
        $data['BoxCatID'] = $id;
        // var_dump($BoxCategoryId);
        $data['Boxes'] = $this->Box_category_model->getAllJoinedData(false, 'BoxCategoryID', $this->language, "box_categories.IsActive = 1 and box_categories.parentID != '0'");
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['result']->MetaTags = 'Choco Box';
        $data['view'] = 'frontend/choco_box/unprintable/choose_box_sub_category';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choose_box($id = '', $catId = '')
    {
        $BoxCategoryId = base64_decode($id);
        if (!is_numeric($BoxCategoryId)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        $data['BoxCatID'] = $catId;
        $data['BoxCategory'] = $this->Box_category_model->getAllJoinedData(false, 'BoxCategoryID', $this->language, "box_categories.IsActive = 1 and box_categories.BoxCategoryID = $BoxCategoryId");
        // var_dump($data['BoxCategory']);
        $data['Boxes'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1 and boxes.BoxCategory = $BoxCategoryId");
        // var_dump($data['Boxes']);
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 12 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco_box/unprintable/choose_box';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function start_customize_choco_box($id = '', $catId)
    {
        $BoxID = base64_decode($id);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        $parent_cat_id = $this->Box_category_model->getAllJoinedData(false, 'BoxCategoryID', $this->language, "box_categories.IsActive = 1 and box_categories.BoxCategoryID = '".base64_decode($catId)."'")[0]->ParentID;
        $data['BoxCategoryId'] = $parent_cat_id;
        $data['BoxCategory'] = $this->Box_category_model->getAllJoinedData(false, 'BoxCategoryID', $this->language, "box_categories.IsActive = 1 and box_categories.BoxCategoryID = ".base64_decode($catId));
        $data['box'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1 and boxes.BoxID = $BoxID");
        $product_ids = explode(',', $data['box'][0]->BoxInside);
        $inside_products = array();
        foreach ($product_ids as $v) {
            $product_data = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, "products.ProductID = '" . $v . "'");
            if ($product_data) {
                array_push($inside_products, $product_data);
            }
        }
        $data['inside_products'] = $inside_products;
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco_box/unprintable/start_customise';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_box_gram($id = '')
    {
        $BoxID = base64_decode($id);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        if($this->session->userdata('printable'))
        {
            $data['printable'] = $this->session->userdata();
        }
        $data['box'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1 and boxes.BoxID = $BoxID");
        $product_ids = explode(',', $data['box'][0]->BoxInside);
        $inside_products = array();
        foreach ($product_ids as $v) {
            $product_data = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, "products.ProductID = '" . $v . "'");
            if ($product_data) {
                array_push($inside_products, $product_data);
            }
        }
        $data['inside_products'] = $inside_products;
        $data['view'] = 'frontend/choco_box/unprintable/choco_box_gram';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function choco_box_item($id = '')
    {
        $BoxID = base64_decode($id);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        if($this->session->userdata('printable'))
        {
            $data['printable'] = $this->session->userdata();
        }
        $data['box'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1 and boxes.BoxID = $BoxID");
        $product_ids = explode(',', $data['box'][0]->BoxInside);
        $inside_products = array();
        foreach ($product_ids as $v) {
            $product_data = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, "products.ProductID = '" . $v . "'");
            if ($product_data) {
                array_push($inside_products, $product_data);
            }
        }
        $data['inside_products'] = $inside_products;
        $data['view'] = 'frontend/choco_box/unprintable/choco_box_item';
        $this->load->view('frontend/layouts/default', $data);
    }

    ///Printtable Box
    public function customize_printable_box($id = '')
    {
        $BoxID = base64_decode($id);
        if (!is_numeric($BoxID)) {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
        // var_dump($BoxID);
        $data['box'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, "boxes.IsActive = 1 and boxes.BoxID = $BoxID");
        $data['wrappings'] = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.IsActive = 1 AND system_languages.ShortCode = '" . $this->language . "'");
        $data['ribbons'] = $this->Ribbon_model->getAllJoinedData(false, 'RibbonID', $this->language, "ribbons.IsActive = 1");
        $data['content_types'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.IsActive = 1 AND shape_content_type.ContentTypeFor = 'Choco Box' AND system_languages.ShortCode = '" . $this->language . "'");
        $data['text_with_image'] = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.IsActive = 1 AND shape_content_type_sub.ContentTypeSubFor = 'Choco Box' AND shape_content_type_sub.ContentTypeID = 2 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['customize_text_types'] = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.IsActive = 1 AND shape_content_type_sub.ContentTypeSubFor = 'Choco Box' AND shape_content_type_sub.ContentTypeID = 3 AND system_languages.ShortCode = '" . $this->language . "'");
        // var_dump($data['text_with_image']);
        $data['menu'] = 'customize';
        $data['view'] = 'frontend/choco_box/printable/customize_printable_box';
        $this->load->view('frontend/layouts/default', $data);
    }
    public function customize_printable_box_post(){
        $post_data = $this->input->post();
        $wrappings = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID = ".$post_data['wrapping']." AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $ribbons = $this->Ribbon_model->getAllJoinedData(false, 'RibbonID', $this->language, "ribbons.RibbonID = ".$post_data['ribbon'])[0];
        $printableBox['printable'] =1;
        $printableBox['ribbon_price'] = $ribbons->RibbonPrice;
        $printableBox['wrapping_price'] = $wrappings->WrappingPrice;
        $printableBox['content_type_sub_price'] = null;
        if($post_data['contentType'] == 1)
        {
            $printableBox['content_type_price'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = ".$post_data['contentType']." AND system_languages.ShortCode = '" . $this->language . "'")[0]->Price;
            $printableBox['contentType'] = $post_data['contentType'];
            $printableBox['ribbon'] = $post_data['ribbon'];
            $printableBox['wrapping'] = $post_data['wrapping'];
            $this->session->set_userdata($printableBox);
            if($post_data['PriceType'] == 'kg')
            {
                redirect(base_url('customize/choco_box_gram/'.base64_encode($post_data['boxID'])));
            }
            else
            {
                redirect(base_url('customize/choco_box_item/'.base64_encode($post_data['boxID'])));
            }
        }
        else if($post_data['contentType'] == 2 || $post_data['contentType'] == 3)
        {
            $printableBox['content_type_price'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = ".$post_data['contentType']." AND system_languages.ShortCode = '" . $this->language . "'")[0]->Price;
            
            $subtypeExplode = explode(',',$post_data['textTemplate']);
            $printableBox['contentType'] = $post_data['contentType'];
            $printableBox['contentTypeSub'] = $subtypeExplode[0];
            $printableBox['content_type_sub_price'] = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = ".$subtypeExplode[0]." AND system_languages.ShortCode = '" . $this->language . "'")[0]->Price;
            $printableBox['contentTypeSubImage'] = $subtypeExplode[1];
            $printableBox['customertext'] = $post_data['customer-content'];
            $printableBox['ribbon'] = $post_data['ribbon'];
            $printableBox['wrapping'] = $post_data['wrapping'];
            $this->session->set_userdata($printableBox);
            if($post_data['PriceType'] == 'kg')
            {
                redirect(base_url('customize/choco_box_gram/'.base64_encode($post_data['boxID'])));
            }
            else
            {
                redirect(base_url('customize/choco_box_item/'.base64_encode($post_data['boxID'])));
            }
        }
        else if($post_data['contentType'] == 8)
        {
            $printableBox['contentType'] = $post_data['contentType'];
            $printableBox['content_type_price'] = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = ".$post_data['contentType']." AND system_languages.ShortCode = '" . $this->language . "'")[0]->Price;
            $printableBox['ribbon'] = $post_data['ribbon'];
            if (isset($_FILES['customer-content']["name"]) && $_FILES['customer-content']["name"] != '') 
            {    
                $printableBox['customImage'] =  $this->uploadImageSingle('customer-content', 'uploads/choco_box_printable_upload_image/',0,'choco_box_printable_upload_image');
            }
            $printableBox['wrapping'] = $post_data['wrapping'];
            $this->session->set_userdata($printableBox);
            if($post_data['PriceType'] == 'kg')
            {
                redirect(base_url('customize/choco_box_gram/'.base64_encode($post_data['boxID'])));
            }
            else
            {
                redirect(base_url('customize/choco_box_item/'.base64_encode($post_data['boxID'])));
            }
        }
        else
        {
            $this->session->set_flashdata('message', lang('a_box_must_be_selected'));
            redirect(base_url('customize'));
        }
    }
}
