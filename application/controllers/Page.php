<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Home_slider_image_model');
        $this->load->model('Product_model');
        $this->load->model('Collection_model');
        $this->load->model('Contact_request_model');
        $this->load->model('Offer_user_notification_model');
        $this->load->model('Customize_model');
        $this->load->model('Order_model');
        $this->load->model('Faq_model');
        $this->data['language'] = $this->language;
    }

    public function index()
    {
        $this->home();
    }

    public function home()
    {
        $data['marquee'] = true;
        $data['menu'] = 'home';
        $data['slider_images'] = $this->Home_slider_image_model->getAllJoinedData(false, 'HomeSliderImageID', $this->language);
        $data['featured_products'] = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, 'products.IsFeatured = 1 AND products.IsCustomizedProduct = 0');
        $data['collections_featured'] = $this->Collection_model->getAllJoinedData(false, 'CollectionID', $this->language, 'collections.IsActive = 1');
       // $data['collections'] = $this->Collection_model->getAllJoinedData(false, 'CollectionID', $this->language);
        $data['customizations'] = $this->Customize_model->getAllJoinedData(false, 'CustomizeID', $this->language,'customizes.IsActive = 1');
        $data['language'] = $this->language;
        $data['view'] = 'frontend/index';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function about_us()
    {
        $data['menu'] = 'about-us';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 1 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['view'] = 'frontend/about';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function return_policy()
    {
        $data['menu'] = 'return_policy';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 13 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['view'] = 'frontend/return_policy';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function privacy_policy()
    {
        $data['menu'] = 'privacy_policy';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 15 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['view'] = 'frontend/privacy_policy';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function delivery_policy()
    {
        $data['menu'] = 'delivery_policy';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 16 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['view'] = 'frontend/delivery_policy';
        $this->load->view('frontend/layouts/default', $data);
    }

     public function site_map()
    {
        $data['menu'] = 'site-map';
        
        $data['view'] = 'frontend/site_map';
        $this->load->view('frontend/layouts/default', $data);
    }

     public function faq()
    {
        $data['menu'] = 'faq';
        
        $data['faqs'] = $this->Faq_model->getAllJoinedData(false,'FaqID',$this->language,'faqs.IsActive = 1');
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 14 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        
        $data['view'] = 'frontend/faq';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function contact_us()
    {
        $data['menu'] = 'contact-us';
        $data['result'] = $this->Page_model->getJoinedData(false, 'PageID', "pages.PageID = 6 AND system_languages.ShortCode = '" . $this->language . "'")[0];
        $data['view'] = 'frontend/contact';
        $this->load->view('frontend/layouts/default', $data);
    }

    public function saveContactForm()
    {
        $post_data = $this->input->post();
        $siteKey = $post_data["g-recaptcha-response"];
        unset($post_data["g-recaptcha-response"]);
        $res = captchaVerify($siteKey);
        if ($res->success != true) {
            $response['message'] = lang('must_use_captcha');
            $response['reset'] = false;
            echo json_encode($response);
            exit;
        } else {
            if (isset($_FILES['CV']["name"]) && $_FILES['CV']["name"] != '') {

                // checking file extension
                $allowed = array('pdf', 'doc', 'docx', 'PDF', 'DOC', 'DOCX');
                $filename = $_FILES['CV']['name'];
                $filename = strtolower($filename);
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $response['reset'] = false;
                    $response['message'] = lang("only_pdf_doc_file_format");
                    echo json_encode($response);
                    exit();
                }

                $post_data['CV'] = uploadImage("CV", "uploads/");
            }
            $post_data['CreatedAt'] = time();
            $insert_id = $this->Contact_request_model->save($post_data);
            if ($insert_id > 0) {
                $response['reset'] = true;
                $response['message'] = lang('feed_back_submitted_submitted_successfully');
                echo json_encode($response);
                exit();
            } else {
                $response['reset'] = false;
                $response['message'] = lang('some_thing_went_wrong');//"Something went wrong. Please try again.";
                echo json_encode($response);
                exit();
            }
        }
    }

    public function newsletter_subscribe()
    {
        $Email = $this->input->post('Email'); // Email
        $subscribed = newsletter_subscribe($Email);
        if ($subscribed['http_code'] == 200) {
            $response['reset'] = true;
            $response['message'] = lang('thank_you_subscribe_our_news_letter');
            echo json_encode($response);
            exit();
        } else {
            $response['reset'] = false;
            $response['message'] = lang("you_have_enter_invalid_email");
            echo json_encode($response);
            exit();
        }
    }

    public function search()
    {

        $search_value = $this->input->get('key');
        $data['products'] = $this->Product_model->searchProducts($search_value, $this->language);
        $data['search'] = true;
        $data['view'] = 'frontend/products';
        $this->load->view('frontend/layouts/default', $data);


    }

    public function markOfferAsRead()
    {
        $update_by['OfferID'] = $_GET['OfferID'];
        $update_by['UserID'] = $this->session->userdata['user']->UserID;
        $update['IsShow'] = 1;
        $this->Offer_user_notification_model->update($update, $update_by);
        $success['status'] = true;
        $success['message'] = 'Offer marked as read successfully';
        echo json_encode($success);
        exit;
    }

    /*public function invoice($OrderID)
    {
        $OrderID = base64_decode($OrderID);
        $order_html = get_order_invoice($OrderID, 'print');
        echo $order_html;
        exit();
    }*/


   public function invoice($OrderID)
    {
        checkFrontendSession();
        $OrderID = base64_decode($OrderID);
        $CI = &get_Instance();
        $CI->load->model('Order_model');
        $CI->load->model('Order_item_model');
        $CI->load->model('User_address_model');
        
        $orderInfo      = $CI->Order_model->getOrders("orders.OrderID = $OrderID");
        $orderDetails   = $CI->Order_item_model->getOrdersItems($OrderID);
        // return $order_details;
        // print_r($orderInfo[0]->isCustomize);
        $itemDetailStr = array();
        foreach ($orderDetails as $orderDetail ) {
            $detailStr      = "";
            $productTile    = "";
            if($orderDetail->ProductID==0){
                $itemDetail   = $CI->Order_item_model->getSubItem($orderDetail->OrderItemID)[0];
                $subItems = array_count_values(explode(",",$itemDetail->CustomizedOrderProductIDs));
                $productTile = "<b>".$itemDetail->ItemType."</b>";
                $detailStr = "<ul style='list-style-type: none;'>";
                foreach ($subItems as $itemId => $count) {
                    $subItemDetial = $CI->Order_item_model->getSubItemDetial($itemId, $itemDetail->ItemType)[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($subItemDetial->CharacterImage)."' style='width:35px;height:auto' /> <span>".$count."x </span> </li>";
                }
                $itemDetail   = $CI->Order_item_model->getSubItem($orderDetail->OrderItemID)[0];
                if($orderDetail->Ribbon != "" && $orderDetail->Ribbon != "0"){
                    $sideItem = $CI->Order_item_model->getSideItemDetail($orderDetail->Ribbon, "ribbons", "RibbonID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->RibbonImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->CustomizedBoxID != "" && $orderDetail->CustomizedBoxID != "0"){
                    $sideItem = $CI->Order_item_model->getSideItemDetail($orderDetail->CustomizedBoxID, "boxes", "BoxID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'>Box Name: ".$sideItem->Title." <span>1x</span> </li>";
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->BoxImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->Wrapping != "" && $orderDetail->Wrapping != "0"){
                    $sideItem = $CI->Order_item_model->getSideItemDetail($orderDetail->Wrapping, "wrappings", "WrappingID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->WrappingImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->content_type_id != "" && $orderDetail->content_type_id != "0"){
                    if($orderDetail->content_text!=""){
                        $sideItem   = $CI->Order_item_model->getContentTypeWithTextnImage($orderDetail->content_type_id, "shape_content_type_text", "ContentTypeID", $this->language)[0];
                        $detailStr .= "<li style='display: flex;align-items:center;'> Text Title:".$sideItem->Title." <br/> Text: ".$orderDetail->content_text." </li>";
                    }
                    if($orderDetail->content_type_sub_id != "" && $orderDetail->content_type_sub_id != "0"){
                        $sideItem   = $CI->Order_item_model->getContentSubTypeWithImage($orderDetail->content_type_sub_id, $orderDetail->content_type_sub_image_id, $this->language);
                        if(count($sideItem)){
                            $detailStr .= "<li style='display: flex;align-items:center;'> <div>Title: ".$sideItem[0]->Title."</div> <div><img src='".base_url($sideItem[0]->ImageName)."' style='width:35px;height:auto' /> </div></li>";
                        }
                    }
                }
                $detailStr .= "</ul>";
                $itemDetailStr["id_".$orderDetail->OrderItemID]["title"]        = $productTile;
                $itemDetailStr["id_".$orderDetail->OrderItemID]["description"]  = $detailStr;
            }
        }

        $order_details = $this->Order_model->getOrders("orders.OrderID = $OrderID");
        $data['order'] = $order_details[0];
        $data['language'] = $this->language;
        $data['barcode']  = generateBarcode($OrderID, "barcode", $type = 'image');
        //print_rm($order_details);
        //$order_html = $this->load->view('frontend/invoice_pdf', $data, true);
        //generate_pdf($order_html);
        $order_html = $this->load->view('frontend/invoice', $data, true);
        echo $order_html;
        exit();
    }

    public function download($OrderID)
    {
        $OrderID = base64_decode($OrderID);
        $order_html = get_order_invoice($OrderID);
        generate_pdf($order_html, $OrderID);
    }

}