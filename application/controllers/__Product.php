<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Category_model');
        $this->load->model('Collection_model');
        $this->load->model('Home_slider_image_model');
        $this->load->model('Product_model');
        $this->load->model('Contact_request_model');
        $this->load->model('Product_rating_model');
        $this->load->model('Product_review_model');
        $this->load->model('Search_tag_model');
        $this->load->model('Offer_model');
        $this->data['language'] = $this->language;
    }



    public function category($title){
        $category = explode('-',$title);
        $category_id = str_replace('c','',end($category));
        $this->data['CategoryID'] = $category_id;
        $this->data['categories'] = subCategories($category_id,$this->language);
        $this->data['view'] = 'frontend/category';
        $this->load->view('frontend/layouts/default', $this->data);
    }
    public function listing(){
        $this->data['view'] = 'frontend/list-product';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function index($collection = '')
    {
        $this->data['menu'] = 'product';
        $sort_by = 'products_text.Title';
        $sort_as = 'ASC';
        if (isset($_GET['sort']) && $_GET['sort'] != '')
        {
            if ($_GET['sort'] == 'price_highest_to_lowest')
            {
                $sort_by = 'products.Price';
                $sort_as = 'DESC';
            }

            if ($_GET['sort'] == 'price_lowest_to_higher')
            {
                $sort_by = 'products.Price';
                $sort_as = 'ASC';
            }

            if ($_GET['sort'] == 'product_older_to_newer')
            {
                $sort_by = 'products.ProductID';
                $sort_as = 'ASC';
            }

            if ($_GET['sort'] == 'product_newer_to_older ')
            {
                $sort_by = 'products.ProductID';
                $sort_as = 'DESC';
            }

            if ($_GET['sort'] == 'purchased_highest_to_lowest')
            {
                $sort_by = 'products.PurchaseCount';
                $sort_as = 'DESC';
            }

            if ($_GET['sort'] == 'purchased_lowest_to_higher')
            {
                $sort_by = 'products.PurchaseCount';
                $sort_as = 'ASC';
            }
        }
        if (isset($_GET['q']) && $_GET['q'] != '') {
            $this->data['products'] = $this->getFilteredProducts($_GET['q'], $sort_by, $sort_as);
            $this->data['countproducts'] = count($this->data['products']);
        } else {
            // $limit = 12;
            $limit = false;

            if ($collection != '') {

                $this->data['collection_data'] = $this->Collection_model->get($collection, false, 'CollectionID');
                if (!$this->data['collection_data']) {
                    redirect(base_url('product'));
                }
                $this->data['products'] = array();//$this->Product_model->getProductsOfCollection($this->data['collection_data']->ProductID,$this->language,$limit,0);
                // $this->data['countproducts']  = $this->Product_model->getProductsOfCollection($this->data['collection_data']->ProductID,$this->language,false,0,true);


                $this->data['countproducts'] = 0;//$this->data['countproducts'][0]->Total;
                $this->data['CollectionID'] = $this->data['collection_data']->CollectionID;
            } else {
                $this->data['products'] = $this->Product_model->getProducts('products.IsCustomizedProduct = 0', $this->language, $limit, 0, $sort_by, $sort_as);

                $this->data['countproducts'] = $this->Product_model->getCountProducts('products.IsCustomizedProduct = 0', $this->language, false, 0);
                $this->data['countproducts'] = $this->data['countproducts']->Total;

            }
        }


        $this->data['collections'] = $this->Collection_model->getAllJoinedData(false, 'CollectionID', $this->language);
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language, 'categories.ParentID = 0 AND categories.IsActive = 1');
        $this->data['offers'] = $this->Offer_model->getAllJoinedData(false, 'OfferID', $this->language, 'offers.IsActive = 1 AND DATE(ValidTo) > "' . Date('Y-m-d') . '" AND IsForAll = 1');
        $this->data['offers_for_you'] = array();
        if ($this->session->userdata('user')) {
            //print_rm($this->session->userdata('user'));
            $this->data['offers_for_you'] = $this->Offer_model->getOfferForUser($this->session->userdata['user']->UserID);
        }
        //print_rm($this->data['offers_for_you']);

        $this->data['view'] = 'frontend/products';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    private function getFilteredProducts($q, $sort_by, $sort_as)
    {
        $filtered_products = array();
        $url_sub_categories = explode(' ', $q);
        if (count($url_sub_categories) > 0) {
            $category_ids = array();
            $collection_ids = array();
            $products_by_categories = array();
            $products_by_collections = array();
            foreach ($url_sub_categories as $url_sub_category) {
                $sub_str = substr($url_sub_category, strrpos($url_sub_category, '-') + 1);
                if (strpos($sub_str, 's') !== false) {
                    $category_ids[] = getNumberFromString($sub_str);
                } elseif (strpos($sub_str, 'c') !== false) {
                    $collection_ids[] = getNumberFromString($sub_str);
                }
            }
            if (count($category_ids) > 0) {
                $products_by_categories = $this->Product_model->getProductsWhereIn($category_ids, $this->language, false, 0, $sort_by, $sort_as);
            }
            if (count($collection_ids) > 0) {
                $products_by_collections = $this->Collection_model->getCollectionProducts($collection_ids);
                if ($products_by_collections && count($products_by_collections) > 0) {
                    $collection_products_arr = array();
                    foreach ($products_by_collections as $product) {
                        $product_ids = explode(',', $product->ProductID);
                        $collection_products = $this->Product_model->getProductsOfCollection($product_ids, $this->language, false, 0, false, $sort_by, $sort_as);
                        foreach ($collection_products as $collection_product) {
                            $collection_products_arr[] = $collection_product;
                        }
                    }

                }
                $products_by_collections = $collection_products_arr;
            }
            $filtered_products = array_merge($products_by_categories, $products_by_collections);
            $filtered_products = array_unique($filtered_products, SORT_REGULAR);
        }
        return $filtered_products;
    }

    public function getAllProducts($page, $limit, $start, $ul_cls)
    {
        $products = $this->Product_model->getProducts('products.IsCustomizedProduct = 0', $this->language, $limit, $start);
        $countproducts = $this->Product_model->getCountProducts('products.IsCustomizedProduct = 0', $this->language, false, $start);
        $total_product_db = $countproducts->Total;
        $total_product = 0;
        $html = '';
        if (!empty($products)) {
            $html .= product_html($products, $ul_cls);
            $total_product = COUNT($products) * $page;
        }
        $response['html'] = $html;
        $response['page'] = $page;
        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;
        if ($total_product <= $total_product_db && COUNT($products) != 0) {
            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();
    }

    /*public function getMoreProducts()
    {

        $collection = $this->input->post('CollectionID');
        $offer = $this->input->post('OfferID');
        $collection_categories = $this->input->post('CollectionSubCategories');
        $offer_categories = $this->input->post('OfferSubCategories');
        $sub_categories = $this->input->post('SubCategories');


        $page = $this->input->post('Page');
        $limit = 12;
        $start = $limit * $page;
        $products = array();
        $products1 = array();
        $products2 = array();
        $countproducts_first = 0;
        $total_product_db = 0;
        //print_rm($sub_categories);
        if (empty($offer) && empty($collection) && empty($collection_categories) && empty($sub_categories) && empty($offer_categories)) {
            $this->getAllProducts($page, $limit, $start);
        }


        $where = '1 = 1';
        $where1 = '1 = 1';
        $product_ids = array();


        if (!empty($offer)) {

            $offer_products = $this->Offer_model->getOfferProducts($offer);


            if ($offer_products) {

                foreach ($offer_products as $product) {
                    $product_id_offer = explode(',', $product->ProductID);

                    foreach ($product_id_offer as $key => $value) {
                        if (!in_array($value, $product_ids)) {
                            $product_ids[] = $value;
                        }

                    }
                }

            }


            $a = implode(',', $product_ids);
            $where1 .= " AND products.ProductID IN ($a)";

            $b = implode(',', $offer_categories);
            $where1 .= " AND products.SubCategoryID IN ($b)";

            $products = $this->Product_model->getProducts($where1, $this->language, $limit, $start);
            $countproducts = $this->Product_model->getCountProducts($where1, $this->language, false, $start);
            $total_product_db = $countproducts->Total;

        }

        $product_ids = array();

        if (!empty($collection)) {
            $collection_products = $this->Collection_model->getCollectionProducts($collection);

            if ($collection_products) {

                foreach ($collection_products as $product) {
                    $product_id = explode(',', $product->ProductID);

                    foreach ($product_id as $key => $value) {
                        if (!in_array($value, $product_ids)) {
                            $product_ids[] = $value;
                        }

                    }
                }

            }


            $a = implode(',', $product_ids);
            $where .= " AND products.ProductID IN ($a)";

            $b = implode(',', $collection_categories);
            $where .= " AND products.SubCategoryID IN ($b)";

            $products2 = $this->Product_model->getProducts($where, $this->language, $limit, $start);
            $countproducts2 = $this->Product_model->getCountProducts($where, $this->language, false, $start);
            $total_product_db = $countproducts2->Total;
            if (!empty($products2)) {
                $products = array_merge($products, $products2);
                //$countproducts = $total_product_db = $countproducts_first + $total_product_db;
            }

        }


        $html = '';


        if (!empty($sub_categories)) {
            $where2 = '1 = 1';
            $c = implode(',', $sub_categories);
            $where2 .= " AND products.SubCategoryID IN ($c)";
            if (!empty($collection)) {
                $a = implode(',', $product_ids);
                $where2 .= " AND products.ProductID NOT IN ($a)";
            }

            $products3 = $this->Product_model->getProducts($where2, $this->language, $limit, $start);
            $countproducts3 = $this->Product_model->getCountProducts($where2, $this->language, false, $start);
            $total_product_db = $total_product_db + $countproducts3->Total;

            if (!empty($products3)) {
                $products = array_merge($products, $products3);
                //$countproducts = $total_product_db = $countproducts_first + $total_product_db;
            }

        }

        $total_product = 0;

        if (!empty($products)) {
            $type_of_item = "'Product'";

            $html .= '<div class="row">';

            foreach ($products as $key => $p) {


                $html .= '<div class="col-md-3 single_product">';
                $html .= '<div class="inbox">';
                $html .= '<div class="imgbox">';
                $html .= '<img src="' . base_url(get_images($p->ProductID, 'product', false)) . '">';
                $html .= '</div>';
                $html .= '<a href="' . base_url() . 'product/detail/' . productTitle($p->ProductID) . '">';
                $html .= '<h4>' . $p->Title . '</h4>';

                $html .= '<h5><strong data-price="' . number_format($p->Price, 2) . '" data-pid="'.$p->ProductID.'">' . number_format($p->Price, 2) . '</strong> SAR</h5>';
                if ($p->OutOfStock == 1) {
                    $html .= '<small style="font-weight: bold;color: red;">'.lang('out_of_stock').'</small>';
                }
                $html .= '</a>';
                $html .= '<a title="'.lang('click_to_add_to_wishlist').'" href="javascript:void(0);"
                                   onclick="addToWishlist(' . $p->ProductID . ', ' . $type_of_item . ');"><i
                                            class="fa fa-heart ' . isLiked($p->ProductID, 'product') . '" id="item' . $p->ProductID . '" aria-hidden="true"></i></a>';
                $html .= '<a href="javascript:void(0);" title="'.lang('click_to_add_to_cart').'"
                                   onclick="addWishlistToCart(' . $p->ProductID . ', ' . $type_of_item . ', '.$p->Price.');">
                                    <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                </a>';
                $html .= '</div></div>';
            }
            $html .= '</div>';

            $total_product = COUNT($products) * $page;

        }


        $response['html'] = $html;
        $response['page'] = $page;


        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;

        if ($total_product <= $total_product_db && COUNT($products) != 0) {

            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();


    }*/

    public function getMoreProducts()
    {

        $collection = $this->input->post('CollectionID');
        $offer = $this->input->post('OfferID');
        $collection_categories = $this->input->post('CollectionSubCategories');
        $offer_categories = $this->input->post('OfferSubCategories');
        $sub_categories = $this->input->post('SubCategories');


        $page = $this->input->post('Page');
        $ul_cls = $this->input->post('ul_cls');

        if (isset($ul_cls) && ($ul_cls == 'items_grid' || $ul_cls == 'items_list'))
        {
            $ul_cls1 = $ul_cls;
        } else {
            $ul_cls1 = 'items_grid';
        }

        $limit = 12;
        $start = $limit * $page;
        $my_products_arr = array();
        $products = array();
        $products1 = array();
        $products2 = array();
        $countproducts_first = 0;
        $total_product_db = 0;
        $html = '';
        $total_product = 0;
        //print_rm($sub_categories);
        if (empty($offer) && empty($collection) && empty($collection_categories) && empty($sub_categories) && empty($offer_categories)) {
            $this->getAllProducts($page, $limit, $start, $ul_cls1);
        }


        $where = '1 = 1 AND products.IsCustomizedProduct = 0 ';
        $where1 = '1 = 1 AND products.IsCustomizedProduct = 0 ';
        $product_ids = array();


        if (!empty($offer)) {

            $offer_products = $this->Offer_model->getOfferProducts($offer);

            if ($offer_products) {

                foreach ($offer_products as $product) {
                    $product_id_offer = explode(',', $product->ProductID);

                    foreach ($product_id_offer as $key => $value) {
                        if (!in_array($value, $product_ids)) {
                            $product_ids[] = $value;
                        }

                    }
                }

            }


            $a = implode(',', $product_ids);
            $where1 .= " AND products.ProductID IN ($a)";

            $b = implode(',', $offer_categories);
            $where1 .= " AND products.SubCategoryID IN ($b)";

            $products = $this->Product_model->getProducts($where1, $this->language, $limit, $start);
            $countproducts = $this->Product_model->getCountProducts($where1, $this->language, false, $start);
            $total_product_db = $countproducts->Total;

            if (!empty($products)) {
                $CurrentDate = date('Y-m-d');
                $type_of_item = "'Product'";

                $html .= '<div class="row"><div class="col-md-12"><ul class="'.$ul_cls1.'">';
                $html .= '<h5>Offered Products</h5>';

                foreach ($products as $key => $p) {

                    // Bilal work here on 05-04-2019 for offered prices
                    $IsOnOffer = false;
                    $url_q = "";
                    $DiscountDescription = 'Click to view product detail';
                    if ($offer_products) {
                        foreach ($offer_products as $offer_product) {
                            $OfferID = $offer_product->OfferID;
                            $ProductIDsOffer = explode(',', $offer_product->ProductID);
                            if (in_array($p->ProductID, $ProductIDsOffer)) {
                                $OfferValidFrom = $offer_product->ValidFrom;
                                $OfferValidTo = $offer_product->ValidTo;
                                if ($CurrentDate >= $OfferValidFrom && $CurrentDate <= $OfferValidTo) {
                                    $IsOnOffer = true;
                                    $url_q = "/" . base64_encode($OfferID);
                                    $DiscountDescription = $offer_product->Title . ": " . $offer_product->Description;
                                    $DiscountType = $offer_product->DiscountType;
                                    $DiscountFactor = $offer_product->Discount;
                                    if ($DiscountType == 'percentage') {
                                        $Discount = ($DiscountFactor / 100) * $p->Price;
                                        if ($Discount > $p->Price) {
                                            $ProductDiscountedPrice = 0;
                                        } else {
                                            $ProductDiscountedPrice = $p->Price - $Discount;
                                        }
                                    } elseif ($DiscountType == 'per item') {
                                        $Discount = $DiscountFactor;
                                        if ($Discount > $p->Price) {
                                            $ProductDiscountedPrice = 0;
                                        } else {
                                            $ProductDiscountedPrice = $p->Price - $Discount;
                                        }
                                    } else {
                                        $Discount = 0;
                                        if ($Discount > $p->Price) {
                                            $ProductDiscountedPrice = 0;
                                        } else {
                                            $ProductDiscountedPrice = $p->Price;
                                        }
                                    }
                                }
                            }
                        }

                    }

                    if ($p->IsCorporateProduct == 1 && $p->CorporateMinQuantity > 0) {
                        $IsCorporateItem = 1;
                    } else {
                        $IsCorporateItem = 0;
                    }


                    $html .= '<li class="single_product">';
                    $html .= '<div class="inbox">';
                    $html .= '<div class="imgbox">';
                    $html .= '<img src="' . base_url(get_images($p->ProductID, 'product', false)) . '">';
                    $html .= '</div>';
                    $html .= '<a href="' . base_url() . 'product/detail/' . productTitle($p->ProductID) . $url_q . '" title="' . $DiscountDescription . '">';
                    $html .= '<h4>' . $p->Title . '</h4>';
                    if ($IsOnOffer) {
                        $html .= '<h5><small style="text-decoration: line-through;">' . number_format($p->Price, 2) . ' SAR</small></h5>';
                        $html .= '<h5><strong data-price="' . number_format($ProductDiscountedPrice, 2) . '" data-pid="'.$p->ProductID.'" class="offered_product" title="' . $DiscountDescription . '">' . number_format($ProductDiscountedPrice, 2) . ' SAR</strong></h5>';
                        $ProductPriceForCart = "'$ProductDiscountedPrice'";
                    } else {
                        $html .= '<h5><strong data-price="' . number_format($p->Price, 2) . '" data-pid="'.$p->ProductID.'">' . number_format($p->Price, 2) . '</strong> SAR</h5>';
                        $ProductPriceForCart = "'$p->Price'";
                    }
                    if ($p->OutOfStock == 1) {
                        $html .= '<small style="font-weight: bold;color: red;">' . lang('out_of_stock') . '</small>';
                    }
                    $html .= '</a>';
                    $html .= '<a title="' . lang('click_to_add_to_wishlist') . '" href="javascript:void(0);"
                                   onclick="addToWishlist(' . $p->ProductID . ', ' . $type_of_item . ');"><i
                                            class="fa fa-heart ' . isLiked($p->ProductID, 'product') . '" id="item' . $p->ProductID . '" aria-hidden="true"></i></a>';
                    $html .= '<a href="javascript:void(0);" title="' . lang('click_to_add_to_cart') . '"
                                   onclick="addWishlistToCart(' . $p->ProductID . ', ' . $type_of_item . ', ' . $ProductPriceForCart . ', ' . $IsCorporateItem . ');">
                                    <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                </a>';
                    $html .= '</div></li>';
                }
                $html .= '</ul></div></div>';

                $total_product = COUNT($products) * $page;

            }


        }

        $product_ids = array();

        if (!empty($collection)) {
            $collection_products = $this->Collection_model->getCollectionProducts($collection);

            if ($collection_products) {

                foreach ($collection_products as $product) {
                    $product_id = explode(',', $product->ProductID);

                    foreach ($product_id as $key => $value) {
                        if (!in_array($value, $product_ids)) {
                            $product_ids[] = $value;
                        }

                    }
                }

            }


            $a = implode(',', $product_ids);
            $where .= " AND products.ProductID IN ($a)";

            $b = implode(',', $collection_categories);
            $where .= " AND products.SubCategoryID IN ($b)";

            $products2 = $this->Product_model->getProducts($where, $this->language, $limit, $start);
            $countproducts2 = $this->Product_model->getCountProducts($where, $this->language, false, $start);
            $total_product_db = $countproducts2->Total;
            if (!empty($products2)) {
                // $products = array_merge($products, $products2);
                //$countproducts = $total_product_db = $countproducts_first + $total_product_db;
                $my_products_arr = array_merge($my_products_arr, $products2);

            }

        }


        if (!empty($sub_categories)) {
            $where2 = '1 = 1 AND products.IsCustomizedProduct = 0 ';
            $c = implode(',', $sub_categories);
            $where2 .= " AND products.SubCategoryID IN ($c)";
            if (!empty($collection)) {
                $a = implode(',', $product_ids);
                $where2 .= " AND products.ProductID NOT IN ($a)";
            }

            $products3 = $this->Product_model->getProducts($where2, $this->language, $limit, $start);
            $countproducts3 = $this->Product_model->getCountProducts($where2, $this->language, false, $start);
            $total_product_db = $total_product_db + $countproducts3->Total;

            if (!empty($products3)) {
                // $products = array_merge($products, $products3);
                //$countproducts = $total_product_db = $countproducts_first + $total_product_db;
                $my_products_arr = array_merge($my_products_arr, $products3);


            }

        }

        if (!empty($my_products_arr)) {
            $html .= product_html($my_products_arr, $ul_cls1);
            $total_product = COUNT($my_products_arr) * $page;
        }


        $response['html'] = $html;
        $response['page'] = $page;


        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;

        if ($total_product <= $total_product_db && COUNT($products) != 0) {

            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();


    }


    public function getMoreProductsSearch()
    {
        $sub_categories = $this->input->post('SubCategories');
        $search_key = $this->input->post('SearchValue');
        $page = $this->input->post('Page');

        // saving searched tag in db
        $this->Search_tag_model->save(array('SearchTag' => $search_key, 'UserID' => $this->UserID, 'SearchedAt' => date('Y-m-d H:i:s')));

        $limit = 12;
        $start = $limit * $page;
        $products = array();
        $countproducts_first = 0;
        $total_product_db = 0;
        //print_rm($sub_categories);
        if (empty($sub_categories)) {
            // $this->getAllProducts($page,$limit,$start);
        }


        $where = "1 = 1  AND products.IsCustomizedProduct = 0 AND products_text.Title LIKE '%$search_key%'";


        $html = '';


        if (!empty($sub_categories)) {

            $c = implode(',', $sub_categories);
            $where .= " AND products.SubCategoryID IN ($c)";


        }

        $products = $this->Product_model->getProducts($where, $this->language, $limit, $start);
        $countproducts = $this->Product_model->getCountProducts($where, $this->language, false, $start);
        $total_product_db = $total_product_db + $countproducts->Total;

        $total_product = 0;
        $response['no_result'] = false;
        if (!empty($products)) {
            $html .= product_html($products);
            $total_product = COUNT($products) * $page;
        } else {
            $response['no_result'] = true;
        }


        $response['html'] = $html;
        $response['page'] = $page;


        $response['count_product'] = $total_product_db;
        $response['total_now'] = $total_product;

        if ($total_product <= $total_product_db && COUNT($products) != 0) {

            $response['page'] = $page + 1;
        }
        echo json_encode($response);
        exit();


    }

    public function detail($product_title, $offer_id = "")
    {
        
        $this->data['menu'] = 'product';
        $product_id = productID($product_title);
        
        $this->data['product'] = $this->Product_model->getJoinedData(false, 'ProductID', "products.ProductID = " . $product_id . " AND system_languages.ShortCode = '" . $this->language . "'");
       
        if (empty($this->data['product'])) {
            redirect(base_url('product'));
        }
        $this->data['result'] = $this->data['product'][0];

        // print_rm($this->data['result']);
        $where = '';
        //print_rm($this->data['result']);
        if ($this->session->userdata('user')) {
            $this->data['rating'] = $this->Product_rating_model->getWithMultipleFields(array('ProductID' => $product_id, 'UserID' => $this->session->userdata['user']->UserID));
        } else {
            $this->data['rating'] = false;
        }
        if ($this->session->userdata('user')) {
            $this->data['review'] = $this->Product_review_model->getWithMultipleFields(array('ProductID' => $product_id, 'UserID' => $this->session->userdata['user']->UserID));
            $where = 'product_reviews.UserID != ' . $this->session->userdata['user']->UserID . ' AND ';
        } else {
            $this->data['review'] = false;

        }
        $where .= "$where product_reviews.ProductID = " . $product_id;
        $this->data['all_reviews'] = $this->Product_review_model->getJoinedDataWithOtherTable(false, 'UserID', 'users_text', $where);

        $this->data['product'] = $this->data['product'][0];
        $this->data['product_images'] = get_images($product_id, 'product');
        $this->data['offer_id'] = $offer_id;

        $this->data['view'] = 'frontend/single-product';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    /*public function giveRating()
    {
        $post_data = $this->input->post(); // ProductID, UserID, Rating
        $alreadyRated = $this->Product_rating_model->getWithMultipleFields(array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
        if ($alreadyRated) {
            $response['status'] = false;
            $response['message'] = lang('you_have_already_rate_this_product');
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Product_rating_model->save($post_data);
            if ($inserted_id > 0) {
                $response['status'] = true;
                $response['message'] = lang('thank_you_for_rating');//"Thank you for rating this product.";
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('some_thing_went_wrong');//"Something went wrong while rating this product.";
                echo json_encode($response);
                exit();
            }
        }
    }*/

    public function giveRating()
    {
        $post_data = $this->input->post(); // ProductID, UserID, Rating
        $alreadyRated = $this->Product_rating_model->getWithMultipleFields(array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
        if ($alreadyRated) {
            $this->Product_rating_model->update(array('Rating' => $post_data['Rating']), array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
            $response['status'] = true;
            $response['message'] = lang('updated_rating');
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Product_rating_model->save($post_data);
            if ($inserted_id > 0) {
                $response['status'] = true;
                $response['message'] = lang('thank_you_for_rating');//"Thank you for rating this product.";
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('some_thing_went_wrong');//"Something went wrong while rating this product.";
                echo json_encode($response);
                exit();
            }
        }
    }

    public function saveReview()
    {
        $post_data = $this->input->post(); // Title, Comment, ProductID
        $post_data['UserID'] = $this->session->userdata['user']->UserID;
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $alreadyReviewed = $this->Product_review_model->getWithMultipleFields(array('ProductID' => $post_data['ProductID'], 'UserID' => $post_data['UserID']));
        if ($alreadyReviewed) {
            $response['message'] = lang('you_have_already_given_review');
            echo json_encode($response);
            exit();
        } else {
            $inserted_id = $this->Product_review_model->save($post_data);
            if ($inserted_id > 0) {
                $response['reset'] = true;
                $response['reload'] = true;
                $response['message'] = lang('giving_your_review');
                echo json_encode($response);
                exit();
            } else {
                $response['message'] = lang('some_thing_went_wrong');
                echo json_encode($response);
                exit();
            }
        }
    }


}
