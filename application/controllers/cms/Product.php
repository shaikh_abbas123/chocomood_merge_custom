<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'Category_model',
            'Nutrition_model',
            'Nutrition_product_model',
            'Packages_product_model',
            'Box_model',
            'What_inside_model',
            'Product_availability_model',
            'Store_model',
            'Tag_model',
            'Site_images_model',
            'Packages_model',
            'Brand_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'ProductID';
        $this->data['Table'] = 'products';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language);

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language, 'categories.ParentID = 0');
        $this->data['nutritions'] = $this->Nutrition_model->getAllJoinedData(false, 'NutritionID', $this->language);
        $this->data['brands'] = $this->Brand_model->getAllJoinedData(false, 'brand_id', $this->language);
        $this->data['packages'] = $this->Packages_model->getAllJoinedData(false, 'PackagesID', $this->language);
        $this->data['boxes'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, 'boxes.IsActive = 1');

        $this->data['tags'] = $this->Tag_model->getAllJoinedData(false, 'TagID', $this->language, 'tags.IsActive = 1');

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['categories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language, 'categories.ParentID = 0');
        $this->data['nutritions'] = $this->Nutrition_model->getAllJoinedData(false, 'NutritionID', $this->language);
        $this->data['packages'] = $this->Packages_model->getAllJoinedData(false, 'PackagesID', $this->language);
        $this->data['brands'] = $this->Brand_model->getAllJoinedData(false, 'brand_id', $this->language);
        $this->data['boxes'] = $this->Box_model->getAllJoinedData(false, 'BoxID', $this->language, 'boxes.IsActive = 1');

        $this->data['product_nutritions'] = $this->Nutrition_product_model->productNutrition($id, $this->language);
        $this->data['product_packages'] = $this->Packages_product_model->productPackages($id, $this->language);
        $this->data['product_nutritions_result'] = array();
        $this->data['product_nutrition_field'] = '';
        if (!empty($this->data['product_nutritions'])) {
            $this->data['product_nutritions_result'] = array_column($this->data['product_nutritions'], 'NutritionID');
            $this->data['product_nutritions_quanity'] = array_column($this->data['product_nutritions'], 'Quantity', 'NutritionID');


            foreach ($this->data['product_nutritions'] as $value) {
                $this->data['product_nutrition_field'] .= '<div class="col-md-6" id="sl_chocolate_text">
                            <div class="form-group label-floating">
                                <label class="control-label">&nbsp;</label>
                                <input type="text" class="form-control" name="sl_text_' . $value['Title'] . '" value="' . $value['Title'] . '">
                            </div>
                        </div>
                        
                        <div class="col-md-6" id="sl_chocolate_qty">
                            <div class="form-group label-floating">
                                <label class="control-label">Write Quantity</label>
                                <input type="text" class="form-control" name="NutritionQuantity[' . $value['NutritionID'] . ']" value="' . $value['Quantity'] . '">
                            </div>
                        </div><div class="clearfix"></div>';
            }
        }

        $this->data['ProductHtml'] = '';
        $this->data['InsideCount'] = 0;

        if($this->data['result'][0]->PriceType == 'item'){
             $product_inside = $this->What_inside_model->getMultipleRows(array('ProductID' => $id));

             if($product_inside){
                foreach ($product_inside as $key => $inside) {
                     $this->data['InsideCount'] =  $this->data['InsideCount'] + 1;
                    $this->data['ProductHtml'] .= '<div class="row" style="margin-bottom:10px;" id="'.$inside->ProductInsideID.'">
                    <div class="col-md-1">
                    <input type="hidden" name="old['.$key.']" value="' . $inside->ProductInsideID . '">
                            <i class="fa fa-times delete_inside" style="margin-top:30px;cursor:pointer;" data-inside-id="' . $inside->ProductInsideID . '" aria-hidden="true"></i>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Title Eng</label>
                                <input type="text" class="form-control" name="WhatsInsideTitle['.$key.']" value="'.$inside->InsideTitle.'">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group label-floating">
                                <label class="control-label">Title Arb</label>
                                <input type="text" class="form-control" name="WhatsInsideTitleAr['.$key.']" value="'.$inside->InsideTitleAr.'">
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-3"><img src="' . base_url() . $inside->InsideImage . '" style="height:100px;width:100px;"></div>
                    </div>';
                }
             }

        }


        $this->data['subcategories'] = $this->Category_model->getAllJoinedData(false, 'CategoryID', $this->language, 'categories.ParentID IN ('.$this->data['result'][0]->CategoryID.')');

         $this->data['tags'] = $this->Tag_model->getAllJoinedData(false, 'TagID', $this->language, 'tags.IsActive = 1');
        


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    public function productAvailability($product_id){

        $products_availability = $this->Product_availability_model->getMultipleRows(array('ProductID' => $product_id),true);
        $this->data['stores'] = $this->Store_model->getAllJoinedData(true, 'StoreID', $this->language, 'stores.IsActive = 1');
        $parent = $this->data['Parent_model'];
        $product_type = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $product_id, 'DESC', '');
        $this->data['product_type'] = @$product_type[0]->PriceType;
        
        $this->data['products_availability']  = array();
        if($products_availability){
            if( $this->data['product_type'] == "kg"){
                $this->data['products_availability'] = array_column($products_availability, 'GramQuantity','StoreID');
            }else{
                $this->data['products_availability'] = array_column($products_availability, 'Quantity','StoreID');
            }
        }

        

        $this->data['ProductID'] = $product_id;
        $html = $this->load->view('backend/product/availability',$this->data,true);
        $reponse['html'] = $html;
        echo json_encode($reponse);
        exit;

    }


    public function saveProductAvailability(){
        $this->Product_availability_model->delete(array('ProductID' => $this->input->post('ProductID')));
        $store_id = $this->input->post('StoreID');
        $quantity = $this->input->post('Quantity');
        $GramQuantity = $this->input->post('GramQuantity');
        if(!empty($store_id)){
            $save_availability = array();
            $save_availability['ProductID'] = $this->input->post('ProductID');
            foreach ($store_id as $key => $value) {
                $save_availability['StoreID'] = $value;
                $save_availability['Quantity'] = isset($quantity[$key])? $quantity[$key] : 0;
                $save_availability['GramQuantity'] = isset($GramQuantity[$key])? $GramQuantity[$key] : 0;
                $this->Product_availability_model->save($save_availability);
            }


        }

        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        $success['reload'] = true;
        //$success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
        echo json_encode($success);
        exit;


    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {

            foreach ($_FILES['Image']["tmp_name"] as $key => $tmp_name) {

                $image_info = getimagesize($tmp_name);
                $image_width = $image_info[0];
                $image_height = $image_info[1];

                if($image_width != $image_height){
                    $errors['error'] =  'All images aspect ration should be same.';
                    $errors['success'] = false;
                    echo json_encode($errors);
                    exit;
                }
            }

        }




        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        if ((isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] == '') || COUNT($_FILES['Image']['name']) > 7) {
            $errors['error'] = lang('please_choose_image');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;

        }

        if (isset($post_data['IsCustomizedProduct']) && empty($post_data['BoxIDs'])) {
            $errors['error'] = "Please select boxes for customization";
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } elseif (isset($post_data['IsCustomizedProduct']) && !empty($post_data['BoxIDs'])) {
            $save_parent_data['BoxIDs'] = implode(',', $post_data['BoxIDs']);
        }

        if (isset($post_data['IsCorporateProduct']) && $post_data['CorporateMinQuantity'] == '') {
            $errors['error'] = "Please insert minimum quantity";
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } elseif (isset($post_data['IsCorporateProduct']) && $post_data['CorporateMinQuantity'] !== '') {
            $save_parent_data['CorporateMinQuantity'] = $post_data['CorporateMinQuantity'];
            $save_parent_data['CorporatePrice'] = $post_data['CorporatePrice'];
        }

        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['brand_id'] = $post_data['brand_id'];
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        $save_parent_data['IsFeatured'] = (isset($post_data['IsFeatured']) ? 1 : 0);
        $save_parent_data['OutOfStock'] = (isset($post_data['OutOfStock']) ? 1 : 0);
        $save_parent_data['IsCustomizedProduct'] = (isset($post_data['IsCustomizedProduct']) ? 1 : 0);
        $save_parent_data['IsCorporateProduct'] = (isset($post_data['IsCorporateProduct']) ? 1 : 0);
        $save_parent_data['SKU'] = $post_data['SKU'];
        $save_parent_data['Price'] = $post_data['Price'];
        $save_parent_data['PriceType'] = $post_data['PriceType'];
        $save_parent_data['Weight'] = $post_data['Weight'];
        $save_parent_data['MinimumOrderQuantity'] = $post_data['MinimumOrderQuantity'];
        $save_parent_data['CategoryID'] = implode(',',$post_data['CategoryID']);
        $save_parent_data['SubCategoryID'] = implode(',',$post_data['SubCategoryID']);
        $save_parent_data['TagIDs'] = implode(',',$post_data['TagIDs']);

        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {
            if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                $this->uploadImage('Image', 'uploads/images/', $insert_id, 'product', true);
            }

            if($post_data['PriceType'] == 'item'){
                $inside_title = $this->input->post('WhatsInsideTitle');
                $inside_title_ar = $this->input->post('WhatsInsideTitleAr');
                $inside_images = $this->input->post('WhatsInsideTitle');
                if(!empty($inside_title)){
                    foreach ($inside_title as $key => $inside) {
                        if($inside != ''){
                            $save_inside_data = array();
                            $save_inside_data['ProductID'] = $insert_id;
                            $save_inside_data['InsideTitle'] = $inside;
                            $save_inside_data['InsideTitleAr'] = $inside_title_ar[$key];
                            if (isset($_FILES['WhatsInsideImage'.$key]["name"][0]) && $_FILES['WhatsInsideImage'.$key]["name"][0] != '') {
                               $save_inside_data['InsideImage'] =  $this->uploadImage('WhatsInsideImage'.$key, 'uploads/images/');
                            }

                            $this->What_inside_model->save($save_inside_data);

                        }
                        
                        
                    }
                }
            }

            // $default_lang = getDefaultLanguage();
            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {


                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];
                $save_child_data['Ingredients'] = $post_data['Ingredients'];
                $save_child_data['Specifications'] = $post_data['Specifications'];
                //$save_child_data['Tags'] = $post_data['Tags'];
                $save_child_data['Keywords'] = $post_data['Keywords'];


                $save_child_data['MetaTags'] = $post_data['MetaTags'];
                $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                $save_child_data['MetaDescription'] = $post_data['MetaDescription'];


                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
            }

            if (!empty($post_data['NutritionQuantity']) && isset($post_data['NutritionQuantity'])) {
                $nutrition_data = array();
                foreach ($post_data['NutritionQuantity'] as $key => $value) {
                    if ($value != '') {
                        $nutrition_data[] = [
                            'ProductID' => $insert_id,
                            'NutritionID' => $key,
                            'Quantity' => ($value == '' ? NULL : $value)
                        ];
                    }

                }

                $this->Nutrition_product_model->insert_batch($nutrition_data);

            }

            if (!empty($post_data['PackagesID']) && isset($post_data['PackagesID'])) {
                $product_packages = array();
                $p = 0;
                foreach ($post_data['PackagesID'] as $key => $value) {
            
                    if ($value != '') {
                        $product_packages[] = [
                            'ProductID' => $insert_id,
                            'PackagesID' =>  ($value == '' ? 0 : $value),
                            'DefaultPackagesID' => ($post_data['DefaultPackagesID'] == '' ? 0 : $post_data['DefaultPackagesID']),
                            'MinimumPackage' => ($post_data['MinimumPackage'] == '' ? 0 : $post_data['MinimumPackage']),
                            'MaximumPackage' => ($post_data['MaximumPackage'] == '' ? 0 : $post_data['MaximumPackage']),
                            'PerPiecePrice' => ($post_data['PerPiecePrice'] == '' ? 0 : $post_data['PerPiecePrice']),
                            'PerGramPrice' => ($post_data['PerGramPrice'] == '' ? 0 : $post_data['PerGramPrice']),
                        ];
                    $p++;
                    }


                }

                $this->Packages_product_model->insert_batch($product_packages);

            }


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {


        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {

            foreach ($_FILES['Image']["tmp_name"] as $key => $tmp_name) {

                $image_info = getimagesize($tmp_name);
                $image_width = $image_info[0];
                $image_height = $image_info[1];

                if($image_width != $image_height){
                    $errors['error'] =  'All images aspect ration should be same.';
                    $errors['success'] = false;
                    echo json_encode($errors);
                    exit;
                }
            }

        }




        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {


                if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                    $product_images = get_images($id, 'product');

                    $total_images = COUNT($product_images) + COUNT($_FILES['Image']['name']);

                    if ($total_images > 7) {
                        $errors['error'] = lang('please_choose_image');
                        $errors['success'] = false;
                        echo json_encode($errors);
                        exit;
                    }


                }



                if (isset($post_data['IsCustomizedProduct']) && empty($post_data['BoxIDs'])) {
                    $errors['error'] = "Please select boxes for customization";
                    $errors['success'] = false;
                    echo json_encode($errors);
                    exit;
                } elseif (isset($post_data['IsCustomizedProduct']) && !empty($post_data['BoxIDs'])) {
                    $save_parent_data['BoxIDs'] = implode(',', $post_data['BoxIDs']);
                }

                if (isset($post_data['IsCorporateProduct']) && $post_data['CorporateMinQuantity'] == '') {
                    $errors['error'] = "Please insert minimum quantity";
                    $errors['success'] = false;
                    echo json_encode($errors);
                    exit;
                } elseif (isset($post_data['IsCorporateProduct']) && $post_data['CorporateMinQuantity'] !== '') {
                    $save_parent_data['CorporateMinQuantity'] = $post_data['CorporateMinQuantity'];
                    $save_parent_data['CorporatePrice'] = $post_data['CorporatePrice'];
                }

                $save_parent_data['brand_id'] = $post_data['brand_id'];
                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['IsFeatured'] = (isset($post_data['IsFeatured']) ? 1 : 0);
                $save_parent_data['OutOfStock'] = (isset($post_data['OutOfStock']) ? 1 : 0);
                $save_parent_data['IsCustomizedProduct'] = (isset($post_data['IsCustomizedProduct']) ? 1 : 0);
                $save_parent_data['IsCorporateProduct'] = (isset($post_data['IsCorporateProduct']) ? 1 : 0);
                $save_parent_data['SKU'] = $post_data['SKU'];
                $save_parent_data['Price'] = $post_data['Price'];
                $save_parent_data['PriceType'] = $post_data['PriceType'];
                $save_parent_data['Weight'] = $post_data['Weight'];
                $save_parent_data['TagIDs'] = implode(',',$post_data['TagIDs']);
                $save_parent_data['MinimumOrderQuantity'] = $post_data['MinimumOrderQuantity'];
                $save_parent_data['CategoryID'] = implode(',',$post_data['CategoryID']);
                $save_parent_data['SubCategoryID'] = implode(',',$post_data['SubCategoryID']);
                if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {
                    if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                        $this->uploadImage('Image', 'uploads/images/', base64_decode($post_data['ProductID']), 'product', true);
                    }
                }
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);


                $this->$parent->update($save_parent_data, $update_by);

                $this->Nutrition_product_model->delete($update_by);

                if (!empty($post_data['NutritionQuantity']) && isset($post_data['NutritionQuantity'])) {
                    $nutrition_data = array();
                    foreach ($post_data['NutritionQuantity'] as $key => $value) {
                        if ($value != '') {
                            $nutrition_data[] = [
                                'ProductID' => base64_decode($post_data[$this->data['TableKey']]),
                                'NutritionID' => $key,
                                'Quantity' => ($value == '' ? NULL : $value)
                            ];
                        }

                    }

                    $this->Nutrition_product_model->insert_batch($nutrition_data);

                }
                $this->Packages_product_model->delete($update_by);

                if (!empty($post_data['PackagesID']) && isset($post_data['PackagesID'])) {
                    $product_packages = array();
                    $p = 0;
                    foreach ($post_data['PackagesID'] as $key => $value) {
                
                        if ($value != '') {
                            $product_packages[] = [
                                'ProductID' => $id,
                                'PackagesID' =>  ($value == '' ? 0 : $value),
                                'DefaultPackagesID' => ($post_data['DefaultPackagesID'] == '' ? 0 : $post_data['DefaultPackagesID']),
                                'MinimumPackage' => ($post_data['MinimumPackage'] == '' ? 0 : $post_data['MinimumPackage']),
                                'MaximumPackage' => ($post_data['MaximumPackage'] == '' ? 0 : $post_data['MaximumPackage']),
                                'PerPiecePrice' => ($post_data['PerPiecePrice'] == '' ? 0 : $post_data['PerPiecePrice']),
                                'PerGramPrice' => ($post_data['PerGramPrice'] == '' ? 0 : $post_data['PerGramPrice']),
                            ];
                        $p++;
                        }


                    }

                    $this->Packages_product_model->insert_batch($product_packages);

                }

                if($post_data['PriceType'] == 'item'){
                    $inside_title = $this->input->post('WhatsInsideTitle');
                    $old_hidden = $this->input->post('old');
                    //print_rm($inside_title);
                    $inside_title_ar = $this->input->post('WhatsInsideTitleAr');
                    $inside_images = $this->input->post('WhatsInsideTitle');
                    if(!empty($inside_title)){
                        foreach ($inside_title as $key => $inside) {
                            if($inside != ''){
                                $save_inside_data = array();
                                $save_inside_data['ProductID'] = $update_by[$this->data['TableKey']];
                                $save_inside_data['InsideTitle'] = $inside;
                                $save_inside_data['InsideTitleAr'] = $inside_title_ar[$key];
                                if (isset($_FILES['WhatsInsideImage'.$key]["name"][0]) && $_FILES['WhatsInsideImage'.$key]["name"][0] != '') {
                                   $save_inside_data['InsideImage'] =  $this->uploadImage('WhatsInsideImage'.$key, 'uploads/images/');
                                }

                                if(isset($old_hidden[$key])){
                                    $this->What_inside_model->update($save_inside_data,array('ProductInsideID' => $old_hidden[$key]));
                                }else{
                                    $this->What_inside_model->save($save_inside_data);
                                }

                                

                            }
                            
                            
                        }
                    }
                }


                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];
                $save_child_data['Ingredients'] = $post_data['Ingredients'];
                $save_child_data['Specifications'] = $post_data['Specifications'];
                //$save_child_data['Tags'] = $post_data['Tags'];
                $save_child_data['Keywords'] = $post_data['Keywords'];
                $save_child_data['MetaTags'] = $post_data['MetaTags'];
                $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                $save_child_data['MetaDescription'] = $post_data['MetaDescription'];
                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);


            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['Description'];
                    $save_child_data['Ingredients'] = $post_data['Ingredients'];
                    $save_child_data['Specifications'] = $post_data['Specifications'];
                   // $save_child_data['Tags'] = $post_data['Tags'];
                    $save_child_data['Keywords'] = $post_data['Keywords'];
                    $save_child_data['MetaTags'] = $post_data['MetaTags'];
                    $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                    $save_child_data['MetaDescription'] = $post_data['MetaDescription'];


                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['Description'];
                    $save_child_data['Ingredients'] = $post_data['Ingredients'];
                    $save_child_data['Specifications'] = $post_data['Specifications'];
                   // $save_child_data['Tags'] = $post_data['Tags'];
                    $save_child_data['Keywords'] = $post_data['Keywords'];
                    $save_child_data['MetaTags'] = $post_data['MetaTags'];
                    $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                    $save_child_data['MetaDescription'] = $post_data['MetaDescription'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->save($save_child_data);
                }


            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    public function importCsv()
    {

       // $this->validateCSV();
        $ErrorCount = 0;
        $msg = '';
        $fileName = $_FILES["Csv"]["name"];
        $temp_name = $_FILES['Csv']['tmp_name'];

        $post_data = $this->input->post();
      //  $locations = $post_data['City'];

        if ($_FILES["Csv"]["size"] > 0) {
            $location = 'uploads/';
            move_uploaded_file($temp_name, $location . $fileName);
            $file = fopen($location . $fileName, "r");
            $save_user_data = array();
            $key = 0;

            // $column = fgetcsv($file, 10000, ",");
            // dump($column);

            while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {

                $save_p_data = array();
                if ($key > 1) {
                 
                    $save_p_data['CategoryID'] = $column[4];
                    $save_p_data['SubCategoryID'] = $column[5];
                    $save_p_data['Price'] = $column[2];
                    $save_p_data['PriceType'] = $column[3];
                    $save_p_data['SKU'] = $column[1];


                    $save_p_data['IsFeatured'] = $column[17];
                    $save_p_data['IsCustomizedProduct'] = $column[22];
                    $save_p_data['OutOfStock'] = $column[18];
                    $save_p_data['BoxIDs'] = $column[23];
                    $save_p_data['IsCorporateProduct'] = $column[19];
                    $save_p_data['CorporateMinQuantity'] = $column[20];
                    $save_p_data['CorporatePrice'] = $column[21];
                    $save_p_data['MinimumOrderQuantity'] = $column[30];
                    $save_c_data['TagIDs'] = $column[11];
                    
                    $save_p_data['IsActive'] = $column[16];;//($this->session->userdata['admin']['RoleID'] == 1 ? 1 : 0);
                    $save_p_data['CreatedAt'] = $save_p_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_p_data['CreatedBy'] = $save_p_data['UpdatedBy'] = 1;
// dump($save_p_data);
                    $insert_id = $this->Product_model->save($save_p_data);

                    if ($insert_id > 0){

                        
                        $save_c_data = array();
                        $save_c_data['ProductID'] = $insert_id;
                        $save_c_data['Title'] = str_replace('?', '', $column[0]);
                        $save_c_data['Description'] = $column[8];
                        $save_c_data['Ingredients'] = $column[9];
                        $save_c_data['Specifications'] = $column[10];
                        //$save_c_data['TagIds'] = $column[11];
                        $save_c_data['Keywords'] = $column[12];
                        $save_c_data['MetaTags'] = $column[13];
                        $save_c_data['MetaKeywords'] = $column[14];
                        $save_c_data['MetaDescription'] = $column[15];
                        $save_c_data['SystemLanguageID'] = 1;
                        $save_c_data['CreatedAt'] = $save_c_data['UpdatedAt'] = date('Y-m-d H:i:s');
                        $save_c_data['CreatedBy'] = $save_c_data['UpdatedBy'] = 1;
                        $this->Product_text_model->save($save_c_data);



                        $save_c_data = array();
                        $save_c_data['ProductID'] = $insert_id;
                        $save_c_data['Title'] = str_replace('?', '', $column[36]);
                        $save_c_data['Description'] = $column[37];
                        $save_c_data['Ingredients'] = $column[38];
                        $save_c_data['Specifications'] = $column[39];
                        //$save_c_data['TagIds'] = $column[11];
                        $save_c_data['Keywords'] = $column[40];
                        $save_c_data['MetaTags'] = $column[41];
                        $save_c_data['MetaKeywords'] = $column[42];
                        $save_c_data['MetaDescription'] = $column[43];
                        $save_c_data['SystemLanguageID'] = 2;
                        $save_c_data['CreatedAt'] = $save_c_data['UpdatedAt'] = date('Y-m-d H:i:s');
                        $save_c_data['CreatedBy'] = $save_c_data['UpdatedBy'] = 1;
                        $this->Product_text_model->save($save_c_data);

                        if($column[6] != '' && $column[7] != ''){
                            $nutritions = explode(',',$column[6]);
                            $quantity = explode(',',$column[7]);
                            $save_n_data = array();
                            foreach($nutritions as $nk => $nutrition){
                                $save_n_data['ProductID'] = $insert_id;
                                $save_n_data['NutritionID'] = $nutrition;
                                $save_n_data['Quantity'] = isset($quantity[$nk]) ? $quantity[$nk] : 0;
                                $this->Nutrition_product_model->save($save_n_data);
                            }
                            
                            

                        }


                        

                        if ($column[24] != '') {
                            $save_image = array();
                            $save_image['FileID'] = $insert_id;
                            $save_image['ImageType'] = 'product';
                            
                            $file_name = $column[24];
                            $save_image['ImageName'] = 'uploads/images/' . $file_name;
                            $this->Site_images_model->save($save_image);
                        }

                        if ($column[25] != '') {
                            $save_image = array();
                            $save_image['FileID'] = $insert_id;
                            $save_image['ImageType'] = 'product';
                            //$file_name = str_replace('https://static-01.daraz.pk/p/','uploads/images/', $column[14]);
                            $file_name = $column[25];
                            $save_image['ImageName'] = 'uploads/images/' . $file_name;
                            $this->Site_images_model->save($save_image);
                        }
                        if ($column[26] != '') {
                            $save_image = array();
                            $save_image['FileID'] = $insert_id;
                            $save_image['ImageType'] = 'product';
                            //$file_name = str_replace('https://static-01.daraz.pk/p/','uploads/images/', $column[14]);
                            $file_name = $column[26];
                            $save_image['ImageName'] = 'uploads/images/' . $file_name;
                            $this->Site_images_model->save($save_image);
                        }
                        if ($column[27] != '') {
                            $save_image = array();
                            $save_image['FileID'] = $insert_id;
                            $save_image['ImageType'] = 'product';
                            //$file_name = str_replace('https://static-01.daraz.pk/p/','uploads/images/', $column[14]);
                            $file_name = $column[27];
                            $save_image['ImageName'] = 'uploads/images/' . $file_name;
                            $this->Site_images_model->save($save_image);
                        }

                        if ($column[28] != '') {
                            $save_image = array();
                            $save_image['FileID'] = $insert_id;
                            $save_image['ImageType'] = 'product';
                            //$file_name = str_replace('https://static-01.daraz.pk/p/','uploads/images/', $column[14]);
                            $file_name = $column[28];
                            $save_image['ImageName'] = 'uploads/images/' . $file_name;
                            $this->Site_images_model->save($save_image);
                        }

                        if ($column[29] != '') {
                            $save_image = array();
                            $save_image['FileID'] = $insert_id;
                            $save_image['ImageType'] = 'product';
                            //$file_name = str_replace('https://static-01.daraz.pk/p/','uploads/images/', $column[14]);
                            $file_name = $column[29];
                            $save_image['ImageName'] = 'uploads/images/' . $file_name;
                            $this->Site_images_model->save($save_image);
                        }



                        $store_id = explode(',',$column[31]);
                        $quantity = explode(',',$column[32]);
                        if(!empty($store_id)){
                            $save_availability = array();
                            $save_availability['ProductID'] = $insert_id;
                            foreach ($store_id as $key => $value) {
                                $save_availability['StoreID'] = $value;
                                $save_availability['Quantity'] = $quantity[$key];
                                $this->Product_availability_model->save($save_availability);
                            }


                        }



                        if($column[3] == 'item'){
                            $inside_title = explode(',',$column[33]);
                            
                            $inside_title_ar = explode(',',$column[34]);
                            $inside_images = explode(',',$column[35]);
                            if(!empty($inside_title)){
                                foreach ($inside_title as $key => $inside) {
                                    if($inside != ''){
                                        $save_inside_data = array();
                                        $save_inside_data['ProductID'] = $insert_id;
                                        $save_inside_data['InsideTitle'] = $inside;
                                        $save_inside_data['InsideTitleAr'] = (isset($inside_title_ar[$key]) ? $inside_title_ar[$key] : '');
                                        $save_inside_data['InsideImage'] =  (isset($inside_images[$key]) ? $inside_images[$key] : '');

                                        $this->What_inside_model->save($save_inside_data);

                                        

                                    }
                                    
                                    
                                }
                            }
                        }


                    }

                }

                $key++;


            }
            if ($ErrorCount > 0) {
                $msg = ' There are some rows skipped';
            }

            if (!empty($_FILES['file']['name'])) {
                $this->extract();
            }
            //$this->User_model->insert_batch($save_user_data);
            $success['error'] = false;
            $success['success'] = 'CSV File imported successfully.' . $msg;
            $success['reload'] = true;
            echo json_encode($success);
            exit;

        }
    }

    public function extract()
    {

        if (!empty($_FILES['file']['name'])) {
            // Set preference
            $config['upload_path'] = 'uploads/images/';
            $config['allowed_types'] = 'zip';
            $config['max_size'] = '512000'; // max_size in kb (5 MB)
            $config['file_name'] = $_FILES['file']['name'];

            // Load upload library
            $this->load->library('upload', $config);

            // File upload
            if ($this->upload->do_upload('file')) {
                // Get data about the file
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];

                ## Extract the zip file ---- start
                $zip = new ZipArchive;
                $res = $zip->open("uploads/images/" . $filename);
                if ($res === TRUE) {

                    // Unzip path
                    $extractpath = "uploads/images/";

                    // Extract file
                    $zip->extractTo($extractpath);
                    $zip->close();
                    unlink("uploads/images/" . $filename);

                    return TRUE;
                } else {
                    $success['success'] = false;
                    $success['error'] = 'Images Zip not extract at server ask admin please.';
                    $success['reload'] = true;
                    echo json_encode($success);
                    exit;
                }


            } else {
                $success['success'] = false;
                $success['error'] = 'Images Zip not uploaded at server ask admin please.';
                $success['reload'] = true;
                echo json_encode($success);
                exit;
            }
        } else {
            $success['success'] = false;
            $success['error'] = 'Images Zip not uploaded at server ask admin please.';
            $success['reload'] = true;
            echo json_encode($success);
            exit;
        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function deleteProductInside()
    {

        if (!checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        

        $deleted_by = array();
        $deleted_by['ProductInsideID'] = $this->input->post('id');
        $inside_data = $this->What_inside_model->get($this->input->post('id'),false,'ProductInsideID');

        if($inside_data->InsideImage != ''){
            unlink($inside_data->InsideImage);
        }
        
        $this->What_inside_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}