<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use SmsaSDK\Smsa;

class Orders extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');
        $this->load->model('City_model');
        $this->load->model('Model_general');
        $this->load->model('User_address_model');
        $this->load->model('Order_extra_charges_model');
        $this->load->model('Model_general');
        $this->load->model('Character_model');
        $this->load->model('Fill_Color_model');
        $this->load->model('Wrapping_model');
        $this->load->model('Content_type_sub_model');
        $this->load->model('Content_type_model');
        $this->load->model('Powder_Color_model');
        $this->load->model('Filling_model');
        $this->load->model('Box_category_model');
        $this->load->model('Box_model');
        $this->load->model('Shape_model');
        $this->load->model('Ribbon_model');
        $this->load->model('Product_model');
        $this->data['language'] = $this->language;

        // https://stackoverflow.com/questions/31721254/codeigniter-initialize-function-not-working
    }


    public function email_test(){
        $data['to'] = 'sarfraz.cs10@gmail.com';
        $data['subject'] = 'Chocomood';
        $data['message'] = 'This is the formate email from chocomood';
        sendEmail($data);
    }

    public function index()
    {
        $this->data['view'] = 'backend/orders/manage';
        $where = '';


        if ($this->session->userdata['admin']['RoleID'] == 2 OR $this->session->userdata['admin']['RoleID'] == 4) {
            $where = ' AND user_address.CityID = ' . $this->session->userdata['admin']['CityID'];
            if($this->session->userdata['admin']['RoleID'] == 4){
                $where = ' AND orders.StoreID = ' . $this->session->userdata['admin']['StoreID'];
            }
        } elseif ($this->session->userdata['admin']['RoleID'] == 3) {
            $where = ' AND orders.DriverID = ' . $this->session->userdata['admin']['UserID'];

        }
        //for standard order
        $where .= ' AND orders.isCustomize = "0"';
        $post_data = $this->input->post();

        if(isset($post_data['OrderTrackID']) && $post_data['OrderTrackID'] != ''){
            $where .= ' AND orders.OrderNumber = '.$post_data['OrderTrackID'];
        }

        if(isset($post_data['From']) && isset($post_data['To']) && $post_data['From'] != '' && $post_data['To'] != ''){
            $where .= ' AND DATE(orders.CreatedAt)  BETWEEN "'.$post_data['From'].'" AND "'.$post_data['To'].'"';
        }


         if(isset($post_data['Email']) && $post_data['Email'] != ''){
            $where .= ' AND users.Email = "'.$post_data['Email'].'"';
        }


         $where .= ' AND orders.Hide = 0';


        $this->data['post_data'] = $post_data;



       $this->data['order_statuses'] = $this->Model_general->getAll('order_statuses', false, 'ASC', 'OrderStatusID');

        if (isset($_GET['status']) && $_GET['status'] !== '')
        {
            $status = $_GET['status'];
        } else {
            $status = 'pending';
        }

        if ($status == 'pending')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 1 $where", false, 0, $this->language, 'DESC');

        } elseif ($status == 'packed')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 2 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'dispatched')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 3 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'delivered')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 4 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'cancelled')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 5 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'unopened')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.IsRead = 0 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'cancelled_not_collect')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.CollectFromStore = 1 AND orders.Status = 5 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'all')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.OrderID > 0 $where", false, 0, $this->language, 'DESC');
        } else {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 1 $where", false, 0, $this->language, 'DESC');
            $status = 'pending';
        }

        //echo $this->db->last_query();exit;
        //print_rm($this->data['orders']);exit;

        $this->data['url_status'] = $status;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function custom_order()
    {
        $this->data['view'] = 'backend/order_custom/manage';
        $where = '';


        if ($this->session->userdata['admin']['RoleID'] == 2 OR $this->session->userdata['admin']['RoleID'] == 4 ) {
            $where = ' AND user_address.CityID = ' . $this->session->userdata['admin']['CityID'];
            if($this->session->userdata['admin']['RoleID'] == 4){
                $where = ' AND orders.StoreID = ' . $this->session->userdata['admin']['StoreID'];
            }
        } elseif ($this->session->userdata['admin']['RoleID'] == 3) {
            $where = ' AND orders.DriverID = ' . $this->session->userdata['admin']['UserID'];

        }

        if (!checkUserRightAccess(112, $this->session->userdata['admin']['UserID'], 'CanView')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }

        //for custom order
        $where .= ' AND orders.isCustomize = "1"';

        $post_data = $this->input->post();
        
        if(isset($post_data['product_type']) && $post_data['product_type'] != ''){
            $product_type = '';
            if($post_data['product_type'] == 1)
            {
                $product_type = 'Choco Shape';
            } else if($post_data['product_type'] == 2)
            {
                $product_type = 'Choco Box';
            } else if($post_data['product_type'] == 3)
            {
                $product_type = 'Choco Message';
            }

            $where .= ' AND order_items.ItemType = "'.$product_type.'"';
        }
        if(isset($post_data['OrderTrackID']) && $post_data['OrderTrackID'] != ''){
            $where .= ' AND orders.OrderNumber = '.$post_data['OrderTrackID'];
        }

        if(isset($post_data['From']) && isset($post_data['To']) && $post_data['From'] != '' && $post_data['To'] != ''){
            $where .= ' AND DATE(orders.CreatedAt)  BETWEEN "'.$post_data['From'].'" AND "'.$post_data['To'].'"';
        }


         if(isset($post_data['Email']) && $post_data['Email'] != ''){
            $where .= ' AND users.Email = "'.$post_data['Email'].'"';
        }


         $where .= ' AND orders.Hide = 0';


        $this->data['post_data'] = $post_data;

        $this->data['order_statuses'] = $this->Model_general->getAll('order_statuses', false, 'ASC', 'OrderStatusID');

       
        if (isset($_GET['status']) && $_GET['status'] !== '')
        {
            $status = $_GET['status'];
        } else {
            $status = 'pending';
        }

        if ($status == 'pending')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 1 $where", false, 0, $this->language, 'DESC');

        } elseif ($status == 'packed')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 2 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'dispatched')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 3 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'delivered')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 4 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'cancelled')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 5 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'unopened')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.IsRead = 0 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'cancelled_not_collect')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.CollectFromStore = 1 AND orders.Status = 5 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'all')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.OrderID > 0 $where", false, 0, $this->language, 'DESC');
        } else {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 1 $where", false, 0, $this->language, 'DESC');
            $status = 'pending';
        }

        //echo $this->db->last_query();exit;
        //print_rm($this->data['orders']);exit;

        $this->data['url_status'] = $status;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($order_id)
    {
        $this->Order_model->update(array('IsRead' => 1), array('OrderID' => $order_id));
        $language = 'EN';
        $this->data['view'] = 'backend/orders/view';
        $where = 'cities.IsActive = 1 AND system_languages.ShortCode = "' . $language . '"';
        $this->data['cities'] = $this->City_model->getJoinedData(false, 'CityID', $where);
        $this->data['order_statuses'] = $this->Model_general->getAll('order_statuses', false, 'ASC', 'OrderStatusID');
        $this->data['order'] = $this->Order_model->getOrders("orders.OrderID = $order_id");
        $this->data['order_extra_charges'] = $this->Order_extra_charges_model->getMultipleRows(array('OrderID' => $order_id));
        $this->data['payment_address'] = $this->User_address_model->getAddresses("user_address.AddressID = " . $this->data['order'][0]->AddressIDForPaymentCollection);
        $this->data['order_items'] = getOrderItems($this->data['order'][0]->OrderID);
        $data['language'] = $this->language;
        //print_rm($this->data);exit;

        $this->load->view('backend/layouts/default', $this->data);
    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;
            case 'assign_driver':
                $this->assignDriver();
                break;
        }
    }

    public function get_drivers()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');
        $order_info = $this->Order_model->getOrders('orders.OrderID = ' .$this->data['OrderID']);
        
        $city  = @$order_info[0]->CityID;

        $this->data['users'] = $this->User_model->getBackendUsers('users.RoleID = 3 AND users.CityID = '.$city);

        $html = $this->load->view('backend/orders/assign_driver_popup', $this->data, true);

        $response = array();
        $response['html'] = $html;
        echo json_encode($response);


    }

    public function assignDriver()
    {
        $update = array();
        $update_by = array();
        $update_by['OrderID'] = $this->input->post('OrderID');
        $update['DriverID'] = $this->input->post('UserID');
        
       
        //print_rm($order_info);
        $update['DeliveryOTP'] = RandomString();
        $this->Order_model->update($update, $update_by);

        $order_info = $this->Order_model->getOrders('orders.OrderID = ' . $update_by['OrderID']);
        $order_info = $order_info[0];
        $order_info->OTP =  $update['DeliveryOTP'];


        $file_name = $this->generateQRCode($order_info->OTP,$this->input->post('OrderID'));
        $this->sendDeliveryOtpSMSToCustomer($order_info);
        $this->sendDeliveryOtpToCustomer($order_info,$file_name);


        

        
        if ($order_info->DriverID > 0) {
            $this->sendOrderAssignedSMSToDriver($order_info);
        }
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        $success['reload'] = true;
        // $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $OrderID;
        echo json_encode($success);
    }

    public function generateQRCode($info,$OrderID){
        $this->load->library('phpqrcode/qrlib');
        $SERVERFILEPATH = FCPATH.'uploads/images/qrcode/';
        
        $text = $info;
        $text1= substr($text, 0,9);
        
        $folder = $SERVERFILEPATH;
        $file_name1 = $OrderID."-Qrcode.png";
        $file_name = $folder.$file_name1;
        QRcode::png($text,$file_name);
        return $file_name;
    }

    public function get_otp()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');

        
        $html = $this->load->view('backend/orders/otp_pop_up', $this->data, true);

        $response = array();
        $response['html'] = $html;
        echo json_encode($response);


    }

    public function update_status_with_otp()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');
        $this->data['OTP'] = $this->input->post('OTP');

        $OrderData = $this->Order_model->get($this->data['OrderID'],true,'OrderID');

        if($OrderData['DeliveryOTP'] == $this->data['OTP']){
            $this->Order_model->update(array('Status' => 4),array('OrderID' => $this->data['OrderID']));
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = TRUE;
            // $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $OrderID;
            echo json_encode($success);
        }else{
            $success['error'] = 'Delivery OTP is not valid';
            $success['success'] = false;
           
           
            echo json_encode($success);

        }

        

        



    }
    public function update()
    {
        $post_data = $this->input->post(); // OrderID, Status
        if (isset($post_data['OrderID'])) {
            $order = $this->Order_model->get($post_data['OrderID'], false, 'OrderID');
            if ( isset($post_data['is_semsa'] ) ) {

                $is_semsa = 0;
                if($post_data['Status'] == '5') //cancelled
                {
                    if($order->SemsaShippingAmount != NULL)
                    {
                        $result = Smsa::key('Testing1');
                        Smsa::nullValues(''); 
                        $is_semsa = 1;
                        $shipmentData = [
                                'awbNo' => $order->AWBNumber, 
                                'reas' => 'SA', 
                            ];
                        $shipment = Smsa::cancelShipment($shipmentData);
                    }
                }
                $this->Order_model->update(array('Status' => $post_data['Status']), array('OrderID' => $post_data['OrderID']));
                $order_info = $this->Order_model->getOrders('orders.OrderID = ' . $post_data['OrderID']);
                $order_info = $order_info[0];
                $this->SendStatusChangedSmsToCustomer($order_info);
                $this->SendStatusChangedEmailToCustomer($order_info);
                if($is_semsa != 1){
                    $this->sendStatusChangedSMSToDriver($order_info);
                    $this->sendStatusChangedEmailToDriver($order_info);
                }
                $this->sendStatusChangedEmailToStoreAdmin($order_info);
                $success['error'] = false;
                $success['success'] = lang('update_successfully');
                echo json_encode($success);
                exit;
            } else {
                $errors['error'] = "Please first assign this order to a driver";
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function SendStatusChangedEmailToStoreAdmin($order_info)
    {
        if ($order_info->StoreAdminEmail !== '') {
            $lang = (isset($order_info->SPreferredLang) && $order_info->SPreferredLang !== '') ? (($order_info->SPreferredLang == 0)? 'EN' : 'AR'): 'EN';
            $email_template = get_email_template(10,$lang);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->StoreAdminFullName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
            $message = str_replace("{{order_status}}", $order_info->OrderStatusEn, $message);
           
            $data['to'] = $order_info->StoreAdminEmail;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    public function invoice($orderId)
    {
       
        $orderInfo      = $this->Order_model->getOrders("orders.OrderID = $orderId");
        $orderDetails   = $this->Order_item_model->getOrdersItems($orderId);
        // return $order_details;
        // print_r($orderInfo[0]->isCustomize);
        $itemDetailStr = array();
        foreach ($orderDetails as $orderDetail ) {
            $detailStr      = "";
            $productTile    = "";
            if($orderDetail->ProductID==0){
                $itemDetail   = $this->Order_item_model->getSubItem($orderDetail->OrderItemID)[0];
                $subItems = array_count_values(explode(",",$itemDetail->CustomizedOrderProductIDs));
                $productTile = "<b>".$itemDetail->ItemType."</b>";
                $detailStr = "<ul style='list-style-type: none;'>";
                foreach ($subItems as $itemId => $count) {
                    $subItemDetial = $this->Order_item_model->getSubItemDetial($itemId, $itemDetail->ItemType)[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($subItemDetial->CharacterImage)."' style='width:35px;height:auto' /> <span>".$count."x </span> </li>";
                }
                $itemDetail   = $this->Order_item_model->getSubItem($orderDetail->OrderItemID)[0];
                if($orderDetail->Ribbon != "" && $orderDetail->Ribbon != "0"){
                    $sideItem = $this->Order_item_model->getSideItemDetail($orderDetail->Ribbon, "ribbons", "RibbonID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->RibbonImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->CustomizedBoxID != "" && $orderDetail->CustomizedBoxID != "0"){
                    $sideItem = $this->Order_item_model->getSideItemDetail($orderDetail->CustomizedBoxID, "boxes", "BoxID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'>Box Name: ".$sideItem->Title." <span>1x</span> </li>";
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->BoxImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->Wrapping != "" && $orderDetail->Wrapping != "0"){
                    $sideItem = $this->Order_item_model->getSideItemDetail($orderDetail->Wrapping, "wrappings", "WrappingID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->WrappingImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->content_type_id != "" && $orderDetail->content_type_id != "0"){
                    if($orderDetail->content_text!=""){
                        $sideItem   = $this->Order_item_model->getContentTypeWithTextnImage($orderDetail->content_type_id, "shape_content_type_text", "ContentTypeID", $this->language)[0];
                        $detailStr .= "<li style='display: flex;align-items:center;'> Text Title:".$sideItem->Title." <br/> Text: ".$orderDetail->content_text." </li>";
                    }
                    if($orderDetail->content_type_sub_id != "" && $orderDetail->content_type_sub_id != "0"){
                        $sideItem   = $this->Order_item_model->getContentSubTypeWithImage($orderDetail->content_type_sub_id, $orderDetail->content_type_sub_image_id, $this->language);
                        if(count($sideItem)){
                            $detailStr .= "<li style='display: flex;align-items:center;'> <div>Title: ".$sideItem[0]->Title."</div> <div><img src='".base_url($sideItem[0]->ImageName)."' style='width:35px;height:auto' /> </div></li>";
                        }
                    }
                }
                $detailStr .= "</ul>";
                $itemDetailStr["id_".$orderDetail->OrderItemID]["title"]        = $productTile;
                $itemDetailStr["id_".$orderDetail->OrderItemID]["description"]  = $detailStr;
            }
        }

        $order_details              = $this->Order_model->getOrders("orders.OrderID = $orderId");
        $data['order']              = $order_details[0];
        $data['language']           = $this->language;
        $data['barcode']            = generateBarcode($orderId, "barcode", $type = 'image');
        $data["itemDetailStrArr"]   = $itemDetailStr;
        $order_html                 = $this->load->view('frontend/invoice', $data, true);
        echo $order_html;
        exit();
        /*$order_html = get_order_invoice($OrderID, 'print');
        echo $order_html;
        exit();*/
    }

    private function delete()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    private function SendStatusChangedSmsToCustomer($order_info)
    {
        if (isset($order_info->Mobile) && $order_info->Mobile != '') {
            if ($order_info->Status == 4) // Delivered order
            {
                $msg = lang('dear')." " . $order_info->FullName . "\n".lang('order_with_deliver')." # " . $order_info->OrderNumber . " ".lang('thankyou_chocomood');
            } else {
                $msg = lang('dear')." ". $order_info->FullName . "\n".lang('order_status_update')." # " . $order_info->OrderNumber . " ".lang('changed_to') ." " . $order_info->OrderStatusEn . ".";
            }
            sendSms($order_info->Mobile, $msg);
        }
    }

    private function SendStatusChangedEmailToCustomer($order_info)
    {
        if ($order_info->Email !== '') {
            $lang = (isset($order_info->UPreferredLang) && $order_info->UPreferredLang !== '') ? (($order_info->UPreferredLang == 0)? 'EN' : 'AR'): 'EN';
            $email_template = get_email_template(5,$lang);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->FullName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
            $message = str_replace("{{order_status}}", $order_info->OrderStatusEn, $message);
            $data['to'] = $order_info->Email;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    private function sendStatusChangedSMSToDriver($order_info)
    {
        if (isset($order_info->AssignedDriverMobile) && $order_info->AssignedDriverMobile != '') {
            $msg = lang('lang')." " . $order_info->AssignedDriverName . "\n".lang('status_assign_order')." # " . $order_info->OrderNumber . " ".lang('changed_to')." " . $order_info->OrderStatusEn . ".";
            sendSms($order_info->AssignedDriverMobile, $msg);
        }
    }

    private function sendStatusChangedEmailToDriver($order_info)
    {
        if ($order_info->AssignedDriverEmail !== '') {
            $lang = (isset($order_info->DPreferredLang) && $order_info->DPreferredLang !== '') ? (($order_info->DPreferredLang == 0)? 'EN' : 'AR'): 'EN';
            $email_template = get_email_template(6,$lang);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->AssignedDriverName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
            $message = str_replace("{{order_status}}", $order_info->OrderStatusEn, $message);
            $data['to'] = $order_info->AssignedDriverEmail;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    private function sendOrderAssignedSMSToDriver($order_info)
    {

        if (isset($order_info->AssignedDriverMobile) && $order_info->AssignedDriverMobile != '') {
            if($order_info->DPreferredLang != 1){
            $msg = "Dear " . $order_info->AssignedDriverName . "\nAn order is assigned to you with Order # " . $order_info->OrderNumber ." At Chocomood";

            }else{
                $msg = "عزيزي " . $order_info->AssignedDriverName . "\n تم تعيين للك ترتيب # " . $order_info->OrderNumber ." في شوكومود";

            }
            //$msg = lang('dear')." " . $order_info->AssignedDriverName . "\n".lang('order_assigned')." # " . $order_info->OrderNumber ." ".lang('at_chocomood');
            //print_rm(sendSms($order_info->AssignedDriverMobile, $msg));
            sendSms($order_info->AssignedDriverMobile, $msg);
        }

    }


    private function sendDeliveryOtpToCustomer($order_info,$file_name = false)
    {
        if ($order_info->Email !== '') {
            $lang = (isset($order_info->UPreferredLang) && $order_info->UPreferredLang !== '') ? (($order_info->UPreferredLang != 1)? 'EN' : 'AR'): 'EN';
            $email_template = get_email_template(7, $lang);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->FullName, $message);
            $message = str_replace("{{OTP}}", $order_info->OTP, $message);
            $message = str_replace("{{order_number}}", $order_info->OrderNumber, $message);
            $data['to'] = $order_info->Email;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data,$file_name);
        }
    }

    private function sendDeliveryOtpSMSToCustomer($order_info)
    {
        if (isset($order_info->Mobile) && $order_info->Mobile != '') {
            if($order_info->UPreferredLang != 1)
            {
                $msg = "Your order"." # " . $order_info->OrderNumber . " "."is assigned to the Delivery Person with OTP"." ".$order_info->OTP.". "."Please provide this OPT to the assigned person.";
            }else
            {
                $msg = "طلبك"." # " . $order_info->OrderNumber . " "."تم تعيينه إلى مندوب التوصيل مع"." ".$order_info->OTP.". "."يرجى تقديم إلى الشخص المعين";    
            }
           // $msg = "Dear " . $order_info->AssignedDriverName . "\nAn order is assigned to you with Order # " . $order_info->OrderNumber . " at Chocomood.";
            
            sendSms($order_info->Mobile, $msg);
        }
    }

    public function getChocoboxDetail()
    {
        $TempOrderID = $this->input->post('id');
        $TempOrder = $this->Order_model->getOrders("orders.OrderID = $TempOrderID");
        $order_items = getOrderItems($TempOrder[0]->OrderID);
        
        $box_id = $this->input->post('box_id');
        $box_type = $this->input->post('box_type');
        $ribbon = $this->input->post('ribbon');
        // var_dump($TempOrder);
        $html = '<div class="row">
                        <h4>' . $box_type . ' Detail</h4>';
        if ($box_type == 'Choco Box') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            foreach ($ProductIDs as $ProductID) {
                $product = $this->Product_model->getProductDetail($ProductID, $this->language);
                $html .= '<div class="col-md-2 chocobox_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . base_url(get_images($product->ProductID, 'product', false)) . '" style="width: 61px;height: 59px;">
                                </div>
                                    <h4>' . $product->Title . '</h4>
                                    <h5><strong>' . number_format($product->Price, 2) . '</strong> ' . lang('sar') . '</h5>';
                if ($product->OutOfStock == 1) {
                    $html .= '<small style="font-weight: bold;color: red;">' . lang('out_of_stock') . '</small>';
                }
                $html .= '</div>
                        </div>';
            }
        } elseif ($box_type == 'Choco Message') {
            $ProductIDs = $this->input->post('ProductIDs');
            $ProductIDs = explode(',', $ProductIDs);
            $html .= "<div class='row'>";
            $productForPrice = array();
            foreach ($ProductIDs as $ProductID) {
                $product = $this->Character_model->getData('CharacterID = ' . $ProductID);
                if (empty($productForPrice)) {
                    array_push($productForPrice, $product[0]);
                } else {
                    $check = 0;
                    foreach ($productForPrice as $v) {
                        if ($v->CharacterID == $ProductID) {
                            $check = 1;
                        }
                    }
                    if ($check == 0) {
                        array_push($productForPrice, $product[0]);
                    }
                }

                $html .= '<div class="col-md-2 chocomsg_item">
                            <div class="inbox">
                                <div class="imgbox">
                                    <img src="' . base_url($product[0]->CharacterImage) . '" style="width: 61px;height: 59px;">
                                </div>';
                $html .= '</div>
                        </div>';
            }
            $html .= "</div>";
        }
        elseif($box_type == 'Choco Shape'){
            $ProductIDs = $this->input->post('ProductIDs');
            $shapeData = $this->Shape_model->getJoinedData(false, 'ShapeID', "shapes.ShapeID = ".$ProductIDs." AND system_languages.ShortCode = '" . $this->language . "'")[0];
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="col-md-12"><img src="' . base_url($order_items[0]->CustomizedShapeImage) . '" alt="" style="width: 400px;"></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_title') . ': <b>'.$shapeData->Title.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_order_processing_time') . ': <b>'.$shapeData->OrderProcessingTime.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_price_per_pcs') . ': <b>'.$shapeData->PricePerPcs.'</b></h5></div>';
            $html .= '<div class="col-md-6"><h5>' . lang('shape_individual_size') . ': <b>'.$shapeData->IndividualSize.'</b></h5></div>';
            $html .= '<div class="col-md-12"><h5>' . lang('shape_Description') . ': <b>'.$shapeData->Description.'</b></h5></div>';
            $html .= '<div class="col-md-12"><h5></b></h5></div>';
            $html .= '<div class="col-md-12"><a href="" target="_blank"></a></div>';
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<table> </table>';
            if($order_items[0]->content_type_id != '')
                    {
                        $content_type = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = " . $order_items[0]->content_type_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                            <div class="col-md-12">
                                <h4>Content Type: </h4>
                                <p>'.$content_type[0]->Title.'</p>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $content_type[0]->Price . '</b>
                            </div>
                           
                        ';
                    }

                    if($order_items[0]->content_type_sub_id != '')
                    {
                        $content_type_sub = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = " . $order_items[0]->content_type_sub_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $content_type_sub_image_id = $order_items[0]->content_type_sub_image_id;
                        
                        $html .= '
                            <div class="col-md-12">
                                <h4>Content Type Sub: </h4>
                                <p>'.$content_type_sub[0]->Title.'</p>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $content_type_sub[0]->Price . '</b>
                            </div>';
                        if($content_type_sub_image_id != '')
                        {
                            $content_type_sub_image = get_images_id($content_type_sub_image_id,'choco_box_printable_upload_image',false);
                            $html .= '<div class="col-md-12">
                                    <img src="' . base_url($content_type_sub_image) . '">
                                </div>
                            ';
                        }
                        
                    }

                    if($order_items[0]->content_text != '')
                    {
                        $html .= '
                            <div class="col-md-12">
                                <h4>Custom Text: </h4>
                                <p>'.$order_items[0]->content_text.'</p>
                            </div>';
                    }
                    
                    if($order_items[0]->Wrapping != '')
                    {
                        $wrappingIDs = $order_items[0]->Wrapping;
                        $wrappingIDs = explode(',', $wrappingIDs);
                        $wrap_count = 0;
                        foreach ($wrappingIDs as $wrappingID) 
                        {
                            $wrap_count++;
                            $wrapping = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID = " . $wrappingID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Wrapping '.$wrap_count.': </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $wrapping[0]->WrappingPrice . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <img src="' . base_url($wrapping[0]->WrappingImage) . '">
                                    </div>
                                ';
                        }
                    }
                    if($order_items[0]->Fillings != '')
                    {
                        $fillingIDs = $order_items[0]->Fillings;
                        $fillingIDs = explode(',', $fillingIDs);
                        $filling_count = 0;
                        foreach ($fillingIDs as $fillingID) 
                        {
                            $filling_count++;
                            $filling = $this->Filling_model->getJoinedData(false, 'FillingId', "fillings.FillingId = " . $fillingID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Filling '.$filling_count.': </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $filling[0]->price . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <img src="' . base_url($filling[0]->image) . '">
                                    </div>
                                ';
                        }
                    }

                    if($order_items[0]->FillColor != '')
                    {
                        $fillColorIDs = $order_items[0]->FillColor;
                        $fillColorIDs = explode(',', $fillColorIDs);
                        $fillColor_count = 0;
                        foreach ($fillColorIDs as $fillColorID) 
                        {
                            $fillColor_count++;
                            $fillColor = $this->Fill_Color_model->getJoinedData(false, 'FillColorId', "fill_colors.FillColorId = " . $fillColorID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Fill Color: </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $fillColor[0]->price . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="color" readonly value="' . $fillColor[0]->code . '">
                                    </div>
                                ';
                        }
                    }

                    if($order_items[0]->PowderColor != '')
                    {
                        $powderColorIDs = $order_items[0]->PowderColor;
                        $powderColorIDs = explode(',', $powderColorIDs);
                        $powderColor_count = 0;
                        foreach ($powderColorIDs as $powderColorID) 
                        {
                            $powderColor_count++;
                            $powderColor = $this->Powder_Color_model->getJoinedData(false, 'PowderColorId', "powder_colors.PowderColorId = " . $powderColorID . " AND system_languages.ShortCode = '" . $this->language . "'");
                            $html .= '
                                    <div class="col-md-12">
                                        <h4>Powder Color: </h4>
                                    </div>
                                    <div class="col-md-12">
                                        Price: <b>' . $powderColor[0]->price . '</b>
                                    </div>
                                    <div class="col-md-12">
                                        <img src="' . base_url($powderColor[0]->image) . '">
                                    </div>
                                ';
                        }
                    }
        }
        $html .= '</div>';

        if ($box_type == 'Choco Message') {
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="row"><h4>' . lang('box_packaging_detail') . '</h4>';
            if (isset($box_id) && $box_id > 0) {
                $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $box_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                if ($box) {
                    $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box[0]->Title . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box[0]->BoxPrice . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box[0]->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><img src="' . base_url($box[0]->BoxImage) . '" alt="' . $box[0]->Title . '" style="width: 400px;"></div>';
                    $html .= '<div class="col-md-12"><a href="' . base_url($box[0]->BoxImage) . '" target="_blank">' . lang('enlarge_image') . '</a></div>';
                }
            }
            $html .= '</div><br><br>';
            $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $ribbon . " AND system_languages.ShortCode = '" . $this->language . "'");
            $html .= '<div class="row">
                    <div class="col-md-12">
                        <h4>Ribbon: </h4>
                    </div>
                    <div class="col-md-12">
                        Price: <b>' . $ribbon[0]->RibbonPrice . '</b>
                    </div>
                    <div class="col-md-12">
                        <img src="' . base_url($ribbon[0]->RibbonImage) . '">
                    </div>
                   </div>';
            $html .= '<div class="row">
                    <div class="col-md-12">
                        <h4>Character: </h4>
                    </div>
                    ';
            foreach ($productForPrice as $v) {
                $html .= '  
                        <div class="col-md-12 ">
                            
                                    <img src="' . base_url($v->CharacterImage) . '" style="width: 61px;height: 59px;">
                                    Price: ' . $v->price . '
                </div>';
            }
            $html .= '</div>';
        }
        elseif($box_type == 'Choco Box')
        {
            $html .= '<div class="clearfix"></div><br><br>';
            $html .= '<div class="row"><h4>' . lang('box_packaging_detail') . '</h4>';
            if (isset($box_id) && $box_id > 0) {
                $box = $this->Box_model->getJoinedData(false, 'BoxID', "boxes.BoxID = " . $box_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                if ($box) {
                    $html .= '<div class="col-md-12"><h5>' . lang('box_title') . ' <b>' . $box[0]->Title . '</b></h5></div>';
                    $html .= '<div class="col-md-12"><h5>' . lang('box_price') . ' <b>' . $box[0]->BoxPrice . '</b></h5></div>';
                    if($box[0]->PriceType == 'item')
                    {
                        $html .= '<div class="col-md-12"><h5>' . lang('box_capacity') . ' <b>' . $box[0]->BoxSpace . ' ' . lang('pieces') . '</b></h5></div>';
                    }else
                    {
                        $html .= '<div class="col-md-12"><h5>Weight: <b>' . $box[0]->weight . ' Gram</b></h5></div>';
                    }
                    
                    if($box[0])
                    $html .= '<div class="col-md-12"><img src="' . base_url($box[0]->BoxImage) . '" alt="' . $box[0]->Title . '" style="width: 400px;"></div>';
                    $html .= '<div class="col-md-12"><a href="' . base_url($box[0]->BoxImage) . '" target="_blank">' . lang('enlarge_image') . '</a></div>';
                }
                if($order_items[0]->Ribbon != '0')
                {
                    if($order_items[0]->content_type_id != '')
                    {
                        $content_type = $this->Content_type_model->getJoinedData(false, 'ContentTypeID', "shape_content_type.ContentTypeID = " . $order_items[0]->content_type_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                            <div class="col-md-12">
                                <h4>Content Type: </h4>
                                <p>'.$content_type[0]->Title.'</p>
                            </div>
                            <div class="col-md-12">
                                Price: <b>' . $content_type[0]->Price . '</b>
                            </div>
                           
                        ';
                    }

                    // if($order_items[0]->content_type_id == 3) //only if content type text with images
                    // {
                        if($order_items[0]->content_type_sub_id != '')
                        {
                            $content_type_sub = $this->Content_type_sub_model->getJoinedData(false, 'ContentTypeSubID', "shape_content_type_sub.ContentTypeSubID = " . $order_items[0]->content_type_sub_id . " AND system_languages.ShortCode = '" . $this->language . "'");
                            
                            
                            $html .= '
                                <div class="col-md-12">
                                    <h4>Content Type Sub: </h4>
                                    <p>'.$content_type_sub[0]->Title.'</p>
                                </div>
                                <div class="col-md-12">
                                    Price: <b>' . $content_type_sub[0]->Price . '</b>
                                </div>';
                        }
                    // }
                    

                    $content_type_sub_image_id = $order_items[0]->content_type_sub_image_id;    
                    if($content_type_sub_image_id != '') //custom uploaded image
                    {
                        $content_type_sub_image = get_images_id($content_type_sub_image_id,'choco_box_printable_upload_image',false);
                        $html .= '<div class="col-md-12">
                                <img src="' . base_url($content_type_sub_image) . '">
                            </div>
                        ';
                    }

                    if($order_items[0]->content_text != '')
                    {
                        $html .= '
                            <div class="col-md-12">
                                <h4>Custom Text: </h4>
                                <p>'.$order_items[0]->content_text.'</p>
                            </div>';
                    }
                    
                    if($ribbon != '')
                    {
                        $ribbon = $this->Ribbon_model->getJoinedData(false, 'RibbonID', "ribbons.RibbonID = " . $ribbon . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                                <div class="col-md-12">
                                    <h4>Ribbon: </h4>
                                </div>
                                <div class="col-md-12">
                                    Price: <b>' . $ribbon[0]->RibbonPrice . '</b>
                                </div>
                                <div class="col-md-12">
                                    <img src="' . base_url($ribbon[0]->RibbonImage) . '">
                                </div>
                            ';
                    }
                    
                    if($order_items[0]->Wrapping != '')
                    {
                        $wrapping = $this->Wrapping_model->getJoinedData(false, 'WrappingID', "wrappings.WrappingID = " . $order_items[0]->Wrapping . " AND system_languages.ShortCode = '" . $this->language . "'");
                        $html .= '
                                <div class="col-md-12">
                                    <h4>Wrapping: </h4>
                                </div>
                                <div class="col-md-12">
                                    Price: <b>' . $wrapping[0]->WrappingPrice . '</b>
                                </div>
                                <div class="col-md-12">
                                    <img src="' . base_url($wrapping[0]->WrappingImage) . '">
                                </div>
                            ';
                    }
                }
                $html .= '<h4>Total Price</h4>';
                        $html .= '<p>'.$TempOrder[0]->TotalAmount.'</p>';
            }
            $html .= '</div><br><br>';
        }
        elseif($box_type == 'Choco Shape'){

        }
        echo $html;
        exit();
    }


}