<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shape extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'Nutrition_model',
            'Nutrition_product_model',
            'What_inside_model',
            'Tag_model',
            'Nutrition_shape_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'ShapeID';
        $this->data['Table'] = 'shapes';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language);

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['nutritions'] = $this->Nutrition_model->getAllJoinedData(false, 'NutritionID', $this->language);
        $this->data['tags'] = $this->Tag_model->getAllJoinedData(false, 'TagID', $this->language, 'tags.IsActive = 1');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');
        $this->data['nutritions'] = $this->Nutrition_model->getAllJoinedData(false, 'NutritionID', $this->language);
        $this->data['product_nutritions'] = $this->Nutrition_shape_model->shapeNutrition($id, $this->language);
        $this->data['product_nutrition_field'] = '';
        $this->data['product_nutritions_result'] = array();
        if (!empty($this->data['product_nutritions'])) {
            $this->data['product_nutritions_result'] = array_column($this->data['product_nutritions'], 'NutritionID');
            $this->data['product_nutritions_quanity'] = array_column($this->data['product_nutritions'], 'Quantity', 'NutritionID');


            foreach ($this->data['product_nutritions'] as $value) {
                $this->data['product_nutrition_field'] .= '<div class="col-md-6" id="sl_chocolate_text">
                            <div class="form-group label-floating">
                                <label class="control-label">&nbsp;</label>
                                <input type="text" class="form-control" name="sl_text_' . $value['Title'] . '" value="' . $value['Title'] . '">
                            </div>
                        </div>
                        
                        <div class="col-md-6" id="sl_chocolate_qty">
                            <div class="form-group label-floating">
                                <label class="control-label">Write Quantity</label>
                                <input type="text" class="form-control" name="NutritionQuantity[' . $value['NutritionID'] . ']" value="' . $value['Quantity'] . '">
                            </div>
                        </div><div class="clearfix"></div>';
            }
        }

        $this->data['tags'] = $this->Tag_model->getAllJoinedData(false, 'TagID', $this->language, 'tags.IsActive = 1');
        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                // $this->validate();
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');
        $this->form_validation->set_rules('dimension_length', 'Length', 'required');
        $this->form_validation->set_rules('dimension_width', 'Width', 'required');
        $this->form_validation->set_rules('dimension_width', 'Height', 'required');
        $this->form_validation->set_rules('dimension_weight', 'Weight', 'required');
        $this->form_validation->set_rules('inner_dimension_length', 'Inner Length', 'required');
        $this->form_validation->set_rules('inner_dimension_weight', 'Inner Weight', 'required');  

        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('minimum_order', 'Minimum Order', 'required');
        $this->form_validation->set_rules('maximum_order', 'Maximum Order', 'required');
        $this->form_validation->set_rules('price_per_kg', 'Price Per Kg', 'required');
        $this->form_validation->set_rules('processing_time', 'Processing Time', 'required');
        $this->form_validation->set_rules('price_per_pcs', 'Price Per PCS', 'required');
        $this->form_validation->set_rules('quantity_per_kg', 'Quantity Per KG', 'required');
        $this->form_validation->set_rules('individual_size', 'Individual Size', 'required');  
        


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        if (isset($_FILES['ShapeImage']["name"][0]) && $_FILES['ShapeImage']["name"][0] != '') {

            foreach ($_FILES['ShapeImage']["tmp_name"] as $key => $tmp_name) {

                $image_info = getimagesize($tmp_name);
                $image_width = $image_info[0];
                $image_height = $image_info[1];

                if($image_width != $image_height){
                    $errors['error'] =  'All images aspect ration should be same.';
                    $errors['success'] = false;
                    echo json_encode($errors);
                    exit;
                }
            }

        }

        if ((isset($_FILES['ShapeImage']["name"][0]) && $_FILES['ShapeImage']["name"][0] == '') || COUNT($_FILES['ShapeImage']['name']) > 7) {
            $errors['error'] = lang('please_choose_image');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;

        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        // $save_parent_data['quantity'] = $post_data['quantity'];
        $save_parent_data['dimension_length'] = $post_data['dimension_length'];
        $save_parent_data['dimension_width'] = $post_data['dimension_width'];
        $save_parent_data['dimension_height'] = $post_data['dimension_height'];
        $save_parent_data['dimension_weight'] = $post_data['dimension_weight'];
        $save_parent_data['inner_dimension_length'] = $post_data['inner_dimension_length'];
        $save_parent_data['inner_dimension_weight'] = $post_data['inner_dimension_weight'];

        $save_parent_data['MinimumOrder'] = $post_data['minimum_order'];
        $save_parent_data['MaximumOrder'] = $post_data['maximum_order'];
        $save_parent_data['PricePerKG'] = $post_data['price_per_kg'];
        $save_parent_data['OrderProcessingTime'] = $post_data['processing_time'];
        $save_parent_data['PricePerPcs'] = $post_data['price_per_pcs'];
        $save_parent_data['QuantityPerKG'] = $post_data['quantity_per_kg'];
        $save_parent_data['IndividualSize'] = $post_data['individual_size'];
        $save_parent_data['TagIDs'] = implode(',',$post_data['TagIDs']);
        
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['ShapeImage'] = uploadImage('Image', 'uploads/images/');
        }

        $insert_id = $this->$parent->save($save_parent_data);
        if (isset($_FILES['ShapeImage']["name"][0]) && $_FILES['ShapeImage']["name"][0] != '') {
            $this->uploadImage('ShapeImage', 'uploads/images/', $insert_id, 'shape', true);
        }
        if ($insert_id > 0) {


            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {


                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['Description'] = $post_data['description'];
                $save_child_data['Ingredients'] = $post_data['Ingredients'];
                $save_child_data['Specifications'] = $post_data['Specifications'];
                $save_child_data['Keywords'] = $post_data['Keywords'];
                $save_child_data['MetaTags'] = $post_data['MetaTags'];
                $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                $save_child_data['MetaDescription'] = $post_data['MetaDescription'];
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
            }

            if (!empty($post_data['NutritionQuantity']) && isset($post_data['NutritionQuantity'])) {
                $nutrition_data = array();
                foreach ($post_data['NutritionQuantity'] as $key => $value) {
                    if ($value != '') {
                        $nutrition_data[] = [
                            'ProductID' => $insert_id,
                            'NutritionID' => $key,
                            'Quantity' => ($value == '' ? NULL : $value)
                        ];
                    }

                }

                $this->Nutrition_shape_model->insert_batch($nutrition_data);

            }

            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {

        $errors = array();
        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $check = 0;
        if($post_data['Title'] != $post_data['Title_old'])
        {
            
            $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');
            $check =1;
        }
        if (isset($post_data['IsDefault']) && $post_data['IsDefault'] != 0) {
            $this->form_validation->set_rules('dimension_length', 'Length', 'required');
            $this->form_validation->set_rules('dimension_width', 'Width', 'required');
            $this->form_validation->set_rules('dimension_width', 'Height', 'required');
            $this->form_validation->set_rules('dimension_weight', 'Weight', 'required');
            $this->form_validation->set_rules('inner_dimension_length', 'Inner Length', 'required');
            $this->form_validation->set_rules('inner_dimension_weight', 'Inner Weight', 'required'); 
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('minimum_order', 'Minimum Order', 'required');
            $this->form_validation->set_rules('maximum_order', 'Maximum Order', 'required');
            $this->form_validation->set_rules('price_per_kg', 'Price Per Kg', 'required');
            $this->form_validation->set_rules('processing_time', 'Processing Time', 'required');
            $this->form_validation->set_rules('price_per_pcs', 'Price Per PCS', 'required');
            $this->form_validation->set_rules('quantity_per_kg', 'Quantity Per KG', 'required');
            $this->form_validation->set_rules('individual_size', 'Individual Size', 'required'); 
            $check = 1;
        }
        if($check == 1)
        {
            if ($this->form_validation->run() == FALSE) {
                $errors['error'] = validation_errors();
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
        }
        
        
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {

                
        

                $save_parent_data['dimension_length'] = $post_data['dimension_length'];
                $save_parent_data['dimension_width'] = $post_data['dimension_width'];
                $save_parent_data['dimension_height'] = $post_data['dimension_height'];
                $save_parent_data['dimension_weight'] = $post_data['dimension_weight'];
                $save_parent_data['inner_dimension_length'] = $post_data['inner_dimension_length'];
                $save_parent_data['inner_dimension_weight'] = $post_data['inner_dimension_weight'];

                $save_parent_data['MinimumOrder'] = $post_data['minimum_order'];
                $save_parent_data['MaximumOrder'] = $post_data['maximum_order'];
                $save_parent_data['PricePerKG'] = $post_data['price_per_kg'];
                $save_parent_data['OrderProcessingTime'] = $post_data['processing_time'];
                $save_parent_data['PricePerPcs'] = $post_data['price_per_pcs'];
                $save_parent_data['QuantityPerKG'] = $post_data['quantity_per_kg'];
                $save_parent_data['IndividualSize'] = $post_data['individual_size'];
                $tags = '';
                if(isset($post_data['TagIDs']))
                {   
                    $tags = implode(',',$post_data['TagIDs']);
                }
                $save_parent_data['TagIDs'] = $tags;
                
                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                    $save_parent_data['ShapeImage'] = uploadImage('Image', 'uploads/images/');
                }

                // if (isset($_FILES['shape_content_image_1']["name"][0]) && $_FILES['shape_content_image_1']["name"][0] != '') {
                //     $save_parent_data['shape_content_image_1'] = uploadImage('shape_content_image_1', 'uploads/images/');
                // }
                // if (isset($_FILES['shape_content_image_2']["name"][0]) && $_FILES['shape_content_image_2']["name"][0] != '') {
                //     $save_parent_data['shape_content_image_2'] = uploadImage('shape_content_image_2', 'uploads/images/');
                // }
                // if (isset($_FILES['shape_content_image_3']["name"][0]) && $_FILES['shape_content_image_3']["name"][0] != '') {
                //     $save_parent_data['shape_content_image_3'] = uploadImage('shape_content_image_3', 'uploads/images/');
                // }
                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {
                    if (isset($_FILES['ShapeImage']["name"][0]) && $_FILES['ShapeImage']["name"][0] != '') {
                        $this->uploadImage('ShapeImage', 'uploads/images/', $id, 'shape', true);
                    }
                }

                $this->$parent->update($save_parent_data, $update_by);
                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['description'];
                $save_child_data['Ingredients'] = $post_data['Ingredients'];
                $save_child_data['Specifications'] = $post_data['Specifications'];
                $save_child_data['Keywords'] = $post_data['Keywords'];
                $save_child_data['MetaTags'] = $post_data['MetaTags'];
                $save_child_data['MetaKeywords'] = $post_data['MetaKeywords'];
                $save_child_data['MetaDescription'] = $post_data['MetaDescription'];
                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);
                $update_by_nu = array();
                $update_by_nu['ProductID'] = base64_decode($post_data[$this->data['TableKey']]);
                $this->Nutrition_shape_model->delete($update_by_nu);
                if (!empty($post_data['NutritionQuantity']) && isset($post_data['NutritionQuantity'])) {
                    $nutrition_data = array();
                    foreach ($post_data['NutritionQuantity'] as $key => $value) {
                        if ($value != '') {
                            $nutrition_data[] = [
                                'ProductID' => base64_decode($post_data[$this->data['TableKey']]),
                                'NutritionID' => $key,
                                'Quantity' => ($value == '' ? NULL : $value)
                            ];
                        }
    
                    }
    
                    $this->Nutrition_shape_model->insert_batch($nutrition_data);
    
                }

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['description'];
                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['description'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->save($save_child_data);
                }


            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}