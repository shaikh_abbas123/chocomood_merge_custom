<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerGroup extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('Customer_group_model');
        $this->load->Model('Order_model');
        $this->load->Model('Customer_group_member_model');
        $this->data['language'] = $this->language;
    }


    public function index()
    {
        $this->data['view'] = 'backend/customer_group/manage';
        $this->data['results'] = $this->Customer_group_model->getAll(false);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(83, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['view'] = 'backend/customer_group/add';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(83, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['group_detail'] = $this->Customer_group_model->get($id, false, 'CustomerGroupID');

        if (!$this->data['group_detail']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        if ($this->data['group_detail']->CustomerGroupType == 'Orders') {
            $this->data['group_members'] = $this->Customer_group_member_model->getMembersWithOrders($id);
        } elseif ($this->data['group_detail']->CustomerGroupType == 'Purchases') {
            $this->data['group_members'] = $this->Customer_group_member_model->getMembersWithPurchases($id);
        }

        $this->data['view'] = 'backend/customer_group/edit';
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function view($id)
    {

        $this->data['group_detail'] = $this->Customer_group_model->get($id, false, 'CustomerGroupID');

        if ($this->data['group_detail']->CustomerGroupType == 'Orders') {
            $this->data['group_members'] = $this->Customer_group_member_model->getMembersWithOrders($id);
        } elseif ($this->data['group_detail']->CustomerGroupType == 'Purchases') {
            $this->data['group_members'] = $this->Customer_group_member_model->getMembersWithPurchases($id);
        }elseif($this->data['group_detail']->CustomerGroupType == 'AllUsers') {
            $this->data['group_members'] = $this->Customer_group_member_model->getMembersWithOrders($id);
        }
        $this->data['view'] = 'backend/customer_group/view';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('CustomerGroupTitle', 'Customer Group Title', 'required|is_unique[customer_groups.CustomerGroupTitle]');
        $post_data = $this->input->post();
        if($post_data['CustomerGroupType'] != 'AllUsers'){
            $this->form_validation->set_rules('MinimumValue', 'Minimum Value', 'required');
            $this->form_validation->set_rules('MaximumValue', 'Maximum Value', 'required');
        }
        
        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {
        if (!checkUserRightAccess(83, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        //print_rm($post_data);
        $CustomerIds = $post_data['CustomerIds'];
        unset($post_data['form_type']);
        unset($post_data['CustomerIds']);
        $CustomerIds = explode(',', $CustomerIds);
        $post_data['GroupMembersCount'] = count($CustomerIds);
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $post_data['UpdatedAt'] = date('Y-m-d H:i:s');


        $insert_id = $this->Customer_group_model->save($post_data);
        if ($insert_id > 0) {
            foreach ($CustomerIds as $customerId) {
                $customer_group_member_data['CustomerGroupID'] = $insert_id;
                $customer_group_member_data['UserID'] = $customerId;
                $this->Customer_group_member_model->save($customer_group_member_data);
            }
            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

    }

    private function update()
    {
        if (!checkUserRightAccess(83, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $CustomerGroupID = $post_data['CustomerGroupID'];
        unset($post_data['form_type']);
        unset($post_data['CustomerGroupID']);
        $post_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $this->Customer_group_model->update($post_data, array('CustomerGroupID' => $CustomerGroupID));
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;

    }


    private function delete()
    {

        if (!checkUserRightAccess(83, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

        $deleted_by['CustomerGroupID'] = $this->input->post('id');
        $this->Customer_group_member_model->delete($deleted_by);
        $this->Customer_group_model->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function showRecords()
    {
        $post_data = $this->input->post();
        if (($post_data['FromDate'] != '' && $post_data['ToDate'] == '') || ($post_data['FromDate'] == '' && $post_data['ToDate'] != '')) {
            $respose['status'] = false;
            $respose['message'] = "Please select From date and To date to proceed with this";
            echo json_encode($respose);
            exit;
        }
        $users = false;
        if ($post_data['CustomerGroupType'] == 'Orders') {
            $unit = ' Order(s)';
            $users = $this->Order_model->getUsersWithOrdersRange($post_data['MinimumValue'], $post_data['MaximumValue'], $post_data['FromDate'], $post_data['ToDate']);
        } elseif ($post_data['CustomerGroupType'] == 'Purchases') {
            $unit = ' SAR';
            $users = $this->Order_model->getUsersWithPurchasesRange($post_data['MinimumValue'], $post_data['MaximumValue'], $post_data['FromDate'], $post_data['ToDate']);
        } elseif($post_data['CustomerGroupType'] == 'AllUsers'){
            $unit = ' Order(s)';
            $users = $this->Order_model->getCustomeUsers();
            
        }

        $html = "<table id=\"datatables\" class=\"table table-striped table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User ID</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>User Mobile</th>
                            <th>Count / Sum</th>
                        </tr>
                    </thead>
                    <tbody>";
        if ($users) {
            foreach ($users as $user) {
                $html .= "<tr>
                            <td><input type='checkbox' name='custom_group_users' value='" . $user->UserID . "' checked></td>
                            <td>" . $user->UserID . "</td>
                            <td>" . $user->FullName . "</td>
                            <td>" . $user->Email . "</td>
                            <td>" . $user->Mobile . "</td>
                            <td>" . $user->count_sum . $unit . "</td>
                          </tr>";
            }
            $show_group_create_button = true;
        } else {
            $html .= "<tr><td colspan='6' style='text-align: center;'>No records found</td></tr>";
            $show_group_create_button = false;
        }
        $html .= "</tbody></table>";
        $respose['status'] = true;
        $respose['html'] = $html;
        $respose['show_group_create_button'] = $show_group_create_button;
        echo json_encode($respose);
        exit;
    }


}