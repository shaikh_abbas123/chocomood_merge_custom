<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        $this->load->model('Product_model');
        $this->load->model('Order_model');
        $this->load->model('User_model');
        $this->data['language'] = $this->language;


    }

    public function index()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['view'] = 'backend/report/reports_sections';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function best_sellers()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getProducts('products.PurchaseCount > 0', 'EN', false, 0, $sort_by = 'products.PurchaseCount', $sort_as = 'DESC');
        $this->data['view'] = 'backend/report/best_sellers';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function products_in_cart()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getProductsInCart();
        //echo $this->db->last_query();
        //print_rm($this->data['results']);
        $this->data['view'] = 'backend/report/products_in_cart';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function products_reviews()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getProductsReviews();
        
        $this->data['view'] = 'backend/report/products_reviews';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function product_reviews($product_id)
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getReviews('products.ProductID = '.$product_id);
        
        $this->data['view'] = 'backend/report/products_reviews_detail';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function customers_reviews_count()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getCountCustomerReviews();
        //print_rm($this->data['results']);
        
        $this->data['view'] = 'backend/report/customers_reviews';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function customer_reviews($user_id)
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getReviews('product_reviews.UserID = '.$user_id);
        
        $this->data['view'] = 'backend/report/customer_reviews_detail';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function out_of_stock()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['results'] = $this->Product_model->getProducts("products.OutOfStock = 1");
        $this->data['view'] = 'backend/report/out_of_stock';
        $this->load->view('backend/layouts/default', $this->data);
    }


     public function customer_orders()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";
            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }
        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
        }else{
            $this->data['Interval'] = 'Year';
        }
        $this->data['results'] = $this->Order_model->getTotalOrdersOfCustomer($where,$this->data['Interval']);
        //echo $this->db->last_query();exit;
       // echo $this->data['Interval'];exit;

        $this->data['view'] = 'backend/report/customer_orders';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function new_account_orders()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "WHERE DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            if($this->data['Interval'] == 'Year'){
                $group_by = 'GROUP BY Year(orders.CreatedAt)';
            }else if($this->data['Interval'] == 'Month'){
                $group_by = 'GROUP BY Month(orders.CreatedAt),Year(orders.CreatedAt)';
            }else if($this->data['Interval'] == 'Day'){
                $group_by = 'GROUP BY Day(orders.CreatedAt),Month(orders.CreatedAt),Year(orders.CreatedAt)';
            }

        }else{
            $this->data['Interval'] = 'Year';
            $group_by = 'GROUP BY Year(orders.CreatedAt)';
        }
        $this->data['results'] = $this->Order_model->getTotalNewAccountOrdersOfCustomers($where,$group_by);
        //print_rm($this->data['results']);
       //echo $this->db->last_query();exit;
       // echo $this->data['Interval'];exit;

        $this->data['view'] = 'backend/report/customer_new_account_orders';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function orders()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        $this->data['results'] = $this->Order_model->getOrdersReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/orders';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function tax()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        $this->data['results'] = $this->Order_model->getOrdersReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/tax';
        $this->load->view('backend/layouts/default', $this->data);
    }

     public function shipping()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        $this->data['results'] = $this->Order_model->getOrdersReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/shipping';
        $this->load->view('backend/layouts/default', $this->data);
    }

     public function coupons()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        $this->data['results'] = $this->Order_model->getCouponReport($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/coupons';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function order_products()
    {
        if (!checkUserRightAccess(74, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }

        $post_data = $this->input->post();
        $where = false;
        $this->data['from'] = '';
        $this->data['to'] = '';
        if(isset($post_data['from']) && $post_data['from'] != '' && isset($post_data['to']) && $post_data['to'] != ''){
            $where = "DATE(orders.CreatedAt) BETWEEN '".date('Y-m-d',strtotime($post_data['from']))."' AND '".date('Y-m-d',strtotime($post_data['to']))."'";

            $this->data['from'] = $post_data['from'];
            $this->data['to'] = $post_data['to'];
        }

        if(isset($post_data['ShowBy'])){
            $this->data['Interval'] = $post_data['ShowBy'];
            

        }else{
            $this->data['Interval'] = 'Year';
            
        }
        $this->data['results'] = $this->Order_model->getProductOrderCount($where,$this->data['Interval']);
        

        $this->data['view'] = 'backend/report/order_products';
        $this->load->view('backend/layouts/default', $this->data);
    }

    /*public function index()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/dashboard'));
        }
        $this->data['view'] = 'backend/customer_group/add';
        $this->load->view('backend/layouts/default', $this->data);
    }*/


}