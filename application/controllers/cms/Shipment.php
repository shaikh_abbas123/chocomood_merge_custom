<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipment extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->data['language'] = $this->language;


    }

    public function index()
    {
        $this->data['view'] = 'backend/shipment/manage';
        $this->data['driver_shipments'] = $this->Order_model->getOrders("orders.DriverID  > 0 AND orders.ShippedThroughApi = 0");
        $this->data['api_shipments'] = $this->Order_model->getOrders("orders.DriverID  = 0 AND orders.ShippedThroughApi = 1");
        $this->load->view('backend/layouts/default', $this->data);
    }


}