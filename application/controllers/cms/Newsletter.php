<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->data['language'] = $this->language;
    }


    public function index()
    {   ini_set('memory_limit', '128M');

        $this->data['view'] = 'backend/newsletter/manage';
        $subscriber = get_mailchimp_subscribers();
        $total = ceil($subscriber['total'] /1000) ;
        unset($subscriber['total']);
        $this->data['results'] = $subscriber;
        if($total > 1){
             for($i=2; $i<=$total; $i++){
                 $offset = ($i - 1) * 1000;
                 $count  = 1000;
                 $subscriber = get_mailchimp_subscribers($count, $offset);
                 unset($subscriber['total']);
                 $this->data['results'] = array_merge($this->data['results'], $subscriber);
             }
         }
        $this->load->view('backend/layouts/default', $this->data);
    }
    
    public function get_data()
    {
        $subscriber = get_mailchimp_subscribers();
        $array = array();
        foreach($subscriber as $v)
        {
            $array_temp = array();
            $array_temp['email'] = @$v->email_address;
            array_push($array,$array_temp);
        }
        echo json_encode($array);
        
    }
   


}