<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            ucfirst($this->router->fetch_class()).'_text_model',
            'City_model',
            'District_model',
            'Payment_method_model',
            'Shipping_method_model',
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['TableKey'] = 'StoreID';
                $this->data['Table'] = 'stores';
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          $child                              = $this->data['Child_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
          $this->data['results'] = $this->$parent->getAllStores($this->language);
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {
         if(!checkUserRightAccess(61,$this->session->userdata['admin']['UserID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        
        
        $this->data['payment_methods'] = $this->Payment_method_model->getAll();
        $this->data['shipping_methods'] = $this->Shipping_method_model->getAll();
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        $this->data['cities'] = $this->City_model->getAllCities($this->language, false,true);
        $this->data['districts'] = $this->District_model->getAllJoinedData(false, 'DistrictID', $this->language, 'districts.CityID = '.$this->data['cities'][0]->CityID.' AND districts.IsActive = 1');
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        if(!checkUserRightAccess(61,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    
        
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
       $this->data['payment_methods'] = $this->Payment_method_model->getAll();
       $this->data['shipping_methods'] = $this->Shipping_method_model->getAll();
       $this->data['cities'] = $this->City_model->getAllCities($this->language, false,true);
        $this->data['districts'] = $this->District_model->getAllJoinedData(false, 'DistrictID', $this->language, 'districts.CityID = '.$this->data['result'][0]->CityID.' AND districts.IsActive = 1');
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique['.$this->data['Table'].'_text.Title]');
        $this->form_validation->set_rules('Address', 'Address', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        
        if(!checkUserRightAccess(61,$this->session->userdata['admin']['UserID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
        $save_parent_data['phone']       = $post_data['phone'];
        $save_parent_data['Latitude']       = $post_data['Latitude'];
        $save_parent_data['Longitude']      = $post_data['Longitude'];
        $save_parent_data['VatNo']          = $post_data['VatNo'];
        $save_parent_data['WorkingHoursSaturdayToThursdayFrom']          = $post_data['WorkingHoursSaturdayToThursdayFrom'];
        $save_parent_data['WorkingHoursSaturdayToThursdayTo']          = $post_data['WorkingHoursSaturdayToThursdayTo'];
        $save_parent_data['WorkingHoursFridayFrom']          = $post_data['WorkingHoursFridayFrom'];
        $save_parent_data['WorkingHoursFridayTo']          = $post_data['WorkingHoursFridayTo'];
         $save_parent_data['WorkingHoursSaturdayToThursdayFrom1']          = $post_data['WorkingHoursSaturdayToThursdayFrom1'];
        $save_parent_data['WorkingHoursSaturdayToThursdayTo1']          = $post_data['WorkingHoursSaturdayToThursdayTo1'];
        $save_parent_data['WorkingHoursFridayFrom1']          = $post_data['WorkingHoursFridayFrom1'];
        $save_parent_data['WorkingHoursFridayTo1']          = $post_data['WorkingHoursFridayTo1'];
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['isCustomize']       = (isset($post_data['isCustomize']) ? 1 : 0 );

        $save_parent_data['CityID']         = $post_data['CityID'];
        if(!empty($post_data['DistrictID'])){
            $save_parent_data['DistrictID']     = implode(',',$post_data['DistrictID']);
        }
        if(!empty($post_data['ShippingID'])){
            $save_parent_data['ShippingID']     = implode(',',$post_data['ShippingID']);
        }
        if(!empty($post_data['PaymentID'])){
            $save_parent_data['PaymentID']     = implode(',',$post_data['PaymentID']);
        }
        
        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];

        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {




                // $default_lang = getDefaultLanguage();
                $system_languages = getSystemLanguages();
                foreach ($system_languages as $system_language) {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Address'] = $post_data['Address'];
                    //$save_child_data['WorkingHours'] = $post_data['WorkingHours'];
                    $save_child_data['DeliveryTime'] = $post_data['DeliveryTime'];
                    $save_child_data[$this->data['TableKey']] = $insert_id;
                    $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                    $this->$child->save($save_child_data);
                }
                
                
                
               
              
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
        private function update()
    {
        


                if(!checkUserRightAccess(61,$this->session->userdata['admin']['UserID'],'CanEdit')){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
                $child                              = $this->data['Child_model'];
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    
        
                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $errors['redirect'] = true;
                   $errors['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }

            
                
            unset($post_data['form_type']);
        $save_parent_data                   = array();
                $save_child_data                    = array();
                if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){
                    $save_parent_data['phone']       = $post_data['phone'];
                   $save_parent_data['VatNo']      = $post_data['VatNo'];
                   $save_parent_data['CityID']         = $post_data['CityID'];
                    if(!empty($post_data['DistrictID'])){
                        $save_parent_data['DistrictID']     = implode(',',$post_data['DistrictID']);
                    }
                    if(!empty($post_data['ShippingID'])){
                        $save_parent_data['ShippingID']     = implode(',',$post_data['ShippingID']);
                    }
                    if(!empty($post_data['PaymentID'])){
                        $save_parent_data['PaymentID']     = implode(',',$post_data['PaymentID']);
                    }
                    $save_parent_data['Latitude']      = $post_data['Latitude'];
                    $save_parent_data['WorkingHoursSaturdayToThursdayFrom']          = $post_data['WorkingHoursSaturdayToThursdayFrom'];
                    $save_parent_data['WorkingHoursSaturdayToThursdayTo']          = $post_data['WorkingHoursSaturdayToThursdayTo'];
                    $save_parent_data['WorkingHoursFridayFrom']          = $post_data['WorkingHoursFridayFrom'];
                    $save_parent_data['WorkingHoursFridayTo']          = $post_data['WorkingHoursFridayTo'];

                    $save_parent_data['WorkingHoursSaturdayToThursdayFrom1']          = $post_data['WorkingHoursSaturdayToThursdayFrom1'];
                    $save_parent_data['WorkingHoursSaturdayToThursdayTo1']          = $post_data['WorkingHoursSaturdayToThursdayTo1'];
                    $save_parent_data['WorkingHoursFridayFrom1']          = $post_data['WorkingHoursFridayFrom1'];
                    $save_parent_data['WorkingHoursFridayTo1']          = $post_data['WorkingHoursFridayTo1'];
                    $save_parent_data['Longitude']      = $post_data['Longitude'];
                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                    $save_parent_data['isCustomize']       = (isset($post_data['isCustomize']) ? 1 : 0 );
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    
                    
                    
                    $this->$parent->update($save_parent_data,$update_by);
                    $save_child_data['Title']                        = $post_data['Title'];
                    $save_child_data['Address']                        = $post_data['Address'];
                   // $save_child_data['WorkingHours']                        = $post_data['WorkingHours'];
                    $save_child_data['DeliveryTime']                        = $post_data['DeliveryTime'];
                    $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                    
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $this->$child->update($save_child_data,$update_by);
                    
                }else{
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['Title']                        = $post_data['Title'];
                        $save_child_data['Address']                        = $post_data['Address'];
                        //$save_child_data['WorkingHours']                        = $post_data['WorkingHours'];
                        $save_child_data['DeliveryTime']                        = $post_data['DeliveryTime'];
                        
                        $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];



                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                        
                        $save_child_data['Title']                        =  $post_data['Title'];
                        $save_child_data['Address']                        = $post_data['Address'];
                        //$save_child_data['WorkingHours']                        = $post_data['WorkingHours'];
                        $save_child_data['DeliveryTime']                        = $post_data['DeliveryTime'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                        $save_child_data['CreatedAt']                    =  $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['CreatedBy']                    =  $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];



                        $this->$child->save($save_child_data);
                    }
                    
                    
                    
                    
                }
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
                
                
              
              
           
        
        


        
    }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   
    
    private function delete(){
        
         if(!checkUserRightAccess(61,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }

    public function getStoresForCity()
    {
        $html = '<option value="" selected>Please select store</option>';
        $CityID = $this->input->post('CityID');
        $stores = $this->Store_model->getJoinedData(false, 'StoreID', "stores.CityID = ".$CityID);
        if ($stores && count($stores) > 0) {
            foreach ($stores as $store) {
                $html .= '<option value="'.$store->StoreID.'">'.$store->Title.'</option>';
            }
        }
        $response_arr['html'] = $html;
        echo json_encode($response_arr);
        exit();
    }



    public function getStore()
    {
        $CityID = $this->input->post('CityID');

        $parent                             = $this->data['Parent_model'];
        
        $where = 'stores.CityID = '.$CityID;
        
        $stores = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language,$where);

        $response = array();
        $options = '<option value="">'.lang("choose_store").'</option>';

        foreach ($stores as $key => $value) {
            $options .= '<option value="'.$value->StoreID.'">'.$value->Title.'</option>';
        }

        if(isset($_POST['District'])){
            $options2 = '<option value="">'.lang("districts").'</option>';
            $where = 'districts.CityID = '.$CityID;
        
             $districts = $this->District_model->getAllJoinedData(false, 'DistrictID', $this->language,$where);

            foreach ($districts as $key => $value2) {
                $options2 .= '<option value="'.$value2->DistrictID.'">'.$value2->Title.'</option>';
            }
            $response['html_district'] = $options2;

        }

        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }

    
    
    

}