<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchTag extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        $this->load->model('Search_tag_model');
        $this->data['language'] = $this->language;
    }


    public function index()
    {
        $this->data['view'] = 'backend/search_tag/manage';
        $this->data['results'] = $this->Search_tag_model->getSearchTags();
        // dump($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

}