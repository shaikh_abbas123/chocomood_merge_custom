<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactRequest extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Contact_request_model');
    }


    public function index()
    {
        $this->data['language'] = $this->language;
        $this->data['view'] = 'backend/contact_requests/manage';
        $this->data['feedbacks'] = $this->Contact_request_model->getMultipleRows(array('Subject' => 'Feedback'));
        $this->data['careers'] = $this->Contact_request_model->getMultipleRows(array('Subject' => 'Career'));
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;
        }
    }

    private function delete()
    {

        if (!checkUserRightAccess(32, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $deleted_by['ContactRequestID'] = $this->input->post('id');
        $this->Contact_request_model->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');
        echo json_encode($success);
        exit;
    }

}