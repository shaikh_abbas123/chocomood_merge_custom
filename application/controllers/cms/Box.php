<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Box extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'Box_category_model',
            'Box_category_text_model',
            'Product_model',
            'Product_text_model',

        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'BoxID';
        $this->data['Table'] = 'boxes';


    }


    public function index() //choco message
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['for'] = "Choco Message";
        $this->data['add'] = "add";
        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language,'boxes.BoxFor = "Choco Message"');
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function choco_box_box()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['for'] = "Choco Box";
        $this->data['add'] = "add_choco_box_box";
        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language,'boxes.BoxFor = "Choco Box"');
        // print_rm($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add() //choco message
    {
        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['for'] = "Choco Message";
        $this->data['box_categories'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID = 0');
        $this->data['box_types'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID != 0');
         $this->data['products'] = $this->Product_model->getAllJoinedData(false, "ProductID", $this->language, 'products.IsCustomizedProduct = 1 AND products.IsActive = 1');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add_choco_box_box()
    {
        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['for'] = "Choco Box";
        $this->data['box_categories'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID = 0  AND box_categories.BoxCategoryFor = "Choco Box"');
        $this->data['box_types'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID != 0');
        $this->data['products'] = $this->Product_model->getAllJoinedData(false, "ProductID", $this->language, 'products.IsCustomizedProduct = 1 AND products.IsActive = 1');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $inside_products = array();
        if ($this->data['result'][0]->BoxFor == 'Choco Box') {
            $product_ids = explode(',', $this->data['result'][0]->BoxInside);
            // var_dump($product_ids);
            
            foreach ($product_ids as $v) {
                $product_data = $this->Product_model->getAllJoinedData(false, 'ProductID', $this->language, "products.ProductID = '".$v."'");
                if ($product_data) {
                    array_push($inside_products, $product_data);
                }
            }
            $this->data['inside_products'] = $inside_products;
            $this->data['box_categories'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID = 0  AND box_categories.BoxCategoryFor = "Choco Box"');
            $this->data['box_sub_categories'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.BoxCategoryID = "'.$this->data['result'][0]->BoxCategory.'"  AND box_categories.BoxCategoryFor = "Choco Box"');
            $this->data['box_sub_categories_all'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.BoxCategoryFor = "Choco Box" AND box_categories.ParentID = "'.$this->data['box_sub_categories'][0]->ParentID.'"');
            $this->data['selected_box_cat_parent'] = $this->data['box_sub_categories'][0]->ParentID;
            $this->data['selected_box_cat_sub'] = $this->data['box_sub_categories'][0]->BoxCategoryID;
            $this->data['box_types'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID != 0');
            $this->data['products'] = $this->Product_model->getAllJoinedData(false, "ProductID", $this->language, 'products.IsCustomizedProduct = 1 AND products.IsActive = 1');
        }
        $this->data['inside_products'] = $inside_products;
        // $this->data['box_categories'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID = 0  AND box_categories.BoxCategoryFor = "Choco Box"');
        // $this->data['box_sub_categories'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.BoxCategoryID = "'.$this->data['result'][0]->BoxCategory.'"  AND box_categories.BoxCategoryFor = "Choco Box"');
        // // var_dump($this->data['result']);
        // $this->data['box_sub_categories_all'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.BoxCategoryFor = "Choco Box" AND box_categories.ParentID = "'.$this->data['box_sub_categories'][0]->ParentID.'"');
        // $this->data['selected_box_cat_parent'] = $this->data['box_sub_categories'][0]->ParentID;
        // $this->data['selected_box_cat_sub'] = $this->data['box_sub_categories'][0]->BoxCategoryID;
        // $this->data['box_types'] = $this->Box_category_model->getAllJoinedData(false, "BoxCategoryID", $this->language, 'box_categories.ParentID != 0');
        // $this->data['products'] = $this->Product_model->getAllJoinedData(false, "ProductID", $this->language, 'products.IsCustomizedProduct = 1 AND products.IsActive = 1');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');
        $this->form_validation->set_rules('BoxPrice', lang('price'), 'required');
        $this->form_validation->set_rules('BoxSpace', 'Box Capacity', 'required');
        $for = $this->input->post('for');
        if($for == 'Choco Box')
        {
            $this->form_validation->set_rules('BoxType', 'Box Category', 'required');
            $this->form_validation->set_rules('weight', 'Weight', 'required');
            $this->form_validation->set_rules('no_of_row', 'No.of Rows', 'required');
            $this->form_validation->set_rules('no_of_choclate_in_row', 'No.of Chocolate in Row', 'required');
            $this->form_validation->set_rules('height', 'Height', 'required');
            $this->form_validation->set_rules('orderProcessing', 'Order Processing', 'required');
            $this->form_validation->set_rules('MinOrder', 'Minimum Order', 'required');
            $this->form_validation->set_rules('BoxInside[]', 'Chocolates', 'required');
            
        }

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {
        // print_rm($_POST);
        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }
        if($post_data['for'] == 'Choco Box')
        {
            if($post_data['BoxCategory'] != '')
            {
                if(isset($post_data['BoxType']))
                {
                    if($post_data['BoxType'] != '')
                    {
                        $save_parent_data['BoxCategory'] = $post_data['BoxType'];    
                    }
                    else
                    {
                        $save_parent_data['BoxCategory'] = $post_data['BoxCategory'];    
                    }
                }
                else
                    {
                        $save_parent_data['BoxCategory'] = $post_data['BoxCategory'];    
                    }
            }

            if(!empty($post_data['BoxInside'])){
                $save_parent_data['BoxInside']     = implode(',',$post_data['BoxInside']);
            }
            
        }
        
        $save_parent_data['weight'] = @$post_data['weight'];
        $save_parent_data['MinOrder'] = @$post_data['MinOrder'];
        $save_parent_data['orderProcessing'] = @$post_data['orderProcessing'];
        $save_parent_data['no_of_choclate_in_row'] = @$post_data['no_of_choclate_in_row'];
        $save_parent_data['height'] = @$post_data['height'];
        $save_parent_data['no_of_row'] = @$post_data['no_of_row'];
        $save_parent_data['PriceType'] = @$post_data['PriceType'];
        $save_parent_data['BoxFor'] = @$post_data['for'];
        $save_parent_data['BoxPrice'] = $post_data['BoxPrice'];
        $save_parent_data['BoxSpace'] = $post_data['BoxSpace'];
        if (isset($_FILES['BoxImage']["name"][0]) && $_FILES['BoxImage']["name"][0] != '') {
            $save_parent_data['BoxImage'] = uploadImage('BoxImage', 'uploads/images/');
        }

        if (isset($_FILES['BoxImageInside']["name"][0]) && $_FILES['BoxImageInside']["name"][0] != '') {
            $save_parent_data['BoxImageInside'] = uploadImage('BoxImageInside', 'uploads/images/');
        }
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {


            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {


                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];
                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
            }


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {


        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($post_data['Title'] != $post_data['Title_old'])
        {
            $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');
        }
        if (isset($post_data['IsDefault']) && $post_data['IsDefault'] != 0) {
        $this->form_validation->set_rules('BoxPrice', lang('price'), 'required');
        }
        if($post_data['for'] == 'Choco Box')
        {
            $this->form_validation->set_rules('BoxCategory', 'Box Category', 'required');
            $this->form_validation->set_rules('weight', 'Weight', 'required');
            $this->form_validation->set_rules('no_of_row', 'No.of Rows', 'required');
            $this->form_validation->set_rules('no_of_choclate_in_row', 'No.of Chocolate in Row', 'required');
            $this->form_validation->set_rules('height', 'Height', 'required');
            $this->form_validation->set_rules('orderProcessing', 'Order Processing', 'required');
            $this->form_validation->set_rules('MinOrder', 'Minimum Order', 'required');
            $this->form_validation->set_rules('BoxInside[]', 'Chocolates', 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } 

        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {

                if(!empty(@$post_data['BoxInside'])){
                    $save_parent_data['BoxInside']     = implode(',',$post_data['BoxInside']);
                }

                if($post_data['for'] == 'Choco Box')
                {
                    if($post_data['BoxCategory'] != '')
                    {
                        if(isset($post_data['BoxType']))
                        {
                            if($post_data['BoxType'] != '')
                            {
                                $save_parent_data['BoxCategory'] = $post_data['BoxType'];    
                            }else
                            {
                                $save_parent_data['BoxCategory'] = $post_data['BoxCategory'];    
                            }
                        }
                        else
                            {
                                $save_parent_data['BoxCategory'] = $post_data['BoxCategory'];    
                            }
                    }

                    if(!empty($post_data['BoxInside'])){
                        $save_parent_data['BoxInside']     = implode(',',$post_data['BoxInside']);
                    }
                    
                }
                $save_parent_data['weight'] = @$post_data['weight'];
                $save_parent_data['MinOrder'] = @$post_data['MinOrder'];
                $save_parent_data['orderProcessing'] = @$post_data['orderProcessing'];
                $save_parent_data['no_of_choclate_in_row'] = @$post_data['no_of_choclate_in_row'];
                $save_parent_data['height'] = @$post_data['height'];
                $save_parent_data['no_of_row'] = @$post_data['no_of_row'];
                $save_parent_data['PriceType'] = @$post_data['PriceType'];
                $save_parent_data['BoxPrice'] = $post_data['BoxPrice'];
                $save_parent_data['BoxSpace'] = $post_data['BoxSpace'];
                if (isset($_FILES['BoxImage']["name"][0]) && $_FILES['BoxImage']["name"][0] != '') {
                    $save_parent_data['BoxImage'] = uploadImage('BoxImage', 'uploads/images/');
                }

                if (isset($_FILES['BoxImageInside']["name"][0]) && $_FILES['BoxImageInside']["name"][0] != '') {
                    $save_parent_data['BoxImageInside'] = uploadImage('BoxImageInside', 'uploads/images/');
                }
                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);


                $this->$parent->update($save_parent_data, $update_by);
                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];

                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['Description'];


                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Description'] = $post_data['Description'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $this->$child->save($save_child_data);
                }


            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(86, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}