<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->data['site_setting'] = $this->getSiteSetting();

    }

    public function test()
    {
        $res = sendSms('+923134554777', 'This is a test message here', true);
        dump($res);
    }

    public function index()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }

        redirect(base_url('cms/account/login'));
    }

    //User Login
    public function login()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }
        $this->data['view'] = 'backend/login';
        $this->load->view('backend/login', $this->data);

    }


    public function checkLogin()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data);

        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email or password incorrect.';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = 'Login Successfully';
            $data['error'] = false;
            $data['redirect'] = true;
            $data['url'] = 'cms/dashboard';
            echo json_encode($data);
            exit();
        }
    }

    private function loginValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        $post_data['Password'] = md5($post_data['Password']);

        $user = $this->User_model->getUserData($post_data, $this->language);

        if (!empty($user)) {

            if($user['RoleID'] == 5)
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'You can\'t be login from here';
                echo json_encode($data);
                exit();

            }
            $this->session->set_userdata('admin', $user);
            //$this->updateUserLoginStatus();
            $this->updateOnlineStatus();
            return true;
        } else {
            return false;
        }

    }


    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->updateOnlineStatus('Offline');
        $this->session->unset_userdata('admin');
        redirect($this->config->item('base_url') . 'cms/account/login');

    }

    public function updateOnlineStatus($status = 'Online')
    {
        $update['OnlineStatus'] = $status;
        $update['UpdatedAt'] =  date('Y-m-d H:i:s');
        $update_by['UserID'] = $this->session->userdata['admin']['UserID'];
        $this->User_model->update($update, $update_by);
    }

    public function suspendUser()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['RoleID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }

        $update['IsActive'] = 0;
        $update['UpdatedAt'] =  date('Y-m-d H:i:s');
        $update_by['UserID'] = $this->input->post('id');
        $this->User_model->update($update, $update_by);

        $success['error'] = false;
        $success['success'] = lang('suspended_successfully');

        echo json_encode($success);
        exit;
    }

    public function unsuspendUser()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['RoleID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }

        $update['IsActive']  = 1;
        $update['UpdatedAt'] =  date('Y-m-d H:i:s');
        $update_by['UserID'] = $this->input->post('id');
        $this->User_model->update($update, $update_by);

        $success['error'] = false;
        $success['success'] = lang('suspended_successfully');

        echo json_encode($success);
        exit;
    }

}