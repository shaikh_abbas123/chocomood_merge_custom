<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Order_model');
        $this->data['language'] = $this->language;


    }

    public function index()
    {
        $this->data['invoices'] = $this->Order_model->getOrders("orders.isCustomize= 0");
        $this->data['view'] = 'backend/invoice/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function download($OrderID)
    {
        $order_html = get_order_invoice($OrderID,'invoice',1);
        generate_pdf($order_html, $OrderID,1);
    }

    public function custom_order_invoice()
    {
        $this->data['invoices'] = $this->Order_model->getOrders("orders.isCustomize= 1");
        $this->data['view']     = 'backend/invoice/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }
    public function view($orderId)
    {
        $CI = &get_Instance();
        $CI->load->model('Order_model');
        $CI->load->model('Order_item_model');
        $CI->load->model('User_address_model');
        $orderInfo      = $CI->Order_model->getOrders("orders.OrderID = $orderId");
        $orderDetails   = $CI->Order_item_model->getOrdersItems($orderId);
        // return $order_details;
        // print_r($orderInfo[0]->isCustomize);
        $itemDetailStr = array();
        foreach ($orderDetails as $orderDetail ) {
            $detailStr      = "";
            $productTile    = "";
            if($orderDetail->ProductID==0){
                $itemDetail   = $CI->Order_item_model->getSubItem($orderDetail->OrderItemID)[0];
                $subItems = array_count_values(explode(",",$itemDetail->CustomizedOrderProductIDs));
                $productTile = "<b>".$itemDetail->ItemType."</b>";
                $detailStr = "<ul style='list-style-type: none;'>";
                foreach ($subItems as $itemId => $count) {
                    $subItemDetial = $CI->Order_item_model->getSubItemDetial($itemId, $itemDetail->ItemType)[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($subItemDetial->CharacterImage)."' style='width:35px;height:auto' /> <span>".$count."x </span> </li>";
                }
                $itemDetail   = $CI->Order_item_model->getSubItem($orderDetail->OrderItemID)[0];
                if($orderDetail->Ribbon != "" && $orderDetail->Ribbon != "0"){
                    $sideItem = $CI->Order_item_model->getSideItemDetail($orderDetail->Ribbon, "ribbons", "RibbonID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->RibbonImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->CustomizedBoxID != "" && $orderDetail->CustomizedBoxID != "0"){
                    $sideItem = $CI->Order_item_model->getSideItemDetail($orderDetail->CustomizedBoxID, "boxes", "BoxID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'>Box Name: ".$sideItem->Title." <span>1x</span> </li>";
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->BoxImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->Wrapping != "" && $orderDetail->Wrapping != "0"){
                    $sideItem = $CI->Order_item_model->getSideItemDetail($orderDetail->Wrapping, "wrappings", "WrappingID")[0];
                    $detailStr .= "<li style='display: flex;align-items:center;'><img src='".base_url($sideItem->WrappingImage)."' style='width:35px;height:auto' /> <span>1x</span> </li>";
                }
                if($orderDetail->content_type_id != "" && $orderDetail->content_type_id != "0"){
                    if($orderDetail->content_text!=""){
                        $sideItem   = $CI->Order_item_model->getContentTypeWithTextnImage($orderDetail->content_type_id, "shape_content_type_text", "ContentTypeID", $this->language)[0];
                        $detailStr .= "<li style='display: flex;align-items:center;'> Text Title:".$sideItem->Title." <br/> Text: ".$orderDetail->content_text." </li>";
                    }
                    if($orderDetail->content_type_sub_id != "" && $orderDetail->content_type_sub_id != "0"){
                        $sideItem   = $CI->Order_item_model->getContentSubTypeWithImage($orderDetail->content_type_sub_id, $orderDetail->content_type_sub_image_id, $this->language);
                        if(count($sideItem)){
                            $detailStr .= "<li style='display: flex;align-items:center;'> <div>Title: ".$sideItem[0]->Title."</div> <div><img src='".base_url($sideItem[0]->ImageName)."' style='width:35px;height:auto' /> </div></li>";
                        }
                    }
                }
                $detailStr .= "</ul>";
                $itemDetailStr["id_".$orderDetail->OrderItemID]["title"]        = $productTile;
                $itemDetailStr["id_".$orderDetail->OrderItemID]["description"]  = $detailStr;
            }
        }

        $order_details              = $this->Order_model->getOrders("orders.OrderID = $orderId");
        $data['order']              = $order_details[0];
        $data['language']           = $this->language;
        $data['barcode']            = generateBarcode($orderId, "barcode", $type = 'image');
        $data["itemDetailStrArr"]   = $itemDetailStr;
        $order_html                 = $this->load->view('frontend/invoice', $data, true);
        echo $order_html;
        exit();
    }
}