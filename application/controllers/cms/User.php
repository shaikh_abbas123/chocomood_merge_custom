<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model',
            'Role_model',
            'User_model',
            'Modules_users_rights_model',
            'Module_model',
            'City_model',
            'Store_model',
            'District_model',
            'Delivery_store_model',
            'Admin_store_model'


        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';


    }


    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['admins'] = $this->User_model->getBackendUsers('users.RoleID = 1');
        $this->data['warehouse_users'] = $this->User_model->getBackendUsers('users.RoleID = 2');
        $this->data['delivery_users'] = $this->User_model->getBackendUsers('users.RoleID = 3');
        $this->data['admin_users'] = $this->User_model->getBackendUsers('users.RoleID = 4');
        $this->data['admin_users_custom'] = $this->User_model->getBackendUsers('users.RoleID = 6');
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {

        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false, 'RoleID', $this->language, 'roles.IsActive = 1');
        if (!$this->data['roles']) {
            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['cities'] = $this->City_model->getAllJoinedData(false, 'CityID', $this->language, 'cities.IsActive = 1');
        if($this->data['cities']){
             $this->data['stores'] = $this->Store_model->getAllJoinedData(false, 'StoreID', $this->language, 'stores.IsActive = 1 AND stores.CityID = '.$this->data['cities'][0]->CityID);
             $this->data['districts'] = $this->District_model->getAllJoinedData(false, 'DistrictID', $this->language, 'districts.CityID = '.$this->data['cities'][0]->CityID.' AND districts.IsActive = 1');

        }else{
             $this->data['stores'] = $this->Store_model->getAllStores($this->language);

        }

       

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($id = '')
    {
        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $id = base64_decode($id);
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['roles'] = $this->Role_model->getAllJoinedData(false, 'RoleID', $this->language, 'roles.IsActive = 1');
        if (!$this->data['roles']) {
            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['cities'] = $this->City_model->getAllJoinedData(false, 'CityID', $this->language, 'cities.IsActive = 1');


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';

        $this->load->view('backend/layouts/default', $this->data);

    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');

        if (!$this->data['result']) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['roles'] = $this->Role_model->getAllJoinedData(false, 'RoleID', $this->language, 'roles.IsActive = 1');
        if (!$this->data['roles']) {
            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['cities'] = $this->City_model->getAllJoinedData(false, 'CityID', $this->language, 'cities.IsActive = 1');
        if($this->data['cities']){
             $this->data['stores'] = $this->Store_model->getAllJoinedData(false, 'StoreID', $this->language, 'stores.CityID = '.$this->data['result'][0]->CityID);
               $this->data['districts'] = $this->District_model->getAllJoinedData(false, 'DistrictID', $this->language, 'districts.CityID = '.$this->data['result'][0]->CityID.' AND districts.IsActive = 1');
        }else{
             $this->data['stores'] = $this->Store_model->getAllStores($this->language);

        }
        $this->data['delivery_stores'] = $this->Delivery_store_model->getMultipleRows(array('user_id' => $id));
        $this->data['admin_stores'] = $this->Admin_store_model->getMultipleRows(array('user_id' => $id));

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';

        $this->load->view('backend/layouts/default', $this->data);

    }


    public function rights($user_id = '')
    {

        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit') && !checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        if ($user_id == '') {
            $user_id = $this->session->userdata['admin']['UserID'];
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['UserID'] = $user_id;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/rights';
        $this->data['results'] = $this->Modules_users_rights_model->getModulesWithRights($user_id, $this->language);


        if (empty($this->data['results'])) {

            $this->session->set_flashdata('message', lang('please_add_module_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['users'] = $this->User_model->getAllJoinedData(false, 'UserID', $this->language);

        if (empty($this->data['users'])) {

            $this->session->set_flashdata('message', lang('please_add_user_first'));
            redirect(base_url('cms/user'));
        }


        $this->load->view('backend/layouts/default', $this->data);


    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                //$this->validate();
                $this->delete();
                break;

            case 'save_rights':
                $this->saveRights();
                break;

        }
    }


    private function validate($check = true)
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
        $this->form_validation->set_rules('Mobile', 'Mobile', 'required');
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
        $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }


    private function save()
    {

        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        $save_parent_data['RoleID'] = $post_data['RoleID'];
        $save_parent_data['Password'] = md5($post_data['Password']);
        $save_parent_data['Email'] = $post_data['Email'];
        $save_parent_data['Mobile'] = $post_data['Mobile'];
        if($post_data['RoleID'] != 1) {

            $save_parent_data['StoreID'] =  isset($post_data['StoreID'][0]) ? $post_data['StoreID'][0] : 0;
            $save_parent_data['PreferredLang'] = $post_data['PreferredLang'];
            
            $save_parent_data['CityID'] = $post_data['CityID'];



        }
        if($post_data['RoleID'] == 3) {
            $save_parent_data['DistrictID'] = implode(',',$post_data['DistrictID']);
        }

        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {


            if($post_data['RoleID'] == 3) {
                if(is_array($post_data['StoreID']) && !empty($post_data['StoreID'])){
                    for($i = 0; $i< sizeof($post_data['StoreID']); $i++){
                        $save_store_data['store_id'] = $post_data['StoreID'][$i];
                        $save_store_data['user_id'] = $insert_id;
                        $this->Delivery_store_model->save($save_store_data);
                    }
                }
            }

            if($post_data['RoleID'] == 4) {
                if(is_array($post_data['StoreID']) && !empty($post_data['StoreID'])){
                    for($i = 0; $i< sizeof($post_data['StoreID']); $i++){
                        $save_store_data['store_id'] = $post_data['StoreID'][$i];
                        $save_store_data['user_id'] = $insert_id;
                        if($post_data['StoreID'][$i] != 0){
                         $this->Admin_store_model->save($save_store_data);
                        }
                    }
                }
            }
            // $default_lang = getDefaultLanguage();
            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {


                $save_child_data['FullName'] = $post_data['Title'];
                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;

                $this->$child->save($save_child_data);
            }


            $modules = $this->Module_model->getAll();
            foreach ($modules as $key => $value) {
                /*if($post_data['RoleID'] == 1 || $post_data['RoleID'] == 2){
                    $rights_all = 1;
                }else{
                    $rights_all = 0;
                }*/

                $other_data[] = [
                    'ModuleID' => $value->ModuleID,
                    'UserID' => $insert_id,
                    'CanView' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanView') ? 1 : 0),
                    'CanAdd' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanAdd') ? 1 : 0),
                    'CanEdit' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanEdit') ? 1 : 0),
                    'CanDelete' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanDelete') ? 1 : 0),
                    'CreatedAt' => date('Y-m-d H:i:s'),
                    'CreatedBy' => $this->session->userdata['admin']['UserID'],
                    'UpdatedAt' => date('Y-m-d H:i:s'),
                    'UpdatedBy' => $this->session->userdata['admin']['UserID']
                ];


            }


            $this->Modules_users_rights_model->insert_batch($other_data);


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {

                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $success['redirect'] = true;
                $success['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            if (isset($post_data['Password']) && $post_data['Password'] == '') {
                unset($post_data['Password']);
            }
            $save_parent_data = array();
            $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {


                //$save_parent_data['Email']          = $post_data['Email'];
                $save_parent_data['Mobile']          = $post_data['Mobile'];
                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
                $save_parent_data['RoleID'] = $post_data['RoleID'];
                if(isset($post_data['Password']) && $post_data['Password'] != ''){
                    $save_parent_data['Password'] = md5($post_data['Password']);
                }
                //$save_parent_data['CenterID']     = $post_data['CenterID'];
                if ($post_data['RoleID'] != 1) {
                    $save_parent_data['PreferredLang'] = $post_data['PreferredLang'];

                    $save_parent_data['StoreID'] = isset($post_data['StoreID'][0]) ? $post_data['StoreID'][0] : 0;
                     $save_parent_data['CityID'] = $post_data['CityID'];


                }
                if($post_data['RoleID'] == 3) {
                    $save_parent_data['DistrictID'] = implode(',',$post_data['DistrictID']);
                }
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $this->$parent->update($save_parent_data, $update_by);
                if($post_data['RoleID'] == 3) {
                    $this->Delivery_store_model->delete(['user_id' => base64_decode($post_data[$this->data['TableKey']])]);
                    if(is_array($post_data['StoreID']) && !empty($post_data['StoreID'])){
                        for($i = 0; $i< sizeof($post_data['StoreID']); $i++){
                            $save_store_data['store_id'] = $post_data['StoreID'][$i];
                            $save_store_data['user_id'] = base64_decode($post_data[$this->data['TableKey']]);
                            $this->Delivery_store_model->save($save_store_data);
                        }
                    }
                }

                if($post_data['RoleID'] == 4) {
                    $this->Admin_store_model->delete(['user_id' => base64_decode($post_data[$this->data['TableKey']])]);
                    if(is_array($post_data['StoreID']) && !empty($post_data['StoreID'])){
                        for($i = 0; $i< sizeof($post_data['StoreID']); $i++){
                            $save_store_data['store_id'] = $post_data['StoreID'][$i];
                            $save_store_data['user_id'] = base64_decode($post_data[$this->data['TableKey']]);
                            if($post_data['StoreID'][$i] != 0){
                             $this->Admin_store_model->save($save_store_data);
                            }
                        }
                    }
                }

                $save_child_data['FullName'] = $post_data['Title'];


                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);

            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['FullName'] = $post_data['Title'];


                    $this->$child->update($save_child_data, $update_by);

                } else {

                    $save_child_data['FullName'] = $post_data['Title'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);


                    $this->$child->save($save_child_data);
                }


            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {

            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function saveRights()
    {


        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $user_id = $this->input->post('UserID');

        $modules = $this->Modules_users_rights_model->getModulesWithRights($user_id, $this->language);

        $can_view = $this->input->post('CanView');

        $can_add = $this->input->post('CanAdd');
        $can_edit = $this->input->post('CanEdit');
        $can_delete = $this->input->post('CanDelete');

        if (!empty($modules)) {

            foreach ($modules as $module) {

                $update_data[] = [
                    'ModuleRightID' => $module['ModuleRightID'],
                    'CanView' => (isset($can_view[$module['ModuleRightID']]) ? 1 : 0),
                    'CanAdd' => (isset($can_add[$module['ModuleRightID']]) ? 1 : 0),
                    'CanEdit' => (isset($can_edit[$module['ModuleRightID']]) ? 1 : 0),
                    'CanDelete' => (isset($can_delete[$module['ModuleRightID']]) ? 1 : 0),
                    'UpdatedAt' => date('Y-m-d H:i:s'),
                    'UpdatedBy' => $this->session->userdata['admin']['UserID']

                ];
            }

            $this->Modules_users_rights_model->update_batch($update_data, 'ModuleRightID');
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = true;

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }


    }


    private function delete()
    {

        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');

        $this->Modules_users_rights_model->delete($deleted_by);

        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        $this->Delivery_store_model->delete(['user_id' => $deleted_by[$this->data['TableKey']]]);
        $this->Admin_store_model->delete(['user_id' => $deleted_by[$this->data['TableKey']]]);



        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


    public function getUsersForStore()
    {
        $delivery_users_html = '<option value="" selected>Please select delivery users</option>';
        $driver_users_html = '<option value="" selected>Please select driver users</option>';
        $StoreID = $this->input->post('StoreID');
        $delivery_users = $this->User_model->getUsers('users.RoleID = 2 AND users.StoreID = ' . $StoreID);
        if ($delivery_users && count($delivery_users) > 0) {
            foreach ($delivery_users as $delivery_user) {
                $delivery_users_html .= '<option value="' . $delivery_user->UserID . '">' . $delivery_user->FullName . '</option>';
            }
        }
        $response_arr['delivery_users_html'] = $delivery_users_html;
        echo json_encode($response_arr);
        exit();
    }


}