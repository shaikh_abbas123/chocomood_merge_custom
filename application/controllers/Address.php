<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model(['Temp_order_model',
            'Semsa_shipment_model',
            'Semsa_shipment_text_model',
        ]);
        $this->load->model('Order_model');
        $this->load->model('User_address_model');
        $this->load->model('District_model');
        $this->load->model('Product_model');
        $this->load->model('Packages_product_model');
        $this->data['language'] = $this->language;

    }

    public function index()
    {

        $this->session->unset_userdata('DeliveryStoreID');
        $this->session->unset_userdata('DeliveryStoreTitle');
        $cart_item = $this->Temp_order_model->getCartItems($this->UserID, $this->language);

        foreach(@$cart_item as $k => $v) 
        {
            if($v->ItemType == 'Product')
            {
                $product = $this->Product_model->getJoinedData(false, 'ProductID', "products.ProductID = " . $v->ProductID . " AND system_languages.ShortCode = '" . $this->language . "'")[0];
                $price = 0;
                
                if($product->IsCorporateProduct)
                {
                    $price = $product->CorporatePrice;
                }else
                {
                    if($v->PriceType == 'kg')
                    {
                        $package = array();
                        $product_packages = $this->Packages_product_model->productPackages($v->ProductID, $this->language);
                        
                        foreach(@$product_packages as $k2 => $v2) {
                            
                            if (@$v2['PackagesProductID'] == @$v->Package)
                            {
                                array_push($package,$v2);
                            }
                        }
                        // print_rm($package[0]);
                        $gram_price =  $package[0]['PerGramPrice']; //208.70
                        $piece_weight =  $package[0]['PerPiecePrice']; //1000
                        $weight = $package[0]['quantity']; // 1000
                        $price = ($gram_price/$piece_weight)*$weight;
                        
                    }else{
                        $price = $product->Price;
                    }
                    
                }

                

                $update['TempItemPrice'] = $price;
                
                $offer_product = checkProductIsInAnyOffer($v->ProductID);
                $IsOnOffer = false;
                if(!empty($offer_product)){
                    $update['DiscountType'] = $offer_product['DiscountType'];
                    $update['Discount'] = $offer_product['Discount'];
                }else
                {
                    $update['DiscountType'] = Null;
                    $update['Discount'] = Null;
                }
                
                $update_by['TempOrderID'] = $v->TempOrderID;
                // print_rm($update);
                $this->Temp_order_model->update($update, $update_by);
            }
        }
        
        $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($data['cart_items'])) {
            redirect('product');
        }
        // var_dump($data['cart_items']);
        //print_rm($data);die;
        $data['addresses'] = $this->User_address_model->getAddresses("user_address.UserID = ".$this->UserID);
        $data['default_city'] = 0;
        $data['default_city_lat'] = 0;
        $data['default_city_long'] = 0;
        // if($data['cart_items'][0]->ItemType != 'Product')
        // {
        //     $data['itemtype'] = 'customize';
        // }
        if ($data['addresses'] && count($data['addresses']) > 0)
        {
            foreach($data['addresses'] as $ad){
                if($ad->IsDefault == 1){
                     $data['default_city'] = $ad->CityID;
                     $data['default_city_lat'] = $ad->latitude;
                     $data['default_city_long'] = $ad->longitude;
                     break; 
                }
            }
            $where = false;
            if(isset($_GET['city']) && $_GET['city'] != ''){
                $where  = 'stores.CityID = '.$_GET['city'].' AND stores.IsActive = 1';
                //$where  = 'stores.IsActive = 1';
            }
            $isCustomize = 0;
            foreach($data['cart_items'] as $v)
            {
                if($v->ProductID == '0')
                {
                    $isCustomize = 1;
                    break;
                }
            }
            
            $where = 'stores.isCustomize = 0 OR stores.isCustomize = "" AND stores.IsActive = 1 AND stores.ShippingID IS NOT NULL';
            
            $data['available_cities'] = $this->Product_model->getCities($where,$this->language);
            // foreach($data['available_cities'] as $v){
            //     $v->ShippingID = explode(',',$v->ShippingID);
            // }
            // print_rm($data['available_cities']);
            $data['isCustomize'] = $isCustomize;
            $data['semsa_shipment'] = $this->Semsa_shipment_model->getAllJoinedDataWithTable('semsa_shipment', false, 'SemsaShipmentID', $this->language, 'semsa_shipment.IsActive = 1');
            $data['view'] = 'frontend/checkout-address';
            $this->load->view('frontend/layouts/default', $data);
        } else {
            $this->session->set_flashdata("address_message", lang('you_have_no_saved_address'));
            redirect('cart');
        }
    }

    public function add()
    {
        $data['view'] = 'frontend/add_address';
        $data['default_address'] = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'IsDefault' => 1));
        //print_rm($data['default_address']);
        $data['get_current_location'] = lang('get_current_location');
        $this->load->view('frontend/layouts/default', $data);
    }



    public function edit($address_id)
    {
        $data['view'] = 'frontend/edit_address';
        $data['address'] = $this->User_address_model->getAddresses("user_address.AddressID = ".$address_id." AND user_address.UserID = " . $this->UserID)[0];
        if(empty($data['address'])){
            redirect('account/addresses');
        }

        $data['districts'] = $this->District_model->getAllJoinedData(false, 'DistrictID', $this->language, "districts.IsActive = 1 AND districts.CityID = ".$data['address']->CityID, 'ASC', 'SortOrder');
        $data['get_current_location'] = lang('get_current_location');
        $this->load->view('frontend/layouts/default', $data);
    }

    public function save()
    {
        $post_data = $this->input->post();
        $check = $post_data['check'];
        unset($post_data['check']);

        if($post_data['Latitude'] == '' && $post_data['Longitude'] == ''){
            $response['message'] = lang('map_pin_required');
            echo json_encode($response);
            exit();
        }

        if(isset($post_data['IsDefault'])){
            $user_default_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'IsDefault' => 1));
            if($user_default_address)
            {
                $this->User_address_model->update(array('IsDefault' => 0),array('AddressID' => $user_default_address->address_id));
                //$post_data['IsDefault'] = 0;
            }

            $post_data['IsDefault'] = 1; 

        }else{
            $post_data['IsDefault'] = 0;
        }
        
        $user_payment_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'UseForPaymentCollection' => 1));
        if ($user_payment_address)
        {
            $post_data['UseForPaymentCollection'] = 0;
        } else {
            $post_data['UseForPaymentCollection'] = 1;
        }
        $post_data['UserID'] = $this->UserID;
        $insert_id = $this->User_address_model->save($post_data);
        if ($insert_id > 0) {
            $response['message'] = lang('address_saved_successfully');
            $response['reset'] = true;
            $response['redirect'] = true;
            if($check == 'true'){
                $response['url'] = "/address";

            }else{
                
                if($this->session->userdata('check_address') == '1') {
                    $response['url'] = "address";
                }else
                {
                    $response['url'] = "account/addresses";
                }
                $this->session->unset_userdata($_SESSION['check_address']);
            }
            echo json_encode($response);
            exit();
        } else {
            $response['message'] = lang('something_went_wrong');
            echo json_encode($response);
            exit();
        }
    }

    public function update()
    {
        $post_data = $this->input->post();

        if($post_data['Latitude'] == '' && $post_data['Longitude'] == ''){
            $response['message'] = lang('map_pin_required');
            echo json_encode($response);
            exit();
        }

        $address = $this->User_address_model->getAddresses("user_address.AddressID = ".$post_data['AddressID'])[0];
        if(!empty($address)){
            if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 0 && $address->IsDefault == 1){
                $response['message'] = lang('make_default_address');
                echo json_encode($response);
                exit();
            }
        }

        if(isset($post_data['IsDefault'])){
            $user_default_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'IsDefault' => 1));
            if($user_default_address)
            {
                $this->User_address_model->update(array('IsDefault' => 0),array('AddressID' => $user_default_address->AddressID));
                //$post_data['IsDefault'] = 0;
            } 

            $post_data['IsDefault'] = 1; 

        }
        
        $user_payment_address = $this->User_address_model->getWithMultipleFields(array('UserID' => $this->UserID, 'UseForPaymentCollection' => 1));
        if ($user_payment_address)
        {
            $post_data['UseForPaymentCollection'] = 0;
        } else {
            $post_data['UseForPaymentCollection'] = 1;
        }
        $post_data['UserID'] = $this->UserID;
        $this->User_address_model->update($post_data,array('AddressID' => $post_data['AddressID'],'UserID' => $this->UserID));
        $response['message'] = lang('address_saved_successfully');
        $response['reset'] = true;
        $response['redirect'] = true;
        $response['url'] = "account/addresses";
        echo json_encode($response);
        exit();
    }

    public function markAddressAsDefault()
    {
        $AddressID = $this->input->get('AddressID');
        $this->User_address_model->update(array('IsDefault' => 0), array('UserID' => $this->UserID));
        $this->User_address_model->update(array('IsDefault' => 1), array('AddressID' => $AddressID));
        $response['message'] = lang('address_set_as_default');
        echo json_encode($response);
        exit();
    }

    public function setAddressForPaymentCollection()
    {
        $AddressID = $this->input->get('AddressID');
        $this->User_address_model->update(array('UseForPaymentCollection' => 0), array('UserID' => $this->UserID));
        $this->User_address_model->update(array('UseForPaymentCollection' => 1), array('AddressID' => $AddressID));
        $response['message'] = lang('address_set_for_payment');
        echo json_encode($response);
        exit();
    }

    public function delete()
    {
        $post_data = $this->input->post();
        $addressUsedForOrder = $this->Order_model->getWithMultipleFields($post_data);
        if ($addressUsedForOrder)
        {
            $response['status'] = false;
            $response['message'] = lang('cant_remove_address_order');
            echo json_encode($response);
            exit();
        } else {
            $this->User_address_model->delete($post_data);
            $response['status'] = true;
            $response['reload'] = true;
            $response['message'] = lang('address_removed');
            echo json_encode($response);
            exit();
        }
    }

}