<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Base_Controller
{
    public function __construct()
    {
        
        parent::__construct();
        //checkFrontendSession();
        $this->load->model('Temp_order_model');
        $this->load->model('User_wishlist_model');
        $this->load->model('Product_model');
        $this->load->model('Coupon_model');
        $this->load->model('Packages_product_model');
        // $this->load->library('session');
        $this->data['language'] = $this->language;
    }

    public function index()
    {
        $cart_item = $this->Temp_order_model->getCartItems($this->UserID, $this->language);

        foreach(@$cart_item as $k => $v) 
        {
            if($v->ItemType == 'Product')
            {
                $product = $this->Product_model->getJoinedData(false, 'ProductID', "products.ProductID = " . $v->ProductID . " AND system_languages.ShortCode = '" . $this->language . "'")[0];
                $price = 0;
                if($product->IsCorporateProduct)
                {
                    $price = $product->CorporatePrice;
                }else
                {
                    if($v->PriceType == 'kg')
                    {
                        $package = array();
                        $product_packages = $this->Packages_product_model->productPackages($v->ProductID, $this->language);
                        
                        foreach(@$product_packages as $k2 => $v2) {
                            
                            if (@$v2['PackagesProductID'] == @$v->Package)
                            {
                                array_push($package,$v2);
                            }
                        }
                        // print_rm($package);
                        $gram_price =  $package[0]['PerGramPrice']; //208.70
                        $piece_weight =  $package[0]['PerPiecePrice']; //1000
                        $weight = $package[0]['quantity']; // 1000
                        $price = ($gram_price/$piece_weight)*$weight;
                        
                    }else{
                        $price = $product->Price;
                    }
                }
                $update['TempItemPrice'] = $price;
                
                $offer_product = checkProductIsInAnyOffer($v->ProductID);
                $IsOnOffer = false;
                if(!empty($offer_product)){
                    $update['DiscountType'] = $offer_product['DiscountType'];
                    $update['Discount'] = $offer_product['Discount'];
                }else
                {
                    $update['DiscountType'] = Null;
                    $update['Discount'] = Null;
                }
                
                $update_by['TempOrderID'] = $v->TempOrderID;
                // print_rm($update);
                $this->Temp_order_model->update($update, $update_by);
            }
        }
       
        $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        $data['wishlist_items'] = $this->Product_model->getWishlistItems($this->UserID, $this->language);
        
        if (empty($data['cart_items'])) {
            //$this->session->set_flashdata('message', lang('your_cart_is_empty'));
             $data['view'] = 'frontend/empty-cart';
            $this->load->view('frontend/layouts/default', $data);
        }else{

            $shipment_method = getTaxShipmentCharges('Shipment', true);
            if ($shipment_method) {
                $this->session->set_userdata('ShipmentMethodIDForBooking', $shipment_method->TaxShipmentChargesID);
            } else {
                $this->session->set_userdata('ShipmentMethodIDForBooking', 0);
            }

            $this->session->set_userdata('PaymentMethodForBooking', 'COD');
            //$this->session->set_userdata('PaymentMethodForBooking', 'PayTab');


            $data['view'] = 'frontend/checkout-cart';
            $this->load->view('frontend/layouts/default', $data);

        }
        
    }

    public function addToCart()
    {
        $fetch_by['UserID'] = $this->UserID;
        $fetch_by['ProductID'] = $this->input->post('ProductID');
        $fetch_by['ItemType'] = $this->input->post('ItemType');
        $posted_product_quantity = $this->input->post('Quantity');
        $posted_product_price = $this->input->post('TempItemPrice');
        $max = $this->input->post('max');
        $min = $this->input->post('min');
        $Weight = $this->input->post('Weight');
        $priceType = $this->input->post('PriceType');
        $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
        if ($already_added) {
            $update['Quantity'] = $already_added->Quantity + $posted_product_quantity;
            $update_by['TempOrderID'] = $already_added->TempOrderID;
            if($priceType == 'kg')
            {
                $update['Package'] = $this->input->post('package');
                if($already_added->Package != $this->input->post('package')){
                    $update['Quantity'] = $posted_product_quantity;
                }
                $update['TempItemPrice'] = $posted_product_price;
                if($update['Quantity'] < $min )
                {
                    $response['status'] = false;
                    $response['message'] = lang('Limit');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                }
                if( $update['Quantity'] > $max)
                {
                    $response['status'] = false;
                    $response['message'] = lang('Limit');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                }
            }
            else
            {
                $update['Package'] = 0;
                $update['Weight'] = $Weight;
            }
            $this->Temp_order_model->update($update, $update_by);
            $response['status'] = true;
            $response['message'] = lang('added_to_basket');
            $response['total_cart_items'] = getTotalProduct($this->UserID);
            echo json_encode($response);
            exit();
        } else {
            $save_data['UserID'] = $this->UserID;
            $save_data['Quantity'] = $posted_product_quantity;
            $save_data['ProductID'] = $this->input->post('ProductID');
            $save_data['ItemType'] = $this->input->post('ItemType');
            $save_data['DiscountType'] = $this->input->post('DiscountType');
            $save_data['Discount'] = $this->input->post('Discount');
            $save_data['IsCorporateItem'] = $this->input->post('IsCorporateItem');
            $save_data['TempItemPrice'] = $posted_product_price;
            if($priceType == 'kg')
            {
                $save_data['Package'] = $this->input->post('package');
            }
            else
            {
                $save_data['Package'] = 0;
                $save_data['Weight'] = $Weight;
            }
            $insert_id = $this->Temp_order_model->save($save_data);
            if ($insert_id > 0) {
                $response['status'] = true;
                $response['message'] = lang('added_to_basket');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('something_went_wrong');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            }
        }
    }

    public function updateCart()
    {
        $update['Quantity'] = $this->input->post('Quantity');
        $update_by['TempOrderID'] = $this->input->post('TempOrderID');
        $priceType = $this->input->post('PriceType');
        $price = $this->input->post('Price');
        if($priceType == 'kg')
        {
            $update['Package'] = $this->input->post('package');
        }
        else
        {
            $update['Package'] = 0;
        }
        // print_r($update);die;
        $this->Temp_order_model->update($update, $update_by);
        $response['total'] = number_format($update['Quantity'] * $price, 2);
        $response['status'] = true;
        $response['message'] = lang('basket_updated');
        $response['total_cart_items'] = getTotalProduct($this->UserID);
        echo json_encode($response);
        exit();
    }

    public function removeFromCart()
    {
        $deleted_by['TempOrderID'] = $this->input->post('TempOrderID');
        $this->Temp_order_model->delete($deleted_by);
        $response['status'] = true;
        $response['reload'] = true;
        $response['message'] = lang('removed_from_basket');
        echo json_encode($response);
        exit();
    }

    public function addToWishlist()
    {
        if ($this->session->userdata('user')) {
            $post_data = $this->input->get();
            $data['UserID'] = $this->session->userdata['user']->UserID;
            $data['ItemID'] = $post_data['ID'];
            $data['ItemType'] = $post_data['Type'];
            $wishlist = $this->User_wishlist_model->getWithMultipleFields($data);
            if ($wishlist) {
                $this->User_wishlist_model->delete($data);
                $response['class'] = "p_unliked";
                $response['message'] = lang('removed_from_wishlist');
                echo json_encode($response);
                exit();
            } else {
                $saved_id = $this->User_wishlist_model->save($data);
                if ($saved_id > 0) {

                    $deleted_by['UserID'] = $this->session->userdata['user']->UserID;
                    $deleted_by['ProductID'] = $post_data['ID'];
                    $this->Temp_order_model->delete($deleted_by);
                    $response['class'] = "p_liked";
                    $response['message'] = lang('added_to_wishlist');
                    echo json_encode($response);
                    exit();
                } else {
                    $response['class'] = "";
                    $response['message'] = lang('something_went_wrong');
                    echo json_encode($response);
                    exit();
                }
            }
        } else {
            $response['class'] = "";
            $response['message'] = lang('need_to_login_wishlist');
            echo json_encode($response);
            exit();
        }
    }
    public function addRemoveToWishlist()
    {
        if ($this->session->userdata('user')) {
            $post_data = $this->input->get();
            $data['UserID'] = $this->session->userdata['user']->UserID;
            $data['ItemID'] = $post_data['ID'];
            $data['ItemType'] = $post_data['Type'];
            
            $wishlist = $this->User_wishlist_model->getWithMultipleFields($data);
            if ($wishlist) {
                $this->User_wishlist_model->delete($data);
            } 
            if(@$post_data['return_cart'] == 1)
            {
                $data['return_cart'] = 1;
            }
            $saved_id = $this->User_wishlist_model->save($data);
            if ($saved_id > 0) {

                $deleted_by['UserID'] = $this->session->userdata['user']->UserID;
                $deleted_by['ProductID'] = $post_data['ID'];
                $this->Temp_order_model->delete($deleted_by);
                $response['class'] = "p_liked";
                $response['message'] = lang('added_to_wishlist');
                echo json_encode($response);
                exit();
            } else {
                $response['class'] = "";
                $response['message'] = lang('something_went_wrong');
                echo json_encode($response);
                exit();
            }
            
        } else {
            $response['class'] = "";
            $response['message'] = lang('need_to_login_wishlist');
            echo json_encode($response);
            exit();
        }
    }
    public function applyCoupon()
    {
        $post_data = $this->input->post(); // CouponCode, Date (timestamp of the date when coupon is being applied)
        $coupon = $this->Coupon_model->getWithMultipleFields(array('CouponCode' => $post_data['CouponCode']), true);
        if ($coupon) {
            $today = date('Y-m-d');
            $expire = $coupon['ExpiryDate'];
            $today_dt = new DateTime($today);
            $expire_dt = new DateTime($expire);
            if ($expire_dt > $today_dt && $coupon['UsageCount'] > 0) {
                $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
                foreach($data['cart_items'] as $k=>$v){
                    $offer_product = checkProductIsInAnyOffer($v->ProductID);
                    if(!empty($offer_product)){
                        $response['status'] = false;
                        $response['message'] = "A Discount product is already in the cart";
                        echo json_encode($response);
                        exit();  
                        return false;
                    }

                }
                // saving coupon in session
                $this->session->set_userdata('order_coupon', $coupon);
                // $update['UsageCount'] = $coupon['UsageCount'] - 1;
                // $update_by['CouponID'] = $coupon['CouponID'];
                // $this->Coupon_model->update($update, $update_by);
                $response['status'] = true;
                $response['message'] = lang('coupon_applied');
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('coupon_expired');
                echo json_encode($response);
                exit();
            }
        } else {
            $response['status'] = false;
            $response['message'] = lang('coupon_invalid');
            echo json_encode($response);
            exit();
        }
    }

    public function removeCoupon()
    {
        $this->session->unset_userdata('order_coupon');
        $response['message'] = lang('coupon_cleared');
        echo json_encode($response);
        exit();
    }
    //choco message
    public function addChocoBoxToCart()
    {
        $product_ids = $this->input->post('ProductIDs');
        // dump($exploded_p_ids);
        if ($product_ids == '') {
            $response['status'] = false;
            $response['message'] = lang('please_add_items_to_box');
            $response['total_cart_items'] = getTotalProduct($this->UserID);
            echo json_encode($response);
            exit();
        } else {
            $fetch_by['UserID'] = $this->UserID;
            $fetch_by['CustomizedOrderProductIDs'] = $product_ids;
            $fetch_by['ItemType'] = $this->input->post('ItemType');
            $fetch_by['CustomizedBoxID'] = $this->input->post('CustomizedBoxID');
            $posted_product_quantity = $this->input->post('Quantity');
            $posted_product_price = $this->input->post('TempItemPrice');
            $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
            if ($already_added) {
                $update['Quantity'] = $already_added->Quantity + $posted_product_quantity;
                $update_by['TempOrderID'] = $already_added->TempOrderID;
                $this->Temp_order_model->update($update, $update_by);
                $response['status'] = true;
                $response['message'] = lang('added_to_basket');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            } else {
                $save_data['UserID'] = $this->UserID;
                $save_data['Quantity'] = $posted_product_quantity;
                $save_data['CustomizedOrderProductIDs'] = $product_ids;
                $save_data['ItemType'] = $this->input->post('ItemType');
                $save_data['ProductID'] = $this->input->post('ItemType');
                $save_data['CustomizedBoxID'] = $this->input->post('CustomizedBoxID');
                $save_data['Ribbon'] = $this->input->post('RibbonColor');
                $save_data['TempItemPrice'] = $posted_product_price;
                // $save_data['Package'] = @$$this->input->post('package');
                $insert_id = $this->Temp_order_model->save($save_data);
                if ($insert_id > 0) {
                    $response['status'] = true;
                    $response['message'] = lang('added_to_basket');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = false;
                    $response['message'] = lang('something_went_wrong');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                }
            }
        }
    }
    //choco shape
    public function addCustomizedShapeToCart()
    {
        // var_dump($this->input->post());
        $choco_shape = $this->input->post();
        // print_rm($choco_shape);
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        // $this->form_validation->set_rules('content-type', 'Content Type', 'required');
       
        if($this->input->post('content-type') == 4)
        {
            
            $this->form_validation->set_rules('customer-content', 'Customer Content', 'required|max_length[8]');
            $this->form_validation->set_rules('textType', 'Text Type', 'required');
            $this->form_validation->set_rules('fill_color', 'Fill Color', 'required');
            $this->form_validation->set_rules('fillings[]', 'Filling', 'required|numeric');
            $this->form_validation->set_rules('wrappings', 'Wrapping', 'required|numeric');
            $this->form_validation->set_rules('powderColor', 'Powder Color', 'required');
            $this->form_validation->set_rules('Quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('shapeID', 'Shape ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                $errors['error'] = validation_errors();
                $errors['success'] = false;
                echo json_encode($errors);
                exit();
            } else {
                $shapedata['content-type'] =  $this->input->post('content-type');
                $shapedata['customer-content'] =  $this->input->post('customer-content');
                $shapedata['textType'] =  $this->input->post('textType');
                $shapedata['fill_color'] =  $this->input->post('fill_color');
                $shapedata['fillings'] = implode(",",$this->input->post('fillings[]'));
                // $shapedata['wrappings'] = implode(",",$this->input->post('wrappings[]'));
                $shapedata['wrappings'] = $this->input->post('wrappings');
                $shapedata['powderColor'] = $this->input->post('powderColor');
                $shapedata['Quantity'] = $this->input->post('Quantity');
                $shapedata['shapeID'] = $this->input->post('shapeID');
                $this->session->set_userdata($shapedata);
                // var_dump($shapedata);
                $success['success'] = true;
                $success['url'] = 'customize/choco_shape_order_preview';
                echo json_encode($success);
                exit;
            }

        }else if($this->input->post('content-type') == 5)
        {
            $this->form_validation->set_rules('customer-content', 'Customer Content', 'required|max_length[8]');
            $this->form_validation->set_rules('drawing', 'Drawing Image', 'required');
            $this->form_validation->set_rules('fill_color', 'Fill Color', 'required');
            $this->form_validation->set_rules('fillings[]', 'Filling', 'required|numeric');
            $this->form_validation->set_rules('wrappings', 'Wrapping', 'required|numeric');
            $this->form_validation->set_rules('powderColor', 'Powder Color', 'required');
            $this->form_validation->set_rules('Quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('shapeID', 'Shape ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                $errors['error'] = validation_errors();
                $errors['success'] = false;
                echo json_encode($errors);
                exit();
            } else {
                $shapedata['content-type'] =  $this->input->post('content-type');
                $shapedata['customer-content'] =  $this->input->post('customer-content');
                $shapedata['drawing'] =  $this->input->post('drawing');
                $shapedata['fill_color'] =  $this->input->post('fill_color');
                $shapedata['fillings'] = implode(",",$this->input->post('fillings[]'));
                $shapedata['wrappings'] = $this->input->post('wrappings');
                $shapedata['powderColor'] = $this->input->post('powderColor');
                $shapedata['Quantity'] = $this->input->post('Quantity');
                $shapedata['shapeID'] = $this->input->post('shapeID');
                $this->session->set_userdata($shapedata);
                $success['success'] = true;
                $success['url'] = 'customize/choco_shape_order_preview';
                echo json_encode($success);
                exit;
            }
        }
        else if($this->input->post('content-type') == 6)
        {
            $this->form_validation->set_rules('drawing', 'Drawing Image', 'required');
            if (empty($_FILES['customer-content']['name']))
            {
                $this->form_validation->set_rules('customer-content', 'Image', 'required');
            }
            $this->form_validation->set_rules('fill_color', 'Fill Color', 'required');
            $this->form_validation->set_rules('fillings[]', 'Filling', 'required|numeric');
            $this->form_validation->set_rules('wrappings', 'Wrapping', 'required|numeric');
            $this->form_validation->set_rules('powderColor', 'Powder Color', 'required');
            $this->form_validation->set_rules('Quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('shapeID', 'Shape ID', 'required');
            if ($this->form_validation->run() == FALSE) {
                $errors['error'] = validation_errors();
                $errors['success'] = false;
                echo json_encode($errors);
                exit();
            } else {
                $shapedata['content-type'] =  $this->input->post('content-type');
                $shapedata['drawing'] =  $this->input->post('drawing');
                $shapedata['fill_color'] =  $this->input->post('fill_color');
                $shapedata['fillings'] = implode(",",$this->input->post('fillings[]'));
                $shapedata['wrappings'] = $this->input->post('wrappings');
                $shapedata['powderColor'] = $this->input->post('powderColor');
                $shapedata['Quantity'] = $this->input->post('Quantity');
                $shapedata['shapeID'] = $this->input->post('shapeID');
                // $shapedata['imageFile'] = $_FILES['customer-content'];
                if (isset($_FILES['customer-content']["name"]) && $_FILES['customer-content']["name"] != '') 
                {    
                    $shapedata['imageFile'] =  $this->uploadImageSingle('customer-content', 'uploads/choco_shape_upload_image/',0,'choco_shape_upload_image');
                }
                $this->session->set_userdata($shapedata);
                $success['success'] = true;
                $success['url'] = 'customize/choco_shape_order_preview';
                echo json_encode($success);
                exit;
            }
        }
    }
    //choco box
    public function addChocoBoxToCart_box()
    {
        // print_rm($_POST);
        $product_ids = $this->input->post('ProductIDs');
        // dump($exploded_p_ids);
        if ($product_ids == '') {
            $response['status'] = false;
            $response['message'] = lang('please_add_items_to_box');
            $response['total_cart_items'] = getTotalProduct($this->UserID);
            echo json_encode($response);
            exit();
        } else {
            $fetch_by['UserID'] = $this->UserID;
            $fetch_by['CustomizedOrderProductIDs'] = $product_ids;
            $fetch_by['ItemType'] = $this->input->post('ItemType');
            $fetch_by['BoxPrintable'] = $this->input->post('boxType'); //0 non printable, 1 printable
            $save_data['Ribbon'] = $this->input->post('ribbon');
            $save_data['Wrapping'] = $this->input->post('wrapping');
            $fetch_by['CustomizedBoxID'] = $this->input->post('BoxID');
            $fetch_by['content_type_id'] = $this->input->post('contentType');
            $fetch_by['content_type_sub_id'] = $this->input->post('contentTypeSub');
            $fetch_by['content_text'] = $this->input->post('customerContent');
            $fetch_by['content_type_sub_image_id'] = $this->input->post('customImageId');
            $posted_product_quantity = $this->input->post('Quantity');
            $posted_product_price = $this->input->post('TotalPrice');
            $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
            if ($already_added) {
                $update['Quantity'] = $already_added->Quantity + $posted_product_quantity;
                $update_by['TempOrderID'] = $already_added->TempOrderID;
                $this->Temp_order_model->update($update, $update_by);
                $response['status'] = true;
                $response['message'] = lang('added_to_basket');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            } else {
                $save_data['UserID'] = $this->UserID;
                $save_data['Quantity'] = $posted_product_quantity;
                $save_data['CustomizedOrderProductIDs'] = $product_ids;
                $save_data['BoxPrintable'] = $this->input->post('boxType');
                $save_data['ItemType'] = $this->input->post('ItemType');
                $save_data['ProductID'] = $this->input->post('ItemType');
                $save_data['CustomizedBoxID'] = $this->input->post('BoxID');
                $save_data['content_type_id'] = $this->input->post('contentType');
                $save_data['content_type_sub_id'] = $this->input->post('contentTypeSub');
                $save_data['content_text'] = $this->input->post('customerContent');
                $save_data['content_type_sub_image_id'] = $this->input->post('contentTypeSubImage') == ""?$this->input->post('customImageId'):$this->input->post('contentTypeSubImage');
                $save_data['TempItemPrice'] = $posted_product_price;
                // $save_data['Package'] = @$$this->input->post('package');
                $insert_id = $this->Temp_order_model->save($save_data);
                if ($insert_id > 0) {
                    $response['status'] = true;
                    $response['message'] = lang('added_to_basket');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    $this->session->unset_userdata(['printable','contentType','contentTypeSub','contentTypeSubImage','customer-content','ribbon','wrapping','customImage']);
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = false;
                    $response['message'] = lang('something_went_wrong');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                }
            }
        }
    }


    //Choco Shap Add To Cart
    public function addChocoShapeToCart()
    {
        $itemType = $this->input->post('ItemType');
        if ($itemType == null) {
            $response['status'] = false;
            $response['message'] = lang('please_add_items_to_box');
            $response['total_cart_items'] = getTotalProduct($this->UserID);
            echo json_encode($response);
            exit();
        } else {
            $fetch_by['UserID'] = $this->UserID;
            $fetch_by['CustomizedOrderProductIDs'] = $this->input->post('shapeId');
            $fetch_by['ItemType'] = $this->input->post('ItemType');
            
            $fetch_by['CustomizedBoxID'] = $this->input->post('BoxID');
            $fetch_by['content_type_id'] = $this->input->post('ContentTypeId');
            $fetch_by['content_type_sub_id'] = $this->input->post('contentTypeSubId');
            $fetch_by['content_text'] = $this->input->post('customerContent');
            $fetch_by['content_type_sub_image_id'] = $this->input->post('contentTypeSubImageId');
            $fetch_by['FillColor'] = $this->input->post('fill_color');
            $fetch_by['Fillings'] = $this->input->post('filling');
            $fetch_by['Wrapping'] = $this->input->post('wrapping');
            $fetch_by['PowderColor'] = $this->input->post('powderColor');
            
            $posted_product_quantity = $this->input->post('Quantity');
            $posted_product_price = $this->input->post('totalPrice');
            $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
            if ($already_added) {
                $update['Quantity'] = $already_added->Quantity + $posted_product_quantity;
                $update_by['TempOrderID'] = $already_added->TempOrderID;
                $this->Temp_order_model->update($update, $update_by);
                $response['status'] = true;
                $response['message'] = lang('added_to_basket');
                $response['total_cart_items'] = getTotalProduct($this->UserID);
                echo json_encode($response);
                exit();
            } else {
                $save_data['UserID'] = $this->UserID;
                $save_data['Quantity'] = $posted_product_quantity;
                $save_data['CustomizedOrderProductIDs'] = $this->input->post('shapeId');
                $save_data['ItemType'] = $this->input->post('ItemType');
                $save_data['ProductID'] = $this->input->post('ItemType');
                $save_data['CustomizedShapeImage'] = $this->input->post('CustomizeShapeImage');
                $save_data['content_type_id'] = $this->input->post('ContentTypeId');
                $save_data['content_type_sub_id'] = $this->input->post('contentTypeSubId');
                $save_data['content_text'] = $this->input->post('customerContent');
                $save_data['content_type_sub_image_id'] = $this->input->post('customImageId')==''? $this->input->post('contentTypeSubImageId'):$this->input->post('customImageId');
                $save_data['TempItemPrice'] = $posted_product_price;
                $save_data['FillColor'] = $this->input->post('fill_color');
                $save_data['Fillings'] = $this->input->post('filling');
                $save_data['Wrapping'] = $this->input->post('wrapping');
                $save_data['PowderColor'] = $this->input->post('powderColor');
                $insert_id = $this->Temp_order_model->save($save_data);
                if ($insert_id > 0) {
                    $response['status'] = true;
                    $response['message'] = lang('added_to_basket');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    $this->session->unset_userdata(['customer-content','textType','content-type','fill_color','fillings','wrappings','powderColor','Quantity','shapeID','drawing']);
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = false;
                    $response['message'] = lang('something_went_wrong');
                    $response['total_cart_items'] = getTotalProduct($this->UserID);
                    echo json_encode($response);
                    exit();
                }
            }
        }
    }
}