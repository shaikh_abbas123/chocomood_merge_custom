<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model([
            'Temp_order_model',
            'Semsa_shipment_model',
            'Semsa_shipment_text_model']);
        $this->load->model('User_address_model');
        $this->load->model('Tax_shipment_charges_model');
        $this->load->model('Order_model');
        $this->load->model('Product_model');
        $this->data['language'] = $this->language;

    }

    public function index()
    {

        redirect(base_url('address'));//login change to that page
        if (isset($_REQUEST['p']) && $_REQUEST['p'] == 'checkout') {
            $this->checkout_address();
        }

        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($this->data['cart_items'])) {
            redirect('product');
        }
        $this->data['address'] = $this->User_address_model->getAddresses("user_address.IsDefault = 1 AND user_address.UserID = " . $this->UserID);
        $this->data['address_for_payment_collection'] = $this->User_address_model->getAddresses("user_address.UseForPaymentCollection = 1 AND user_address.UserID = " . $this->UserID);

        // checking here if collect from store cookie is not set and default address and payment collection address are not found then set flash message to select address properly and redirect to same page again
        if (!isset($_COOKIE['CollectFromStore'])) {
            if (empty($this->data['address']) || empty($this->data['address_for_payment_collection'])) {
                $this->session->set_flashdata('message', 'You must select address for delivery and payment collection in order to proceed with checkout');
                redirect(base_url('address'));
            }
        }

        $this->data['available_cities'] = $this->Product_model->getCities(false,$this->language);

        $this->data['view'] = 'frontend/checkout-payment';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function checkout_address()
    {
        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($this->data['cart_items'])) {
            redirect(base_url());
        }
        $this->data['address'] = $this->User_address_model->getAddresses("user_address.IsDefault = 1 AND user_address.UserID = " . $this->UserID);
        $this->data['view'] = 'frontend/checkout-address';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function thank_you($order_id = '')
    {
        $this->session->unset_userdata('order_coupon');
        $this->session->unset_userdata('DeliveryStoreID');
        $this->session->unset_userdata('DeliveryStoreTitle');
        $order = new Order_model();
        // $this->data['order'] = $this->Order_model->getUserLastOrderDetails($this->UserID, $this->language);
        if($order_id == ''){
            $where = "orders.UserID = " . $this->UserID;
        }else{
            $where = "orders.OrderID = " . base64_decode($order_id);
        }
        $temp_order = $order->getOrders($where, 1, 0, $this->language, 'DESC')[0];
        $this->data['order'] = $temp_order;
        //$this->data['payment_address'] = $this->User_address_model->getAddresses("user_address.AddressID = " . $this->data['order']->AddressIDForPaymentCollection)[0];
        $remaining_order = 0;
        if($temp_order->isCustomize == 1)
        {
            $wishlist = $this->Product_model->getWishlistItems($this->UserID, $this->language);
            foreach ($wishlist as $w) 
            {
                if($w->return_cart == 1)
                {
                    $default_package = 0;
                    $product_packages = (object)get_product_packages($w->ItemID,'EN');
                    foreach(@$product_packages as $k => $v) {
                        if(@$v['DefaultPackagesID'] == @$v['PackagesID'])
                        {
                            $default_package = @$v['PackagesProductID'];
                        }
                    }
                    $remaining_order = 1;
                    $this->addToCart($w->ProductID ,ucfirst($w->ItemType),1,$w->Price,0,$w->PriceType,$w->IsCorporateProduct,$default_package);
                }
            }
        }
        $this->data['remaining_order'] = $remaining_order;
        $this->data['view'] = 'frontend/checkout-done';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function addToCart($ProductID,$ItemType,$Quantity,$TempItemPrice,$Weight,$PriceType,$IsCorporateItem,$package)
    {
        $posted_product_quantity = $Quantity;
        $posted_product_price = $TempItemPrice;
        $Weight = $Weight;
        $priceType = $PriceType;
        
        $save_data['UserID'] = $this->UserID;
        $save_data['Quantity'] = $posted_product_quantity;
        $save_data['ProductID'] = $ProductID;
        $save_data['ItemType'] = $ItemType;
        $save_data['IsCorporateItem'] = $IsCorporateItem;
        $save_data['TempItemPrice'] = $posted_product_price;
        if($priceType == 'kg')
        {
            $save_data['Package'] = $package;
        }
        else
        {
            $save_data['Package'] = 0;
            $save_data['Weight'] = $Weight;
        }
        $insert_id = $this->Temp_order_model->save($save_data);
        if ($insert_id > 0) {
           return 1;
        } else {
            return 0;
        }
        
    }
    public function pay_tab($val)
    {
        //$this->session->unset_userdata('order_coupon');
        //$this->data['order'] = $this->Order_model->getUserLastOrderDetails($this->UserID, $this->language);
        /*$order_id = base64_decode($order_id);
        $order = new Order_model();
        $where = "orders.OrderID = " . $order_id ;
        $this->data['order'] = $order->getOrders($where, 1, 0, $this->language, 'DESC')[0];
        if(empty($this->data['order'])){
            redirect(base_url());
        
        }*/
        //print_rm( $this->session->userdata('StoreIDOrder'));die;
        $total_amount = base64_decode($val);
        $order = new Temp_order_model();
        $this->data['order'] = $order->getCartItems($this->UserID, $this->language);
        $this->data['order']['TotalAmount'] = $total_amount;
        //print_rm($this->data);die;
        if(empty($this->data['order'])){
            redirect(base_url());
        
        }
        //if($this->data['order']->TransactionID != ''){
        //print_rm($this->session->userdata);
        // $this->data['view'] = 'frontend/checkout-payment-paytab';
        // $this->load->view('frontend/layouts/default', $this->data);
        
        $curl = curl_init();
        $obj = new stdClass();
        $obj->name= "rami";
        $obj->email= "rami@henka.com.sa";
        $obj->phone= "";
        $obj->city= $this->data['order'][0]->UserCity;
        $obj->state= $this->data['order'][0]->UserCity;
        $obj->country= "SAU";
        $object = json_encode($obj);
        $order_id = json_encode($this->data['order'][0]->TempOrderID);
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://secure.paytabs.sa/payment/request",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{
                \n    \"profile_id\": 53608,
                \n    \"tran_type\": \"sale\",
                \n    \"tran_class\": \"ecom\" ,
                \n    \"cart_id\": $order_id,
                \n    \"cart_description\": \"Dummy Order 35925502061445345\",
                \n    \"cart_currency\": \"SAR\",
                \n    \"cart_amount\": $total_amount,
                \n    \"callback\": \"https://custom.henka.tech/customization/order/updatePaymentStatus\",
                \n    \"return\": \"https://custom.henka.tech/customization/order/updatePaymentStatus\",
                \n    \"customer_details\": $object
                \n  
          }",
          CURLOPT_HTTPHEADER => array(
            "authorization: SHJNMM6BNH-HZZB6NHNHW-WHRMDL9RKK",
            "cache-control: no-cache",
            "content-type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            // redirect($response['redirect_url']);
           $result = json_decode($response);
        //   print_rm($result);
           redirect($result->redirect_url);
        }
    }

    public function changeShipmentMethod()
    {
        $ShipmentMethodID = $this->input->post('ShipmentMethodID');
        $this->session->set_userdata('ShipmentMethodIDForBooking', $ShipmentMethodID);
         $this->unsetSemsaShipmentMethod();
        $where = "tax_shipment_charges.TaxShipmentChargesID = " . $ShipmentMethodID;
        $shipment_method = $this->Tax_shipment_charges_model->getAllJoinedData(false, 'TaxShipmentChargesID', $this->language, $where);
        $response['message'] = lang('shipment_method_changed');
        $response['amount'] = @$shipment_method[0]->Amount;
        echo json_encode($response);
        exit();
    }
    public function changeSemsaShipmentMethod()
    {
        $ShipmentMethodID = $this->input->post('ShipmentMethodID');
        /*if(!$this->session->userdata('SemsaShipmentID')){
            $this->unsetSemsaShipmentMethod();
        }else{*/
        if(!$this->session->userdata('SemsaShipmentID')){
            $this->session->set_userdata('SemsaShipmentID', trim($ShipmentMethodID, ' '));
        }
        //}
        $where = "semsa_shipment.IsActive = 1";
        $shipment_method = $this->Semsa_shipment_model->getAllJoinedDataWithTable('semsa_shipment',false, 'SemsaShipmentID', $this->language, $where);
        $response['message'] = lang('shipment_method_changed');
        $response['Title'] = @$shipment_method[0]->Title;
        $response['amount'] = @findSemsaShippingCharges();
        $response['FifteenKgPrice'] = @$shipment_method[0]->FifteenKgPrice;
        $response['AdditionalKgPrice'] = @$shipment_method[0]->AdditionalKgPrice;
        echo json_encode($response);
        exit();
    }
    public function unsetShipmentMethod()
    {
        
        $this->session->unset_userdata('ShipmentMethodIDForBooking', 0);
        
    }
    public function unsetSemsaShipmentMethod()
    {
        
        $this->session->unset_userdata('SemsaShipmentID');
        
    }
    public function unsetShipmentMethodID()
    {
       $this->session->unset_userdata('ShipmentMethodIDForBooking', 0);
        $this->unsetSemsaShipmentMethod();
        echo true;
        
    }


    public function changePaymentMethod()
    {
        $PaymentMethod = $this->input->post('PaymentMethod');
        $this->session->set_userdata('PaymentMethodForBooking', $PaymentMethod);
        $response['message'] = lang('payment_method_changed');
        echo json_encode($response);
        exit();
    }

    public function changeDeliveryStoreID()
    {
        $StoreID = $this->input->post('StoreID');
        $StoreTitle = $this->input->post('StoreTitle');
        $this->session->set_userdata('DeliveryStoreID', $StoreID);
        $this->session->set_userdata('DeliveryStoreTitle', $StoreTitle);
        $data = getCustomRow("Select * from stores where StoreID=". $StoreID);
        $response['payment_method'] = explode(',',$data['PaymentID']);
        $response['shipping_method'] = explode(',',$data['ShippingID']);
        $response['message'] = lang('delivery_store_id_selected');
        echo json_encode($response);
        exit();
    }

    public function unsetDeliveryStoreID()
    {
        
        $this->session->unset_userdata('DeliveryStoreID');
        $this->session->unset_userdata('DeliveryStoreTitle');
        $response['message'] = lang('payment_method_changed');
        echo json_encode($response);
        exit();
        
    }

    

}