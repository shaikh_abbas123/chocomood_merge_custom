<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model('Ticket_model');
        $this->load->model('Ticket_comment_model');
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->data['language'] = $this->language;
    }

    public function createTicket()
    {
        $post_data = $this->input->post();
        $checkIfAlreadyExist = $this->Ticket_model->getWithMultipleFields($post_data);
        if ($checkIfAlreadyExist) {
            $response['status'] = false;
            $response['message'] = lang('ticket_already_generated');
            echo json_encode($response);
            exit();
        } else {
            $post_data['TicketNumber'] = time() . $post_data['OrderID'];
            $post_data['CreatedAt'] = date('Y-m-d H:i:s');
            $inserted_id = $this->Ticket_model->save($post_data);
            if ($inserted_id > 0) {
                $update_ticket_by['TicketID'] = $inserted_id;
                $update_ticket['TicketNumber'] = str_pad($inserted_id, 5, '0', STR_PAD_LEFT);
                $this->Ticket_model->update($update_ticket, $update_ticket_by);

                $ticket_comment['TicketID'] = $inserted_id;
                $ticket_comment['UserID'] = 0;
                $ticket_comment['Message'] = "Welcome to chocomood support. How may I help you?<br>مرحبا بكم في الدعم. كيف يمكنني مساعدتك؟";
                $ticket_comment['IsRead'] = 1;
                $ticket_comment['CreatedAt'] = date('Y-m-d H:i:s');
                $this->Ticket_comment_model->save($ticket_comment);
                $response['status'] = true;
                $response['message'] = lang('ticket_raised');
                $response['TicketID'] = $inserted_id;
                $response['ComplaintDetail'] = "Complain No. " . $update_ticket['TicketNumber'] . "<span>Submitted on " . date('d-m-Y h:i:s A', strtotime($post_data['CreatedAt'])) . "</span>";
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('something_went_wrong');
                echo json_encode($response);
                exit();
            }
        }
    }

    public function getMessagesForTicket()
    {
        $TicketID = $this->input->post('TicketID');
        $messages = $this->Ticket_comment_model->getTicketComments(array('TicketID' => $TicketID));
        $ticket = $this->Ticket_model->get($TicketID, false, 'TicketID');
        $order = $this->Order_model->get($ticket->OrderID, false, 'OrderID');
        $html = '';
        foreach ($messages as $message) {
            $WhoSaid = ($message->UserID == $this->session->userdata['user']->UserID) ? "Customer" : "Support";
            $SentReceived = ($message->UserID == $this->session->userdata['user']->UserID) ? "msgsent" : "msgreceive";
            $html .= '<div class="' . $SentReceived . '">
                        <p>' . $message->Message . '</p>
                    </div>';
        }
        $response['html'] = $html;
        $response['OrderNumber'] = $order->OrderNumber;
        $response['TotalAmount'] = number_format($order->TotalAmount, 2) . ' SAR';
        $response['IsClosed'] = $ticket->IsClosed;
        echo json_encode($response);
        exit();
    }

    public function saveMessage()
    {
        $post_data = $this->input->post(); // Message, TicketID
        $post_data['UserID'] = $this->session->userdata['user']->UserID;
        $post_data['IsRead'] = 1;
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $inserted_id = $this->Ticket_comment_model->save($post_data);
        if ($inserted_id > 0)
        {
            $messages = $this->Ticket_comment_model->getTicketComments(array('TicketID' => $post_data['TicketID']));
            $this->pusherCall($messages, $post_data['TicketID']);
            $ticket = $this->Ticket_model->get($post_data['TicketID'], false, 'TicketID');
            $order = $this->Order_model->get($ticket->OrderID, false, 'OrderID');
            $html = '';
            foreach ($messages as $message) {
                $WhoSaid = ($message->UserID == $this->session->userdata['user']->UserID) ? "Customer" : "Support";
                $SentReceived = ($message->UserID == $this->session->userdata['user']->UserID) ? "msgsent" : "msgreceive";
                $html .= '<div class="' . $SentReceived . '">
                        <p>' . $message->Message . '</p>
                    </div>';
            }
            $response['TicketID'] = $post_data['TicketID'];
            $response['html'] = $html;
            $response['message'] = lang('ticket_message_sent');
            echo json_encode($response);
            exit();
        } else {
            $response['TicketID'] = $post_data['TicketID'];
            $response['message'] = lang('something_went_wrong');
            echo json_encode($response);
            exit();
        }
    }

    private function pusherCall($messages, $TicketID)
    {
        $html = '';
        foreach ($messages as $message) {
            $WhoSaid = ($message->UserID == 0) ? "Support" : "Customer";
            $class = $message->UserID == 0 ? "label-danger" : "label-success";
            $right_left = $message->UserID == 0 ? "pull-right" : "pull-left";
            $SentReceived = ($message->UserID == 0) ? "msgreceive" : "msgsent";
            $html .= '<div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="timeline-panel '.$right_left.'">
                                            <div class="timeline-body">
                                                <p><span class="label '.$class.'">'.$WhoSaid.'</span>&nbsp;&nbsp;&nbsp;&nbsp;'.$message->Message.'</p>
                                            </div>
                                            <small>
                                                <i class="ti-time"></i> '.date('d.m.Y h:i:s A', strtotime($message->CreatedAt)).'
                                            </small>
                                        </div>
                                    </div>
                                </div>';
        }
        //echo $html;exit();
        $pusher_data['my_html'] = $html;
        // dump($pusher_data);
        $pusher_data['TicketID'] = $TicketID;
        pusher($pusher_data, "Chocomood_Ticket_Channel_".$TicketID, "Chocomood_Ticket_Event_".$TicketID);
        // pusher($pusher_data, 'Chocomood_Ticket_Channel_Admin', 'Chocomood_Ticket_Event_Admin');
    }
}