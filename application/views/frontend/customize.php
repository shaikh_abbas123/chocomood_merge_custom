<style>
    .footer-widgets {
        margin-top:0px;
    }
    .content.customize .captionbox .inner , .content.customize .captionbox1 .inner{
        position: relative;
        top: calc(50% - 80px);
    }
    .content.customize .captionbox1 {
        top: 0;
        height: 100%;
    }
    .content
    {
        margin: 0px !important;
    }
</style>
<?php if($customizations){ 
    $count =0;
    $class = ['first-1', 'second', ' third','fourth', 'fifth'];
    foreach ($customizations as $key => $value) { ?>
<section class="content customize first <?= $class[$count]?>" style="background-size:cover !important;background:url(<?php echo base_url($value->Image); ?>);">
    <img src="">
    <div class="bannerbox">
        
        <div class="<?= ($count%2 == 0)?'captionbox':'captionbox1'?>">
            <div class="inner">
                <h2><?= limit_string($value->Title, 50); ?></h2>
                <p class="para"><?=limit_string($value->Description, 100); ?></p>
                <a href="<?php echo base_url('customize/'.@$value->link);?>" class="btn btn-secondary"><?php echo lang('customize_now'); ?></a>
            </div>
        </div>
    </div>
</section>

        <?php 
        $count++;
    }

} ?>





