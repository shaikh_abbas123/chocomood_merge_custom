<style type="text/css">
    .no-js .owl-carousel, .owl-carousel.owl-loaded {
        display: block;
    }
    .owl-carousel .owl-dots.disabled, .owl-carousel .owl-nav.disabled {
        display: block;
    }
    .start-customize a{
        display:inline-flex;
    }
    div.box-details p {
        font-weight: 100;
        font-size: 14px;
        text-transform: capitalize;
        letter-spacing: 1px;
        color: #606060;
        margin-right: 0 ;
        margin-left: 0 ;
    }

    @media (max-width: 991px){
        .order-first {
          -ms-flex-order: -1;
          order: -1
        }
    }
</style>

<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?= $BoxCategory[0]->Title;?></li>
                    <li><?= $box[0]->Title; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="customize-box-details container">
    <!-- details content -->
    <div class="row align-items-center details-content-info mt-3">
        <!-- main detils -->
        <!-- name and description -->
        <div class="col-xl-4 col-lg-4 col-md-4 col-xs-12 col-sm-12 order-lg-0 order-md-1 pb-4">
            <!-- box details container -->
            <div class="product-details-container">
                <!-- details -->
                <div class="product-details">
                    <!-- box name -->
                    <h1 class="box-name"><?= $box[0]->Title; ?></h1>
                    <!-- box details -->
                    <div class="box-details">
                        <?= $box[0]->Description; ?>
                    </div>
                    <!-- box specifications  -->
                    <div class="row">
                        <!-- quantity -->
                        <div class="col-md-12">
                            <!-- quantity title -->
                            <span class="quantity_title d-block">Quantity:</span>
                            <!-- number of pieces for 1 KG -->
                            <table class="pieces_table specifications d-block w-100">
                                <tbody class="d-block">
                                    <tr class="d-block">
                                        <td class="w-auto numb-pieces-headng">Number of pieces for 1 Box:</td>
                                        <td class="w-auto numb-pieces-desc"> <?= $box[0]->BoxSpace; ?> PC`S</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- BOX DIMENSIONS -->
                        <div class="col-md-12 specifications">
                            <!-- title -->
                            <span class="choco_box_detail d-block">BOX DETAIL</span>
                            <table class="specifications d-block w-100 choco-shape_printable_table">
                                <tbody class="d-block">
                                    <!-- kength -->
                                    <tr class="d-block">
                                        <td class="printable-type">Price :</td>
                                        <td class="printable-type text-right"><?= $box[0]->BoxPrice ?></td>
                                    </tr>
                                    <!-- width -->
                                    <tr>
                                        <td class="printable-type">No of Rows :</td>
                                        <td class="printable-type text-right"><?= $box[0]->no_of_row ?></td>
                                    </tr>
                                    <!-- hight -->
                                    <tr>
                                        <td class="printable-type">No of Choclates in Row :</td>
                                        <td class="printable-type text-right"><?= $box[0]->no_of_choclate_in_row ?></td>
                                    </tr>
                                    <!-- width -->
                                    <tr>
                                        <td class="printable-type">Height :</td>
                                        <td class="printable-type text-right"><?= $box[0]->height ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- WINDOW DIMENSIONS -->
                        <div class="col-md-12 specifications">
                            <!-- title -->
                            <span class="choco_box_detail d-block">BOX DETAIL</span>
                            <table class="specifications d-block w-100">
                                <tbody class="d-block">
                                    <!-- lrngth -->
                                    <tr class="d-block">
                                        <td class="printable-type">Min Order :</td>
                                        <td class="printable-type text-right"><?= $box[0]->MinOrder ?></td>
                                    </tr>
                                    <!-- width -->
                                    <tr>
                                        <td class="printable-type">Processing Time :</td>
                                        <td class="printable-type text-right"><?= $box[0]->orderProcessing ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- start customize box gallary -->
        <div class="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-sm-12 order-first order-md-0 d-flex justify-content-center pt-4 pb-4">
          <img src="<?php echo base_url($box[0]->BoxImage); ?>" width="300px" height="300px" alt="">      
        </div>
        <div class="pull-center-mobile start-customize text-center">
            <a <?php 

                    if($BoxCategoryId == 1){
                        ?>
                        href="<?php echo base_url('customize/customize_printable_box/'.base64_encode(@$box[0]->BoxID)); ?>"
                        <?php
                    }else{
                        if ($box[0]->PriceType == 'kg')
                        {
                            ?>
                            href="<?php echo base_url('customize/choco_box_gram/'.base64_encode(@$box[0]->BoxID)); ?>"
                            <?php
                        }
                        if ($box[0]->PriceType == 'item')
                        {
                            ?>
                            href="<?php echo base_url('customize/choco_box_item/'.base64_encode(@$box[0]->BoxID)); ?>"
                            <?php
                        }
                    }
                ?>
                >
                Start Customization
                <span class="d-inline-block pl-2">
                    <svg class="svg-inline--fa fa-pencil-alt fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                        <path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path>
                    </svg><!-- <i class="fas fa-pencil-alt"></i> -->
                </span>
            </a>
        </div>
    </div>
    <!-- end customize box gallary -->
    <!-- start what`s inside -->
    
    <div class="row mt-5 mb-5">
        <div class="col-12">
            <!-- title -->
            <h1 class="text-center what-inside-title">What`s Inside</h1>
        </div>
        <div class="col-md-12">
            <div id="carousel2" class="owl-carousel owl-theme what-inside">
                <!-- chocolate item -->
                
                    <?php
                        foreach ($inside_products as $v) {
                        $product_images = get_images($v[0]->ProductID, 'product');
                        ?>
                        <div class="item">
                            <img src="<?php echo base_url() . $product_images[0]->ImageName; ?>" alt="" width="250px" height="250px">
                        </div>
                        <?php
                        }
                    ?>
                
            </div> 
        </div>


    </div>
    <!-- end whats inside -->
</section>
<script>
    jQuery("#carousel2").owlCarousel({
      items: 4,
      autoplay: true,
      nav: true,
      dots: true
    });
</script>