<style>
.mb-5{
    margin-bottom:220px;
}
.p-5{
    padding:50px;
}
.bd-bottom{
  border-bottom: 2px solid #a0725091;
  padding-top: 15px;
  padding-bottom: 15px;
  text-align: right;
}
.sort-drp-down{
  border: 1px solid #a07250;
  padding: 5px;
  text-transform: uppercase;
  letter-spacing: 1px;
  color: #a0725094;
  font-size: 11px;
  padding-right: 20px;
}
.sort-drp-down:focus-visible{
  border: 1px solid #a07250 !important;
  outline: none !important;
}
.box-title-new{
  font-size: 20px;
  color: #2d2d2d;
}
.text-dark-shade{
  color:#2d2d2d !important;
}
.p-font{
  font-family: sans-serif;
  letter-spacing: 1px;
}
.mt-10{
  margin-top: 40px;
}
.bg-custom-header {
    min-height: 250px;
    margin: 0px 0px 0px 0px;
    max-height: 100%;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}
.pagination-icon-default{
  width: 25px !important;
  height: 25px !important;
  padding: 5px;
  border: 1px solid #bd9371;
  color: #bd9371;
}
.pagination-span{
  border: none;
  width: auto !important;
  height: auto !important;
  padding: 0 10px !important;
  font-size: 26px;
  font-family: sans-serif;
}
.hr-bd{
  border-color: #a07250a8;
  border-width: 2px;
}
.select-box-continue{
  color: #0000008c;
  letter-spacing: 2px;
}
</style>

<section class="content customize bg-custom-header" style="background:url(<?= base_url('uploads/images/rose-box-bg.png') ?>);">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="bannerbox">
          <div class="choco-shape_captionbox">
              <div class="inner">
                  <h2 style="letter-spacing: 5px;"><?= @$BoxCategory[0]->Title?></h2>
                 
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>    
</section>

<section class="container mt-5">
  <div class="row">
    <div class="col-md-12 bd-bottom">
      <select name="sort" id="sort" class="sort-drp-down">
        <option value="1">Sort by</option>
        <option value="1">Sort by</option>
        <option value="1">Sort by</option>
      </select>
      <select name="prouctperpage" id="prouctperpage" class="sort-drp-down">
        <option value="1">Prouct per Page</option>
        <option value="1">Sort by</option>
        <option value="1">Sort by</option>
      </select>
    </div>
  </div>
</section>

<section class="content titlarea" style="padding-bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li><?= lang('home')?></li>
                    <li><?= lang('customize')?></li>
                    <li><?= $BoxCategory[0]->Title;?></li>
                    <li><?= lang('boxes')?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class=" container mb-4">
      <!-- bannar -->
      <!-- <div class="row box-item">
        <div class="col-md-12 box-image" style="background-image: url(<?php echo base_url($BoxCategory[0]->Image); ?>);">
            <div class="row align-items-end justify-content-start h-100 content-row">
                <div class="col-md-6 p-3 box-content rounded-right-top">
                    <h4 class="title"><?= $BoxCategory[0]->Title;?></h4>
                    <p class="description position-relative">
                    <?= $BoxCategory[0]->Description;?>
                    </p>
                </div>
            </div>
        </div>
      </div> -->
      <div class="row">
      <?php
        foreach ($Boxes as $key => $item) {
            ?>
        <div class="col-md-3 text-center p-3 pb-5 mt-10">
          <a href="<?php echo base_url('customize/start_customize_choco_box/'.base64_encode(@$item->BoxID).'/'.base64_encode($item->BoxCategory)); ?>">
            <img src="<?php echo base_url($item->BoxImage); ?>" alt="box-1" class="img-fluid" style="width: 245px;height: 200px;">
            <h2 class="box-title-new font-weight-bold m-0"><?= $item->Title?> (<?= ($item->PriceType == 'item')?$item->BoxSpace.' PCS':$item->weight.' GM'?>)</h2>
            <p class="font-weight-light text-dark-shade m-0 p-font"> <?= $item->BoxPrice?> <?= lang('sar')?></p>
          </a>
        </div>
        <?php 
        }
        ?>
      </div>
      <div class="row mt-5">
        <div class="col-md-12 text-center">
          <a href="#"><i class="fa fa-backward pagination-icon-default" aria-hidden="true"></i></a>
          <a href="#"><i class="fa fa-play pagination-icon-default" aria-hidden="true" style="transform: rotate(180deg);padding:7px;"></i></a>
          <span class="pagination-icon-default pagination-span">1</span>
          <a href="#"><i class="fa fa-play pagination-icon-default" aria-hidden="true" style="padding:7px;"></i></a>
          <a href="#"><i class="fa fa-forward pagination-icon-default" aria-hidden="true"></i></a>
          <hr class="hr-bd" />
          <p class="text-uppercase text-right mb-5 select-box-continue"><?= lang('select_box')?> <i class="fa fa-play" aria-hidden="true" style="color:#bd9371;font-size: 10px;margin-left: 10px;"></i></p>
        </div>
      </div>
        
            
    </section>

