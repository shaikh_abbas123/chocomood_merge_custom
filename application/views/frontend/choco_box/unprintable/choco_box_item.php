<style>
    .custom-title-h2{
        color: #69411f;
        font-size: 38px;
        letter-spacing: 2px;
        margin-bottom: 0px;
    }
    .bd-bottom{
        border-bottom: 1px solid #b67b61;
    }
    .dark-brown{
        color: #9b5717 !important;
    }
    .dark-gray{
        color: #777777 !important;
    }
    /* div.box-details p {
        font-weight: 100;
        font-size: 14px;
        text-transform: capitalize;
        letter-spacing: 1px;
        margin: 0 0 3px;
        padding: 0;
    } */
    .box_order-details tbody tr td , .details h5 {
        font-weight: 100;
        font-size: 14px;
        text-transform: uppercase;
        letter-spacing: 1px;
    }
    .total_price {
        padding-top: 5px;
        border-top: 2px solid #A07250;
    }
    .box-counter .counter button.box-item-total-price {
        width: 25px;
        height: 25px;
        font-size: 20px;
    }
    .box-counter .counter input {
        font-size: 20px;
    }
</style>
<section class="chocolate-box-container container-fluid mb-4 " id="customize-chocolate-box">
        <form class="form-row" action="" method="POST" id="chocobox_form">
        <input type="hidden" value="<?= @$printable['printable'] ?>" name="boxType">
        <input type="hidden" value="<?= @$printable['contentType'] ?>" name="contentType">
        <input type="hidden" value="<?= @$printable['contentTypeSub'] ?>" name="contentTypeSub">
        <input type="hidden" value="<?= @$printable['contentTypeSubImage'] ?>" name="contentTypeSubImage">
        <input type="hidden" value="<?= @$printable['customertext'] ?>" name="customerContent">
        <input type="hidden" value="<?= @$printable['ribbon'] ?>" name="ribbon">
        <input type="hidden" value="<?= @$printable['wrapping'] ?>" name="wrapping">
        <input type="hidden" value="<?= @$printable['customImage'] ?>" name="customImageId">
        <input type="hidden" value="<?= @$printable['wrapping_price'] ?>" name="wrapping_price">
        <input type="hidden" value="<?= @$printable['ribbon_price'] ?>" name="ribbon_price">
        <input type="hidden" value="<?= @$printable['content_type_price'] ?>" name="content_type_price">
        <input type="hidden" value="<?= @$printable['content_type_sub_price'] ?>" name="content_type_sub_price">
            <!-- start chocolate gallary for box -->
            <div class="col-xl-8 col-lg-8 col-md-12 my-5 ">
                <div class="row chocolate-for-box  py-4 ">
                    <!-- chocolate for box item -->
                    <?php
                        foreach ($inside_products as $v) {
                        $product_images = get_images($v[0]->ProductID, 'product');
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <div style="width:150px;height: auto; overflow:hidden;margin:0px auto">
                            <img src="<?php echo base_url() . $product_images[0]->ImageName; ?>" alt="" class="transform-scale-2" width="100px" height="auto">
                        </div>
                        <!-- chocolate for box content -->
                        <p class="chocolate-title mt-2 mb-1 text-muted">
                            <!-- name -->
                            <b><?= $v[0]->Title?></b>
                            <small>(<span class="chocolate-weight"><?= $v[0]->Weight?></span> gm)</small>
                        </p>
                        <p class="w-100 product-desc-one-line mx-0"><?= $v[0]->Description?></p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase chocobox_increase" data-product_price="<?php echo number_format($v[0]->Price, 2); ?>" data-product_id="<?= $v[0]->ProductID?>" data-box_size="<?php echo $box[0]->BoxSpace; ?>">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input disabled="true" class="form-control quantity-count" type="text" value="0" 
                                name="chocolate-<?= $box[0]->BoxID;?>" data-chocolate="<?= $v[0]->ProductID?>" data-chocolate-name="<?= $v[0]->Title ?>">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease chocobox_decrease"  data-product_price="<?php echo number_format($v[0]->Price, 2); ?>" data-product_id="<?= $v[0]->ProductID?>" data-box_size="<?php echo $box[0]->BoxSpace; ?>">-</button>
                        </div>
                    </div>
                    <?php
                        }
                    ?>

                </div>
            </div>
            <!-- end chocolate gallary for box -->
            <!-- start chocolate in box -->
            <div class="col-xl-4 col-lg-4 col-md-12 mt-5 order-first order-lg-last mb-5">
                <div class="row w-auto chocolate-box-details">
                    <div class="col-md-12 position-relative">
                        <!-- this is the image for box -->
                        <img id="box" src="<?php echo base_url($box[0]->BoxImageInside); ?>" alt="" class="" width="100%" height="">
                        <!-- this is the container which save chocolate and view it over box image 
                            - must add the type for box in => data-box-type attribute => [by gm , by pcs]
                            - must add th max quantity for box in => data-max-quantity attribute
                            - must add th max number of rows for box in => data--count-rows attribute
                            - must add th add the max number for each row for box in => data-chocolate-count-in-rows attribute
                            - it must be in order and the sepration between each number is -
                        -->
                        <?php
                            $box_row_choclates = '';
                            for($i=0;$i<$box[0]->no_of_row;$i++)
                            {
                                $box_row_choclates.= $box[0]->no_of_choclate_in_row;
                                if($i != $box[0]->no_of_row-1)
                                {
                                    $box_row_choclates.= '-';
                                }
                            }
                        ?>
                        <div class="row w-100 p-4 position-absolute chocolate-choosen" data-box-type="PCS" data-max-quantity="<?= $box[0]->BoxSpace?>" data-count-rows="<?= $box[0]->no_of_row?>" data-chocolate-count-in-rows="<?= $box_row_choclates?>">
                            <div class="col-md-12 chocolate-choosen-container">

                                <?php
                                
                                    for($i=0;$i<$box[0]->no_of_row;$i++)
                                    {
                                        ?>
                                            <div class="row chocolate-choosen-row" style="height: 156.207px; width: 780px;">

                                            <?php 
                                                
                                                for($j=0; $j < $box[0]->no_of_choclate_in_row; $j++)
                                                {
                                                    ?>
                                                    
                                                    <div class="col-md-2 col-sm-2 col-xs-2 chocolate-choosen-item p-1">
                                                        <div class="chocolate-choosen-card"></div>
                                                    </div>

                                                    <?php
                                                }
                                            ?>
                                            </div>
                                        <?php
                                    }
                                ?>
                              

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 p-5 mt-5" style="border-radius: 1.5rem;box-shadow: 0 .5rem 1rem #A07250;">
                    <h2 class="text-center choco-grm-headng">PLEASE SELECT FLAVORS & QUANTITIES UNTIL THE BOX IS FULL</h2>
                        <!-- title  -->
                        <h3 class="title"> <?= $box[0]->Title; ?> </h3>
                        <!-- description -->
                        <!-- <div class="m-0 text-grey box-details">
                            <p><?= $box[0]->Description; ?></p>
                        </div> -->
                        <!-- box dimensions -->
                        <div class="details d-xl-block ">
                            <div class="row">
                                <div class="col-md-7">
                                    <h4 class="mt-4">BOX DIMENSIONS</h4>
                                    <table class="text-grey box_order-details">
                                        <tbody><tr>
                                            <td>Height:</td>
                                            <td class="pl-3"><?= $box[0]->height ?></td>
                                        </tr>
                                        <tr>
                                            <td>No of Rows:</td>
                                            <td class="pl-3"><?= $box[0]->no_of_row ?></td>
                                        </tr>
                                    </tbody></table>
                                </div>
                                <div class="col-md-5">
                                    <!-- order nots -->
                                    <h4 class="mt-4 text-left">ORDER NOTES</h4>
                                    <table class="text-grey box_order-details">
                                        <tbody><tr>
                                            <td>Minimum Order:</td>
                                            <td class="pl-3"><?= $box[0]->MinOrder ?></td>
                                        </tr>
                                        <tr>
                                            <td>Processing Time:</td>
                                            <td class="pl-3"><?= $box[0]->orderProcessing ?></td>
                                        </tr>

                                    </tbody></table>
                                </div>
                            </div>
                            
                            
                            <h4 class="mt-4" data-value="0" data-toggle="collapse" href="#collapseExample" role="button">WHATS INSIDE <span class="pull-right"><i class="fa fa-caret-down"></i></span></h4>
                            <ul class="list-group list-unstyled chocolate-in-box collapse border-0 m-0 p-0" id="collapseExample">
                                
                            </ul>
                            <!-- price for 1 bpx -->
                            <?php if(isset($printable)){?>
                            <h5 class="mt-4 text-grey">Wrapping Price: <span class="pull-right"><?= isset($printable['wrapping_price'])?$printable['wrapping_price']:'0.00' ?> SAR</span>  </h5>
                            <h5 class="mt-4 text-grey">Ribbon Price: <span class="pull-right"><?= isset($printable['ribbon_price'])?$printable['ribbon_price']:'0.00' ?> SAR</span>  </h5>
                            <h5 class="mt-4 text-grey">Content Type Price: <span class="pull-right"><?= $printable['content_type_price']+$printable['content_type_sub_price'] ?> SAR</span> </h5>
                            <h5 class="mt-4 text-grey">Box Price: <span class="pull-right"><?= $box[0]->BoxPrice ?> SAR</span> </h5>
                            <h4 class="mt-4 text-grey total_price"><strong>TOTAL PRICE:</strong> <span class="pull-right ProductP"><?= $box[0]->BoxPrice+$printable['wrapping_price']+$printable['ribbon_price']+$printable['content_type_price']+$printable['content_type_sub_price'] ?> SAR</span> </h4>
                            <?php }else{ ?>
                                <h5 class="mt-4 text-grey">Box Price: <span class="pull-right "><?= $box[0]->BoxPrice ?> SAR</span></h5>
                                <h4 class="mt-4 text-grey total_price"><strong>TOTAL PRICE:</strong> <span class="pull-right ProductP"><?= $box[0]->BoxPrice ?>SAR</span></h4>
                            <?php } ?>
                        </div>
                        <!-- counter -->
                        <div class="quantity">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-md-3 col-sm-3 col-xs-3 ">
                                    <h4 class="title mb-0"><strong>Quantity</strong></h4>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9 box-counter text-right mb-4">
                                    <div class="counter">
                                        <!-- increas -->
                                        <button type="button" class="btn btn-primary increase box-item-total-price">+</button>
                                        <input class="form-control quantity-count" type="text" value="1"
                                            name="Quantity" data-box="https://via.placeholder.com/150">
                                        <button type="button" class="btn btn-primary decrease box-item-total-price">-</button>
                                    
                                    <!-- <input name="Quantity" style="font-size: 28px" type="number" class="box-item-total-price" value='1'> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- add to cart -->
                        <div class=" d-xl-block">
                            <input type="hidden" class="ProductIDs" name="ProductIDs">
                            <input type="hidden" class="ItemType" name="ItemType" value="Choco Box">
                            <input type="hidden" class="TotalPrice" name="TotalPrice" value="<?php echo $box[0]->BoxPrice; ?>">
                            <input type="hidden" class="BoxID" name="BoxID" value="<?php echo $box[0]->BoxID; ?>">
                            <button type="button" class="btn btn-primary btn-block cart-n-buy"  onclick="addChocoBoxToCart_box();"><?php echo lang('add_to_basket'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            
        </form>
        <a class="btn btn-primary fixed-bottom rounded-circle" id="show-box-btn" href="#customize-chocolate-box" ><i
                class="fas fa-arrow-up"></i></a>
</section>
<script>

// another

$(document).ready(function(){
    if(document.getElementById("customize-chocolate-box")!=null && $('.chocolate-choosen').attr('data-box-type').toLowerCase() == 'pcs'){
    var chocolateChoosed = [];
    var maxQuantity      = $('.chocolate-choosen').attr('data-max-quantity'); 
    var rowsCount      = $('.chocolate-choosen').attr('data-count-rows');
    var chocolateInRow     = $('.chocolate-choosen').attr('data-chocolate-count-in-rows').split("-");
    document.querySelector(".chocolate-choosen-container").innerHTML='';
    for(row=0;row<rowsCount;row++) {
        document.querySelector(".chocolate-choosen-container").innerHTML += `
            <div class="row chocolate-choosen-row"> </div>
        `;
        document.querySelectorAll(".chocolate-choosen-row")[row].innerHTML='';        
        for(cr= 0; cr<chocolateInRow[row];cr++){
            document.querySelectorAll(".chocolate-choosen-row")[row].innerHTML +=`
            <div class="col-md-2 col-sm-2 col-xs-2 chocolate-choosen-item p-1">
                <div class="chocolate-choosen-card">
                </div>
            </div>
        `
        }
        
    }
    var count = 0;

    // $(`.chocolate-for-box input.quantity-count`).on('focus' ,(e)=> {
    //     count=0
    //     $(`.chocolate-for-box input.quantity-count`).each( num => {
    //         count=count+Number($(`.chocolate-for-box input.quantity-count:eq(${num})`).val());
    //     });
    //     console.log(count);
        
    // })
    // $(`.chocolate-for-box input.quantity-count`).on('blur' ,(e)=> {
    //     count=0
    //     $(`.chocolate-for-box input.quantity-count`).each( num => {
    //         count=count+Number($(`.chocolate-for-box input.quantity-count:eq(${num})`).val());
    //     });
    //     console.log(count);
    // })
    $(`.chocolate-for-box input.quantity-count`).on('keyup' , (e)=>{



        var $input = $(`input.quantity-count:eq(${$('input.quantity-count').index(e.target)})`);
        var chocolateInArray = chocolateChoosed.filter( chocolate => {
            return chocolate == e.originalEvent.path[2].querySelector("img").getAttribute("src") == true
        });
        count=0
        $(`.chocolate-for-box input.quantity-count`).each( num => {
            count=count+Number($(`.chocolate-for-box input.quantity-count:eq(${num})`).val());
        });
        count = count -$input.val()
        console.log('count =>'+count);
        var $inputValue = 0;
        if(count == maxQuantity){
            $input.val(0)
        }
        else if($input.val() < maxQuantity && count == 0) {
            $inputValue= $input.val();
        }else if ($input.val() < maxQuantity && count > 0 && count <= maxQuantity){
            if($input.val() > maxQuantity - count){
                $inputValue= $input.val(maxQuantity - count);
            } else {
                $inputValue= $input.val();
            }
            
        }else if ($input.val() > maxQuantity && count == 0 && $input.val() > 10){
            $inputValue = $input.val(maxQuantity)
            
        }else if ($input.val() >= maxQuantity && count > 0 && count <= maxQuantity && $input.val() > 10){
            $inputValue = $input.val(maxQuantity - count)
        } else if ($input.val() > maxQuantity && count > 0 && chocolateChoosed.length != maxQuantity){
            $inputValue = $input.val()
        }
        console.log($inputValue)



        if($input.val() > chocolateInArray.length) {
            for(ci=0;ci<$input.val() - chocolateInArray.length;ci++){
                chocolateChoosed.push(e.originalEvent.path[2].querySelector("img").getAttribute("src"));
            }
            chocolateChoosen();
            // setTimeout( () => {
            //     $input.blur()
            // }, 1000)
        }else if($input.val() < chocolateInArray.length){
                
            for(ci=0;ci<chocolateInArray.length - $input.val();ci++){
                var index = chocolateChoosed.lastIndexOf(e.originalEvent.path[2].querySelector("img").getAttribute("src"));
                chocolateChoosed.splice(index,1);
            }
            chocolateChoosen();
            // setTimeout( () => {
            //     $input.blur()
            // }, 1000)
        }

        
        
    })
    $('button.increase').click( (e) => {
        var $input = $(`input.quantity-count:eq(${$('button.increase').index(e.target)})`);

        
        if(e.originalEvent.path[2].className.indexOf('box-counter') == -1 && chocolateChoosed.length < maxQuantity){
            $input.val( +$input.val() + 1 );
            chocolateChoosed.push({
                                chocolate_name: $input.attr('data-chocolate-name'),
                                image: e.originalEvent.path[2].querySelector("img").getAttribute("src")
                            });
            // chocolateChoosed.push(e.originalEvent.path[2].querySelector("img").getAttribute("src"));
            chocolateChoosen();

        }else if(chocolateChoosed.length == maxQuantity){
            $('html, body').animate({
                scrollTop: 0
            }, 10, function(){
                
            });
        }else if(e.originalEvent.path[2].className.indexOf('box-counter') != -1){
            $input.val( +$input.val() + 1 );
        }
    })
    $('button.decrease').on('click', (e) => {
        var $input = $(`input.quantity-count:eq(${$('button.decrease').index(e.target)})`);
        if(e.originalEvent.path[2].className.indexOf('box-counter') != -1){
            if($input.val()> 1){
                
                $input.val( +$input.val() - 1 )
            }
        } else {
            if($input.val() > 0){
                
                $input.val( +$input.val() - 1 );
                var index = chocolateChoosed.map(el => el.chocolate_name).lastIndexOf($input.attr('data-chocolate-name')) ;
                // var index = chocolateChoosed.lastIndexOf(x=>x.originalEvent.path[2].querySelector("img").getAttribute("src"));
                chocolateChoosed.splice(index,1);
                chocolateChoosen()

            }
        }
        
        
    })

    var count_item = [];
    function chocolateChoosen(){
        var item = document.getElementsByClassName('chocolate-choosen-card');
        var item_name = [];
        
        for(c=0;c<item.length;c++){
            item[c].innerHTML = ``
        }
        for(c=0;c<chocolateChoosed.length;c++){
            item[c].innerHTML = `<img class="m-auto d-block" src="${chocolateChoosed[c].image}" width="100%" alt="">`;
            item_name.push(chocolateChoosed[c].chocolate_name);
            // $('.chocolate-in-box').html(chocolateChoosed[c].chocolate_name);
        }

        var html = '';
        count_item = findOcc(chocolateChoosed,'chocolate_name');
        console.log("chocolateChoosed",chocolateChoosed);
        for(c=0;c<count_item.length;c++)
        {
            html += '<li class="list-group-item p-3">'+count_item[c].chocolate_name+'<strong class="pull-right">('+count_item[c].occurrence+')</strong></li>';
        }
        $('.chocolate-in-box').html('');
        $('.chocolate-in-box').html(html);
        

        
    }

    function findOcc(arr, key){
        let arr2 = [];
        
    arr.forEach((x)=>{
        
        // Checking if there is any object in arr2
        // which contains the key value
        if(arr2.some((val)=>{ return val[key] == x[key] })){
            
        // If yes! then increase the occurrence by 1
        arr2.forEach((k)=>{
            if(k[key] === x[key]){ 
            k["occurrence"]++
            }
        })
            
        }else{
        // If not! Then create a new object initialize 
        // it with the present iteration key's value and 
        // set the occurrence to 1
        let a = {}
        a[key] = x[key]
        a["occurrence"] = 1
        arr2.push(a);
        }
    })
        
    return arr2
    }                                                                       
    var boxHeight = document.getElementById('box').clientHeight; 
    var boxWidth = document.getElementById('box').clientWidth;
    var rowsCount      = $('.chocolate-choosen').attr('data-count-rows'); 
    $('.chocolate-choosen-row').css({'height':boxHeight/5.8,'width':boxWidth/1.2});
    if(rowsCount > 0){
        $('#box').css({'height':((boxHeight/5.8)*rowsCount)*1.12,'width':((boxWidth/1.2)*rowsCount)*1.12});
    }
    window.onresize = ()=> {
        boxHeight = document.getElementById('box').clientHeight; 
        boxWidth = document.getElementById('box').clientWidth;
        rowsCount      = $('.chocolate-choosen').attr('data-count-rows');
        $('.chocolate-choosen-row').css({'height':boxHeight/5.8,'width':boxWidth/1.2});
        if(rowsCount > 0){
            $('#box').css({'height':((boxHeight/5.8)*rowsCount)*1.12,'width':((boxWidth/1.2)*rowsCount)*1.12});
        }    
    }
    if(window.scrollY >= 800){
        document.getElementById("show-box-btn").style.display="block";
    }else {
        document.getElementById("show-box-btn").style.display="none";
    }
    window.onscroll = () => {
        if(window.scrollY >= 800){
            document.getElementById("show-box-btn").style.display="block";
        }else {
            document.getElementById("show-box-btn").style.display="none";
        }
    }
    $("a#show-box-btn").on('click', function(event) {
        if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 0, function(){
            window.location.hash = hash;
        });
        } 
    });
    }else if((document.getElementById("customize-chocolate-box")!=null && $('.chocolate-choosen').attr('data-box-type').toLowerCase() == 'gm')){
        var chocolateChoosed    = [],
            maxQuantity         = Number($('.chocolate-choosen').attr('data-max-quantity')),
            count               = [0];
        $('button.increase').click( (e) => {
            if(e.originalEvent.path[2].className.indexOf('box-counter') == -1){ 
                var $input = $(`input.quantity-count:eq(${$('button.increase').index(e.target)})`);
                var checkInserted = chocolateChoosed.filter( chocolate => {
                    return chocolate.name == $input.attr('data-chocolate') == true
                });
                
                var weightInBox = count.reduce( (total,number)=>{
                    return total + number
                });
                if(checkInserted.length){
                    
                    if(Number($(`.chocolate-weight:eq(${$('button.increase').index(e.target)})`).text()) <= (maxQuantity-weightInBox) ) {
                        
                        $input.val( +$input.val() + 1 );
                        var index = chocolateChoosed.indexOf(checkInserted[0]);
                            chocolateChoosed[index].count = ++chocolateChoosed[index].count;
                    }else {
                        var stillWight = maxQuantity-weightInBox;
                        if(stillWight>0){
                            alert('choose a little weight still ' + stillWight + 'gm' )
                        }else {
                            alert('complate' )
                        }
                        
                    }
                } else{
                    if(Number($(`.chocolate-weight:eq(${$('button.increase').index(e.target)})`).text()) <= (maxQuantity-weightInBox) ) {
                        $input.val( +$input.val() + 1 );
                        chocolateChoosed.push({
                            name    :   $input.attr('data-chocolate'),
                            weight  :   Number($(`.chocolate-weight:eq(${$('button.increase').index(e.target)})`).text()),
                            count   :   Number($input.val()),
                        });
                    }else {
                        var stillWight = maxQuantity-weightInBox;
                        if(stillWight>0){
                            alert('choose a little weight still ' + stillWight + 'gm' )
                        }else {
                            alert('complate' )
                        }
                        
                    }
                    
                };
                count=[0]
                chocolateChoosed.map( chocolate => {
                    count.push(chocolate.count * chocolate.weight)
                });
                weightInBox = count.reduce( (total,number)=>{
                    return total + number
                });
                $('.chocolate-in-box *').remove()
                chocolateChoosed.forEach( chocolate => {
                    return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                });
                $('.inbox-progress').css('width',(weightInBox/maxQuantity)*100+'%')
                $('.remove-from-box').on('click' ,(e)=>{
                        var index = $('.remove-from-box').index(e.target);
                        console.log(index)
                        console.log(chocolateChoosed[Number(index)])
                        if($('.chocolate-for-box input.quantity-count').attr('data-chocolate').toLowerCase() == chocolateChoosed[index].name.toLowerCase()){
                            $('.chocolate-for-box input.quantity-count').val(0)
                        }
                        console.log(e.target)
                        chocolateChoosed.splice(index,1)
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach( chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                        });
                    })
            }else {
                $input.val( +$input.val() + 1 );
            }
            
        })
        $('button.decrease').on('click', (e) => {
            var $input = $(`input.quantity-count:eq(${$('button.decrease').index(e.target)})`);
            if($input.val()  > 0){  
                
                if(e.originalEvent.path[2].className.indexOf('box-counter') == -1){
                    $input.val( +$input.val() - 1 );
                    var checkInserted = chocolateChoosed.filter( chocolate => {
                        return chocolate.name == $input.attr('data-chocolate') == true
                    });
                    if(checkInserted.length && $input.val() == 0 ){
                        var index = chocolateChoosed.indexOf(checkInserted[0]);
                        chocolateChoosed.splice(index,1)
                    } else {
                        var index = chocolateChoosed.indexOf(checkInserted[0]);
                        chocolateChoosed[index].count = --chocolateChoosed[index].count
                    };
                    count=[0]
                    chocolateChoosed.map( chocolate => {
                        count.push(chocolate.count * chocolate.weight)
                    });
                    if(count.length){
                        var weightInBox    = count.reduce( (total,number)=>{
                            return total + number
                        });
                        $('.inbox-progress').css('width',(weightInBox/maxQuantity)*100+'%')
                        $('.remove-from-box').on('click' ,(e)=>{
                        var index = $('.remove-from-box').index(e.target);
                        console.log(index)
                        console.log(chocolateChoosed[Number(index)])
                        if($('.chocolate-for-box input.quantity-count').attr('data-chocolate').toLowerCase() == chocolateChoosed[index].name.toLowerCase()){
                            $('.chocolate-for-box input.quantity-count').val(0)
                        }
                        console.log(e.target)
                        chocolateChoosed.splice(index,1)
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach( chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                        });
                    })
                    }else {
                        $('.inbox-progress').css('width','0')
                    }
                }else {
                    $input.val( +$input.val() - 1 );
                }
            }
        });
        $('.chocolate-for-box input.quantity-count').on('blur',(e)=>{
            var $input          = e.target;
            var $inputValue     = e.target.value;
            var $inputIndex     = $('.chocolate-for-box input.quantity-count').index(e.target);
            var $inputVal       = Number(e.target.value) *  Number($(`.chocolate-weight:eq(${$inputIndex})`).text()) ;
            var checkInserted = chocolateChoosed.filter( chocolate => {
                return chocolate.name == $input.getAttribute('data-chocolate') == true
            });
            if(checkInserted.length && $inputValue == checkInserted[0].count){
            }else if(checkInserted.length && $inputValue != checkInserted[0].count){
                count=[0]
                chocolateChoosed.map( chocolate => {
                    count.push(chocolate.count * chocolate.weight)
                });
                var weightInBox = count.reduce( (total,number)=>{
                    return total + number
                });
                weightInBox = weightInBox - $inputVal;
                console.log(weightInBox)
                if($inputVal > maxQuantity && weightInBox == 0){
                    $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                }else if($inputVal > maxQuantity && weightInBox > 0){
                    $input.value = parseInt((maxQuantity- weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                }else if($inputVal > maxQuantity - weightInBox && weightInBox == 0){
                    $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                }else if($inputVal > maxQuantity - weightInBox && weightInBox > 0){
                    $input.value = parseInt((maxQuantity - weightInBox)/  Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                };
                if($input.value > 0){
                    var index = chocolateChoosed.indexOf(checkInserted[0]);
                    chocolateChoosed[index].count = $inputValue;
                    $('.chocolate-in-box *').remove()
                    chocolateChoosed.forEach( chocolate => {
                        return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                    });
                    $('.inbox-progress').css('width',(weightInBox/maxQuantity)*100+'%')
                    $('.remove-from-box').on('click' ,(e)=>{
                        var index = $('.remove-from-box').index(e.target);
                        console.log(index)
                        console.log(chocolateChoosed[Number(index)])
                        if($('.chocolate-for-box input.quantity-count').attr('data-chocolate').toLowerCase() == chocolateChoosed[index].name.toLowerCase()){
                            $('.chocolate-for-box input.quantity-count').val(0)
                        }
                        console.log(e.target);

                        chocolateChoosed.splice(index,1)
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach( chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                        });
                    })
                }else {
                    var index = chocolateChoosed.indexOf(checkInserted[0]);
                    chocolateChoosed.splice(index,1);
                    $('.chocolate-in-box *').remove()
                    chocolateChoosed.forEach( chocolate => {
                        return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                    });
                    $('.inbox-progress').css('width',(weightInBox/maxQuantity)*100+'%')
                    $('.remove-from-box').on('click' ,(e)=>{
                        var index = $('.remove-from-box').index(e.target);
                        console.log(index)
                        console.log(chocolateChoosed[Number(index)])
                        if($('.chocolate-for-box input.quantity-count').attr('data-chocolate').toLowerCase() == chocolateChoosed[index].name.toLowerCase()){
                            $('.chocolate-for-box input.quantity-count').val(0)
                        }
                        console.log(e.target)
                        chocolateChoosed.splice(index,1)
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach( chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                        });
                    })
                }
            }
            else {
                
                count=[0]
                chocolateChoosed.map( chocolate => {
                    count.push(chocolate.count * chocolate.weight)
                });
                var weightInBox = count.reduce( (total,number)=>{
                    return total + number
                });
                if($inputVal > maxQuantity && weightInBox == 0){
                    $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                }else if($inputVal > maxQuantity && weightInBox > 0){
                    $input.value = parseInt((maxQuantity- weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                }else if($inputVal > maxQuantity - weightInBox && weightInBox == 0){
                    $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                }else if($inputVal > maxQuantity - weightInBox && weightInBox > 0){
                    $input.value = parseInt((maxQuantity - weightInBox)/  Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                };
                if($input.value > 0){
                
                    chocolateChoosed.push({
                        name    :   $input.getAttribute('data-chocolate'),
                        weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
                        count   :   Number($input.value)
                    });
                    $('.chocolate-in-box *').remove()
                    chocolateChoosed.forEach( chocolate => {
                        return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                    });
                    $('.inbox-progress').css('width',(weightInBox/maxQuantity)*100+'%')
                    $('.remove-from-box').on('click' ,(e)=>{
                        var index = $('.remove-from-box').index(e.target);
                        console.log(index)
                        console.log(chocolateChoosed[Number(index)])
                        if($('.chocolate-for-box input.quantity-count').attr('data-chocolate').toLowerCase() == chocolateChoosed[index].name.toLowerCase()){
                            $('.chocolate-for-box input.quantity-count').val(0)
                        }
                        console.log(e.target)
                        chocolateChoosed.splice(index,1)
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach( chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                        });
                    })
                }
            }
            
            
            
        })
    }
    
    
  });


  


//   var checkInserted   = chocolateChoosed.filter( chocolate => {
//     return chocolate.name == $input.getAttribute('data-chocolate') == true
// });
// count=[0];
// chocolateChoosed.map( chocolate => {
//     if(chocolate.name == $input.getAttribute('data-chocolate') == false){
//         count.push(chocolate.count * chocolate.weight)
//     }
    
// });
// var weightInBox     = count.reduce( (total,number)=>{
//     return total + number
// });
// weightInBox -= $inputVal ; 
// var $maxInputval    = parseInt((maxQuantity - weightInBox)/Number($(`.chocolate-weight:eq(${$inputIndex})`).text()));
// var $stillVal       = 0;
// console.log('before $maxInputval' + $maxInputval)
// if($input.value > $maxInputval){
    
// console.log('after $maxInputval if ' + $maxInputval)
//     $input.value    = ($maxInputval - weightInBox)/Number($(`.chocolate-weight:eq(${$inputIndex})`).text());
//     $stillVal       = maxQuantity - $inputVal;
//     console.log('still gm => ' + $stillVal);
    
//     if($input.value>0){
//         var index = chocolateChoosed.indexOf(checkInserted[0]);
//         if(checkInserted.length){
//             chocolateChoosed.splice(index);    
//             chocolateChoosed.push({
//                 name    :   $input.getAttribute('data-chocolate'),
//                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
//                 count   :   Number($input.value)
//             });
//         }else {
//             chocolateChoosed.push({
//                 name    :   $input.getAttribute('data-chocolate'),
//                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
//                 count   :   Number($input.value)
//             });
//         }
//     }
// }
// // else if($input.value > $maxInputval && weightInBox > 0) {
// //     console.log('wight in inbix 2 => ' + weightInBox)
// //     $input.value    = ($maxInputval - weightInBox)/Number($(`.chocolate-weight:eq(${$inputIndex})`).text());
// //     $stillVal       = maxQuantity - $inputVal ;
// //     console.log(maxQuantity);
// //     console.log(weightInBox);
// //     console.log($maxInputval);
// //     console.log( Number($(`.chocolate-weight:eq(${$inputIndex})`).text()));
// //     console.log('still gm => ' + $stillVal);
// //     if($input.value>0){
// //         var index = chocolateChoosed.indexOf(checkInserted[0]);
// //         if(checkInserted.length){
// //             chocolateChoosed.splice(index);    
// //             chocolateChoosed.push({
// //                 name    :   $input.getAttribute('data-chocolate'),
// //                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
// //                 count   :   Number($input.value)
// //             });
// //         }else {
// //             chocolateChoosed.push({
// //                 name    :   $input.getAttribute('data-chocolate'),
// //                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
// //                 count   :   Number($input.value)
// //             });
// //         }
// //     }
// // }
// else {
    
//     console.log('after $maxInputval else' + $maxInputval)
//     $stillVal       = maxQuantity - $inputVal ;
//     console.log(maxQuantity);
//     console.log(weightInBox);
//     console.log($maxInputval);
//     console.log( Number($(`.chocolate-weight:eq(${$inputIndex})`).text()));
//     console.log('still gm => ' + $stillVal);
//     if($input.value>0){
//         var index = chocolateChoosed.indexOf(checkInserted[0]);
//         if(checkInserted.length){
//             chocolateChoosed.splice(index);    
//             chocolateChoosed.push({
//                 name    :   $input.getAttribute('data-chocolate'),
//                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
//                 count   :   Number($input.value)
//             });
//         }else {
//             chocolateChoosed.push({
//                 name    :   $input.getAttribute('data-chocolate'),
//                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
//                 count   :   Number($input.value)
//             });
//         }
//     }
// }

// $('.chocolate-in-box *').remove()
// chocolateChoosed.forEach( chocolate => {
//     return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
// });
// $('.inbox-progress').css('width',(weightInBox/maxQuantity)*100+'%')
$('.remove-from-box').on('click' ,(e)=>{
                        var index = $('.remove-from-box').index(e.target);
                        console.log(index)
                        console.log(chocolateChoosed[Number(index)])
                        if($('.chocolate-for-box input.quantity-count').attr('data-chocolate').toLowerCase() == chocolateChoosed[index].name.toLowerCase()){
                            $('.chocolate-for-box input.quantity-count').val(0)
                        }
                        console.log(e.target)
                        chocolateChoosed.splice(index,1)
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach( chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fas fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
                        });
                    })
// console.log($maxInputval )
// console.log($input)
// console.log($inputIndex)
</script>