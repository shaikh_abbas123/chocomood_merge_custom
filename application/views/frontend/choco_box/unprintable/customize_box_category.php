<style>
.mb-5{
    margin-bottom:220px;
}
.p-5{
    padding:50px;
}
</style>
<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><a href="<?= base_url('customize/choose_box_category');?>">Box Category</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="container mt-5 mb-5" style="height: auto;">
        <div class="row">
        <?php
            foreach ($Boxes as $key => $item) {
        ?>
            <div class="col-md-6">
                <div class="card bg-dark text-white mb-5" >
                    <img src="<?php echo base_url($item->Image); ?>" class="card-img chocobox-print_unprint" alt="<?= @$item->Title ?>">
                    <a href="<?php echo base_url('customize/choose_box_category/'.base64_encode(@$item->BoxCategoryID)); ?>">
                        <div class="row align-items-end card-img-overlay p-5 d-flex">
                            <div class="col-12">
                                <h5 class="card-title text-white"><?= @$item->Title ?></h5>
                                <p class="card-text text-white"><?= @$item->Description ?></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <?php
            }
            ?>
            
        </div>
    </section>