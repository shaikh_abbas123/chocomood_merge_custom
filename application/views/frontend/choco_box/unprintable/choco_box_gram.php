<style>
    
    @media(max-width:1280px){
        .customize-box-grm-pricing {
            position: inherit;
            right: unset;
            padding-bottom: 30px;
        }
        h2.choco-grm-headng {
            font-size: 14px;
            line-height: 1.5;
        }
        .cart-n-buy {
            margin: 10px 0;
        }
    }

    @media(max-width:1024px){
        .chocolate-item {
            position: unset;
            width: 50%;
            padding-right: unset;
            padding-left: unset;
        }
    }
    @media(max-width:767px){
        .chocolate-item {
            position: unset;
            width: unset;
            padding-right: unset;
            padding-left: unset;
        }
    }
    @media(max-width:570px){
        .weight_details {
            padding: 5px;
            margin: 5px;
        }
        .chocolate-item {
            position: unset;
            width: unset;
            padding: unset;
        }
    }
    @media (max-width: 375px){
        .weight_details {
            padding: 5px;
            margin: 5px;
            display: flow-root;
            text-align: center;
        }
        .weight_details .pull-right {
            float: unset !important;
            display: block;
        }
    }
</style>
<section class="chocolate-box-container container-fluid mb-4 " id="customize-chocolate-box">
    <form class="form-row" action="" method="POST" id="chocobox_form">
        <input type="hidden" value="<?= @$printable['printable'] ?>" name="boxType">
        <input type="hidden" value="<?= @$printable['contentType'] ?>" name="contentType">
        <input type="hidden" value="<?= @$printable['contentTypeSub'] ?>" name="contentTypeSub">
        <input type="hidden" value="<?= @$printable['contentTypeSubImage'] ?>" name="contentTypeSubImage">
        <input type="hidden" value="<?= @$printable['customertext'] ?>" name="customerContent">
        <input type="hidden" value="<?= @$printable['ribbon'] ?>" name="ribbon">
        <input type="hidden" value="<?= @$printable['wrapping'] ?>" name="wrapping">
        <input type="hidden" value="<?= @$printable['customImage'] ?>" name="customImageId">
        <input type="hidden" value="<?= @$printable['wrapping_price'] ?>" name="wrapping_price">
        <input type="hidden" value="<?= @$printable['ribbon_price'] ?>" name="ribbon_price">
        <input type="hidden" value="<?= @$printable['content_type_price'] ?>" name="content_type_price">
        <input type="hidden" value="<?= @$printable['content_type_sub_price'] ?>" name="content_type_sub_price">
        <!-- start chocolate gallary for box -->
        <div class="col-xl-8 col-lg-12 col-md-12 my-5 ">

            <div class="row chocolate-for-box py-4">

                <?php
                foreach ($inside_products as $v) {
                    $product_images = get_images($v[0]->ProductID, 'product');
                ?>
                    <!-- chocolate for box item -->
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <div style="width:150px;height: auto; overflow:hidden;margin:0px auto">
                            <img src="<?php echo base_url() . $product_images[0]->ImageName; ?>" alt="" class="transform-scale-2" width="100px" height="auto">
                        </div>
                        <!-- chocolate for box content -->
                        <p class="chocolate-title mt-2 mb-1 text-muted">
                            <!-- name -->
                            <b><?= $v[0]->Title?></b>
                            <small>(<span class="chocolate-weight"><?= $v[0]->Weight?></span> gm)</small>
                        </p>
                        <p class="w-100 product-desc-one-line"><?= $v[0]->Description?></p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase" >+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input disabled="true" class="form-control quantity-count" type="text" value="0" name="chocolateName" data-product_price="<?php echo number_format($v[0]->Price, 2); ?>" data-chocolate="<?= $v[0]->ProductID ?>" data-chocolate-name="<?= $v[0]->Title ?>">
                            <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease" >-</button>
                        </div>
                    </div>

                <?php
                }
                ?>
            </div>
        </div>
        <!-- end chocolate gallary for box -->
        <!-- start chocolate in box -->
        <div class="col-xl-4 col-lg-12 col-md-12 mt-5 customize-box-grm-pricing">
            <div class="p-5" style="border-radius: 1.5rem;box-shadow: 0 .5rem 1rem #A07250;position:relative">
            <div class="row w-auto chocolate-box-details">
                <h2 class="text-center choco-grm-headng">PLEASE SELECT FLAVORS & QUANTITIES UNTIL THE BOX IS FULL</h2>
                <p class="px-4 weight_details">FILLED WEIGHT: <span id="filledWeight">0</span>GM <span class="pull-right">TOTAL WEIGHT: <?= $box[0]->weight?>GM</span></p>
                <!-- this is the container which save chocolate and view it over box image 
                        - must add the type for box in => data-box-type attribute => [by gm , by pcs]
                        - must add the max quantity for box in => data-max-quantity attribute
                        - must add the max number of rows for box in => data--count-rows attribute
                        - must add the add the max number for each row for box in => data-chocolate-count-in-rows attribute
                            - it must be in order and the sepration between each number is -
                    -->
                <div class="col-md-12 px-4 chocolate-choosen" data-box-type="gm" data-max-quantity="<?= $box[0]->weight?>" data-count-rows="5" data-chocolate-count-in-rows="6-6-6-6-6">
                    <div class="chocolate-choosen-container-for-type-gm pb-4">
                        <div class="progress progressbar-parent">
                            <div class="progress-bar bg-brown-progressbar inbox-progress" role="progressbar" style="" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <p class="weight_details">REMAINING WEIGHT: <span id="remainingWeight"><?= $box[0]->weight?></span>GM</p>
                        <h4  data-value="0" data-toggle="collapse" href="#collapseExample" role="button">WHATS INSIDE <span class="pull-right"><i class="fa fa-caret-down"></i></span></h4>
                        <ul class="list-group list-unstyled chocolate-in-box collapse border-0 p-0" id="collapseExample">
                            
                        </ul>
                    </div>
                </div>
                <!-- this is the box information will hidden in mobile  -->
                <div class="col-md-12 title-and-description d-xl-block d-none">
                    <!-- title  -->
                    <!-- <h2 class="title mt-4"> <?= $box[0]->Title; ?> </h2> -->
                    <!-- description -->
                    <!-- <p><?= $box[0]->Description; ?></p> -->
                </div>
                <!-- box dimensions -->
                <!-- <div class="col-md-12 details d-xl-block d-none">
                    <h4 class="title mt-4"> <strong>Box Dimensions</strong></h4>
                    <table>
                        <tr>
                        <td>Height:</td>
                                <td class="pl-3"><?= $box[0]->height ?></td>
                        </tr>
                        <tr>
                        <td>No of Rows:</td>
                                <td class="pl-3"><?= $box[0]->no_of_row ?></td>
                        </tr>
                    </table> -->
                    <!-- order nots -->
                    <!-- <h4 class=" title mt-4 "> <strong>Order Notes</strong> </h4>
                    <table>
                        <tr>
                        <td>Minimum Order:</td>
                                <td class="pl-3"><?= $box[0]->MinOrder ?></td>
                        </tr>
                        <tr>
                        <td>Processing Time:</td>
                                <td class="pl-3"><?= $box[0]->orderProcessing ?></td>
                        </tr>

                    </table> -->
                    <!-- price for 1 bpx -->
                    <?php if(isset($printable)){?>
                        <h5 class="px-4 title mt-4 text-grey"><strong>Wrapping Price:</strong> <span class="pull-right"><?= isset($printable['wrapping_price'])?$printable['wrapping_price']:'0.00' ?> SAR</span></h5>
                        <h5 class="px-4 title mt-4 text-grey"><strong>Ribbon Price:</strong>  <span class="pull-right"><?= isset($printable['ribbon_price'])?$printable['ribbon_price']:'0.00' ?> SAR</span></h5>
                        <h5 class="px-4 title mt-4 text-grey"><strong>Content Type Price:</strong> <span class="pull-right"><?= $printable['content_type_price']+$printable['content_type_sub_price'] ?> SAR</span></h5>
                        <h5 class="px-4 title mt-4 text-grey"><strong>Box Price:</strong> <span class="pull-right"><?= $box[0]->BoxPrice ?> SAR</span> </h5>
                        <h4 class="px-4 title mt-4 text-grey"><strong>TOTAL:</strong> <small class="pl-3 ProductP pull-right">  <?= $box[0]->BoxPrice+$printable['wrapping_price']+$printable['ribbon_price']+$printable['content_type_price']+$printable['content_type_sub_price'] ?> SAR</small></h4>
                        <?php }else{ ?>
                            <!-- <h5 class=" title mt-4"><strong>Box Price:</strong> <small class="pl-3"> <?= $box[0]->BoxPrice ?></small> <small class="pl-3">SAR </small></h5> -->
                            <h4 class="px-4 title mt-4"><strong>TOTAL:</strong> <small class="pl-3 ProductP pull-right"><?= $box[0]->BoxPrice ?></small> </h4>
                        <?php } ?>
                        <input name="Quantity" type="hidden" value='1'>
            </div>
                <!-- add to cart -->
            <div class="d-xl-block d-none">
                <input type="hidden" class="ProductIDs" name="ProductIDs">
                <input type="hidden" class="ItemType" name="ItemType" value="Choco Box">
                    <input type="hidden" class="TotalPrice" name="TotalPrice" value="<?php echo $box[0]->BoxPrice; ?>">
                    <input type="hidden" class="BoxID" name="BoxID" value="<?php echo $box[0]->BoxID; ?>">
                    <button type="button" class="btn btn-primary btn-block cart-n-buy" onclick="addChocoBoxToCart_box();"><?php echo lang('add_to_basket'); ?>
            </div>
            <div class="row mt-4">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary btn-block text-uppercase cart-n-buy" onclick=""><?php echo lang('auto_fill'); ?>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary btn-block text-uppercase cart-n-buy reset-selection-box" ><?php echo lang('reset'); ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
        
    </form>
    <a class="btn btn-primary fixed-bottom rounded-circle" id="show-box-btn" href="#body"><i class="fas fa-arrow-up"></i></a>
</section>
<script>
    var chocolateChoosed = [],count = [0];
    $(document).ready(function() {
            
            var maxQuantity = Number($('.chocolate-choosen').attr('data-max-quantity'));
            $('button.increase').click((e) => {
                var $input = $(`input.quantity-count:eq(${$('button.increase').index(e.target)})`);
                if (e.originalEvent.path[2].className.indexOf('box-counter') == -1) {
                    var checkInserted = chocolateChoosed.filter(chocolate => {
                        return chocolate.name == $input.attr('data-chocolate') == true
                    });
                    var weightInBox = count.reduce((total, number) => {
                        return total + number
                    });
                    if (checkInserted.length) {
                        if (Number($(`.chocolate-weight:eq(${$('button.increase').index(e.target)})`).text()) <= (maxQuantity - weightInBox)) {

                            $input.val(+$input.val() + 1);
                            var index = chocolateChoosed.indexOf(checkInserted[0]);
                            chocolateChoosed[index].count = ++chocolateChoosed[index].count;
                    
                            
                        } else {
                            var stillWight = maxQuantity - weightInBox;
                            if (stillWight > 0) {
                                notifyMessageCustom('choose a little weight still ' + stillWight + 'gm', "danger")
                            } else {
                                notifyMessageCustom('Box Space Full.')
                            }

                        }
                    } else {
                        if (Number($(`.chocolate-weight:eq(${$('button.increase').index(e.target)})`).text()) <= (maxQuantity - weightInBox)) {
                            $input.val(+$input.val() + 1);
                            chocolateChoosed.push({
                                chocolate_name: $input.attr('data-chocolate-name'),
                                price: $input.attr('data-product_price'),
                                name: $input.attr('data-chocolate'),
                                weight: Number($(`.chocolate-weight:eq(${$('button.increase').index(e.target)})`).text()),
                                count: Number($input.val()),
                            });

                            
                        } else {
                            var stillWight = maxQuantity - weightInBox;
                            if (stillWight > 0) {
                                alert('choose a little weight still ' + stillWight + 'gm')
                            } else {
                                alert('complate')
                            }

                        }
                    };
                    count = [0];
                    chocolateChoosed.map(chocolate => {
                        count.push(chocolate.count * chocolate.weight)
                    });
                    weightInBox = count.reduce((total, number) => {
                        return total + number
                    });
                    $('.chocolate-in-box *').remove();
                    chocolateChoosed.forEach(chocolate => {
                        return $('.chocolate-in-box').append(`<li class="list-group-item p-3">${chocolate.chocolate_name}<strong class="pull-right">(${chocolate.count})</strong></li>`)
                    });
                    var ProductIDsJoined = '';
                    var new_price = Number($('.TotalPrice').val());
                    chocolateChoosed.forEach(chocolate => {
                        for(var i=0; i<chocolate.count;i++ )
                        {
                            ProductIDsJoined = (ProductIDsJoined + ',' + chocolate.name).replace(/^,/,'');
                            new_price +=  Number(chocolate.price);
                            
                        }
                    });
                    $('.ProductIDs').val(ProductIDsJoined);
                    $('.ProductP').html(formatNumber(new_price));

                    $('.inbox-progress').css('width', (weightInBox / maxQuantity) * 100 + '%')
                    $('#filledWeight').text((weightInBox));
                    $('#remainingWeight').text((maxQuantity - weightInBox));
                    
                } else {
                    $input.val(+$input.val() + 1);
                }

                

            });

            $('button.decrease').on('click', (e) => {
                var $input = $(`input.quantity-count:eq(${$('button.decrease').index(e.target)})`);
                if ($input.val() > 0) {

                    if (e.originalEvent.path[2].className.indexOf('box-counter') == -1) {
                        $input.val(+$input.val() - 1);
                        var checkInserted = chocolateChoosed.filter(chocolate => {
                            return chocolate.name == $input.attr('data-chocolate') == true
                        });

                        if (checkInserted.length && $input.val() == 0) {
                            var index = chocolateChoosed.indexOf(checkInserted[0]);
                            chocolateChoosed.splice(index, 1)
                        } else {
                            var index = chocolateChoosed.indexOf(checkInserted[0]);
                            chocolateChoosed[index].count = --chocolateChoosed[index].count;

                        };
                        count = [0]
                        chocolateChoosed.map(chocolate => {
                            count.push(chocolate.count * chocolate.weight)
                        });
                        if (count.length) {
                            var weightInBox = count.reduce((total, number) => {
                                return total + number
                            });
                            $('.chocolate-in-box *').remove();
                            console.log(chocolateChoosed);
                            chocolateChoosed.forEach(chocolate => {
                                return $('.chocolate-in-box').append(`<li class="list-group-item p-3">${chocolate.chocolate_name}<strong class="pull-right">(${chocolate.count})</strong></li>`)
                            });
                            var ProductIDsJoined = '';
                            var new_price = Number($('.TotalPrice').val());
                            chocolateChoosed.forEach(chocolate => {
                                for(var i=0; i<chocolate.count;i++ )
                                {
                                    ProductIDsJoined = (ProductIDsJoined + ',' + chocolate.name).replace(/^,/,'');
                                    new_price +=  Number(chocolate.price);
                                }
                            });
                            $('.ProductIDs').val(ProductIDsJoined);
                            $('.ProductP').html(formatNumber(new_price));
                            $('.inbox-progress').css('width', (weightInBox / maxQuantity) * 100 + '%')
                            $('#filledWeight').text((weightInBox));
                            $('#remainingWeight').text((maxQuantity - weightInBox));
                        } else {
                            $('.inbox-progress').css('width', '0')
                        }
                        
                    } else {
                        $input.val(+$input.val() - 1);
                    }
                }
            });

            //NOT USED, DISBALED START.
            $('.chocolate-for-box input.quantity-count').on('blur', (e) => {
                var $input = e.target;
                var $inputValue = e.target.value;
                var $inputIndex = $('.chocolate-for-box input.quantity-count').index(e.target);
                var $inputVal = Number(e.target.value) * Number($(`.chocolate-weight:eq(${$inputIndex})`).text());
                var checkInserted = chocolateChoosed.filter(chocolate => {
                    return chocolate.name == $input.getAttribute('data-chocolate') == true
                });
                
                if (checkInserted.length && $inputValue == checkInserted[0].count) {} 
                else if (checkInserted.length && $inputValue != checkInserted[0].count) {
                    count = [0]
                    chocolateChoosed.map(chocolate => {
                        count.push(chocolate.count * chocolate.weight)
                    });
                    var weightInBox = count.reduce((total, number) => {
                        return total + number
                    });
                    weightInBox = weightInBox - $inputVal;
                    console.log(weightInBox)
                    if ($inputVal > maxQuantity && weightInBox == 0) {
                        $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    } else if ($inputVal > maxQuantity && weightInBox > 0) {
                        $input.value = parseInt((maxQuantity - weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    } else if ($inputVal > maxQuantity - weightInBox && weightInBox == 0) {
                        $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    } else if ($inputVal > maxQuantity - weightInBox && weightInBox > 0) {
                        $input.value = parseInt((maxQuantity - weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    };
                    if ($input.value > 0) {
                        var index = chocolateChoosed.indexOf(checkInserted[0]);
                        chocolateChoosed[index].count = $inputValue;
                        $('.chocolate-in-box *').remove();
                        chocolateChoosed.forEach(chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3">${chocolate.chocolate_name}<strong class="pull-right">(${chocolate.count})</strong></li>`)
                        });
                        $('.inbox-progress').css('width', (weightInBox / maxQuantity) * 100 + '%')
                       
                    } else {
                        var index = chocolateChoosed.indexOf(checkInserted[0]);
                        chocolateChoosed.splice(index, 1);
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach(chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3">${chocolate.chocolate_name}<strong class="pull-right">(${chocolate.count})</strong></li>`)
                        });
                        $('.inbox-progress').css('width', (weightInBox / maxQuantity) * 100 + '%')
                       
                    }
                } else {

                    count = [0]
                    chocolateChoosed.map(chocolate => {
                        count.push(chocolate.count * chocolate.weight)
                    });
                    var weightInBox = count.reduce((total, number) => {
                        return total + number
                    });
                    if ($inputVal > maxQuantity && weightInBox == 0) {
                        $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    } else if ($inputVal > maxQuantity && weightInBox > 0) {
                        $input.value = parseInt((maxQuantity - weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    } else if ($inputVal > maxQuantity - weightInBox && weightInBox == 0) {
                        $input.value = parseInt(maxQuantity / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    } else if ($inputVal > maxQuantity - weightInBox && weightInBox > 0) {
                        $input.value = parseInt((maxQuantity - weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()))
                    };
                    if ($input.value > 0) {

                        chocolateChoosed.push({
                            chocolate_name: $input.getAttribute('data-chocolate-name'),
                            name: $input.getAttribute('data-chocolate'),
                            weight: Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
                            count: Number($input.value)
                        });
                        $('.chocolate-in-box *').remove()
                        chocolateChoosed.forEach(chocolate => {
                            return $('.chocolate-in-box').append(`<li class="list-group-item p-3">${chocolate.chocolate_name}<strong class="pull-right">(${chocolate.count})</strong></li>`)
                        });
                        $('.inbox-progress').css('width', (weightInBox / maxQuantity) * 100 + '%')
                        
                    }
                }



            });
            //NOT USED, DISBALED END.

            $(document).on("click", ".remove-from-box" , function(e) {
            
                var index = $(this).parent().index();
                // console.log(chocolateChoosed[Number(index)])

                $('.chocolate-for-box input.quantity-count').filter(function() {
                    if($(this).attr('data-chocolate') == chocolateChoosed[index].name)
                    {
                        $(this).val(0);
                    }
                });
                
                
                chocolateChoosed.splice(index, 1);
                $('.chocolate-in-box *').remove();
                weightInBox = 0;
                chocolateChoosed.forEach(chocolate => {
                    weightInBox += chocolate.weight*chocolate.count;
                    return $('.chocolate-in-box').append(`<li class="list-group-item p-3">${chocolate.chocolate_name}<strong class="pull-right">(${chocolate.count})</strong></li>`)
                });
                
                var ProductIDsJoined = '';
                var new_price = Number($('.TotalPrice').val());
                chocolateChoosed.forEach(chocolate => {
                    for(var i=0; i<chocolate.count;i++ )
                    {
                        ProductIDsJoined = (ProductIDsJoined + ',' + chocolate.name).replace(/^,/,'');
                        new_price +=  Number(chocolate.price);
                    }
                });
                $('.ProductIDs').val(ProductIDsJoined);
                $('.ProductP').html(formatNumber(new_price));
                $('.inbox-progress').css('width', (weightInBox / maxQuantity) * 100 + '%')
            });
    });
    $(document).on("click", ".reset-selection-box" , function(e) {
        let maxQuantity = $('.chocolate-choosen').attr('data-max-quantity');
        let boxPrice = <?= isset($printable) ? ($box[0]->BoxPrice+$printable['wrapping_price']+$printable['ribbon_price']+$printable['content_type_price']+$printable['content_type_sub_price']):$box[0]->BoxPrice ?>;
        $('.inbox-progress').css('width', 0 + '%')
        $('#filledWeight').text('0');
        $('#remainingWeight').text(maxQuantity);
        $('.ProductP').text(boxPrice);
        $('.chocolate-in-box').html('');
        $('.quantity-count').val(0);
        chocolateChoosed = new Array();
        $('.ProductIDs').val('');
        count = [0];
    });
    // var checkInserted = chocolateChoosed.filter(chocolate => {
    //     return chocolate.name == $input.getAttribute('data-chocolate') == true
    // });
    // count = [0];
    // chocolateChoosed.map(chocolate => {
    //     if (chocolate.name == $input.getAttribute('data-chocolate') == false) {
    //         count.push(chocolate.count * chocolate.weight)
    //     }

    // });
    // var weightInBox = count.reduce((total, number) => {
    //     return total + number
    // });
    // weightInBox -= $inputVal;
    // var $maxInputval = parseInt((maxQuantity - weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text()));
    // var $stillVal = 0;
    // console.log('before $maxInputval' + $maxInputval)
    // if ($input.value > $maxInputval) {

    //     console.log('after $maxInputval if ' + $maxInputval)
    //     $input.value = ($maxInputval - weightInBox) / Number($(`.chocolate-weight:eq(${$inputIndex})`).text());
    //     $stillVal = maxQuantity - $inputVal;
    //     console.log('still gm => ' + $stillVal);

    //     if ($input.value > 0) {
    //         var index = chocolateChoosed.indexOf(checkInserted[0]);
    //         if (checkInserted.length) {
    //             chocolateChoosed.splice(index);
    //             chocolateChoosed.push({
    //                 name: $input.getAttribute('data-chocolate'),
    //                 weight: Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
    //                 count: Number($input.value)
    //             });
    //         } else {
    //             chocolateChoosed.push({
    //                 name: $input.getAttribute('data-chocolate'),
    //                 weight: Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
    //                 count: Number($input.value)
    //             });
    //         }
    //     }
    // }
    // else if($input.value > $maxInputval && weightInBox > 0) {
    //     console.log('wight in inbix 2 => ' + weightInBox)
    //     $input.value    = ($maxInputval - weightInBox)/Number($(`.chocolate-weight:eq(${$inputIndex})`).text());
    //     $stillVal       = maxQuantity - $inputVal ;
    //     console.log(maxQuantity);
    //     console.log(weightInBox);
    //     console.log($maxInputval);
    //     console.log( Number($(`.chocolate-weight:eq(${$inputIndex})`).text()));
    //     console.log('still gm => ' + $stillVal);
    //     if($input.value>0){
    //         var index = chocolateChoosed.indexOf(checkInserted[0]);
    //         if(checkInserted.length){
    //             chocolateChoosed.splice(index);    
    //             chocolateChoosed.push({
    //                 name    :   $input.getAttribute('data-chocolate'),
    //                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
    //                 count   :   Number($input.value)
    //             });
    //         }else {
    //             chocolateChoosed.push({
    //                 name    :   $input.getAttribute('data-chocolate'),
    //                 weight  :   Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
    //                 count   :   Number($input.value)
    //             });
    //         }
    //     }
    // }
    // else {

    //     console.log('after $maxInputval else' + $maxInputval)
    //     $stillVal = maxQuantity - $inputVal;
    //     console.log(maxQuantity);
    //     console.log(weightInBox);
    //     console.log($maxInputval);
    //     console.log(Number($(`.chocolate-weight:eq(${$inputIndex})`).text()));
    //     console.log('still gm => ' + $stillVal);
    //     if ($input.value > 0) {
    //         var index = chocolateChoosed.indexOf(checkInserted[0]);
    //         if (checkInserted.length) {
    //             chocolateChoosed.splice(index);
    //             chocolateChoosed.push({
    //                 name: $input.getAttribute('data-chocolate'),
    //                 weight: Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
    //                 count: Number($input.value)
    //             });
    //         } else {
    //             chocolateChoosed.push({
    //                 name: $input.getAttribute('data-chocolate'),
    //                 weight: Number($(`.chocolate-weight:eq(${$inputIndex})`).text()),
    //                 count: Number($input.value)
    //             });
    //         }
    //     }
    // }

    // $('.chocolate-in-box *').remove()
    // chocolateChoosed.forEach(chocolate => {
    //     return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fa fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
    // });
    // $('.inbox-progress').css('width', (weightInBox / maxQuantity) * 100 + '%')
    // $('.remove-from-box').on('click', (e) => {
    //     var index = $('.remove-from-box').index(e.target);
    //     console.log(index)
    //     console.log(chocolateChoosed[Number(index)])
    //     if ($('.chocolate-for-box input.quantity-count').attr('data-chocolate').toLowerCase() == chocolateChoosed[index].name.toLowerCase()) {
    //         $('.chocolate-for-box input.quantity-count').val(0)
    //     }
    //     console.log(e.target)
    //     chocolateChoosed.splice(index, 1)
    //     $('.chocolate-in-box *').remove()
    //     chocolateChoosed.forEach(chocolate => {
    //         return $('.chocolate-in-box').append(`<li class="list-group-item p-3"><span style="cursor:pointer;" class="text-danger d-onlone-block mr-3 remove-from-box"><i class="fa fa-times"></i></span>${chocolate.name}<strong>(${chocolate.count})</strong></li>`)
    //     });
    // })
    // console.log($maxInputval )
    // console.log($input)
    // console.log($inputIndex)
    $(window).scroll(function() {
        console.log($(window).scrollTop())
        if($(window).scrollTop() < 250){
            $(".customize-box-grm-pricing").css({
            "top": "250px"
        });
        }else
        {
            $(".customize-box-grm-pricing").css({
            "top": ($(window).scrollTop()) + "px"
        });
        }
        
    });
</script>