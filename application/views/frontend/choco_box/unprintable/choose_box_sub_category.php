<style>
.choco-box {
    padding-bottom: 48px !important;
}
</style>
<section class="content products titlarea">
<div class="container">
        <div class="row">
            <div class="col-md-12 choco-msg_border-bot">
                <h2 class="choco_msg-headng">
                    <?= lang('choco_box')?>
                </h2>
                    <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                  <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                     <li><?= lang('box_category')?></li>
                    </ul>
                    <br/>
                <span class="choco_msg-step">
                    <b><?= lang('step_1')?></b>
                </span>
                <span class="choco_msg-desc">
                    <?= lang('select_favourite_box')?>
                </span>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container">
        <div class="row">
        <?php
        foreach ($Boxes as $key => $item) {
            $iterate = $key+1;
            if($iterate%2 != 0)
            {
    ?>
        <div class="col-md-6 mt-4 pr-0 straight-line choco-box order-first">
            <a class="box" href="<?php echo base_url('customize/choose_box/'.base64_encode(@$item->BoxCategoryID).'/'.$BoxCatID); ?>">
                <div class="imgbox">
                    <img src="<?php echo base_url($item->Image); ?>" alt="" style="width:100%; height:20%;">
                </div>
                <div class="content_descrip1 box_textleft_h ">
                    <!-- name -->
                    <h4 class="title-left"><?= @$item->Title ?></h4> 
                    <!-- description -->
                    <p class="description-left position-relative text-left">
                    <?= @$item->Description ?>
                    </p>
                </div>
            </a>
        </div>

    <?php
        }else{
    ?>
    <div class="col-md-6 mt-4 pr-0 choco-box">
        <a class="" href="<?php echo base_url('customize/choose_box/'.base64_encode(@$item->BoxCategoryID).'/'.$BoxCatID); ?>">
                <div class="content_descrip2 box_textright_h">
                    <!-- name -->
                    <h4 class="title-right pr-4"><?= @$item->Title ?></h4> 
                    <!-- description -->
                    <p class="description-right position-relative text-right" style="margin-left: -15px;">
                    <?= @$item->Description ?>
                    </p>
                </div>
                <div class="imgbox box">
                    <img src="<?php echo base_url($item->Image); ?>" alt="" style="width:100%; height:20%;margin-left: -15px;">
                </div>
            </div>
        </a>
    <?php
        } }
    ?>
       </div>     

         <div class="row">
            <div class="col-md-12 pt-4 pb-4 mt-5">
                <p class="continue-txt"><?= lang('select_box')?> <i class="fa fa-caret-right" aria-hidden="true"></i></p>
            </div>
        </div>


</div>
</section>
       