<style type="text/css">
	.accordion>.card:first-of-type {
		border-bottom: 0;
		border-bottom-right-radius: 0;
		border-bottom-left-radius: 0;
	}

	img.choco-bg {
		position: relative;
	}

	.content-type-active:before,
	.fillings-active:before,
	.wrappings-active:before,
	.drawing-active:before,
	.checked:before,
	.text-color-active:before,
	.text-type-active:before {
		height: 7px;
		width: 15px;
		border-bottom: rgb(255, 255, 255) solid 3px;
		border-left: rgb(255, 251, 251) solid 3px;
		content: "";
		display: block;
		transform: rotateZ(-40deg);
		-webkit-transform: rotateZ(-40deg);
		-ms-transform: rotateZ(-40deg);
		position: absolute;
		top: 7px;
		right: 10px;
		z-index: 1;
	}

	.text-type-active {
		height: 200px;
	}

	.content-type-active:after,
	.fillings-active:after,
	.wrappings-active:after,
	.drawing-active:after,
	.checked:after,
	.text-color-active:after,
	.text-type-active:after {
		content: "";
		background-color: #4b436073;
		width: 25px;
		height: 25px;
		position: absolute;
		top: 0;
		right: 5px;
		border-radius: 50%;
	}

	*,
	::after,
	::before {
		box-sizing: border-box;
	}

	::-webkit-scrollbar {
		width: 8px;
	}

	::-webkit-scrollbar-thumb {
		background: #ccc;
		border-radius: 10px;
	}

	::-webkit-scrollbar-track {
		border-radius: 10px;
	}

	.type-options {
		overflow: hidden;
	}

	#content-output[type="text"],
	#content-output[type="text"]::placeholder {
		color: #BD9371;
		border-color: rgb(189, 147, 113);
		font-size: 16px;
	}

	.form-control {
		display: block;
		width: 100%;
		height: calc(1.5em + .75rem + 2px);
		padding: .375rem .75rem;
		font-size: 1rem;
		font-weight: 400;
		line-height: 1.5;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #CED4DA;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
	}

	ul.drawing-filter-list {
		list-style-type: none;
		overflow: hidden;
		margin-bottom: 0;
		padding-left: 0;
	}

	li.filter-drawing-list-item[aria-expanded="true"] {
		color: rgb(189 147 113);
		border-bottom-color: rgb(189 147 113);
		opacity: 1;
	}

	li.filter-drawing-list-item {
		float: left;
		padding: 0;
		font-size: 15px;
		opacity: 0.5;
		cursor: pointer;
		font-weight: 700;
		border: 1px solid rgb(189 147 113);
		margin-top: 14px !important;
		margin-left: 10px !important;
		border-radius: 19px;
		line-height: 2;
		padding-left: 12px;
		height: 35px;
	}

	li.filter-drawing-list-item span {
		display: inline-block;
		background: rgb(189 147 113);
		padding: 1px;
		width: 52px;
		height: 100%;
		text-align: center;
		color: rgb(255 255 255);
		border-radius: 0 19px 19px 0;
		position: relative;
		margin-left: 10px;
	}

	li.filter-drawing-list-item span:after {
		content: "";
		position: absolute;
		width: 10px;
		height: 10px;
		background: rgb(189 147 113);
		left: -4px;
		top: 11px;
		transform: rotate(45deg);
	}

	section ul li:last-child {
		margin: unset;
	}

	.transtion,
	.transtion:hover,
	.transtion:focus {
		transition: all .2s .1s ease-in-out;
		-webkit-transition: all .2s .1s ease-in-out;
		-moz-transition: all .2s .1s ease-in-out;
		-ms-transition: all .2s .1s ease-in-out;
		-o-transition: all .2s .1s ease-in-out;
	}

	.text-type-item img,
	.drawing-item img {
		width: 90%;
		height: auto;
	}

	div#filter-drawing {
		max-height: 205px;
		overflow: auto;
	}

	.drawing-item {
		float: left;
		height: 85px;
	}

	.content-type-item,
	.fillings-item,
	.wrappings-item,
	.drawing-item,
	.color-item,
	.text-color-item,
	.text-type-item {
		position: relative;
	}

	.accordion>.card:not(:first-of-type):not(:last-of-type) {
		border-bottom: 0;
		border-radius: 0;
	}

	.accordion>.card {
		overflow: hidden;
	}

	.border-0 {
		border: 0 !important;
	}

	.accordion>.card:not(:first-of-type) .card-header:first-child {
		border-radius: 0;
	}

	.card {
		position: relative;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-direction: column;
		flex-direction: column;
		min-width: 0;
		word-wrap: break-word;
		background-color: #fff;
		background-clip: border-box;
		border: 1px solid rgba(0, 0, 0, .125);
		border-radius: .25rem;
	}

	/* .accordion>.card .card-header {
      margin-bottom: -1px;
  } */
	div.customize-title {
		border: none;
		border-bottom: 3px solid white;
		padding: 14px 0 7px 0;
		cursor: pointer;
		background-color: #fff;
		border-bottom: 2px solid #A07250;
	}
  div.customize-title {
      border: none;
      border-bottom: 3px solid white;
      padding: 14px 0 7px 0;
      cursor: pointer;
      background-color: #fff;
      border-bottom:2px solid #A07250; 
  }
  div.customize-title h4 {
      color: #32231F;
      font-size: 18px;
      text-transform:uppercase;
      letter-spacing:5px;
      font-weight:600;
  }
  div.customize-title h4[aria-expanded="false"]:after {
      content: "+";
      float: right;
      padding-right: 1rem;
      font-size: 23px;
      font-weight: bold;
	  margin-top: -5px;
  }
  .ar div.customize-title h4[aria-expanded="false"]:after {
      content: "+";
      float: left;
      padding-right: 1rem;
      font-size: 23px;
      font-weight: bold;
  }
  div.customize-title h4[aria-expanded="true"]:after {
      content: "-";
      float: right;
      padding-right: 1rem;
      font-size: 23px;
      font-weight: bold;
	  margin-top: -5px;
  }
  #content-type-view .content-items .view-item {
      width: 50%;
      float: left;
  }
  .position-relative {
      position: relative!important;
  }
  .d-flex {
      display: -ms-flexbox!important;
      display: flex!important;
  }
  h1.customize-chocolate-price {
    color: #BD9371;
    font-size: 24px;
    text-align: center;
    padding: 10px 35px;
    margin:auto;
  }
  h1 strong {
      display: unset;
      font-weight: 900;
      text-transform: none;
  }
  .mask-item {
      -webkit-mask-image: url("http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png");
      mask-image: url("http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png");
      mask-mode: alpha;
      -webkit-mask-mode: alpha;
      mask-repeat: no-repeat;
      mask-size: 100%;
      -webkit-mask-repeat: no-repeat;
      -webkit-mask-size: 100%;
      mask-position: center center;
      -webkit-mask-position: center center;
      width: 40%;
      height: 40%;
  }
  .mask-container {
      -webkit-filter: drop-shadow(2px 3px 4px rgba(0,0,0,0.75));
      -moz-filter: drop-shadow(2px 3px 4px rgba(0,0,0,0.75));
      -ms-filter: drop-shadow(2px 3px 4px rgba(0,0,0,0.75));
      -o-filter: drop-shadow(2px 3px 4px rgba(0,0,0,0.75));
      filter: drop-shadow(2px 3px 4px rgba(0,0,0,0.75));
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
  }
  .card-body {
      -ms-flex: 1 1 auto;
      flex: 1 1 auto;
      padding: 1.25rem;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  }
  @media (max-width: 425px){
    .order-last-mobile {
        order: 1 !important;
    }
  }
  .content-type-item {
      height: 200px;
  }
  #content-output[type="file"] {
      height: 125px;
      width: 139px;
      position: relative;
      cursor: pointer;
      border-color: rgb(189, 147, 113);
  }
  #content-output[type="file"]:before {
      content: "+";
      position: absolute;
      top: 0;
      left: 0;
      text-align: center;
      display: flex;
      align-items: center;
      justify-content: center;
      color: rgb(189, 147, 113);
      z-index: 1;
      width: 100%;
      height: 74%;
      font-size: 3rem;
      font-weight: bold;
  }
  #content-output[type="file"]:after {
      content: "Your Logo";
      display: block;
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      text-align: center;
      display: flex;
      align-items: center;
      justify-content: center;
      color: rgb(189, 147, 113);
      background-color: white;
      padding-top: 31px;
      font-size: 1.5rem;
  }
  [type=button]:not(:disabled), [type=reset]:not(:disabled), [type=submit]:not(:disabled), button:not(:disabled) {
      cursor: pointer;
  }
  .done button {
      transition: all 0.5s 0.1s ease-in-out;
      -webkit-transition: all 0.5s 0.1s ease-in-out;
      -moz-transition: all 0.5s 0.1s ease-in-out;
      -ms-transition: all 0.5s 0.1s ease-in-out;
      -o-transition: all 0.5s 0.1s ease-in-out;
      width: 195px;
      height: 53px;
      color: white;
      background-color: #4B4360;
      display: flex;
      text-align: center;
      align-items: center;
      justify-content: center;
      border-radius: 30px;
      padding-bottom: 4px;
      border: none;
      outline: none!important;
  }
  li.filter-drawing-list-item:hover {
      opacity: 1;
  }
  #colors .color-items .color-item:first-of-type {
      background-color: #59899F;
  }
  #colors .color-items .color-item {
      position: relative;
      width: 50px;
      height: 50px;
      float: left;
      margin: 2px;
  }
  #colors .color-items .color-item input {
      cursor: pointer;
  }
  #colors .color-items .color-item input {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      opacity: 0;
      z-index: 1;
  }
  .template-type-item input, .ribbons-item input, .wrappings-item input, .drawing-item input, .color-item input, .text-color-item input, .text-template-item input {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0;
    cursor: pointer;
    z-index: 10;
  }
  .template-type-active:after, .ribbons-active:after, .wrappings-active:after, .drawing-active:after, .checked:after, .text-color-active:after, .text-template-active:after {
    content: "";
    background-color: #4b436073;
    width: 25px;
    height: 25px;
    position: absolute;
    top: 0;
    right: 5px;
    border-radius: 50%;
  }
  .template-type-active:before, .ribbons-active:before, .wrappings-active:before, .drawing-active:before, .checked:before, .text-color-active:before, .text-template-active:before {
    height: 7px;
    width: 15px;
    border-bottom: rgb(255, 255, 255) solid 3px;
    border-left: rgb(255, 251, 251) solid 3px;
    content: "";
    display: block;
    transform: rotateZ(-40deg);
    -webkit-transform: rotateZ(-40deg);
    -ms-transform: rotateZ(-40deg);
    position: absolute;
    top: 7px;
    right: 9px;
    z-index: 1;
  }
  div#filter-drawing {
    width: 100%;
}
.content {
    padding: 50px 0 0;
}
.select-chocolates-btn{
  transition: all .5s .1s ease-in-out;
  -webkit-transition: all .5s .1s ease-in-out;
  -moz-transition: all .5s .1s ease-in-out;
  -ms-transition: all .5s .1s ease-in-out;
  -o-transition: all .5s .1s ease-in-out;
  width: 350px;
  height: 50px;
  color: white;
  background-color: #bd9371;
  text-align: center;
  align-items: center;
  justify-content: center;
  border-radius: 0;
  font-size: 24px;
  text-transform: uppercase;
  border:inherit;
}
.ribbon-clr_img , .wrapping-img{
  width:80px;
  height:70px;
}

	div.customize-title h4 {
		color: #32231F;
		font-size: 18px;
		text-transform: uppercase;
		letter-spacing: 5px;
		font-weight: 600;
	}

	div.customize-title h4[aria-expanded="false"]:after {
		content: "+";
		float: right;
		padding-right: 1rem;
		font-size: 23px;
		font-weight: bold;
	}

	.ar div.customize-title h4[aria-expanded="false"]:after {
		content: "+";
		float: left;
		padding-right: 1rem;
		font-size: 23px;
		font-weight: bold;
	}

	div.customize-title h4[aria-expanded="true"]:after {
		content: "-";
		float: right;
		padding-right: 1rem;
		font-size: 23px;
		font-weight: bold;
	}

	#content-type-view .content-items .view-item {
		width: 50%;
		float: left;
	}

	.position-relative {
		position: relative !important;
	}

	.d-flex {
		display: -ms-flexbox !important;
		display: flex !important;
	}

	h1.customize-chocolate-price {
		color: #BD9371;
		font-size: 24px;
		text-align: center;
		/* float: right; */
		padding: 10px 35px;
		margin: auto;
	}

	h1 strong {
		display: unset;
		font-weight: 900;
		text-transform: none;
	}

	.mask-item {
		-webkit-mask-image: url("http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png");
		mask-image: url("http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png");
		mask-mode: alpha;
		-webkit-mask-mode: alpha;
		mask-repeat: no-repeat;
		mask-size: 100%;
		-webkit-mask-repeat: no-repeat;
		-webkit-mask-size: 100%;
		mask-position: center center;
		-webkit-mask-position: center center;
		width: 40%;
		height: 40%;
	}

	.mask-container {
		-webkit-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		-moz-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		-ms-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		-o-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.card-body {
		-ms-flex: 1 1 auto;
		flex: 1 1 auto;
		padding: 1.25rem;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
	}

	@media (max-width: 425px) {
		.order-last-mobile {
			order: 1 !important;
		}
	}

	.content-type-item {
		height: 200px;
	}

	#content-output[type="file"] {
		height: 125px;
		width: 139px;
		position: relative;
		cursor: pointer;
		border-color: rgb(189, 147, 113);
	}

	#content-output[type="file"]:before {
		content: "+";
		position: absolute;
		top: 0;
		left: 0;
		text-align: center;
		display: flex;
		align-items: center;
		justify-content: center;
		color: rgb(189, 147, 113);
		z-index: 1;
		width: 100%;
		height: 74%;
		font-size: 3rem;
		font-weight: bold;
	}

	#content-output[type="file"]:after {
		content: "Your Logo";
		display: block;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0;
		left: 0;
		text-align: center;
		display: flex;
		align-items: center;
		justify-content: center;
		color: rgb(189, 147, 113);
		background-color: white;
		padding-top: 31px;
		font-size: 1.5rem;
	}

	[type=button]:not(:disabled),
	[type=reset]:not(:disabled),
	[type=submit]:not(:disabled),
	button:not(:disabled) {
		cursor: pointer;
	}

	.done button {
		transition: all 0.5s 0.1s ease-in-out;
		-webkit-transition: all 0.5s 0.1s ease-in-out;
		-moz-transition: all 0.5s 0.1s ease-in-out;
		-ms-transition: all 0.5s 0.1s ease-in-out;
		-o-transition: all 0.5s 0.1s ease-in-out;
		width: 195px;
		height: 53px;
		color: white;
		background-color: #4B4360;
		display: flex;
		text-align: center;
		align-items: center;
		justify-content: center;
		border-radius: 30px;
		padding-bottom: 4px;
		border: none;
		outline: none !important;
	}

	li.filter-drawing-list-item:hover {
		opacity: 1;
	}

	#colors .color-items .color-item:first-of-type {
		background-color: #59899F;
	}

	#colors .color-items .color-item {
		position: relative;
		width: 50px;
		height: 50px;
		float: left;
		margin: 2px;
	}

	#colors .color-items .color-item input {
		cursor: pointer;
	}

	#colors .color-items .color-item input {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		opacity: 0;
		z-index: 1;
	}

	.template-type-item input,
	.ribbons-item input,
	.wrappings-item input,
	.drawing-item input,
	.color-item input,
	.text-color-item input,
	.text-template-item input {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		opacity: 0;
		cursor: pointer;
		z-index: 10;
	}

	.template-type-active:after,
	.ribbons-active:after,
	.wrappings-active:after,
	.drawing-active:after,
	.checked:after,
	.text-color-active:after,
	.text-template-active:after {
		content: "";
		background-color: #4b436073;
		width: 25px;
		height: 25px;
		position: absolute;
		top: 0;
		right: 5px;
		border-radius: 50%;
	}

	.template-type-active:before,
	.ribbons-active:before,
	.wrappings-active:before,
	.drawing-active:before,
	.checked:before,
	.text-color-active:before,
	.text-template-active:before {
		height: 7px;
		width: 15px;
		border-bottom: rgb(255, 255, 255) solid 3px;
		border-left: rgb(255, 251, 251) solid 3px;
		content: "";
		display: block;
		transform: rotateZ(-40deg);
		-webkit-transform: rotateZ(-40deg);
		-ms-transform: rotateZ(-40deg);
		position: absolute;
		top: 7px;
		right: 9px;
		z-index: 1;
	}

	div#filter-drawing {
		width: 100%;
	}

	.content {
		padding: 50px 0 0;
	}

	.select-chocolates-btn {
		transition: all .5s .1s ease-in-out;
		-webkit-transition: all .5s .1s ease-in-out;
		-moz-transition: all .5s .1s ease-in-out;
		-ms-transition: all .5s .1s ease-in-out;
		-o-transition: all .5s .1s ease-in-out;
		width: 350px;
		height: 50px;
		color: white;
		background-color: #bd9371;
		text-align: center;
		align-items: center;
		justify-content: center;
		border-radius: 0;
		font-size: 24px;
		text-transform: uppercase;
		border: inherit;
	}

	.ribbon-clr_img,
	.wrapping-img {
		width: 80px;
		height: 70px;
	}

	a:link.tooltip_pop {
		color: #5F5F5F;
		background-color: transparent;
		text-decoration: none;
	}

	@media (max-width:1024px) {

		.ribbon-clr_img,
		.wrapping-img {
			width: 80px;
			height: 50px;
		}

		.pull-arbitrary {
			float: unset !important;
			text-align: center !important;
			display: block;
			margin: auto;
		}

		h1.customize-chocolate-price {
			float: unset !important;
		}
	}

	@media (max-width:991px) {

		.ribbon-clr_img,
		.wrapping-img {
			width: 80px;
			height: 60px;
		}
	}

	@media (max-width: 375px) {

		.ribbon-clr_img,
		.wrapping-img {
			width: 80px;
			height: 55px;
		}
	}

	.ribbons-item:first-child,
	.wrappings-item:first-child,
	.wrappings-active:first-child,
	.ribbons-active:first-child {
		padding-bottom: 5px;
	}
	canvas#myCanvas2 {
    left: unset !important;
    right: 0px !important;
}
@media (max-width:991px){
  .ribbon-clr_img , .wrapping-img {
    width: 80px;
    height: 60px;
  }
}
@media (max-width: 375px){
  .ribbon-clr_img , .wrapping-img {
      width: 80px;
      height: 55px;
  }
}
.ribbons-item:first-child, .wrappings-item:first-child
{
  padding-bottom:5px;
}

canvas#myCanvas2 {
    right: calc(50% - 160px) !important;
}
</style>
<?php $choco_shape = getPageContent(12, $lang) ?>
<!-- <section class="content customize first "
	style="background:url(http://localhost/chocomood_merge_custom/uploads/images/5430795057720210830014624rose_boxes.png);"> -->
<section class="content customize first " style="background:url(<?= base_url('uploads/images/choco-shape-stp1.png') ?>);">

	<div class="bannerbox">
		<div class="choco-shape_captionbox">
			<div class="inner">
				<h2><b>ROSES BOX PRINTABLE</b></h2>
			</div>
		</div>
	</div>
</section>
<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12 choco-msg_border-bot">
            <ul>
                  <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                  <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                  <li>Customize Printable Box</li>
              </ul>
              <br>
              <span class="choco_msg-step">
                  <b>Step 2:</b>
              </span>
              <span class="choco_msg-desc">
                  Customize the box according to your wish
              </span>  
              <!--<h2>Printable Box Customization</h2>
              <ul>
                  <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                  <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                  <li>Printable Box Customization</li>
              </ul> -->
			</div>
		</div>
	</div>
</section>

<section class="customize-box container" style="margin: 20px auto 100px;" id="customize-box">
	<form action="<?= base_url('customize/customize_printable_box_post')?>" method="post" enctype="multipart/form-data">
		<div class="row">
			<!-- start customize options -->
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 nt-2 mb-4 order-last-mobile">

				<!-- this form to send the customer to customize to database -->

				<!-- start customize options accordion -->
				<div class="accordion" id="customize-chocolate-tabs">
					<!-- start template type option -->
					<div id="template-type" class="card border-0">
						<!-- title -->
						<div class="card-header customize-title" id="headingOne">
							<h4 class="mb-0 mt-0 collapsed" data-toggle="collapse" data-target="#type"
								aria-expanded="false" aria-controls="type">
								Template Type
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover" data-html="true" 
									data-content="<b>Upload Logo</b> <br> It should be in jpg with max dimension around 125x125 <br><b>Name & Logo</b> Customer who purchase from this seller can leave a rating. This is calculated by getting the average of the total number of rating. A seller with five star a top rated seller.">
									<i class="far fa-question-circle"></i>
								</a>
							</h4>
						</div>
						<!-- template -->
						<div id="type" class="collapse" aria-labelledby="headingOne"
							data-parent="#customize-chocolate-tabs" style="" aria-expanded="true">
							<!-- template container-->
							<div class="card-body row">

								<?php
                      foreach ($content_types as $key => $content_type) {
                    ?>
								<div class="col-4 template-type-item <?= $key == 0 ?'template-type-active':'' ?>">
									<!-- template image -->
									<img class="choco-bg" src="<?php echo base_url($content_type->Image); ?>"
										width="100%" height="auto" alt="">
									<!-- this input save data which will send to the database after customer press Done button -->
									<!-- this is text template type  -->
									<input type="radio" name="contentType" value="<?= $content_type->ContentTypeID?>"
										id="" <?= $key == 0 ?'checked':'' ?>>
								</div>
								<?php
                    }
                    ?>
								<div class="col-12">
									<div class="type-options pl-0 pt-3 pr-3 pb-3">
										<!-- <div class="input-group"> -->
										<input class="form-control add-your-photo" type="text" name="customer-content"
											id="content-output" placeholder="Your Name Here" style="display: none;">
										<span class="max-character" style="display: none;">
											<strong style="color:red;">*</strong>
											<small>max character 8</small>
										</span>
										<!-- <a onclick="addInput()" id="add-input-button"><i class="fas fa-plus"></i></a> -->
										<!-- </div> -->
										<div id="drawing" class="card-body pl-2 p-0 row" style="display:none">
											<ul class="drawing-filter-list mb-4">
												<?php
                              foreach($customize_text_types as $key =>$value){
                            ?>
												<li class="filter-drawing-list-item transtion mb-0"
													data-toggle="collapse"
													data-target="#<?=preg_replace('/\s+/', '', @$value->Title);?>"
													aria-expanded="true"
													aria-controls="<?=preg_replace('/\s+/', '', @$value->Title);?>">
													<?= @$value->Title ?>
													<?php $images = get_images($value->ContentTypeSubID, 'content_type_sub_image');
                                          if(@$images == NULL)
                                          {
                                            $images= array();
                                          }
                                    ?>
													<span><?= count(@$images)?></span>
												</li>
												<?php
                            }
                            ?>
											</ul>
											<div class="accordion" id="filter-drawing">

												<?php
                                        $drawingActive =1;
                                        foreach($customize_text_types as $key =>$value){
                                       ?>
												<div id="<?=preg_replace('/\s+/', '', @$value->Title);?>"
													class="collapse transtion" aria-labelledby="headingOne"
													data-parent="#filter-drawing">
													<!-- content item -->
													<?php
                                              $images_2 = get_images($value->ContentTypeSubID, 'content_type_sub_image');
                                             if(@$images_2 != NULL)
                                             {
                                              foreach(get_images($value->ContentTypeSubID, 'content_type_sub_image') as $subKey => $image){
                                                ?>
													<div
														class="col-2 drawing-item <?= $drawingActive == 1?'drawing-active':'' ?>">
														<!-- content image -->
														<div class="content-svg">
															<div class="mask-container">
																<img class="mask-item"
																	src="<?= @base_url($image->ImageName) ?>"
																	data-mask="<?= base_url() . $image->ImageName?>"
																	width="500px" height="500px" alt=""
																	style="-webkit-mask-image:url(<?= base_url() . $image->ImageName?>)!important;mask-image:url(<?= base_url() . $image->ImageName?>)!important;background-color: rgb(253, 105, 43);">
															</div>
														</div>
														<input type="radio" name="textTemplate"
															value="<?= $value->ContentTypeSubID ?>,<?= $image->SiteImageID ?>"
															<?= $subKey == 0?'checked':'' ?>>
													</div>
													<?php
                                                $drawingActive =0;
                                               }
                                             }
                                            ?>
												</div>
												<?php
                                        }
                                        ?>
											</div>

										</div>
										<div id="text-templates" style="display: none;">
											<?php
                        $content_sub_images = get_images($text_with_image->ContentTypeSubID, 'content_type_sub_image');
                          if(isset($content_sub_images) && !empty($content_sub_images))
                          foreach($content_sub_images as $key => $item){
                          
                        ?>
											<div
												class="col-4 p-0 text-template-item <?= $key ==0?'text-template-active':''?>">
												<!-- content image -->
												<img src="<?= @base_url($item->ImageName) ?>" alt="" width="100%">
												<!-- this input save data which will send to the database after customer press Done button -->
												<input type="radio" name="textTemplate"
													value="<?= $text_with_image->ContentTypeSubID ?>,<?= $item->SiteImageID ?>"
													id="" <?= $key == 0?'checked':''?>>
											</div>
											<?php 
                          } 
                        ?>


										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- end content type option -->

					<!-- start ribbons option -->
					<div class="card border-0">
						<!-- title -->
						<div class="card-header customize-title" id="ribbon">
							<h4 class="mb-0 mt-0 collapsed" data-toggle="collapse" data-target="#ribbon-container"
								aria-expanded="false" aria-controls="ribbon-container">
								Ribbon
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover"
									data-content="Lorem ipsum doret">
									<i class="far fa-question-circle"></i>
								</a>
							</h4>
						</div>
						<!-- content -->
						<div id="ribbon-container" class="collapse" aria-labelledby="ribbon"
							data-parent="#customize-chocolate-tabs" style="">
							<div class="card-body row">
								<?php foreach($ribbons as $key => $item){?>

									<div class="col-md-3 col-sm-3 col-xs-3 ribbons-item <?= $key ==0?'ribbons-active':''?>" onclick="change_ribbon(myCanvas2,'<?php echo base_url($item->RibbonImage); ?>')">
									<img src="<?php echo base_url($item->RibbonImage); ?>" class="ribbon-clr_img"
										alt="" >
									<input type="radio" name="ribbon" value="<?= $item->RibbonID?>" id=""
										<?= $key ==0?'checked':''?>>
								</div>
								<!-- <div class="col-md-3 col-sm-3 col-xs-3 ribbons-item <?= $key ==0?'ribbons-active':''?>">
									<img src="<?php echo base_url($item->RibbonImage); ?>" class="ribbon-clr_img"
										alt="">
									<input type="radio" name="ribbon" value="<?= $item->RibbonID?>" id=""
										<?= $key ==0?'checked':''?>>
								</div> -->
								<?php } ?>
								
							</div>
						</div>
					</div>
					<!-- end Ribbons option -->
					<!-- start Wrappings option -->
					<div class="card border-0">
						<!-- title -->
						<div class="card-header customize-title" id="wrappings">
							<h4 class="mb-0 mt-0 collapsed" data-toggle="collapse" data-target="#wrappings-container"
								aria-expanded="false" aria-controls="wrappings-container">
								Wrappings
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover"
									data-content="Lorem ipsum doret">
									<i class="far fa-question-circle"></i>
								</a>
							</h4>
						</div>
						<!-- content -->
						<div id="wrappings-container" class="collapse" aria-labelledby="wrappings"
							data-parent="#customize-chocolate-tabs" style="">
							<div class="card-body row">
								<?php
                      foreach($wrappings as $key => $wrappping){
                    ?>
								<!-- content item -->
								<div
									class="col-md-3 col-sm-3 col-xs-3 wrappings-item <?= $key == 0 ? 'wrappings-active': ''?>">
									<!-- content image -->
									<img src="<?php echo base_url($wrappping->WrappingImage); ?>" class="wrapping-img"
										alt="">
									<!-- this input save data which will send to the database after customer press Done button -->
									<input type="checkbox" name="wrapping" value="<?= @$wrappping->WrappingID ?>"
										<?= $key == 0 ? 'checked': ''?>>
								</div>
								<?php } ?>
								
							</div>
						</div>
					</div>
					<!-- end wrappings option -->
				</div>
				<!-- end customize option acordion -->

			</div>
			<!-- start customize View -->

			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 nt-2 mb-4 " id="template-type-view">
				<!-- customize box items color view contauner -->
				<div class="content-items d-flex justify-content-center">
					<canvas id="myCanvas2" > </canvas>
					<input type="hidden" name="image" value="<?= base_url('uploads/images/ribbon_box.png') ?>" id="image_url"> 
					<!-- <input type="hidden" name="image" value="<?= base_url($box[0]->BoxImage) ?>" id="image_url"> -->
					<!-- customize box item-->
					<!-- <div class="view-item position-relative p-0 pull-arbitrary"> -->
					<!-- <div class=""> -->

						<!-- customize box item img -->
						<!-- <img class="choco-bg"
							src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSYX_SMDWwE179o8H-XZDZ_u_HgRh2h_fciSQ&amp;usqp=CAU width="
							100%"="" height="100%" alt="">

						<img class="template-type-choosen"
							src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Ski_trail_rating_symbol_black_circle.png"
							style="position:absolute; width:80%;height:80%; display:block;z-index:1;top:22px;left:23px;">

						<img class="riboon-choosen" src=""
							style="position:absolute; width:100%;height: 100%; display:block;z-index:2;top:0;left:0;"> -->
					<!-- </div> -->
				</div>
				<h1 class="customize-chocolate-price">Price: <strong><?=$box[0]->BoxPrice?></strong><small>SAR</small></h1>

				<!-- start submit button form -->
				<div class="col-md-12 done mt-3 pl-0 mb-4 d-flex justify-content-center p-0">
					<input type="hidden" name="PriceType" value="<?=$box[0]->PriceType?>">
					<input type="hidden" name="boxID" value="<?=$box[0]->BoxID?>">
					<div class="pull-center-mobile start-customize">

						<!-- <a
                  <?php
                    if ($box[0]->PriceType == 'kg')
                    {
                        ?>
                        <a href="<?php echo base_url('customize/choco_box_gram/'.base64_encode(@$box[0]->BoxID)); ?>"
                        <?php
                    }
                    if ($box[0]->PriceType == 'item')
                    {
                        ?>
                        <a href="<?php echo base_url('customize/choco_box_item/'.base64_encode(@$box[0]->BoxID)); ?>"
                        <?php
                    }
                  ?>
                  >Add Chocolates</a> -->
						<input type="submit" value="Select Chocolates" class="select-chocolates-btn">
					</div>
				</div>
				<!-- end submit button form -->
			</div>

	</form>
	</div>
	<!-- end customize option -->
	<script src="<?php echo front_assets(); ?>js/fabric/fabric.min.js"></script>
<script src="<?php echo front_assets(); ?>js/fabric/ribbon.js"></script>
</section>




<!-- end customize shape content -->
<script>


	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip({
			'placement': 'top'
		});
		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'center',
			container: 'body'
		});
	});

	$('.card-header').click(function (e) {
		e.preventDefault();
		var myGroup = $('.accordion');
		myGroup.find('.collapse.in').collapse('hide');
	});


	$(document).ready(() => {

		if (document.getElementById("customize-box") != null) {
			addRiboon()

			function checkTemplateType() {
				// check template type == text
				if ($(".template-type-active:eq(0) input:checked").val() == "2") {
					document.getElementById("text-templates").style.display = 'flex';

				} else {
					document.getElementById("text-templates").style.display = 'none';
				}
				// check template type == drawing
				if ($(".template-type-active:eq(0) input:checked").val() == "3") {
					document.getElementById("drawing").style.display = 'flex';

				} else {
					document.getElementById("drawing").style.display = 'none';
				}
				// check template type == text || template type == text
				if ($(".template-type-active:eq(0) input:checked").val() == "2" || $(
						".template-type-active:eq(0) input:checked").val() == "3") {
					$('.max-character:eq(0)').css('display', 'block');
					$("#content-output").attr('type', 'text')
					$("#content-output").removeAttr('accept');
				} else {
					$('.max-character:eq(0)').css('display', 'none');
					$("#content-output").attr('type', 'file');
					$("#content-output").attr('accept', 'image/*');
					if ($(".template-type-active:eq(0) input:checked").val() == "logo") {
						$("#content-output:after").css('content', 'Add Your Logo');
						$("#content-output").addClass('add-your-logo').removeClass('add-your-photo');;
					} else {
						$("#content-output").addClass('add-your-photo').removeClass('add-your-logo');;
					}

				}
				// if type = none  
				if ($(".template-type-active:eq(0) input:checked").val() == "1") {
					$("#content-output").css('display', 'none');
					$(".max-character").css('display', 'none');
					$('.template-type-choosen:eq(0)').remove();
				} else {
					$("#content-output").css('display', 'block');
					$(".max-character").css('display', 'block');
					templateTypeSrc();
				}
				// if type = 8 (add logo)  
				if ($(".template-type-active:eq(0) input:checked").val() == "8") {
					$(".max-character").css('display', 'none');
				}

				addRiboon()
			}

			function templateTypeSrc() {
				var templateType = $('.template-type-active:eq(0) img').attr('src');
				if ($(".template-type-active:eq(0) input:checked").val() == "text") {
					templateType = $('.text-template-active:eq(0) img').attr('src')
				} else if ($(".template-type-active:eq(0) input:checked").val() == "3") {
					templateType = $('.drawing-active:eq(0) img').attr('src')
				}
				$('.template-type-choosen:eq(0)').remove();

				$('.view-item:eq(0)').append(`
                  <img class="template-type-choosen" src="${templateType}" style="position:absolute; width:80%; display:block;z-index:1;top:22px;left:23px;" />
              `);
			}

			function addRiboon() {
				if ($('.ribbons-active:eq(0) input:checked').val() != 'none') {

					if ($(".template-type-active:eq(0) input:checked").val() == "none") {
						$('.template-type-choosen:eq(0)').remove();
					}
					var riboon = $('.ribbons-active:eq(0) img').attr('src');
					$('.riboon-choosen:eq(0)').remove();

					$('.view-item:eq(0)').append(`
                  <img class="riboon-choosen" src="${riboon}" style="position:absolute; width:100%;height:100%; display:block;z-index:2;top:0;left:0;" />
              `);
				} else {
					$('.riboon-choosen:eq(0)').remove();
				}
			} // template type
			var templateType = document.getElementsByClassName("template-type-item");
			for (var i = 0; i < templateType.length; i++) {
				templateType[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("template-type-active");
					current[0].className = current[0].className.replace(
						" template-type-active",
						""
					);
					this.className += " template-type-active";
					checkTemplateType();
				});
			}

			// start fillings
			var addOns = document.getElementsByClassName("fillings-item");
			for (var i = 0; i < addOns.length; i++) {
				addOns[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("fillings-active");

					current[0].className = current[0].className.replace(" fillings-active", "");
					this.className += " fillings-active";
				});
			}
			var drawing = document.getElementsByClassName("drawing-item");
			for (var i = 0; i < drawing.length; i++) {
				drawing[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("drawing-active");

					current[0].className = current[0].className.replace(
						" drawing-active",
						""
					);
					this.className += " drawing-active";
					checkTemplateType();
				});
			}
			var textTemplate = document.getElementsByClassName("text-template-item");
			for (var i = 0; i < textTemplate.length; i++) {
				textTemplate[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("text-template-active");

					current[0].className = current[0].className.replace(
						" text-template-active",
						""
					);
					this.className += " text-template-active";
					checkTemplateType();
				});
			}
			var ribbons = document.getElementsByClassName("ribbons-item");
			for (var i = 0; i < ribbons.length; i++) {
				ribbons[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("ribbons-active");

					current[0].className = current[0].className.replace(
						" ribbons-active",
						""
					);
					this.className += " ribbons-active";
					checkTemplateType()
				});
			}
			//  start wrappings

			var wrappings = document.getElementsByClassName("wrappings-item");
			for (var i = 0; i < wrappings.length; i++) {
				wrappings[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("wrappings-active");

					current[0].className = current[0].className.replace(
						" wrappings-active",
						""
					);
					this.className += " wrappings-active";
				});
			}
		}
	})

	//uses classList, setAttribute, and querySelectorAll
	//if you want this to work in IE8/9 youll need to polyfill these
	(function () {
		var d = document,
			accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
			setAria,
			setAccordionAria,
			switchAccordion,
			touchSupported = ('ontouchstart' in window),
			pointerSupported = ('pointerdown' in window);

		skipClickDelay = function (e) {
			e.preventDefault();
			e.target.click();
		}

		setAriaAttr = function (el, ariaType, newProperty) {
			el.setAttribute(ariaType, newProperty);
		};
		setAccordionAria = function (el1, el2, expanded) {
			switch (expanded) {
				case "true":
					setAriaAttr(el1, 'aria-expanded', 'true');
					setAriaAttr(el2, 'aria-hidden', 'false');
					break;
				case "false":
					setAriaAttr(el1, 'aria-expanded', 'false');
					setAriaAttr(el2, 'aria-hidden', 'true');
					break;
				default:
					break;
			}
		};
		//function
		switchAccordion = function (e) {
			console.log("triggered");
			e.preventDefault();
			var thisAnswer = e.target.parentNode.nextElementSibling;
			var thisQuestion = e.target;
			if (thisAnswer.classList.contains('is-collapsed')) {
				setAccordionAria(thisQuestion, thisAnswer, 'true');
			} else {
				setAccordionAria(thisQuestion, thisAnswer, 'false');
			}
			thisQuestion.classList.toggle('is-collapsed');
			thisQuestion.classList.toggle('is-expanded');
			thisAnswer.classList.toggle('is-collapsed');
			thisAnswer.classList.toggle('is-expanded');

			thisAnswer.classList.toggle('animateIn');
		};
		for (var i = 0, len = accordionToggles.length; i < len; i++) {
			if (touchSupported) {
				accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
			}
			if (pointerSupported) {
				accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
			}
			accordionToggles[i].addEventListener('click', switchAccordion, false);
		}
	})();
</script>