<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo site_title(); ?> | Receipt</title>
    <style type="text/css">

        p {
            margin-bottom: 5px !important;
            margin-top: 5px !important;
        }

        h4 {

            margin-bottom: 20px;


        }


        #wrap {
            float: left;
            position: relative;
            left: 50%;
        }

        #content {
            float: left;
            position: relative;
            left: -50%;
        }

        table {
            border-collapse: collapse;
            font-family: sans-serif;
        }

    </style>

</head>


<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                        <tr>
                                            <td>
                                                <?php
                                                if($this->session->userdata('user')->PreferredLang == 1){
                                                ?>
                                                    <h4>مرحبا <?php echo ucfirst($order->FullName); ?>,</h4>
                                                    <p>شكرا لك على الشراء من chocomood.</p>
                                                    <p> طلبك # <?php echo $order->OrderNumber; ?> تم وضعه
                                                        بنجاح.</p>
                                                    <?php
                                                    if (isset($type) && $type == 'print') { ?>
                                                        <p>انقر <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" onclick="window.print();"
                                                                    style="text-decoration: none;">هنا</a>
                                                                بعد تسجيل الدخول لطباعة هذا الإيصال</p>
                                                <?php }
                                                }else{ 
                                                ?>
                                                    <h4>Hi <?php echo ucfirst($order->FullName); ?>,</h4>
                                                    <p>Thank you for Purchasing from chocomood.</p>
                                                    <p> Your order # <?php echo $order->OrderNumber; ?> has been placed
                                                        successfully.</p>
                                                    <?php
                                                    if (isset($type) && $type == 'print') { ?>
                                                        <p>Click <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" onclick="window.print();"
                                                                    style="text-decoration: none;">here</a> after login to print this
                                                            receipt</p>
                                                <?php }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                       <!-- <tr>
                                            <td align="center" valign="top">
                                                <table border="1" style="text-align: center;">
                                                    <thead>
                                                    <tr style="border: #0b2e13 solid 1px;">
                                                        <th colspan="2" class="th_heading"
                                                            style="color: #015685 !important;">Order Details
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td style="width: 140px;">Order Tracking #</td>
                                                        <td><strong><?php echo $order->OrderNumber; ?></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px;">Order Status</td>
                                                        <td><?php echo ucfirst($order->OrderStatusEn); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px;">Name</td>
                                                        <td><?php echo ucfirst($order->FullName); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px;">Email</td>
                                                        <td><?php echo $order->Email; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px;">Mobile</td>
                                                        <td><?php echo $order->Mobile; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Delivery Address</td>
                                                        <td>
                                                            <?php
                                                            if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                                <h5>Collect From Store</h5>
                                                            <?php } else {
                                                                $full_address = '';
                                                                if ($order->RecipientName != '') {
                                                                    $full_address .= 'Recipient Name: ' . $order->RecipientName . ', ';
                                                                }
                                                                if ($order->MobileNo != '') {
                                                                    $full_address .= 'Recipient Mobile: ' . $order->MobileNo . ', ';
                                                                }
                                                                if ($order->BuildingNo != '') {
                                                                    $full_address .= 'Building No: ' . $order->BuildingNo . ', ';
                                                                }
                                                                if ($order->Street != '') {
                                                                    $full_address .= 'Street: ' . $order->Street . ', ';
                                                                }
                                                                if ($order->POBox != '') {
                                                                    $full_address .= 'P.O Box: ' . $order->POBox . ', ';
                                                                }
                                                                if ($order->ZipCode != '') {
                                                                    $full_address .= 'Zip Code: ' . $order->ZipCode . ', ';
                                                                }
                                                                if ($order->AddressDistrict != '') {
                                                                    $full_address .= 'District: ' . $order->AddressDistrict . ', ';
                                                                }
                                                                if ($order->AddressCity != '') {
                                                                    $full_address .= 'City: ' . $order->AddressCity . ', ';
                                                                }
                                                                echo rtrim($full_address, ', ');
                                                            } ?>
                                                            </p></td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td style="width: 120px;">Payment Method</td>
                                                        <td><?php echo $order->PaymentMethod; ?></td>
                                                    </tr>
                                                   <?php if (isset($order->CollectFromStore) && $order->CollectFromStore != 1) { ?>
                                                    <tr>
                                                        <td style="width: 120px;">Shipment Method</td>
                                                        <td><?php echo $order->ShipmentMethodTitle; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>-->
                                    </table>
                                </td>
                            </tr>
                            <!--<tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailBody">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="1" style="text-align: center;">
                                                    <thead>
                                                    <tr>
                                                        <th style="color: #015685 !important;">#</th>
                                                        <th style="color: #015685 !important;">Image</th>
                                                        <th style="color: #015685 !important;">Item Name</th>
                                                        <th style="color: #015685 !important;">Unit Price</th>
                                                        <th style="color: #015685 !important;">Quantity</th>
                                                        <th style="color: #015685 !important;">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $i = 1;
                                                    $total_cost = 0;
                                                    $order_items = getOrderItems($order->OrderID);
                                                    foreach ($order_items as $item) {
                                                        $total_cost = $total_cost + ($item->Amount * $item->Quantity);
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td>
                                                                <?php
                                                                if ($item->ItemType == 'Product') { ?>
                                                                    <a href="<?php echo base_url(get_images($item->ProductID, $item->ItemType, false)); ?>"
                                                                       target="_blank">
                                                                        <img src="<?php echo base_url(get_images($item->ProductID, $item->ItemType, false)); ?>"
                                                                             style="height:70px;width:70px;">
                                                                    </a>
                                                                <?php } elseif ($item->ItemType == 'Customized Shape') { ?>
                                                                    <img src="<?php echo base_url($item->CustomizedShapeImage); ?>"
                                                                         style="width: 70px;height: 70px;">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo front_assets("images/" . $item->ItemType . ".png"); ?>"
                                                                         style="width: 70px;height: 70px;">
                                                                <?php } ?>
                                                            </td>
                                                            <td class="product-name">
                                                                <?php
                                                                if ($item->ItemType == 'Product') { ?>
                                                                    <?php echo $item->Title; ?>
                                                                <?php } elseif ($item->ItemType == 'Customized Shape') { ?>
                                                                    Customized Shape
                                                                <?php } else { ?>
                                                                    <?php echo $item->ItemType; ?>
                                                                <?php }
                                                                ?>
                                                            </td>
                                                            <td class="product-price"><?php echo number_format($item->Amount, 2); ?>
                                                                SAR
                                                            </td>
                                                            <td class="product-quantity">
                                                                <?php echo $item->Quantity; ?><?php echo ($item->IsCorporateItem ? ' kg' : 'pcs'); ?>
                                                            </td>
                                                            <td class="product-total">
                                                                <?php echo number_format($item->Amount * $item->Quantity, 2); ?>
                                                                SAR
                                                            </td>
                                                        </tr>
                                                        <?php $i++;
                                                    }
                                                    ?>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="7"
                                                            style="text-align: right; color: #015685 !important;">Sub
                                                            Total: <?php echo number_format($total_cost, 2); ?> SAR
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    if ($order->CouponCodeDiscountPercentage > 0) { ?>
                                                        <tr style="font-weight: bold;">
                                                            <td colspan="7"
                                                                style="text-align: right; color: #015685 !important;">
                                                                Coupon Discount
                                                                %: <?php echo $order->CouponCodeDiscountPercentage; ?>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                    <?php
                                                    if ($order->DiscountAvailed > 0) { ?>
                                                        <tr style="font-weight: bold;">
                                                            <td colspan="7"
                                                                style="text-align: right; color: #015685 !important;">
                                                                Discount
                                                                Availed: <?php echo number_format($order->DiscountAvailed, 2); ?>
                                                                SAR
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="7"
                                                            style="text-align: right; color: #015685 !important;">
                                                            Shipment
                                                            Charges: <?php echo $order->TotalShippingCharges; ?> SAR
                                                        </td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="7"
                                                            style="text-align: right; color: #015685 !important;">Total
                                                            Tax Paid: <?php echo $order->TotalTaxAmount; ?> SAR
                                                        </td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="7"
                                                            style="text-align: right; color: #015685 !important;">Grand
                                                            Total: <?php echo $order->TotalAmount; ?> SAR
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>-->
                        </table>
                    </td>
                </tr>
            </table>
            <!--<p style="font-family:sans-serif;font-size:14px;">Need Help? <a href="<?php /*echo base_url(); */ ?>"
                                                                            style="color:#015685;text-decoration:none;">Click
                    here</a></p>
            <br/>-->
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="<?php echo base_url(); ?>assets/logo.png" width="60" height="60"
                             alt="Site Logo">
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;"><?php echo site_title(); ?></h4>
                        <span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
                        <span style="font-family:sans-serif;">
                            <a href="<?php echo base_url(); ?>"
                               style="color:grey;font-size:10px;text-decoration: none;"><?php echo my_site_url(); ?></a>
                        </span>
                    </td>
                </tr>
            </table>


        </td>
    </tr>
</table>

</body>
</html>