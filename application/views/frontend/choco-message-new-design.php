<style type="text/css">
 
 div#customsgbox {
    /* padding: 10px; */
    /* box-shadow: 0 0 15px; */
    box-shadow: 0px 1px 18px 4px #888686;
    height: 75%;
}
.content.productskg.customizebox .sidebox #customsgbox ul li img {
 box-shadow: -1px 5px 7px 3px;
}


    .draggable {
        filter: alpha(opacity=60);
        opacity: 0.6;
    }

    .dropped {
        position: static !important;
        list-style-type: none;
        float: left;
        margin: 0 1% 0 0;
        width: 19%;
    }

    #dvSource, #dvDest {
        border: 5px solid #ccc;
        padding: 5px;
        min-height: 100px;
        width: 430px;
    }

    img {
        -webkit-transition-duration: 0s;
        -o-transition-duration: 0s;
        transition-duration: 0s;
    }
    .custom-radios div {
  display: inline-block;
}
.custom-radios input[type=radio] {
  display: none;
}
.custom-radios input[type=radio] + label {
  color: #333;
  font-family: Arial, sans-serif;
  font-size: 14px;
}
.custom-radios input[type=radio] + label span {
  display: inline-block;
  width: 40px;
  height: 40px;
  margin: -1px 4px 0 0;
  vertical-align: middle;
  cursor: pointer;
  border-radius: 50%;
  border: 2px solid #ffffff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
  background-repeat: no-repeat;
  background-position: center;
  text-align: center;
  line-height: 44px;
}
.custom-radios input[type=radio] + label span img {
  opacity: 0;
  transition: all 0.3s ease;
}
.custom-radios input[type=radio]#color-1 + label span {
  background-color: #2ecc71;
}
.custom-radios input[type=radio]#color-2 + label span {
  background-color: #3498db;
}
.custom-radios input[type=radio]#color-3 + label span {
  background-color: #f1c40f;
}
.custom-radios input[type=radio]#color-4 + label span {
  background-color: #e74c3c;
}
.custom-radios input[type=radio]:checked + label span img {
  opacity: 1;
}
.ribbon-img{
    box-shadow: 0px 1px 18px #00000036;
    margin-top: 25px;
    margin-bottom: 25px;
}
.content.products {
    background-color: transparent;
}
p.total-price-new {
    font-size: 16px;
    text-transform: uppercase;
    font-weight: 600;
    letter-spacing: 3px;
}
.quantity-row {
    padding-right: 25px;
    padding-left: 25px;
}
.content.single-products .product-info h6 {
    font-size: 18px;
    font-weight: 700;
    color: #bd9371;
    margin: 0;
    line-height: 36px;
}
.content.productskg .sidebox .btn {
    width: 60%;
    display: block;
    margin: auto !important;
}
.content.productskg .sidebox h2 {
    font-size: 22px;
}
.input-group-addon, .input-group-btn {
    width: 35%;
    white-space: nowrap;
    vertical-align: middle;
}
@media (max-width:991px){
    .content.single-products .product-info h6{
        text-align:center;
    }
    .content.single-products .product-info .input-group {
        width: 100%;
    }
    .input-group-addon, .input-group-btn {
        width: 12%;
    }
    .content.productskg .sidebox h2{
        text-align:center;
    }
}
@media (max-width: 570px){
    .input-group-addon, .input-group-btn {
        width: 33%;
    }
}
</style>
<?php $choco_msg = getPageContent(10, $lang) ?>
<!-- <section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $choco_msg->Title; ?></h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?php echo $choco_msg->Title; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section> -->

<section class="content products titlarea">
    <div class="container ">
        <div class="row">
            <div class="col-md-12 choco-msg_border-bot">
                <h2 class="choco_msg-headng">
                <?php echo $choco_msg->Title; ?> New Design
                </h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?php echo $choco_msg->Title; ?></li>
                </ul>
                <br>
                <span class="choco_msg-step">
                    <b><?= lang('step_2')?></b>
                </span>
                <span class="choco_msg-desc">
                    <?= lang('customise_your_message')?>
                </span>
            </div>
        </div>
    </div>
</section>
<section class="content single-products productskg customizebox">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="list-group">
                    <a href="#menu1" class="list-group-item icon-float" data-toggle="collapse" data-parent="#sidebar" style="display:none;">
                        <span>Compose your message</span>
                        <svg class="fa fa-angle-down" aria-hidden="true"></svg>
                    </a>
                    <div class="collapse in" id="menu1">
                        <p><?= lang('click_chocolate_letter_to_add')?></p>
                        <ul class="customletters custommsg">
                        <?php
                        foreach ($characters as $character) { 
                            if($character->CharacterType == 1){
                            ?>
                            <!-- <li data-product_id="<?= $character->CharacterID; ?>" data-product_price="<?= $character->price; ?>" onclick="addImage(canvas,'<?php echo base_url( $character->CharacterImage); ?>')">
                                <div class="img-overlay">
                                    <img src="<?php echo base_url( $character->CharacterImage); ?>">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li> -->

                            <li data-product_id="<?= $character->CharacterID; ?>" data-product_price="<?= $character->price; ?>"  onclick="addImage(canvas,'<?php echo base_url( $character->CharacterImage); ?>')">
                                <img src="<?php echo base_url( $character->CharacterImage); ?>">
                            </li>
                        <?php 
                            }
                        } ?>
                           
                        </ul>
                    </div>
                </div>
                <div class="list-group">
                    <a href="#menu2" class="list-group-item icon-float" data-toggle="collapse" data-parent="#sidebar" style="display: none;">
                        <span>Numbers & Symbols</span>
                        <svg class="fa fa-angle-down" aria-hidden="true"></svg>
                    </a>
                    <div class="" id="menu2">
                        <p><?= lang('click_symbol_letter_to_add')?></p>
                        <ul class="customletters custommsg">
                        <?php
                        foreach ($characters as $character) { 
                            if($character->CharacterType == 0){
                            ?>
                            <!-- <li data-product_id="<?= $character->CharacterID; ?>" data-product_price="<?= $character->price; ?>" onclick="addImage(canvas,'<?php echo base_url( $character->CharacterImage); ?>')">
                                <div class="img-overlay">
                                    <img src="<?php echo base_url( $character->CharacterImage); ?>">
                                </div>
                                <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                            </li> -->
                            <li data-product_id="<?= $character->CharacterID; ?>" data-product_price="<?= $character->price; ?>" onclick="addImage(canvas,'<?php echo base_url( $character->CharacterImage); ?>')">
                                <img src="<?php echo base_url( $character->CharacterImage); ?>">
                            </li>
                        <?php 
                            }
                        } ?>
                        </ul>
                    </div>
                </div>
                <!--<div class="list-group">
                    <a href="#menu3" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span>Ribbon Color<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu3">
                        <p>Click Symbols and Shapes to add them to your box.</p>
                        <ul class="customletters custommsg">
                            <li>
                                <div class="img-overlay">
                                    <img src="<?php /*echo front_assets(); */?>images/red.png">
                                </div>
                                <img src="<?php /*echo front_assets(); */?>images/closes.png" class="close">
                            </li>
                            <li>
                                <div class="img-overlay">
                                    <img src="<?php /*echo front_assets(); */?>images/green.png">
                                </div>
                                <img src="<?php /*echo front_assets(); */?>images/closes.png" class="close">
                            </li>
                            <li>
                                <div class="img-overlay">
                                    <img src="<?php /*echo front_assets(); */?>images/pink.png">
                                </div>
                                <img src="<?php /*echo front_assets(); */?>images/closes.png" class="close">
                            </li>
                        </ul>
                    </div>
                </div>-->
                <section class="content products titlarea mb-4">
                    <div class="container w-100">
                        <div class="row">
                            <div class="col-md-12 choco-msg_border-bot">
                                <span class="choco_msg-step">
                                    <b><?= lang('step_3')?></b>
                                </span>
                                <span class="choco_msg-desc">
                                    <?= lang('ribbons')?>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <img src="<?=base_url()?>uploads/character/ribbon.png" class="ribbon-img">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 custom-radios">
                            <?php
                            foreach ($ribbons as $ribbon) { ?>
                                <a href="javascript:void(0);" data-ribbon_color="<?php echo $ribbon->RibbonID; ?>" data-ribbon_price="<?php echo $ribbon->RibbonPrice; ?>" class="selectRibbon">
                                    <img src="<?php echo base_url($ribbon->RibbonImage); ?>" width="150px" height="150px">
                                </a>
                            <?php }
                            ?>
                               
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-5 sidebox choco-msg-right">
                <div class="product-info">
                    <h2 class="choco_msg-headng custom-margin "><?php echo $box->Title; ?></h2>
                    <input type="hidden" id="customsgbox" data-box_size="<?php echo $box->BoxSpace; ?>" data-box_min_price="<?php echo $box->BoxPrice; ?>" data-box_id="<?php echo $box->BoxID; ?>">
                    <input type="hidden" name="image" id="custom_box" value="<?php echo base_url($box->BoxImageInside); ?>" >

                    <!-- <canvas id="canvas" style="width: 100% !important"></canvas>
                    <button onclick="clearCanvas(canvas, svgState)" class="clears-btn" data-box-price="<?= $box->BoxPrice?>">Clear</button> -->
                    <div id="customsgbox" data-box_size="<?php echo $box->BoxSpace; ?>" data-box_min_price="<?php echo $box->BoxPrice; ?>" data-box_id="<?php echo $box->BoxID; ?>"  ?>
                        <ul>

                        </ul>
                    </div>
                    <!-- <h2 class="choco_msg-headng inside-box choco-msg_border-bot">What's inside the box</h2>
                    <p class="choco-p-desc choco-msg_border-bot">4 X  CREMOLA</p>
                    <p class="choco-p-desc choco-msg_border-bot">4 X  CREMOLA</p>
                    <p class="choco-p-desc choco-msg_border-bot">4 X  CREMOLA</p> -->
                    <!-- <h5>
                        <?php echo lang('box_price'); ?>
                        <strong><?php echo number_format($box->BoxPrice, 2); ?></strong>
                        <?php echo lang('sar'); ?>
                    </h5>
                    <h5><?php echo lang('box_capacity'); ?>
                        <strong><?php echo $box->BoxSpace; ?></strong> <?php echo lang('pieces'); ?></h5>
                    <?php echo $box->Description; ?>
                    
                    <h2><?php echo $ribbon->Title; ?></h2> -->
                    <!-- <h5>
                        Price
                        <strong><?php echo number_format($ribbon->RibbonPrice, 2); ?></strong>
                        <?php echo lang('sar'); ?>
                    </h5> -->
                    
                    <p class=" total-price-new choco-msg_border-bot"><?php echo lang('price'); ?> <span><b class="ProductP"><?php echo number_format($box->BoxPrice , 2); ?></b> <?php echo lang('sar'); ?></span></p>
                    <p class=" total-price-new choco-msg_border-bot"><b><?= lang('box_capacity')?></b>  <span class="align-right"><?= $box->BoxSpace?> </span></p>
                    <p class=" total-price-new choco-msg_border-bot"><b><?= lang('ribbon')?> <?= lang('price')?>:</b>  <span class="align-right"><span class="RibbonPriceP">0 <?php echo lang('sar'); ?></span> </span></p>
                    <div class="row quantity-row">
                        <div class="col-md-6">
                            <h6 class="choco-quantity-new"><?php echo lang('quantity'); ?></h6>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" class="ProductIDs" name="ProductIDs">
                            <input type="hidden" class="ProductPrice" name="ProductPrice" value="<?php echo $box->BoxPrice; ?>">
                            <input type="hidden" class="BoxID" name="BoxID" value="<?php echo $box->BoxID; ?>">
                            <input type="hidden" id="RibbonColor" class="RibbonColor" name="RibbonColor"  >
                            <input type="hidden" id="RibbonPrice" class="RibbonPrice" name="RibbonPrice" >
                            <input type="hidden" class="ItemType" value="Choco Message">
                            <input id="after" name="Quantity" class="form-control Quantity " type="number" value="1" min="1"/>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary cart-n-buy mt-0 align-right" onclick="addChocoBoxToCart();"><?php echo lang('add_to_basket'); ?>
                            </button>
                            <!-- <button type="button" class="btn btn-primary cart-n-buy" onclick="addChocoBoxToCart();">Buy Now
                            </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).on('click', '.selectRibbon', function () {
        showCustomLoader();
        var currency = "<?= lang('sar')?>";
        var ribbon_color = $(this).data('ribbon_color');
        var ribbon_price = Number($(this).data('ribbon_price'));
        $('#RibbonColor').val(ribbon_color);
        $('#RibbonPrice').val(ribbon_price);
        $('.RibbonPriceP').html(formatNumber(ribbon_price)+' '+currency);
        var price = Number($('.ProductPrice').val());
        var total = price + ribbon_price;
        $('.ProductP').html(formatNumber(total));
        setTimeout(function () {
            hideCustomLoader()
        }, 500);
       
    });
</script>
<script src="<?php echo front_assets(); ?>js/fabric/fabric.min.js"></script>
<script src="<?php echo front_assets(); ?>js/fabric/index.js"></script>