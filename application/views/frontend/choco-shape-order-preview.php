<style>
	table.order-preview td {
		width: 33.3%;
	}
	
	@media(max-width:1024px){
		.shape_order_prev {
			/* width: 100%; */
			margin-right: auto;
			margin-left:auto;
			padding-top:30px;
			padding-bottom:30px;
		}
		.add-to-cart_choc {
			width: 100%;
		}
	}
	.count_quantity {
		display: inline-block;
		padding: 0 20px;
		color: #606060;
	}
	h6.kg_quantity {
		display: inline-block;
		padding: 0 0px;
		vertical-align: text-top;
		color: #606060;
	}
	#decrease,
	#increase {
		border-radius: 50px;
		line-height: 0;
		width: 25px;
		background: transparent;
	}
	.new-btn-added{
		padding: 13px 10px !important;
		width: 30px !important;
		height: 30px !important;
		background-color: #F4EBD3 !important;
		border: #F4EBD3 !important;
		color: #BD9371 !important;
		box-shadow: #929292 0px 2px 7px !important;
		font-size: 20px !important;
		border-radius: 4px !important;
	}
	.value-button {
		display: inline-block;
		border: 1px solid #ddd;
		margin: 0px;
		width: 40px;
		height: 20px;
		text-align: center;
		vertical-align: middle;
		padding: 11px 0;
		background: #eee;
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	.value-button:hover {
		cursor: pointer;
	}
	input#number {
		text-align: center;
		border: none;
		margin: 0px;
		width: 40px;
		height: 40px;
		font-weight: 900;
		font-size: 20px;
		display: inline-block;
		vertical-align: middle;
	}
</style>
<?php $choco_shape = getPageContent(12, $lang) ?>
<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Shape Order Preview</h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li>Shape Order Preview</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="container invoice">
	<div class="row d-md-flex justify-content-lg-start justify-content-md-center">
		<div class="col-md-6">
				<table class="box-order-table">
					<tbody class="order-title-details-body">
						<tr class="order-title-details-row"style="background-color: #e5daca;">
						<h3 class="order-title text-center"><th colspan="4">Your Order Details</th></h3>
						</tr>
					</tbody>
				</table>
		</div>

		<div class="col-xl-12 col-lg-12 col-md-12">
			<div class="row d-md-flex justify-content-md-center">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 order-lg-first order-md-last order-1">
					<h3 class="product-title" style="text-align:left"><?= @$shapeDetail->Title?></h3>
					<?php 
						$totalPrice = 0.00;
					?>
					<table class="order-preview mb-5" width="100%">
						<tbody class="order-details-body">
							<?php
								if($contentType->ContentTypeID == 4){
							?>
							<tr class="order-details-row">
								<td class="order-details-head text-left">Content-Type</td>
								<td class="order-details-value text-center"><?= @$contentType->Title ?></td>
								<td class="order-details-price text-right"><?= ($contentType->Price > 0)?@$contentType->Price:''?></td>
								<?php $totalPrice += $contentType->Price ?>
							</tr>
							<tr class="order-details-row">
								<td class="order-details-head text-left">Type</td>
								<td class="order-details-value text-center"><?= @$subcontentType->Title ?></td>
								<td class="order-details-price text-right"><?= ($subcontentType->Price > 0)?@$subcontentType->Price:'' ?></td>
								<?php $totalPrice += $subcontentType->Price ?>
							</tr>
							<tr class="order-details-row">
								<td class="order-details-head text-left">Color</td>
								<td class="order-details-value text-center"><?= @$fillColor->title ?></td>
								<td class="order-details-price text-right"><?= ($fillColor->price>0)?@$fillColor->price:'' ?></td>
								<?php $totalPrice += $fillColor->price ?>
							</tr>
							<tr class="order-details-row">
								<td class="order-details-head text-left">Filling</td>
								<?php 
									$fillingsArr = [];
									$fillingsAmount = 0.00;
									foreach($fillingsList as $key => $item){
										array_push($fillingsArr,$item->title);
										$fillingsAmount += $item->price;
									}
								?>
								<td class="order-details-value text-center"><?= implode(',',$fillingsArr) ?></td>
								<td class="order-details-price text-right"><?= ($fillingsAmount >0)?$fillingsAmount:'' ?></td>
								<?php $totalPrice += $fillingsAmount ?>
							</tr>
							<tr class="order-details-row"  style="display:none">
								<td class="order-details-head text-left">Wrapping</td>
								<?php
									
									$wrappingsArr = [];
									$wrappingAmount = 0.00;
									if(@$wrappingList)
										{
									foreach($wrappingList as $key => $item){
										array_push($wrappingsArr,$item->Title);
										$wrappingAmount += $item->WrappingPrice;
									}
								}
								?>
								<td class="order-details-value text-center"><?= implode(',',$wrappingsArr) ?></td>
								<td class="order-details-price text-right"><?= ($wrappingAmount >0 )?$wrappingAmount:'' ?></td>
								<?php $totalPrice += $wrappingAmount ?>
							</tr>
							<tr class="order-details-row">
								<td class="order-details-head text-left">Text</td>
								<td class="order-details-value text-center"><?= @$customerContent?></td>
								<td class="order-details-price text-right"></td>
							</tr>
							<!-- <tr class="order-details-row">
								<td class="order-details-head text-left">Text Type</td>
								<td class="order-details-value text-center"><?= @$textTypePreview->Title?></td>
								<td class="order-details-price text-right">0.00</td>
							</tr> -->
							<tr class="order-details-row">
								<td class="order-details-head text-left">Powder color</td>
								<td class="order-details-value text-center"><?= @$powderColor->title?></td>
								<td class="order-details-price text-right"><?= ($powderColor->price>0)?@$powderColor->price:''?></td>
								<?php $totalPrice += $powderColor->price ?>
							</tr>
							<tr class="order-details-row">
								<td class="order-details-head text-left">Price For Per KG</td>
								<td class="order-details-value text-center"></td>
								<td class="order-details-price text-right"><?= @$shapeDetail->PricePerKG ?></td>
								
							</tr>
							<tr class="order-details-row">
								<td class="order-details-head text-left">Quantity</td>
								<td class="order-details-value text-center"></td>
								<td class="order-details-price text-right"><?= @$Quantity?></td>
								<?php 
								$totalPrice += $shapeDetail->PricePerKG;
								$total = $totalPrice; 
								?>
							</tr>
							<input type="hidden" id="total_ing" value="<?= $total?>">
							<tr class="order-details-row">
								<td class="order-details-head text-left" >Total</td>
								<td class="order-details-value text-center"></td>
								<td class="order-details-price text-right" ><strong id="price_view"><?= $totalPrice*$Quantity ?></strong><small>SAR</small></td>
							</tr>
							<?php }else if($contentType->ContentTypeID == 5){?>
								<tr class="order-details-row">
								<td class="order-details-head text-left">Type</td>
								<td class="order-details-value text-center"><?= @$contentType->Title ?></td>
								<td class="order-details-price text-right"><?= ($contentType->Price > 0)?@$contentType->Price:''?></td>
								<?php $totalPrice += $contentType->Price ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Content-Type</td>
									<td class="order-details-value text-center"><?= @$subcontentType->Title ?></td>
									<td class="order-details-price text-right"><?= ($subcontentType->Price>0)?@$subcontentType->Price:'' ?></td>
									<?php $totalPrice += $subcontentType->Price ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Color</td>
									<td class="order-details-value text-center"><?= @$fillColor->title ?></td>
									<td class="order-details-price text-right"><?= ($fillColor->price>0)?@$fillColor->price:'' ?></td>
									<?php $totalPrice += $fillColor->price ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Filling</td>
									<?php 
										$fillingsArr = [];
										$fillingsAmount = 0.00;
										foreach($fillingsList as $key => $item){
											array_push($fillingsArr,$item->title);
											$fillingsAmount += $item->price;
										}
									?>
									<td class="order-details-value text-center"><?= implode(',',$fillingsArr) ?></td>
									<td class="order-details-price text-right"><?= ($fillingsAmount>0)?$fillingsAmount:'' ?></td>
									<?php $totalPrice += $fillingsAmount ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Wrapping</td>
									<?php
										
										$wrappingsArr = [];
										$wrappingAmount = 0.00;
										if(@$wrappingList)
										{
											foreach($wrappingList as $key => $item){
												array_push($wrappingsArr,$item->Title);
												$wrappingAmount += $item->WrappingPrice;
											}
										}
										
									?>
									<td class="order-details-value text-center"><?= implode(',',$wrappingsArr) ?></td>
									<td class="order-details-price text-right"><?= ($wrappingAmount>0)?$wrappingAmount:'' ?></td>
									<?php $totalPrice += $wrappingAmount ?>
								</tr>
									<tr class="order-details-row">
									<td class="order-details-head text-left">Text</td>
									<td class="order-details-value text-center"><?= @$customerContent?></td>
									<td class="order-details-price text-right"></td>
								</tr>
								<!-- <tr class="order-details-row">
									<td class="order-details-head text-left">Text Type</td>
									<td class="order-details-value text-center"><?= @$textTypePreview->Title?></td>
									<td class="order-details-price text-right">0.00</td>
								</tr> -->
								<tr class="order-details-row">
									<td class="order-details-head text-left">Powder color</td>
									<td class="order-details-value text-center"><?= @$powderColor->title?></td>
									<td class="order-details-price text-right"><?= ($powderColor->price>0)?@$powderColor->price:''?></td>
									<?php $totalPrice += $powderColor->price ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Price For 1 KG</td>
									<td class="order-details-value text-center"></td>
									<td class="order-details-price text-right"><?= @$shapeDetail->PricePerKG ?></td>
									
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Quantity</td>
									<td class="order-details-value text-center"></td>
									<td class="order-details-price text-right"><?= @$Quantity?></td>
									<?php
										$total = ($totalPrice + $shapeDetail->PricePerKG); 
										$totalPrice += ($shapeDetail->PricePerKG * $Quantity);
									 ?>
								</tr>
								<input type="hidden" id="total_ing" value="<?= $total?>">
								<tr class="order-details-row">
									<td class="order-details-head text-left"style="font-size:30px !important;" >Total</td>
									<td class="order-details-value text-center"></td>
									<td class="order-details-price text-right"style="font-size:30px !important;"><strong id="price_view"><?= $totalPrice ?></strong><small>SAR</small></td>
								</tr>
							<?php }else if($contentType->ContentTypeID == 6){?>
								<tr class="order-details-row">
								<td class="order-details-head text-left">Type</td>
								<td class="order-details-value text-center"><?= @$contentType->Title ?></td>
								<td class="order-details-price text-right"><?= ($contentType->Price>0)?$contentType->Price:'' ?></td>
								<?php $totalPrice += $contentType->Price ?>
								</tr>
								<tr class="order-details-row">
								<td class="order-details-head text-left">Image</td>
								<td class="order-details-value text-center"><img src="<?= base_url($imageFile) ?>" width="100"></td>
								<td class="order-details-price text-right"></td>
							</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Color</td>
									<td class="order-details-value text-center"><?= @$fillColor->title ?></td>
									<td class="order-details-price text-right"><?= ($fillColor->price>0)?@$fillColor->price:'' ?></td>
									<?php $totalPrice += $fillColor->price ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Filling</td>
									<?php 
										$fillingsArr = [];
										$fillingsAmount = 0.00;
										foreach($fillingsList as $key => $item){
											array_push($fillingsArr,$item->title);
											$fillingsAmount += $item->price;
										}
									?>
									<td class="order-details-value text-center"><?= implode(',',$fillingsArr) ?></td>
									<td class="order-details-price text-right"><?= ($fillingsAmount>0)?$fillingsAmount:'' ?></td>
									<?php $totalPrice += $fillingsAmount ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Wrapping</td>
									<?php
										
										$wrappingsArr = [];
										$wrappingAmount = 0.00;
										if(@$wrappingList)
										{
										foreach($wrappingList as $key => $item){
											array_push($wrappingsArr,$item->Title);
											$wrappingAmount += $item->WrappingPrice;
										}
									}
									?>
									<td class="order-details-value text-center"><?= implode(',',$wrappingsArr) ?></td>
									<td class="order-details-price text-right"><?= ($wrappingAmount>0)?$wrappingAmount:'' ?></td>
									<?php $totalPrice += $wrappingAmount ?>
								</tr>
								<!-- <tr class="order-details-row">
									<td class="order-details-head text-left">Text Type</td>
									<td class="order-details-value text-center"><?= @$textTypePreview->Title?></td>
									<td class="order-details-price text-right">0.00</td>
								</tr> -->
								<tr class="order-details-row">
									<td class="order-details-head text-left">Powder color</td>
									<td class="order-details-value text-center"><?= @$powderColor->title?></td>
									<td class="order-details-price text-right"><?= ($powderColor->price>0)?@$powderColor->price:''?></td>
									<?php $totalPrice += $powderColor->price ?>
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">Price Per KG</td>
									<td class="order-details-value text-center"></td>
									<td class="order-details-price text-right"><?= @$shapeDetail->PricePerKG ?></td>
									
								</tr>
								<tr class="order-details-row">
									<td class="order-details-head text-left">KG</td>
									<td class="order-details-value text-center"></td>
									<td class="order-details-price text-right"><?= @$Quantity?></td>
									<?php 
									$totalPrice += $shapeDetail->PricePerKG;
									$total = $totalPrice; 
									$totalPrice *= $Quantity;
									?>
								</tr>
								<input type="hidden" id="total_ing" value="<?= $total?>">
								<tr class="order-details-row">
									<td class="order-details-head text-left"style="font-size:30px!important;">Total</td>
									<td class="order-details-value text-center"></td>
									<td class="order-details-price text-right" style="font-size:30px!important;"><strong id="price_view"><?= $totalPrice ?></strong><small>SAR</small></td>
								</tr>
							<?php }?>
						</tbody>
					</table>
				</div>

				<div class="col-lg-6 col-md-6 shape_order_prev">
					<div class="image-container h-auto">
						<div class="view-item position-relative p-0 d-flex justify-content-center m-auto mb-4" style="width:250px;height:250px;background-color: <?= @$fillColor->code ?>">
						<?php
						$content_image = "";
						if($contentType->ContentTypeID == 6)
						{
							$content_image = $imageFile;
						} else if($contentType->ContentTypeID == 4)
						{
							$content_image = $contentType->Image;
						} else if($contentType->ContentTypeID == 5)
						{
							// print_rm(get_images_id(@$formdata['subcontentTypeImageId'], 'content_type_sub_image'));
							$content_image = get_images_id(@$formdata['subcontentTypeImageId'], 'content_type_sub_image')[0]->ImageName;
							
						}
						
						?>
						<!-- <img class="choco-bg" id="choco-content-type" src="<?php echo base_url($contentType->Image); ?>" width="250px" height="auto" alt="" style="position: absolute;"> -->
						<?php
						$style="position: absolute;";
						if(trim(strtolower($shapeDetail_en->Title)) == "parallelogram")
						{
							$style .= "width:100px;left:111px;top:65px";
						}else
						{
							$style .= "top:50px;";
						}
						?>
						<img class="choco-bg" id="choco-content-type" src="<?= base_url($content_image)?>" width="150px" height="auto" alt="" style="<?= $style; ?>"/>
						<img class="choco-bg" id="shape_image" src="<?php echo base_url($shapeDetail->ShapeImage); ?>" width="250px"
							height="auto" alt="" style="position: absolute;">	
					</div>
					</div>
					<div class="col-md-12 d-block mb-4 mt-4 choco_preview">
						<form action="" method="POSt" id="chocoShapeAddToCartForm">
							<div class="row">
								Price: <strong id="price_view_2"><?= $totalPrice ?></strong><small>SAR</small>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 text-center">
									<h6 class="kg_quantity ">KG</h6>
									<div class="count_quantity">
										
										<div class="value-button new-btn-added" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
											
											<input id="number" name="Quantity" type="number"
												value="<?= @$formdata['quantity'] ?>"  data-max="<?= @$shapeDetail->MaximumOrder?>"
												data-min="<?= @$shapeDetail->MinimumOrder?>" />
										<div class="value-button new-btn-added" id="increase" onclick="increaseValue()" value="Increase Value">+</div>

										
									</div>	  
								</div>
							</div>
							<input type="hidden" value="<?= $shapeDetail->ShapeImage ?>" name="CustomizeShapeImage">
							<input type="hidden" value="Choco Shape" name="ItemType">
							<input type="hidden" value="<?= @$formdata['shapeId'] ?>" name="shapeId">
							<!-- <input type="hidden" value="<?= @$formdata['quantity'] ?>" name="Quantity"> -->
							<input type="hidden" value="<?= @$formdata['contentTypeId'] ?>" name="ContentTypeId">
							<input type="hidden" value="<?= @$formdata['customerContent'] ?>" name="customerContent">
							<input type="hidden" value="<?= @$formdata['textTypeId'] ?>" name="contentTypeSubId">
							<input type="hidden" value="<?= @$formdata['subcontentTypeImageId'] ?>" name="contentTypeSubImageId">
							<input type="hidden" value="<?= @$formdata['fill_color'] ?>" name="fill_color">
							<input type="hidden" value="<?= @$formdata['filling'] ?>" name="filling">
							<input type="hidden" value="<?= @$formdata['wrapping'] ?>" name="wrapping">
							
							<input type="hidden" value="<?= @$formdata['powderColor'] ?>"  name="powderColor">
							<input type="hidden" value="<?= $powderColor->colorCode ?>" name="color_code" >
							<input type="hidden" value="<?= @$formdata['shapecustomImageId'] ?>" name="customImageId">
							<input type="hidden" value="<?= @$totalPrice ?>" name="totalPrice">
							<button type="button" onclick="addChocoShapeToCart()"  class="add-to-cart_choc "><?php echo lang('add_to_basket'); ?></button>
						</form>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="<?php echo front_assets(); ?>js/hex_convert.js" ></script>
<script>
	$(document).ready(() => {
		const rgb = hexToRgb($('input[name="color_code"]').val());
		if (rgb.length !== 3) {
		return;
		}

		const color = new Color(rgb[0], rgb[1], rgb[2]);
		const solver = new Solver(color);
		const result = solver.solve();
		var style= "position: absolute;";
		
		<?php
		if(trim(strtolower($shapeDetail_en->Title)) == "parallelogram")
		{
			?>
				style += "width:100px;left:111px;top:65px";
			<?php
		}else
		{
			?>
			style += "top:50px;";
			<?php
		}
		?>
		$('#choco-content-type').attr('style', result.filter+style);
	
	});
	function increaseValue() {
		var value = parseInt(document.getElementById('number').value, 10);
		var dataMax = document.getElementById('number').dataset.max;
		value = isNaN(value) ? 0 : value;
		value++;
		if(value <= dataMax)
		{
			$('#price_view').text($('#total_ing').val()*value);
			$('#price_view_2').text($('#total_ing').val()*value);
			document.getElementById('number').value = value;
		}
	}

	function decreaseValue() {
		var value = parseInt(document.getElementById('number').value, 10);
		var dataMin = document.getElementById('number').dataset.min;
		value = isNaN(value) ? 0 : value;
		value < 1 ? value = 1 : '';
		value--;
		if(value >= dataMin)
		{
			$('#price_view').text($('#total_ing').val()*value);
			$('#price_view_2').text($('#total_ing').val()*value);
			document.getElementById('number').value = value;
		}
	}
</script>