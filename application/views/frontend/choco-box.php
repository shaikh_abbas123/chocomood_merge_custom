<?php $choco_box = getPageContent(11, $lang) ?>
<?php $home = getPageContent(7, $lang) ?>
<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $choco_box->Title; ?></h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?php echo $choco_box->Title; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="content single-products productskg customizebox">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="wbox">
                    <ul id="customice">
                        <?php
                            foreach ($products as $product)
                            { ?>
                                <li data-product_id="<?php echo $product->ProductID; ?>" data-product_price="<?php echo $product->Price; ?>">
                                    <div class="img-overlay">
                                        <img src="<?php echo base_url(get_images($product->ProductID, 'product', false)); ?>">
                                    </div>
                                    <h4><?php echo $product->Title; ?> <span><b><?php echo number_format($product->Price, 2); ?></b> <?php echo lang('sar'); ?></span></h4>
                                    <img src="<?php echo front_assets(); ?>images/closes.png" class="close">
                                </li>
                            <?php }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 sidebox">
                <div class="product-info">
                    <div id="custombox" data-box_size="<?php echo $box->BoxSpace; ?>" data-box_min_price="<?php echo $box->BoxPrice; ?>" data-box_id="<?php echo $box->BoxID; ?>" style="background-image: url('<?php echo base_url($box->BoxImage); ?>');">
                        <ul>

                        </ul>
                    </div>
                    <h2><?php echo $box->Title; ?></h2>
                    <h5>
                        <?php echo lang('box_price'); ?>
                        <strong><?php echo number_format($box->BoxPrice, 2); ?></strong>
                        <?php echo lang('sar'); ?>
                    </h5>
                    <h5><?php echo lang('box_capacity'); ?>
                        <strong><?php echo $box->BoxSpace; ?></strong> <?php echo lang('pieces'); ?></h5>
                    <?php echo $box->Description; ?>
                    <h5 class="bordered"><?php echo lang('price'); ?> <span><b class="ProductP"><?php echo number_format($box->BoxPrice, 2); ?></b> <?php echo lang('sar'); ?></span></h5>
                    <div class="row">
                        <div class="col-md-6">
                            <h6><?php echo lang('quantity'); ?></h6>
                        </div>
                        <div class="col-md-6">
                            <input id="after" name="Quantity" class="form-control Quantity" type="number" value="1"
                                   min="1"/>
                            <input type="hidden" class="ProductIDs" name="ProductIDs">
                            <input type="hidden" class="ProductPrice" name="ProductPrice" value="<?php echo $box->BoxPrice; ?>">
                            <input type="hidden" class="BoxID" name="BoxID" value="<?php echo $box->BoxID; ?>">
                            <input type="hidden" class="RibbonColor" name="RibbonColor" value="<?php echo $RibbonColor; ?>">
                            <input type="hidden" class="ItemType" value="Chocobox">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary" onclick="addChocoBoxToCart();"><?php echo lang('add_to_basket'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>