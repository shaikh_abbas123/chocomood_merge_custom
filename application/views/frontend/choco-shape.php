<style type="text/css">
    html,body {
    overflow-x: hidden;
    }
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .btn {
        border: 2px solid gray;
        color: gray;
        background-color: white;
        padding: 8px 20px;
        border-radius: 8px;
        font-size: 20px;
        font-weight: bold;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .draggable {
        filter: alpha(opacity=60);
        opacity: 0.6;
    }

    .dropped {
        position: static !important;
        list-style-type: none;
        float: left;
        margin: 0 1% 0 0;
        width: 19%;
    }

    #dvSource,
    #dvDest {
        border: 5px solid #ccc;
        padding: 5px;
        min-height: 100px;
        width: 430px;
    }

    img {
        -webkit-transition-duration: 0s;
        -o-transition-duration: 0s;
        transition-duration: 0s;
    }

    .vl1 {
        border-left: 2px solid green;
        height: 500px;
        position: absolute;
        left: calc(50% - 210px);
        margin-left: -3px;
        top: 0;
        filter: blur(0.6px);
        border-image: linear-gradient(white, #A07250, white) 5 stretch;
    }

    .vl2 {
        border-left: 2px solid green;
        height: 500px;
        position: absolute;
        right: calc(50% - 420px);
        margin-left: -3px;
        top: 0;
        filter: blur(0.6px);
        border-image: linear-gradient(white, #A07250, white) 5 stretch;
    }

    @media(max-width:1280px) {
        .vl1 {
            left: calc(50% - 190px);
            height: 460px;
        }

        .vl2 {
            right: calc(50% - 385px);
            height: 460px;
        }
    }

    @media(max-width:1024px) {
        .vl1 {
            left: calc(50% - 156px);
            height: 460px;
        }

        .vl2 {
            right: calc(50% - 323px);
            height: 460px;
        }
    }

    @media(max-width:991px) {

        .vl1,
        .vl2 {
            display: none;
        }
    }


    .custom-container {
        width: 90% !important;
    }
    .inbox {
        text-align: left;
        margin-bottom: 35px;
        position: relative;
        background-color: white !important;
        padding: 25px !important;
        border: 1px solid #bd937161;
        border-radius: 5px;
    }

    .inbox i {   
        color: #fb7176;
        font-size: 20px;
        -webkit-transition-duration: 0.65s;
        -o-transition-duration: 0.65s;
        transition-duration: 0.65s;
        padding:0px 4px !important;
    }
    .add_to_Cart_dv {
        text-align: right;
    }
    .choco_under_img_size{
        margin-top:30px;
        margin-bottom:30px;
    }
    p.text-left.custom-shape-small-text {
        margin-left: 0px;
    }

    .custom-shape-small-text{
        font-size:14px;
    }



/* SIDEBAR */

#sidebar1 .list-group-item.list-item-product,
#sidebar1 .list-group-item.list-item-brand {
    color: #50456d !important;
}

#sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand {
    margin: 0 !important;
    color: black !important;
    background-color: #f4ebd3 !important;
    border: none !important;
    border-radius: 0 !important;
    font-size: 15px !important;
    /* border-bottom: 1px solid #adadad !important; */
    font-weight: bold !important;
    padding: 15px 35px;
}
#sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand {
    color: #50456d !important;
}


.subList-item-product {
    color: #50456d !important;
    font-weight: bold;
    font-size: 12px !important;
    padding: 7px 30px 10px 40px !important;
    transition: .4s;
    text-align: left;
 
}

div#sidebar1 .list-group a .submenu{
    color: #50456d !important;
    font-weight: bold;
    font-size: 12px !important;
    padding: 7px 30px 10px 40px !important;
    transition: .4s;
    text-align: left;
}
#sidebar1 .collapse-span .list-group {
    margin-bottom:0px !important;;
}


#sidebar1 .collapse.in {
    border-bottom: 1px solid #ddd !important;
}

div#sidebar1 .list-group .collapse.in #sidebar1 .collapse.in:last-child {
    border-bottom: none !important;
}

/* .filter-column{
        display:flex;
        justify-content:center;
    } */
@media (max-width: 1500px) {

    #sidebar1 .list-group-item.list-item-product,
    #sidebar1 .list-group-item.list-item-brand {
        font-size: 14px !important;
        padding: 15px 15px !important;
    }
}

@media screen and (min-width:1400px) and (max-width: 1650px) {
    .choco-name {
        padding: 7px 30px 10px 25px !important;
    }
}

/* @media screen and (min-width:1201px) and (max-width: 1399px) {
    .choco-name {
        padding: 7px 22px 10px 0px !important;
    }

    .choco-name .left-submenu {
        width: 100px !important;
    }

    .choco-name .right-submenu {
        text-align: right !important;
    }
} */
i.bi.bi-plus,
.bi.bi-dash{
    font-size: 22px;
}

/* @media screen and (min-width:992px) and (max-width:1200px) {

        #sidebar1 .list-group-item.list-item-product,
        #sidebar1 .list-group-item.list-item-brand {
            font-size: 12px !important;
            padding: 12px 8px !important;
        }
    } */

.dv-hide-desktop {
    display: none;
    position: fixed;
    top: 122px;
    background: #32231f;
    width: 100%;
    z-index: 9;
    height: 100%;
    overflow-y: scroll;
    padding-bottom: 89px;
    padding-top: 30px;
}

.dv-hide-desktop select.form-control {
    background-color:
        transparent;
    color: #fff;
    border: 1px solid #f4ebd3;
    margin-bottom: 10px !important;
    width: 94%;
    margin: 0 auto;
}

.dv-hide-desktop h6 {
    color: #fff;
    font-size: 16px;
    width: 93%;
    margin-bottom: 20px !important;
    margin: 0 auto;
}

.dv-hide-desktop h6 span {
    float: right;
}

img.only-mob {
    display: none;
}

@media only screen and (max-width:600px) {
    img.only-mob {
        display: block;
    }

    .col-md-12.holder a {
        display: none;
    }

    .col-md-12.holder span {
        display: none;
    }

    .col-md-12.holder a.jp-previous,
    .col-md-12.holder a.jp-next {
        display: inline-block;
        width: 40%;

    }


.custom-container.on-mob-w-100 {
        width: 100% !important;
    }


}

@media only screen and (max-width:601px) {
    .to-hide-600 {
        display: none !important;
    }
}

img.only-mob {
    position: fixed;
    top: 141px;
    right: 16px;
    z-index: 9;
}

.mobile-filter {
    display: none !important;
}

@media screen and (max-width:1200px) and (min-width:600px) {
    .mobile-filter {
        /* display: block !important; */
        margin-bottom: 30px;
    }

    .desktop-filter {
        display: none !important;
    }
}

#sidebar1 .subList-item-product, #sidebar1 .subList-item-brand {
    color: #50456d !important;
}
#sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand {
    color: #50456d !important;
}
.productsContainer{
    min-height:unset !important;
}
/* .margin-zero {
    margin: 10px 0px 0px 0px !important;
} */
@media screen and (max-width:1199px) and (min-width:600px) {
.tab-filter-feature{
    display:block !important;
}
.tab-filter-feature label.customcheck {
    margin: 0;
    color: #50456d;
    background-color: #f4ebd3;
    border: none;
    border-radius: 0;
    font-size: 15px;
    font-weight: bold;
    margin-bottom:10px;
}
 .content.products #sidebar .list-group-item {
    padding: 15px 15px !important;
}
div#sidebar1 {
    margin-bottom: -15px;
}
}
@media screen and (max-width:767px) and (min-width:600px) {
    .content.titlarea .filter select.form-control {
    margin-top: 0 !important;
}
.align-filter-right {
    display: block !important;
    margin-bottom:25px !important;
}
.custom-filter{
display:block !important;
}
.right-filter-icon{
 padding-right:10px !important;
}
}
@media screen and (max-width:1024px) and (min-width:992px){
.content.titlarea .filter select.form-control {
    margin-left: 10px !important;
}
/* .content.titlarea .filter .iconsrow {
    padding-right: 30px;
} */
.right-filter-icon{
 /* padding-left:30px !important; */
 margin-right:10px !important;
}
}
@media screen and (max-width:991px) and (min-width:768px){
.content.titlarea .filter select.form-control {
    margin-left: 10px !important;
}
.right-filter-icon{
 margin-right:10px !important;
}
.content.titlarea .filter .iconsrow {
    margin-left: 50px !important;
}
}

#sidebar .list-group.onlyChkBox .customcheck {
    margin: 0;
    color: #50456d;
    background-color: #f4ebd3;
    border: none;
    border-radius: 0;
    font-size: 15px;
    font-weight: bold;
}

a .left-submenu, a .right-submenu, a .choco-name {
    color: #50456d !important;
    font-weight: bold;
    font-size: 12px;
}
.choco-name {
    padding: 7px 30px 10px 40px;
    transition: .4s;
    text-align: left;
}
/* SIZEBAR */

</style>
<?php $choco_shape = getPageContent(12, $lang) ?>
<!-- <section class="content customize first " style="background:url(http://localhost/chocomood_merge_custom/uploads/images/choco-shape-stp1.png);"> -->
<section class="content products titlarea pt-0">
    <div class="edProdBanner" style="background-image:url(<?= base_url($customizations[0]->Image) ?>)">
        <div class="container">
            <h2>Choco Shape</h2>
            <ul>
                <li><a href="<?= base_url('customise');?>">Home</a></li>
                <li><a href="<?= base_url('customize/choco_shape');?>">Choco Shape</a>
                </li>

            </ul>
        </div>
    </div>

    <div class="container-fluid to-hide-600">
        <div class="row">
            <div class="col-md-12">
                <div class="filter">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 custom-filter" style="display:flex;">
                            <h6>
                                <i class="fa fa-filter" aria-hidden="true"></i>
                                <?php echo lang('filter'); ?>
                                
                            </h6>
                            <?php
                            $price_highest_to_lowest = "";
                            $price_lowest_to_higher = "";
                            if (isset($_GET['sort']) && $_GET['sort'] != '') {
                                if ($_GET['sort'] == 'price_highest_to_lowest') {
                                    $price_highest_to_lowest = 'selected';
                                }

                                if ($_GET['sort'] == 'price_lowest_to_higher') {
                                    $price_lowest_to_higher = 'selected';
                                }

                            }
                            ?>
                            <select class="form-control" onchange="sortProducts(this.value);">
                                <option selected disabled><?php echo lang('Sort_by'); ?></option>
                                <option value="price_highest_to_lowest" <?php echo $price_highest_to_lowest ?>>
                                    <?php echo lang('price'); ?>:
                                    <?php echo lang('Highest_to_lowest'); ?>
                                </option>
                                <option value="price_lowest_to_higher" <?php echo $price_lowest_to_higher ?>>
                                    <?php echo lang('price'); ?>:
                                    <?php echo lang('Lowest_to_Highest'); ?>

                                </option>
                            </select>
                        </div>
                        <!-- <div class="col-md-3 col-sm-3">
                            <h6>
                                <span>
                                    <b><?php echo lang('count'); ?></b>
                                    <i aria-hidden="true" id="now"><?php echo count($shapes); ?> </i>/
                                    <i aria-hidden="true" id="total"> <?php echo @$countShapes; ?></i>
                                </span>
                            </h6> 
                        </div>
                        <div class="col-md-3 col-sm-2">
                            <select class="form-control ProductsPerPage">
                                <option selected disabled><?php echo lang('product_per_page'); ?></option>
                                <option>9</option>
                                <option>15</option>
                                <option>20</option>
                            </select>
                        </div>
                        <div class="col-md-1 iconsrow text-right col-sm-1 align-filter-right">
                            <a onclick="changeGridLayout('items_list');" class="right-filter-icon"><i class="fa fa-list align-filter-right-fa"></i></a>
                            <a onclick="changeGridLayout('items_grid');"class="left-filter-icon"><i class="fa fa-th align-filter-right-fa"></i></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12 choco-msg_border-bot">
                <span class="choco_msg-step">
                    <b>Step 1:</b>
                </span>
                <span class="choco_msg-desc">
                    Select the chocolate shape you would like to customize
                </span>
            </div>
        </div>
        <!-- <h2><?php echo $choco_shape->Title; ?></h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?php echo $choco_shape->Title; ?></li>
                </ul> -->
    </div>
</section>

<section class="content single-products productskg customizebox">
    <div class="container-fluid custom-container on-mob-w-100">
        <div class="row">
            <div class="col-xs-12 tab-filter-feature d-none">
                        <!-- <div class="list-group onlyChkBox">
                            <label class="customcheck">
                                <input class="get_products featured2" type="checkbox" data-url-title=""
                                    name="chkFeature" value="">
                                <span class="checkmark"></span><?php echo lang('featured'); ?>
                            </label>
                        </div> -->
                    </div>



            <div class="col-lg-2 col-xs-12 to-hide-600 mb-4" id="sidebar1">
                <!-- Left Filter Desktop -->
                <div class="row">
                    <?php if (!isset($search)) { ?>
                    <div class="col-lg-12 col-xs-6 to-hide-600 desktop-filter" id="sidebar">
                        <!-- <div class="list-group onlyChkBox">
                            <label class="customcheck">
                                <input class="get_products featured" type="checkbox" data-url-title="" name="chkFeature"
                                    value="">
                                <span class="checkmark"></span><?php echo lang('featured'); ?>
                            </label>
                        </div> -->
                    </div>
                    <?php } ?>
                    <div class="col-xs-6 col-lg-12">
                        <div class="list-group list-group-brand">
                            <a href="#menu-cate-brands" onclick="myFunction(this)"
                                class="list-group-item collapsed list-item-brand" data-toggle="collapse"
                                data-parent="#sidebar" aria-expanded="false">
                                <span><?= lang('chocomood')?><i class="bi bi-plus" style="float:right !important"
                                        aria-hidden="true"></i></span>
                            </a>

                            <div class="collapse" id="menu-cate-brands" aria-expanded="false" style="height: 0px;">
                                <div class="collapse-span">
                                    <!-- <?php
                                    foreach ($brands as $key => $p) {
                                    ?>
                                    <a
                                        href="<?= base_url() ?>product/brand_detail?q=<?=strtolower(str_replace(' ','-',$p->brand_name))?>-s<?=$p->brand_id?>">
                                        <div class="choco-name">
                                            <span class="left-submenu"><?= $p->brand_name?></span>
                                            <span
                                                class="right-submenu"><?= count_product($p->brand_id,'brand');?></span>
                                        </div>
                                    </a>

                                    <?php 
                                        }
                                    ?> -->
                                    <a href="<?= base_url() ?>product/brand_detail?q=chocomood-s0">
                                        <div class="choco-name ">
                                            <span class="left-submenu float-left">All Products</span>
                                            <span class="right-submenu"><?= count_product(0,'brand');?></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-lg-12">
                        <div class="list-group mbottom-5">
                            <a href="#menu-cate-product" onclick="myFunction(this)"
                                class="list-group-item collapsed list-item-product plusToggle" data-toggle="collapse"
                                data-parent="#sidebar1" aria-expanded="false">
                                <span><?= lang('categories')?><i class="bi bi-plus"  style="float:right !important"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate-product" aria-expanded="false" style="height: 0px;">
                                <div class="collapse-span">
                                    <?php $menu_categories = subCategories(0,$language);
                                if($menu_categories){ 
                                    foreach ($menu_categories as $key => $category) {?>
                                    <div class="list-group">
                                        <a href="#submenu-cate-product<?=$category->CategoryID?>"
                                            onclick="myFunction(this)"
                                            class="list-group-item collapsed subList-item-product"
                                            data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                                            <span><?= $category->Title?><i class="bi bi-plus"
                                                    style="float:right !important" aria-hidden="true"></i></span>
                                        </a>
                                        <div class="collapse" id="submenu-cate-product<?=$category->CategoryID?>"
                                            aria-expanded="false" style="height: 0px;" style="text-align:left">
                                            <?php $sub_cat = subCategories($category->CategoryID, $lang);
                                        foreach ($sub_cat as $k => $v) {?>
                                            <a
                                                href="<?php echo base_url('product');?>?q=<?php echo strtolower(str_replace(' ','-',$v->Title)); ?><?php echo '-s'.$v->CategoryID;?>">
                                                <div class="submenu">
                                                    <span class="left-submenu"><?= $v->Title?></span>
                                                    <span
                                                        class="right-submenu"><?= count_product($v->CategoryID,'subcat');?></span>
                                                </div>
                                            </a>
                                            <?php }?>


                                        </div>
                                    </div>
                                    <?php 
                                    }
                                }
                            ?>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php if (!isset($search)) { ?>
                    <!-- <div class="col-lg-12 col-xs-6 to-hide-600 desktop-filter" id="sidebar">
                        
                        <?php if ($offers || $offers_for_you) { ?>
                        <div class="list-group">
                            <a href="#menu-cate6" onclick="myFunction_2(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('offers'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                            </a>

                            <div class="collapse" id="menu-cate6">
                                <?php if ($offers) { 

                                                        foreach ($offers as $key => $value) {
                                                    ?>
                                <label class="customcheck">
                                    <input class="offers get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php  
                                                }
                                                    }


                                                    ?>

                                <?php if ($offers_for_you) {
                                                            foreach ($offers_for_you as $key => $value) {
                                                    ?>
                                <label class="customcheck">
                                    <input class="offers get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php } } ?>

                            </div>
                        </div>
                        <?php } ?>

                        <div class="list-group">
                            <a href="#menu-cate2" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('price'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate2">
                                <input type="hidden" id="amount">
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="1">1$ - 50$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="2">51$ - 100$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="3">101$ - 150$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="4">151$ - 200$
                                    <span class="checkmark"></span>
                                </label>
                                <div class="priceRange">
                                    <span><input type="number" style="width:65px" id="minPrice" value="0" min="0" max="120000"
                                            readonly /></span>
                                    <span>to</span>
                                    <span><input type="number" id="maxPrice" style="width:65px" value="0" min="0" max="120000"
                                            readonly /></span>
                                </div> 
                            </div>
                        </div>
                        <div class="list-group">
                            <a href="#menu-cate4" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('rating'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate4">

                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="1"><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="2"><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="3"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="4"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="5"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="list-group">
                            <a href="#menu-cate5" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('tags'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate5">
                                <div class="priceRangeEd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="search_tags"
                                            placeholder="<?php echo lang('search_tags'); ?>" style="margin: 0;">
                                    </div>
                                </div>
                                <?php if($tags){
                                        foreach ($tags as $key => $value) { ?>

                                <label class="customcheck search-labels" id="search-label-<?php echo $value->TagID;?>"
                                    data-text="<?php echo $value->Title; ?>">
                                    <input class="get_products tags" type="checkbox" data-url-title="" name="TagID[]"
                                        value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                    <?php /*$p_count =   getCustomRow("SELECT count(*) as count FROM products where IsCustomizedProduct = 0 AND products.IsActive = 1 AND TagIDs like '%".$value->TagID."%'")['count'];*/ ?>
                                    <span style="float:right;"><?= @$tags_count[$value->TagID]; ?></span>
                                </label>

                                <?php
                                                        }
                                            }

                                            ?>


                            </div>
                        </div>
                    </div> -->

                <?php } ?>
                    
                    
                    
                </div>
                <!-- Left Filter Desktop -->


            </div>

            <!-- tab filter -->
            <div class="col-lg-2 col-xs-12 to-hide-600 mobile-filter mt-4 to-hide-600" id="sidebar">
                <div class="row">
                    <!-- <div class="col-xs-6">
                        <div class="list-group onlyChkBox">
                            <label class="customcheck">
                                <input class="get_products featured2" type="checkbox" data-url-title=""
                                    name="chkFeature" value="">
                                <span class="checkmark"></span><?php echo lang('featured'); ?>
                            </label>
                        </div>
                    </div> -->
                    <div class="col-xs-6">
                        <?php if ($offers || $offers_for_you) { ?>
                        <div class="list-group">
                            <a href="#menu-cate6-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('offers'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>

                            <div class="collapse" id="menu-cate6-mobile">
                                <?php if ($offers) { 

                                                foreach ($offers as $key => $value) {
                                            ?>
                                <label class="customcheck">
                                    <input class="offers2 get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php  
                                        }
                                            }


                                            ?>

                                <?php if ($offers_for_you) {
                                                    foreach ($offers_for_you as $key => $value) {
                                            ?>
                                <label class="customcheck">
                                    <input class="offers get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php } } ?>

                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-6">
                        <div class="list-group">
                            <a href="#menu-cate2-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('price'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate2-mobile">
                                <input type="hidden" id="amount">
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="1">1$ - 50$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="2">51$ - 100$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="3">101$ - 150$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="4">151$ - 200$
                                    <span class="checkmark"></span>
                                </label>
                                <div class="priceRange">
                                    <span><input type="number" style="width:65px" id="minPrice" value="0" min="0"
                                            max="120000" readonly /></span>
                                    <span>to</span>
                                    <span><input type="number" id="maxPrice" style="width:65px" value="0" min="0"
                                            max="120000" readonly /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="list-group">
                            <a href="#menu-cate4-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('rating'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate4-mobile">

                                <label class="customcheck">
                                    <input class="rating2 get_products" type="checkbox" data-url-title=""
                                        name="Rating[]" value="1"><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="2"><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="3"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="4"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="5"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="list-group">
                            <a href="#menu-cate5-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('tags'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate5-mobile">
                                <div class="priceRangeEd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="search_tags"
                                            placeholder="<?php echo lang('search_tags'); ?>" style="margin: 0;">
                                    </div>
                                </div>
                                <?php if($tags){
                                                foreach ($tags as $key => $value) { ?>

                                <label class="customcheck search-labels" id="search-label-<?php echo $value->TagID;?>"
                                    data-text="<?php echo $value->Title; ?>">
                                    <input class="get_products tags2" type="checkbox" data-url-title="" name="TagID[]"
                                        value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                    <?php /*$p_count =   getCustomRow("SELECT count(*) as count FROM products where IsCustomizedProduct = 0 AND products.IsActive = 1 AND TagIDs like '%".$value->TagID."%'")['count'];*/ ?>
                                    <span style="float:right;"><?= @$tags_count[$value->TagID]; ?></span>
                                </label>

                                <?php
                                                }
                                    }

                                    ?>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end tab filter -->

            <div class="col-lg-9 col-sm-12">
            <form action="<?php echo base_url('cart/addCustomizedShapeToCart'); ?>" method="post" id="addCustomizedShapeToCart" enctype="multipart/form-data" onsubmit="return false;">
                <?php
                foreach ($shapes as $item) {
                    $shape_images = get_images($item->ShapeID, 'shape');
                    if(isset($shape_images[0]->ImageName))
                    {
                        $image = $shape_images[0]->ImageName;    
                    }else
                    {
                        $image = $item->ShapeImage;    
                    }
                    
                    
                ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wrapper p-2 inbox" style="height: auto;">
                        <!-- customize shape  -->
                        
                        
                            <!-- customize shape content -->
                            <div class="add_to_Cart_dv">
                            <!-- <a href="javascript:void(0);" title="' . lang('click_to_add_to_cart') . '" onclick="addWishlistToCart(' . $p->ProductID . ', ' . $type_of_item . ', ' . $p->Price . ', ' . $IsCorporateItem . ',`'.$DiscountType.'`,`'.$DiscountFactor.'`,`'.@$default_package.'`,`'.@$p->PriceType.'`);">
                                <i class="bi bi-cart3"></i>
                            </a> -->
                            <!-- <a class="" title="' . lang('click_to_add_to_wishlist') . '" href="javascript:void(0);"
                                   onclick="addToWishlist(' . $p->ProductID . ', ' . $type_of_item . ');"><i
                                    class="listing_heart bi bi-heart ' . isLiked($p->ProductID, 'product') . '" id="item' . $p->ProductID . '" aria-hidden="true"></i>
                            </a> -->
                            
                            </div>
                            
                            <a href="<?php echo base_url('customize/choco_shape_details/'.base64_encode(@$item->ShapeID)); ?>">
                            <!-- <a href="<?php echo base_url('customize/choco_shape_engraved/'.base64_encode(@$item->ShapeID)); ?>"> -->
                            <div class="d-flex">
                                <img class="mx-auto d-block shape_img choco_under_img_size" src="<?php echo base_url($image); ?>" alt="<?= @$item->Title ?>">
                                
                            </div>
                            <div class="">
                                <div class="left">
                                    <div class="details w-100">
                                        <?php
                                            $Price = @$item->PricePerKG;
                                        ?>
                                        <h4 class="mb-0" style="font-size:30px;"><strong><?= get_taxt_amount($Price)+$Price ?><span style="font-size:10px; padding-left:7px;">SAR</span></strong></h4>
                                        <p class="text-left custom-shape-small-text" style="margin-bottom:0px;">Price Per KG</p>
                                    <h4 class="product_title"><?= @$item->Title ?> <?= @$item->IndividualSize ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                            
                            </div>
                        </a>
                    </div>
                <?php } ?>
                     
            </form>
            <!-- </div> -->
        </div>
        <div class="row">
            <div class="col-md-12 pt-4 pb-4">
                <p class="continue-txt continueBTN" disabled onclick="continueToNext();">SELECT A SHAPE TO CUSTOMISE <i class="fa fa-caret-right" style="color:#A07250;" aria-hidden="true"></i></p>
                <!-- <button class="btn btn-secondary continueBTN mb-4" disabled onclick="continueToNext();">Continue</button> -->
            </div>

        </div>

        

        <div class="dv-hide-desktop">
            <div class="filter">
                <!-- <div class="row">
                    <div class="col-md-6 col-sm-5 custom-filter">
                        <h6>
                            <i class="fa fa-filter" aria-hidden="true"></i>
                            <?php echo lang('filter'); ?>
                            <span>
                                <b><?php echo lang('count'); ?></b>
                                <i aria-hidden="true" id="now"><?php echo count($products); ?> </i>/
                                <i aria-hidden="true" id="total"> <?php echo $countproducts; ?></i>
                            </span>
                        </h6>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?php
                        $price_highest_to_lowest = "";
                        $price_lowest_to_higher = "";
                        $product_older_to_newer = "";
                        $product_newer_to_older = "";
                        $purchased_highest_to_lowest = "";
                        $purchased_lowest_to_higher = "";
                        if (isset($_GET['sort']) && $_GET['sort'] != '') {
                            if ($_GET['sort'] == 'price_highest_to_lowest') {
                                $price_highest_to_lowest = 'selected';
                            }

                            if ($_GET['sort'] == 'price_lowest_to_higher') {
                                $price_lowest_to_higher = 'selected';
                            }

                            if ($_GET['sort'] == 'product_older_to_newer') {
                                $product_older_to_newer = 'selected';
                            }

                            if ($_GET['sort'] == 'product_newer_to_older') {
                                $product_newer_to_older = 'selected';
                            }

                            if ($_GET['sort'] == 'purchased_highest_to_lowest') {
                                $purchased_highest_to_lowest = 'selected';
                            }

                            if ($_GET['sort'] == 'purchased_lowest_to_higher') {
                                $purchased_lowest_to_higher = 'selected';
                            }
                        }
                        ?>
                        <select class="form-control" onchange="sortProducts(this.value);">
                            <option selected disabled><?php echo lang('Sort_by'); ?></option>
                            <option value="price_highest_to_lowest" <?php echo $price_highest_to_lowest ?>>
                                <?php echo lang('price'); ?>:
                                <?php echo lang('Highest_to_lowest'); ?>
                            </option>
                            <option value="price_lowest_to_higher" <?php echo $price_lowest_to_higher ?>>
                                <?php echo lang('price'); ?>:
                                <?php echo lang('Lowest_to_Highest'); ?>

                            </option>
                            <option value="product_older_to_newer" <?php echo $product_older_to_newer ?>><?php echo lang('products'); ?>:
                                <?php echo lang('Older_to_Newer'); ?>
                            </option>
                            <option value="product_newer_to_older" <?php echo $product_newer_to_older ?>><?php echo lang('products'); ?>:
                                <?php echo lang('Newer_to_Older'); ?>
                            </option>
                            <option value="purchased_highest_to_lowest" <?php echo $purchased_highest_to_lowest ?>>
                                <?php echo lang('Purchased'); ?>:
                                <?php echo lang('Highest_to_lowest'); ?>
                            </option>
                            <option value="purchased_lowest_to_higher" <?php echo $purchased_lowest_to_higher ?>>
                                <?php echo lang('Purchased'); ?>:
                                <?php echo lang('Lowest_to_Highest'); ?>
                            </option>
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <select class="form-control ProductsPerPage">
                            <option selected disabled><?php echo lang('product_per_page'); ?></option>
                            <option>9</option>
                            <option>15</option>
                            <option>20</option>
                        </select>
                    </div>
                    <div class="col-md-1 iconsrow text-right col-sm-2 to-hide-600">
                        <a onclick="changeGridLayout('items_list');" class="right-filter-icon"><i class="fa fa-list"></i></a>
                        <a onclick="changeGridLayout('items_grid');" class="left-filter-icon"><i class="fa fa-th"></i></a>
                    </div>
                </div> -->
            </div>
            <div class="col-md-2" id="sidebar1">
                <div class="list-group">
                    <a href="#menu-cate-product-mob" onclick="myFunction(this)" class="list-group-item collapsed list-item-product plusToggle" data-toggle="collapse"
                        data-parent="#sidebar" aria-expanded="false">
                        <span><?= lang('categories')?><i  class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate-product-mob" aria-expanded="false" style="height: 0px;">
                    <div class="collapse-span">
                            <?php $menu_categories = subCategories(0,$language);
                                if($menu_categories){ 
                                    foreach ($menu_categories as $key => $category) {?>
                                    <div class="list-group">
                                        <a href="#submenu-cate-product-mob<?=$category->CategoryID?>" onclick="myFunction(this)" class="list-group-item collapsed subList-item-product" data-toggle="collapse"
                                            data-parent="#sidebar" aria-expanded="false">
                                            <span><?= $category->Title?><i  class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                                        </a>
                                        <div class="collapse" id="submenu-cate-product-mob<?=$category->CategoryID?>" aria-expanded="false" style="height: 0px;" style="text-align:left">
                                        <?php $sub_cat = subCategories($category->CategoryID, $lang);
                                        foreach ($sub_cat as $k => $v) {?>
                                         <a href="<?php echo base_url('product');?>?q=<?php echo strtolower(str_replace(' ','-',$v->Title)); ?><?php echo '-s'.$v->CategoryID;?>">
                                            <div class="submenu">
                                                <span class="left-submenu"><?= $v->Title?></span>
                                                <span class="right-submenu"><?= count_product($v->CategoryID,'subcat');?></span> 
                                            </div> 
                                        </a>
                                        <?php }?>
                                        
                                        
                                        </div>
                                    </div>
                            <?php 
                                    }
                                }
                            ?>       
                    </div>
                        
                    </div>
                </div>
                <div class="list-group list-group-brand">
                    <a href="#menu-cate-brands-mob" onclick="myFunction(this)" class="list-group-item collapsed list-item-brand" data-toggle="collapse"
                        data-parent="#sidebar" aria-expanded="false">
                        <span><?= lang('chocomood')?><i  class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                    </a>
                    
                    <div class="collapse" id="menu-cate-brands-mob" aria-expanded="false" style="height: 0px;">
                    <div class="collapse-span">
                        
                        <a href="<?= base_url() ?>product/brand_detail?q=chocomood-s0">
                            <div class="choco-name">
                                All Products
                                <span class="right-submenu"><?= count_product(0,'brand');?></span> 
                            </div>
                        </a>
                    </div>              
                    </div>
                </div>
            </div>
            <?php if (!isset($search)) { ?>
            <div class="col-md-2" id="sidebar">
 
                <!-- <div class="list-group onlyChkBox">
                    <label class="customcheck">
                        <input class="get_products featured" type="checkbox" data-url-title="" name="chkFeature"
                            value="">
                        <span class="checkmark"></span><?php echo lang('featured'); ?>
                    </label>
                </div> -->
                <!-- <?php if ($offers || $offers_for_you) { ?> -->
                <!-- <div class="list-group">
                    <a href="#menu-cate6-mob" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('offers'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>

                    <div class="collapse" id="menu-cate6-mob">
                        <?php if ($offers) { 

                                            foreach ($offers as $key => $value) {
                                        ?>
                        <label class="customcheck">
                            <input class="offers get_products" type="checkbox" name="Offer[]"
                                value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                            <span class="checkmark"></span>
                        </label>

                        <?php  
                                     }
                                        }


                                        ?>

                        <?php if ($offers_for_you) {
                                                 foreach ($offers_for_you as $key => $value) {
                                          ?>
                        <label class="customcheck">
                            <input class="offers get_products" type="checkbox" name="Offer[]"
                                value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                            <span class="checkmark"></span>
                        </label>

                        <?php } } ?>

                    </div>
                </div> -->
                <!-- <?php } ?> -->

                <!-- <div class="list-group">


                    <a href="#menu-cate2-mob" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('price'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate2-mob">
                        <div class="priceRangeEd">
                            <div class="form-group">
                              >
                                <div id="slider-range"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" id="amount" >
                        <label class="customradio">
                                        <input class="filter_price radio " type="radio" 
                                                name="Price"
                                                value="1">1$ - 50$
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio">
                                        <input class="filter_price radio " type="radio" 
                                                name="Price"
                                                value="2">51$ - 100$
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio">
                                        <input class="filter_price radio " type="radio" name="Price"
                                                value="3">101$ - 150$
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio">
                                        <input class="filter_price radio " type="radio" name="Price"
                                                value="4">151$ - 200$
                                        <span class="checkmark"></span>
                                    </label>
                                    <div class="priceRange">
                                        <span ><input type="number" style="width:65px" id="minPrice" value="0" min="0" max="120000" readonly/></span>
                                        <span>to</span>
                                        <span><input type="number" style="width:65px" id="maxPrice" value="0" min="0" max="120000" readonly/></span> 
                                    </div>    
                    </div>
                </div> -->
                <!-- <div class="list-group">
                    <a href="#menu-cate4-mob" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('rating'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate4-mob">

                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="1"><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="2"><i class="filled_star"></i><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="3"><i class="filled_star"></i><i class="filled_star"></i><i
                                class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="4"><i class="filled_star"></i><i class="filled_star"></i><i
                                class="filled_star"></i><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="5"><i class="filled_star"></i><i class="filled_star"></i><i
                                class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div> -->
                <!-- <div class="list-group">
                    <a href="#menu-cate5-mob" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('tags'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate5-mob">
                        <div class="priceRangeEd">
                            <div class="form-group">
                                <input type="text" class="form-control" id="search_tags"
                                    placeholder="<?php echo lang('search_tags'); ?>" style="margin: 0;">
                            </div>
                        </div>
                        <?php if($tags){
                                                foreach ($tags as $key => $value) { ?>

                        <label class="customcheck search-labels" id="search-label-<?php echo $value->TagID;?>"
                            data-text="<?php echo $value->Title; ?>">
                            <input class="get_products tags" type="checkbox" data-url-title="" name="TagID[]"
                                value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?>
                            <span class="checkmark"></span>
                            <?php /*$p_count =   getCustomRow("SELECT count(*) as count FROM products where IsCustomizedProduct = 0 AND products.IsActive = 1 AND TagIDs like '%".$value->TagID."%'")['count'];*/ ?>
                            <span style="float:right;"><?= @$tags_count[$value->TagID]; ?></span>
                        </label>

                        <?php
                                                }
                                      }

                                      ?>


                    </div>
                </div> -->
            </div>
            <?php } ?>
        </div>
        <img src="<?= base_url('assets/frontend/images/filter-img.png')?>" class="only-mob">



    </div>
</section>

<script>
    $('.ProductP').html($('.product-info').data('box_min_price'));
    $('.ProductPrice').val($('.product-info').data('box_min_price'));

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function(oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
            $('#uploadPreview').show();
        };
    };

    $(document).ready(function() {
        $('input[type="file"]').change(function(e) {
            // var fileName = e.target.files[0].name;
            var fileType = e.target.files[0].type;
            if (fileType == 'image/png' || fileType == 'image/jpeg') {
                PreviewImage();
                $('.addToCartBTN').attr('disabled', false);
            } else {
                showMessage('<?php echo lang('only_shape_formats'); ?>', 'danger');
                $('.addToCartBTN').attr('disabled', true);
                $('#uploadPreview').hide();
            }
        });
    });



    if ($(window).width() < 601) {
    $("img.only-mob").click(function() {
        $(".dv-hide-desktop").slideToggle("fast");
    });
    } else {
        $(".dv-hide-desktop").hide();
    }

    function myFunction(x) {
        x.childNodes[1].childNodes[1].classList.toggle('bi-plus');
    x.childNodes[1].childNodes[1].classList.toggle('bi-dash');
    }
    
    function sortProducts(sort_val) {
    var current_url = $(location).attr('href');
    url = new URL(window.location.href);
    if (url.searchParams.get('q')) {
        // append sort value at the end
        if (url.searchParams.get('sort')) {
            current_url = removeParam('sort', current_url);
            current_url += "&sort=" + sort_val;
        } else {
            current_url += "&sort=" + sort_val;
        }
    } else {
        // append sort value at the start
        if (url.searchParams.get('sort')) {
            current_url = removeParam('sort', current_url);
            current_url += "sort=" + sort_val;
        } else {
            current_url += "?sort=" + sort_val;
        }
    }
    window.location.href = current_url;
}
function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}
</>