<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
		integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<title>Invoice</title>
</head>

<body>
<?php 
?>
	<div class="container-fluid ">
		<div class="row mt-4 mx-3">
			<div class="col-md-6 bill-info">
				<div class="row">
					<div class="col-md-5 logo">
						<img src="<?php echo base_url('assets/frontend/images/logoin.png'); ?>">
					</div>
					<div class="col-md-7 address">
						<h5>
							BILL FROM:
						</h5>
						<h5>
							CHOCOMOOD
						</h5>
						<p><?php echo $order->StoreTitle; ?><br>

							<?php echo $order->StoreCityTitle; ?><br>
							Kingdom of Saudi Arabia</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 shopping-info">
				<div class="row receipt-no">
					<div class="col-md-6">
						<h4 class="reg-no">VAT REG No.</h4>
						<h3><?php echo $order->VatNo; ?></h3>
					</div>
					<div class="col-md-6">
						<!--<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/barCode.jpg" class="img-responsive">-->
						<p style="text-align:right;">
							<img src="<?php echo base_url(); ?><?= $barcode; ?>">
							<a href="javascript:void(0);" onclick="window.print();">
								<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/print.jpg" height="28"
									width="28">
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row  mt-2 mx-3">
			<div class="col-md-6 bill-info">
				<div class="row">
					<div class="col-md-12">
						<div class="edHeading">
							<h3>BILL TO:</h3>
						</div>
						<div class="row">
							<div class="col-md-5 client-info">
								<strong class="haveIcon"><img
										src="<?php echo base_url(); ?>assets/frontend/images/invoice/userIcon.jpg">
									<?php echo $order->FullName; ?></strong>
								<p><?php echo $order->MobileNo; ?></p>
							</div>
							<div class="col-md-7 client-info">

								<strong class="haveIcon"><img
										src="<?php echo base_url(); ?>assets/frontend/images/invoice/MapIcon.jpg">
									Address</strong>
									<p>
									<?php 
								if ($order->CollectFromStore == 0){ ?>
								<?php echo $order->Street; ?><br>
									<?php echo $order->BuildingNo; ?><br>
									<?php echo $order->AddressCity; ?><br>
									<?php } ?>
									Kingdom of Saudi Arabia</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--2nd Column-->
			<div class="col-md-6 shopping-info">
				<div class="edHeading">
					<div class="row">
						<div class="col-md-6">
							<h3>ORDER DETAILS:</h3>
						</div>
						<div class="col-md-6">
							<h3><img src="<?php echo base_url(); ?>assets/frontend/images/invoice/userIcon.jpg">CUSTOMER
								ID: <?php echo $order->UserID; ?></h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 order-info">
						<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/invoiceNo.jpg" height="28"
							width="28" class="icon">
						<strong>Invoice No. :</strong>
						<p><?php echo $order->OrderNumber; ?></p>
					</div>
					<div class="col-md-6 order-info">
						<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/calender.jpg" height="28"
							width="28" class="icon">
						<strong>Date of issue:</strong>
						<p><?php echo date('d/m/Y', strtotime($order->OrderCreateDate)); ?></p>
					</div>
					<div class="col-md-6 order-info">
						<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/timeIcon.jpg" height="28"
							width="28" class="icon">
						<strong>Time of issue:</strong>
						<p><?php echo date('H:i a', strtotime($order->OrderCreateDate)); ?></p>
					</div>
					<?php 
					if ($order->CollectFromStore != 1): ?>
						<div class="col-md-6 order-info">
							<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/dropBox.jpg" height="28"
								width="28" class="icon">
							<?php 
							if($order->AWBNumber != ''){ ?>
								<strong>Tracking ID:</strong>
								<p><?= @$order->AWBNumber; ?></p>
								<?php 
							}else{ ?>
								<strong>Estimated Delivery:</strong>
								<?php
									$site_setting 	 = site_settings();
									$days_to_deliver = "+$site_setting->DaysToDeliver days";
								?>
								<p>
									<?php echo date('d/m/Y', strtotime($days_to_deliver, strtotime($order->OrderCreateDate))); ?>
								</p>
								<?php
							} ?>
						</div>
						<?php 
					endif; ?>
					<div class="col-md-6 order-info">
						<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/cardIcon.jpg" height="28"
							width="28" class="icon">
						<strong>Payment Menthod.:</strong>
						<p>
							<?php
								if ($order->CollectFromStore == 1){ 
									echo "Payment on Store"; 
								}else{
									echo $order->PaymentMethod;	
								}
							?>
						</p>
					</div>
					<?php 
					if ($order->CollectFromStore == 1): ?>
						<div class="col-md-6 order-info">
							<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/pStore.jpg" height="28"
								width="28" class="icon">
							<strong>Pick Up Store:</strong>
							<p><?php echo $order->StoreTitle; ?> / <?php echo $order->StoreAddress; ?></p>
						</div>
						<?php 
					endif; 
					if ($order->CouponCodeUsed != ''): ?>
						<div class="col-md-6 order-info">
							<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/codeUSed.jpg" height="28"
								width="28" class="icon">
							<strong>Promo Code Used:</strong>
							<p><?php echo $order->CouponCodeUsed; ?></p>
						</div>
						<?php 
					endif; ?>
					<?php 
					if ($order->CollectFromStore == 0): ?>

						<div class="col-md-6 order-info">
							<?php 
							if ($order->ShipmentMethodID == 4): ?>
								<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/typeShipment.jpg" height="28" width="28" class="icon">
								<?php
							else: ?>
								<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/shipping.jpg" height="28" width="28" class="icon">
								<?php
							endif; ?>
							<strong>Type of Shipment:</strong>
							<p>
								<?php
								if($order->SemsaShippingAmount && $order->SemsaShippingAmount > 0):
									echo 'SMSA';
								else:
									if(isset($order->CollectFromStore) && $order->CollectFromStore > 0):
										echo "Collect From Store";
									else:
										echo $order->ShipmentMethodTitle;
									endif;
								endif;?>
							</p>
						</div>
						<?php 
					endif; ?>
					<div class="col-md-6 order-info">
						<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/trackOrder.jpg" height="28"
							width="28" class="icon">
						<strong>Tracking Order No.</strong>
						<p><?php echo $order->OrderNumber; ?></p>
					</div>
					<div class="col-md-6 order-info">
						<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/status.jpg" height="28"
							width="28" class="icon">
						<strong>Status:</strong>
						<p><?php echo $order->OrderStatusEn; ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-4 mx-3 pb-4">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="edHeading">
							<h3>RECIPIENT INFORMATION:</h3>
						</div>
						<div class="row">
							<div class="col-md-5 client-info">
								<strong class="haveIcon"><img
										src="<?php echo base_url(); ?>assets/frontend/images/invoice/userIcon.jpg">
									<?php echo $order->FullName; ?></strong>
								<p><?php echo $order->Mobile; ?></p>
							</div>
							<div class="col-md-7 client-info">
								<strong class="haveIcon"><img
										src="<?php echo base_url(); ?>assets/frontend/images/invoice/MapIcon.jpg">
									Address</strong>
								<p>
								<?php 
								if ($order->CollectFromStore == 0){ ?>
									<?php echo $order->Street; ?><br>
									<?php echo $order->BuildingNo; ?><br>
									<?php echo $order->AddressCity; ?><br>
								<?php } ?>
									Kingdom of Saudi Arabia</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-4 mx-3 mb-5">
			<div class="col-md-12">
				<div class="inv_Heading">
					<h3>Invoice:</h3>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">
													<H6>SN</H6>
												</th>
												<th class="text-center">
													<h6>SKU</h6>
												</th>
												<th class="text-center">
													<h6>Image</h6>
												</th>
												<th class="text-center">
													<h6>Title</h6>
												</th>
												<th class="text-center">
													<h6>Discription</h6>
												</th>
												<th class="text-center">
													<h6>Unit</h6>
												</th>
												<th class="text-center">
													<h6>QTY</h6>
												</th>
												<th class="text-center">
													<h6>Price</h6>
												</th>
												<th class="text-center">
													<h6>Sub Total</h6>
												</th>
												<th class="text-center">
													<h6>VAT</h6>
												</th>
												<th class="text-center">
													<h6>VAT Rate</h6>
												</th>
												<th class="text-center">
													<h6>TOTAL</h6>
												</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i 				= 1;
											$total_cost 	= 0;
											$order_items 	= getOrderItems($order->OrderID);
											$taxes 			= getTaxShipmentCharges('Tax');
											$tax_amount 	= @(isset($taxes[0]->Amount))? $taxes[0]->Amount : 0;
											$tax_factor 	= @(isset($taxes[0]->Type))? (($taxes[0]->Type == 'Fixed') ? '' : $tax_amount . '%') : '';
											$subtotal_total = 0;                                       	
											$tax_rate_total = 0;    
											$i 				= 0;                        	
											foreach ($order_items as $item):
												
												
												$IsOnOffer = false;
												$Price = $item->Amount;
												$DiscountType = $item->DiscountType;
												$DiscountFactor = $item->Discount;
												if ($DiscountType == 'percentage') {
													$IsOnOffer = true;
													$Discount = ($DiscountFactor / 100) * $Price;
													if ($Discount > $Price) {
														$ProductDiscountedPrice = 0;
													} else {
														$ProductDiscountedPrice = $Price - $Discount;
													}
												} elseif ($DiscountType == 'per item') {
													$IsOnOffer = true;
													$Discount = $DiscountFactor;
													if ($Discount > $Price) {
														$ProductDiscountedPrice = 0;
													} else {
														$ProductDiscountedPrice = $Price - $DiscountFactor;
													}
												} else {
													$Discount = 0;
													if ($Discount > $Price) {
														$ProductDiscountedPrice = 0;
													} else {
														$ProductDiscountedPrice = $Price;
													}
												}
												
												$total_cost = $total_cost + (($ProductDiscountedPrice * $item->Quantity) + get_taxt_amount($ProductDiscountedPrice * $item->Quantity));
												?>
												<tr>
													<td class="text-center"><?php echo $i; ?></td>
													<td class="text-center">
														<?php echo $item->SKU; ?>
													</td>
													<td align="center">
														<?php
														if($item->ItemType == 'Product'): ?>
															<img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>" style="width: 70px;height: 70px;">
															<?php 
														elseif($item->ItemType == 'Choco Shape'): ?>
															<img src="<?php echo base_url($item->CustomizedShapeImage); ?>" style="width: 70px;height: 70px;">
															<?php 
														else: ?>
															<img src="<?php echo front_assets("images/".$item->ItemType.".png"); ?>" style="width: 70px;height: 70px;">
															<?php 
														endif; ?>
													</td>
													<td class="text-center">
														<?php
															echo $item->Title;
															echo @$itemDetailStrArr["id_".$item->OrderItemID]["title"];
														?>
													</td>
													<td class="text-center">
														<?php 
															echo $item->Description; 
															echo @$itemDetailStrArr["id_".$item->OrderItemID]["description"];
														?>
													</td>
													<td class="text-center">
														<?php echo (($item->PriceType == 'kg') ? ' kg' : 'pcs'); ?>
													</td>
													<td class="text-center">
														<?php
														if ($item->PriceType == 'kg') {
															$product_packages = get_product_packages($item->ProductID, $language);
															if($product_packages){
																foreach (@$product_packages as $k => $v) {
																	if ($item->package_id == $v['PackagesProductID']) {
																		echo $v['Title'].' x '. $item->Quantity;
																	}
																}
															}else{
																echo $item->Quantity;
															}
														}else{
															echo $item->Quantity;
														}
														?>

													</td>
													<td class="text-center">
														<?php 
														if($IsOnOffer)
																{
																	?>
																		<span style="text-decoration: line-through;"><?php echo number_format($item->Amount, 2); ?></span> <?= number_format($ProductDiscountedPrice, 2) ?> SAR
																	<?php
																	
																}
																else
																{
																	?>
																		<?php echo number_format($item->Amount, 2); ?> SAR
																	<?php
																	
																}   
																?>
													</td>
													<td>
													<?php 
																if($IsOnOffer)
																{
																	?>
																	<?= number_format($ProductDiscountedPrice*$item->Quantity, 2) ?> SAR
																	<?php
																	$subtotal_total = $subtotal_total + $ProductDiscountedPrice*$item->Quantity;
																}
																else
																{
																	?>
																		<?php echo number_format($item->Amount*$item->Quantity, 2); ?> SAR
																	<?php
																	$subtotal_total = $subtotal_total + $item->Amount*$item->Quantity;
																}   
															?>
													</td>
													
													<td class="text-center">
														<?php echo $tax_factor; ?>
													</td>
													<td class="text-center">
														<?php echo get_taxt_amount($ProductDiscountedPrice * $item->Quantity); ?>
														SAR
													</td>
													<td class="text-center">
														<?php
															$t_total 		=  get_taxt_amount($ProductDiscountedPrice * $item->Quantity) ;
															$tax_rate_total = $tax_rate_total+$t_total;
															$s_total 		= $ProductDiscountedPrice * $item->Quantity;
															echo number_format($t_total + $s_total, 2);
														?>
														SAR
													</td>
												</tr>
												<?php $i++;
											endforeach; ?>
											<tr>
												<td class="text-right" colspan="8"><strong>Total</strong></td>
												<td class="text-center clr">
													<?php echo number_format($subtotal_total, 2); ?> SAR</td>
												<td class="text-center clr"><?php echo $tax_factor; ?> </td>
												<td class="text-center clr">
													<?php echo number_format($tax_rate_total, 2); ?> SAR</td>
												<td class="text-center clr"><?php echo number_format($total_cost, 2); ?>
													SAR</td>
											</tr>
										</tbody>
										<tfoot>
											<?php 
											if ($order->CollectFromStore == 0): ?>
												<tr>
													<td class="text-left no-borders" colspan="4">
														<p>Transactions made based on gross total</p>
													</td>
													<td class="text-right no-borders"
														colspan="<?= ($order->TotalShippingCharges != 0 && !$order->SemsaShippingAmount && $order->SemsaShippingAmount < 1)? 4 : 7 ?>">
														<strong>Shipment Charges
															<?= ($order->TotalShippingCharges == 0) ? '' : '(including  VAT)'?>:
														</strong> 
														<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/shipping.jpg" class="td-icon">
													</td>
													<td class="text-center clr">
														<?php 
														if($order->TotalShippingCharges == 0){
															echo freeShippingTitle(); 
														}else{
															echo $order->TotalShippingCharges.' SAR'; 
														} ?>
													</td>
													<?php 
													if($order->TotalShippingCharges != 0 && !$order->SemsaShippingAmount && $order->SemsaShippingAmount < 1): ?>
														<td class="text-center clr">
															<?php echo $tax_amount; ?> %
														</td>
														<td class="text-center clr">
															<?php echo number_format(($order->TotalShippingCharges/100)*$tax_amount, 2); ?> SAR
														</td>
														<td class="text-center clr">
															<?php 
																echo number_format((($order->TotalShippingCharges/100)*$tax_amount)+$order->TotalShippingCharges, 2); 
																$order->TotalAmount = number_format($order->TotalAmount+ ($order->TotalShippingCharges/100)*$tax_amount, 2);
															?> 
															SAR
														</td>
														<?php 
													endif; ?>
												</tr>
												<?php
											endif; 
											
											if ($order->DiscountAvailed > 0): ?>
												<tr>
													<td class="text-right no-borders" colspan="11">
														<strong>Discount:</strong><img
															src="<?php echo base_url(); ?>assets/frontend/images/invoice/cardDiscount.jpg"
															class="td-icon"></td>
													<td class="text-center clr">
														<?php echo number_format($order->DiscountAvailed, 2); ?> SAR</td>
												</tr>
												<?php
											endif; ?>
											<tr>
												<td class="text-right no-borders" colspan="11"><strong>Grand
														Total:</strong> <img
														src="<?php echo base_url(); ?>assets/frontend/images/invoice/grandTotal.jpg"
														class="td-icon"></td>
												<td class="text-center clr"><?php echo $order->TotalAmount; ?> SAR</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row note">
			<div class="col-md-12">
				<h2 class="text-center">THANK YOU FOR YOUR BUSINESS!</h2>
				<?php 
				if ($order->CollectFromStore == 1): 
					$site_setting = site_settings();?>
					<p style="color: red">Your order will be Cancelled if not picked up in
						<?= @$site_setting->DaysToDeliver ?> Days from the <?= @$order->StoreTitle;?> store
					</p>
					<?php 
				endif;
				?>
			</div>
		</div>
	</div>
	<style type="text/css">
		.edHeading {
			border-bottom: 3px solid black;
			margin: 0 0 10px;
			/* padding: 0 10px; */
		}

		.edHeading h3 {
			margin: 0 0 5px;
			font-size: 20px;
			font-weight: bolder;
			line-height: 24px;
		}

		.inv_Heading h3 {
			margin: 0 0 25px;
			font-size: 45px;
			font-weight: bolder;
			line-height: 24px;
		}

		.edHeading h3 img {
			height: 24px;
			display: inline-block;
			vertical-align: top;
			margin-right: 3px;
		}

		.address p {
			font-size: 1rem;
		}

		.address h5 {
			font-size: 20px;
		}

		.bill-text {
			border-bottom: 3px solid black;
			width: 90%;

		}

		.recipient-info {
			border-bottom: 3px solid black;
			width: 90%;
		}

		.text {
			font-size: 20px;
		}

		.reg-no {
			margin-top: 20px;
		}

		.receipt-no {

			width: 90%;
			margin-bottom: 20px;
		}

		.details {
			border-bottom: 3px solid black;
		}

		.user-info {
			margin-bottom: 10px;
			border-bottom: 3px solid black;

		}

		.note {
			margin-top: 50px;
			margin-bottom: 30px;
			margin-left: 50px;
			margin-right: 50px;
			text-align: center;
		}

		.note h2 {
			margin-bottom: 25px;
		}

		td.no-borders {
			border: none;
		}

		td.clr {
			background-color: #E5F6AA;
		}

		.client-info p {
			margin-left: 28px;
			font-size: 1rem;
		}

		.client-info strong {
			font-size: 20px;
		}

		strong.haveIcon {
			display: block;
			width: 100%;
			font-size: 1rem;
			line-height: 1.5;
		}

		strong.haveIcon img {
			display: inline-block;
			vertical-align: top;
			width: 24px;
			height: auto;
		}

		.recipient-info p {
			margin-left: 40px;
			font-size: 1rem;
		}

		.recipient-info strong {
			font-size: 20px;
		}

		.order-info p {
			margin-left: 55px;
		}

		.icon {
			float: left;
			margin-right: 10px;
			margin-bottom: 15px;
			width: 48px;
			height: 48px;
			padding: 0px 12px 24px 12px;
			border-left: 1px solid #000;
			border-bottom: 1px solid #000;
		}

		.img-responsive {
			max-width: 100%;
		}

		.id-icon {
			padding: 0 5px 5px 5px;
			height: 40px;
		}

		.td-icon {
			padding: 0 5px 5px 5px;
			height: 40px;
		}
	</style>
</body>
</html>