<section class="content products titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Corporate Chocolates <span>Chocolates Per KG</span></h2>
            </div>
        </div>
    </div>
</section>

<section class="content products">
    <div class="container">
        <div class="row">
                <?php
                if ($corporate_products) {
                    foreach ($corporate_products as $corporate_product) {
                        ?>
                        <div class="col-md-3">
                            <div class="inbox">
                                <a href="<?php echo base_url() . 'product/detail/' . productTitle($corporate_product->ProductID); ?>">
                                    <div class="imgbox">
                                        <img src="<?php echo base_url(get_images($corporate_product->ProductID, 'product', false)); ?>">
                                    </div>
                                    <h4><?php echo $corporate_product->Title; ?></h4>
                                    <h5><strong><?php echo number_format($corporate_product->CorporatePrice, 2); ?></strong> SAR</h5>
                                    <?php
                                    if ($corporate_product->OutOfStock == 1) { ?>
                                        <small style="font-weight: bold;color: red;">(Out Of Stock)</small>
                                    <?php }
                                    ?>
                                </a>
                            </div>
                        </div>
                    <?php }
                }
                ?>
        </div>
    </div>
</section>

