<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .content.checkout .navarea li:nth-child(1) img.gold {display:none;}
    .content.checkout .navarea li:nth-child(1) img.gray {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gold {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gray {display:none;}
    .div-height {
        height: 50vh;
    }
    @media(max-width: 768px){
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 400px !important;
            color: #fb7176;
            font-size: 20px;
        }
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 270px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
    }
    @media(max-width: 425px){
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 110px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 230px !important;
            color: #fb7176;
            font-size: 20px;
        }
    }
    @media(max-width: 375px){
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 110px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 245px !important;
            color: #fb7176;
            font-size: 20px;
        }
    }
    @media(max-width: 320px){
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 110px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 215px !important;
            color: #fb7176;
            font-size: 20px;
        }
    }
</style>
<section class="content products checkout pt10">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="navarea">
                    <li class="active">
                        <a data-toggle="tab" href="#shopping">
                        <img class="gray" src="<?php echo front_assets() ?>images/shopping_basket_gray_small.png">
                        <img class="gold" src="<?php echo front_assets() ?>images/shopping_basket_gold_small.png">
                            <span><?php echo lang('shopping_basket'); ?></span>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#wishlist">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <span><?php echo lang('wishlist'); ?></span>
                        </a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div id="shopping" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="text-center">
                                        <img class="gray" src="<?php echo front_assets() ?>images/noProductInCart.png">
                                        <p class="blueNoteText"><span><?= lang('Looks_like_there_are_no_items_in_your_basket_yet')?></span></p>
                                    </div>
                                    <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div id="wishlist" class="tab-pane fade">
                        <div class="row">
                            <?php
                             if(empty($wishlist_items))
                            {
                            ?>
                               <div style="height:50vh;">
                                    <div class="col-md-12 text-center">
                                        <p><?= lang('no_item_in_wishlist')?></p>
                                        <img src="<?php echo front_assets("images/" . "no-orders.png"); ?>" alt="no-orders" width="200px" height="300px">
                                        <p class="no-orders-text">
                                        <br>
                                        <a href="<?php echo base_url('product'); ?>?q=elegant-s40" class="btn btn-primary"> <?= lang('start_browsing_our_products')?></a>
                                        </p> 
                                    </div>
                                <div>
                                
                            <?php 
                            }
                            foreach ($wishlist_items as $wishlist_item) {
                                $default_package = 0;
                                $product_packages = get_product_packages($wishlist_item->ItemID,'EN');
                                foreach(@$product_packages as $k => $v) {
                                    if(@$v['DefaultPackagesID'] == @$v['PackagesID'])
                                    {
                                        $default_package = @$v['PackagesProductID'];
                                    }
                                }
                                if ($wishlist_item->ItemType == 'kg') {
                                    $url = base_url() . 'product/detail/' . productTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'product', false));
                                } elseif ($wishlist_item->ItemType == 'Collection') {
                                    $url = base_url() . 'collection/detail/' . collectionTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'collection', false));
                                }
                                $offer_product = checkProductIsInAnyOffer($wishlist_item->ItemID);
                                $DiscountType = '';
                                $DiscountFactor = '';
                                if(!empty($offer_product)){
                                    
                                    $DiscountType = $offer_product['DiscountType'];
                                    $DiscountFactor = $offer_product['Discount'];
                                }
                                ?>
                                <div class="col-md-3 div-height" >
                                    <div class="inbox">
                                        <div class="imgbox">
                                            <img src="<?php echo @$image; ?>">
                                        </div>
                                        <a href="<?php echo @$url; ?>">
                                            <h4><?php echo $wishlist_item->Title; ?></h4>
                                            <h5><strong><?php echo $wishlist_item->Price; ?></strong> SAR</h5>
                                        </a>
                                        <a title="Click to add to your wishlist" href="javascript:void(0);"
                                           onclick="addToWishlist(<?php echo $wishlist_item->ItemID; ?>, '<?php echo $wishlist_item->ItemType; ?>');"><i
                                                    class="fa fa-heart <?php echo isLiked($wishlist_item->ItemID, $wishlist_item->ItemType); ?>"
                                                    id="item<?php echo $wishlist_item->ItemID; ?>"
                                                    aria-hidden="true"></i></a>
                                        <a href="javascript:void(0);" class="<?= ($wishlist_item->OutOfStock == 1)? 'disable_event' : ''; ?>" title="Click to add this to your cart"
                                        onclick="addWishlistToCart(<?php echo $wishlist_item->ItemID; ?>, '<?php echo ucfirst($wishlist_item->ItemType); ?>', '<?php echo $wishlist_item->Price; ?>','<?php echo $wishlist_item->IsCorporateProduct; ?>','<?php echo $DiscountType; ?>','<?php echo $DiscountFactor; ?>','<?= $default_package?>','<?= $wishlist_item->PriceType ?>');">
                                            <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

