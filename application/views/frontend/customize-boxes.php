<style>
.content.products .inbox .choco_msg_imgbox {
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    width: 380px;
    height: 260px;
    display: block;
    margin-left: auto;
    margin-right: auto;
}
.content.products .inbox h4 {
    font-size: 32px;
    font-weight: 600;
    color: #291F0F;
}
.content.products {
    margin-bottom: 0px;
}
</style>
<section class="content products titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12 choco-msg_border-bot">
                <h2 class="choco_msg-headng">
                     <?= lang('choco_message')?>
                </h2>
                <span class="choco_msg-step">
                    <b><?= lang('step_1')?></b>
                </span>
                <span class="choco_msg-desc">
                    <?= lang('select_favourite_box')?>
                </span>
            </div>
        </div>
    </div>
</section>

<section class="content products">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 choco-msg_border-bot">
            <?php
                if ($boxes) {
                    foreach ($boxes as $box) {
                        ?>
                <div class="col-md-4">
                    <div class="inbox">
                        <a class="selectBox" title="<?php echo lang('click_to_proceed_with_box'); ?>"
                                   href="javascript:void(0);" data-box_id="<?php echo $box->BoxID; ?>" onclick="continueToNext();">
                            <div class="choco_msg_imgbox">
                                <img src="<?= base_url($box->BoxImage)?>" alt="" >
                            </div>
                        </a>
                        <h4><?=$box->Title; ?></h4>
                        <h5>
                            <?= lang('box_price').' '.number_format($box->BoxPrice, 2).' '.lang('sar'); ?> 
                        </h5>
                        <h5><?= lang('box_capacity').' '.$box->BoxSpace.' '.lang('pieces'); ?>
                        </h5>
                        <a class="selectBox" title="<?php echo lang('click_to_proceed_with_box'); ?>"
                                   href="javascript:void(0);" data-box_id="<?php echo $box->BoxID; ?>" onclick="continueToNext_new_design();">
                           New Design
                        </a>
                    </div>
                </div>
                <?php }
                }
                ?>
                <!-- <div class="col-md-4">
                    <div class="inbox">
                        <a class="selectBox" title="" href="#" data-box_id="">
                            <div class="imgbox">
                                <img src="http://localhost/chocomood_merge_custom/uploads/images/choco_message_box_1.png" alt="" >
                            </div>
                        </a>
                        <h4>Choco Message Box 2</h4>
                        <h5>Box Price: 60.00 SR</h5>
                        <h5>Box Capacity: 18 SR Pieces</h5>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 pt-4 pb-4">
            <input type="hidden" id="BoxID" value="<?php echo $boxes[0]->BoxID; ?>">
                <input type="hidden" id="BoxType" value="<?php echo $type; ?>">
                <!-- <input type="hidden" id="RibbonColour" value="<?php echo $ribbons[0]->RibbonID; ?>"> -->
                <p class="continue-txt continueBTN" disabled ><?= lang('select_box')?> <i class="fa fa-caret-right" style="color:#A07250;" aria-hidden="true"></i></p>
                <!-- <button class="btn btn-secondary continueBTN mb-4" disabled onclick="continueToNext();">Continue</button> -->
            </div>
            
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $('.continueBTN').attr('disabled', false);
    });

    $(document).on('click', '.selectBox', function () {
        showCustomLoader();
        var box_id = $(this).data('box_id');
        $('#BoxID').val(box_id);
        var box_image = $(this).children('.imgbox').children('img').attr('src');
        setTimeout(function () {
            $('.box_detail').children('img').attr('src', box_image);
            hideCustomLoader()
        }, 500);
    });
    $(document).on('click', '.selectRibbon', function () {
        showCustomLoader();
        var ribbon_color = $(this).data('ribbon_color');
        $('#RibbonColour').val(ribbon_color);
        var ribbon = $(this).children('img').attr('src');
        setTimeout(function () {
            $('.ribbon_detail').children('img').attr('src', ribbon);
            hideCustomLoader()
        }, 500);
    });

    function continueToNext() {
        var box_id = $('#BoxID').val();
        // var ribbon_color = $('#RibbonColour').val();
        var box_type = $('#BoxType').val();
        if (box_id !== '' ) {
            showCustomLoader();
            box_id = btoa(box_id);
            // ribbon_color = btoa(ribbon_color);
            window.location.href = base_url + "customize/" + box_type + "/" + box_id ;
        } else {
            showMessage('something missing', 'danger');
        }
    }

    function continueToNext_new_design() {
        var box_id = $('#BoxID').val();
        // var ribbon_color = $('#RibbonColour').val();
        var box_type = $('#BoxType').val();
        if (box_id !== '' ) {
            showCustomLoader();
            box_id = btoa(box_id);
            // ribbon_color = btoa(ribbon_color);
            window.location.href = base_url + "customize/choco_message_new_design/" + box_id ;
        } else {
            showMessage('something missing', 'danger');
        }
    }
</script>

