<?php
if ($this->session->userdata('user')) {
    $IsProductPurchased = IsProductPurchased($this->session->userdata['user']->UserID, $product->ProductID);
    if ($IsProductPurchased) {
        $UserCanReviewRate = true;
    } else {
        $UserCanReviewRate = false;
    }
} else {
    $UserCanReviewRate = false;
}

if ($product->IsCorporateProduct == 1 && $product->CorporateMinQuantity > 0) {
    $MinQuantity = $product->CorporateMinQuantity;
    $Price = $product->CorporatePrice;
    $MinQClass = "bordered";
    $PriceClass = "";
    $PriceType = lang('price_type_kg');
    $IsCorporateItem = 1;
} else {
    $MinQuantity = 1;
    $MinQClass = "";
    $PriceClass = "bordered";
    $Price = $product->Price;
    if ($product->PriceType == 'kg') {
        $PriceType = lang('price_type_kg');
    } else {
        $PriceType = lang('price_type_item');
    }
    $IsCorporateItem = 0;
}
?>

<?php
// offer logic here
$IsOnOffer = false;
$DiscountDescription = '';
if ($product->IsCorporateProduct == 0 && $offer_id != "") {
    $CurrentDate = date('Y-m-d');
    $offer_id = base64_decode($offer_id);
    $offer = array($offer_id);
    $offer_products = $this->Offer_model->getOfferProducts($offer);
    if ($offer_products) {
        foreach ($offer_products as $offer_product) {
            $OfferID = $offer_product->OfferID;
            $ProductIDsOffer = explode(',', $offer_product->ProductID);
            if (in_array($product->ProductID, $ProductIDsOffer)) {
                $OfferValidFrom = $offer_product->ValidFrom;
                $OfferValidTo = $offer_product->ValidTo;
                if ($CurrentDate >= $OfferValidFrom && $CurrentDate <= $OfferValidTo) {
                    $IsOnOffer = true;
                    $DiscountDescription = $offer_product->Title . ": " . $offer_product->Description;
                    $DiscountType = $offer_product->DiscountType;
                    $DiscountFactor = $offer_product->Discount;
                    if ($DiscountType == 'percentage') {
                        $Discount = ($DiscountFactor / 100) * $Price;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price - $Discount;
                        }
                    } elseif ($DiscountType == 'per item') {
                        $Discount = $DiscountFactor;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price - $DiscountFactor;
                        }
                    } else {
                        $Discount = 0;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price;
                        }
                    }
                }
                break;
            }
        }

    }
}
?>

    <style>
        #PriceT {
            padding: 0 !important;
        }
    </style>

    <section class="content titlarea">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php echo $product->Title; ?></h2>
                    <ul>
                        <?php
                        if ($product->IsCorporateProduct == 1) { ?>
                            <li><a href="<?php echo base_url('corporate'); ?>"><?php echo lang('corporate'); ?></a></li>
                            <li><a href="<?php echo base_url('corporate/products'); ?>"><?php echo lang('products'); ?></a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                            <li><a href="<?php echo base_url('product'); ?>"><?php echo lang('products'); ?></a></li>
                            <li>
                                <a href="javascript:void(0);"><?php echo categoryName($product->CategoryID, $language); ?></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><?php echo categoryName($product->SubCategoryID, $language); ?></a>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="content single-products">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="imggallery">
                        <div id="slideshow">
                            <!-- Below are the images in the gallery -->
                            <?php
                            $i = 1;
                            foreach ($product_images as $product_image) { ?>
                                <div id="img-<?php echo $i; ?>" data-img-id="<?php echo $i; ?>"
                                     class="img-wrapper <?php echo($i == 1 ? 'active' : ''); ?>"
                                     style="background-image: url('<?php echo base_url($product_image->ImageName); ?>')"></div>
                                <?php $i++;
                            }
                            ?>

                            <!-- Below are the thumbnails of above images -->
                            <div class="thumbs-container bottom">
                                <div id="prev-btn" class="prev">
                                    <i class="fa fa-angle-left fa-3x"></i>
                                </div>

                                <ul class="thumbs">
                                    <?php
                                    $i = 1;
                                    foreach ($product_images as $product_image) { ?>
                                        <li data-thumb-id="<?php echo $i; ?>"
                                            class="thumb <?php echo($i == 1 ? 'active' : ''); ?>"
                                            style="background-image: url('<?php echo base_url($product_image->ImageName); ?>')"></li>
                                        <?php $i++;
                                    }
                                    ?>
                                </ul>

                                <div id="next-btn" class="next">
                                    <i class="fa fa-angle-right fa-3x"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-info">
                        <h2><?php echo $product->Title; ?></h2>
                        <p><?php echo $product->Description; ?></p>
                        <div class="ratings">
                            <div id="showRating"></div>
                        </div>
                        <h4>Specifications</h4>
                        <?php echo $product->Specifications; ?>
                        <?php if ($product->IsCorporateProduct == 1) { ?>
                            <h5 class="MinimumQty <?php echo $MinQClass; ?>">Minimum Qty
                                <span><?php echo $MinQuantity; ?> KG</span></h5>
                        <?php } ?>
                        <h5 class="<?php echo $PriceClass; ?>">Price
                            <?php
                            if ($IsOnOffer) { ?>
                                <span id="PriceP"
                                      style="text-decoration: line-through;"><?php echo number_format($Price, 2); ?> SAR</span>
                                <span style="text-decoration: line-through;"
                                      id="PriceT"><?php echo $PriceType; ?></span>
                                <span style="font-weight: bold;" class="offered_product"
                                      title="<?php echo $DiscountDescription; ?>"><?php echo number_format($ProductDiscountedPrice, 2); ?> SAR</span>
                                <span style="font-weight: bold;" class="offered_product"
                                      id="PriceT"><?php echo $PriceType; ?></span>
                                <?php
                                $ProductPriceForCart = $ProductDiscountedPrice;
                            } else { ?>
                                <span id="PriceP"><?php echo number_format($Price, 2); ?> SAR</span>
                                <span id="PriceT"><?php echo $PriceType; ?></span>
                                <?php
                                $ProductPriceForCart = $Price;
                            }
                            ?>
                        </h5>
                        <?php
                        if ($product->OutOfStock == 1) { ?>
                            <small style="font-weight: bold;color: red;">(Out Of Stock)</small>
                        <?php }
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <h6><?php echo lang('quantity'); ?></h6>
                            </div>
                            <div class="col-md-6">
                                <input id="after" name="Quantity" class="form-control Quantity" type="number"
                                       value="<?php echo $MinQuantity; ?>" min="1"/>
                                <input type="hidden" class="ProductID" name="ProductID"
                                       value="<?php echo $product->ProductID; ?>">
                                <input type="hidden" class="ProductPrice" name="ProductPrice"
                                       value="<?php echo $ProductPriceForCart; ?>">
                                <input type="hidden" class="IsCorporateItem" value="<?php echo $IsCorporateItem; ?>">
                                <input type="hidden" class="ItemType" value="Product">


                                <input id="IsCorporateProductOriginal" type="hidden"
                                       value="<?php echo $product->IsCorporateProduct; ?>">
                                <input id="CorporateMinQuantityOriginal" type="hidden"
                                       value="<?php echo $product->CorporateMinQuantity; ?>">
                                <input id="ProductPriceOriginal" type="hidden"
                                       value="<?php echo number_format($product->Price, 2); ?>">
                                <input id="CorporateProductPriceOriginal" type="hidden"
                                       value="<?php echo number_format($product->CorporatePrice, 2); ?>">
                                <input id="PriceTypeOriginal" type="hidden"
                                       value="<?php echo($product->PriceType == 'item' ? lang('price_type_item') : lang('price_type_kg')); ?>">
                            </div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary"
                                        onclick="addToCart();"><?php echo lang('add_to_basket'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content rateproduct">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3><strong><span>rate the</span> Product</strong></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?php if ($UserCanReviewRate && $review) { ?>
                        <h6 class="heading">Your review</h6>
                        <div class="row">
                            <div class="col-md-7">

                                <h6><span><?php echo date('d.m.Y h:i A', strtotime($review->CreatedAt)) ?></span>
                                </h6>
                                <p class="cname"><strong><?php echo $review->Title; ?></strong></p>
                            </div>
                            <div class="col-md-5">
                                <div class="ratings">
                                    <?php
                                    if ($rating) { ?>
                                        <div class="alreadyRated"></div>
                                    <?php } else { ?>
                                        <div class="giveRating"></div>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p><?php echo $review->Comment; ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($UserCanReviewRate && !$review) { ?>
                        <form action="<?php echo base_url('product/saveReview'); ?>" method="post" class="ajaxForm"
                              id="productReviewForm">
                            <div class="row">
                                <h6 class="heading">Give Your Review</h6>
                                <div class="col-md-7">
                                    <input type="text" name="Title" placeholder="Title" class="form-control required">
                                    <input type="hidden" name="ProductID" value="<?php echo $product->ProductID; ?>">
                                </div>
                                <div class="col-md-5">
                                    <div class="ratings">
                                        <?php
                                        if ($rating) { ?>
                                            <div class="alreadyRated"></div>
                                        <?php } else { ?>
                                            <div class="giveRating"></div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea placeholder="Review" name="Comment"
                                              class="form-control required"></textarea>
                                    <input type="submit" name="" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    <?php } ?>

                    <div class="row">
                        <h6 class="heading">Other Reviews</h6>
                        <?php
                        if ($all_reviews) {
                            foreach ($all_reviews as $item) {
                                ?>
                                <div class="col-md-12">
                                    <h6><?php echo $item->FullName; ?>
                                        <span><?php echo date('d.m.Y h:i A', strtotime($item->CreatedAt)) ?></span></h6>
                                    <p class="cname"><strong><?php echo $item->Title; ?></strong></p>
                                    <p><?php echo $item->Comment; ?></p>
                                </div>
                            <?php }
                        } else { ?>
                            <div class="col-md-12">
                                <?php
                                if ($review) { ?>
                                    <h6>No other reviews yet.</h6>
                                <?php } else { ?>
                                    <h6>No reviews yet.</h6>
                                <?php }
                                ?>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
                <?php
                $ratings = productRatings($product->ProductID);
                ?>
                <div class="col-md-4">
                    <div class="ratingbox">
                        <h6 class="heading">Ratings
                            <small>(<?php echo $ratings['total_ratings_count']; ?>)</small>
                        </h6>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_1']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_1']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_1']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_2']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_2']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_2']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_3']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_3']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_3']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_4']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_4']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_4']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                        <div class="progress progress-bar-vertical last">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="<?php echo $ratings['rating_5']; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="height: <?php echo $ratings['rating_5']; ?>%;">
                                <span class="sr-only"><?php echo $ratings['rating_5']; ?>% Complete</span>
                                <img src="<?php echo front_assets(); ?>images/wstar.png" class="star">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php if ($product->IsCorporateProduct == 1) { ?>
    <script>
        $(document).on('keyup', '.Quantity', function () {
            setTimeout(function () {
                updateFields();
            }, 500);
        });

        $(document).on('click', '.input-group-btn', function () {
            var operator = $(this).find('button').text();
            if (operator == '+' || operator == '-') {
                setTimeout(function () {
                    updateFields();
                }, 100);
            }
        });

        function updateFields() {
            var IsCorporateItem = $('.IsCorporateItem').val();
            var IsCorporateProductOriginal = $('#IsCorporateProductOriginal').val();
            var CorporateMinQuantityOriginal = $('#CorporateMinQuantityOriginal').val();
            var ProductPriceOriginal = $('#ProductPriceOriginal').val();
            var CorporateProductPriceOriginal = $('#CorporateProductPriceOriginal').val();
            var PriceTypeOriginal = $('#PriceTypeOriginal').val();
            var Quantity = $('.Quantity').val();
            if (IsCorporateItem == 1) {
                if (Quantity < CorporateMinQuantityOriginal) {
                    showCustomLoader();
                    $('.ProductPrice').val(ProductPriceOriginal);
                    $('.IsCorporateItem').val(0);
                    $('.MinimumQty').hide();
                    $('#PriceT').html(PriceTypeOriginal);
                    $('#PriceP').html(PriceTypeOriginal);
                    $('#PriceP').html(ProductPriceOriginal);
                    $('#PriceP').parent('h5').addClass('bordered');
                    hideCustomLoader();
                    showMessage('This item will not be added to cart as a corporate product now', 'danger');
                }
            } else if (IsCorporateItem == 0) {
                if (Quantity >= CorporateMinQuantityOriginal) {
                    showCustomLoader();
                    $('.ProductPrice').val(CorporateProductPriceOriginal);
                    $('.IsCorporateItem').val(1);
                    $('.MinimumQty').show();
                    $('#PriceT').html('<?php echo lang('price_type_kg'); ?>');
                    $('#PriceP').html(CorporateProductPriceOriginal);
                    $('#PriceP').parent('h5').removeClass('bordered');
                    hideCustomLoader();
                    showMessage('This item will be added to cart as a corporate product now');
                }
            }
        }
    </script>
<?php } ?>
    <script>
        $(function () {
            $("#showRating").rateYo({
                rating: <?php echo productAverageRating($product->ProductID); ?>,
                readOnly: true
            });
        });
    </script>
<?php
if ($UserCanReviewRate) { ?>
    <script>
        $(function () {
            // $(".alreadyRated").rateYo({
                // rating: <?php // echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
            // });

            $(".alreadyRated").rateYo({
                rating: <?php echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
                fullStar: true,
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });

        $(function () {
            $(".giveRating").rateYo({
                fullStar: true,
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
    </script>
<?php } ?>