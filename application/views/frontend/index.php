<?php $home = getPageContent(7, $language); ?>
<style>
nav.navbar {
    height:125px;
}
.carousel {
    position: relative;
    display: block;
    width: 100%;
    height: calc(100vh - 130px);
}
.carousel-inner {
    height: 100%;
}
.slider .carousel-inner .item {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 100%;
    background-size: cover;
    background-position: center center;
    background-repeat:no-repeat;
}
.slider .carousel-inner .item > img {display:none;}
.owl-carousel.owl-drag .owl-item img {
    width: 192px;
    height: 192px;
}
@media(max-width: 576px){
    .slider .carousel-inner .item {
        background-attachment: scroll;
    }
}
a.carousel-control-prev {
    left: 0 !important;
    right: auto !important;
    top: 0 !important;
    bottom: 0 !important;
    color: #000 !important;
   background-color: rgba(255, 255, 255, 0.5); 
    position: absolute !important;
    padding: 0 7px 0 5px !important;
    border-radius: 50px !important;
    height: 31px !important; 
    margin: auto 15px !important;
}
 a.carousel-control-next {
    left: auto !important;
    right: 0 !important;
    background-color: rgba(255, 255, 255, 0.5); 
    position: absolute !important;
    top: 0 !important;
    color: #000 !important;
    padding: 0px 5px 0px 7px !important;
    border-radius: 50px !important;
    height: 31px !important; 
    margin: auto 15px !important;
 }
.bi.custom-bi::before{
    font-size: 20px !important;
    line-height: 1.5 !important;
}
.carousel-caption{
    bottom: 15% !important;
    /* top:50% !important; */
    max-width: 1500px !important;
}
.carousel-caption h2{
    margin: 0px 0px 30px !important;
    /* color: #392323 !important; */
}
.carousel-caption h2 span{
   padding-top:20px !important;
}
/* .back-color{
    background-color: rgba(255, 255, 255, 0.5); 
    padding:20px;
} */
.brands-slider .owl-nav button{
    top: -30px;
}
/* .brands-slider .owl-nav button.owl-prev {
    top: -30px;
    left: -65px;
}
.brands-slider .owl-nav button {
    width: 68px;
    height: 65px;
    top: -30px;
    right: -65px;
} */
/* .custom-indicators,.custom-indicators .active{
width:15px !important;
height:15px !important;
} */
.custom-indicators li .active{
width:18px !important;
height:18px !important;
}
.custom-indicators li{
width:15px !important;
height:15px !important;
}
</style>
<div class="slider homeEdSlider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators custom-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
        <!-- Carousel items -->
        <div class="carousel-inner">
            <?php
            $i = 0;
            foreach ($slider_images as $slider_image) { ?>
                <div style="background-image:url('<?php echo base_url($slider_image->Image); ?>');" class="item <?php echo($i == 0 ? 'active' : ''); ?>">
                    <img src="<?php echo base_url($slider_image->Image); ?>">
                    <div class="container carousel-caption">
                        <div class="">
                        <div class="wow fadeInRightBig back-color" data-wow-animation="2s">
                        <br>
                            <h2>
                            <?php echo $slider_image->Title; ?><br>
                            <span><?php echo $slider_image->Description; ?></span>
                            </h2>
                            <?php
                            if ($slider_image->UrlLink !== '') { ?>
                                <a href="<?php echo $slider_image->UrlLink; ?>">
                                    <button type="button" class="btn btn-secondary"><?php echo lang('view_detail'); ?></button>
                                </a>
                            <?php }
                            ?>
                        </div>
                        </div>
                        
                    </div>
                </div>
                <?php $i++;
            }
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"><i class="bi bi-chevron-left custom-bi"></i></span>
            <span class="sr-only">Previous</span>
            <!-- <i class="fa fa-chevron-left"></i> -->
        </a>
        <!-- <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <i class="bi bi-chevron-right custom-bi" aria-hidden="true"></i> -->
            <!-- <i class="fa fa-chevron-right"></i> -->
        <!-- </a> -->
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"> <i class="bi bi-chevron-right custom-bi" aria-hidden="true"></i></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<section class="content one">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner">
                    <img src="<?php echo front_assets(); ?>images/logo2.png">
                    <?php echo $home->Description; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content two imgan">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <?php
                    $total_featured = 1;
                    foreach ($collections_featured as $val) {
                        $total_featured++;

                        $image = get_images($val->CollectionID, 'collection', false);

                        ?>
                        <li>
                            <a href="<?php echo $val->Link; ?>">
                                <div class="img-overlay"
                                     style="background-image: url(<?php echo base_url($val->HomeImage); ?>);">
                                </div>
                                <div class="titlebox">
                                    <h4><?php echo $val->Title; ?> <span></span></h4>
                                </div>
                            </a>
                        </li>
                    <?php }

                   /* if ((Count($collections_featured) < 4) && $collections) {

                        for ($i = $total_featured; $i < 5; $i++) {

                            if (isset($collections[$i]->IsFeatured) && $collections[$i]->IsFeatured == 0) {
                                $image = get_images($collections[$i]->CollectionID, 'collection', false)
                                ?>

                                <li>
                                    <a href="<?php echo $collections->Link; ?>">
                                        <div class="img-overlay"
                                             style="background-image: url(<?php echo base_url($collections[$i]->HomeImage); ?>);">
                                        </div>
                                        <div class="titlebox">
                                            <h4><?php echo(isset($collections[$i]->Title) ? $collections[$i]->Title : ''); ?>
                                                <span></span></h4>
                                        </div>
                                    </a>
                                </li>


                                <?php
                            }

                        }

                    }*/


                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php
if ($featured_products) { ?>
    <section class="content three">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h2><?php echo lang('featured'); ?> <span><?php echo lang('products'); ?></span></h2>
                    </div>
                    <div class="procarousel">
                        <div class="owl-carousel owl-theme brands-slider">
                            <?php
                            foreach ($featured_products as $featured_product) { ?>
                                <div class="item">
                                    <a href="<?php echo base_url() . 'product/detail/' . productTitle($featured_product->ProductID); ?>">
                                        <img src="<?php echo base_url(get_images($featured_product->ProductID, 'product', false)); ?>">
                                        <h4><?php echo $featured_product->Title; ?></h4>
                                    </a>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="text-center btn-row">
                            <a href="<?php echo base_url('product'); ?>"
                               class="btn btn-primary"><?php echo lang('view_all'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }
?>

<section class="content four imgan hide">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2><?php echo lang('customize'); ?> <span><?php echo lang('your_order'); ?></span></h2>
                </div>
                <ul>

                    <?php if($customizations){
                        foreach($customizations as $customize){ ?>
                    
                    <li>
                        <!--<a href="<?php echo base_url('customize/boxes/choco_box'); ?>">-->
                            <a href="javascript:void(0);">

                            <div class="img-overlay"
                                 style="background-image: url('<?php echo base_url($customize->Image); ?>');">
                            </div>
                            <div class="titlebox">
                                <h4><?php echo $customize->Title; ?>
                                </h4>
                            </div>
                        </a>
                    </li>
                <?php } } ?>
                   
                    
                </ul>
            </div>
        </div>
    </div>
</section>
