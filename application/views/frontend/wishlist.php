

<section class="content checkout">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="navarea">
                    <li><a href="checkout.php">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span>Shopping Basket</span>
                    </a></li>
                    <li><a href="wishlist.php" class="active">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <span>Wishlist</span>
                    </a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="content products pt0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img12.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img13.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img14.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img15.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img16.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img17.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img18.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img19.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img20.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img21.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img22.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox custom">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img23.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>25</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img18.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img19.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img20.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img21.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img22.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>20</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="inbox custom">
                            <a href="single-product.php">
                                <div class="imgbox">
                                    <img src="<?php echo front_assets(); ?>images/img23.png">
                                </div>
                                <h4>Walnut & Chocolate</h4>
                                <h5><strong>25</strong> SAR</h5>
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

