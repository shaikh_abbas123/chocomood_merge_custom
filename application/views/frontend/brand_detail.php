<?php
$URL_Ids = getURLIds();
$category_ids_in_url = $URL_Ids['category_ids'];
$collection_ids_in_url = $URL_Ids['collection_ids'];
?>
<link rel="stylesheet" href="<?php echo front_assets(); ?>jPages/css/jPages.css">
<link rel="stylesheet" href="<?php echo front_assets(); ?>jPages/css/animate.css">
<script src="<?php echo front_assets(); ?>jPages/js/highlight.pack.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/tabifier.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/js.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/jPages.js"></script>
<style>
    .holder {
        margin: 15px 0;
    }

    .holder a {
        font-size: 12px;
        cursor: pointer;
        margin: 0 5px;
        color: #333;
    }

    .holder a:hover {
        background-color: #222;
        color: #fff;
    }

    .holder a.jp-previous {
        margin-right: 15px;
    }

    .holder a.jp-next {
        margin-left: 15px;
    }

    .holder a.jp-current,
    a.jp-current:hover {
        color: #FF4242;
        font-weight: bold;
    }

    .holder a.jp-disabled,
    a.jp-disabled:hover {
        color: #bbb;
    }

    .holder a.jp-current,
    a.jp-current:hover,
    .holder a.jp-disabled,
    a.jp-disabled:hover {
        cursor: default;
        background: none;
    }

    .holder span {
        margin: 0 5px;
    }


    .content.products .inbox .add_wishlist_to_cart {
        position: absolute;
        top: 7px;
        <?php echo($lang=='AR'? 'left' : 'right');
        ?>: 55px;
        bottom: auto;
        <?php echo($lang=='AR'? 'right' : 'left');
        ?>: auto;
        color: #fb7176;
        font-size: 20px;
    }

    .btnrow {
        margin-top: 25px;
    }

    .custom-container {
        width: 90% !important;
    }

    #sidebar1 .list-group-item.list-item-product,
    #sidebar1 .list-group-item.list-item-brand {
        margin: 0 !important;
        color: #000 !important;
        background-color: #f4ebd3 !important;
        /* border-bottom: 1px solid #adadad !important; */
        border: none !important;
        border-radius: 0 !important;
        font-size: 15px !important;
        font-weight: bold !important;
        /* text-transform: uppercase; */
        padding: 15px 35px !important;
    }

    .collapse-span {
        background-color: #F4EBD3;

    }

    .collpase.collapse-span {
        padding-bottom: 10px;
        display: table;
        margin-right: auto;
        margin-left: auto;

    }

    .submenu:hover {
        background: #e9e8e6;
    }

    .choco-name {
        padding: 7px 30px 7px 35px;
        text-align: left;
        color: #000 !important;

    }

    #sidebar1 .list-group-item.list-item-product,
    #sidebar1 .list-group-item.list-item-brand {
        /* border-bottom: 1px solid #adadad !important; */
        color: #50456d !important;
    }

    @media screen and (min-width: 1400px) and (max-width: 1850px) {
        .choco-name {
            padding: 7px 7px 7px 40px !important;
        }
    }

    .choco-name:hover {
        background-color: #d9d0b8;
    }

    #sidebar1 .subList-item-product,
    #sidebar1 .subList-item-brand {
        margin: 0 !important;
        color: black !important;
        /* background-color: #f4ebd3 !important; */
        border: none !important;
        border-radius: 0 !important;
        font-size: 14px !important;
        font-weight: bold !important;
        padding: 15px 35px !important;

    }

    .submenu {
        text-align: center;
        padding: 7px 30px 7px 30px;
        transition: .3s !important;
    }

    .left-submenu {
        display: inline-block;
        width: 128px;
        text-align: left;
        padding-left: 9px;
        color: #000 !important;
    }

    #sidebar1 .subList-item-product:hover {
        background-color: #e9e8e6 !important;
        /* padding:0 10p 0 10px !important; */
    }

    .left-submenu:hover {
        background-color: #e9e8e6 !important;
        /* padding: 0px 30px 0px 30px !important; */
    }

    .right-submenu {
        /* background-color:#A1826A !important; */
        display: inline-block;
        width: 50px;
        float: right;
        text-align: center;
    }

    .priceRange {
        text-align: center;
        padding: 10px;
    }

    .priceRange span {
        display: inline-block;
        margin: 0px 5px;
        color: #50456d !important;
    }

    input[type=number] {
        border: 1px solid #ddd;
        text-align: center;
        border-radius: 10px;
        padding: 8px;
    }

    .customradio,
    .customcheck {
        color: #50456d !important;
    }

    .mobile-filter {
        display: none !important;
    }

    /* .filter-column{
        display:flex;
        justify-content:center;
    } */
    @media (max-width: 1500px) {

        #sidebar1 .list-group-item.list-item-product,
        #sidebar1 .list-group-item.list-item-brand {
            font-size: 15px !important;
            padding: 15px 15px !important;
        }
        #sidebar1 .subList-item-product {
            padding: 15px 15px !important
        }
    }

    @media screen and (min-width:992px) and (max-width:1200px) {

        #sidebar1 .list-group-item.list-item-product,
        #sidebar1 .list-group-item.list-item-brand {
            font-size: 14px !important;
            padding: 12px 8px !important;
        }
    }

    @media screen and (max-width:1200px) and (min-width:600px) {
        .mobile-filter {
            display: block !important;
        }

        .desktop-filter {
            display: none !important;
        }
        .to-hide-600.mobile-filter.mt-4{
            margin-top:0px !important;
        }
    }

    .dv-hide-desktop {
        display: none;
        position: fixed;
        top: 122px;
        background: #32231f;
        width: 100%;
        z-index: 9;
        height: 100%;
        overflow-y: scroll;
        padding-bottom: 130px;
        padding-top: 30px;
    }

    .dv-hide-desktop select.form-control {
        background-color:
            transparent;
        color: #fff;
        border: 1px solid #f4ebd3;
        margin-bottom: 10px !important;
        width: 94%;
        margin: 0 auto;
    }

    .dv-hide-desktop h6 {
        color: #fff;
        font-size: 16px;
        width: 93%;
        margin-bottom: 20px !important;
        margin: 0 auto;
    }

    .dv-hide-desktop h6 span {
        float: right;
    }

    img.only-mob {
        display: none;
    }

    @media only screen and (max-width:600px) {
        img.only-mob {
            display: block;
        }

        .col-md-12.holder a {
            display: none;
        }

        .col-md-12.holder span {
            display: none;
        }

        .col-md-12.holder a.jp-previous,
        .col-md-12.holder a.jp-next {
            display: inline-block;
            width: 40%;

        }

        .custom-container.on-mob-w-100 {
            width: 100% !important;
        }


    }

    @media only screen and (max-width:601px) {
        .to-hide-600 {
            display: none;
        }
    }

    img.only-mob {
        position: fixed;
        top: 141px;
        right: 16px;
        z-index: 9;
    }

    a .left-submenu,
    a .right-submenu,
    a .choco-name {
        color: #50456d !important;
        font-weight: bold;
        font-size: 12px;
    }

    a .right-submenu {
        color: #50456d !important;
        font-weight: 800 !important;
        font-size: 13px !important;
    }

    #sidebar1 .collapse.in {
        border-bottom: 1px solid #ddd !important;
    }

    div#sidebar1 .list-group .collapse.in #sidebar1 .collapse.in:last-child {
        border-bottom: none !important;
    }

    .choco-name .right-submenu {
        text-align: right !important;
    }

    /* .filter-column{
        display:flex;
        justify-content:center;
    } */
    @media (max-width: 1500px) {

        #sidebar1 .list-group-item.list-item-product,
        #sidebar1 .list-group-item.list-item-brand {
            font-size: 15px !important;
            padding: 15px 15px !important;
        }
    }

    @media screen and (min-width:1400px) and (max-width: 1650px) {
        .choco-name {
            padding: 7px 30px 10px 35px !important;
        }
    }

    @media screen and (min-width:1201px) and (max-width: 1399px) {
        .choco-name {
            padding: 7px 22px 10px 0px !important;
        }

        .submenu {
            padding: 7px 7px 7px 30px !important;
        }

        .choco-name .left-submenu {
            width: 100px !important;
        }

        .choco-name .right-submenu {
            text-align: right !important;
        }
    }

    i.bi.bi-plus,
    .bi.bi-dash {
        font-size: 22px;
    }

    .submenu {
        padding: 7px 7px 7px 35px !important;
    }

    @media screen and (min-width:1200px) {
        /* div#sidebar1 .list-group {
            padding-bottom: 3px !important;
        } */
    }

    @media screen and (min-width:1650px) and (max-width:1850px) {
        .choco-name .right-submenu {
            text-align: center !important;
        }
    }
    .list-group.list-group-brand{
        margin-bottom: 5px !important;
    }

    @media screen and (max-width: 767px) and (min-width: 600px){
    .custom-filter {
        display: block !important;
    }
    .align-filter-right {
    display: block !important;
    margin-bottom: 25px !important;
    }
        .content.titlarea .filter select.form-control {
        margin-top: 0 !important;
    }
}

@media screen and (max-width: 991px) and (min-width: 768px){
    
    .width--change-n {
        margin-left: 10px !important;
    }
   
}
@media screen and (max-width: 991px) and (min-width: 600px){
    .right-filter-icon {
        margin-right: 10px !important;
    }
}

@media screen and (max-width: 1199px) and (min-width: 600px){
    .tab-filter-feature label.customcheck {
        margin: 0;
        color: #50456d;
        background-color: #f4ebd3;
        border: none;
        border-radius: 0;
        font-size: 15px;
        font-weight: bold;
        margin-bottom: 5px;
    }
    .tab-filter-feature {
        display: block !important;
    }
    #sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand {
    font-size: 15px !important;
    padding: 15px 35px !important;
}
}

@media screen and (min-width: 1399px) and (max-width: 1850px){
    #sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand {
    padding: 15px 25px !important;
}
}
@media screen and (min-width: 1400px) and (max-width: 1580px){
    #sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand {
    padding: 15px 20px !important;
}
}
.content.products div#sidebar .list-group.tags-list {
    margin-bottom: 20px !important;
}
</style>

<section class="content products titlarea pt-0">
    <div class="edProdBanner"
        style="background-image:url('<?= (isset($brand_data[0]->Image))?base_url($brand_data[0]->Image):base_url().'uploads/images/945234280892020080201470516022425066202003230806332.jpg';?>')">
        <div class="container">
            <h2><?= (isset($brand_data[0]->brand_name))?$brand_data[0]->brand_name:lang('chocomood') ?></h2>
            <ul>
                <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                <li><a><?= (isset($brand_data[0]->brand_name))?$brand_data[0]->brand_name:lang('chocomood') ?></a>
                </li>

            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="filter to-hide-600">
                    <div class="row">
                        <div class="col-md-6 col-sm-5 custom-filter" style="display:flex;">
                            <h6>
                                <i class="fa fa-filter" aria-hidden="true"></i>
                                <?php echo lang('filter'); ?>
                            </h6>
                            <?php
                            $price_highest_to_lowest = "";
                            $price_lowest_to_higher = "";
                            $product_older_to_newer = "";
                            $product_newer_to_older = "";
                            $purchased_highest_to_lowest = "";
                            $purchased_lowest_to_higher = "";
                            if (isset($_GET['sort']) && $_GET['sort'] != '') {
                                if ($_GET['sort'] == 'price_highest_to_lowest') {
                                    $price_highest_to_lowest = 'selected';
                                }

                                if ($_GET['sort'] == 'price_lowest_to_higher') {
                                    $price_lowest_to_higher = 'selected';
                                }

                                if ($_GET['sort'] == 'product_older_to_newer') {
                                    $product_older_to_newer = 'selected';
                                }

                                if ($_GET['sort'] == 'product_newer_to_older') {
                                    $product_newer_to_older = 'selected';
                                }

                                if ($_GET['sort'] == 'purchased_highest_to_lowest') {
                                    $purchased_highest_to_lowest = 'selected';
                                }

                                if ($_GET['sort'] == 'purchased_lowest_to_higher') {
                                    $purchased_lowest_to_higher = 'selected';
                                }
                            }
                            ?>
                            <select class="form-control width--change-n" onchange="sortProducts(this.value);">
                                <option selected disabled><?php echo lang('Select_filter_to_sort_by'); ?></option>
                                <option value="price_highest_to_lowest" <?php echo $price_highest_to_lowest ?>>
                                    <?php echo lang('price'); ?>:
                                    <?php echo lang('Highest_to_lowest'); ?>
                                </option>
                                <option value="price_lowest_to_higher" <?php echo $price_lowest_to_higher ?>>
                                    <?php echo lang('price'); ?>:
                                    <?php echo lang('Lowest_to_Highest'); ?>

                                </option>
                                <!-- <option value="product_older_to_newer" <?php echo $product_older_to_newer ?>><?php echo lang('products'); ?>:
                                   <?php echo lang('Older_to_Newer'); ?>
                                </option>
                                <option value="product_newer_to_older" <?php echo $product_newer_to_older ?>><?php echo lang('products'); ?>:
                                    <?php echo lang('Newer_to_Older'); ?>
                                </option> -->
                                <option value="purchased_highest_to_lowest" <?php echo $purchased_highest_to_lowest ?>>
                                    <?php echo lang('Purchased'); ?>:
                                    <?php echo lang('Highest_to_lowest'); ?>
                                </option>
                                <option value="purchased_lowest_to_higher" <?php echo $purchased_lowest_to_higher ?>>
                                    <?php echo lang('Purchased'); ?>:
                                    <?php echo lang('Lowest_to_Highest'); ?>
                                </option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <h6>
                                <span>
                                    <b><?php echo lang('count'); ?></b>
                                    <i aria-hidden="true" id="now"><?php echo count($products); ?> </i>/
                                    <i aria-hidden="true" id="total"> <?php echo $countproducts; ?></i>
                                </span>
                            </h6>
                        </div>
                        <div class="col-md-2 col-sm-3">
                            <select class="form-control ProductsPerPage">
                                <option selected disabled><?php echo lang('product_per_page'); ?></option>
                                <option>9</option>
                                <option>15</option>
                                <option>20</option>
                            </select>
                        </div>
                        <div class="col-md-1 iconsrow text-right col-sm-1 align-filter-right">
                            <a onclick="changeGridLayout('items_list');" class="right-filter-icon"><i class="fa fa-list align-filter-right-fa"></i></a>
                            <a onclick="changeGridLayout('items_grid');" class="left-filter-icon"><i class="fa fa-th align-filter-right-fa"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content products">
    <div class="container-fluid custom-container on-mob-w-100">
        <div class="row">
        <div class="col-xs-12 tab-filter-feature d-none">
                        <div class="list-group onlyChkBox">
                            <label class="customcheck">
                                <input class="get_products featured2" type="checkbox" data-url-title=""
                                    name="chkFeature" value="">
                                <span class="checkmark"></span><?php echo lang('featured'); ?>
                            </label>
                        </div>
                    </div>
            <div class="col-lg-2 col-xs-12 to-hide-600" id="sidebar1">
                <div class="row">
                    <?php if (!isset($search)) { ?>
                    <div class="col-lg-12 col-xs-6 to-hide-600 desktop-filter" id="sidebar">
                        <div class="list-group onlyChkBox">
                            <label class="customcheck">
                                <input class="get_products featured" type="checkbox" data-url-title="" name="chkFeature"
                                    value="">
                                <span class="checkmark"></span><?php echo lang('featured'); ?>
                            </label>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-xs-6 col-lg-12">
                        <div class="list-group list-group-brand">
                            <a href="#menu-cate-brands" onclick="myFunction(this)"
                                class="list-group-item collapsed list-item-brand" data-toggle="collapse"
                                data-parent="#sidebar" aria-expanded="false">
                                <span><?= lang('brands')?><i class="bi bi-plus" style="float:right !important"
                                        aria-hidden="true"></i></span>
                            </a>

                            <div class="collapse" id="menu-cate-brands" aria-expanded="false" style="height: 0px;">
                                <div class="collapse-span">
                                    <?php
                            foreach ($brands as $key => $p) {
                        ?>
                                    <a
                                        href="<?= base_url() ?>product/brand_detail?q=<?=strtolower(str_replace(' ','-',$p->brand_name))?>-s<?=$p->brand_id?>">
                                        <div class="choco-name">
                                            <span class="left-submenu"><?= $p->brand_name?></span>
                                            <span
                                                class="right-submenu"><?= count_product($p->brand_id,'brand');?></span>
                                        </div>
                                    </a>

                                    <?php 
                            }
                        ?>
                                    <a href="<?= base_url() ?>product/brand_detail?q=chocomood-s0">
                                        <div class="choco-name">
                                            <span class="left-submenu"><?=lang('chocomood') ?></span>
                                            <span class="right-submenu"><?= count_product(0,'brand');?></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-lg-12">
                        <div class="list-group mbottom-5">
                            <a href="#menu-cate-product" onclick="myFunction(this)"
                                class="list-group-item collapsed list-item-product plusToggle" data-toggle="collapse"
                                data-parent="#sidebar" aria-expanded="false">
                                <span><?= lang('products')?><i class="bi bi-plus" style="float:right !important"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate-product" aria-expanded="false" style="height: 0px;">
                                <div class="collapse-span">
                                    <?php $menu_categories = subCategories(0,$language);
                                if($menu_categories){ 
                                    foreach ($menu_categories as $key => $category) {?>
                                    <div class="list-group">
                                        <a href="#submenu-cate-product<?=$category->CategoryID?>"
                                            onclick="myFunction(this)"
                                            class="list-group-item collapsed subList-item-product"
                                            data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                                            <span><?= $category->Title?><i class="bi bi-plus"
                                                    style="float:right !important" aria-hidden="true"></i></span>
                                        </a>
                                        <div class="collapse" id="submenu-cate-product<?=$category->CategoryID?>"
                                            aria-expanded="false" style="height: 0px;" style="text-align:left">
                                            <?php $sub_cat = subCategories($category->CategoryID, $lang);
                                        foreach ($sub_cat as $k => $v) {?>
                                            <a
                                                href="<?php echo base_url('product');?>?q=<?php echo strtolower(str_replace(' ','-',$v->Title)); ?><?php echo '-s'.$v->CategoryID;?>">
                                                <div class="submenu">
                                                    <span class="left-submenu"><?= $v->Title?></span>
                                                    <span
                                                        class="right-submenu"><?= count_product($v->CategoryID,'subcat');?></span>
                                                </div>
                                            </a>
                                            <?php }?>


                                        </div>
                                    </div>
                                    <?php 
                                    }
                                }
                            ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                    <?php if (!isset($search)) { ?>
                    <div class="col-lg-12 col-xs-6 to-hide-600 desktop-filter" id="sidebar">

                        <?php /*if ($collections) {  ?>
                        <h5><?php echo lang('seasons'); ?></h5>
                        <?php
                        foreach ($collections as $key => $value) {
                            $collection_products = getCollectionProductsSubCategories(json_encode($value), $language);
                            ?>
                        <div class="list-group">
                            <a href="#menu<?php echo $key; ?>" class="list-group-item" data-toggle="collapse"
                                data-parent="#sidebar">
                                <span><?php echo $value->Title; ?><i class="fa fa-angle-down"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse<?php echo(($key == 0 && !isset($CollectionID)) ? ' in' : ''); ?><?php echo((isset($CollectionID) && $CollectionID == $value->CollectionID) ? ' in' : ''); ?>"
                                id="menu<?php echo $key; ?>">
                                <?php if ($collection_products) {
                                        $collection_category = array();
                                        foreach ($collection_products as $key => $category) {
                                            if (!in_array($category->CategoryID, $collection_category)) {
                                                ?>
                                <label class="customcheck"><?php echo $category->Title; ?>
                                    <input
                                        class="get_products <?php echo((isset($CollectionID) && $value->CollectionID == $CollectionID) ? 'checked_this' : ''); ?>"
                                        type="checkbox" data-collection-id="<?php echo $value->CollectionID; ?>"
                                        data-url-title="<?php echo strtolower(str_replace(' ', '-', $value->Title)); ?>-c<?php echo $value->CollectionID; ?>"
                                        name="SubCategoryID[]" value="<?php echo $category->CategoryID; ?>">
                                    <span class="checkmark"></span>
                                </label>
                                <?php
                                                $collection_category[] = $category->CategoryID;
                                            }
                                        }
                                    }
                                    ?>
                            </div>
                        </div>
                        <?php
                        }
                    }*/
                    ?>
                        <?php if ($offers || $offers_for_you) { ?>
                        <div class="list-group">
                            <a href="#menu-cate6" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('offers'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>

                            <div class="collapse" id="menu-cate6">
                                <?php if ($offers) { 

                                            foreach ($offers as $key => $value) {
                                        ?>
                                <label class="customcheck">
                                    <input class="offers get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php  
                                     }
                                        }


                                        ?>

                                <?php if ($offers_for_you) {
                                                 foreach ($offers_for_you as $key => $value) {
                                          ?>
                                <label class="customcheck">
                                    <input class="offers get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php } } ?>

                            </div>
                        </div>
                        <?php } ?>

                        <div class="list-group">


                            <a href="#menu-cate2" class="list-group-item" onclick="myFunction(this)"
                                data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('price'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate2">
                                <!-- <div class="priceRangeEd">
                            <div class="form-group">
                              >
                                <div id="slider-range"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div> -->
                                <input type="hidden" id="amount">
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="1">1$ - 50$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="2">51$ - 100$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="3">101$ - 150$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="4">151$ - 200$
                                    <span class="checkmark"></span>
                                </label>
                                <div class="priceRange">
                                    <span><input type="number" style="width:65px" id="minPrice" value="0" min="0"
                                            max="120000" readonly /></span>
                                    <span>to</span>
                                    <span><input type="number" style="width:65px" id="maxPrice" value="0" min="0"
                                            max="120000" readonly /></span>
                                </div>
                            </div>
                        </div>
                        <div class="list-group">
                            <a href="#menu-cate4" class="list-group-item" onclick="myFunction(this)"
                                data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('rating'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate4">

                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="1"><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="2"><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="3"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="4"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="5"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="list-group tags-list">
                            <a href="#menu-cate5" class="list-group-item" onclick="myFunction(this)"
                                data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('tags'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate5">
                                <div class="priceRangeEd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="search_tags"
                                            placeholder="<?php echo lang('search_tags'); ?>" style="margin: 0;">
                                    </div>
                                </div>
                                <?php if($tags){
                                                foreach ($tags as $key => $value) { ?>

                                <label class="customcheck search-labels" id="search-label-<?php echo $value->TagID;?>"
                                    data-text="<?php echo $value->Title; ?>">
                                    <input class="get_products tags" type="checkbox" data-url-title="" name="TagID[]"
                                        value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                    <?php /*$p_count =   getCustomRow("SELECT count(*) as count FROM products where IsCustomizedProduct = 0 AND products.IsActive = 1 AND TagIDs like '%".$value->TagID."%'")['count'];*/ ?>
                                    <span style="float:right;"><?= @$tags_count[$value->TagID]; ?></span>
                                </label>

                                <?php
                                                }
                                      }

                                      ?>


                            </div>
                        </div>

                        <?php /*if ($categories) { ?>
                        <h5><?php echo lang('categorys'); ?></h5>
                        <?php
                        foreach ($categories as $key => $category) {
                            $sub_categories = subCategories($category->CategoryID, $language);
                            if ($sub_categories) {
                                ?>
                        <div class="list-group">
                            <a href="#menu-cate<?php echo $key; ?>" class="list-group-item" data-toggle="collapse"
                                data-parent="#sidebar">
                                <span><?php echo $category->Title; ?><i class="fa fa-angle-down"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate<?php echo $key; ?>">
                                <?php if ($sub_categories) {
                                            foreach ($sub_categories as $key => $subcategory) { ?>
                                <label class="customcheck"><?php echo $subcategory->Title; ?>
                                    <input class="get_products" type="checkbox" data-collection-id="0"
                                        data-url-title="<?php echo strtolower(str_replace(' ', '-', $subcategory->Title)); ?>-s<?php echo $subcategory->CategoryID; ?>"
                                        name="SubCategoryID[]" value="<?php echo $subcategory->CategoryID; ?>">
                                    <span class="checkmark"></span>
                                </label>
                                <?php
                                            }
                                        }
                                        ?>
                            </div>
                        </div>
                        <?php
                            }
                        }
                    } */
                    ?>
                    </div>
                    <?php } ?>
                </div>


            </div>


            <div class="col-lg-2 col-xs-12 to-hide-600 mobile-filter mt-4 to-hide-600" id="sidebar">
                <div class="row">
                    <div class="col-xs-6">
                        <?php if ($offers || $offers_for_you) { ?>
                        <div class="list-group">
                            <a href="#menu-cate6-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('offers'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>

                            <div class="collapse" id="menu-cate6-mobile">
                                <?php if ($offers) { 

                                                foreach ($offers as $key => $value) {
                                            ?>
                                <label class="customcheck">
                                    <input class="offers2 get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php  
                                        }
                                            }


                                            ?>

                                <?php if ($offers_for_you) {
                                                    foreach ($offers_for_you as $key => $value) {
                                            ?>
                                <label class="customcheck">
                                    <input class="offers get_products" type="checkbox" name="Offer[]"
                                        value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                </label>

                                <?php } } ?>

                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-6">
                        <div class="list-group">
                            <a href="#menu-cate2-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('price'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate2-mobile">
                                <input type="hidden" id="amount">
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="1">1$ - 50$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="2">51$ - 100$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="3">101$ - 150$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customradio">
                                    <input class="filter_price radio " type="radio" name="Price" value="4">151$ - 200$
                                    <span class="checkmark"></span>
                                </label>
                                <div class="priceRange">
                                    <span><input type="number" style="width:65px" id="minPrice" value="0" min="0"
                                            max="120000" readonly /></span>
                                    <span>to</span>
                                    <span><input type="number" id="maxPrice" style="width:65px" value="0" min="0"
                                            max="120000" readonly /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="list-group">
                            <a href="#menu-cate4-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('rating'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate4-mobile">

                                <label class="customcheck">
                                    <input class="rating2 get_products" type="checkbox" data-url-title=""
                                        name="Rating[]" value="1"><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="2"><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="3"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="4"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="customcheck">
                                    <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                        value="5"><i class="filled_star"></i><i class="filled_star"></i><i
                                        class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="list-group">
                            <a href="#menu-cate5-mobile" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                                <span><?php echo lang('tags'); ?><i class="bi bi-plus"
                                        aria-hidden="true"></i></span>
                            </a>
                            <div class="collapse" id="menu-cate5-mobile">
                                <div class="priceRangeEd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="search_tags"
                                            placeholder="<?php echo lang('search_tags'); ?>" style="margin: 0;">
                                    </div>
                                </div>
                                <?php if($tags){
                                                foreach ($tags as $key => $value) { ?>

                                <label class="customcheck search-labels" id="search-label-<?php echo $value->TagID;?>"
                                    data-text="<?php echo $value->Title; ?>">
                                    <input class="get_products tags2" type="checkbox" data-url-title="" name="TagID[]"
                                        value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?>
                                    <span class="checkmark"></span>
                                    <?php /*$p_count =   getCustomRow("SELECT count(*) as count FROM products where IsCustomizedProduct = 0 AND products.IsActive = 1 AND TagIDs like '%".$value->TagID."%'")['count'];*/ ?>
                                    <span style="float:right;"><?= @$tags_count[$value->TagID]; ?></span>
                                </label>

                                <?php
                                                }
                                    }

                                    ?>


                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-9 col-xs-12 margin-bottom-40">
                <div class="row">
                    <div class="col-md-12" id="Product-Listing">
                        <?php echo product_html($products); ?>
                    </div>
                    <div class="col-md-12 holder"
                        <?php echo (empty($products) ? 'style="display:none;"' : 'style="display:block;"'); ?>></div>
                    <div class="col-md-12 btnrow text-center" style="display: none;">
                        <input type="hidden" value="0" id="Page">
                        <button id="loadmore" class="get_products loadmore btn btn-primary"
                            <?php echo((count($products) <= $countproducts) ? 'style="display:none;"' : ''); ?>><?php echo lang('load_more'); ?></button>
                    </div>
                </div>
            </div>

        </div>
        <div class="row dv-hide-desktop">
            <div class="filter">
                <div class="row">
                    <div class="col-md-6 col-sm-5">
                        <h6>
                            <i class="fa fa-filter" aria-hidden="true"></i>
                            <?php echo lang('filter'); ?>
                            <span>
                                <b><?php echo lang('count'); ?></b>
                                <i aria-hidden="true" id="now"><?php echo count($products); ?> </i>/
                                <i aria-hidden="true" id="total"> <?php echo $countproducts; ?></i>
                            </span>
                        </h6>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?php
                        $price_highest_to_lowest = "";
                        $price_lowest_to_higher = "";
                        $product_older_to_newer = "";
                        $product_newer_to_older = "";
                        $purchased_highest_to_lowest = "";
                        $purchased_lowest_to_higher = "";
                        if (isset($_GET['sort']) && $_GET['sort'] != '') {
                            if ($_GET['sort'] == 'price_highest_to_lowest') {
                                $price_highest_to_lowest = 'selected';
                            }

                            if ($_GET['sort'] == 'price_lowest_to_higher') {
                                $price_lowest_to_higher = 'selected';
                            }

                            if ($_GET['sort'] == 'product_older_to_newer') {
                                $product_older_to_newer = 'selected';
                            }

                            if ($_GET['sort'] == 'product_newer_to_older') {
                                $product_newer_to_older = 'selected';
                            }

                            if ($_GET['sort'] == 'purchased_highest_to_lowest') {
                                $purchased_highest_to_lowest = 'selected';
                            }

                            if ($_GET['sort'] == 'purchased_lowest_to_higher') {
                                $purchased_lowest_to_higher = 'selected';
                            }
                        }
                        ?>
                        <select class="form-control" onchange="sortProducts(this.value);">
                            <option selected disabled><?php echo lang('Select_filter_to_sort_by'); ?></option>
                            <option value="price_highest_to_lowest" <?php echo $price_highest_to_lowest ?>>
                                <?php echo lang('price'); ?>:
                                <?php echo lang('Highest_to_lowest'); ?>
                            </option>
                            <option value="price_lowest_to_higher" <?php echo $price_lowest_to_higher ?>>
                                <?php echo lang('price'); ?>:
                                <?php echo lang('Lowest_to_Highest'); ?>

                            </option>
                            <!-- <option value="product_older_to_newer" <?php echo $product_older_to_newer ?>><?php echo lang('products'); ?>:
                                <?php echo lang('Older_to_Newer'); ?>
                            </option>
                            <option value="product_newer_to_older" <?php echo $product_newer_to_older ?>><?php echo lang('products'); ?>:
                                <?php echo lang('Newer_to_Older'); ?>
                            </option> -->
                            <option value="purchased_highest_to_lowest" <?php echo $purchased_highest_to_lowest ?>>
                                <?php echo lang('Purchased'); ?>:
                                <?php echo lang('Highest_to_lowest'); ?>
                            </option>
                            <option value="purchased_lowest_to_higher" <?php echo $purchased_lowest_to_higher ?>>
                                <?php echo lang('Purchased'); ?>:
                                <?php echo lang('Lowest_to_Highest'); ?>
                            </option>
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <select class="form-control ProductsPerPage">
                            <option selected disabled><?php echo lang('product_per_page'); ?></option>
                            <option>9</option>
                            <option>15</option>
                            <option>20</option>
                        </select>
                    </div>
                    <div class="col-md-1 iconsrow text-right col-sm-2 to-hide-600">
                        <a onclick="changeGridLayout('items_list');"><i class="fa fa-list"></i></a>
                        <a onclick="changeGridLayout('items_grid');"><i class="fa fa-th"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-2" id="sidebar1">
                <div class="list-group">
                    <a href="#menu-cate-product-mob" onclick="myFunction(this)"
                        class="list-group-item collapsed list-item-product plusToggle" data-toggle="collapse"
                        data-parent="#sidebar" aria-expanded="false">
                        <span><?= lang('products')?><i class="bi bi-plus" style="float:right !important"
                                aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate-product-mob" aria-expanded="false" style="height: 0px;">
                        <div class="collapse-span">
                            <?php $menu_categories = subCategories(0,$language);
                                if($menu_categories){ 
                                    foreach ($menu_categories as $key => $category) {?>
                            <div class="list-group">
                                <a href="#submenu-cate-product-mob<?=$category->CategoryID?>" onclick="myFunction(this)"
                                    class="list-group-item collapsed subList-item-product" data-toggle="collapse"
                                    data-parent="#sidebar" aria-expanded="false">
                                    <span><?= $category->Title?><i class="bi bi-plus" style="float:right !important"
                                            aria-hidden="true"></i></span>
                                </a>
                                <div class="collapse" id="submenu-cate-product-mob<?=$category->CategoryID?>"
                                    aria-expanded="false" style="height: 0px;" style="text-align:left">
                                    <?php $sub_cat = subCategories($category->CategoryID, $lang);
                                        foreach ($sub_cat as $k => $v) {?>
                                    <a
                                        href="<?php echo base_url('product');?>?q=<?php echo strtolower(str_replace(' ','-',$v->Title)); ?><?php echo '-s'.$v->CategoryID;?>">
                                        <div class="submenu">
                                            <span class="left-submenu"><?= $v->Title?></span>
                                            <span
                                                class="right-submenu"><?= count_product($v->CategoryID,'subcat');?></span>
                                        </div>
                                    </a>
                                    <?php }?>


                                </div>
                            </div>
                            <?php 
                                    }
                                }
                            ?>
                        </div>

                    </div>
                </div>
                <div class="list-group list-group-brand">
                    <a href="#menu-cate-brands-mob" onclick="myFunction(this)"
                        class="list-group-item collapsed list-item-brand" data-toggle="collapse" data-parent="#sidebar"
                        aria-expanded="false">
                        <span><?= lang('brands')?><i class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                    </a>

                    <div class="collapse" id="menu-cate-brands-mob" aria-expanded="false" style="height: 0px;">
                        <div class="collapse-span">
                            <?php
                            foreach ($brands as $key => $p) {
                        ?>
                            <a
                                href="<?= base_url() ?>product/brand_detail?q=<?=strtolower(str_replace(' ','-',$p->brand_name))?>-s<?=$p->brand_id?>">
                                <div class="choco-name">
                                    <?= $p->brand_name?>
                                    <span class="right-submenu"><?= count_product($p->brand_id,'brand');?></span>
                                </div>
                            </a>

                            <?php 
                            }
                        ?>
                            <a href="<?= base_url() ?>product/brand_detail?q=chocomood-s0">
                                <div class="choco-name">
                                    <?= lang('chocomood')?>
                                    <span class="right-submenu"><?= count_product(0,'brand');?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (!isset($search)) { ?>
            <div class="col-md-2" id="sidebar">

                <?php /*if ($collections) {  ?>
                <h5><?php echo lang('seasons'); ?></h5>
                <?php
                        foreach ($collections as $key => $value) {
                            $collection_products = getCollectionProductsSubCategories(json_encode($value), $language);
                            ?>
                <div class="list-group">
                    <a href="#menu<?php echo $key; ?>" class="list-group-item" data-toggle="collapse"
                        data-parent="#sidebar">
                        <span><?php echo $value->Title; ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse<?php echo(($key == 0 && !isset($CollectionID)) ? ' in' : ''); ?><?php echo((isset($CollectionID) && $CollectionID == $value->CollectionID) ? ' in' : ''); ?>"
                        id="menu<?php echo $key; ?>">
                        <?php if ($collection_products) {
                                        $collection_category = array();
                                        foreach ($collection_products as $key => $category) {
                                            if (!in_array($category->CategoryID, $collection_category)) {
                                                ?>
                        <label class="customcheck"><?php echo $category->Title; ?>
                            <input
                                class="get_products <?php echo((isset($CollectionID) && $value->CollectionID == $CollectionID) ? 'checked_this' : ''); ?>"
                                type="checkbox" data-collection-id="<?php echo $value->CollectionID; ?>"
                                data-url-title="<?php echo strtolower(str_replace(' ', '-', $value->Title)); ?>-c<?php echo $value->CollectionID; ?>"
                                name="SubCategoryID[]" value="<?php echo $category->CategoryID; ?>">
                            <span class="checkmark"></span>
                        </label>
                        <?php
                                                $collection_category[] = $category->CategoryID;
                                            }
                                        }
                                    }
                                    ?>
                    </div>
                </div>
                <?php
                        }
                    }*/
                    ?>
                <div class="list-group onlyChkBox">
                    <label class="customcheck">
                        <input class="get_products featured" type="checkbox" data-url-title="" name="chkFeature"
                            value="">
                        <span class="checkmark"></span><?php echo lang('featured'); ?>
                    </label>
                </div>
                <?php if ($offers || $offers_for_you) { ?>
                <div class="list-group">
                    <a href="#menu-cate6-mob" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('offers'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>

                    <div class="collapse" id="menu-cate6-mob">
                        <?php if ($offers) { 

                                            foreach ($offers as $key => $value) {
                                        ?>
                        <label class="customcheck">
                            <input class="offers get_products" type="checkbox" name="Offer[]"
                                value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                            <span class="checkmark"></span>
                        </label>

                        <?php  
                                     }
                                        }


                                        ?>

                        <?php if ($offers_for_you) {
                                                 foreach ($offers_for_you as $key => $value) {
                                          ?>
                        <label class="customcheck">
                            <input class="offers get_products" type="checkbox" name="Offer[]"
                                value="<?php echo $value->OfferID; ?>"><?php echo $value->Title; ?>
                            <span class="checkmark"></span>
                        </label>

                        <?php } } ?>

                    </div>
                </div>
                <?php } ?>

                <div class="list-group">


                    <a href="#menu-cate2-mob" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('price'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate2-mob">
                        <!-- <div class="priceRangeEd">
                            <div class="form-group">
                              >
                                <div id="slider-range"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div> -->
                        <input type="hidden" id="amount">
                        <label class="customradio">
                            <input class="filter_price radio " type="radio" name="Price" value="1">1$ - 50$
                            <span class="checkmark"></span>
                        </label>
                        <label class="customradio">
                            <input class="filter_price radio " type="radio" name="Price" value="2">51$ - 100$
                            <span class="checkmark"></span>
                        </label>
                        <label class="customradio">
                            <input class="filter_price radio " type="radio" name="Price" value="3">101$ - 150$
                            <span class="checkmark"></span>
                        </label>
                        <label class="customradio">
                            <input class="filter_price radio " type="radio" name="Price" value="4">151$ - 200$
                            <span class="checkmark"></span>
                        </label>
                        <div class="priceRange">
                            <span><input type="number" style="width:65px" id="minPrice" value="0" min="0" max="120000"
                                    readonly /></span>
                            <span>to</span>
                            <span><input type="number" style="width:65px" id="maxPrice" value="0" min="0" max="120000"
                                    readonly /></span>
                        </div>
                    </div>
                </div>
                <div class="list-group">
                    <a href="#menu-cate4-mob" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('rating'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate4-mob">

                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="1"><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="2"><i class="filled_star"></i><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="3"><i class="filled_star"></i><i class="filled_star"></i><i
                                class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="4"><i class="filled_star"></i><i class="filled_star"></i><i
                                class="filled_star"></i><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                        <label class="customcheck">
                            <input class="rating get_products" type="checkbox" data-url-title="" name="Rating[]"
                                value="5"><i class="filled_star"></i><i class="filled_star"></i><i
                                class="filled_star"></i><i class="filled_star"></i><i class="filled_star"></i>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="list-group">
                    <a href="#menu-cate5-mob" onclick="myFunction(this)" class="list-group-item" data-toggle="collapse" data-parent="#sidebar">
                        <span><?php echo lang('tags'); ?><i class="bi bi-plus" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate5-mob">
                        <div class="priceRangeEd">
                            <div class="form-group">
                                <input type="text" class="form-control" id="search_tags"
                                    placeholder="<?php echo lang('search_tags'); ?>" style="margin: 0;">
                            </div>
                        </div>
                        <?php if($tags){
                                                foreach ($tags as $key => $value) { ?>

                        <label class="customcheck search-labels" id="search-label-<?php echo $value->TagID;?>"
                            data-text="<?php echo $value->Title; ?>">
                            <input class="get_products tags" type="checkbox" data-url-title="" name="TagID[]"
                                value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?>
                            <span class="checkmark"></span>
                            <?php /*$p_count =   getCustomRow("SELECT count(*) as count FROM products where IsCustomizedProduct = 0 AND products.IsActive = 1 AND TagIDs like '%".$value->TagID."%'")['count'];*/ ?>
                            <span style="float:right;"><?= @$tags_count[$value->TagID]; ?></span>
                        </label>

                        <?php
                                                }
                                      }

                                      ?>


                    </div>
                </div>

                <?php /*if ($categories) { ?>
                <h5><?php echo lang('categorys'); ?></h5>
                <?php
                        foreach ($categories as $key => $category) {
                            $sub_categories = subCategories($category->CategoryID, $language);
                            if ($sub_categories) {
                                ?>
                <div class="list-group">
                    <a href="#menu-cate<?php echo $key; ?>" class="list-group-item" data-toggle="collapse"
                        data-parent="#sidebar">
                        <span><?php echo $category->Title; ?><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate<?php echo $key; ?>">
                        <?php if ($sub_categories) {
                                            foreach ($sub_categories as $key => $subcategory) { ?>
                        <label class="customcheck"><?php echo $subcategory->Title; ?>
                            <input class="get_products" type="checkbox" data-collection-id="0"
                                data-url-title="<?php echo strtolower(str_replace(' ', '-', $subcategory->Title)); ?>-s<?php echo $subcategory->CategoryID; ?>"
                                name="SubCategoryID[]" value="<?php echo $subcategory->CategoryID; ?>">
                            <span class="checkmark"></span>
                        </label>
                        <?php
                                            }
                                        }
                                        ?>
                    </div>
                </div>
                <?php
                            }
                        }
                    } */
                    ?>
            </div>
            <?php } ?>
        </div>
        <img src="<?= base_url('assets/frontend/images/filter-img.png')?>" class="only-mob">
    </div>
</section>
<button class="get_products" id="hidden_btn" style="display: none;">&nbsp;</button>
<input type="hidden" value="0" id="hidden_price">

<script>
    $(document).ready(function () {
        $('.filter_price').change(function () {
            $('#hidden_price').val(1);
            if (this.value == 1) {
                $("#minPrice").val(1);
                $("#maxPrice").val(50);
                $('#amount').val("1-50");
            } else if (this.value == 2) {
                $("#minPrice").val(51);
                $("#maxPrice").val(100);
                $('#amount').val("51-100");
            } else if (this.value == 3) {
                $("#minPrice").val(101);
                $("#maxPrice").val(150);
                $('#amount').val("101-150");
            } else if (this.value == 4) {
                $("#minPrice").val(151);
                $("#maxPrice").val(200);
                $('#amount').val("151-200");
            }
            $('#hidden_btn').click();
        }); <?php
        if (isset($CollectionID)) {
            ?>
            setTimeout(function () {
                $('.checked_this').click();
            }, 500); <?php
        } ?>



        $(document).on("input", "#search_tags", function () {
            var v = $(this).val();
            v = v.toLowerCase();
            $('.search-labels').each(function () {
                var str = $(this).attr('data-text');
                var res = str.toLowerCase();

                if (res.indexOf(v) >= 0) {
                    $(this).show();

                } else {
                    $(this).hide();
                }

            });

        });


        $(".get_products").on('click', function (e) {
            // clearing url titles
            // removeTitleToUrl();

            var loadmore = true;
            $(".overlaybg").show();
            $("#loadmore").show();
            if (!$(this).hasClass("loadmore")) {
                loadmore = false;
                if ($(this).hasClass("checked")) {
                    $(this).removeClass("checked");
                } else {
                    $(this).addClass("checked");
                }

                if ($(this).hasClass('radio')) {
                    $('.radio').removeClass("checked");
                    $(this).addClass("checked");
                }
                $("#Page").val(0);
            }

            var TagID = [];
            var OfferID = [];
            var brand_id = <?= (isset($brand_data[0]->brand_id)) ? $brand_data[0]->brand_id :0; ?>;
            var featured = false;
            if ($('#hidden_price').val() == 1) {
                var price = $('#amount').val();
            } else {
                var price = 0;
            }

            var Rating = [];
            var page;
            page = $("#Page").val();
            var h = 0;
            var i = 0;
            var j = 0;
            $(".get_products").each(function () {
                if ($(this).hasClass("checked")) {

                    if ($(this).hasClass("featured")) {
                        featured = true;
                    }

                    if ($(this).hasClass("tags")) {

                        TagID[h] = $(this).val();
                        h = h + 1;
                    }

                    /*if($(this).hasClass("price")){
                       
                        price = $(this).val();
                        
                    }*/

                    if ($(this).hasClass("rating")) {

                        Rating[i] = $(this).val();
                        i = i + 1;
                    }

                    if ($(this).hasClass("offers")) {

                        OfferID[j] = $(this).val();
                        j = j + 1;
                    }

                }
            });
            // add category titles to URL logic here
            var favorite = [];
            $.each($(".get_products:checked"), function () {
                if (typeof $(this).data('url-title') !== 'undefined' && $(this).data(
                        'url-title') != '') {
                    favorite.push($(this).data('url-title'));
                }
            });
            if (favorite.length > 0) {
                var titles = favorite.join("+");
                addTitleToUrl(titles);
            }

            var ul_cls = $('#Product-Listing > .row > .col-md-12 > ul').attr('class');
            var show_no_of_items = $('.ProductsPerPage').val();

            $.ajax({
                type: "POST",
                url: base_url + 'product/getMoreProducts_brand',
                data: {
                    'Featured': featured,
                    'brand_id': brand_id,
                    'TagID': TagID,
                    'OfferID': OfferID,
                    'Rating': Rating,
                    'Price': price,
                    'Page': $("#Page").val(),
                    'ul_cls': ul_cls
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {
                    if (loadmore) {
                        $("#Product-Listing").append(result.html);
                    } else {
                        $("#Product-Listing").html(result.html);
                    }
                    $("#total").html(result.count_product);
                    $("#now").html(result.total_now);
                    if (result.total_now == 0) {
                        $("#now").html(result.count_product);
                    }
                    if (result.page == page) {
                        $("#loadmore").hide();
                    }
                    $("#Page").val(result.page);
                },
                complete: function () {
                    $(".overlaybg").hide();
                    $('.offered_product').tooltip();
                    $('.holder').show();
                    $("div.holder").jPages("destroy").jPages({
                        containerID: "productsContainer",
                        perPage: show_no_of_items > 0 ? show_no_of_items : 6,
                        animation: "fadeInLeft",
                        keyBrowse: true
                    });
                    // $.unblockUI();
                }
            });
        });

        function addTitleToUrl(titles) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname +
                "?q=" + titles;
            window.history.pushState({
                path: newurl
            }, '', newurl);
        }

        function removeTitleToUrl() {
            window.history.replaceState(null, null, "/product");
        }
    });

    $(".sorting").on('change', function (e) {
        $('.overlaybg').fadeIn();
        var sort_val = $(this).val();
        if (sort_val == 'price_highest_to_lowest'); {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'price',
                order: 'desc'
            });
        }
        if (sort_val == 'price_lowest_to_higher') {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'price'
            });
        }
        if (sort_val == 'product_newer_to_older') {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'pid',
                order: 'desc'
            });
        }
        if (sort_val == 'product_older_to_newer') {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'pid'
            });
        }
        setTimeout(function () {
            $('.overlaybg').fadeOut();
        }, 500);
    });

    function sortProducts(sort_val) {
        var current_url = $(location).attr('href');
        url = new URL(window.location.href);
        if (url.searchParams.get('q')) {
            // append sort value at the end
            if (url.searchParams.get('sort')) {
                current_url = removeParam('sort', current_url);
                current_url += "&sort=" + sort_val;
            } else {
                current_url += "&sort=" + sort_val;
            }
        } else {
            // append sort value at the start
            if (url.searchParams.get('sort')) {
                current_url = removeParam('sort', current_url);
                current_url += "sort=" + sort_val;
            } else {
                current_url += "?sort=" + sort_val;
            }
        }
        window.location.href = current_url;
    }

    function removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }
</script>
<script>
    /* when document is ready */
    $(function () {

        /* initiate plugin */
        $("div.holder").jPages({
            containerID: "productsContainer",
            perPage: 6,
            animation: "fadeInLeft",
            keyBrowse: true
        });

        /* on select change */
        $(".ProductsPerPage").change(function () {
            /* get new nº of items per page */
            var newPerPage = parseInt($(this).val());

            /* destroy jPages and initiate plugin again */
            $("div.holder").jPages("destroy").jPages({
                containerID: "productsContainer",
                perPage: newPerPage,
                animation: "fadeInLeft",
                keyBrowse: true
            });
        });

    });

    function changeGridLayout(cls) {

        $('.overlaybg').fadeIn();
        $('#Product-Listing > .row > .col-md-12 > ul').removeClass('items_grid');
        $('#Product-Listing > .row > .col-md-12 > ul').removeClass('items_list');
        setTimeout(function () {
            $('.overlaybg').fadeOut();
            $('#Product-Listing > .row > .col-md-12 > ul').addClass(cls);
        }, 500);
    }

    function myFunction(x) {
        x.childNodes[1].childNodes[1].classList.toggle('bi-plus');
        x.childNodes[1].childNodes[1].classList.toggle('bi-dash');
    }
</script>

<script>
    if ($(window).width() < 601) {
        $("img.only-mob").click(function () {
            $(".dv-hide-desktop").slideToggle("fast");
        });
    } else {
        $(".dv-hide-desktop").hide();
    }
</script>