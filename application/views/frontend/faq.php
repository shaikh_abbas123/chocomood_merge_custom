<style>
h2 {
    font-size: 28px;
    margin: 35px 0 0;
    line-height: 1.2;
}
.content p  {margin:0;}
@media (max-width:767px;) {
    h2 {
        font-size: 22px;
    }
}
</style>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($faqs){ 
                                foreach ($faqs as $key => $value) { ?>
                                    <h2><?php echo $value->Title; ?></h2>
                                    <p><?php echo $value->Answer; ?></p>
                                   
                                <?php }
                            }
                            ?>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
