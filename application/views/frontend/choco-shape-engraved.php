<style type="text/css">
	.accordion>.card:first-of-type {
		border-bottom: 0;
		border-bottom-right-radius: 0;
		border-bottom-left-radius: 0;
	}

	img.choco-bg {
		position: relative;
	}

	.content-type-active:before,
	.fillings-active:before,
	.wrappings-active:before,
	.drawing-active:before,
	.checked:before,
	.text-color-active:before,
	.text-type-active:before {
		height: 7px;
		width: 15px;
		border-bottom: rgb(255, 255, 255) solid 3px;
		border-left: rgb(255, 251, 251) solid 3px;
		content: "";
		display: block;
		transform: rotateZ(-40deg);
		-webkit-transform: rotateZ(-40deg);
		-ms-transform: rotateZ(-40deg);
		position: absolute;
		top: 7px;
		right: 10px;
		z-index: 1;
	}

	.text-type-active {
		height: 200px;
	}

	.content-type-active:after,
	.fillings-active:after,
	.wrappings-active:after,
	.drawing-active:after,
	.checked:after,
	.text-color-active:after,
	.text-type-active:after {
		content: "";
		background-color: #bd9371;
		width: 25px;
		height: 25px;
		position: absolute;
		top: 0;
		right: 5px;
		border-radius: 50%;
	}

	*,
	::after,
	::before {
		box-sizing: border-box;
	}

	::-webkit-scrollbar {
		width: 8px;
	}

	::-webkit-scrollbar-thumb {
		background: #ccc;
		border-radius: 10px;
	}

	::-webkit-scrollbar-track {
		border-radius: 10px;
	}

	.content-type-item input,
	.fillings-item input,
	.wrappings-item input,
	.drawing-item input,
	.color-item input,
	.text-color-item input,
	.text-type-item input {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		opacity: 0;
		cursor: pointer;
		z-index: 10;
	}

	.type-options {
		overflow: hidden;
	}

	#content-output[type="text"],
	#content-output[type="text"]::placeholder {
		color: #BD9371;
		border-color: rgb(189, 147, 113);
		font-size: 16px;
	}

	.form-control {
		display: block;
		width: 100%;
		height: calc(1.5em + .75rem + 2px);
		padding: .375rem .75rem;
		font-size: 1rem;
		font-weight: 400;
		line-height: 1.5;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #CED4DA;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
	}

	ul.drawing-filter-list {
		list-style-type: none;
		overflow: hidden;
		margin-bottom: 0;
		padding-left: 0;
	}


	li.filter-drawing-list-item {
		font-size: 12px !important;
		float: left;
		padding: 0;
		font-size: 15px;
		opacity: 0.5;
		cursor: pointer;
		font-weight: 700;
		border: 1px solid rgb(189 147 113);
		margin-top: 14px !important;
		margin-left: 10px !important;
		border-radius: 19px;
		line-height: 2;
		padding-left: 12px;
		padding-top: 2px;
		height: 35px;
	}

	li.filter-drawing-list-item span {
		float: right;
		display: inline-block;
		background: rgb(189 147 113);
		padding: 1px;
		width: 52px;
		height: 106%;
		text-align: center;
		color: rgb(255 255 255);
		border-radius: 0 19px 19px 0;
		position: relative;
		margin-left: 10px;
		margin-top: -2px;
	}

	li.filter-drawing-list-item span:after {
		content: "";
		position: absolute;
		width: 10px;
		height: 10px;
		background: rgb(189 147 113);
		left: -4px;
		top: 11px;
		transform: rotate(45deg);
	}

	section ul li:last-child {
		margin: unset;
	}

	.transtion,
	.transtion:hover,
	.transtion:focus {
		transition: all .2s .1s ease-in-out;
		-webkit-transition: all .2s .1s ease-in-out;
		-moz-transition: all .2s .1s ease-in-out;
		-ms-transition: all .2s .1s ease-in-out;
		-o-transition: all .2s .1s ease-in-out;
	}

	.text-type-item img,
	.drawing-item img {
		width: 90%;
		height: auto;
	}

	div#filter-drawing {
		max-height: 205px;
		overflow: auto;
	}

	.drawing-item {
		float: left;
		height: 85px;
	}

	.content-type-item,
	.fillings-item,
	.wrappings-item,
	.drawing-item,
	.color-item,
	.text-color-item,
	.text-type-item {
		position: relative;
	}

	.accordion>.card:not(:first-of-type):not(:last-of-type) {
		border-bottom: 0;
		border-radius: 0;
	}

	.accordion>.card {
		overflow: hidden;
	}

	.border-0 {
		border: 0 !important;
	}

	.accordion>.card:not(:first-of-type) .card-header:first-child {
		border-radius: 0;
	}

	.accordion>.card .card-header {
		margin-bottom: -1px;
	}

	div.customize-title {
		border: none;
		padding-left:0;
		padding-right:0;
		padding-top: 14px;
		padding-bottom: 7px;
		cursor: pointer;
		border-bottom: 3px solid #BD9371;
		/* background-color: rgba(204, 204, 204, 0.3); */
	}

	div.customize-title h4 {
		/* color: #BD9371; */
		color: #5f5f5f;

		font-size: 22px;
	}

	div.customize-title h4[aria-expanded="false"]:after {
		content: "+";
		float: right;
		padding-right: 1rem;
		font-size: 23px;
		font-weight: bold;
		margin-top:-5px;
	}

	div.customize-title h4[aria-expanded="true"]:after {
		content: "-";
		float: right;
		padding-right: 1rem;
		font-size: 23px;
		font-weight: bold;
		margin-top:-5px;
	}

	#content-type-view .content-items .view-item {
		width: 250px;
		float: left;
	}

	.position-relative {
		position: relative !important;
	}

	.d-flex {
		display: -ms-flexbox !important;
		display: flex !important;
	}

	h1.customize-chocolate-price {
		/* color: #BD9371; */
		color: #606060;
		font-size: 24px;
	}

	h1 strong {
		display: unset;
		font-weight: 900;
		text-transform: none;
	}

	.mask-item {
		-webkit-mask-image: url("<?= @base_url('assets/frontend/images/Ski_trail_rating_symbol_black_Square-01.png') ?>");
		mask-image: url("<?= @base_url('assets/frontend/images/Ski_trail_rating_symbol_black_Square-01.png') ?>");
		mask-mode: alpha;
		-webkit-mask-mode: alpha;
		mask-repeat: no-repeat;
		mask-size: 100%;
		-webkit-mask-repeat: no-repeat;
		-webkit-mask-size: 100%;
		mask-position: center center;
		-webkit-mask-position: center center;
		width: 40%;
		height: 40%;
	}

	

	.mask-container {
		/* -webkit-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		-moz-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		-ms-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		-o-filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75));
		filter: drop-shadow(2px 3px 4px rgba(0, 0, 0, 0.75)); */
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.card-body {
		-ms-flex: 1 1 auto;
		flex: 1 1 auto;
		padding: 1.25rem;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
	}
	@media (max-width: 575px){
		.content.customize .choco-shape_captionbox .inner {
			left: auto !important;
			right:auto;
		}
		.choco-shape_captionbox h2 {
			font-size: 20px !important;
		}
	}

	@media (max-width: 425px) {
		.order-last-mobile {
			order: 1 !important;
		}
	
		
		/* input#number {
    height: 21px !important;
}
.col-sm-3.col-xs-3.inc-dec-value {
    display: flex !important;
    margin-left: -65px!important;
} */

	}

	.content-type-item {
		height: 130px;
	}

	#content-output[type="file"] {
		height: 100px;
		width: 100px;
		position: relative;
		cursor: pointer;
		border-color: rgb(189, 147, 113);
	}

	#content-output[type="file"]::before {
		content: "+";
		position: absolute;
		top: 0;
		left: 0;
		text-align: center;
		display: flex;
		align-items: center;
		justify-content: center;
		color: rgb(189, 147, 113);
		z-index: 1;
		width: 100%;
		height: 74%;
		font-size: 3rem;
		font-weight: bold;
	}
	
	#content-output[type="file"]::after {
		content: "Your Logo";
		display: block;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0;
		left: 0;
		text-align: center;
		display: flex;
		align-items: center;
		justify-content: center;
		color: rgb(189, 147, 113);
		background-color: white;
		padding-top: 31px;
		font-size: 1.5rem;
	}

	[type=button]:not(:disabled),
	[type=reset]:not(:disabled),
	[type=submit]:not(:disabled),
	button:not(:disabled) {
		cursor: pointer;
	}

	.done button {
		transition: all 0.5s 0.1s ease-in-out;
		-webkit-transition: all 0.5s 0.1s ease-in-out;
		-moz-transition: all 0.5s 0.1s ease-in-out;
		-ms-transition: all 0.5s 0.1s ease-in-out;
		-o-transition: all 0.5s 0.1s ease-in-out;
		width: 195px;
		height: 53px;
		color: white;
		/* background-color: #4B4360; */
		display: flex;
		text-align: center;
		align-items: center;
		justify-content: center;
		/* border-radius: 30px; */
		padding-bottom: 4px;
		border: none;
		outline: none !important;
		float: right;
		background-color: #bd9371;
	}

	li.filter-drawing-list-item:hover {
		opacity: 1;
	}

	#colors .color-items .color-item:first-of-type {
		background-color: #59899F;
	}

	#colors .color-items .color-item {
		position: relative;
		width: 50px;
		height: 50px;
		float: left;
		margin: 2px;
	}

	#colors .color-items .color-item input {
		cursor: pointer;
	}

	#colors .color-items .color-item input {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		opacity: 0;
		z-index: 1;
	}

	span.d-inline-block {
		background-color: red;
	}

	a:link.tooltip_pop {
		color: #5f5f5f;
		background-color: transparent;
		text-decoration: none;
	}

	/* /////////////////////////////////// */

	.value-button {
		display: inline-block;
		border: 1px solid #ddd;
		margin: 0px;
		width: 40px;
		height: 20px;
		text-align: center;
		vertical-align: middle;
		padding: 11px 0;
		background: #eee;
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	.value-button:hover {
		cursor: pointer;
	}

	#decrease,
	#increase {
		border-radius: 50px;
		line-height: 0;
		width: 25px;
		background: transparent;
	}

	/* form #increase {
  margin-left: -4px;
  border-radius: 0 8px 8px 0;
} */

	form #input-wrap {
		margin: 0px;
		padding: 0px;
	}

	input#number {
		text-align: center;
		border: none;
		margin: 0px;
		width: 40px;
		height: 40px;
		font-weight: 900;
		font-size: 20px;
		display: inline-block;
		vertical-align: middle;
	}

	input[type=number]::-webkit-inner-spin-button,
	input[type=number]::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}


	/* #colors .color-items .color-item:nth-of-type(2) {
      background-color: #FE4E3C;
  }
  #colors .color-items .color-item:nth-of-type(3) {
      background-color: #DC2014;
  }
  #colors .color-items .color-item:nth-of-type(4) {
      background-color: #FD692B;
  }
  #colors .color-items .color-item:nth-of-type(5) {
      background-color: #EFBF01;
  }
  #colors .color-items .color-item:nth-of-type(6) {
      background-color: #72963F;
  }
  #colors .color-items .color-item:nth-of-type(7) {
      background-color: #AB5850;
  }
  #colors .color-items .color-item:nth-of-type(8) {
      background-color: #F1D689;
  }
  #colors .color-items .color-item:last-of-type {
      background-color: #521B07;
  } */
  
  
  .bg-custom-header {
    min-height: 250px;
    margin: 0px 0px 0px 0px;
    max-height: 100%;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}
div.customize-title h4 {
	color: #32231F;
    font-size: 18px;
    text-transform: uppercase;
    letter-spacing: 5px;
    font-weight: 600;
}
.new-btn-added{
	padding: 13px 10px !important;
    width: 30px !important;
    height: 30px !important;
    background-color: #F4EBD3 !important;
    border: #F4EBD3 !important;
    color: #BD9371 !important;
    box-shadow: #929292 0px 2px 7px !important;
    font-size: 20px !important;
    border-radius: 4px !important;
}
.row {
    margin-left: unset;
    margin-right: unset;
}
span.price_rate {
    padding: 0 20px;
	color: #606060;
}
h6.kg_quantity {
    display: inline-block;
    padding: 0 0px;
	vertical-align: text-top;
	color: #606060;
}
.count_quantity {
    display: inline-block;
    padding: 0 20px;
	color: #606060;
}
input[type=number] {-moz-appearance: textfield;}
input[type=file] {
  -moz-appearance:initial 
}
.choco_msg-step
{
	font-size: 20px;
    color: #32231F;
    display: inline-block;
    letter-spacing: 2px;
}
@media (min-width: 500px) {
	.pt-100{
		padding-top: 100px;
	}
}
.filter-drawing-list-item[aria-expanded="true"] span,
.filter-drawing-list-item[aria-expanded="true"] span:after
{
	background-color: purple;
}
.filter-drawing-list-item[aria-expanded="true"]
{
	opacity: 1;
	color:purple;
	border-color: purple;
	border-bottom-color:purple ;
}
.wrapper_shape
{
	mix-blend-mode: multiply;
    position: absolute;
    left: 0;
	right: 0;
	top: 0;
	bottom: 0;
    height: auto;
    width: 75px;
    height: 75px;
	margin: auto;
	
}
</style>
<?php $choco_shape = getPageContent(12, $lang) ?>
<?php 
// print_rm(base_url('uploads/images/test.png'));
$header_url = base_url('uploads/images/test.png');
?>
<section class="content titlarea">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Choco Shape Engraved</h2>
                    <ul>
						<li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
						<li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
						<li>Choco Shape Engraved</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
	
<section class="content products ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 choco-msg_border-bot">
                <span class="choco_msg-step">
                    <b>Step 2:</b>
                </span>
                <span class="choco_msg-step">
                    Please select from below
                </span>
            </div>
        </div>
    </div>
</section>

<section class="customize-shape container content single-products" id="customize-chocolate-shape" style="padding-top:40px;">
	<div class="row">
		<form action="<?= base_url('cart/addCustomizedShapeToCart')?>" method="POST" id="addCustomizedShapeToCart">
			<!-- start customize options -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 order-last-mobile">
				<!-- this form to send the customer to customize to database -->

				<!-- start customize options accordion -->
				<div class="accordion" id="customize-chocolate-tabs">
					<!-- start content type option -->
					<div id="content-type" class="card border-0">
						<!-- title -->
						<div class="card-header customize-title" id="headingOne">
							<h4 class="mb-0 mt-0" data-toggle="collapse" data-target="#type" aria-expanded="false"
								aria-controls="type">
								Content Type &nbsp;
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover"
									data-content="And here's some amazing content. It's very engaging. right?" ><i
										class="far fa-question-circle"></i></a>

							</h4>
						</div>
						<!-- content -->
						<div id="type" class="collapse" aria-labelledby="headingOne"
							data-parent="#customize-chocolate-tabs">
							<!-- content container-->
							<div class="card-body pl-0 row">
								<?php
                              foreach ($content_types as $key => $content_type) {
                              
                           ?>
								<!-- content item -->
								<div data-toggle="tooltip" data-placement="top" title="<?= $content_type->Title?>" class="col-4 p-0 pr-0 pl-0 content-type-item <?= $key == 0 ?'content-type-active':'' ?>"
									style="">
									<!-- content image -->
									<img class="choco-bg"  src="<?php echo base_url($content_type->Image); ?>"
										width="100%" alt="" >
									<!-- <div class="mask-container">
                                    <img class="mask-item" src="" data-mask="http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png"
                                       width="500px" height="500px" alt=""
                                       style="-webkit-mask-image:url(http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png)!important;mask-image:url(http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png)!important;background-color: rgb(253, 105, 43);">
                                 </div> -->
									<!-- this input save data which will send to the database after customer press Done button -->
									<!-- this is text content type  -->
									<input type="radio" class="change_price" name="content-type" value="<?= $content_type->ContentTypeID?>" data-price="<?= $content_type->Price?>"
										<?= $key == 0 ?'checked':'' ?> data-image="<?php echo base_url($content_type->Image); ?>">
								</div>
								<?php
                              }
                              ?>

								<div class="col-12 pt-100">
									<div class="type-options pl-0 pt-3 pr-3 pb-3">
										<!-- <div class="input-group"> -->
										<div style="display:flex">
											<input style="display:inline" class="form-control" type="text" name="customer-content"
												id="content-output" placeholder="Your Name Here" maxlength="16">
											<img id="uploadedImage" class="ml-4" src="#" alt="Uploaded Image" accept="image/png, image/jpeg" style="display:none;width:100px">
										</div>
										<label for="customer-content" id="customer-content-img-label" class="custom-file-upload">
												<strong style="color:red;">*</strong> Required
										</label>
										<label id="customer-content-img-label-2" >
												<strong style="color:red;">Note:</strong> Image should be black color with transparent background, dimension 150 x 150.
										</label>
										<span id="content-output-description"><strong
												style="color:red;">*</strong><small>max character 16</small></span>
										<!-- <a onclick="addInput()" id="add-input-button"><i class="fas fa-plus"></i></a> -->
										<!-- </div> -->
										<div id="drawing" class="card-body pl-2 p-0 row" style="display: none;">
											<ul class="drawing-filter-list mb-4">
												
												<?php
												$drawingActive =1;
                                        foreach($customize_text_types as $key =>$value){
                                       ?>
												<li class="filter-drawing-list-item transtion mb-0" 
													data-toggle="collapse"
													data-target="#<?=preg_replace('/\s+/', '', @$value->Title);?>"
													aria-expanded="<?= ($drawingActive == 1)?'true':'false'?>"
													aria-controls="<?=preg_replace('/\s+/', '', @$value->Title);?>">
													<?= limit_string(@$value->Title, 100) ?>
													<span><?= count(get_images($value->ContentTypeSubID, 'content_type_sub_image'))?></span>
												</li>
												<?php
												$drawingActive =0;
                                        }
                                        ?>
											</ul>
											<div class="accordion" id="filter-drawing">
												<?php
                                        $drawingActive =1;
                                        foreach($customize_text_types as $key =>$value){
                                       ?>
												<div id="<?=preg_replace('/\s+/', '', @$value->Title);?>"
													class=" transtion <?= $drawingActive == 1?'':'collapse' ?>" aria-labelledby="headingOne"
													data-parent="#filter-drawing">
													<!-- content item -->
													<?php
                                            foreach(get_images($value->ContentTypeSubID, 'content_type_sub_image') as $subKey => $image){
                                             ?>
													<div data-toggle="tooltip" data-placement="top" title="<?= $value->Title?>"
														class="col-2 drawing-item <?= $drawingActive == 1?'drawing-active':'' ?>">
														<!-- content image -->
														<div class="content-svg">
															<div class="mask-container">
																<img class="" src="<?= @base_url($image->ImageName) ?>"
																	style="border: 1px solid #8080807d;"
																	width="500px" height="500px" alt=""
																	>
															</div>
														</div>
														<input type="radio" name="drawing" class="change_price" data-price="<?= $value->ContentTypeSubID ?>" data-img="<?= @base_url($image->ImageName) ?>"
															value="<?= $value->ContentTypeSubID ?>,<?= $image->SiteImageID ?>"
															<?= $drawingActive == 1?'checked':'' ?>>
													</div>
													<?php
                                             $drawingActive =0;
                                            }
                                            ?>
												</div>
												<?php
                                        }
                                        ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end content type option -->
					<!-- start text Type option -->
					<div id="text-type" class="card border-0" style="">
						<!-- title -->
						<div class="card-header customize-title" id="textType">
							<h4 class="mb-0 mt-0" data-toggle="collapse" data-target="#textTypeTab"
								aria-expanded="false" aria-controls="textTypeTab">
								Text Type &nbsp;
								<a id="pop" class="tooltip_pop" href="#" data-toggle="popover"
									data-content="2And here's some amazing content. It's very engaging. right?"><i
										class="far fa-question-circle"></i></a>
							</h4>
						</div>
						<!-- content -->
						<div id="textTypeTab" class="collapse" aria-labelledby="textType"
							data-parent="#customize-chocolate-tabs" style="height: 0;">
							<div class="card-body pl-0 row panel-body">
								<?php
                              foreach($text_types as $key => $text_type){
                            ?>
								<div class="col-4 p-0 text-type-item  <?= $key == 0 ?'text-type-active':'' ?>" data-toggle="tooltip" data-placement="top" title="<?= $text_type->Title?>">
									<div class="mask-container">
										<img class=""
											src="<?= @base_url($shapeDetail->ShapeImage) ?>"
											style="background-image:url('<?= @base_url(get_images($text_type->ContentTypeSubID, 'content_type_sub_image')[0]->ImageName) ?>')!important;background-size: 50%;background-repeat: no-repeat;background-position:center;"
											width="500px" height="500px" alt=""
											>
									</div>

									
									<input type="radio" class="change_price" data-price="<?=@$text_type->Price ?>" name="textType" value="<?=@$text_type->ContentTypeSubID?>"
										<?= $key == 0 ?'checked':'' ?>>
								</div>
								<?php 
                              }
                            ?>
							</div>
						</div>
					</div>
					<!-- end text Type option -->
					<!-- start color option -->
					<div id="colors" class="card border-0">
						<!-- title -->
						<div class="card-header customize-title" id="headingTwo">
							<h4 class="mb-0 mt-0" data-toggle="collapse" data-target="#collapseTwo"
								aria-expanded="false" aria-controls="collapseTwo">
								Fill Color &nbsp;
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover"
									data-content="And here's some amazing content. It's very engaging. right?"><i
										class="far fa-question-circle"></i></a>
							</h4>
						</div>
						<!-- content -->
						<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
							data-parent="#customize-chocolate-tabs">
							<div class="card-body pl-0 color-items">
								<!-- content item -->
								<!-- you can choose multi-color -->
								<?php
                                foreach($fillColor as $key => $color)
                                {
                              ?>
								<div class="color-item float-left mr-3" data-toggle="tooltip" data-placement="top" title="<?= $color->title?>"
									style="background-color: <?= @$color->code ?>;">
									<input type="radio" class="colors checked change_price" data-price="<?= @$color->price ?>" id="colors<?= $key?>"
										data-color="<?= @$color->code ?>" name="fill_color"
										value="<?= @$color->FillColorId ?>">
										<img src="<?php echo base_url($shapeDetail->ShapeImage); ?>" width="auto" height="auto" alt="">
								</div>
								<?php
                                }
                              ?>
							</div>
						</div>
					</div>
					<!-- end color option -->
					<!-- start fillngs option -->
					<div class="card border-0">
						<!-- title -->
						<div class="card-header customize-title" id="headingThree">
							<h4 class="mb-0 mt-0" data-toggle="collapse" data-target="#collapseThree"
								aria-expanded="false" aria-controls="collapseThree">
								Fillings &nbsp;
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover"
									data-content="And here's some amazing content. It's very engaging. right?"><i
										class="far fa-question-circle"></i></a>
							</h4>
						</div>
						<!-- content -->
						<div id="collapseThree" class="collapse" aria-labelledby="headingThree"
							data-parent="#customize-chocolate-tabs">
							<!-- content container -->
							<div class="card-body pl-0 row">
								<?php
                                  foreach ($fillings as $key => $filling) {
                              ?>
								<!-- content item -->
								<div class="col-3 p-0 fillings-item <?= $key == 0 ? 'fillings-active': ''?>" data-toggle="tooltip" data-placement="top" title="<?= $filling->title?>">
									<!-- fillings image -->
									<img src="<?php echo base_url($filling->image); ?>" width="100%" alt="">
									<!-- this input save data which will send to the database after customer press Done button -->
									<input type="checkbox" class="change_price" data-price="<?= @$filling->price;?>" name="fillings[]" value="<?= @$filling->FillingId ?>"
										<?= $key == 0 ? 'checked': ''?>>
								</div>
								<?php
                              }
                              ?>
							</div>
						</div>
					</div>
					<!-- end fillngs option -->
					<!-- start Wrappings option -->
					<div class="card border-0" style="display:none">
						<!-- title -->
						<div class="card-header customize-title" id="headingThree">
							<h4 class="mb-0 mt-0" data-toggle="collapse" data-target="#collapseFour"
								aria-expanded="false" aria-controls="collapseFour">
								Wrappings &nbsp;
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover"
									data-content="And here's some amazing content. It's very engaging. right?"><i
										class="far fa-question-circle"></i></a>
							</h4>
						</div>
						<!-- content -->
						<div id="collapseFour" class="collapse" aria-labelledby="headingThree"
							data-parent="#customize-chocolate-tabs">
							<div class="card-body pl-0 row">
								<?php
                              foreach($wrappings as $key => $wrappping){
                           ?>
								<!-- content item -->
								<div class="col-2 p-0 wrappings-item <?= $key == 0 ? 'wrappings-active': ''?>" data-toggle="tooltip" data-placement="top" title="<?= $wrappping->Title?>">
									<!-- content image -->
									<?php 
									$found_key = array_search($wrappping->WrappingID, array_column($wrappings_en, 'WrappingID'));
									$wrap_check = 0;
									$display = 0;
									if(trim(strtolower($wrappings_en[$found_key]->Title)) == "no wrapping")
									{
										$display = 1;
									}
										if(@getimagesize(base_url($wrappping->WrappingImage))){
											$wrap_check = 1;
											?>
											<img src="<?= base_url($wrappping->WrappingImage); ?>" style="<?= ($display == 1)?'display:none':''?>" width="100px" alt="">
											<?php
										}
									?>
									
									<img class="wrapper_shape" src="<?php echo base_url($shapeDetail->ShapeImage); ?>" style="<?= ($wrap_check == 0)?'opacity:1':'opacity:0.5'?>">
									<!-- this input save data which will send to the database after customer press Done button -->
									<input type="checkbox" class="change_price" data-price="<?= @$wrappping->WrappingPrice ?>" name="wrappings" value="<?= @$wrappping->WrappingID ?>"
									<?= $key == 0 ? 'checked': ''?>>
								</div>
								<?php } ?>
								
							</div>
						</div>
					</div>
					<!-- end wrappings option -->
					<!-- start Powder color option -->
					<div class="card border-0">
						<!-- title -->
						<div class="card-header customize-title" id="textColor">
							<h4 class="mb-0 mt-0" data-toggle="collapse" data-target="#textColorTab"
								aria-expanded="false" aria-controls="textColorTab">
								Powder Color &nbsp;
								<a id="pop" href="#" class="tooltip_pop" data-toggle="popover"
									data-content="And here's some amazing content. It's very engaging. right?"><i
										class="far fa-question-circle"></i></a>
							</h4>
						</div>
						<!-- content -->
						<div id="textColorTab" class="collapse" aria-labelledby="textColor"
							data-parent="#customize-chocolate-tabs">
							<div class="card-body pl-0 row">
								<?php
                              foreach($powder_colors as $key => $powder_color){
                           ?>
								<!-- content item -->
								<div class="col-2 p-0 text-color-item " data-toggle="tooltip" data-placement="top" title="<?= $powder_color->title?>">
									<!-- content image -->
									<?php
										$style="";
										if(trim(strtolower($shapeDetail_en->Title)) == "parallelogram")
										{
											$style .= "background-position: left 23px center;background-size: 83px;";
										}
									?>
									<img src="<?php echo base_url($shapeDetail->ShapeImage); ?>" style="background-image:url('<?= base_url($powder_color->image); ?>');<?= $style?>" width="100px" height="100px" alt="">
									<!-- this input save data which will send to the database after customer press Done button -->
									<input type="radio" name="powderColor" class="change_price" data-price="<?= $powder_color->price;?>" value="<?= @$powder_color->PowderColorId?>" data-color="<?= @$powder_color->colorCode?>"
										id="powderColor" >
								</div>
								<?php
                              }
                              ?>
							</div>
						</div>
					</div>
					<!-- end Powder color option -->
				</div>
				<!-- end customize option acordion -->
				<!-- start submit button form -->
				<div class="col-md-12 done mt-3 pl-0 mb-3">
					<!-- <button style="float:right; background-color:#bd9371;" type="submit">Preview</button> -->
					<button style="" type="submit">Preview</button>

				</div>
				<!-- end submit button form -->

			</div>
			<!-- start customize option -->
			<!-- start customize View -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 content customize-shape-details" id="content-type-view">
				<!-- customize shape items color view contauner -->
				<div class="content-items d-flex justify-content-center">
					<!-- customize shape item-->
					<div class="view-item position-relative p-0 d-flex justify-content-center" id="shape-item" style="position: relative;height: 250px;margin-bottom:50px">
						<!-- customize shape item img -->
						<?php
						$style="position: absolute;";
						if(trim(strtolower($shapeDetail_en->Title)) == "parallelogram")
						{
							$style .= "width:100px;left:111px;top:65px";
						}else
						{
							$style .= "top:50px;";
						}
						?>
						<img class="choco-bg" id="choco-content-type" src="<?= base_url($content_types[0]->Image)?>" width="150px" height="auto" alt="" style="<?= $style; ?>"/>
						<img class="choco-bg" id="shape_image" src="<?php echo base_url($shapeDetail->ShapeImage); ?>" width="250px"
							height="auto" alt="" style="position: absolute;">
						
						
						<!-- <img class="mask-item-main-shape choco-bg" id="choco-customize-shape" src="" data-mask="<?php echo base_url($shapeDetail->ShapeImage); ?>" width="250px" height="auto" alt="" style="-webkit-mask-image:url(<?php echo base_url($shapeDetail->ShapeImage); ?>)!important;mask-image:url(<?php echo base_url($shapeDetail->ShapeImage); ?>)!important;"> -->
						<!-- <div class="icon icon-red">
                        <img src="../img/Ski_trail_rating_symbol_black_circle.png" alt="">
                        </div> -->
						<!-- <div class="mask-container">
                        <img class="mask-item" src="" width="500px" height="500px" alt=""
                           style="-webkit-mask-image:url(http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png)!important;mask-image:url(http://localhost/chocomood_merge_custom/assets/frontend/images/Ski_trail_rating_symbol_black_circle.png)!important;">
                     </div> -->
					</div>
				</div>
				<div style="clear: both;"></div>
				<h1 class="customize-chocolate-price text-center">Price:
					<span class="price_rate">
						<strong id="price_rate_custom"><?= floatval($content_types[0]->Price)+floatval(@$shapeDetail->PricePerKG).' '?></strong>
						<small>SAR / KG</small>
					</span>
				</h1>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-center">
						<h6 class="kg_quantity ">KG</h6>
						<div class="count_quantity">
							<!-- <input id="after" name="Quantity" class="form-control Quantity" type="number" value="<?= @$shapeDetail->MinimumOrder?>" max="<?= @$shapeDetail->MaximumOrder?>" min="<?= @$shapeDetail->MinimumOrder?>" /> -->
							<div class="value-button new-btn-added" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
								<!-- <input type="number" id="number" value="0" /> -->
								<input id="number" name="Quantity" type="number"
									value="<?= @$shapeDetail->MinimumOrder?>"  data-max="<?= @$shapeDetail->MaximumOrder?>"
									data-min="<?= @$shapeDetail->MinimumOrder?>" />
							<div class="value-button new-btn-added" id="increase" onclick="increaseValue()" value="Increase Value">+</div>

							<input type="hidden" class="shapeID" name="shapeID" value="<?php echo $shapeDetail->ShapeID; ?>">
							<input type="hidden" class="ProductPrice" name="ProductPrice" value="<?php echo $shapeDetail->PricePerPcs; ?>">
							<input type="hidden" class="ItemType" name="ItemType" value="Choco Shape">
							<input type="hidden" value="<?= $shapeDetail->PricePerKG?>" id="price_per_kg"> 
						</div>	  
					</div>
<!-- 					
					<div class="col-md-3 col-sm-7 col-xs-7 ">
						
					</div> -->
				</div>
			</div>
		</form>
	</div>
	<!-- end customize option -->
	<!-- customer options -->
</section>
<script src="<?php echo front_assets(); ?>js/hex_convert.js" ></script>
<!-- end customize shape content -->
<script>
	$(document).ready(function(){
		$("input[name='textType']").trigger('click');
		$(document).on('click', '.change_price', function () {
			var price = parseFloat($('#price_per_kg').val());
			var content_type = $('input[name="content-type"]:checked').val();
			price += parseFloat($("input[name='content-type']:checked").data('price'));
			if(content_type == 4)
			{
				price += parseFloat($("input[name='textType']:checked").data('price'));
			}
			else if(content_type == 5)
			{
				price += parseFloat($("input[name='drawing']:checked").data('price'));
			}
			var powder = $("input[name='powderColor']:checked").data('price');
			if(powder != undefined || powder > 0)
			{
				price += powder;
			}
			$("#price_rate_custom").text(price);
		});
		document.getElementById('content-output').addEventListener('change', function(){
		if (this.files[0] ) {
			var picture = new FileReader();
			picture.readAsDataURL(this.files[0]);
			picture.addEventListener('load', function(event) {
				document.getElementById('choco-content-type').setAttribute('src', event.target.result);
			document.getElementById('uploadedImage').setAttribute('src', event.target.result);
			document.getElementById('uploadedImage').style.display = 'inline';
			});
		}else
		{
			document.getElementById('uploadedImage').style.display = 'none';
		}
		});

		document.getElementById("customer-content-img-label").style.display = 'none';
		document.getElementById("customer-content-img-label-2").style.display = 'none';
		
	})
	$(document).ready(() => {
		if (document.getElementById("customize-chocolate-print") != null || document.getElementById(
				"customize-chocolate-shape") != null) {
			var contentType = document.getElementById("content-type"),

				drawing = document.getElementById("drawing"),
				contentTypeItems = contentType.getElementsByClassName("content-type-item"),
				drawingItems = drawing.getElementsByClassName("drawing-item"),
				colorsChecked = document.getElementsByClassName("color-item"),
				contentTypeView = document.getElementById("content-type-view"),
				contentTypeViewItem = contentTypeView.children[0].children,
				contentTypeViewImage = "",
				color = '#FD692B',
				powderColor = "",
				contentInput = document.getElementsByClassName(
					"customize-shape-item-content-view"
				),
				contentOutput = document.getElementById("content-output");


			//content Type
			for (var i = 0; i < contentTypeItems.length; i++) {
				contentTypeItems[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("content-type-active"),
						contentTypeItemsImg = this.querySelector("Img");
					current[0].className = current[0].className.replace(" content-type-active", "");
					this.className += " content-type-active";
					if (this.querySelector("input[type=radio]:checked").value == "4") {
						document.getElementById("text-type").style.display = "block"
					} else {
						if (document.getElementById("text-type")) {
							document.getElementById("text-type").style.display = "none"
						}

					}
					document.querySelector('#shape_image').src = '<?= base_url($shapeDetail->ShapeImage)?>'
					if (this.querySelector("input[type=radio]:checked").value == "4") {
						document.querySelector('#choco-content-type').src = document.querySelector('input[type=radio]:checked').dataset.image;
						// document.querySelector('#shape_image').src = '<?= base_url($shapeDetail->shape_content_image_1)?>'
						document
							.getElementById("content-output")
							.setAttribute("type", "text");
							document.getElementById('uploadedImage').style.display = 'none';
						document.getElementById("content-output").style.display = 'inline';
						$('#content-output-description small').text('Max character 16');
						$("#content-output").attr('maxlength','16');
						document.getElementById('content-output-description').style.display = 'block';
						document.getElementById("customer-content-img-label").style.display = 'none';
						document.getElementById("customer-content-img-label-2").style.display = 'none';
						
					} else if (this.querySelector("input[type=radio]:checked").value == "5") {
						// console.log($('input[name="drawing"]').is(":checked").data('img'));
						$('#choco-content-type').attr('src',$('input[name="drawing"]:checked').data('img'));
						
						$("#content-output").attr('maxlength','12');
						document
							.getElementById("content-output")
							.setAttribute("type", "text");
						document
							.getElementById("content-output").style.display = 'inline';
							document.getElementById('uploadedImage').style.display = 'none';
						$('#content-output-description small').text('Max character 12');
						
						document.getElementById('content-output-description').style.display = 'block';
						document.getElementById("customer-content-img-label").style.display = 'none';
						document.getElementById("customer-content-img-label-2").style.display = 'none';
						
					} else {
						document.querySelector('#choco-content-type').src = document.querySelector('input[type=radio]:checked').dataset.image;
						// document.querySelector('#shape_image').src = '<?= base_url($shapeDetail->shape_content_image_3)?>'
						document
							.getElementById("content-output")
							.setAttribute("type", "file");
						document
							.getElementById("content-output").style.display = 'inline';
							document.getElementById('content-output-description').style.display = 'none';
							document.getElementById("customer-content-img-label").style.display = 'block';
							document.getElementById("customer-content-img-label-2").style.display = 'block';
							$('#customer-content-img-label').empty().append("<strong style='color:red;'>*</strong>Required");	
							$('#content-output').change(function() {
								if($('#content-output')[0].files.length == 0)
								{
									$('#customer-content-img-label').empty().append("<strong style='color:red;'>*</strong>Required");	
								}else
								{
									var file = $('#content-output')[0].files[0].name;
									$('#customer-content-img-label').text(file);
								}
								
							});
					}

					if (this.querySelector("input[type=radio]:checked").value == "5") {
						drawing.style.display = "block";

						for (ci = 0; ci < contentInput.length; ci++) {
							contentInput[ci].style.backgroundImage = "unset";
						}
					} else {
						drawing.style.display = "none";
					}
				});
			}

			// $(document).on('click','.drawing-item',function(){
			//   checkInput = $(this).find('input[type=checkbox]');
			//   if($(this).hasClass('drawing-active'))
			//   {
			//     $(this).removeClass('drawing-active')
			//     $(checkInput).prop('checked',false);
			//   }else
			//   {
			//     $(this).addClass('drawing-active')
			//     $(checkInput).prop('checked',true);
			//   }
			// })
			for (var di = 0; di < drawingItems.length; di++) {
				drawingItems[di].addEventListener("click", function () {
					var current = document.getElementsByClassName("drawing-active");

					current[0].className = current[0].className.replace(" drawing-active", "");
					this.className += " drawing-active";

				});
			}
			
			$(document).on("click", "input[name='drawing']", function () {
				$('#choco-content-type').attr('src',$('input[name="drawing"]:checked').data('img'));
			});
			// colors
			$(document).on("change", ".colors", function () {
				$('.colors').parent('div').removeClass('text-color-active');
				if ($(this).is(":checked")) {
					color = $(this).attr('data-color');
					$(this).parent('div').addClass('text-color-active');
				}
				$('#shape-item').css('background-color', color);
				// drawingChoose = drawing.querySelector(".drawing-active").children[0]
				//   .children[0],
				//   drawingChooseActive = drawingChoose.cloneNode(true);

			});

			// for (var i = 0; i < colorsChecked.length; i++) {
			//   colorsChecked[i].addEventListener("click", function () {
			//     var checked = document.getElementsByClassName("checked");
			//     checked[0].className = checked[0].className.replace(" checked", "");
			//     this.className += " checked";
			//     this.className = this.className.replace(" checked", "");
			//   });
			// }



			// start fillings
			$(document).on('click', '.fillings-item', function () {
				// $('#collapseThree').find('.fillings-item').removeClass('fillings-active');
				// $('.fillings-item').find('input').prop('checked', false);
				checkInput = $(this).find('input[type=checkbox]');
				if ($(this).hasClass('fillings-active')) {
					$(this).removeClass('fillings-active')
					$(checkInput).prop('checked', false);
				} else {
					$(this).addClass('fillings-active')
					$(checkInput).prop('checked', true);
				}
			})

			//  start wrappings
			$(document).on('click', '.wrappings-item', function () {
				// $('#collapseFour').find('.wrappings-item').removeClass('wrappings-active');
				// $('.wrappings-item').find('input').prop('checked', false);
				checkInput = $(this).find('input[type=checkbox]');
				if ($(this).hasClass('wrappings-active')) {
					$(this).removeClass('wrappings-active')
					$(checkInput).prop('checked', false);
				} else {
					$('.wrappings-item').removeClass('wrappings-active')
					$(this).addClass('wrappings-active')
					$(checkInput).prop('checked', true);
				}
			})



			//  start text type

			var textType = document.getElementsByClassName("text-type-item");
			for (var i = 0; i < textType.length; i++) {
				textType[i].addEventListener("click", function () {
					var current = document.getElementsByClassName("text-type-active");

					current[0].className = current[0].className.replace(
						" text-type-active",
						""
					);
					this.className += " text-type-active";

				});
			}


			//  start text color

			// var textColor = document.getElementsByClassName("text-color-item");
			// for (var i = 0; i < textColor.length; i++) {
			// 	textColor[i].addEventListener("click", function () {
			// 		var current = document.getElementsByClassName("text-color-active");
			// 		current[0].className = current[0].className.replace(
			// 			" text-color-active",
			// 			""
			// 		);
			// 		this.className += " text-color-active";
			// 		// console.log('color click')
			// 	});
			// }

			$(document).on('click', '.text-color-item', function () {
				// $('#collapseThree').find('.fillings-item').removeClass('fillings-active');
				// $('.fillings-item').find('input').prop('checked', false);
				checkInput = $(this).find('input[type=radio]');
				if ($(this).hasClass('text-color-active')) {
					$(this).removeClass('text-color-active')
					$(checkInput).prop('checked', false);
				} else {
					$('.text-color-item').removeClass('text-color-active')
					$('.text-color-item').find('input').prop('checked', false);
					$(this).addClass('text-color-active')
					$(checkInput).prop('checked', true);
				}
			})

		}
	})

	//uses classList, setAttribute, and querySelectorAll
	//if you want this to work in IE8/9 youll need to polyfill these
	// (function(){
	//     var d = document,
	//     accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
	//     setAria,
	//     setAccordionAria,
	//     switchAccordion,
	//   touchSupported = ('ontouchstart' in window),
	//   pointerSupported = ('pointerdown' in window);

	//   skipClickDelay = function(e){
	//     e.preventDefault();
	//     e.target.click();
	//   }

	//         setAriaAttr = function(el, ariaType, newProperty){
	//         el.setAttribute(ariaType, newProperty);
	//     };
	//     setAccordionAria = function(el1, el2, expanded){
	//         switch(expanded) {
	//       case "true":
	//         setAriaAttr(el1, 'aria-expanded', 'true');
	//         setAriaAttr(el2, 'aria-hidden', 'false');
	//         break;
	//       case "false":
	//         setAriaAttr(el1, 'aria-expanded', 'false');
	//         setAriaAttr(el2, 'aria-hidden', 'true');
	//         break;
	//       default:
	//           break;
	//         }
	//     };
	// //function
	// switchAccordion = function(e) {
	//   console.log("triggered");
	//     e.preventDefault();
	//     var thisAnswer = e.target.parentNode.nextElementSibling;
	//     var thisQuestion = e.target;
	//     if(thisAnswer.classList.contains('is-collapsed')) {
	//         setAccordionAria(thisQuestion, thisAnswer, 'true');
	//     } else {
	//         setAccordionAria(thisQuestion, thisAnswer, 'false');
	//     }
	//     thisQuestion.classList.toggle('is-collapsed');
	//     thisQuestion.classList.toggle('is-expanded');
	//         thisAnswer.classList.toggle('is-collapsed');
	//         thisAnswer.classList.toggle('is-expanded');

	//     thisAnswer.classList.toggle('animateIn');
	//     };
	//     for (var i=0,len=accordionToggles.length; i<len; i++) {
	//         if(touchSupported) {
	//       accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
	//     }
	//     if(pointerSupported){
	//       accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
	//     }
	//     accordionToggles[i].addEventListener('click', switchAccordion, false);
	//   }
	// })();
	$('.card-header').click(function (e) {
		e.preventDefault();
		var myGroup = $('.accordion');
		myGroup.find('.collapse.in').collapse('hide');
	});

	// $('.wrappings-item').click(function (e) {
	// 	e.preventDefault();
	// 	console.log('bilal')
	// 	let temp = $('#collapseFour');
	// 	temp.find('.wrappings-item.wrappings-active').removeClass('wrappings-active');
	// 	$('.wrappings-item').find('input').attr('checked', false);
	// 	$(this).find('input').attr('checked', true);
	// });



	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip({
			'placement': 'top'
		});

		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			'placement': 'right'
		});

	});


	function increaseValue() {
		var value = parseInt(document.getElementById('number').value, 10);
		var dataMax = document.getElementById('number').dataset.max;
		value = isNaN(value) ? 0 : value;
		value++;
		if(value <= dataMax)
		{
			$('.price_rate strong').text($('#price_per_kg').val()*value);
			document.getElementById('number').value = value;
		}
	}

	function decreaseValue() {
		var value = parseInt(document.getElementById('number').value, 10);
		var dataMin = document.getElementById('number').dataset.min;
		value = isNaN(value) ? 0 : value;
		value < 1 ? value = 1 : '';
		value--;
		if(value >= dataMin)
		{
			$('.price_rate strong').text($('#price_per_kg').val()*value);
			document.getElementById('number').value = value;
		}
	}
	//convert color for svg
	'use strict';


$(document).ready(() => {
	// $('input[name="powderColor"]').trigger('click');
  $('input[name="powderColor"]').click(() => {
    const rgb = hexToRgb($('input[name="powderColor"]:checked').data('color'));
    if (rgb.length !== 3) {
      return;
    }

    const color = new Color(rgb[0], rgb[1], rgb[2]);
    const solver = new Solver(color);
    const result = solver.solve();
	var style= "position: absolute;";
	
	<?php
	if(trim(strtolower($shapeDetail_en->Title)) == "parallelogram")
	{
		?>
			style += "width:100px;left:111px;top:65px";
		<?php
	}else
	{
		?>
		style += "top:50px;";
		<?php
	}
	?>
    $('#choco-content-type').attr('style', result.filter+style);
  });
});

</script>