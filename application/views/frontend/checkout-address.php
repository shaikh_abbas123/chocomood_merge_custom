<script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4"></script>
<style>
.content.checkout.address .wbox {
    /* margin-bottom: 30px; */
    margin-bottom: 10px;
    margin-top: -15px;
}
#shipping_div .row.no-border.shipMethodTxt {
    display: flex;
    justify-content:center;
}
.mb-0 {
    margin-bottom: 0px !important;
}

.h-100 {
    height: 100%;
}

.editbox.alwaysBeThere .bbox {
    background-color: #D3E6CF !important;
}
.g-total-txt{
    text-align: left;
    position: absolute;
    left: 45px;
}
.content.checkout.address .wbox .bbox {
    border-radius: 5px;
}
.pay-tab-ico{
    margin-top: 27px;
}
h6.shipping-address-heading {
    padding-bottom: 12px;
}

p.collect-para {
    font-weight: 700;
    font-size: 17px;
    margin-bottom: 0px;
}
/** margin-bottom:34px !important; **/
.content.checkout .wbox .row.no-border {
    border: 0px;
    margin-bottom: 0px;
    padding-bottom: 0px
}

.content.checkout .wbox.side ol.chkOutAdd li {
    margin: 0;
    color: #bd9371;
    text-align: center;
}
img.explore-cities-icon {
    width: 40px;
    height: 100%;
}

.explore-cities-text {
    display: flex;
}
.explore-cities-text p{
    margin-left:10px;
    padding-top:12px
}
.content.checkout .wbox.side ol.chkOutAdd li span {
    float: left;
    color: inherit;
    display: inline-block;
    vertical-align: middle;
    font-size: 18px;
    margin: 0;
    font-weight: normal;
}
.ship-pr-30{
    padding-right: 30px !important;
}
.content.checkout .wbox.side ol.chkOutAdd li strong {
    font-weight: normal;
    font-size: 18px;
}

.content.checkout.address .wbox .btn.ccheckout {
    margin-top: 0px;
}

label.customradio.default {
    font-weight: 900;
    padding: <?php echo($lang=='AR'? ' 0px 35px 0px 0px ' : ' 0px 0px 0px 35px');
    ?>;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    line-height: 25px;
}

label.customradio.default .checkmark {
    border: 1px solid rgb(153, 153, 153);
}

.shipMethodTxt h5 strong {
    display: block;
    font-size: 18px;
    font-weight: bold;
}

.shipMethodTxt h5 {
    line-height: 1.1 !important;
}

.plusNewBox a {
    display: BLOCK;
    width: 100%;
    height: 100%;
    border: 1px dashed #86A57F;
    border-radius: 5px;
    background-color: #fff;
    position: relative;
    font-size: 0;
}

.plusNewBox a:before {
    content: '';
    width: 100px;
    height: 100px;
    background: #CE8D8D 0% 0% no-repeat padding-box;
    box-shadow: 0px 3px 6px #00000029;
    display: block;
    border-radius: 50%;
    margin: auto;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}

.plusNewBox a:after {
    background: transparent;
    content: '+';
    width: 40px;
    height: 40px;
    display: block;
    margin: auto;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    font-size: 65px;
    color: #fff;
    text-shadow: 0px 3px 6px #00000029;
    text-align: center;
    line-height: 36px;
}

.customradio .checkmark:after {
    top: 7.5px;
    left: 8px;
}

.content.checkout.address .wbox .bbox h5 {
    font-size: 15px;
    margin: 0;
    line-height: 1.5;
    font-weight: normal;
    margin: 10px 0 8px;
    word-break: break-word;
}

.content.checkout.address .wbox .edImgBox {
    display: block;
    width: 100%;
    padding: 15px 0;
}

.content.checkout.address .wbox .edImgBox1 {
    display: block;
    width: 100%;
    padding: 0px 0;
}

.content.checkout.address .wbox .edImgBox2 {
    display: block;
    width: 100%;
    padding: 0px 0;
}

.content.checkout.address .wbox .edImgBox img {
    max-width: 100%;
    height: auto;
}

.content.checkout.address .wbox .bbox h5 a {
    color: #0000004E;
    margin-top: 4px;
    display: inline-block;
}

.collOptRow_f_Wrap>div {
    margin-bottom: 25px;
}

.collOptRow_f_Wrap {
    flex-wrap: wrap;
    margin-bottom: -25px !important;
}

.col-md-4.editbox.plusNewBox:nth-child(n+4) {
    display: none;
}

.mod-cnt {
    padding: 2% 6%;
    border-radius: 5px;
}

.modW2 {
    max-width: 1104px;
    max-height: 1176px;
}

.mod_hdd {
    border: none;
}

.mod_ftt {
    border: none;
}

.chcimg {
    width: 60px;
    height: 60px;
    border-radius: 17px;
}

.title {
    margin-bottom: 5%;
    margin-top: 3%;
}
.col-sm-12.col-md-4.order-summary h5 {
    width: 75%;
}
.list-product-checkout {
    height: 200px;
    overflow-y: scroll;
    overflow-x: hidden;
}
h3.scnd-title {
    color: #50456D !important;
    font-size: 30px !important;
    position: relative;
    bottom: 14px;
}

h1.hed-title {
    color: #CE8D8D;
    font-size: 36px;
}

.txt-detail p.chc-nam {
    color: #000000 !important;
    font-size: 17px !important;
}

.crs-btn.btn-dark {
    background-color: #CE8D8D;
    width: 35px;
    height: 35px;
    border-radius: 50px;
    border: none;
}

button.btn-light.crt-btn {
    width: 100%;
    height: 46px;
    background-color: #F4EBD3 !important;
    color: #000;
    border: none;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    border-radius: 7px;
    font-size: 16px;
    margin-bottom: 30px;
    margin-top: 7%;
}
.branch-name-desc{
    margin-bottom: 0px;
}
span.back-to-cart-css:before {
    content: "\f177";
    font-family: 'FontAwesome';
    padding-right: 6px;
    padding-left: 2px;
    }
.cat-choc .txt-price {
    /* position: relative; */
    display: block;
    width: 100%;
    padding-right: 40px;
}
a.btn.ccheckout.btn-primary {
    /* padding: 10px 5px; */
    /* margin: 10px 5px; */
    float: right;
    /* padding: 13px 44px; */
    border-radius: 0px;
    font-weight: 600;
}
.cat-choc .txt-price p,
.cat-choc .txt-detail p.chc-nam {
    color: #000 !important;
}
h6.shipping-address-heading {
    padding-bottom: 12px;
}
.cat-choc .txt-price p span.green,
.cat-choc .txt-detail p.chc-nam span.green {
    color: #21AD00 !important;
}

.cat-choc .txt-price p span.red,
.cat-choc .txt-detail p.chc-nam span.red {
    color: #C30000 !important;
    -webkit-text-decoration-line: line-through;
    /* Safari */
    text-decoration-line: line-through;
}

.cat-choc .txt-price button.crs-btn.btn-dark {
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    color: #fff;
    text-shadow: 0px 3px 6px #00000029;
}

.btn-info.pay-btn {
    background-color: #CE8D8D;
    color: #fff;
    width: 100%;
    height: 46px;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    border-radius: 7px;
    font-size: 16px;
    border: none;
    display: inline-block;
    line-height: 46px;
}

.cat-choc {
    border-bottom: 1px solid #E2E2E2;
    padding: 14px 0;
    margin: 0 15px;
}

.chc-nam {
    font-size: 18px;
}

.txt-price {
    font-size: 18px;
}

.clk-con {
    font-size: 16px;
    color: #1A1526;
    font-weight: normal;
    margin-top: 14px;
    margin-bottom: 15px;
    padding: 0;
}

.edt-crt {
    width: 100%;
}

.edt-pay {
    width: 100%;
}

.p-15 {
    padding: 15px !important;
}
.order-summary {
    background-color: whitesmoke;
    padding: 20px;
}
h2.top-heading {
    margin-bottom:0px !important;
}
.summary-last-row-b-bottom{
    border-bottom: 0px !important;
}
.g-total-hover:hover {
    background: #f4ebd3 !important;
    color: #bd9381 !important;
    cursor:initial;
}
.g-total-hover:hover span{
    color:#bd9381 !important;
}
.g-total-hover:hover strong span{
    color:unset !important;
}
.fieldset-payment .bbox.dropdown {
    min-height: 130px !important;
}

@media (min-width: 768px) and (max-width: 1199px) {
    .customradio .checkmark {
        position: absolute;
        top: 0;
        right: auto;
        bottom: auto;
        left: -15px !important;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border-radius: 50%;
        color: #50456d;
        -webkit-transition-duration: 0.6s;
        -o-transition-duration: 0.6s;
        transition-duration: 0.6s;
    }

    label.customradio.default {
        font-weight: 900;
        padding: 0px 0px 0px 15px !important;
        border-width: initial;
        border-style: none;
        border-color: initial;
        border-image: initial;
        line-height: 25px;
    }

    .content.checkout.address .wbox .edImgBox1 {
        display: block;
        width: 100%;
        padding: 30px 0;
    }
    .content.checkout.address .wbox .edImgBox1.cod-img {
        padding: 0px;
    }
}
@media (min-width: 691px) and (max-width: 991px) {
    .content.checkout.address .wbox .editbox .bbox.dropdown.custom-height-130 {
        min-height: 130px !important;
        height:130px !important;
    }
}
@media only screen and (max-width:991px){
    .col-md-12.st-form-height {
        /* height: 1000px !important; */
        height:730px !important;
    }
    .content.checkout.address .wbox .editbox .bbox.dropdown {
        min-height: 230px !important;
        height:230px !important;
    }
    .plusNewBox a {
        height: 230px;
    }
    .content.checkout.address .wbox.side h5 {
        width: 80%;
    }
    .edImgBox img {
        height: 70px !important;
    }
    .content.checkout.address .wbox .bbox h5 {
        font-size: 14px;
    }
    .customradio {
        margin-bottom: 0px;
    }
    img.pay_on_store_image {
        margin-top: 10px !important;
    }
}
@media only screen and (max-width: 690px){
    .content.checkout.address .wbox .editbox .bbox.dropdown {
        height:auto !important;
    }
    .col-md-12.st-form-height {
        height: 170% !important;
    }
    .row.paymentnSummary.d-flex.summary-last-row-b-bottom{
        display: block !important;
    }
    a.btn.ccheckout.btn-primary {
        padding: 13px 12px;
    }
    .content.checkout.address .wbox.side h5 {
        width: 100%;
    }
    .content.checkout.address .wbox .editbox .bbox.dropdown.custom-height-130 {
        min-height: 130px !important;
    }
    .content.checkout.address .wbox.side .price{
        text-align: right;
    }
    .payment-method-block{
            display:block !important;
        }
     .content.checkout .wbox .row {
    padding-bottom: 0px;
     margin-bottom: 0px;
}
}
@media (max-width: 768px) {
    /* .plusNewBox a {
        height: 40% !important;
    } */
}
.edImgBox img {
    height: 100px !important;
}
span.back-to-cart-css {
    font-size: 16px !important;
}

h2.top-heading {
    font-size: 33px;
}
/* MULTI STEP CHECKOUT FORM CSS */

#msform {
    width: 100%;
    margin: 15px auto;
    text-align: center;
    position: relative;
}

#msform fieldset {
    /* background: white; */
    border: 0 none;
    /* border-radius: 3px; */
    /* box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4); */
    padding: 0;
    box-sizing: border-box;
    width: 100%;
    margin: 0;

    /*stacking fieldsets above each other*/
    position: relative;
}

/*Hide all except first fieldset*/
#msform fieldset:not(:first-of-type) {
    display: none;
}

/*inputs*/
#msform input,
#msform textarea {
    padding: 15px;
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-bottom: 10px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    font-size: 13px;
}

/*buttons*/
#msform .action-button {
    width: 100px;
    background: #ce8d8d;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 1px;
    cursor: pointer;
    /* padding: 10px 5px;
    margin: 10px 5px; */
}

#msform .action-button:hover,
#msform .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #ce8d8d;
}

/*headings*/
.fs-title {
    font-size: 15px;
    text-transform: uppercase;
    color: #2C3E50;
    margin-bottom: 10px;
}

.fs-subtitle {
    font-weight: normal;
    font-size: 13px;
    color: #666;
    margin-bottom: 20px;
}

/*progressbar*/
#progressbar {
    margin-bottom: 5px;
    overflow: hidden;
    /*CSS counters to number the steps*/
    counter-reset: step;
}

#progressbar li {
    list-style-type: none;
    color: #000;
    text-transform: uppercase;
    font-size: 14px;
    width: 25%;
    float: left;
    position: relative;
}

#progressbar li:before {
    content: counter(step);
    counter-increment: step;
    width: 30px;
    line-height: 30px;
    display: block;
    font-size: 18px;
    color: #333;
    background: #f4ebd3;
    border-radius: 50%;
    margin: 0 auto 5px auto;
}

/*progressbar connectors*/
#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: #b28c67;
    position: absolute;
    left: -50%;
    top: 14px;
    z-index: -1;
    /*put it behind the numbers*/
}

#progressbar li:first-child:after {
    /*connector not needed before the first step*/
    content: none;
}

/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
#progressbar li.active:before,
#progressbar li.active:after {
    background: #32231f;
    color: white;
}

/* MULTISTEP CHECKOUT FORM */


p.address {
    margin-top: 25px !important;
    color: black !important;
    font-weight: 900 !important;
    font-size:14px !important
}
</style>
<section class="content checkout address"> 
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="top-heading"><?= lang('checkout')?> <a href="<?= base_url('cart')?>"><span class="back-to-cart-css"><?= lang('back_to_cart')?></span></a></h2>
            </div>
            <div class="col-md-12 st-form-height" style="height:720px">





                <form id="msform" action="" method="post">
                    <ul id="progressbar">
                        <li class="active"><?= lang('address')?></li>
                        <li><?= lang('branch')?></li>
                        <li><?= lang('shipping')?></li>
                        <li><?= lang('payment')?></li>
                    </ul>
                    <fieldset id="address_div">
                        <div class="wbox mb-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h6 class="shipping-address-heading">
                                        <?= lang('Shipping_Address')?></h6>
                                    <!-- <div class="dropdown barownButton">
                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"
                                                aria-hidden="true"></i></button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <li>
                                                <a href="<?php echo base_url(); ?>/address/add?check=true">
                                                    <?= lang('Add_an_Address')?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div> -->
                                </div>
                            </div>
                            <div class="row no-border d-flex align-items-stretch collOptRow_f_Wrap">
                               
                                <div class="col-md-4  editbox alwaysBeThere">
                                    <div class="bbox dropdown mb-0 h-100">
                                        <label class="customradio default"><?php echo lang('Collect_From_Store'); ?>
                                            <input type="radio" name="collectionOption"
                                                class="collectFromStore shipping_method" data-shipping-id="2"
                                                data-district-id="0"
                                                <?php echo(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ? 'checked' : ''); ?>>
                                            <span class="checkmark"></span>
                                        </label>
                                        <div class="edImgBox">
                                            <img src="<?php echo front_assets(); ?>images/hand.svg" height=""
                                                width="" />
                                        </div>
                                        <h5><?= lang('No_Shipping_Charges')?></h5>
                                    </div>
                                </div>
                               
                                <?php
                        foreach ($addresses as $address) { ?>
                                <div class="col-md-4 editbox">
                                    <div class="bbox dropdown mb-0 h-100">
                                        <label class="customradio default"><?= lang('Deliver_here')?>
                                            <input type="radio" name="collectionOption" class="addressCheckbox chk"
                                                value="<?php echo $address->AddressID; ?>"
                                                data-district-id="<?php echo $address->DistrictID; ?>"
                                                <?php echo !isset($_COOKIE['CollectFromStore']) && $address->IsDefault == 1 ? 'checked' : '' ?>>
                                            <span class="checkmark"></span>
                                        </label>
                                        <!--<label class="customcheck default">Collect payment here
                                        <input type="checkbox" class="CollectPaymentChk"
                                               value="<?php echo $address->AddressID; ?>" <?php echo !isset($_COOKIE['CollectFromStore']) && $address->UseForPaymentCollection == 1 ? 'checked' : '' ?>>
                                        <span class="checkmark"></span>
                                    </label>-->
                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="javascript:void(0);"
                                                    onclick="removeIt('address/delete', 'AddressID', <?php echo $address->AddressID ?>, <?php echo "'".lang('are_you_sure')."'"; ?>);">

                                                    Remove this address
                                                </a>
                                            </li>
                                        </ul>
                                        <h5>
                                            <?php echo $address->RecipientName; ?>
                                            <span><?php echo $address->MobileNo; ?></span>
                                        </h5>
                                        <h5>
                                            <span><?php echo lang('email'); ?>: <?php echo $address->Email; ?></span>
                                            <span><?php echo lang('city'); ?>: <?php echo $address->CityTitle; ?></span>
                                            <span><?php echo lang('district'); ?>:
                                                <?php echo $address->DistrictTitle; ?></span>

                                            <span><?php echo lang('street'); ?>: <?php echo $address->Street; ?></span>
                                            <a href="https://maps.google.com/?q=<?php echo $address->Latitude; ?>,<?php echo $address->Longitude; ?>"
                                                target="_blank"><?php echo lang('google_pin_link'); ?></a>
                                        </h5>
                                    </div>
                                </div>
                                <?php }
                        ?>
                                <div class="col-md-4 editbox plusNewBox">
                                    <a href="<?php echo base_url(); ?>/address/add">+<p class="address"><?= lang('add_address')?></p></a>
                                </div>
                            </div>
                        </div>
                        <input type="button" name="next" class="next action-button p-15" id="scrollToTopBtn" value="<?= lang('nex_t')?>" />
                    </fieldset>
                    <fieldset id="branch_div" >
                        <div class="wbox" id="branch_map_custom" >
                            <p class="collect-para">No Map For Custom Order</p>
                        </div>
                        <div id="branch_map" >
                            <div class="wbox mb-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 class="branch_title shipping-address-heading"><span id="delivery"
                                                <?php echo((isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) ? 'style="display: none;"' : 'style="display: inline;"'); ?>>Deliver</span><span
                                                id="collect"
                                                <?php echo((isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) ? 'style="display: inline;"' : 'style="display: none;"'); ?>>Collect</span>
                                            From Branch</h6>
                                    </div>
                                </div>
                                <div class="row no-border">
                                    <div class="col-md-12">
                                        <div class="form-group edSelectStyle white">
                                            <select id="option-city-change" class="form-control border" name="citiess"
                                                onchange="selectBrach(this);">

                                                <?php 
                                                if(isset($_GET['city']) && $_GET['city'] !=  ''){
                                                    if(!isset($itemtype))
                                                    {
                                                        echo '<option value="">'.lang('show_all_city').'</option>';
                                                    }
                                                }else{
                                                    echo '<option value="">'.lang('Select_City').'</option>';
                                                }
                                                if($available_cities){
                                                    $check_array = array();
                                                    foreach ($available_cities as $store){
                                                        if(!in_array($store->CityID,$check_array)){
                                                            echo '<option data-lat="'.$store->latitude.'" data-long="'.$store->longitude.'" value="'.$store->CityID.'" '.((isset($_GET['city']) && $_GET['city'] == $store->CityID) ? 'selected' : '').'>'.$store->CityTitle.'</option>';
                                                        }
                                                        $check_array[] = $store->CityID;
                                                        
                                                    }
                                                }
                                                ?>


                                            </select>
                                        </div>
                                        <div class="googleMap">
                                            <div id="map" style="height:300px; width:100%;"></div>
                                        </div>
                                        <br>
                                        <p id="selected_branch_html"
                                            style="<?php echo ($this->session->userdata('DeliveryStoreID') ? 'display: block;' : 'display: none;' );?> margin-bottom:0px !important;">
                                            <!-- <?php echo ($this->session->userdata('DeliveryStoreID') ? $this->session->userdata('DeliveryStoreTitle').' '.lang('Selected') : '' );?> -->
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="explore-cities-text"><img class="explore-cities-icon" src="<?php echo front_assets(); ?>images/cluster-icon-2.png"
                                                                height="" width="" />
                                <p><?php echo lang('click_on_this_icon_to_explore'); ?></p>
                            </div>
                        </div>
                        <input type="button" name="previous" class="previous action-button p-15" id="scrollToTopBtn" value="<?= lang('pre_vious')?>"
                            style="float:left;" />
                        <input type="button" name="next" class="next action-button p-15 branch-empty" id="scrollToTopBtn"value="<?= lang('nex_t')?>" />
                    </fieldset>
                    <fieldset id="shipping_div">
                        <div class="wbox shipment_method" id="shipping_div_collect_from_store" >
                            <p class="collect-para"><?php echo lang('collect_from_store_no_shipping_req'); ?></p>
                        </div>
                        <div class="wbox shipment_method" id="shipment_method"
                            <?php echo(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ? 'style="display:none;"' : 'style="display:block;"'); ?>>
                            <div class="row">
                                <div class="col-md-12">
                                    <h6 class="shipping-address-heading"><?php echo lang('shipping_method'); ?></h6>
                                </div>
                            </div>
                            <div class="row no-border shipMethodTxt payment-method-block">
                                <input type="hidden" id="getShipmentMaxAmount" value="<?= getShipmentMaxAmount();?>" />
                                <?php
                                            $shipment_methods = getTaxShipmentCharges('Shipment');
                                            
                                            foreach ($shipment_methods as $shipment_method) { ?>
                                        <div class="col-md-4 editbox shipping_method_parent" style="display: none;">
                                            <div class="bbox dropdown">
                                                <label class="customradio default"><?php echo $shipment_method->Title; ?>
                                                <input type="hidden" id="shipping_method_title_3" value="<?php echo $shipment_method->Title; ?>" />
                                                <input type="radio" name="shipment_method"
                                                            value="<?php echo $shipment_method->TaxShipmentChargesID; ?>" <?php echo($shipment_method->TaxShipmentChargesID == $this->session->userdata('ShipmentMethodIDForBooking') ? 'checked' : ''); ?>
                                                            onclick="changeShipmentMethod(this.value);" class="shipping_method"
                                                                    data-shipping-id="3" >
                                                    <span class="checkmark"></span>
                                                </label>
                                                <h5>
                                                    <strong><?php echo $shipment_method->Description; ?></strong>
                                                    <?php
                                                    $taxes = getTaxShipmentCharges('Tax');
                                                    foreach ($taxes as $tax) {
                                                        if ($tax->Type == 'Fixed') {
                                                            $tax_amount = $tax->Amount;
                                                        } elseif ($tax->Type == 'Percentage') {
                                                            $tax_amount = ($tax->Amount / 100) * ($shipment_method->Amount);
                                                        }
                                                    $temp_amount = $tax_amount +$shipment_method->Amount;
                                                    }
                                                    ?>
                                                    <span class="light"><?php echo number_format($temp_amount,2); ?> <?php echo lang('sar'); ?></span>
                                                    <?= lang('inclusive_vat')?>
                                                </h5>
                                            </div>
                                        </div>
                                        <?php }
                                                ?>
                                <?php
                                if(!empty($semsa_shipment)){
                                
                                    foreach ($semsa_shipment as $data) { ?>
                                <div class="col-md-4 editbox shipping_method_parent col-sm-6">
                                    <div class="bbox dropdown">
                                        <label class="customradio default"><?php echo $data->Title; ?>
                                            <input type="radio" name="shipment_method" value="semsa"
                                                <?php echo($this->session->userdata('SemsaShipmentID') && $this->session->userdata('SemsaShipmentID') == 'semsa' ? 'checked' : ''); ?>
                                                onclick="changeSemsaShipmentMethod(this.value);" class="shipping_method"
                                                data-shipping-id="1">
                                            <span class="checkmark"></span>
                                        </label>
                                        <h5>
                                            <strong><?php echo $data->Description; ?></strong>

                                            <span
                                                class="light"><?php echo number_format(findSemsaShippingCharges(), 2); ?>
                                                <?php echo lang('sar'); ?></span>

                                        </h5>
                                    </div>
                                </div>
                                <?php } 
                                    }
                                        ?>


                            </div>

                        </div>
                        <input type="button" name="previous" class="previous action-button p-15" id="scrollToTopBtn" style="float:left;"
                            value="<?= lang('pre_vious')?>" />
                        <input type="button" name="next" class="next action-button p-15 shipping-empty" id="scrollToTopBtn" value="<?= lang('nex_t')?>" />
                        <!-- <input type="submit" name="submit" class="submit action-button" value="Submit" /> -->
                    </fieldset>
                    <fieldset class="fieldset-payment">
                        <div class="wbox side">
                            <div class="row paymentnSummary d-flex summary-last-row-b-bottom">
                                <div class="col-sm-12 col-md-7 select-payment">
                                    <div class="row no-border shipMethodTxt">
                                        <div class="col-md-12">
                                            <h6 class="text-center" style="margin-bottom: 20px;">
                                                <?= lang('Select_Payment_Method')?>
                                            </h6>
                                            <div
                                                class="col-md-6 col-sm-12 col-xs-12 payment_method_parent  editbox alwaysBeThere" style="margin-bottom:10px;">
                                                <div class="bbox dropdown mb-0 custom-height-130">
                                                    <label class="customradio default"><span class="cod_text">COD</span>
                                                        <input type="radio" name="paymentMethod" class="payment_method"
                                                            data-district-id="0" data-payment-id="1" value="cod">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <div class="edImgBox1 cod-img">
                                                        <img class="cod_image payment_img" src="<?php echo front_assets(); ?>images/COD.png"
                                                            height="" width="" />
                                                        <img class="pay_on_store_image"
                                                            src="<?php echo front_assets(); ?>images/pay_on_store.png" height=""
                                                            width="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12  editbox payment_method_parent" style="margin-bottom:10px;">
                                                <div class="bbox dropdown mb-0 custom-height-130">
                                                    <label class="customradio default"><?= lang('Pay_Online')?>
                                                        <input type="radio" name="paymentMethod" class="payment_method"
                                                            data-district-id="0" value="paytab" data-payment-id="2">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <div class="edImgBox2 pay-tab-ico">
                                                        <img class="payment_img" src="<?php echo front_assets(); ?>images/PayTab.png" height=""
                                                            width="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row no-border shipMethodTxt">

                                        <label class="customcheck" style="border: none !important; text-align:left;">
                                            <?= lang('I_accept_the')?> <a href="javascript:void(0)" data-toggle="modal"
                                                data-target="#TermsConditions"><?= @lang('terms_and_conditions')?></a>
                                            <input type="checkbox" class="acceptTerms">
                                            <span class="checkmark"></span>
                                        </label>
                                        <div class="col-md-12">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-5 order-summary">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6><?= lang('order_summary')?></h6>
                                        </div>
                                    </div>
                                    <div class="list-product-checkout">
                                        <?php
                                        $total = 0;
                                        $product_name = '';
                                        //print_rm($cart_items);
                                        foreach ($cart_items as $itm => $cart_item) {
                                            $IsOnOffer = false;
                                            if($itm != 0){
                                                $product_name .= ','.$cart_item->Title;
                                            }else{
                                                $product_name .= $cart_item->Title;
                                            }
                                            
                                        ?>
                                        
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if ($cart_item->ItemType == 'Product') {
                                                        $Price = $cart_item->TempItemPrice;
                                                        $DiscountType = $cart_item->DiscountType;
                                                        $DiscountFactor = $cart_item->Discount;
                                                        if ($DiscountType == 'percentage') {
                                                            $IsOnOffer = true;
                                                            $Discount = ($DiscountFactor / 100) * $Price;
                                                            if ($Discount > $Price) {
                                                                $ProductDiscountedPrice = 0;
                                                            } else {
                                                                $ProductDiscountedPrice = $Price - $Discount;
                                                            }
                                                        } elseif ($DiscountType == 'per item') {
                                                            $IsOnOffer = true;
                                                            $Discount = $DiscountFactor;
                                                            if ($Discount > $Price) {
                                                                $ProductDiscountedPrice = 0;
                                                            } else {
                                                                $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                            }
                                                        } else {
                                                            $Discount = 0;
                                                            if ($Discount > $Price) {
                                                                $ProductDiscountedPrice = 0;
                                                            } else {
                                                                $ProductDiscountedPrice = $Price;
                                                            }
                                                        }
                                                        ?>
                                                    <img
                                                        src="<?php echo base_url(get_images($cart_item->ProductID, 'product', false)); ?>">
                                                    <h5><?php echo $cart_item->Title; ?>
                                                        <?php if($cart_item->PriceType == 'kg'){ ?>
                                                        <?php
                                                        $PerGramPrice = 0;
                                                        $Perpieceprice = 0;
                                                        $PackageQuantity = 0;
                                                        $packageTotal = 0;
                                                        $product_packages = get_product_packages($cart_item->ProductID,$language);
                                                            foreach(@$product_packages as $k => $v) {
                                                                if($cart_item->Package == $v['PackagesProductID'] ){
                                                                    $PerGramPrice = $v['PerGramPrice'];
                                                                    $Perpieceprice = $v['PerPiecePrice'];
                                                                    $PackageQuantity = $v['quantity'];
                                                                    $packageTotal = ( $PerGramPrice / $Perpieceprice ) * $PackageQuantity;
                                                        ?>
                                                        <span><?= $v['Title'] ?> x <?= @$cart_item->Quantity ?></span>
                                                        <?php 
                                                            }
                                                        } ?>
                                                        <!-- <span><?php echo $cart_item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?></span> -->
                                                        <?php }else{ ?>
                                                        <span><?php echo $cart_item->Quantity; ?>pc(s)
                                                            <!-- x <?= @$cart_item->Quantity ?> -->
                                                        </span>
                                                        <?php  } ?>
                                                    </h5>
                                                    <?php } elseif ($cart_item->ItemType == 'Choco Shape') { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                        title="Click to view whats inside" data-url="customize/getChocoboxDetail"
                                                        data-id="<?php echo $cart_item->TempOrderID; ?>"
                                                        data-box_id="<?php echo $cart_item->CustomizedBoxID; ?>"
                                                        data-pids="<?php echo $cart_item->CustomizedOrderProductIDs; ?>"
                                                        data-box_type="<?php echo $cart_item->ItemType; ?>"
                                                        data-ribbon="<?php echo $cart_item->Ribbon; ?>">
                                                        <img src="<?php echo base_url($cart_item->CustomizedShapeImage); ?>">
                                                        <h5>Choco Shape</h5>
                                                    </a>
                                                    <?php } else { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                        title="Click to view whats inside" data-url="customize/getChocoboxDetail"
                                                        data-id="<?php echo $cart_item->TempOrderID; ?>"
                                                        data-box_id="<?php echo $cart_item->CustomizedBoxID; ?>"
                                                        data-pids="<?php echo $cart_item->CustomizedOrderProductIDs; ?>"
                                                        data-box_type="<?php echo $cart_item->ItemType; ?>"
                                                        data-ribbon="<?php echo $cart_item->Ribbon; ?>">
                                                        <img
                                                            src="<?php echo front_assets("images/" . $cart_item->ItemType . ".png"); ?>">
                                                        <h5><?php echo $cart_item->ItemType; ?></h5>
                                                        <?php if($cart_item->ItemType == 'Choco Box') { ?>
                                                        <label><?= (@$cart_item->BoxPrintable == 1)?'Printable':'Unprintable'?></label>
                                                        <?php }?>
                                                    </a>
                                                    <?php } ?>
                                                    <div class="col-sm-12  col-md-12">
                                                        <?php if($IsOnOffer)
                                                    {
                                                    ?>
                                                            <div class="row">
                                                                <div class="price text-right"><span
                                                                        style="text-decoration: line-through;"><?php echo number_format($cart_item->TempItemPrice, 2); ?>
                                                                        <?php echo lang('sar'); ?></span>
                                                                        <br>
                                                                    <?php echo number_format($ProductDiscountedPrice, 2); ?>
                                                                    <?php echo lang('sar'); ?></div>
                                                            </div>
                                                            <?php
                                                    }else
                                                    {
                                                    ?>
                                                            <div class="price">
                                                                <?php echo number_format(($cart_item->TempItemPrice * $cart_item->Quantity), 2); ?>
                                                                <?php echo lang('sar'); ?></div>
                                                            <?php 
                                                    }
                                                    ?>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        <?php 
                                            if($IsOnOffer)
                                            {
                                                $total += $ProductDiscountedPrice * $cart_item->Quantity;
                                            }else{
                                                $total += $cart_item->TempItemPrice * $cart_item->Quantity;
                                            }
                                            
                                        }
                                        ?>
                                    </div>
                                    <div class="row last summary-last-row-b-bottom">
                                        <div class="col-md-12">
                                            <?php
                                                if ($this->session->userdata('order_coupon')) {
                                                    $order_coupon = $this->session->userdata('order_coupon');
                                                    $coupon_code = $order_coupon['CouponCode'];
                                                    $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                                                    $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total;
                                                    $total = $total - $coupon_discount_availed;
                                            ?>
                                            <p><span>Promo Applied:</span> <strong><?php echo $coupon_code; ?></strong></p>
                                            <p><span><?php echo lang('promo_discount'); ?> %:</span>
                                                <strong><?php echo $coupon_discount_percentage; ?></strong>
                                            </p>
                                            <p><span><?php echo lang('promo_discount_availed'); ?>:</span>
                                                <strong><?php echo $coupon_discount_availed; ?>
                                                    <?php echo lang('sar'); ?></strong>
                                            </p>
                                            <?php } ?>
                                            <!--<h6>
                                                <span>Shipping VAT 5%</span>
                                                <strong>20.00 sar 7.00 sar</strong>
                                                </h6>-->
                                            <ol class="chkOutAdd ship-pr-30">
                                                <?php
                                                    $shipping_amount = 0;
                                                    if(!isset($_COOKIE['CollectFromStore'])){
                                                    
                                                    if($this->session->userdata('ShipmentMethodIDForBooking')){
                                                        $shipment_method = getTaxShipmentChargesByID('Shipment', $this->session->userdata('ShipmentMethodIDForBooking'));
                                                    }else{
                                                        $shipment_method = getTaxShipmentCharges('Shipment', true);
                                                    }
                                                    //$shipment_method = getTaxShipmentCharges('Shipment', true);
                                                    if ($shipment_method) {
                                                        $shipping_title = $shipment_method->Title;
                                                        $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                                                        if ($shipment_method->Type == 'Fixed') {
                                                            $shipping_amount = $shipment_method->Amount;
                                                        } elseif ($shipment_method->Type == 'Percentage') {
                                                            $shipping_amount = ($shipment_method->Amount / 100) * ($total);
                                                        }

                                                        //count tax 
                                                        $total_tax = 0;
                                                        $taxes = getTaxShipmentCharges('Tax');
                                                        foreach ($taxes as $tax) {
                                                            if ($tax->Type == 'Fixed') {
                                                                $tax_amount = $tax->Amount;
                                                            } elseif ($tax->Type == 'Percentage') {
                                                                $tax_amount = ($tax->Amount / 100) * ($total);
                                                            }
                                                        $total_tax += $tax_amount;
                                                        }
                                                        $get_amount = number_format(@$total, 2) + number_format(@$total_tax, 2);
                                                        ?>
                                                                <li class="log_distance">
                                                                    <span>
                                                                        <!-- <i class="fa fa-truck" aria-hidden="true"></i> -->
                                                                        <?php echo $shipping_title; ?>
                                                                    </span>
                                                                    <?php
                                                                if($get_amount >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                                                                $shipping_amount = 0;
                                                            ?>
                                                                    <strong id="ShippingAmount" style="display: none;"><span
                                                                            class="distance_amount applied_shipment_charges"><?php echo number_format($shipping_amount, 2); ?>
                                                                        </span>
                                                                        <?php echo lang('sar'); ?></strong>
                                                                    <strong>
                                                                        <span class=""><?= freeShippingTitle(); ?></span>
                                                                    </strong>
                                                                    <?php
                                                                }else{
                                                            ?>
                                                                    <strong id="ShippingAmount"><span
                                                                            class="distance_amount applied_shipment_charges"><?php echo number_format($shipping_amount, 2); ?>
                                                                        </span>
                                                                        <?php echo lang('sar'); ?></strong>
                                                                    <?php } ?>
                                                                </li>
                                                                <?php }
                                                }
                                                    ?>
                                                    <?php
                                                        $total_tax = 0;
                                                        $tax_amount = 0;
                                                        $taxes = getTaxShipmentCharges('Tax');
                                                        foreach ($taxes as $tax) {
                                                            $tax_title = $tax->Title;
                                                            $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                                                            if ($tax->Type == 'Fixed') {
                                                                $tax_amount = $tax->Amount;
                                                            } elseif ($tax->Type == 'Percentage') {
                                                                //$tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
                                                                $tax_amount = ($tax->Amount / 100) * ($total);
                                                            }
                                                        ?>
                                                        <li>
                                                            <span>
                                                                <!-- <i class="fa fa-file-text-o" aria-hidden="true"></i> -->
                                                                <?php echo $tax_title; ?> <?php echo $tax_factor; ?>
                                                            </span>
                                                            <strong id="TaxAmount"><?php echo number_format($tax_amount, 2); ?>
                                                                <?php echo lang('sar'); ?></strong>
                                                        </li>
                                                    <?php
                                                    $total_tax += $tax_amount;
                                                }
                                                $semsa_amount = 0;
                                                if($this->session->userdata('SemsaShipmentID') && !isset($_COOKIE['CollectFromStore']) && !empty($semsa_shipment)){
                                                $title  = @$semsa_shipment[0]->Title; 
                                                $semsa_amount = findSemsaShippingCharges();

                                                ?>
                                                            <li>
                                                                <span> <?php echo $title; ?></span>
                                                                <strong id="SemsaShipmentID">
                                                                    <span
                                                                        class="applied_shipment_charges"><?php echo number_format($semsa_amount, 2); ?>
                                                                        <?php echo lang('sar'); ?></span>

                                                                </strong>
                                                            </li>
                                                            <?php   
                                                }
                                                
                                                $total = $total + $shipping_amount + $total_tax +$semsa_amount;
                                                ?>

                                                        </ol>
                                        </div>
                                        <div class="col-md-12">
                                            <p class="btn btn-secondary g-total-hover">
                                                <span class="g-total-txt"><?php echo lang('Grand_Total'); ?></span><strong><span
                                                        class="total_price"><?php echo number_format($total, 2); ?></span>
                                                    <span class="this_lang"><?php echo lang('sar'); ?></span></strong>
                                            </p>
                                            <input type="hidden" class="total_price_val" name="total_price_val"
                                                value="<?php echo number_format($total, 2); ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <input type="button" name="previous" class="previous action-button p-15" id="scrollToTopBtn" style="float:left;"
                            value="<?= lang('pre_vious')?>" />
                            <a href="javascript:void(0);" class="btn ccheckout btn-primary"
                                                onclick="<?= (!isset($isCustomize)) ?'placeOrder();':'placeOrderCustomize();'?> "><?= lang('Continue_to_Payment')?></a>
                    </fieldset>
                </form>




            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>




</section>


<!-- Modal -->
<div class="modal fade" id="chkContShipping" tabindex="1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
    <div class="modal-dialog modal-dialog-centered modWl" role="document">
        <div class="modal-content mod-cont <?php echo($lang == 'AR' ? 'text-right' : 'text-left'); ?>">
            <div class="modal-header mod_hdd">

                <div class="title">
                    <h1 class="hed-title"><?= lang('Attention')?>!</h1>
                    <h3 class="scnd-title"><?= lang('Some_of_the_Items_are_not_available_in_this_Branch')?></h3>
                </div>
            </div>
            <div class="modal-body" id="chkContShippingBody">

            </div>
            <div class="modal-footer mod_ftt">

                <div class="edt-crt text-center">
                    <a href="<?php echo base_url('cart');?>"> <button
                            class="btn-light crt-btn"><?=lang('Edit_Cart') ?></button></a>
                </div>

                <div class="edt-pay text-center">
                    <a href="javascript:void();" class="btn-info pay-btn"
                        onclick="placeOrder();"><?= lang('Continue_to_Payment')?></a>
                    <p class="clk-con text-center"><?= lang('By_Clicking_continue') ?></p>
                </div>
            </div>

        </div>
    </div>
</div>
<input type="hidden" value="<?= lang('Shipping_method_required')?>" id="Shipping_method_required_lang" placeholder="">
<input type="hidden" value="<?= lang('Please_select_brach_from_map')?>" id="Please_select_brach_from_map_lang"
    placeholder="">
<input type="hidden" value="<?= lang('Please_select_payment_method')?>" id="Please_select_payment_method_lang"
    placeholder="">
<input type="hidden" id="StoreID"
    value="<?php echo ($this->session->userdata('DeliveryStoreID') ? $this->session->userdata('DeliveryStoreID') : ''); ?>">
<input type="hidden" name="isCustomize" id="isCustomize" value="<?= $isCustomize?>"> 
<input type="hidden" name="selected_city_lat"  value="<?= $default_city_lat?>">
<input type="hidden" name="selected_city_long" value="<?= $default_city_long?>">
<script src="https://unpkg.com/@googlemaps/markerclusterer/dist/index.min.js"></script>
<script>
    
$(document).ready(function() {
    
    var def_city = "<?= @$default_city; ?>";
    if (window.location.href.indexOf("city") < 0) {
        if (def_city != 0 && window.location.href.indexOf("branch_all") == -1) {
            // selectBrach("<?= @$addresses[0]->CityID ?>");

            $('#option-city-change option[value="<?= @$addresses[0]->CityID ?>"]').prop('selected', true);
            var lat = $('input[name="selected_city_lat"]').val();
            var long = $('input[name="selected_city_long"]').val();
            map.setCenter(new google.maps.LatLng(lat, long));
        }
    }
    $("input[name='paymentMethod']").trigger('change');
    $('input[name="collectionOption"]').trigger('change');
    $('.collectFromStore').on('click', function() {
        $('#collect').show();
        //$('.shipment_method').hide();
        $("input:radio[name='shipment_method']").each(function(i) {
            this.checked = false;
        });
        // $("#shipment_method").css("pointer-events","none");
        //$('#delivery').hide();
        $('#shipment_method').hide();


    });
    $('.addressCheckbox').on('click', function() {

        $('#collect').hide();
        // $('.shipment_method').show();
        //$("#shipment_method").css("pointer-events","auto");
        $('#delivery').show();
        $('#shipment_method').show();
    });
    <?php if(isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1 ){ ?>
    $("#shipment_method").css("pointer-events", "none");
    $("input:radio[name='shipment_method']").each(function(i) {
        this.checked = false;
    });
    <?php } ?>
});

function selectBrach(cities) {
    var opt = cities.options[cities.selectedIndex];
    var lat = opt.dataset.lat;
    var long = opt.dataset.long;
    map.setCenter(new google.maps.LatLng(lat, long));

    // if (city_id == '') {
    //     window.location.replace("<?php echo base_url();?>address?branch_all=1");
    // } else {
    //     window.location.replace("<?php echo base_url();?>address?city=" + cities.value);
    // }
    // window.location.replace("<?php echo base_url();?>address?city="+city_id);
}

var center_lat = <?php echo(isset($available_cities[0]) ? $available_cities[0]->Latitude : 21.484716); ?>;
var center_lng = <?php echo(isset($available_cities[0]) ? $available_cities[0]->Longitude : 39.189606); ?>;
var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: new google.maps.LatLng(center_lat, center_lng),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [{
                    "color": "#50456d"
                },
                {
                    "saturation": "29"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{
                    "color": "#444444"
                },
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "color": "#b31f1f"
            }]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#dfd1ae"
            }]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "labels",
            "stylers": [{
                "color": "#f00000"
            }]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "labels.text",
            "stylers": [{
                "color": "#1a1526"
            }]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#ff0f0f"
            }]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "all",
            "stylers": [{
                "color": "#ebe3cd"
            }]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                    "color": "#892020"
                },
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
                    "saturation": "-84"
                },
                {
                    "lightness": "85"
                },
                {
                    "gamma": "0.00"
                },
                {
                    "weight": "1"
                },
                {
                    "color": "#50456d"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [{
                "color": "#ffffff"
            }]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [{
                "visibility": "on"
            }]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [{
                "hue": "#ff0000"
            }]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
                    "visibility": "on"
                },
                {
                    "color": "#f6c866"
                }
            ]
        },
        {
            "featureType": "road.highway.controlled_access",
            "elementType": "all",
            "stylers": [{
                "visibility": "on"
            }]
        },
        {
            "featureType": "road.arterial",
            "elementType": "all",
            "stylers": [{
                    "visibility": "on"
                },
                {
                    "color": "#e98d58"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{
                    "color": "#46bcec"
                },
                {
                    "visibility": "on"
                }
            ]
        }
    ]
})


function myClick(index) {
    google.maps.event.trigger(markers[index], 'click');
}
// Code to disable browser back button
/* $(document).ready(function () {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    });
// In the following example, markers appear when the user clicks on the map.
      // Each marker is labeled with a single alphabetical character.
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var labelIndex = 0;
      function initialize(){
        var bangalore = { lat: 12.97, lng: 77.59 };
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: bangalore
        });
        // This event listener calls addMarker() when the map is clicked.
        google.maps.event.addListener(map, 'click', function(event) {
          addMarker(event.latLng, map);
        });
        // Add a marker at the center of the map.
        addMarker(bangalore, map);
      }
      // Adds a marker to the map.
      function addMarker(location, map) {
        // Add the marker at the clicked location, and add the next-available label
        // from the array of alphabetical characters.
        var marker = new google.maps.Marker({
            icon: '<?php echo base_url(); ?>assets/frontend/images/pin.svg',
          position: location,
          label: '1',
          map: map
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);*/
      var markers = [];
      
      //clear markers from map
      function clearOverlays() {
        for (var i = 0; i < markers.length; i++ ) {
            markers[i].setMap(null);
            // markerCluster.removeMarker(markers[i]);
        }
        markers = [];
        
    } 
$("input[name='paymentMethod']").on('change', function() {
    if ($("input[name='paymentMethod']:checked").val() !== undefined && $("input[name='paymentMethod']:checked")
        .val() == 'cod') {
        $(".ccheckout").html('<?= lang('Continue_to_complete_the_order')?>');
    } else {
        $(".ccheckout").html('<?= lang('Continue_to_Payment')?>');
    }
});

function add_markers(shipping_method_type)
{
    <?php if ($available_cities && count($available_cities) > 0)
    { ?>
    var infowindow = new google.maps.InfoWindow();
    
    
    <?php
    $i = 0;
    
    foreach ($available_cities as $store)
    {
        $i++;
         ?>
         var marker;
         var shipping = '<?php echo $store->ShippingID; ?>';
         var shipping_list = shipping.split(',');
        if(shipping != null || shipping != ''){
            if ( shipping_list.includes(shipping_method_type.toString())){

                var check = true;
                for(var mj = 0;mj < markers.length; mj++) {
                    if (markers[mj].label.text == <?= $i;?>) {
                        check = false;
                    }
                }
                if(check)
                {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<?php echo $store->Latitude; ?>, <?php echo $store->Longitude; ?>),
                        icon: '<?php echo base_url(); ?>assets/frontend/images/pin.svg',
                        label: {
                            text: '<?php echo $i; ?>',
                            color: 'white'
                        },
                        map: map
                    });
                    google.maps.event.addListener(marker, 'click', (function(marker) {
                            return function() {
                                infowindow.setContent('<h5 style="color:black;"><?php echo $store->StoreTitle; ?></h5>');
                                infowindow.open(map, marker);
                                var district = '<?php echo $store->DistrictID; ?>';
                                var district_list = district.split(',');
                                var address_district = $('input[name="collectionOption"]:checked').attr('data-district-id'); //Address
                                // console.log('Shipping: ' + $('input[name="shipment_method"]:checked').data('shipping-id'));
                                if (address_district > 0) {
                                    $('#StoreID').val(<?php echo $store->StoreID; ?>);
                                    if ($('input[name="shipment_method"]:checked').data('shipping-id') == undefined) {
                                        $('#selected_branch_html').text('<?php echo $store->StoreTitle; ?>' + ' Selected');
                                        $('#selected_branch_html').show();
                                        changeDeliveryStoreID('<?php echo $store->StoreID; ?>','<?php echo $store->StoreTitle; ?>');
                                    
                                        $('#shipment_method').show();
                                    } else if ($('input[name="shipment_method"]:checked').data('shipping-id') == '1') {
                                        changeDeliveryStoreID(<?php echo $store->StoreID; ?>,'<?php echo $store->StoreTitle; ?>');
                                    } else {
                                        if (jQuery.inArray(address_district, district_list) != -1) {
                                            $('#selected_branch_html').text('<?php echo $store->StoreTitle; ?>' + ' Selected');
                                            $('#selected_branch_html').show();
                                            changeDeliveryStoreID(<?php echo $store->StoreID; ?>,'<?php echo $store->StoreTitle; ?>');
                                        } else {
                                            showMessage('<?= lang('Branch_is_not_delivered_in_your_district')?>', 'danger');
                                            $('#selected_branch_html').text(
                                                '<?= lang('Branch_is_not_delivered_in_your_district')?>');
                                            $('#selected_branch_html').show();
                                            $('#StoreID').val('');
                                            unsetDeliveryStoreID();
                                            //return false;
                                        }
                                    }
                                } else {
                                    $('#selected_branch_html').text('<?php echo $store->StoreTitle; ?>' + ' Selected');
                                    // $('#selected_branch_html').html('');
                                    $('#selected_branch_html').show();
                                    $('#StoreID').val(<?php echo $store->StoreID; ?>);
                                    changeDeliveryStoreID(<?php echo $store->StoreID; ?>, '<?php echo $store->StoreTitle; ?>');
                                }

                            }
                    })(marker));
 
                    markers.push(marker);
                    // markerCluster.addMarker(marker);
                }
                
            }
        }
    <?php  } ?>
    <?php } ?>
    if($('#isCustomize').val() == 1 && $('input[name="collectionOption"]:checked').val() != 'on') 
    {
        $('#StoreID').val(10); //setting workshop id by default from store table.
            changeDeliveryStoreID('10','Workshop');
       
    }
    
}


$('input[name="collectionOption"]').on('change', function() {
    clearOverlays()
    $('#selected_branch_html').text('');
    $('#StoreID').val('');
    var selected_address = ($('input[name="collectionOption"]:checked').data('shipping-id'))?$('input[name="collectionOption"]:checked').data('shipping-id'):'all';
    if(selected_address == 2)
    {
        add_markers(2);
    }else
    {
        add_markers(1);
        add_markers(3);
    }
    for(var mj = 0;mj < markers.length; mj++) {
        console.log(markers[mj].label.text);
    }
    
    var markerCluster = new markerClusterer.MarkerClusterer({ map, markers });

    
    

    if ($('input[name="collectionOption"]:checked').attr('data-district-id') !== undefined && $(
            'input[name="collectionOption"]:checked').attr('data-district-id') > 0) {
        $(".branch_title").html("<?= lang('pickup_location')?>");
        $(".cod_text").html("<?= lang('cod')?>");
        $(".cod_image").show();
        $(".pay_on_store_image").hide();
    } else {
        $(".branch_title").html("<?= lang('pickup_location')?>");
        $(".cod_text").html("<?= lang('Payment_at_Store')?>");
        $(".cod_image").hide();
        $(".pay_on_store_image").show();
    }

});


/* MULTI STEP FORM JS */

var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches



$(".next").click(function() {
    $("body").animate({ scrollTop: 0 }, "slow");
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();
    console.log(current_fs.attr('id'));
    if(current_fs.attr('id') == 'address_div')
    {
        console.log('Changed ...');
        if($('#isCustomize').val() == 1)
        {
            if($('input[name="collectionOption"]:checked').attr('data-district-id') == 0){
                $('#branch_map').show();
                $('#branch_map_custom').hide();
                
            }
            else
            {
                $('#branch_map').hide();
                $('#branch_map_custom').show();
                setTimeout(function(){
                    $('input.branch-empty').trigger('click');
                }, 3000);
            }
        }
        else
        {
            $('#branch_map').show();
            $('#branch_map_custom').hide();
        }
    }
    else if(current_fs.attr('id') == 'branch_div')
    {
        
        var storeID = $('#StoreID').val();
        if(storeID == '')
        {
            animating = false;
            showMessage('Select Branch to proceed.', 'danger');
            return false;
        }

        if($('input[name="collectionOption"]:checked').attr('data-district-id') > 0)
        {
            $('#shipping_div_collect_from_store').hide();
        }else
        {
            $('#shipping_div_collect_from_store').show();
            
            $(document).ready(function(){
                $('input.shipping-empty').trigger('click');

                    // set time out 3 sec
                    setTimeout(function(){
                        $('input.shipping-empty').trigger('click');
                    }, 3000);
                });
            
        }

    }else if(current_fs.attr('id') == 'shipping_div')
    {
        if($('input[name="collectionOption"]:checked').attr('data-district-id') > 0)
        {
           var shipping = $('input[name="shipment_method"]:checked').length;
            if(shipping == 0)
            {
                animating = false;
                showMessage('Select Shipping Method.', 'danger');
                return false;
            }
        }
    }
    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
        opacity: 0
    }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale(' + scale + ')',
                'position': 'absolute'
            });
            next_fs.css({
                'left': left,
                'opacity': opacity
            });
        },
        duration: 800,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function() {
    $("body").animate({ scrollTop: 0 }, "slow");
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
        opacity: 0
    }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'left': left
            });
            previous_fs.css({
                'transform': 'scale(' + scale + ')',
                'opacity': opacity
            });
        },
        duration: 800,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".submit").click(function() {
    return false;
});


/* MULTI STEP FORM JS */

/* DISABLE PAGE RELOADING */
// $('#option-city-change').change(function()  
//   { 
//       var option_val = $(this).val();
//       $.ajax({
//         type: 'post',
//         url: "",
//         data: "option="+option_val,
//         success: function(res) 
//         {  
//             alert(res)   
//         }
//       });
//   });

/* DISABLE PAGE RELOADING */

</script>