<?php $site_settings = site_settings(); ?>
<div class="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <h5><?php echo lang('newsletter_subscribe'); ?></h5>
                <form action="<?php echo base_url('page/newsletter_subscribe'); ?>" method="post" class="ajaxForm"
                      id="newsletterForm">
                    <div class="row">
                        <div class="col-md-8">
                            <input type="email" name="Email" class="form-control required"
                                   placeholder="<?php lang('email_placeholder') ?>">
                        </div>
                        <div class="col-md-4 p0">
                            <button type="submit" class="btn btn-secondary"><?php echo lang('subscribe'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-6 p0 text-center">
                        <h5><?php echo lang('payments_supported_by'); ?></h5>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="wbox">
                            <ul>
                                <li><img src="<?php echo front_assets(); ?>images/c1.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c2.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c3.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c4.png"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" id="Become_a_user_and_login_to_add_items_to_your_favourite_list" value="<?= lang('Become_a_user_and_login_to_add_items_to_your_favourite_list')?>">
<input type="hidden" id="confirm_lang" value="<?= lang('confirm')?>">
<input type="hidden" id="cancel_lang" value="<?= lang('cancel')?>">
<style>
.mx-3 {
    margin-right: 1rem;
    margin-left: 1rem;
}
footer.site-footer a {
    color: #fff;
    transition-duration:0.5s;
}
footer.site-footer a:hover {
    color:#f4ebd3
}
</style>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <p><?php echo lang('copyright'); ?> @ <?php echo date('Y'); ?> <?php echo lang('chocomood'); ?>
                    . <?php echo lang('all_rights_reserved'); ?>
                    <span class="mx-3">|</span>
                    <a href="javascript:void(0);" data-toggle="modal"
                       data-target="#TermsConditions" class="term_modal">
                        <?php
                        $terms_content = getPageContent(9, $lang);
                        echo $terms_content->Title;
                        ?>
                    </a>
                    <span class="mx-3">|</span>
                    <a href="<?php echo base_url('privacy-policy'); ?>"><?php echo lang('privacy_policy'); ?></a>
                    <span class="mx-3">|</span>
                    <a href="">Shopping Policy</a>
                    <span class="mx-3">|</span>
                    <a href="<?php echo base_url('delivery-policy'); ?>"><?php echo lang('delivery_policy'); ?></a>
                    <span class="mx-3">|</span>
                    <a href="<?php echo base_url('return-policy'); ?>"><?php echo lang('return_policy'); ?></a>
                    <span class="mx-3">|</span>
                    <!-- <a href="<?php echo base_url('site-map'); ?>"><?php echo lang('site_map'); ?></a>
                    <span class="mx-3">|</span> -->
                    <a href="<?php echo base_url('faq'); ?>"><?php echo lang('faq'); ?></a>
                </p>
                <p>
                    <span class="font-weight-bold">C.R.No. <?= @$site_settings->CrNumber; ?> &nbsp;&nbsp; VAT No. <?= @$site_settings->VatNumber; ?></span>
                </p>
            </div>
            <div class="col-md-3 col-sm-3 text-right">
                <ul>
                    <?php
                    if ($site_settings->FacebookUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->FacebookUrl; ?>" target="_blank">
                                <i class="bi bi-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->TwitterUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->TwitterUrl; ?>" target="_blank">
                                <i class="bi bi-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->GoogleUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->GoogleUrl; ?>" target="_blank">
                                <i class="bi bi-google" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->LinkedInUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->LinkedInUrl; ?>" target="_blank">
                                <i class="bi bi-linkedin" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->InstagramUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->InstagramUrl; ?>" target="_blank">
                                <i class="bi bi-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->YoutubeUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->YoutubeUrl; ?>" target="_blank">
                                <i class="bi bi-youtube-play" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="alert alert-dismissible text-center cookiealert" role="alert">
        <div class="cookiealert-container">
            <?php echo lang('cookies_usage_alert'); ?>
            <a href="http://cookiesandyou.com/" target="_blank"><?php echo lang('learn_more'); ?></a>
            <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
                <?php echo lang('got_it'); ?>
            </button>
        </div>
    </div>
</footer>
<script src="<?php echo front_assets(); ?>js/wow.js" defer></script>
<script src="<?php echo front_assets(); ?>js/owl.carousel.min.js" defer></script>
<script src="<?php echo front_assets(); ?>js/bootstrap-number-input.js" defer></script>
<script src="<?php echo front_assets(); ?>js/jquery.rateyo.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js" defer></script>
<script src="<?php echo front_assets(); ?>cookie_alert/cookiealert.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js" defer></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/tinysort.js" defer></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/jquery.tinysort.js" defer></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/tinysort.charorder.js" defer></script>
<script src="<?php echo front_assets(); ?>int_tel_input/js/intlTelInput.js" defer></script>
<script src="<?php echo front_assets(); ?>js/gallery.js" defer></script>
<script src="<?php echo front_assets(); ?>js/custom.js?v=<?php echo rand(); ?>" defer></script>
<script src="<?php echo front_assets(); ?>js/functions.js?v=<?php echo rand(); ?>" defer></script>
<link href="<?php echo front_assets(); ?>css/magiczoomplus.css?v=<?php echo rand(); ?>" rel="stylesheet" type="text/css" media="screen" async/>
<script src="<?php echo front_assets(); ?>js/magiczoomplus.js?v=<?php echo rand(); ?>" defer></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" async>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" defer></script>

<!-- THESE SCRIPTS ARE MOVED FROM HEADER -->

<script src="<?php echo front_assets(); ?>js/owl.carousel.min.js" defer></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" defer></script>
    <script src="<?php echo front_assets(); ?>bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js" defer></script>
    <script src="<?php echo front_assets(); ?>css/font-awesome.min.js" async></script>
    <script src="<?php echo front_assets(); ?>bootstrap_notify/bootstrap-notify.js" defer></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js" defer></script>
    <script type="text/javascript" id="hs-script-loader" async defer src="//js-eu1.hs-scripts.com/25093520.js"></script>

    <!--  -->
<?php if(isset($_GET['verify_mobile']) && $this->session->userdata('user')){ ?>
<script>
    $(document).ready(function () {
setTimeout(function(){ showVerifyMobile('Write mobile number for OTP.'); }, 1000);
    });
   
</script>
<?php    

}
?>
<script>
// $(function () {
 
//  $("#showRating").rateYo({

//    "rating" : 3.2,
//    "starSvg": "<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">"+
//                 "<path d="M12 6.76l1.379 4.246h4.465l-3.612 2.625 1.379"+
//                           " 4.246-3.611-2.625-3.612 2.625"+
//                           " 1.379-4.246-3.612-2.625h4.465l1.38-4.246zm0-6.472l-2.833"+
//                           " 8.718h-9.167l7.416 5.389-2.833 8.718 7.417-5.388"+
//                           " 7.416 5.388-2.833-8.718"+
//                           " 7.417-5.389h-9.167l-2.833-8.718z"></path>"+
//                "</svg>"
//  });
// });

// $(function () {
 
//  $("#showRating").rateYo({
//    ratedFill: "#E74C3C"
//  });

// });
    var mzOptions = {
        cssClass: "edImgZoomCus"
    };
    // $("#prodCatImg a.dropdown-item").hover( function() {
    //     var value=$(this).attr('data-image');
    //     $("#replaceImgHere").attr("src", value);
    // });
    $("#prodCatImg a.dropdown-item").hover( function() {
        var hoverId=$(this).attr('data-imgId');
        // $(".imbBoxRight .imagesURL").removeClass('active');
        // $(".imbBoxRight .imagesURL#"+hoverId).addClass('active');
        $(".imbBoxRight .imagesURL").fadeOut('');
        $(".imbBoxRight .imagesURL#"+hoverId).fadeIn('');
    });
</script>
<script>
    $(document).ready(function () {
        hideCustomLoader();
        $('.offered_product').tooltip();
        <?php
        if ($this->session->flashdata('message') && $this->session->flashdata('message') != '')
        { 
            if($this->session->flashdata('message_type')){
                $message_type = $this->session->flashdata('message_type');
            }else{
                $message_type = 'warning';
            }

            ?>
            showMessage('<?php echo $this->session->flashdata('message'); ?>', '<?php echo $message_type; ?>');
        <?php }
        ?>

<?php
        if ($this->session->flashdata('address_message') && $this->session->flashdata('address_message') != '')
        { 
            $this->session->set_userdata('check_address', '1');
            ?>
            // swal("<?php echo $this->session->flashdata('address_message'); ?>","", "warning")
            swal({
                title: "<?= lang('add_address')?>",
                text: "<?php echo $this->session->flashdata('address_message'); ?>",
                type: "error",
                showConfirmButton: false
            })
            setTimeout(function(){ 
                window.location = "<?= base_url('address/add')?>"; 
            }, 3000);
            
        <?php }
        ?>

        <?php if (!TermsAcceptedByUser())
        { ?>
            $('#TermsUpdatedModal').modal('show');
        <?php } ?>
        <?php
        if ($this->session->flashdata('social_data') && $this->session->flashdata('social_data') != '')
        { 
            if($this->session->flashdata('social_data')['Email']){
               $email = $this->session->flashdata('social_data')['Email']; 
               $user_name = $this->session->flashdata('social_data')['FullName']; ?>
               var email = "<?= @$email; ?>";
               var user_name = "<?= @$user_name; ?>";
               $(".open_register_dd").trigger("click");
               $("#Email").val(email);
               $("#Email").siblings('label').hide();
               $("#Email").hide();

               $("[name='FullName']").val(user_name);
               $("[name='FullName']").hide();
               $("[name='FullName']").siblings('label').hide();
            <?php }else{
               $email = '';
            }
        }

        ?>
    });
</script>
</body>
</html>