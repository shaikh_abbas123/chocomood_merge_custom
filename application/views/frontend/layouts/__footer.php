<?php $site_settings = site_settings(); ?>
<div class="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <h5><?php echo lang('newsletter_subscribe'); ?></h5>
                <form action="<?php echo base_url('page/newsletter_subscribe'); ?>" method="post" class="ajaxForm"
                      id="newsletterForm">
                    <div class="row">
                        <div class="col-md-8">
                            <input type="email" name="Email" class="form-control required"
                                   placeholder="<?php lang('email_placeholder') ?>">
                        </div>
                        <div class="col-md-4 p0">
                            <button type="submit" class="btn btn-secondary"><?php echo lang('subscribe'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-4 p0">
                        <h5><?php echo lang('payments_supported_by'); ?></h5>
                    </div>
                    <div class="col-md-8 text-center">
                        <div class="wbox">
                            <ul>
                                <li><img src="<?php echo front_assets(); ?>images/c1.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c2.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c3.png"></li>
                                <li><img src="<?php echo front_assets(); ?>images/c4.png"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <p><?php echo lang('copyright'); ?> @ <?php echo date('Y'); ?> <?php echo lang('chocomood'); ?>
                    . <?php echo lang('all_rights_reserved'); ?>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="javascript:void(0);" style="color: #ffffff;" data-toggle="modal"
                       data-target="#TermsConditions">
                        <?php
                        $terms_content = getPageContent(9, $lang);
                        echo $terms_content->Title;
                        ?>
                    </a>
                </p>
            </div>
            <div class="col-md-6 col-sm-6 text-right">
                <ul>
                    <?php
                    if ($site_settings->FacebookUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->FacebookUrl; ?>" target="_blank">
                                <i class="fa fa-facebook-official" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->TwitterUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->TwitterUrl; ?>" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->GoogleUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->GoogleUrl; ?>" target="_blank">
                                <i class="fa fa-google" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->LinkedInUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->LinkedInUrl; ?>" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->InstagramUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->InstagramUrl; ?>" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php
                    if ($site_settings->YoutubeUrl != '') { ?>
                        <li>
                            <a href="<?php echo $site_settings->YoutubeUrl; ?>" target="_blank">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="alert alert-dismissible text-center cookiealert" role="alert">
        <div class="cookiealert-container">
            <?php echo lang('cookies_usage_alert'); ?>
            <a href="http://cookiesandyou.com/" target="_blank"><?php echo lang('learn_more'); ?></a>
            <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
                <?php echo lang('got_it'); ?>
            </button>
        </div>
    </div>
</footer>
<script src="<?php echo front_assets(); ?>js/wow.js"></script>
<script src="<?php echo front_assets(); ?>js/owl.carousel.min.js"></script>
<script src="<?php echo front_assets(); ?>js/bootstrap-number-input.js"></script>
<script src="<?php echo front_assets(); ?>js/jquery.rateyo.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
<script src="<?php echo front_assets(); ?>cookie_alert/cookiealert.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/tinysort.js"></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/jquery.tinysort.js"></script>
<script src="<?php echo front_assets(); ?>js/sort_plugin/tinysort.charorder.js"></script>
<script src="<?php echo front_assets(); ?>int_tel_input/js/intlTelInput.js"></script>
<script src="<?php echo front_assets(); ?>js/gallery.js"></script>
<script src="<?php echo front_assets(); ?>js/custom.js?v=<?php echo rand(); ?>"></script>
<script src="<?php echo front_assets(); ?>js/functions.js?v=<?php echo rand(); ?>"></script>
<script>
    $(document).ready(function () {
        hideCustomLoader();
        $('.offered_product').tooltip();
        <?php
        if ($this->session->flashdata('message') && $this->session->flashdata('message') != '')
        { ?>
            showMessage('<?php echo $this->session->flashdata('message'); ?>', 'warning');
        <?php }
        ?>

        <?php if (!TermsAcceptedByUser())
        { ?>
            $('#TermsUpdatedModal').modal('show');
        <?php } ?>
    });
</script>
</body>
</html>