<?php $site_settings = site_settings();

?>

<?php if (isset($result->MetaTags) && $result->MetaTags != '') {
    $title = ' | ' . $result->MetaTags;
} else {
    $title = ($this->uri->segment(1) != '' ? ' | ' . str_replace(array('-', '_'), ' ', ucfirst($this->uri->segment(1))) : '');
} ?>
<html lang="en" <?php if ($lang == 'AR') {
    echo 'dir="rtl" ';
} ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/favicon.png">
        <meta property="og:image" content="<?php echo base_url(); ?>assets/frontend/images/logoin.png">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <meta name="description" content="<?php if (isset($result->MetaDescription)) {
        echo $result->MetaDescription;
    } ?>">
    <meta name="keywords" content="<?php if (isset($result->MetaKeywords)) {
        echo $result->MetaKeywords;
    } ?>">
    
    <title><?php echo site_title(); ?><?php echo $title; ?></title>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet" async >
    <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet" async>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>css/animate.min.css" async/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>css/font-awesome.min.css" async/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>css/owl.carousel.min.css" async/>
    <link rel="stylesheet" href="<?php echo front_assets(); ?>css/jquery.rateyo.min.css" async>
    <link rel="stylesheet" href="<?php echo front_assets(); ?>css/slideshow.css" async>
    <link rel="stylesheet" href="<?php echo front_assets(); ?>cookie_alert/cookiealert.css" async>
    <link rel="stylesheet" href="<?php echo front_assets(); ?>bootstrap_notify/animate.css" async>
    <link rel="stylesheet" href="<?php echo front_assets(); ?>int_tel_input/css/intlTelInput.css" async>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" async>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" async>
    <link href="<?php echo front_assets(); ?>css/<?php echo($lang == 'AR' ? 'rtl' : 'ltr'); ?>.css?v=<?php echo(rand());?>" rel="stylesheet"
          type="text/css" media="all" async>
    <link href="<?php echo front_assets(); ?>css/responsive.css?1.11" rel="stylesheet"
          type="text/css" media="all" async>
    <!-- Start of HubSpot Embed Code -->
    
    <!-- End of HubSpot Embed Code -->
    <script src="<?php echo front_assets(); ?>bower_components/jquery/dist/jquery.min.js"></script>
    
    <style type="text/css">
        .basket_to_top {
            position: absolute;
            top: 6px;
            right: 55px;
        }
        .error {
            color: red !important;
        }
        .error-border {
            border: 1px red solid !important;
        }
        .overlaybg {
            display: block;
            position: fixed;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, .3);
            z-index: 999999;
        }
        .overlaybg > img {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .p_liked {
            color: #fb7176 !important;
        }
        .p_unliked {
            color: slategray !important;
        }
        .cart_image {
            position: absolute;
            top: 38px;
            right: auto;
            bottom: auto;
            left: 0;
            font-size: 28px;
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
        }
        .alert-info {
            z-index: 99999 !important;
        }
        .intl-tel-input {
            width: 100%;
        }
        .navbar.inner .navbar-nav > li > a.active-menu {
            color: #BD9371 !important;
        }
        .btn.fbLogin, .btn.twLogin {
            padding: 6px;
            width: 100%;
            max-width: 194px;
            border: 0;
        }

        .btn.fbLogin {
            background: #4b7ebd;
        }

        .btn.twLogin {
            background: #0fd4ff;
        }
        .TermsConditions-content{
            overflow-y: scroll;
            height: 80%;
            margin: auto;
        }
        @media (min-width: 320px) and (max-width: 425px){
            .TermsConditions-content {
                overflow-y: scroll;
                height: 80%;
                margin: 80px 10px;
            }
        }
        .content.products {
            background-color: #fff;
        }
        .ar .intl-tel-input
        {
            direction:ltr !important;
        }
        .ar .intl-tel-input input#Mobile {
            text-align: left !important;
            padding-left: 80px !important;
        }
        .modal-header h3 span{
            display: block;
            color: #50456d;
            font-size: 30px;
        }
        .custom-modal-content h3 {
        color: #bd9371;
        font-size: 35px;
        margin: 0 0 20px !important;
        text-align: left;
        float:left;
        }
        .forget-password a{
            color: #bd9371 !important;
            display: block;
            padding: 15px 0;
        }
        a.register_now{
            padding: 0;
          color: #555555 !important;
        }
        .custom-register-modal{
            width:500px !important;
            margin: 0 auto!important;
        }
        @media screen and (max-width: 767px) {
            .custom-register-modal {
                width:100% !important;
              padding: 0px 100px !important;
              margin: 15px auto!important;
        }
        .modal-backdrop{
            z-index: unset !important;
        }
        .custom-modal-header {
            padding: 20px 20px 0px;
         }
         .modal .modal-content h3 {
            font-size: 30px;
            margin: 0px;
        }
        .modal .modal-content .modal-body.custom-modal-body {
         padding: 0px 25px 0px;
        
        }
       .modal .custom-form-group label {
            font-size: 14px !important;
        }
        .modal .custom-form-group input{
            font-size: 14px !important;
        }
        .custom-form-group {
            margin-bottom: 10px;
        }
        .modal .modal-content .modal-body .btn {
            margin: 10px 0 20px;
        }
        }
        @media screen and (max-width: 576px) {
            .custom-register-modal {
                width:100% !important;
              padding: 0px 50px !important;
        }
        .custom-modal-header {
            padding: 20px 20px 0px;
         }
         .modal .modal-content .custom-modal-header h3 {
            font-size: 27px;
            margin: 0px;
        }
        .modal-header.custom-modal-header h3 span {
            font-size: 25px;
        }
        .modal .modal-content .modal-body.custom-modal-body {
         padding: 0px 25px 0px;
        
        }
       .modal .custom-form-group label {
            font-size: 13px !important;
        }
        .modal .custom-form-group input{
            font-size: 13px !important;
        }
        .custom-form-group {
            margin-bottom: 10px;
        }
        .modal .modal-content .modal-body .btn {
            margin: 10px 0 20px;
        }
        .btn.btn-primary.btn-register {
            padding: 5px 40px;
        }
        a.phoneNo {
        display: none !important;
    }
        }
    </style>
    <script>
        var base_url = '<?php echo base_url(); ?>';
        var align_notify_message = '<?php echo($lang == 'AR' ? 'left' : 'right'); ?>';
    </script>
</head>
<body class="<?php echo $lang == 'AR' ? 'ar' : 'en'; ?>" data-spy="scroll" data-target="#HomeNav" data-offset="103">
<!--<div class="loader-wrapper">
    <div class="loader full-center">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
</div>-->
<div class="overlaybg" style="display: block !important;">
    <img src="<?php echo front_assets(); ?>loader.svg">
</div>
<?php
if (isset($marquee) && $marquee == true) { ?>
    <!--<div class="marquee">
        <marquee>For better performance, This site will save Cookies</marquee>
    </div>-->
<?php }
?>
<header class="site-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <a  class="phoneNo" href="tel:<?php echo $site_settings->PhoneNumber; ?>"><?php echo $site_settings->PhoneNumber; ?></a>
                    </li>
                    <?php
                    // if not logged in
                    if (!isset($this->session->userdata['user']->UserID)) { ?>
                                                 <li class="dropdown dropdown-masked login_li">
                            <a href="javascript:void(0)" class="open_login_dd" data-toggle="modal" data-target="#exampleModalCenter0"><?php echo lang('login'); ?></a>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter0" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content custom-modal-content">
                                    <div class="modal-header">
                                         <h3><?php echo lang('login'); ?></h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    <form action="<?php echo base_url('account/checkLogin'); ?>" method="post"
                                          class="ajaxForm"
                                          id="loginForm">
                                        <!-- <h3><?php echo lang('login'); ?></h3> -->
                                        <label><?php echo lang('email'); ?></label>
                                        <input type="email" name="Email" placeholder="<?php echo lang('Enter Email Address'); ?>"
                                               class="form-control required">
                                        <label><?php echo lang('password'); ?></label>
                                        <input type="password" name="Password" placeholder="<?php echo lang('Enter Password'); ?>"
                                               class="form-control required">
                                        <input type="hidden" name="redirect_url"
                                               value="<?php echo ltrim($_SERVER['REQUEST_URI'], '/'); ?>">
                                        <p class="text-right forget-password">
                                            <a href="javascript:void(0);" data-toggle="modal"
                                               data-target="#ForgotPassword">
                                                <?php echo lang('forgot_password'); ?>
                                            </a>
                                        </p>
                                        <button type="submit" class="btn btn-primary"><?php echo lang('login'); ?></button>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="<?php echo base_url('account/facebook_login');?>">
                                                <button type="button" class="btn btn-primary fbLogin"><i class="fa fa-facebook-f"></i> Login with Facebook</button></a>
                                            </div>
                                            <!-- <div class="col-md-6">
                                                <a href="<?php echo base_url('account/twitter_login');?>">
                                                    <button type="button" class="btn btn-primary twLogin"><i class="fa fa-twitter"></i> Login with Twitter </button>
                                                </a>
                                            </div> -->
                                        </div>
                                        <a href="javascript:void(0);" class="register_now">
                                            <?php echo lang('dont_have_account_register'); ?>
                                        </a>
                                    </form>
                                    </div>
                                    </div>
                                </div>
                                </div>
                        </li>
                        <li class="dropdown dropdown-masked register_li">
                            <!-- <a href="javascript:void(0)" class="open_register_dd" dropdown-toggle=""
                               data-toggle="dropdown"><?php echo lang('register'); ?></a> -->
                               <a href="javascript:void(0)" class="open_register_dd" data-toggle="modal" data-target="#exampleModalCenter"><?php echo lang('register'); ?></a>
                            <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered custom-register-modal" role="document">
                            <div class="modal-content custom-modal-content">
                            <div class="modal-header custom-modal-header">
                              <h3><?php echo lang('register'); ?><span><?php echo lang('your_account'); ?></h3>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body custom-modal-body">
                            <form action="<?php echo base_url('account/signUp'); ?>" method="post"
                                          id="signUpForm">
                                        <div class="form-group custom-form-group">
                                            <label><?php echo lang('full_name'); ?></label>
                                            <input type="text" name="FullName" placeholder="<?php echo lang('Enter Full Name'); ?>"
                                                   class="form-control required">
                                        </div>
                                        <div class="form-group custom-form-group">
                                            <label><?php echo lang('mobile_no'); ?></label>
                                            <input type="hidden" id="MobileCode" name="MobileCode" value="+966" >
                                            <input type="tel" placeholder="5xxxxxxxx" name="Mobile" style="direction:ltr;" class="form-control number-only phone required" id="Mobile">
                                        </div>
                                        <div class="form-group custom-form-group">
                                            <label><?php echo lang('email'); ?></label>
                                            <input type="email" name="Email" placeholder="<?php echo lang('Enter Email Address'); ?>"
                                                   class="form-control required" id="Email">
                                        </div>
                                        <div class="form-group custom-form-group">
                                            <label><?php echo lang('city'); ?></label>
                                            <select class="form-control" name="CityID">
                                                <?php
                                                $cities = getCities($language);
                                                foreach ($cities as $city) { ?>
                                                    <option value="<?php echo $city->CityID; ?>"><?php echo $city->Title; ?></option>
                                                <?php }
                                                ?>
                                            </select>
                                        </div>
                                         <div class="form-group custom-form-group">
                                            <label><?php echo lang('language'); ?></label>
                                            <select class="form-control" name="PreferredLang">
                                                <option value="0"  selected>English</option>
                                                <option value="1">Arabic</option>
                                            </select>
                                        </div>
                                        <div class="form-group custom-form-group">
                                            <label><?php echo lang('password'); ?></label>
                                            <input type="password" name="Password"
                                                   placeholder="<?php echo lang('new_password'); ?>" minlength="6"
                                                   class="form-control password required">
                                        </div>
                                        <div class="form-group custom-form-group">
                                            <label class="customcheck"
                                                   style="border: none !important;font-size: 14px !important;">
                                                <?php echo lang('i_accept_the'); ?> <a href="javascript:void(0)"  data-toggle="modal"data-target="#TermsConditions"style="display: initial !important;text-decoration: underline; color:#000 !important;" class="term_modal" data-id="0">
                                                    <?php echo lang('terms_and_conditions'); ?>
                                                </a>
                                                <input type="checkbox" class="acceptSignupTerms">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-register" onclick="sendOTP();">
                                            <?php echo lang('register_now'); ?>
                                        </button>
                                    </form>
                            </div>
                            <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div> -->
                            </div>
                        </div>
                        </div>
                        </li>
                    <?php }
                    ?>
                    <?php
                    // if logged in
                    if (isset($this->session->userdata['user']->UserID) && $this->session->userdata['user']->UserID > 0) { ?>
                        <li class="userNameDD">
                            <!-- <?php echo base_url('account'); ?> -->
                            <a href="#" class="myaccounts"><span>Hi, <?php echo $this->session->userdata['user']->FullName; ?></span>
                                <?php // echo lang('my_account'); ?>
                            </a>
                            <ul class="dropdown-menu profilemenu">
                                <li><a href="<?php echo base_url('account/'); ?>" class="myaccounts"><?php echo lang('my_account'); ?></a></li>
                                <li><a href="<?php echo base_url('account/orders'); ?>"><?php echo lang('my_orders'); ?></a></li>
                                <li><a href="<?php echo base_url('account/addresses'); ?>" class="myaccounts"> <?php echo lang('my_addresses'); ?></a></li>
                                <li><a href="<?php echo base_url('account/wishlist'); ?>" class="myaccounts"><?php echo lang('wishlist_items'); ?></a></li>
                                <li><a href="<?php echo base_url('account/logout'); ?>"><?php echo lang('logout'); ?></a></li>
                            </ul>
                        </li>
                        <!--<li>
                            <a href="<?php echo base_url('account/logout'); ?>">Logout</a>
                        </li>-->
                    <?php }
                    ?>
                    <?php
                    if ($this->session->userdata('user')) {
                        $user_id = $this->session->userdata['user']->UserID;
                    } else {
                        $user_id = get_cookie('temp_user_key');
                    }
                    $user_total_product = getTotalProduct($user_id);
                    ?>
                    <li>
                        <a href="javascript:void(0);"
                           onclick="changeLanguage('<?php echo($this->session->userdata('lang') == 'AR' ? 'EN' : 'AR'); ?>');"
                           class=""><?php echo($this->session->userdata('lang') == 'AR' ? 'ENG' : 'العربية'); ?></a>
                    </li>
                    <?php
                    $user_offers = getUserOffers($language);
                    if ($user_offers) { ?>
                        <li>
                            <a href="javascript:void(0)" dropdown-toggle="" data-toggle="dropdown">
                                <i class="fa fa-bell fa-inverse" aria-hidden="true"></i>
                                <b><?php echo count($user_offers); ?></b>
                            </a>
                            <ul class="dropdown-menu profilemenu">
                                <?php
                                foreach ($user_offers as $user_offer) { ?>
                                    <li>
                                        <a href="javascript:void(0);" id="openOfferModal"
                                           data-offer_notification_id="<?php echo $user_offer->OfferUserNotificationID; ?>"
                                           data-offer_id="<?php echo $user_offer->OfferID; ?>"
                                           data-offer_title="<?php echo $user_offer->Title; ?>"
                                           data-offer_description="<?php echo $user_offer->Description; ?>">New
                                            Offer: <?php echo $user_offer->Title; ?></a>
                                    </li>
                                <?php }
                                ?>
                            </ul>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</header>
<nav class="navbar <?php echo(isset($marquee) && $marquee == true ? '' : 'inner'); ?>">
    <div class="container">
        <div class="navbar-header">
            <button type="button" id="nav-icon2">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img
                        src="<?php echo front_assets(); ?>images/<?php echo(isset($marquee) && $marquee == true ? 'logo' : 'logoin'); ?>.png"></a>
        </div>
        <div>
            <div id="mobilenav">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo base_url('about-us'); ?>"
                           class="<?php echo(isset($menu) && $menu == 'about-us' ? 'active-menu' : ''); ?>"><?php echo lang('about'); ?></a>
                    </li>
                    <li><a href="<?php echo base_url('customize'); ?>"
                           class="<?php echo(isset($menu) && $menu == 'customize' ? 'active-menu' : ''); ?>"><?php echo lang('customize'); ?></a>
                    </li>
                    <li class=" dropdown"><a href="javascript:void(0);"
                           class="<?php echo(isset($menu) && $menu == 'product' ? 'active-menu' : ''); ?>"><?php echo lang('products'); ?></a>
                           <?php $menu_categories = subCategories(0,$language);
                                if($menu_categories){ ?>

                        
                           <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div class="links" id="prodCatImg">
                                    <?php foreach ($menu_categories as $key => $category) { ?>
                                    <a data-imgId="<?php echo 'imgEd_'.$category->CategoryID;?>" data-image="<?php echo base_url($category->Image); ?>" class="dropdown-item <?php echo ((isset($CategoryID) && $CategoryID == $category->CategoryID) ? 'active' : ''); ?>" href="<?php echo base_url();?>product/category/<?php echo strtolower(str_replace(' ','-',$category->Title)); ?><?php echo '-c'.$category->CategoryID;?>"><?php echo $category->Title; ?></a>
                                    <?php } ?>
                                </div>
                                <div class="imbBoxRight" style="background-image:url(<?php echo base_url($category->Image); ?>)">
                                    <?php foreach ($menu_categories as $key => $category) { ?>
                                        <span id="<?php echo 'imgEd_'.$category->CategoryID;?>" style="background-image:url(<?php echo base_url($category->Image); ?>)" class="imagesURL "></span>
                                    <?php } ?>
                                    <!-- <img id="replaceImgHere" src="<?php echo base_url($category->Image); ?>" width="" height="" alt=""/> -->
                                </div>
                            </div>
                        <?php } ?>
                    </li>
                    <!-- <li><a href="<?php echo base_url('corporate'); ?>"
                           class="<?php echo(isset($menu) && $menu == 'corporate' ? 'active-menu' : ''); ?>"><?php echo lang('corporate'); ?></a>
                    </li> -->
                    <li><a href="<?php echo base_url('Product/brands'); ?>"
                                class="<?php echo(isset($menu) && $menu == 'brands' ? 'active-menu' : ''); ?>"><?php echo lang('brands'); ?></a>
                    </li>
                    <li><a href="<?php echo base_url('store'); ?>"
                           class="<?php echo(isset($menu) && $menu == 'store' ? 'active-menu' : ''); ?>"><?php echo lang('store'); ?></a>
                    </li>
                    <li><a href="<?php echo base_url('contact-us'); ?>"
                           class="<?php echo(isset($menu) && $menu == 'contact-us' ? 'active-menu' : ''); ?>"><?php echo lang('contact_us'); ?>
                        </a></li>
                </ul>
            </div>
        </div>
        <div class="cartbox">
            <div class="cartitem">
                <?php
                if (isset($menu) && $menu == 'home') {
                    // $cart_icon = '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
                    $cart_icon = '<img class="cart_image" src="' . front_assets() . 'images/shopping_basket_white_small.png">';
                } else {
                    $cart_icon = '<img class="cart_image" src="' . front_assets() . 'images/shopping_basket_black_small.png">';
                }
                ?>
                <a href="<?php echo base_url('cart'); ?>"><?php echo $cart_icon; ?>
                    <b class="CartItemsCount"><?php echo $user_total_product; ?></b></a>
            </div>
            <a href="javascript:void(0)" id="searchtrigger">
                <i class="fa fa-search" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</nav>
<?php
$categories = getCategories($lang);
?>
<div id="searcharea" style="z-index:999">
    <div class="searchheader">
        <form class="searchForm" onsubmit="return false;">
            <input type="text" id="search_field" class="form-control">
            <input type="hidden" id="PageSearch" value="0">
            <button type="button" class="btn btn-primary back" id="back"><i class="fa fa-chevron-<?php echo($lang == 'AR' ? 'right' : 'left'); ?>"
                                                                            aria-hidden="true"></i></button>
            <a href="<?php echo base_url('cart'); ?>">
                <button type="button" class="btn btn-primary cart">
                    <img src="<?php echo front_assets(); ?>images/shopping_basket_white_small.png">
                    <b class="CartItemsCount"><?php echo $user_total_product; ?></b>
                </button>
            </a>
            <button type="submit" class="btn btn-secondary submit"><i class="fa fa-search"
                                                                                    aria-hidden="true"></i></button>
        </form>
    </div>
    <div class="searchfields ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                
                <?php if ($categories) {
                        $numItems = count($categories);
                        $i = 0;
                        $count = 0;
                        foreach ($categories as $key => $category) {
                            $sub_categories = subCategories($category->CategoryID, $lang);
                            if ($sub_categories) {
                                ?>
                                
                                    <?php foreach ($sub_categories as $key => $subcategory) {
                                        $count++;
                                        if ($count == 1) {
                                            echo '<div class="rfour">';
                                        }
                                        ?>
                                        <label class="customcheck"><?php echo $subcategory->Title; ?>
                                            <input type="checkbox" class="subcategory_search" value="<?php echo $subcategory->CategoryID; ?>">
                                            <span class="checkmark"></span>
                                        </label>

                                    <?php 
                                    if($count == 4)
                                    {
                                        echo '</div>';
                                        $count =0;
                                    }
                                    
                                } ?>
                                
                            <?php }
                            if(++$i === $numItems && $count != 0) {
                                echo '</div>';
                                // echo '<label>'.$count.'</label>';
                                $count =0;
                              }
                        }
                    } ?>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="searchresults" id="searchresults">
        <div class="simg" id="nosearchproduct" style="display: none;">
            <div class="row">
                <img src="<?php echo front_assets(); ?>images/seaimg.png">
            </div>
            <div class="row">
                <h3><?= lang('sorry_we_could_not_find_any_result_that_matches_your_request')?></h3>
            </div>
        </div>
        <div class="content products">
            <div class="container">
                <div id="search_products">

                </div>
                <div class="col-md-12 btnrow text-center">
                    <!-- <button id="loadmores" class="search_submit loadmore btn btn-primary" style="display:none"
                            ;><?php echo lang('load_more'); ?></button> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".searchForm").on('submit', function (e) {
            searchLogic();
        });
        $(".search_submit").on('click', function (e) {
            searchLogic();
        });
        function searchLogic() {
            if ($("#search_field").val() == '') {
                return false;
            }
            $("#nosearchproduct").hide();
            var loadmore = true;
            $(".overlaybg").show();
            $("#loadmores").show();
            if (!$(this).hasClass("loadmore")) {
                loadmore = false;
                $("#PageSearch").val(0);
            }
            var SubCategories = [];
            var page;
            page = $("#PageSearch").val();
            var i = 0;
            // var j = 0;
            $(".subcategory_search").each(function () {
                if ($(this).is(':checked')) {
                    //alert();
                    SubCategories[i] = $(this).val();
                    i = i + 1;
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + 'product/getMoreProductsSearch',
                data: {
                    'SubCategories[]': SubCategories,
                    'Page': $("#PageSearch").val(),
                    'SearchValue': $("#search_field").val()
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {
                    if (page == 0 && result.no_result) {
                        $("#nosearchproduct").show();
                        $("#loadmores").hide();
                        $("#search_products").html("");
                    } else {
                        $("#loadmores").show();
                        if (loadmore) {
                            $("#search_products").append(result.html);
                        } else {
                            $("#search_products").html(result.html);
                        }
                        if (result.page == page) {
                            $("#loadmoresearch").hide();
                        }
                        if (result.no_result) {
                            $("#loadmores").hide();
                        }
                        if(result.total_now < 1){
                            $("#loadmores").hide();
                        }
                        $("#PageSearch").val(result.page);
                    }
                },
                complete: function () {
                    $(".overlaybg").hide();
                    // $.unblockUI();
                }
            });
        }
    });

</script>