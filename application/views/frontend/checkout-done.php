<section class="content checkout address done">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alertbox">
                    <h5>
                        <i class="fa fa-check-circle ok" aria-hidden="true"></i>
                        <strong><?= lang('Your_Order_Has_been_Placed_Successfully')?>! </strong>
                        <span><?php echo lang('Order_No'); ?>. <?php echo $order->OrderNumber; ?> 

                            <?php 
                                if($order->TransactionID) {
                                    echo ",".lang('Transaction_ID')." . "; 
                                    echo $order->TransactionID; 
                                }
                            ?>   
                        </span>
                        <!--<a href="javascript:void(0);" onclick="window.print();" style="text-decoration: none;">-->
                        <a href="<?php echo base_url(); ?>page/invoice/<?php echo base64_encode($order->OrderID); ?>" target="_blank" style="text-decoration: none;">
                        <b>
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <?= lang('Print') ?>
                        </b>
                        </a>
                    </h5>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wbox side">
                    <?php
                    $order_items = getOrderItems($order->OrderID);
                    foreach ($order_items as $item) {
                        $IsOnOffer = false;
                        $Price = $item->Amount;
                        $DiscountType = $item->DiscountType;
                        $DiscountFactor = $item->Discount;
                        if ($DiscountType == 'percentage') {
                            $IsOnOffer = true;
                            $Discount = ($DiscountFactor / 100) * $Price;
                            if ($Discount > $Price) {
                                $ProductDiscountedPrice = 0;
                            } else {
                                $ProductDiscountedPrice = $Price - $Discount;
                            }
                        } elseif ($DiscountType == 'per item') {
                            $IsOnOffer = true;
                            $Discount = $DiscountFactor;
                            if ($Discount > $Price) {
                                $ProductDiscountedPrice = 0;
                            } else {
                                $ProductDiscountedPrice = $Price - $DiscountFactor;
                            }
                        } else {
                            $Discount = 0;
                            if ($Discount > $Price) {
                                $ProductDiscountedPrice = 0;
                            } else {
                                $ProductDiscountedPrice = $Price;
                            }
                        }
                     ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                if ($item->ItemType == 'Product') { ?>
                                    <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                    <h5><a href="<?php echo base_url() . 'product/detail/' . productTitle($item->ProductID); ?>"
                                           target="_blank"><?php echo $item->Title; ?></a>
                                        <?php if($item->PriceType == 'kg'){ 
                                            $PerGramPrice = 0;
                                            $Perpieceprice = 0;
                                            $PackageQuantity = 0;
                                            $packageTotal = 0;
                                            $product_packages = get_product_packages($item->ProductID,$language);
                                            foreach (@$product_packages as $k => $v) {
                                                if ($item->package_id == $v['PackagesProductID']) {
                                                    $PerGramPrice = $v['PerGramPrice'];
                                                        $Perpieceprice = $v['PerPiecePrice'];
                                                        $PackageQuantity = $v['quantity'];
                                                        $packageTotal = ( $PerGramPrice / $Perpieceprice ) * $PackageQuantity;
                                                    ?>
                                                    <span><?= $v['Title'] ?> x  <?= $item->Quantity?></span>
                                             <!-- <span><?php echo $item->Quantity * 1000; ?> <?php echo($language == 'AR' ? 'غرام' : 'Grams'); ?></span> -->
                                         <?php
                                                }
                                            }
                                        }else{ ?>
                                             <span><?php echo $item->Quantity; ?>pcs</span>
                                        <?php  } ?>
                                    </h5>
                                <?php } elseif ($item->ItemType == 'Choco Shape') { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "Order/getChocoboxDetail"
                                                       data-id="<?php echo $order->OrderID; ?>"
                                                       data-box_id="<?php echo $item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $item->ItemType; ?>"
                                                       data-ribbon="<?php echo $item->Ribbon; ?>">
                                                       <img width="100px !important"src="<?php echo base_url($item->CustomizedShapeImage); ?>">
                                                    <h5>Choco Shape</h5>
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "Order/getChocoboxDetail"
                                                       data-id="<?php echo $order->OrderID; ?>"
                                                       data-box_id="<?php echo $item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $item->ItemType; ?>"
                                                       data-ribbon="<?php echo $item->Ribbon; ?>">
                                                       <img width="100px !important"src="<?php echo front_assets("images/" . str_replace(" ", "_", $item->ItemType) . ".png"); ?>">           
                                                        <p><?php echo $item->ItemType; ?></p>
                                                        <?php if($item->ItemType == 'Choco Box') { ?>
                                                        <label><?= (@$item->Ribbon != 1)?'Printable':'Unprintable'?></label>
                                                        <?php }?>
                                                    </a>
                                                <?php }
                                ?>
                                <div class="price">
                                <?php if($item->PriceType == 'kg'){
                                    ?>
                                    <?php echo number_format(($ProductDiscountedPrice * $item->Quantity), 2); ?>
                                    <?php }else{ ?>
                                    <?php echo number_format(($ProductDiscountedPrice * $item->Quantity), 2); ?>
                                    <?php } ?>
                                    <?php echo lang('sar'); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row last">
                        <div class="col-md-12">
                            <?php
                            if ($order->CouponCodeUsed != '') { ?>
                                <p><span>Promo Applied:</span> <strong><?php echo $order->CouponCodeUsed; ?></strong></p>
                                <p><span><?php echo lang('promo_discount'); ?> %:</span>
                                    <strong><?php echo $order->CouponCodeDiscountPercentage; ?></strong></p>
                                <p><span><?php echo lang('promo_discount_availed'); ?>:</span> <strong><?php echo $order->DiscountAvailed; ?>
                                <?php echo lang('sar'); ?></strong></p>
                            <?php }
                            ?>
                            <ol class="p-0">
                                <?php if($order->CollectFromStore != 1 && $order->ShipmentMethodID > 0){ 
                                    $val = 0;
                                    if($order->TotalAmount >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                                        $val = freeShippingTitle();
                                    }else{
                                        $val = number_format($order->TotalShippingCharges, 2).' '.lang('sar');
                                    }
                                ?>
                                <li>
                                    <?php
                                    $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, $language);
                                    ?>
                                    <span><i class="fa fa-truck"
                                             aria-hidden="true"></i> <?php echo $shipment_method->Title; ?></span>
                                    <strong id="ShippingAmount"><?php echo $val; ?>
                                    </strong>
                                </li>
                            <?php } ?>
                                <li>
                                    <span>
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i> 
                                        <?= lang('Total_Tax_Paid')?>
                                    </span>
                                    <strong id="TaxAmount">
                                        <?php echo number_format($order->TotalTaxAmount, 2); ?>
                                    <?php echo lang('sar'); ?>
                                        
                                    </strong>
                                </li>
                            </ol>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-secondary pl-0 pr-0 w-100">
                                <ol>
                                    <li>
                                        <span class="g-t-padding-right-ar">
                                            <?php echo lang('Grand_Total'); ?>
                                        </span>
                                        <strong>
                                            <?php echo number_format($order->TotalAmount, 2); ?>
                                            <?php echo lang('sar'); ?>
                                        </strong>
                                    </li>
                                </ol>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <?php
                            if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                <h5 class="mb-5 mt-5" style="text-align:center;color:#BD9371;"><?php echo "Please note that your order will be ready for pick up in 20 minutes at branch ".$order->StoreAddress; ?></h5>
                            <?php } else { ?>
                            <button class="btn btn-secondary w-100">
                            <div class="abox">
                                    <h5 style="text-align:center;"><?php echo lang('Delivery_Address'); ?></h5>
                                    <h6 style="text-align:center;"><?php echo $order->RecipientName; ?></h6>
                                    <p>
                                        <span class="d-block w-100">
                                            <?php echo $order->MobileNo; ?>
                                        </span>
                                        <span class="d-block w-100">
                                            <a href="mailto:<?php echo $order->Email; ?>"><?php echo $order->Email; ?></a>
                                        </span>
                                        <span class="d-block w-100">
                                            <?php echo $order->AddressDistrict; ?>, <?php echo $order->AddressCity; ?>
                                        </span>
                                    </p>
                                    <p>
                                        <!--<span>City: <?php echo $order->AddressCity; ?></span><br>
                                        <span>District: <?php echo $order->AddressDistrict; ?></span><br>-->
                                       
                                        <span><?php echo lang('street'); ?>: <?php echo $order->Street; ?></span><br>
                                        
                                    </p>
                                    <p>
                                        <a href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $order->Latitude; ?>,<?php echo $order->Longitude; ?>"
                                           target="_blank">
                                           <?php echo lang('google_pin_link'); ?>
                                        </a>
                                    </p>
                                    <br>
                                    <!--h5>Payment Collection Address</h5>-->
                                    <?php
                                    if ($order->AddressID == $order->AddressIDForPaymentCollection) { ?>
                                        <p>Same as shipping address</p>
                                    <?php }  ?>
                                        
                                
                            </div>
                        </button>
                        <?php } ?>
                            <!--<label class="customcheck default">Register Me <a href="javascript:void(0)" data-toggle="modal" data-target="#TermsConditions">(I Agree to License Agreement)</a>
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>-->
                            <a href="<?php echo base_url(); ?>">
                                <button class="full btn btn-secondary w-100"><?= lang('Continue')?></button>
                            </a>
                            <input type="hidden" id="remaining_order" value="<?= $remaining_order; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var remaining_order = $('#remaining_order').val();
    if (remaining_order == 1) {
        setTimeout(function () {
            window.location.href = base_url + result.url;
        }, 2000);
    }
    // Code to disable browser back button
    $(document).ready(function () {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    });

    eraseCookie('CollectFromStore');
    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }
</script>