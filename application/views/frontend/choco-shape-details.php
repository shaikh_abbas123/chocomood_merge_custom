<script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/main.js"></script>
<script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/library/drift/Drift.min.js"></script>
<script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/library/drift/drift.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/product-details.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/slider.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customer-comment.css"/>

<style type="text/css">
  .ar .drift-zoom-pane {
            left: 10%;
            width:80%;
            height:60%;
        }
        .drift-zoom-pane {
            top:10%;
            left: 10%;
            width:79%;
            height:60%;
        }
  .upload-btn-wrapper {
    position: relative;
    overflow: hidden;
    display: inline-block;
  }

  .btn {
    border: 2px solid gray;
    color: gray;
    background-color: white;
    padding: 8px 20px;
    border-radius: 8px;
    font-size: 20px;
    font-weight: bold;
  }

  .upload-btn-wrapper input[type=file] {
    font-size: 100px;
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
  }

  button.mi-check-availability-btn {
    font-weight: bold;
    padding: .5rem .8rem;
    height: 50px;
    border-radius: 5rem;
    font-size: 1.5rem;
    border:none;
    background-color: #f4ebd3 !important;
    color: #BD9371;
    width: 100%
}
  .draggable {
    filter: alpha(opacity=60);
    opacity: 0.6;
  }

  .dropped {
    position: static !important;
    list-style-type: none;
    float: left;
    margin: 0 1% 0 0;
    width: 19%;
  }

  #dvSource,
  #dvDest {
    border: 5px solid #ccc;
    padding: 5px;
    min-height: 100px;
    width: 430px;
  }

  img {
    -webkit-transition-duration: 0s;
    -o-transition-duration: 0s;
    transition-duration: 0s;
  }

  @media (max-width: 1024px) {
    .pull-arbitrary {
      float: unset !important;
      text-align: center !important;
      display: flex;
      justify-content: center;
    }
  }



  .product-info h2.mi-product-title {
    color: #bd9371 !important;
    font-size: 24px !important;
    font-weight: 500 !important;
    line-height: 1.1;
}

.mi-product-desc {
    margin: 0;
}
.content.single-products .product-info .mi-txt-price {
    color: #bd9371;
    font-weight: bold;
    font-size: 1.9rem;
    margin: 10px 0 10px;
}
#PriceT {
    padding: 0 !important;
}

.content.single-products .product-info h5 span.mi-currency {
    color: #9c5114;
    font-weight: 700;
    font-size: 16px;
    padding: 0 5px;
}
.content.single-products .product-info h5.mi-txt-price span {
    padding: 0;
}
.content.single-products .product-info h5 span.mi-PriceP {
    font-size: 25px !important;
    color: #9c5114;
    font-weight: bold;
}

.content.single-products .product-info h4 {
    padding: 0 0 12px;
    margin: 0 0 12px;
    border-bottom: 1px solid #999;
    font-size: 20px;
    color: #bd9371;
}

button.nutration-facts-btn, button.check-availability-btn {
    background-color: #f4ebd3 !important;
    color: #BD9371;
    border: none;
    padding: 0.8rem;
}
.nutrition-pad-left {
    padding-left: 3rem !important;
}
.mi-nutritionBtn {
    text-align: right;
}

button.nutration-facts-btn {
    border-radius: 2rem;
    -webkit-border-radius: 2rem;
    -moz-border-radius: 2rem;
    -ms-border-radius: 2rem;
    -o-border-radius: 2rem;
    margin-top: 3px;
    font-size: 14px;
}

.mi-nutritionBtn a.mi-pointer {
    margin: 0;
    color: #bd9371;
    font-weight: bolder;
}
.content.single-products .product-info .no-border {
    border-bottom: none !important;
    font-weight: bold;
    padding-top: 10px;
}

.nutrition-pad-right {
    padding-right: 3rem !important;
}

span.mi-order-icon {
    color: #804b21 !important;
    font-size: 18px !important;
    margin-right: 3px;
    margin-left: 11px;
}

.mi-order-content {
    padding: 0.5rem 0;
    line-height: 1.3;
    font-size: 16px;
    margin: 0 5px 20px 40px;
}

span.mi-order-title {
    color: #bd9371;
    font-weight: 800;
    display: block;
    font-family: "Lato", sans-serif !important;
    font-size: 16px;
}

.mi-disp-inline-block {
    display: inline-block!important;
}

.mi-disp-inline {
    display: inline!important;
}
div.mi-share-on-social a {
    font-size: 13px;
    background-color: #f4ebd3;
    padding: 11px 11px;
    border-radius: 5rem!important;
    -webkit-border-radius: 5rem!important;
    -moz-border-radius: 5rem!important;
    -ms-border-radius: 5rem!important;
    -o-border-radius: 5rem!important;
    text-decoration: none;
    display: inline-block;
    text-align: center;
    font-size: 2rem;
}
h5.hdTxt {
    font-size: 31px !important;
    color: #50456D;
    width: 87px;
    height: 42px;
    padding: 7% 10% 9% 10%;
}
h5.hdTxt span {
    font-size: 41px;
    color: #CE8D8D;
    position: relative;
    bottom: 20px;
}
.frmcity {
    padding: 13% 10% 5% 10%;
    text-align: left;
}
.branchTXT {
    border-bottom: 1px solid #E2E2E2;
    line-height: 0.8;
    padding: 3% 10% 5% 10%;
}
.rsltTxt h6 {
    font-size: 18px;
    color: #CE8D8D;
    border-top: 1px solid #E2E2E2;
    border-bottom: 1px solid #E2E2E2;
    opacity: 1;
    font-weight: bolder;
    text-align: left;
    padding: 2% 10% 2% 10%;
}
p.hTxt {
    font-size: 15px !important;
    color: #000 !important;
    text-align: left !important;
}
button.btn-success.close-icon {
    background-color: #50456D;
    border-radius: 6px;
    opacity: 1;
    width: 166px;
    height: 37px;
    color: #fff;
    border: none;
    font-size: 15px;
}
.mod-cont {
    margin: 10%;
}
.owl-item
{
  width: 110px !important;
}
#shapes_related .owl-nav button{
    top: -30px;
}
</style>
<?php $choco_shape = getPageContent(12, $lang) ?>

<section class="content titlarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo @$shapeDetail->Title; ?></h2>
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('customize'); ?>"><?php echo lang('customize'); ?></a></li>
                    <li><?php echo $choco_shape->Title; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="content single-products">
  <div class="container-fluid responsve-width-containr product-details">
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 d-lg-block mr-lg-auto ml-lg-auto">
        <div class="imggallery text-center product-img-Zoom">
        <?php
                                 if(!empty($shape_images)){
                                  ?>
                                  <img id="main" class=" w-100 mb-3 rounded" 
                                            src="<?php echo base_url($shape_images[0]->ImageName); ?>?h=400" 
                                            data-zoom="<?php echo base_url($shape_images[0]->ImageName); ?>?h=400"
                                            alt=""/>
                                  <?php
                                 }else{
                    if(isset($shapeDetail->ShapeImage)) {
                                    ?>
                                        <img id="main" class=" w-100 mb-3 rounded" 
                                            src="<?php echo base_url($shapeDetail->ShapeImage); ?>?h=400" 
                                            data-zoom="<?php echo base_url($shapeDetail->ShapeImage); ?>?h=400"
                                            alt=""/>
                                        
                                    <?php
                                }
                              }
                            ?>
          <div id="thumbs" class="product-img-slider row p-3">
              <div class="col-12 p-0">
                  <?php
                      $ed = 1;
                      if(!empty($shape_images)){
                      foreach ($shape_images as $product_image) { ?>
                      
                          <img class="rounded p-1 w-100 h-auto transtion" src="<?php echo base_url($product_image->ImageName); ?>?h=60"/>
                          <?php $ed++;
                      } }
                      else{ 
                      if(isset($shapeDetail->ShapeImage))
                      {
                        ?>
                        <img class="rounded p-1 w-100 h-auto transtion" src="<?php echo base_url($shapeDetail->ShapeImage); ?>?h=60"/>
                      <?php
                      }
                    }
                  ?>
              </div>
          </div>
        </div>
      </div>
      <!-- Start Product Details -->
      <div class="col-xl-5 col-lg-5 col-md-4 col-sm-6">
        <div class="product-info ">
          <div class="row ar-row">
            <div class="col-xs-12">
              <h2 class="mi-product-title"><?= @$shapeDetail->Title ?></h2>
              <p class="mi-product-desc"><?= @$shapeDetail->Description ?></p>
            </div>
            <div class="col-xs-12">
              <h5 class="mi-txt-price">


                <span class="mi-currency" style=""><?php echo lang('SAR'); ?></span>
                <?php
                    $Price = @$shapeDetail->PricePerKG;
                ?>
                <span id="PriceP" class="mi-PriceP" style=""><?= get_taxt_amount($Price)+$Price ?></span>
                <span style="" id="PriceT">Per KG</span>

              </h5>
              <span class="stock-normal">
                  <i class="fa fa-check-circle" style="font-size:1.6rem;color: #00ae42;"></i> 
                  <?= lang('in_stock_online')?>
              </span>

              
              
            </div>
          </div>
          
            <div class="row ar-row">
              <div class="col-xs-6">
                  <h4 class="no-border"><?php echo lang('specifications'); ?></h4> 
              </div>
                <div class="col-xs-6 mi-nutritionBtn">
                  <button class="nutration-facts-btn nutrition-pad-left nutrition-pad-right"><a class="pointer mi-pointer" data-toggle="modal" data-target="#nutritionInfo"><?php echo lang('nutrition_info'); ?></a></button>
              </div>
          </div>
          

          <h4></h4>
          <div class="row ar-row">
            <div class="col-xs-12 col-md-12">
            <?php echo $shapeDetail->Specifications; ?>     
            </div>                                              
          </div>
          <h5 class="bordered mi-bordered"></h5>
          </div>
        
          <div class="row">
          <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 text-right p-3">
            <button type="button" style="" class=" mi-check-availability-btn " data-toggle="modal" data-target="#chk-avail">Service Available At Workshop</button>
          </div>
        </div>
        <div class="row ar-row">
          <div class="pull-center-mobile start-customize mb-4 d-flex ">
            <a href="<?php echo base_url('customize/choco_shape_engraved/'.base64_encode($shapeDetail->ShapeID)); ?>" class="btn btn-primary ">
              Start Customization
              <span class="d-inline-block pl-2">
                <svg class="svg-inline--fa fa-pencil-alt fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas"
                  data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                  <path fill="currentColor"
                    d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z">
                  </path>
                </svg><!-- <i class="fas fa-pencil-alt"></i> -->
              </span>
            </a>
          </div>
        </div>



          
        </div>
      
    
      <aside class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
        <!-- Product Action -->
        <div class="mi-product-action rounded border-right  p-4">
          <a href="<?= site_url('faq');?>">
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <span class="mi-order-icon mi-disp-inline ">
                  <svg class="svg-inline--fa fa-store fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas"
                    data-icon="store" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 616 512"
                    data-fa-i2svg="">
                    <path fill="currentColor"
                      d="M602 118.6L537.1 15C531.3 5.7 521 0 510 0H106C95 0 84.7 5.7 78.9 15L14 118.6c-33.5 53.5-3.8 127.9 58.8 136.4 4.5.6 9.1.9 13.7.9 29.6 0 55.8-13 73.8-33.1 18 20.1 44.3 33.1 73.8 33.1 29.6 0 55.8-13 73.8-33.1 18 20.1 44.3 33.1 73.8 33.1 29.6 0 55.8-13 73.8-33.1 18.1 20.1 44.3 33.1 73.8 33.1 4.7 0 9.2-.3 13.7-.9 62.8-8.4 92.6-82.8 59-136.4zM529.5 288c-10 0-19.9-1.5-29.5-3.8V384H116v-99.8c-9.6 2.2-19.5 3.8-29.5 3.8-6 0-12.1-.4-18-1.2-5.6-.8-11.1-2.1-16.4-3.6V480c0 17.7 14.3 32 32 32h448c17.7 0 32-14.3 32-32V283.2c-5.4 1.6-10.8 2.9-16.4 3.6-6.1.8-12.1 1.2-18.2 1.2z">
                    </path>
                  </svg>
                </span>
                <div class="mi-disp-inline">
                  <span class="mi-order-title mi-disp-inline-block"><?php echo lang('collect_from_store'); ?></span>
                  <p class="mi-order-content">
                    <?php echo lang('order_now_and_collect_from_store'); ?>
                    <br>
                    <span class="learn-more"><?php echo lang('learn_more'); ?></span>
                  </p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <span class="mi-order-icon mi-disp-inline ">
                  <svg class="svg-inline--fa fa-shipping-fast fa-w-20" aria-hidden="true" focusable="false"
                    data-prefix="fas" data-icon="shipping-fast" role="img" xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 640 512" data-fa-i2svg="">
                    <path fill="currentColor"
                      d="M624 352h-16V243.9c0-12.7-5.1-24.9-14.1-33.9L494 110.1c-9-9-21.2-14.1-33.9-14.1H416V48c0-26.5-21.5-48-48-48H48C21.5 0 0 21.5 0 48v320c0 26.5 21.5 48 48 48h16c0 53 43 96 96 96s96-43 96-96h128c0 53 43 96 96 96s96-43 96-96h48c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zM160 464c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm320 0c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm80-208H416V144h44.1l99.9 99.9V256z">
                    </path>
                  </svg>
                </span>
                <div class="mi-disp-inline">
                  <span class="mi-order-title mi-disp-inline-block"><?php echo lang('Delivery'); ?></span>
                  <p class="mi-order-content">
                    <?php echo lang('Order_now_and_your_order_will_be_delivered_in_two_to_five_days'); ?>
                    <br>
                    <span class="learn-more"><?php echo lang('learn_more'); ?></span>
                  </p>
                </div>
              </div>
             
            </div>
            <!-- <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <span class="mi-order-icon mi-disp-inline ">
                                <svg class="svg-inline--fa fa-biking fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="biking" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" data-fa-i2svg=""><path fill="currentColor" d="M400 96a48 48 0 1 0-48-48 48 48 0 0 0 48 48zm-4 121a31.9 31.9 0 0 0 20 7h64a32 32 0 0 0 0-64h-52.78L356 103a31.94 31.94 0 0 0-40.81.68l-112 96a32 32 0 0 0 3.08 50.92L288 305.12V416a32 32 0 0 0 64 0V288a32 32 0 0 0-14.25-26.62l-41.36-27.57 58.25-49.92zm116 39a128 128 0 1 0 128 128 128 128 0 0 0-128-128zm0 192a64 64 0 1 1 64-64 64 64 0 0 1-64 64zM128 256a128 128 0 1 0 128 128 128 128 0 0 0-128-128zm0 192a64 64 0 1 1 64-64 64 64 0 0 1-64 64z"></path></svg>
                            </span>
                            <div class="mi-disp-inline">
                                <span class="mi-order-title mi-disp-inline-block"><?php echo lang('fast_delivery'); ?></span>
                                <p class="mi-order-content">   
                                <?php echo lang('delivered_in_business_days'); ?><span class="learn-more"><?php echo lang('learn_more'); ?></span>.
                                </p>    
                            </div>
                        </div>                       
                    </div> -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <span class="mi-order-icon mi-disp-inline ">
                  <svg class="svg-inline--fa fa-truck fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas"
                    data-icon="truck" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"
                    data-fa-i2svg="">
                    <path fill="currentColor"
                      d="M400 96a48 48 0 1 0-48-48 48 48 0 0 0 48 48zm-4 121a31.9 31.9 0 0 0 20 7h64a32 32 0 0 0 0-64h-52.78L356 103a31.94 31.94 0 0 0-40.81.68l-112 96a32 32 0 0 0 3.08 50.92L288 305.12V416a32 32 0 0 0 64 0V288a32 32 0 0 0-14.25-26.62l-41.36-27.57 58.25-49.92zm116 39a128 128 0 1 0 128 128 128 128 0 0 0-128-128zm0 192a64 64 0 1 1 64-64 64 64 0 0 1-64 64zM128 256a128 128 0 1 0 128 128 128 128 0 0 0-128-128zm0 192a64 64 0 1 1 64-64 64 64 0 0 1-64 64z">
                    </path>
                  </svg>
                </span>
                <div class="mi-disp-inline">
                  <span class="mi-order-title mi-disp-inline-block"><?php echo lang('free_delivery'); ?></span>
                  <p class="mi-order-content">
                    <?php
                                $shippingCharges = getShipmentMaxAmount(); 
                                echo lang('Free_delivery_starts_from').$shippingCharges.lang('SAR_or_more'); ?>
                    <br><span class="learn-more"><?php echo lang('learn_more'); ?></span>.
                  </p>
                </div>
              </div>
            </div>
          </a>
          <!-- <a href="<?= site_url('return-policy');?>"> -->
          <!-- <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <span class="mi-order-icon mi-disp-inline ">
                                <svg class="svg-inline--fa fa-exclamation-circle fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="exclamation-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                            </span>
                            <div class="mi-disp-inline">
                                <span class="mi-order-title mi-disp-inline-block"><?php echo lang('fast_delivery'); ?></span>
                                <p class="mi-order-content">   
                                <?php echo lang('up_to_14_days_from_the_date_of_order'); ?><span class="learn-more"><?php echo lang('learn_more'); ?></span>.
                                </p>    
                            </div>
                        </div>                       
                    </div> -->
          <!-- </a> -->
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="mi-share-on-social">
                <a class="facebook transtion" href="<?= @$site_setting->FacebookUrl?>"><svg
                    class="svg-inline--fa fa-facebook fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab"
                    data-icon="facebook" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                    data-fa-i2svg="">
                    <path fill="currentColor"
                      d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z">
                    </path>
                  </svg><!-- <i class="fab fa-facebook"></i> --> </a>
                <a class="twitter transtion" href="<?= @$site_setting->TwitterUrl?>"><svg
                    class="svg-inline--fa fa-twitter fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab"
                    data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                    data-fa-i2svg="">
                    <path fill="currentColor"
                      d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z">
                    </path>
                  </svg><!-- <i class="fab fa-twitter"></i> --> </a>
                <a class="whatsapp transtion" href=" https://wa.me/<?= @$site_setting->Whatsapp?>"><svg
                    style="color: green;" class="svg-inline--fa fa-whatsapp fa-w-14" aria-hidden="true"
                    focusable="false" data-prefix="fab" data-icon="whatsapp" role="img"
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                    <path fill="currentColor"
                      d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z">
                    </path>
                  </svg><!-- <i class="fab fa-whatsapp"></i> --> </a>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
<?php
if ($featured_products) { ?>
    <section class="content three">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h2>Related <span><?php echo lang('products'); ?></span></h2>
                    </div>
                    <div class="procarousel">
                        <div id="shapes_related" class="owl-carousel owl-theme ">
                            <?php
                            foreach ($featured_products as $item) { ?>
                                <div class="item">
                                    <a href="<?php echo base_url('customize/choco_shape_details/'.base64_encode(@$item->ShapeID)); ?>">
                                        <img src="<?php echo base_url($item->ShapeImage); ?>">
                                        <h4><?php echo @$item->Title; ?></h4>
                                    </a>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="text-center btn-row">
                            <a href="<?php echo base_url('product'); ?>"
                               class="btn btn-primary"><?php echo lang('view_all'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }
?>
<!-- Modal -->
<div id="nutritionInfo" class="modal fade nutritionInfoModal" role="dialog">
  <div class="modal-dialog mi-modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close mi-close" data-dismiss="modal">&times;</button>
        <h2 class="mi-txt-align"><?php echo lang('Nutrition_Facts'); ?></h2>
      </div>
      <div class="modal-body">
      <table class="table">
<colgroup>
<col style="width: 156pt" width="208">
<col style="width: 56pt" width="75">
<col style="width: 67pt" width="89">
</colgroup>
<tbody>
<tr>
<td colspan="4" width="372"><p><b><?php echo $result->Title; ?></b></p></td>
</tr>
<!--<tr>
<td colspan="3" width="372"><span>Servings Per Container about 8</span></td>
</tr>
<tr>-->
<td class="text-center" colspan="3"><b class="gold"><?php echo lang('Nutrition'); ?> &nbsp;</b></td>
<td class="text-center" width="89"><b class="gold"> Value</b></td>
</tr>
<?php if(!empty($product_nutritions)){
    foreach ($product_nutritions as $key => $value) { ?>
    <tr>
        <td class="text-center" colspan="3"><b><?php echo $value['Title']; ?></b></td>
        <td  width="89"><span><?php echo $value['Quantity']; ?></span></td>
       
    </tr>

    <?php   
    }
} ?>

<!--<tr>
<td colspan="3"  width="372"><span>Vitamin D 4%• Potassium 2% • Calcium 2% • Iron 6%&nbsp;</span></td>
</tr>-->
<tr height="33" style="height: 24.75pt">
<td colspan="4" width="372">
<p class="goldBlack mi-goldBlack"><b>Ingredients:</b> <?php echo $shapeDetail->Ingredients; ?></p>
</td>
</tr>
</tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>

  </div>
</div>

<!-- Availiblity Modal -->
<div class="modal fade" id="chk-avail" tabindex="1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">

  <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
  <div class="modal-dialog modal-dialog-centered modWl mi-modal" role="document">


    <div class="modal-content mod-cont">
      <div class="modal-header mod_hed" >
        <h5 class="modal-title hdTxt" id="exampleModalLongTitle"><?php echo lang('check');?> <span><?php echo lang('availability'); ?></span> </h5>
      </div>
      <div class="modal-body mod-bod">
        <div class="frmcity">
           <p><strong><?php echo lang('please_select_city'); ?></strong></p>
              <div class="form-group edSelectstyle  white" >
                   <select class="fmod form-control check_availability" style="">
                      <option><?php echo lang('check');?> <?php echo lang('availability'); ?></option>
                       <?php 
                       $branches_html = '';
                       $available = false;

                       if($available_cities){
                        $ontime_array = array();
                                               
                                  foreach ($available_cities as $key => $value) {
                                            
                                            $quantity = '<span class="gTxt">'.lang('available').'</span>';
                                            if(!in_array($value->CityID,$ontime_array)){?>
                                        
                                        <option value="<?php echo $value->CityID; ?>"><?php echo $value->CityTitle; ?></option> 

                                    <?php
                                }

                                $branches_html .= '<div class="branchTXT check_availability_city city-'.$value->CityID.'" style="display:none;">
                                                 <p class="hTxt"><strong>'.$value->StoreTitle.'</strong><br>
                                                 <span>'.$value->Address.'</span><br>
                                                '.$quantity.'</p>
                                             </div>';
                                $ontime_array[] = $value->CityID;
                                 }
                       }

                       ?>
                                                                                        
                      </select>
                      <p id="show_availability" style="display: none;"></p>
                  </div>
              </div>
        <div class="rsltTxt">
                         <h6><?php echo lang('results'); ?></h6>
                     </div>

                     <?php echo $branches_html; ?>
        </div>
        <div class="modal-footer mod_fot text-center">
            <div class="text-right cls-bt">
                <button type="button" class="btn-success close-icon" data-dismiss="modal"><?php echo lang('Close'); ?></button>
            </div>
        </div>
      </div>
    </div>
</div>
<script>
  $( document ).ready(function(){
    $('#shapes_related').owlCarousel({   
      center: true,
      loop: true,
      margin: 70,
      nav: true,
      dots: false,
      autoWidth:true,
      autoplay: true,
      autoplayTimeout: 2000,
      autoplayHoverPause: true,  
      responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            700: {
                items: 3
            },
            1000: {
                items: 4
            }
        } 
    });
    $('#shapes_related').find('.owl-nav').removeClass('disabled');
    $('#shapes_related').on('changed.owl.carousel', function(event) {
      $(this).find('.owl-nav').removeClass('disabled');
    });
    $('.check_availability').on('change',function(){
            var city_id = $(this).find(':selected').val();
            $('.check_availability_city').hide();

            $('.city-'+city_id).show();
    });

    $('#thumbs').delegate('img','click', function(){
    $('#main').attr('src',$(this).attr('src').replace('thumb','large'));
    $('#main').attr('data-zoom',$(this).attr('src'));
    });

    var demoTrigger = document.querySelector('.drift-demo-trigger');
    var paneContainer = document.querySelector('.product-img-Zoom');

    new Drift(demoTrigger, {
        paneContainer: paneContainer,
        inlinePane: false,
    });

    function scrollWin() {
      window.scrollBy(0, -200);
    }
    
});

</script>