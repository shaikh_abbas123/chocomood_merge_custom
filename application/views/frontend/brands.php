<?php
$URL_Ids = getURLIds();
$category_ids_in_url = $URL_Ids['category_ids'];
$collection_ids_in_url = $URL_Ids['collection_ids'];
?>
<link rel="stylesheet" href="<?php echo front_assets(); ?>jPages/css/jPages.css">
<link rel="stylesheet" href="<?php echo front_assets(); ?>jPages/css/animate.css">
<script src="<?php echo front_assets(); ?>jPages/js/highlight.pack.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/tabifier.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/js.js"></script>
<script src="<?php echo front_assets(); ?>jPages/js/jPages.js"></script>
<style>
    .holder {
        margin: 15px 0;
    }

    .holder a {
        font-size: 12px;
        cursor: pointer;
        margin: 0 5px;
        color: #333;
    }

    .holder a:hover {
        background-color: #222;
        color: #fff;
    }

    .holder a.jp-previous {
        margin-right: 15px;
    }

    .holder a.jp-next {
        margin-left: 15px;
    }

    .holder a.jp-current,
    a.jp-current:hover {
        color: #FF4242;
        font-weight: bold;
    }

    .holder a.jp-disabled,
    a.jp-disabled:hover {
        color: #bbb;
    }

    .holder a.jp-current,
    a.jp-current:hover,
    .holder a.jp-disabled,
    a.jp-disabled:hover {
        cursor: default;
        background: none;
    }

    .holder span {
        margin: 0 5px;
    }


    .content.products .inbox .add_wishlist_to_cart {
        position: absolute;
        top: 7px;
        <?php echo($lang=='AR'? 'left' : 'right');
        ?>: 55px;
        bottom: auto;
        <?php echo($lang=='AR'? 'right' : 'left');
        ?>: auto;
        color: #fb7176;
        font-size: 20px;
    }

    .btnrow {
        margin-top: 25px;
    }
    .custom-container{
        width: 90% !important;
    }
    #sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand{
        margin: 0!important;
        color: #50456d !important;
        background-color: #f4ebd3 !important;
        border: none !important;
        border-radius: 0 !important;
    /* border-bottom: 1px solid #adadad !important; */
        font-size: 15px !important;
        font-weight: bold !important;
        padding: 15px 35px !important;
        /* text-transform: capitalize !important; */
    }
    .collapse-span{
      background-color:#F4EBD3;

    }
    .collpase.collapse-span{
        padding-bottom:10px;
        display:table;
        margin-right:auto;
        margin-left:auto;

    }
    .choco-name{
         padding:7px 7px 7px 30px !important;
         text-align:center;
         font-size:16px;
         color:#000 !important;
    }
    .choco-name:hover{
        background-color:#d9d0b8;
    }
    #sidebar1 .subList-item-product, #sidebar1 .subList-item-brand{
        margin: 0!important;
        color: black !important;
        /* background-color: #f4ebd3 !important; */
        border: none !important;
        border-radius: 0 !important;
        font-size: 14px !important;
        font-weight: bold !important;
        padding: 15px 35px !important;

    }
        .submenu {
        text-align:center;
        transition: .3s !important;
        }
    .left-submenu{
        display: inline-block;
          width: 150px;
          font-size:16px;
          color:#000 !important;
    }
     #sidebar1 .subList-item-product:hover{
        background-color:#e9e8e6  !important;
        /* padding:0 10p 0 10px !important; */
    }
    .left-submenu:hover{
        background-color:#e9e8e6  !important;
        /* padding: 0px 30px 0px 30px !important; */
    }
    .right-submenu{
        /* background-color:#A1826A !important; */
        display: inline-block;
         width: 50px;
         color:#000 !important;
    }
    .priceRange{
        text-align:center;
        padding:10px;
    }
    .priceRange span{
        display:inline-block;
        margin:0px 5px;
        color: #50456d !important;
    }
        input[type=number] {
        border: 1px solid #ddd;
        text-align: center;
        border-radius:10px;
        padding:8px;
    }
    .customradio,.customcheck{
        color: #50456d !important;
    }
    /* .filter-column{
        display:flex;
        justify-content:center;
    } */
    @media (max-width: 1500px) {
        #sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand{
            font-size: 15px !important;
            padding: 15px 15px !important;
        }
        #sidebar1 .subList-item-product {
            padding: 15px 15px !important
        }
        }
        @media screen and (min-width:992px) and (max-width:1200px) {
        #sidebar1 .list-group-item.list-item-product, #sidebar1 .list-group-item.list-item-brand {
            font-size: 14px !important;
            padding: 12px 8px !important;
        }
    }
    .submenu:hover {
    background: #e9e8e6;
}
    .dv-hide-desktop {
        display: none;
        position: fixed;
        top: 122px;
        background: #32231f;
        width: 100%;
        z-index: 9;
        height: 100%;
        overflow-y: scroll;
        padding-bottom: 130px;
        padding-top:30px;
    }
    .dv-hide-desktop select.form-control{
    background-color:
    transparent;
    color:#fff;
    border: 1px solid #f4ebd3;
    margin-bottom: 10px !important;
    width: 100%;
    margin:0 auto;
    }
    .dv-hide-desktop h6{
        color:#fff;
        font-size:16px;
        width: 93%;
        margin-bottom:20px !important;
        margin:0 auto;
    }
    .dv-hide-desktop h6 span{
        float:right;
    }
    img.only-mob {
        display: none;
    }

    @media only screen and (max-width:600px) {
        img.only-mob {
            display: block;
        }
        .col-md-12.holder a {
            display: none;
        }
        .col-md-12.holder span{
            display:none;
        }
        .col-md-12.holder a.jp-previous, .col-md-12.holder a.jp-next {
            display: inline-block;
            width:40%;
        
        }
        .custom-container.on-mob-w-100 {
            width: 100% !important;
        }


    }

    @media only screen and (max-width:601px) {
        .to-hide-600 {
            display: none;
        }
    }

    img.only-mob {
        position: fixed;
        top: 141px;
        right: 16px;
        z-index: 9;
    }
    /* @media screen and (min-width:992px) and (max-width: 1599px) {
     #sidebar1 .subList-item-product, #sidebar1 .subList-item-brand {
    padding: 15px 15px !important;
}
    } */






    
a .left-submenu,
a .right-submenu,
a .choco-name {
    color: #50456d !important;
    font-weight: bold;
    font-size: 12px;
}

a .right-submenu {
    color: #50456d !important;
    font-weight: 800 !important;
    font-size: 13px !important;
}

#sidebar1 .collapse.in {
    border-bottom: 1px solid #ddd !important;
}

div#sidebar1 .list-group .collapse.in #sidebar1 .collapse.in:last-child {
    border-bottom: none !important;
}
@media screen and (min-width:1201px) {
    .choco-name .right-submenu {
            text-align: center !important;
        }
}
@media screen and (min-width:1400px) {
    .choco-name .right-submenu {
            text-align: right !important;
        }
}
@media screen and (min-width:1650px) {
    .choco-name .right-submenu {
            text-align: center !important;
        }
}

/* .filter-column{
        display:flex;
        justify-content:center;
    } */
@media (max-width: 1500px) {

    #sidebar1 .list-group-item.list-item-product,
    #sidebar1 .list-group-item.list-item-brand {
        font-size: 15px !important;
        padding: 15px 15px !important;
    }
}

@media screen and (min-width:1400px) and (max-width: 1650px) {
    .choco-name {
        padding: 7px 30px 10px 25px !important;
    }
}

@media screen and (min-width:1201px) and (max-width: 1399px) {
    .choco-name {
        padding: 7px 22px 10px 0px !important;
    }

    .submenu {
    padding: 7px 7px 7px 30px !important;
}
    .choco-name .left-submenu {
        width: 100px !important;
    }

    /* .choco-name .right-submenu {
        text-align: right !important;
    } */
}
i.bi.bi-plus,
.bi.bi-dash{
    font-size: 22px;
}
.submenu {
    padding: 7px 7px 7px 30px !important;
}
@media screen and (min-width:1200px) {
    div#sidebar1 .list-group {
        padding-bottom: 5px !important;
    }
}

.list-group.list-group-brand {
    margin-bottom: 20px;
}
</style>

<section class="content products titlarea pt-0">
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="filter">
                    <div class="row">
                        <div class="col-md-6 col-sm-5">
                            <h6>
                                <i class="fa fa-filter" aria-hidden="true"></i>
                                <?php echo lang('filter'); ?>
                                
                            </h6>
                        </div>
                        <div class="col-md-3 col-sm-3">
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <select class="form-control ProductsPerPage">
                                <option selected disabled><?php echo lang('product_per_page'); ?></option>
                                <option>9</option>
                                <option>15</option>
                                <option>20</option>
                            </select>
                        </div>
                        <div class="col-md-1 iconsrow text-right col-sm-2">
                            <a onclick="changeGridLayout('items_list');"><i class="fa fa-list d-none"></i></a>
                            <a onclick="changeGridLayout('items_grid');"><i class="fa fa-th d-none"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content products">
    <div class="container-fluid custom-container on-mob-w-100">
        <div class="row">
            <div class="col-lg-2 col-xs-12 to-hide-600" id="sidebar1">
                <div class="row">
                    <div class="col-xs-6 col-lg-12">
                    <div class="list-group">
                    <a href="#menu-cate-product" onclick="myFunction(this)" class="list-group-item collapsed list-item-product plusToggle" data-toggle="collapse"
                        data-parent="#sidebar" aria-expanded="false">
                        <span>Products<i  class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                    </a>
                    <div class="collapse" id="menu-cate-product" aria-expanded="false" style="height: 0px;">
                    <div class="collapse-span">
                            <?php $menu_categories = subCategories(0,$language);
                                if($menu_categories){ 
                                    foreach ($menu_categories as $key => $category) {?>
                                    <div class="list-group">
                                        <a href="#submenu-cate-product<?=$category->CategoryID?>" onclick="myFunction(this)" class="list-group-item collapsed subList-item-product" data-toggle="collapse"
                                            data-parent="#sidebar" aria-expanded="false">
                                            <span><?= $category->Title?><i  class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                                        </a>
                                        <div class="collapse" id="submenu-cate-product<?=$category->CategoryID?>" aria-expanded="false" style="height: 0px;" style="text-align:left">
                                        <?php $sub_cat = subCategories($category->CategoryID, $lang);
                                        foreach ($sub_cat as $k => $v) {?>
                                         <a href="<?php echo base_url('product');?>?q=<?php echo strtolower(str_replace(' ','-',$v->Title)); ?><?php echo '-s'.$v->CategoryID;?>">
                                            <div class="submenu">
                                                <span class="left-submenu"><?= $v->Title?></span>
                                                <span class="right-submenu"><?= count_product($v->CategoryID,'subcat');?></span> 
                                            </div> 
                                        </a>
                                        <?php }?>
                                        
                                        
                                        </div>
                                    </div>
                            <?php 
                                    }
                                }
                            ?>       
                    </div>
                        
                    </div>
                </div>
                    </div>
                    <div class="col-xs-6 col-lg-12">
                    <div class="list-group list-group-brand">
                    <a href="#menu-cate-brands" onclick="myFunction(this)" class="list-group-item collapsed list-item-brand" data-toggle="collapse"
                        data-parent="#sidebar" aria-expanded="false">
                        <span>Brands<i class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                    </a>
                    
                    <div class="collapse" id="menu-cate-brands" aria-expanded="false" style="height: 0px;">
                    <div class="collapse-span">
                        <?php
                            foreach ($brands as $key => $p) {
                        ?>
                            <a href="<?= base_url() ?>product/brand_detail?q=<?=strtolower(str_replace(' ','-',$p->brand_name))?>-s<?=$p->brand_id?>">
                            <div class="choco-name">
                            <span class="left-submenu"><?= $p->brand_name?></span>
                                <span class="right-submenu"><?= count_product($p->brand_id,'brand');?></span> 
                            </div>
                            </a>
                          
                        <?php 
                            }
                        ?>
                        <a href="<?= base_url() ?>product/brand_detail?q=chocomood-s0">
                            <div class="choco-name">
                            <span class="left-submenu"><?=lang('chocomood')?></span>
                                <span class="right-submenu"><?= count_product(0,'brand');?></span> 
                            </div>
                        </a>
                    </div>               
                    </div>
                </div>
                    </div>
                </div>
                
               
            </div>
            <div class="col-lg-8 col-xs-12 margin-bottom-40">
                <div class="row">
                    <div class="col-md-12" id="Product-Listing">
                        <?php echo brand_html($brands); ?>
                    </div>
                    <div class="col-md-12 holder"
                        <?php echo (empty($brands) ? 'style="display:none;"' : 'style="display:block;"'); ?>></div>
                    <div class="col-md-12 btnrow text-center" style="display: none;">
                        <input type="hidden" value="0" id="Page">
                        <button id="loadmore" class="get_products loadmore btn btn-primary"
                            <?php echo((count($brands) <= $countbrands) ? 'style="display:none;"' : ''); ?>><?php echo lang('load_more'); ?></button>
                    </div>
                </div>
            </div>
            <?php if (!isset($search)) { ?>
            <div class="col-md-2 to-hide-600" id="sidebar">
            </div>
            <?php } ?>
        </div>
        <div class="row dv-hide-desktop container-fluid">
        <div class="filter">
            <div class="row">
                <div class="col-md-6 col-sm-5">
                    <h6>
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        <?php echo lang('filter'); ?>
                        
                    </h6>
                </div>
                <div class="col-md-3 col-sm-3">
                </div>
                <div class="col-md-2 col-sm-2">
                    <select class="form-control ProductsPerPage">
                        <option selected disabled><?php echo lang('product_per_page'); ?></option>
                        <option>9</option>
                        <option>15</option>
                        <option>20</option>
                    </select>
                </div>
                <div class="col-md-1 iconsrow text-right col-sm-2 to-hide-600">
                    <a onclick="changeGridLayout('items_list');"><i class="fa fa-list d-none"></i></a>
                    <a onclick="changeGridLayout('items_grid');"><i class="fa fa-th  d-none"></i></a>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-md-2" id="sidebar1">
                    <div class="list-group">
                        <a href="#menu-cate-product-mob" onclick="myFunction(this)" class="list-group-item collapsed list-item-product plusToggle" data-toggle="collapse"
                            data-parent="#sidebar" aria-expanded="false">
                            <span>Products<i  class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                        </a>
                        <div class="collapse" id="menu-cate-product-mob" aria-expanded="false" style="height: 0px;">
                        <div class="collapse-span">
                                <?php $menu_categories = subCategories(0,$language);
                                    if($menu_categories){ 
                                        foreach ($menu_categories as $key => $category) {?>
                                        <div class="list-group">
                                            <a href="#submenu-cate-product-mob<?=$category->CategoryID?>" onclick="myFunction(this)" class="list-group-item collapsed subList-item-product" data-toggle="collapse"
                                                data-parent="#sidebar" aria-expanded="false">
                                                <span><?= $category->Title?><i  class="bi bi-plus" style="float:right !important" aria-hidden="true"></i></span>
                                            </a>
                                            <div class="collapse" id="submenu-cate-product-mob<?=$category->CategoryID?>" aria-expanded="false" style="height: 0px;" style="text-align:left">
                                            <?php $sub_cat = subCategories($category->CategoryID, $lang);
                                            foreach ($sub_cat as $k => $v) {?>
                                            <a href="<?php echo base_url('product');?>?q=<?php echo strtolower(str_replace(' ','-',$v->Title)); ?><?php echo '-s'.$v->CategoryID;?>">
                                                <div class="submenu">
                                                    <span class="left-submenu"><?= $v->Title?></span>
                                                    <span class="right-submenu"><?= count_product($v->CategoryID,'subcat');?></span> 
                                                </div> 
                                            </a>
                                            <?php }?>
                                            
                                            
                                            </div>
                                        </div>
                                <?php 
                                        }
                                    }
                                ?>       
                        </div>
                            
                        </div>
                    </div>
                    <div class="list-group list-group-brand">
                        <a href="#menu-cate-brands-mob" onclick="myFunction(this)" class="list-group-item collapsed list-item-brand" data-toggle="collapse"
                            data-parent="#sidebar" aria-expanded="false">
                            <span>Brands<i class="fa fa-plus" style="float:right !important" aria-hidden="true"></i></span>
                        </a>
                        
                        <div class="collapse" id="menu-cate-brands-mob" aria-expanded="false" style="height: 0px;">
                        <div class="collapse-span">
                            <?php
                                foreach ($brands as $key => $p) {
                            ?>
                                <a href="<?= base_url() ?>product/brand_detail?q=<?=strtolower(str_replace(' ','-',$p->brand_name))?>-s<?=$p->brand_id?>">
                                <div class="choco-name">
                                <span class="left-submenu"><?= $p->brand_name?></span>
                                    <span class="right-submenu"><?= count_product($p->brand_id,'brand');?></span> 
                                </div>
                                </a>
                            
                            <?php 
                                }
                            ?>
                            <a href="<?= base_url() ?>product/brand_detail?q=chocomood-s0">
                                <div class="choco-name">
                                <span class="left-submenu"><?= lang('chocomood')?></span>
                                    <span class="right-submenu"><?= count_product(0,'brand');?></span> 
                                </div>
                            </a>
                        </div>               
                        </div>
                    </div>
                </div>
                
                <?php if (!isset($search)) { ?>
                
                <?php } ?>
            </div>
        </div>
        <img src="<?= base_url('assets/frontend/images/filter-img.png')?>" class="only-mob">
    </div>
</section>


<button class="get_products" id="hidden_btn" style="display: none;">&nbsp;</button>
<input type="hidden" value="0" id="hidden_price">
<script>
    $(function () {
        $("#slider-range").slider({
            range: true,
            min: 1,
            max: 1000,
            values: [75, 300],
            slide: function (event, ui) {
                $("#amount").val(ui.values[0] + "-" + ui.values[1]);

            },
            stop: function (event, ui) {
                $('#hidden_price').val(1);
                $('#hidden_btn').click();


            }
        });
        $("#amount").val($("#slider-range").slider("values", 0) +
            "-" + $("#slider-range").slider("values", 1));


    });
</script>
<script>
    $(document).ready(function () {
        $( 'body' ).on( 'click', '.plusToggle', function () {
            console.log("AB");
            console.log($(this).attr("href"));
        });

        <?php
        if (isset($CollectionID)) {
            ?>
            setTimeout(function () {
                $('.checked_this').click();
            }, 500); 
            <?php
        } ?>



        $(document).on("input", "#search_tags", function () {
            var v = $(this).val();
            v = v.toLowerCase();
            $('.search-labels').each(function () {
                var str = $(this).attr('data-text');
                var res = str.toLowerCase();

                if (res.indexOf(v) >= 0) {
                    $(this).show();

                } else {
                    $(this).hide();
                }

            });

        });



        function addTitleToUrl(titles) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname +
                "?q=" + titles;
            window.history.pushState({
                path: newurl
            }, '', newurl);
        }

        function removeTitleToUrl() {
            window.history.replaceState(null, null, "/product");
        }
    });

    $(".sorting").on('change', function (e) {
        $('.overlaybg').fadeIn();
        var sort_val = $(this).val();
        if (sort_val == 'price_highest_to_lowest'); {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'price',
                order: 'desc'
            });
        }
        if (sort_val == 'price_lowest_to_higher') {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'price'
            });
        }
        if (sort_val == 'product_newer_to_older') {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'pid',
                order: 'desc'
            });
        }
        if (sort_val == 'product_older_to_newer') {
            tinysort('.single_product', {
                selector: 'strong',
                data: 'pid'
            });
        }
        setTimeout(function () {
            $('.overlaybg').fadeOut();
        }, 500);
    });

    function sortProducts(sort_val) {
        var current_url = $(location).attr('href');
        url = new URL(window.location.href);
        if (url.searchParams.get('q')) {
            // append sort value at the end
            if (url.searchParams.get('sort')) {
                current_url = removeParam('sort', current_url);
                current_url += "&sort=" + sort_val;
            } else {
                current_url += "&sort=" + sort_val;
            }
        } else {
            // append sort value at the start
            if (url.searchParams.get('sort')) {
                current_url = removeParam('sort', current_url);
                current_url += "sort=" + sort_val;
            } else {
                current_url += "?sort=" + sort_val;
            }
        }
        window.location.href = current_url;
    }

    function removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }
</script>
<script>
    /* when document is ready */
    $(function () {

        /* initiate plugin */
        $("div.holder").jPages({
            containerID: "productsContainer",
            perPage: 6,
            animation: "fadeInLeft",
            keyBrowse: true
        });

        /* on select change */
        $(".ProductsPerPage").change(function () {
            /* get new nº of items per page */
            var newPerPage = parseInt($(this).val());

            /* destroy jPages and initiate plugin again */
            $("div.holder").jPages("destroy").jPages({
                containerID: "productsContainer",
                perPage: newPerPage,
                animation: "fadeInLeft",
                keyBrowse: true
            });
        });

    });

    function changeGridLayout(cls) {

        $('.overlaybg').fadeIn();
        $('#Product-Listing > .row > .col-md-12 > ul').removeClass('items_grid');
        $('#Product-Listing > .row > .col-md-12 > ul').removeClass('items_list');
        setTimeout(function () {
            $('.overlaybg').fadeOut();
            $('#Product-Listing > .row > .col-md-12 > ul').addClass(cls);
        }, 500);
    }
    function myFunction(x) {
        x.childNodes[1].childNodes[1].classList.toggle('bi-plus');
    x.childNodes[1].childNodes[1].classList.toggle('bi-dash');
    }
</script>

<script>
    if ($(window).width() < 601) {
        $("img.only-mob").click(function () {
            $(".dv-hide-desktop").slideToggle("fast");
        });
    } else {
        $(".dv-hide-desktop").hide();
    }
 </script>