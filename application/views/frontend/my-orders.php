<style>
   .content.products .inbox i.add_wishlist_to_cart {
   position: absolute;
   top: 10px;
   right: 55px;
   bottom: auto;
   left: auto;
   color: #fb7176;
   font-size: 20px;
   }
   .msgbox {
   overflow: scroll;
   height: 600px;
   }
   .msgreceive {
   padding-left: 40px !important;
   }
   .no-orders-text{
   }
</style>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<section class="content products checkout address myaccount">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h2><?php echo lang('my_account'); ?> <span><?php echo $user->FullName; ?></span></h2>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="wbox">
               <ul class="nav nav-tabs tabsInBoxes">
                  <li>
                     <a href="<?php echo base_url('account/profile');?>">
                        <?php echo lang('my_information'); ?>
                        <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                        <span class="iconEd"><i class="fa fa-user"></i></span>
                     </a>
                  </li>
                  <li class="active">
                     <a href="<?php echo base_url('account/orders');?>">
                        <?php echo lang('orders'); ?>
                        <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                        <span class="iconEd"><i class="fa fa-shopping-cart"></i></span>
                     </a>
                  </li>
                  <li>
                     <a href="<?php echo base_url('account/addresses');?>">
                        <?php echo lang('my_addresses'); ?>
                        <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                        <span class="iconEd"><i class="fa fa-address-card"></i></span>
                     </a>
                  </li>
                  <li>
                     <a href="<?php echo base_url('account/wishlist');?>">
                        <?php echo lang('wishlist_items'); ?>
                        <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                        <span class="iconEd"><i class="fa fa-heart"></i></span>
                     </a>
                  </li>
               </ul>
               <div class="tab-content">
                  <div id="orders" class="tab-pane fade in active">
                     <style>
                        .edOrderListStyle > a.list-group-item {
                        margin-bottom: 30px !important;
                        }
                        .edOrderListStyle .ostatus.odetails {
                        padding: 0 !important;
                        background-color: #f7f7f7 !important;
                        border: 0 !important;
                        border-radius: 0 !important;
                        }
                        .orderHeader .infos {
                        display: -ms-flexbox!important;
                        display: flex!important;
                        -ms-flex-pack: justify!important;
                        justify-content: space-between!important;
                        -ms-flex-align: stretch!important;
                        align-items: stretch!important;
                        }
                        .edOrderListStyle .ostatus.odetails .row {
                        border-bottom: 0 !important;
                        padding-bottom: 0 !important;
                        margin-bottom: 0 !important;
                        }
                        .edOrderListStyle .ostatus.odetails .row {
                        border-bottom: 0 !important;
                        padding-bottom: 0 !important;
                        margin-bottom: 0 !important;
                        }
                        .edOrderListStyle .ostatus.odetails .orderHeader .infos li {
                        margin: 0;
                        }
                        .edOrderListStyle .orderHeader .infos li.dropdown {
                        position: relative;
                        z-index: 10;
                        }
                        .edOrderListStyle .ostatus.odetails .orderHeader li .btn {
                        top: 10px !important;
                        z-index: 2;
                        }
                        .edOrderListStyle .ostatus.odetails .orderHeader {
                        margin: 0;
                        border-radius: 5px;
                        padding: 20px 10px !important;
                        }
                        .content.myaccount .tab-content .ostatus ul ul.dropdown-menu {
                        top: 27px;
                        z-index: 0;
                        }
                        .edOrderListStyle .card-body {
                        background: #f3ebd5;
                        padding: 20px 20px;
                        }
                        .edOrderListStyle .ostatus.odetails .accordion .card {
                        margin: 0 0 15px;
                        border: 1px solid #ddd;
                        border-radius: 10px;
                        /* background: #f3ebd5; */
                        }
                        .edOrderListStyle .ostatus.odetails .orderHeader {
                        transition-duration: 0.35s;
                        }
                        .edOrderListStyle .ostatus.odetails .orderHeader[aria-expanded="true"] {
                        background: #f3ebd5;
                        }
                        .edOrderListStyle .ostatus .wbox {
                        margin-bottom: 25px;
                        background-color: #ffffff;
                        padding: 25px;
                        border-radius: 6px;
                        box-shadow: 3px 3px 13px -2px rgba(0, 0, 0, 0.59);
                        }
                        .edOrderListStyle .ostatus a.btn.btn-success.btn-red {
                        margin-top: 25px;
                        }
                        .edOrderListStyle .ostatus .shippingVat ol {
                        border-top: 1px solid #dddddd;
                        padding: 11px 0 2px !important;
                        float: left;
                        width: 100%;
                        text-align: center;
                        }
                        .edOrderListStyle .ostatus .shippingVat ol li {
                        color: #bd9371 !important;
                        font-size: 20px;
                        font-weight: bold;
                        line-height: 1.5;
                        margin: 0 !important;
                        }
                        .eye-view-p{
                        font-size: 16px !important;
                        }
                        .eye-view {
                        margin : 5px 7px 0;
                        float: right;
                        font-size: 20px;
                        cursor: pointer;
                        }
                        @media (max-width: 768px){
                        .eye-view{
                        margin : 3px 33px 0;
                        }
                        }
                        @media (min-width: 1000px) and (max-width: 1024px){
                        .eye-view{
                        margin : 3px 2px 0;
                        }
                        }
                        @media (max-width: 425px){
                        .eye-view-p {
                        font-size: 13px !important;
                        }
                        .eye-view{
                        margin : 3px 0 0;
                        }
                        }
                     </style>
                     <div class="list-group">
                        <div class="edOrderListStyle">
                           <?php 
                              if ($pending_orders && count($pending_orders) > 0) { ?>
                           <a href="#OrderCat_1" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                           <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('pending_orders');?>
                           </a>
                           <div class="collapse" id="OrderCat_1" aria-expanded="false" style="">
                              <div class="ostatus odetails">
                                 <div class="accordion" id="allOrderAccordion">
                                    <?php
                                       foreach ($pending_orders as $key => $order) { 
                                          ?>
                                    <div class="card">
                                       <div class="card-header" id="headingTwo">
                                          <div class="row orderHeader">
                                             <div class="col-md-5">
                                                <p>
                                                   <?php echo lang('Order_No'); ?>:
                                                   <span>   <?php echo $order->OrderNumber; ?>
                                                   </span>
                                                   <?php if($order->TransactionID){?>
                                                   ,  
                                                   <?php echo lang('Transaction_ID'); ?>: <span><?php echo $order->TransactionID; ?></span>
                                                   <?php } ?>
                                                </p>
                                                 <?php if($order->AWBNumber != ''){ 
                                                   ?>
                                                   <p>
                                                   <a href="https://www.smsaexpress.com/trackingdetails?tracknumbers%5B0%5D=<?php echo $order->AWBNumber; ?>" target="_blank">
                                                   <?= lang('Tracking_ID') ?> : <span><?php echo $order->AWBNumber; ?></span>
                                                   </a>
                                                   </p>
                                                   <?php }?>
                                                <div class="header-progress-container">
                                                   <img src="<?php echo front_assets(); ?>images/orderPending.svg" alt="Pending Order" height="" width=" ">
                                                </div>
                                                <p class="eye-view-p"><?= lang('Click_here_for_order_details') ?><i class="fa fa-eye eye-view collapsed" data-toggle="collapse" data-target="#pending<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                                             </div>
                                             <div class="col-md-7">
                                                <ul class="infos">
                                                   <?php if($order->AWBNumber == ''  && $order->CollectFromStore != 1){ 
                                                   ?>
                                                   <li style="width:25%">
                                                      <p><?php echo lang('Est_Delivery'); ?>
                                                         <span> <?php
                                                            $site_setting = site_settings();
                                                            $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                            ?>
                                                         <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                      </p>
                                                   </li>
                                                   <?php }?>
                                                   <li style="width:<?= ($order->AWBNumber == '')?(($order->CollectFromStore != 1)?'25%': '50%'):'50%';?>">
                                                      <p> <?php echo lang('Total_Cost'); ?>
                                                      <span class=""><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%">
                                                      <p><?php echo lang('Paid_By'); ?>
                                                         <span>
                                                         <?php
                                                            if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                         <img src="<?php echo front_assets(); ?>images/pay_on_store.png" style="" alt="Payment Method">
                                                         <?php }else{?>
                                                         <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                         <?php } ?>
                                                         <?php ?>
                                                         </span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%" class="dropdown">
                                                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                      <ul class="dropdown-menu">
                                                         <li>
                                                            <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                         </li>
                                                         <li>
                                                            <a href="#" data-toggle="modal" class="cancel_returnModal" data-id="pending<?php echo $key;?>" data-target="#cancel_returnModal" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                         </li>
                                                         <li>
                                                            <a href="#" data-toggle="modal" class="cancel_returnModal" data-id="pending<?php echo $key;?>" data-target=" #cancel_returnModal" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                         </li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="pending<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                          <div class="card-body">
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="wbox side">
                                                      <?php
                                                         $order_items = getOrderItems($order->OrderID);
                                                         foreach ($order_items as $item) { 
                                                            $IsOnOffer = false;
                                                            $Price = $item->Amount;
                                                            $DiscountType = $item->DiscountType;
                                                            $DiscountFactor = $item->Discount;
                                                            if ($DiscountType == 'percentage') {
                                                               $IsOnOffer = true;
                                                               $Discount = ($DiscountFactor / 100) * $Price;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $Discount;
                                                               }
                                                            } elseif ($DiscountType == 'per item') {
                                                               $IsOnOffer = true;
                                                               $Discount = $DiscountFactor;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                               }
                                                            } else {
                                                               $Discount = 0;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price;
                                                               }
                                                            }
                                                            ?>
                                                      <div class="row">
                                                         <div class="col-sm-12">
                                                            <?php
                                                               if ($item->ItemType == 'Product') { ?>
                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                            <p><?php echo $item->Title.' x '.$item->Quantity; ?></p>
                                                            <?php
                                                               if($IsOnOffer)
                                                               {
                                                                  ?>
                                                                  <div class="price">
                                                                  <span style="text-decoration: line-through;"><?php echo number_format($item->Amount * $item->Quantity, 2); ?></span> <?= number_format($ProductDiscountedPrice* $item->Quantity, 2) ?> 
                                                                  <?php echo lang('SAR'); ?>
                                                                  </div>
                                                                  <?php
                                                               }
                                                               else
                                                               {
                                                                  ?>
                                                                  <div class="price"><?php echo number_format(($item->Amount * $item->Quantity), 2); ?> <?php echo lang('SAR'); ?></div>
                                                                  <?php
                                                               }
                                                            ?>
                                                            
                                                            <?php } ?>
                                                         </div>
                                                      </div>
                                                      
                                                      <?php } ?>
                                                      <div class="row last">
                                                         <div class="col-md-12 shippingVat text-center">
                                                            <ol>
                                                               <?php if($order->CollectFromStore != 1){ ?>
                                                               <li>
                                                                  <?php
                                                                     $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                     ?>
                                                                  <?php 
                                                                  if($order->AWBNumber != ''  && $order->CollectFromStore != 1){
                                                                     echo @$semsa_shipment[0]->Title;
                                                                  }else{
                                                                    echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); 
                                                                  }
                                                                   ?>
                                                                  <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                  <?php echo lang('SAR'); ?></strong>
                                                               </li>
                                                            <?php }?>
                                                               <li>
                                                                  Total Tax Paid
                                                                  <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                  <?php echo lang('SAR'); ?></strong>
                                                               </li>
                                                            </ol>
                                                            <div class="clearfix"></div>
                                                         </div>
                                                         <div class="col-md-12 grandTotal">
                                                            <button class="btn btn-secondary">
                                                            <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    <?php echo lang('SAR'); ?></strong></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="wbox">
                                                      <?php
                                                         if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                      <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                      <?php } else { ?>
                                                      <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                      <ul>
                                                         <li>
                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('StreetName'); ?></span>
                                                            <strong><?php echo $order->Street; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('CityName'); ?></span>
                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('POBox'); ?></span>
                                                            <strong><?php echo $order->POBox; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                         </li>
                                                      </ul>
                                                      <?php } ?>
                                                   </div>
                                                   <div class="wbox">
                                                      <h6><?php echo lang('Complain'); ?></h6>
                                                      <?php
                                                         if ($order->HasTicket == 1) {
                                                            $HasTicket1Style = "display:block;";
                                                            $HasTicket0Style = "display:none;";
                                                         } else {
                                                            $HasTicket1Style = "display:none;";
                                                            $HasTicket0Style = "display:block;";
                                                         }
                                                         if ($order->IsClosed == 0) {
                                                            $IsClosed = "Ongoing";
                                                         } else if ($order->IsClosed == 1) {
                                                            $IsClosed = "Closed";
                                                         } else if ($order->IsClosed == 2) {
                                                            $IsClosed = "Re-Opened";
                                                         }
                                                         ?>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                         <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                         <?php echo lang('Complain_No'); ?>.
                                                            <span><?php echo $order->TicketNumber; ?></span>
                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo lang('on_going'); ?></span>
                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                         </p>
                                                         <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                         <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                         </button>
                                                         </a>
                                                      </div>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                         <p><?php echo lang('if_you_did'); ?></p>
                                                         <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php }  ?>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                           <?php 
                              if ($packed_orders && count($packed_orders) > 0) { ?>
                           <a href="#OrderCat_2" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                           <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('packed_orders');?>
                           </a>
                           <div class="collapse" id="OrderCat_2" aria-expanded="false" style="">
                              <div class="ostatus odetails">
                                 <div class="accordion" id="allOrderAccordion">
                                    <?php
                                       foreach ($packed_orders as $key => $order) { ?>
                                    <div class="card">
                                       <div class="card-header" id="headingTwo">
                                          <div class="row orderHeader " >
                                             <div class="col-md-5">
                                                <p>
                                                   <?php echo lang('Order_No'); ?> :
                                                   <span>   <?php echo $order->OrderNumber; ?>
                                                   </span>
                                                   <?php if($order->TransactionID){?>
                                                   ,  
                                                   <?php echo lang('Transaction_ID'); ?> : <span><?php echo $order->TransactionID; ?></span>
                                                   <?php } ?> 
                                                </p>
                                                 <?php if($order->AWBNumber != ''){ 
                                                   ?>
                                                   <p>
                                                   <a href="https://www.smsaexpress.com/trackingdetails?tracknumbers%5B0%5D=<?php echo $order->AWBNumber; ?>" target="_blank">
                                                   <?= lang('Tracking_ID') ?> : <span><?php echo $order->AWBNumber; ?></span>
                                                   </a>
                                                   </p>
                                                   <?php }?>
                                                <div class="header-progress-container">
                                                   <img src="<?php echo front_assets(); ?>images/orderPacked.svg" alt="Pending Order" height="" width=" ">
                                                </div>
                                                <p class="eye-view-p"><?= lang('Click_here_for_order_details') ?><i class="fa fa-eye eye-view collapsed" data-toggle="collapse" data-target="#packed_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                                             </div>
                                             <div class="col-md-7">
                                                <ul class="infos">
                                                   <?php if($order->AWBNumber == ''  && $order->CollectFromStore != 1){ 
                                                   ?>
                                                   <li style="width:25%">
                                                      <p><?php echo lang('Est_Delivery'); ?>
                                                         <span> <?php
                                                            $site_setting = site_settings();
                                                            $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                            ?>
                                                         <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                      </p>
                                                   </li>
                                                   <?php } ?>
                                                   <li style="width:<?= ($order->AWBNumber == '')?'25%':'50%';?>">
                                                      <p><?php echo lang('Total_Cost'); ?>
                                                         <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%">
                                                      <p><?php echo lang('Paid_By'); ?>
                                                         <span>
                                                         <?php
                                                            if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                         <img src="<?php echo front_assets(); ?>images/pay_on_store.png" style="" alt="Payment Method">
                                                         <?php }else{?>
                                                         <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                         <?php } ?>
                                                         <?php ?>
                                                         </span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%" class="dropdown">
                                                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                      <ul class="dropdown-menu">
                                                         <li>
                                                            <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                         </li>
                                                         <li>
                                                            <a href="#" data-toggle="modal" data-target="#cancel_returnModal" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                         </li>
                                                         <li>
                                                            <a href="#" data-toggle="modal" data-target="#cancel_returnModal" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                         </li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="packed_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                          <div class="card-body">
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="wbox side leftbox">
                                                      <?php
                                                         $order_items = getOrderItems($order->OrderID);
                                                         foreach ($order_items as $item) {
                                                            $IsOnOffer = false;
                                                            $Price = $item->Amount;
                                                            $DiscountType = $item->DiscountType;
                                                            $DiscountFactor = $item->Discount;
                                                            if ($DiscountType == 'percentage') {
                                                               $IsOnOffer = true;
                                                               $Discount = ($DiscountFactor / 100) * $Price;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $Discount;
                                                               }
                                                            } elseif ($DiscountType == 'per item') {
                                                               $IsOnOffer = true;
                                                               $Discount = $DiscountFactor;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                               }
                                                            } else {
                                                               $Discount = 0;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price;
                                                               }
                                                            }
                                                            ?>
                                                      <div class="row">
                                                         <div class="col-sm-12">
                                                            <?php
                                                               if ($item->ItemType == 'Product') { ?>
                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                            <p><?php echo $item->Title.' x '.$item->Quantity; ?></p>
                                                            <?php
                                                               if($IsOnOffer)
                                                               {
                                                                  ?>
                                                                  <div class="price">
                                                                  <span style="text-decoration: line-through;"><?php echo number_format($item->Amount * $item->Quantity, 2); ?></span> <?= number_format($ProductDiscountedPrice* $item->Quantity, 2) ?> 
                                                                  <?php echo lang('SAR'); ?>
                                                                  <?php
                                                               }
                                                               else
                                                               {
                                                                  ?>
                                                                  <div class="price"><?php echo number_format(($item->Amount * $item->Quantity), 2); ?> <?php echo lang('SAR'); ?>
                                                                  <?php
                                                               }
                                                            ?>
                                                         </div>
                                                      </div>
                                                      <?php } ?>
                                                      <?php } ?>
                                                      <div class="row last">
                                                         <div class="col-md-12 shippingVat text-center">
                                                            <ol>
                                                               <li>
                                                                  <?php
                                                                     $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                     ?>
                                                                  <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                  <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                  SAR</strong>
                                                               </li>
                                                               <li>
                                                                  Total Tax Paid
                                                                  <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                  <?php echo lang('SAR'); ?></strong>
                                                               </li>
                                                            </ol>
                                                            <div class="clearfix"></div>
                                                         </div>
                                                         <div class="col-md-12 grandTotal">
                                                            <button class="btn btn-secondary">
                                                            <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    SAR</strong></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="wbox">
                                                      <?php
                                                         if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                      <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                      <?php } else { ?>
                                                      <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                      <ul>
                                                         <li>
                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('StreetName'); ?></span>
                                                            <strong><?php echo $order->Street; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('CityName'); ?></span>
                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('POBox'); ?></span>
                                                            <strong><?php echo $order->POBox; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                         </li>
                                                      </ul>
                                                      <?php } ?>
                                                   </div>
                                                   <div class="wbox">
                                                      <h6><?php echo lang('Complain'); ?></h6>
                                                      <?php
                                                         if ($order->HasTicket == 1) {
                                                             $HasTicket1Style = "display:block;";
                                                             $HasTicket0Style = "display:none;";
                                                         } else {
                                                             $HasTicket1Style = "display:none;";
                                                             $HasTicket0Style = "display:block;";
                                                         }
                                                         if ($order->IsClosed == 0) {
                                                             $IsClosed = "Ongoing";
                                                         } else if ($order->IsClosed == 1) {
                                                             $IsClosed = "Closed";
                                                         } else if ($order->IsClosed == 2) {
                                                             $IsClosed = "Re-Opened";
                                                         }
                                                         ?>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                         <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                         <?php echo lang('Complain_No'); ?>.
                                                            <span><?php echo $order->TicketNumber; ?></span>
                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo lang('on_going'); ?></span>
                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                         </p>
                                                         <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                         <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                         </button>
                                                         </a>
                                                      </div>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                         <p><?php echo lang('if_you_did'); ?></p>
                                                         <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?> a Complaint
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php }  ?>
                                 </div>
                              </div>
                           </div>
                           <?php }  ?>
                           <?php 
                              if ($dispatch_orders && count($dispatch_orders) > 0) { ?>
                           <a href="#OrderCat_3" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                           <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('dispatch_orders');?>
                           </a>
                           <div class="collapse" id="OrderCat_3" aria-expanded="false" style="">
                              <div class="ostatus odetails">
                                 <div class="accordion" id="allOrderAccordion">
                                    <?php
                                       foreach ($dispatch_orders as $key => $order) { ?>
                                    <div class="card">
                                       <div class="card-header" id="headingTwo">
                                          <div class="row orderHeader ">
                                             <div class="col-md-5">
                                                <p>
                                                <?php echo lang('Order_No'); ?>:
                                                   <span>   <?php echo $order->OrderNumber; ?>
                                                   </span>
                                                   <?php if($order->TransactionID){?>
                                                   ,  
                                                   <?php echo lang('Transaction_ID'); ?> : <span><?php echo $order->TransactionID; ?></span>
                                                   <?php } ?>

                                                </p>
                                                 <?php if($order->AWBNumber != ''){ 
                                                   ?>
                                                   <p>
                                                   <a href="https://www.smsaexpress.com/trackingdetails?tracknumbers%5B0%5D=<?php echo $order->AWBNumber; ?>" target="_blank">
                                                   <?= lang('Tracking_ID') ?> : <span><?php echo $order->AWBNumber; ?></span>
                                                   </a>
                                                   </p>
                                                <?php }?>

                                               
                                                <div class="header-progress-container">
                                                   <img src="<?php echo front_assets(); ?>images/orderDispatched.svg" alt="Pending Order" height="" width=" ">
                                                </div>
                                                <p class="eye-view-p"><?= lang('Click_here_for_order_details') ?><i class="fa fa-eye eye-view collapsed" data-toggle="collapse" data-target="#dispatch_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                                             </div>
                                             <div class="col-md-7">
                                                <ul class="infos">
                                                   <?php if($order->AWBNumber == ''  && $order->CollectFromStore != 1){ 
                                                   ?>
                                                   <li style="width:25%"> 
                                                      <p><?php echo lang('Est_Delivery'); ?>
                                                         <span> <?php
                                                            $site_setting = site_settings();
                                                            $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                            ?>
                                                         <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                      </p>
                                                   </li>
                                                <?php } ?>
                                                   <li style="width:<?= ($order->AWBNumber == '')?'25%':'50%';?>">
                                                      <p><?php echo lang('Total_Cost'); ?>
                                                         <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%">
                                                      <p><?php echo lang('Paid_By'); ?>
                                                         <span>
                                                         <?php
                                                            if (isset($order[0]->CollectFromStore) && $order[0]->CollectFromStore == 1) { ?>
                                                         <img src="<?php echo front_assets(); ?>images/pay_on_store.png" style="" alt="Payment Method">
                                                         <?php }else{?>
                                                         <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                         <?php } ?>
                                                         <?php ?>
                                                         </span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%" class="dropdown">
                                                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                      <ul class="dropdown-menu">
                                                         <li>
                                                            <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                         </li>
                                                         <li>
                                                            <a href="#" data-toggle="modal" data-target="#cancel_returnModal" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                         </li>
                                                         <li>
                                                            <a href="#" data-toggle="modal" data-target="#cancel_returnModal" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                         </li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="dispatch_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                          <div class="card-body">
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="wbox side leftbox">
                                                      <?php
                                                         $order_items = getOrderItems($order->OrderID);
                                                         foreach ($order_items as $item) {
                                                            $IsOnOffer = false;
                                                            $Price = $item->Amount;
                                                            $DiscountType = $item->DiscountType;
                                                            $DiscountFactor = $item->Discount;
                                                            if ($DiscountType == 'percentage') {
                                                               $IsOnOffer = true;
                                                               $Discount = ($DiscountFactor / 100) * $Price;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $Discount;
                                                               }
                                                            } elseif ($DiscountType == 'per item') {
                                                               $IsOnOffer = true;
                                                               $Discount = $DiscountFactor;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                               }
                                                            } else {
                                                               $Discount = 0;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price;
                                                               }
                                                            }
                                                             ?>
                                                      <div class="row">
                                                         <div class="col-sm-12">
                                                            <?php
                                                               if ($item->ItemType == 'Product') { ?>
                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                            <p><?php echo $item->Title.' x '.$item->Quantity; ?></p>
                                                            <?php
                                                               if($IsOnOffer)
                                                               {
                                                                  ?>
                                                                  <div class="price">
                                                                  <span style="text-decoration: line-through;"><?php echo number_format($item->Amount * $item->Quantity, 2); ?></span> <?= number_format($ProductDiscountedPrice* $item->Quantity, 2) ?> 
                                                                  <?php echo lang('SAR'); ?>
                                                                  <?php
                                                               }
                                                               else
                                                               {
                                                                  ?>
                                                                  <div class="price"><?php echo number_format(($item->Amount * $item->Quantity), 2); ?> <?php echo lang('SAR'); ?>
                                                                  <?php
                                                               }
                                                            ?>
                                                         </div>
                                                      </div>
                                                      <?php } ?>
                                                      <?php } ?>
                                                      <div class="row last">
                                                         <div class="col-md-12 shippingVat text-center">
                                                            <ol>
                                                               <li>
                                                                  <?php
                                                                     $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                     ?>
                                                                  <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                  <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                  <?php echo lang('SAR'); ?></strong>
                                                               </li>
                                                               <li>
                                                                  Total Tax Paid
                                                                  <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                  <?php echo lang('SAR'); ?></strong>
                                                               </li>
                                                            </ol>
                                                            <div class="clearfix"></div>
                                                         </div>
                                                         <div class="col-md-12 grandTotal">
                                                            <button class="btn btn-secondary">
                                                            <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    <?php echo lang('SAR'); ?></strong></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="wbox">
                                                      <?php
                                                         if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                      <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                      <?php } else { ?>
                                                      <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                      <ul>
                                                         <li>
                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('StreetName'); ?></span>
                                                            <strong><?php echo $order->Street; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('CityName'); ?></span>
                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('POBox'); ?></span>
                                                            <strong><?php echo $order->POBox; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                         </li>
                                                      </ul>
                                                      <?php } ?>
                                                   </div>
                                                   <div class="wbox">
                                                      <h6><?php echo lang('Complain'); ?></h6>
                                                      <?php
                                                         if ($order->HasTicket == 1) {
                                                             $HasTicket1Style = "display:block;";
                                                             $HasTicket0Style = "display:none;";
                                                         } else {
                                                             $HasTicket1Style = "display:none;";
                                                             $HasTicket0Style = "display:block;";
                                                         }
                                                         if ($order->IsClosed == 0) {
                                                             $IsClosed = "Ongoing";
                                                         } else if ($order->IsClosed == 1) {
                                                             $IsClosed = "Closed";
                                                         } else if ($order->IsClosed == 2) {
                                                             $IsClosed = "Re-Opened";
                                                         }
                                                         ?>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                         <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                         <?php echo lang('Complain_No'); ?>.
                                                            <span><?php echo $order->TicketNumber; ?></span>
                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo lang('on_going'); ?></span>
                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                         </p>
                                                         <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                         <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                         </button>
                                                         </a>
                                                      </div>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                         <p><?php echo lang('if_you_did'); ?></p>
                                                         <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php }  ?>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                           <?php 
                              if ($deliver_orders && count($deliver_orders) > 0) { ?>
                           <a href="#OrderCat_4" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                           <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('delivered_orders');?>
                           </a>
                           <div class="collapse" id="OrderCat_4" aria-expanded="false" style="">
                              <div class="ostatus odetails">
                                 <div class="accordion" id="allOrderAccordion">
                                    <?php
                                       foreach ($deliver_orders as $key => $order) { ?>
                                    <div class="card">
                                       <div class="card-header" id="headingTwo">
                                          <div class="row orderHeader ">
                                             <div class="col-md-5">
                                                <p>
                                                <?php echo lang('Order_No'); ?>:
                                                   <span>   <?php echo $order->OrderNumber; ?>
                                                   </span>
                                                   <?php if($order->TransactionID){?>
                                                   , 
                                                   <?php echo lang('Transaction_ID'); ?> : <span><?php echo $order->TransactionID; ?></span>
                                                   <?php } ?>
                                                </p>
                                                 <?php if($order->AWBNumber != ''){ 
                                                   ?>
                                                   <p>
                                                   <a href="https://www.smsaexpress.com/trackingdetails?tracknumbers%5B0%5D=<?php echo $order->AWBNumber; ?>" target="_blank">
                                                   <?= lang('Tracking_ID') ?> : <span><?php echo $order->AWBNumber; ?></span>
                                                   </a>
                                                   </p>
                                                   <?php }?>
                                                <div class="header-progress-container">
                                                   <img src="<?php echo front_assets(); ?>images/orderDelivered.svg" alt="Pending Order" height="" width=" ">
                                                </div>
                                                <p class="eye-view-p"><?= lang('Click_here_for_order_details') ?><i class="fa fa-eye eye-view collapsed" data-toggle="collapse" data-target="#deliver_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                                             </div>
                                             <div class="col-md-7">
                                                <ul class="infos">
                                                   <?php if($order->AWBNumber == '' && $order->CollectFromStore != 1){ 
                                                   ?>
                                                   <li style="width:25%">
                                                      <p><?php echo lang('Est_Delivery'); ?>
                                                         <span> <?php
                                                            $site_setting = site_settings();
                                                            $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                            ?>
                                                         <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                      </p>
                                                   </li>
                                                   <?php 
                                                      }
                                                   ?>
                                                   <li style="width:<?= ($order->AWBNumber == '')?'25%':'50%';?>">
                                                      <p><?php echo lang('Total_Cost'); ?>
                                                         <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%">
                                                      <p>
                                                         <?php echo lang('Paid_By'); ?>
                                                         <span>
                                                            <!-- <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method"> -->
                                                            <?php
                                                               if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                            <img src="<?php echo front_assets(); ?>images/pay_on_store.png" style="" alt="Payment Method">
                                                            <?php }else{?>
                                                            <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                            <?php } ?>
                                                            <?php ?>
                                                         </span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%" class="dropdown">
                                                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                      <ul class="dropdown-menu">
                                                         <li>
                                                            <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                         </li>
                                                         <!-- <li>
                                                            <a href="#" target="_blank" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                            </li>
                                                            <li>
                                                            <a href="#" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                            </li> -->
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="deliver_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                          <div class="card-body">
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="wbox side leftbox">
                                                      <?php
                                                         $order_items = getOrderItems($order->OrderID);
                                                         foreach ($order_items as $item) { 
                                                            $IsOnOffer = false;
                                                            $Price = $item->Amount;
                                                            $DiscountType = $item->DiscountType;
                                                            $DiscountFactor = $item->Discount;
                                                            if ($DiscountType == 'percentage') {
                                                               $IsOnOffer = true;
                                                               $Discount = ($DiscountFactor / 100) * $Price;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $Discount;
                                                               }
                                                            } elseif ($DiscountType == 'per item') {
                                                               $IsOnOffer = true;
                                                               $Discount = $DiscountFactor;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                               }
                                                            } else {
                                                               $Discount = 0;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price;
                                                               }
                                                            }
                                                            ?>
                                                      <div class="row">
                                                         <div class="col-sm-12">
                                                            <?php
                                                               if ($item->ItemType == 'Product') { ?>
                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                            <p><?php echo $item->Title.' x '.$item->Quantity; ?></p>
                                                            <?php
                                                               if($IsOnOffer)
                                                               {
                                                                  ?>
                                                                  <div class="price">
                                                                  <span style="text-decoration: line-through;"><?php echo number_format($item->Amount * $item->Quantity, 2); ?></span> <?= number_format($ProductDiscountedPrice* $item->Quantity, 2) ?> 
                                                                  <?php echo lang('SAR'); ?>
                                                                  <?php
                                                               }
                                                               else
                                                               {
                                                                  ?>
                                                                  <div class="price"><?php echo number_format(($item->Amount * $item->Quantity), 2); ?> <?php echo lang('SAR'); ?>
                                                                  <?php
                                                               }
                                                            ?>
                                                         </div>
                                                      </div>
                                                      <?php } ?>
                                                      <?php } ?>
                                                      <div class="row last">
                                                         <div class="col-md-12 shippingVat text-center">
                                                            <ol>
                                                               <li>
                                                                  <?php
                                                                     $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                     ?>
                                                                  <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                  <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                  SAR</strong>
                                                               </li>
                                                               <li>
                                                                  Total Tax Paid
                                                                  <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                  <?php echo lang('SAR'); ?></strong>
                                                               </li>
                                                            </ol>
                                                            <div class="clearfix"></div>
                                                         </div>
                                                         <div class="col-md-12 grandTotal">
                                                            <button class="btn btn-secondary">
                                                            <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    SAR</strong></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="wbox">
                                                      <?php
                                                         if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                      <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                      <?php } else { ?>
                                                      <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                      <ul>
                                                         <li>
                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('StreetName'); ?></span>
                                                            <strong><?php echo $order->Street; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('CityName'); ?></span>
                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('POBox'); ?></span>
                                                            <strong><?php echo $order->POBox; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                         </li>
                                                      </ul>
                                                      <?php } ?>
                                                   </div>
                                                   <div class="wbox">
                                                      <h6><?php echo lang('Complain'); ?></h6>
                                                      <?php
                                                         if ($order->HasTicket == 1) {
                                                             $HasTicket1Style = "display:block;";
                                                             $HasTicket0Style = "display:none;";
                                                         } else {
                                                             $HasTicket1Style = "display:none;";
                                                             $HasTicket0Style = "display:block;";
                                                         }
                                                         if ($order->IsClosed == 0) {
                                                             $IsClosed = "Ongoing";
                                                         } else if ($order->IsClosed == 1) {
                                                             $IsClosed = "Closed";
                                                         } else if ($order->IsClosed == 2) {
                                                             $IsClosed = "Re-Opened";
                                                         }
                                                         ?>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                         <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                         <?php echo lang('Complain_No'); ?>.
                                                            <span><?php echo $order->TicketNumber; ?></span>
                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo lang('on_going'); ?></span>
                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                         </p>
                                                         <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                         <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                         </button>
                                                         </a>
                                                      </div>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                         <p><?php echo lang('if_you_did'); ?></p>
                                                         <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php }  ?>
                                 </div>
                              </div>
                           </div>
                           <?php }  ?>
                           <?php 
                              if ($cancel_orders && count($cancel_orders) > 0) { ?>
                           <a href="#OrderCat_5" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false">
                           <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> <?php echo lang('cancelled_orders');?>
                           </a>
                           <div class="collapse" id="OrderCat_5" aria-expanded="false" style="">
                              <div class="ostatus odetails">
                                 <div class="accordion" id="allOrderAccordion">
                                    <?php
                                       foreach ($cancel_orders as $key => $order) { ?>
                                    <div class="card">
                                       <div class="card-header" id="headingTwo">
                                          <div class="row orderHeader " >
                                             <div class="col-md-5">
                                                <p>
                                                <?php echo lang('Order_No'); ?>:
                                                   <span>   <?php echo $order->OrderNumber; ?>
                                                   </span>
                                                   <?php if($order->TransactionID){?>
                                                   ,  
                                                   <?php echo lang('Transaction_ID'); ?> : <span><?php echo $order->TransactionID; ?></span>
                                                <?php } ?>
                                                </p>
                                                 <?php if($order->AWBNumber != ''){ 
                                                   ?>
                                                   <p>
                                                   <a href="https://www.smsaexpress.com/trackingdetails?tracknumbers%5B0%5D=<?php echo $order->AWBNumber; ?>" target="_blank">
                                                   <?= lang('Tracking_ID') ?> : <span><?php echo $order->AWBNumber; ?></span>
                                                   </a>
                                                   </p>
                                                <?php }?>
                                                <div class="header-progress-container">
                                                   <img src="<?php echo front_assets(); ?>images/orderCancelled.svg" alt="Pending Order" height="" width=" ">
                                                </div>
                                                <p class="eye-view-p"><?= lang('Click_here_for_order_details') ?><i class="fa fa-eye eye-view collapsed" data-toggle="collapse" data-target="#cancel_orders<?php echo $key;?>" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                                             </div>
                                             <div class="col-md-7">
                                                <ul class="infos">
                                                   <?php if($order->AWBNumber == ''  && $order->CollectFromStore != 1){ 
                                                   ?>
                                                   <li style="width:25%">
                                                      <p><?php echo lang('Est_Delivery'); ?>
                                                         <span> <?php
                                                            $site_setting = site_settings();
                                                            $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                            ?>
                                                         <?php echo date('l\, F jS\, Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></span>
                                                      </p>
                                                   </li>
                                                   <?php } ?>
                                                   <li style="width:<?= ($order->AWBNumber == '')?'25%':'50%';?>">
                                                      <p><?php echo lang('Total_Cost'); ?>
                                                         <span><?php echo number_format($order->TotalAmount, 2); ?> <?php echo lang('SAR'); ?></span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%">
                                                      <p>
                                                         <?php echo lang('Paid_By'); ?>
                                                         <span>
                                                            <!-- <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method"> -->
                                                            <?php
                                                               if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                            <img src="<?php echo front_assets(); ?>images/pay_on_store.png" style="" alt="Payment Method">
                                                            <?php }else{?>
                                                            <img src="<?php echo front_assets(); ?>images/<?php echo $order->PaymentMethod; ?>.png" style="width: 84px !important;" alt="Payment Method">
                                                            <?php } ?>
                                                            <?php ?>
                                                         </span>
                                                      </p>
                                                   </li>
                                                   <li style="width:25%" class="dropdown">
                                                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                      <ul class="dropdown-menu">
                                                         <li>
                                                            <a href="<?php echo base_url('page/invoice/'.base64_encode($order->OrderID)); ?>" target="_blank" style="text-decoration: none;"><?php echo lang('Invoice'); ?></a>
                                                         </li>
                                                         <!-- <li>
                                                            <a href="#" target="_blank" style="text-decoration: none;"><?php echo lang('Return_Order'); ?></a>
                                                            </li>
                                                            <li>
                                                            <a href="#" style="text-decoration: none;"><?php echo lang('Cancel_Order'); ?></a>
                                                            </li> -->
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>  
                                       <div id="cancel_orders<?php echo $key;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#allOrderAccordion">
                                          <div class="card-body">
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="wbox side leftbox">
                                                      <?php
                                                         $order_items = getOrderItems($order->OrderID);
                                                         foreach ($order_items as $item) { 
                                                            $IsOnOffer = false;
                                                            $Price = $item->Amount;
                                                            $DiscountType = $item->DiscountType;
                                                            $DiscountFactor = $item->Discount;
                                                            if ($DiscountType == 'percentage') {
                                                               $IsOnOffer = true;
                                                               $Discount = ($DiscountFactor / 100) * $Price;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $Discount;
                                                               }
                                                            } elseif ($DiscountType == 'per item') {
                                                               $IsOnOffer = true;
                                                               $Discount = $DiscountFactor;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                               }
                                                            } else {
                                                               $Discount = 0;
                                                               if ($Discount > $Price) {
                                                                  $ProductDiscountedPrice = 0;
                                                               } else {
                                                                  $ProductDiscountedPrice = $Price;
                                                               }
                                                            }
                                                            ?>
                                                      <div class="row">
                                                         <div class="col-sm-12">
                                                            <?php
                                                               if ($item->ItemType == 'Product') { ?>
                                                            <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                                            <p><?php echo $item->Title.' x '.$item->Quantity; ?></p>
                                                            <div class="price"><?php echo number_format(($item->Amount * $item->Quantity), 2); ?> <?php echo lang('SAR'); ?>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <?php } ?>
                                                      <?php } ?>
                                                      <div class="row last">
                                                         <div class="col-md-12 shippingVat text-center">
                                                            <ol>
                                                               <li>
                                                                  <?php
                                                                     $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, 'EN');
                                                                     ?>
                                                                  <?php echo(isset($shipment_method->Title) ? $shipment_method->Title : ''); ?>
                                                                  <strong id="ShippingAmount"><?php echo number_format($order->TotalShippingCharges, 2); ?>
                                                                  SAR</strong>
                                                               </li>
                                                               <li>
                                                                  Total Tax Paid
                                                                  <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                                                  <?php echo lang('SAR'); ?></strong>
                                                               </li>
                                                            </ol>
                                                            <div class="clearfix"></div>
                                                         </div>
                                                         <div class="col-md-12 grandTotal">
                                                            <button class="btn btn-secondary">
                                                            <span><?php echo lang('Grand_Total'); ?></span><strong><?php echo number_format($order->TotalAmount,2);?>                                                                                    SAR</strong></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="wbox">
                                                      <?php
                                                         if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                      <h6><?php echo lang('Collect_From_Store'); ?></h6>
                                                      <?php } else { ?>
                                                      <h6><?php echo lang('Delivery_Address'); ?></h6>
                                                      <ul>
                                                         <li>
                                                            <span><?php echo lang('RecipientName'); ?></span>
                                                            <strong><?php echo $order->RecipientName; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('BuildingNo'); ?>.</span>
                                                            <strong><?php echo $order->BuildingNo; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('StreetName'); ?></span>
                                                            <strong><?php echo $order->Street; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('DistrictName'); ?></span>
                                                            <strong><?php echo $order->AddressDistrict; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('CityName'); ?></span>
                                                            <strong><?php echo $order->AddressCity; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('POBox'); ?></span>
                                                            <strong><?php echo $order->POBox; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('ZipCode'); ?></span>
                                                            <strong><?php echo $order->ZipCode; ?></strong>
                                                         </li>
                                                         <li>
                                                            <span><?php echo lang('MobileNumber'); ?></span>
                                                            <strong><?php echo $order->MobileNo; ?></strong>
                                                         </li>
                                                      </ul>
                                                      <?php } ?>
                                                   </div>
                                                   <div class="wbox">
                                                      <h6><?php echo lang('Complain'); ?></h6>
                                                      <?php
                                                         if ($order->HasTicket == 1) {
                                                             $HasTicket1Style = "display:block;";
                                                             $HasTicket0Style = "display:none;";
                                                         } else {
                                                             $HasTicket1Style = "display:none;";
                                                             $HasTicket0Style = "display:block;";
                                                         }
                                                         if ($order->IsClosed == 0) {
                                                             $IsClosed = "Ongoing";
                                                         } else if ($order->IsClosed == 1) {
                                                             $IsClosed = "Closed";
                                                         } else if ($order->IsClosed == 2) {
                                                             $IsClosed = "Re-Opened";
                                                         }
                                                         ?>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket1" style="<?php echo $HasTicket1Style; ?>">
                                                         <p id="ComplaintDetail<?php echo $order->OrderID; ?>">
                                                         <?php echo lang('Complain_No'); ?>.
                                                            <span><?php echo $order->TicketNumber; ?></span>
                                                            <span><?php echo lang('Ticket_Status'); ?>: <?php echo lang('on_going'); ?></span>
                                                            <span><?php echo lang('Submitted_on'); ?> <?php echo date('d-m-Y h:i:s A', strtotime($order->TicketCreatedAt)); ?></span>
                                                         </p>
                                                         <a href="javascript:void(0);" class="showMessagesForTicket" data-ticket_id="<?php echo $order->TicketID; ?>">
                                                         <button class="btn btn-success faded"><?php echo lang('ViewMessages'); ?>
                                                         </button>
                                                         </a>
                                                      </div>
                                                      <div id="Order<?php echo $order->OrderID; ?>HasTicket0" style="<?php echo $HasTicket0Style; ?>">
                                                         <p><?php echo lang('if_you_did'); ?></p>
                                                         <a href="javascript:void(0);" class="btn btn-success btn-red" onclick="createTicket(<?php echo $order->OrderID; ?>);"><?php echo lang('Raise_a_Complaint'); ?>
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php }  ?>
                                 </div>
                              </div>
                           </div>
                           <?php }  
                           
                           if (!$pending_orders && !$packed_orders && !$dispatch_orders && !$deliver_orders && !$cancel_orders){
                           ?>
                           <div class="text-center">
                              <img src="<?php echo front_assets(); ?>images/no-orders.png" alt="no-orders" width="200px" height="300px">
                              <p class="no-orders-text"><?php echo lang('You_do_not_have_any_order_yet');?></p>
                              <a href="<?php echo base_url('/product?q=elegant-s40');?>" class="btn btn-primary"><?php echo lang('continue_shopping');?></a> 
                           </div>
                            <?php } ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<input type="hidden"  value="" class="tab_id">
<?php if (isset($_GET['p'])) { ?>
<script>
   $('a[href="#<?php echo $_GET['
      p ']; ?>"]').tab('show');
</script>
<?php } ?>
<script>
   // Enable pusher logging - don't include this in production
   Pusher.logToConsole = true;
   var pusher = new Pusher('a796cb54d7c4b4ae4893', {
       cluster: 'ap2',
       forceTLS: true
   });
   var channel = pusher.subscribe('Chocomood_Ticket_Channel');
   channel.bind('Chocomood_Ticket_Event', function(data) {
       var my_html = data.my_html;
       var TicketID = data.TicketID;
       $('.TicketID' + TicketID).html(my_html);
       $('.TicketID' + TicketID).animate({
           scrollTop: $('.msgbox').prop("scrollHeight")
       }, 1000);
   });
   $(document).ready(function() {
       $('.msgbox').animate({
           scrollTop: $('.msgbox').prop("scrollHeight")
       }, 1000);
       $(".cancel_returnModal").click(function(event) {
          var id = $(this).data('id');
          $(".tab_id").val(id);
       });
       $(".cancel_returnModal_close").click(function(event) {
          var this_id = $(".tab_id").val();
        $('#'+this_id).collapse('toggle'); 

       });
   });
</script>