<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Invoices</h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Order #</th>
                                    <th>Order Status</th>
                                    <th>Order Amount</th>
                                    <th>User City</th>
                                    <th>Received At</th>
                                    <?php if (checkUserRightAccess(66, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($invoices) {
                                    foreach ($invoices as $invoice) {
                                        $status = $invoice->Status;
                                        if ($status == 1) {
                                            $class = "btn btn-sm";
                                        } else if ($status == 2) {
                                            $class = "btn btn-primary btn-sm";
                                        } else if ($status == 3) {
                                            $class = "btn btn-warning btn-sm";
                                        }else if ($status == 4) {
                                            $class = "btn btn-success btn-sm";
                                        } else if ($status == 5) {
                                            $class = "btn btn-danger btn-sm";
                                        }
                                        ?>
                                        <tr id="<?php echo $invoice->OrderID; ?>">
                                            <td><?php echo $invoice->FullName; ?></td>
                                            <td><?php echo $invoice->Mobile; ?></td>
                                            <td><?php echo $invoice->Email; ?></td>
                                            <td><?php echo $invoice->OrderNumber; ?></td>
                                            <td>
                                                <button class="<?php echo $class; ?>">
                                                    <?php echo $invoice->OrderStatusEn; ?>
                                                    <div class="ripple-container"></div>
                                                </button>
                                            </td>
                                            <td><?php echo number_format($invoice->TotalAmount, 2); ?> SAR</td>
                                            <td><?php echo $invoice->UserCity; ?></td>
                                            <td><?php echo date('d-m-Y h:i A', strtotime($invoice->CreatedAt)); ?></td>
                                            <?php if (checkUserRightAccess(66, $this->session->userdata['admin']['UserID'], 'CanEdit')) { 
                                                $baseUrl = $invoice->isCustomize==0?'cms/orders/invoice/':'cms/invoice/view/';?>
                                                <td>
                                                    <a href="<?php echo base_url($baseUrl.$invoice->OrderID); ?>"
                                                       class="btn btn-simple btn-warning btn-icon"
                                                       target="_blank"><i
                                                                class="material-icons"
                                                                title="View order invoice">receipt</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                    <a href="<?php echo base_url('cms/invoice/download/' . $invoice->OrderID); ?>"
                                                       class="btn btn-simple btn-warning btn-icon"><i
                                                                class="material-icons"
                                                                title="Download order invoice">save_alt</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>