<?php
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if ($result[0]->Image == '') {
    $image = base_url("assets/backend/img/no_img.png");
} else {
    $image = base_url($result[0]->Image);
}
if ($result[0]->BannerImage == '') {
    $banner_image = base_url("assets/backend/img/no_img.png");
} else {
    $banner_image = base_url($result[0]->BannerImage);
}
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $is_active_field = '';
        $image_field = '';
        $banner_image_field = '';
        if ($key == 0) {

            $is_active_field = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="togglebutton">
                                                <label for="IsActive">
                                                 <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[0]->IsActive) && $result[0]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                                </label>
                                            </div>
                                    </div>
                                </div>';
            if ($result[0]->PageID == 1 || $result[0]->PageID == 7 || $result[0]->PageID == 8 || $result[0]->PageID == 10 || $result[0]->PageID == 11 || $result[0]->PageID == 12) {
                $image_field = '<div class="row">
                                <div class="col-md-12">
                                    
                                    <img src="' . $image . '" style="width:100px;height:100px;">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                    <label>Image</label>
                                         <input type="file" name="Image[]"  required accept="image/*">
                                    </div>
                                </div>
                            </div>';
            }
            if ($result[0]->PageID == 1) {
                $banner_image_field = '<div class="row">
                                <div class="col-md-12">
                                    
                                    <img src="' . $banner_image . '" style="width:100px;height:100px;">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                    <label>Banner Image</label>
                                         <input type="file" name="BannerImage[]"  required accept="image/*">
                                    </div>
                                </div>
                            </div>';
            }
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                         ' . $is_active_field . '
                                                    </div>
                                                    <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description' . $key . '">Description</label>
                                        <textarea class="form-control textarea" name="Description" id="Description' . $key . '" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>

                                    </div>
                                </div>
                            </div>';
        if ($result[0]->PageID == 7 || $result[0]->PageID == 8) {
            $lang_data .= '<div class="row">
                            <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ShortDescription' . $key . '">Short Description</label>
                                        <textarea class="form-control textarea" name="ShortDescription" id="ShortDescription' . $key . '" style="height: 100px;">' . ((isset($result[$key]->ShortDescription)) ? $result[$key]->ShortDescription : '') . '</textarea>

                                    </div>
                                </div>
</div>';
        }

        $lang_data .= '<div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaTages">'.lang('meta_tages').'</label>
                                                                    <input type="text" name="MetaTags" required  class="form-control" id="MetaTages" value="'.((isset($result[$key]->MetaTags)) ? $result[$key]->MetaTags : '').'">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaKeywords">'.lang('meta_keywords').'</label>
                                                                    <input type="text" name="MetaKeywords" required  class="form-control" id="MetaKeywords" value="' . ((isset($result[$key]->MetaKeywords)) ? $result[$key]->MetaKeywords : '') . '">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaDescription">'.lang('meta_description').'</label>
                                                                    <input type="text" class="form-control" name="MetaDescription" id="MetaDescription" value="' . ((isset($result[$key]->MetaDescription)) ? $result[$key]->MetaDescription : '') . '">
                                                                </div>
                                                            </div>
                                                        </div>
                            ' . $banner_image_field . '
                            ' . $image_field . '
                            
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/tinymce_custom.js"></script>