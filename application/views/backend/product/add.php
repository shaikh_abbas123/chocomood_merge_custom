<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
.form-group.label-floating label.control-label, .form-group.label-placeholder label.control-label {
    color: #bd9371;
    position: static;
    float: left;
    min-width: 160px;
}
.form-group.label-floating .form-control,
.form-group.label-floating .select2-container {
    float: right;
    width: calc(100% - 160px) !important;
    border-color: #D2D2D2;
    border-style: solid;
    border-width: 1px 1px 1px;
    background-position: center bottom, center calc(100% + 1px);
    padding-left: 10px;
    padding-right: 10px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
    border: none !important;
    outline: 0;
}
.select2-container--default .select2-selection--multiple {
    background: transparent;
    border: 0;
    border-radius: 0;
}
.select2-container--default .select2-selection--multiple ul {
    padding:0;
}
.add_whats_inside + .row .form-group.label-floating .form-control {
    width: 100%;
    margin-top: 5px;
}
.form-group.label-floating .btn-group.bootstrap-select,
.form-group.label-floating .note-editor.note-frame {
    float: right;
    width: calc(100% - 165px);
}
.note-editor.note-frame {
    border: 1px solid #D2D2D2 !important;
}
.chocoCMSAccordion .card {
    margin: 15px 0;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.2);
    /* overflow: hidden; */
}
.chocoCMSAccordion .card .card-header {
    padding: 0 !important;
}
.chocoCMSAccordion .card .card-header h2 {
    margin: 0;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link {
    color: #50456d;
    background: transparent;
    border: 0;
    border-radius: 0;
    margin: 0;
    box-shadow: none;
    padding: 14px 20px;
    line-height: 1.5;
    min-height: inherit;
    font-size: 22px;
    font-weight: normal;
    display: block;
    width: 100%;
    text-align: inherit;
    text-decoration: none;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link:hover {
    background-color: #f7f4eb;
}
.chocoCMSAccordion .card .card-body {
    padding: 50px 35px;
    /* border-top: 2px solid #513c7e; */
}
.chocoCMSAccordion .card .card-header i.fa {
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    line-height: 12px;
    margin-top: 8px;
    margin-right: 4px;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn[aria-expanded="true"] i.fa {
    transform: rotateX(180deg);
}
.add-packages-field-width{
    min-width: 70% !important;
}

@media (min-width: 1330px) and (max-width: 1440px){
    .respnsve-width-packages{
        min-width:100% !important;
    }
    .per_pc_weight_in_gm{
        min-width:100px !important;
    }
}

@media (min-width: 992px) and (max-width: 1024px){
    .per_pc_weight_in_gm{
        min-width:100px !important;
    }
    .add-packages-field-width{
        min-width:75px !important;
        /*position: relative;*/
        /*top: 40px;*/
        margin-bottom: 20px;
    }
    .respnsve-width-packages{
        min-width:160px !important;
        position: relative;
        top: 40px;
        margin-bottom: 20px;
    }
}

@media (min-width:1680px){
    .respnsve-width-packages{
        min-width:100%;
    }
}

input.upload-img {
    top: 20px !important;
}
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                            <div class="accordion chocoCMSAccordion" id="addProdAccordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Product Information</button>
                                    </h2>
                                    </div>
                                    <div id="collapseOne" class="collapse in" aria-labelledby="headingOne" data-parent="#addProdAccordion" aria-expanded="true">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Title"><?php echo lang('title'); ?> *</label>
                                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="SKU">SKU *</label>
                                                        <input type="text" name="SKU" required  class="form-control" id="SKU">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10 peice_field" >
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Price"><?php echo lang('price'); ?> *</label>
                                                        <input type="text" name="Price" required  class="form-control number-with-decimals" id="Price">
                                                    </div>
                                                </div>
                                               

                                            </div>



                                            <div class="row">
                                                <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10" >
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">&nbsp;</label>
                                                        <select name="PriceType" class="form-control" id="PriceType" required>
                                                            <!--<option value=""><?php echo lang('price_type');?></option>-->
                                                            <option value="item"><?php echo lang('price_type_item');?></option>
                                                            <option value="kg"><?php echo lang('price_type_kg');?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="clearfix"></div>
                                                 <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10 weight_field" >
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Weight">Weight </label>
                                                        <input type="text" name="Weight" required  class="form-control number-with-decimals" id="Weight">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 weight_field">
                                                    <label class="control-label is-empty" style="margin-top:36px!important;" >grams</label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 minimum_purchasable">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10 minimum_kg_quantity">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="MinimumOrderQuantity">Minimum Purchasable</label>
                                                                <input type="number" name="MinimumOrderQuantity" required  class="form-control number-with-decimals" id="MinimumOrderQuantity" value="1">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                            <label class="control-label is-empty" style="margin-top:36px!important;" id="min_order_label">Pcs</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Select Brand*</label>
                                                            <select name="brand_id" class="form-control" data-style="select-with-transition" id="brand_id" required>
                                                            <option value="0" selected>Chocomood</option>
                                                            <?php
                                                            // foreach($brands as $k=>$v){
                                                            // echo "<option  value=".$v->brand_id.">".$v->brand_name."</option>";
                        
                                                            // }
                        
                                                            ?>
                                                            </select>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-md-12" id="kilo_field">
                                                    <!-- <a href="#" class="kilo_field">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                            Add Packages
                                                        </button>
                                                    </a> -->
                                                    <div class="row">
                                                        <!-- <div class="col-md-12"> -->
                                                          
                                                            <div class="col-lg-6 col-md-10 col-sm-10">
                                                                <div class="form-group label-floating">
                                                                   <label class="control-label">Select Package*</label>
                                                                     <select name="PackagesID[]" class="  selectpicker" data-style="select-with-transition" id="PackagesID" required multiple>
                                                                       <option value="0">-- Select Package --</option>
                                                                       option
                                                                       <?php
                                                                       foreach($packages as $k=>$v){
                                                                       echo "<option value=".$v->PackagesID.">".$v->Title."</option>";
                                    
                                                                       }
                                    
                                                                       ?>
                                                                       </select>
                                                                </div>
                                                            </div>
                                                           
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="row">
                                                        <div class="">
                                                          
                                                            <div class="col-lg-6 col-md-10 col-sm-10">
                                                                <div class="form-group label-floating">
                                                                   <label class="control-label">Select Default Package*</label>
                                                                     <select name="DefaultPackagesID" class="form-control PackagesID" data-style="select-with-transition" id="PackagesID" required>
                                                                       <option value="0">-- Select Default Package --</option>
                                                                       option
                                                                       <?php
                                                                       foreach($packages as $k=>$v){
                                                                       echo "<option data-weight=".$v->Quantity." value=".$v->PackagesID.">".$v->Title."</option>";
                                    
                                                                       }
                                    
                                                                       ?>
                                                                       </select>
                                                                </div>
                                                            </div>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="row" id="kilo_field`+kilo_count+`">
                                                      <!--  <div class="col-md-1"> -->
                                                            <!-- <i class="fa fa-times delete_inside_added_row" onclick="removeRowkilo(`+kilo_count+`)" style="margin-top:30px;cursor:pointer;" aria-hidden="true"></i> -->
                                                      <!--   </div> -->
                                                        
                                                        <div class="col-lg-4 col-md-6 col-sm-10 ">
                                                            <div class="form-group label-floating">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-3">                   
                                                                        <label class="control-label">Minimum amount to purchase (in grams) </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6">
                                                                        <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width min_package" name="MinimumPackage" value="0">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-lg-4 col-md-6 col-sm-10 ">
                                                            <div class="form-group label-floating">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-3">                    
                                                                        <label class="control-label">Maximum amount to purchase (in grams) </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6">
                                                                        <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width max_package" name="MaximumPackage" value="0">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-lg-4 col-md-6 col-sm-10 ">
                                                            <div class="form-group label-floating">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-3">                    
                                                                        <label class="control-label per_pc_weight_in_gm">Unit Grams Value</label>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6">
                                                                        <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width gram_weight" name="PerPiecePrice" value="1000">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-lg-4 col-md-6 col-sm-10 ">
                                                            <div class="form-group label-floating">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-3">                    
                                                                        <label class="control-label">Price of total unit grams value</label>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6">
                                                                        <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width gram_price" name="PerGramPrice" value="0" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Category</button>
                                    </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#addProdAccordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Main Category *</label>
                                                        <select name="CategoryID[]" class="selectpicker" data-style="select-with-transition" id="CategoryID" required multiple>
                                                            <option value=""><?php echo lang('choose_category');?></option>
                                                            <?php foreach ($categories as $key => $value) { ?>
                                                                <option value="<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Sub Category *</label>
                                                        <select name="SubCategoryID[]" class="selectpicker" data-style="select-with-transition" id="SubCategoryID" required multiple>
                                                            <option value=""><?php echo lang('choose_sub_category');?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Product Description</button>
                                    </h2>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#addProdAccordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Serving Size</label>
                                                        <input type="text" name="ServingSize" required  class="form-control" id="ServingSize" value="">
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label"><?php echo lang('select_nutritions');?></label>
                                                        <select  class="form-control" id="Sl_Chocolate" multiple required>
                                                            <?php foreach ($nutritions as $key => $nutrition) { ?>
                                                                <option value="<?php echo $nutrition->NutritionID; ?>" class="nutrition_<?php echo $nutrition->NutritionID; ?>" data-quantity="0"><?php echo $nutrition->Title; ?></option>
                                                        <?php        
                                                                
                                                            }
                                                            ?>
                                                            
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="row" id="multiple_fields"></div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Description"><?php echo lang('description'); ?></label>
                                                        <textarea class="form-control" name="Description" id="Description" style="height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Ingredients"><?php echo lang('ingredients'); ?></label>
                                                        <textarea class="form-control" name="Ingredients" id="Ingredients" style="height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>        
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Specifications">Specifications</label>
                                                        <textarea class="form-control summernote" name="Specifications" id="Specifications" style="height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12" id="add_whats_inside_fields">
                                                    <a href="#" class="add_whats_inside">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                            Add What's Inside Field
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Tags *</label>
                                                        <select name="TagIDs[]" class="selectpicker select2" data-style="select-with-transition" id="TagID" required multiple>
                                                            <option value=""><?php echo lang('tags');?></option>
                                                            <?php foreach ($tags as $key => $value) { ?>
                                                                <option value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Tags"><?php echo lang('tags'); ?></label>
                                                        <input type="text" name="Tags" required  class="form-control" id="Tags">
                                                    </div>
                                                </div>-->
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Keywords"><?php echo lang('keywords'); ?></label>
                                                        <input type="text" name="Keywords" required  class="form-control" id="Keywords">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="MetaTages"><?php echo lang('meta_tages'); ?></label>
                                                        <input type="text" name="MetaTags" required  class="form-control" id="MetaTages">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="MetaKeywords"><?php echo lang('meta_keywords'); ?></label>
                                                        <input type="text" name="MetaKeywords" required  class="form-control" id="MetaKeywords">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="MetaDescription"><?php echo lang('meta_description'); ?></label>
                                                        <textarea class="form-control" name="MetaDescription" id="MetaDescription" style="height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4 checkbox-radios">
                                                    <div class="form-group label-floating">
                                                        <div class="checkbox">
                                                            <label for="IsActive">
                                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 checkbox-radios">
                                                    <div class="form-group label-floating">
                                                        <div class="checkbox">
                                                            <label for="IsFeatured">
                                                                <input name="IsFeatured" value="1" type="checkbox" id="IsFeatured"/> <?php echo lang('is_featured'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-4 checkbox-radios">
                                                    <div class="form-group label-floating">
                                                        <div class="checkbox">
                                                            <label for="OutOfStock">
                                                                <input name="OutOfStock" value="1" type="checkbox" id="OutOfStock"/> <?php echo lang('out_of_stock'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 checkbox-radios">
                                                    <div class="form-group label-floating">
                                                        <div class="checkbox">
                                                            <label for="IsCorporateProduct">
                                                                <input name="IsCorporateProduct" value="1" type="checkbox" id="IsCorporateProduct"/> <?php echo lang('is_corporate'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-4 checkbox-radios">
                                                    <div class="form-group label-floating">
                                                        <div class="checkbox">
                                                            <label for="IsCustomizedProduct">
                                                                <input name="IsCustomizedProduct" value="1" type="checkbox" id="IsCustomizedProduct"/> <?php echo lang('is_customize'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="ForIsCorporateProduct" style="display: none;">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="CorporateMinQuantity">Minimum Quantity</label>
                                                        <input type="text" name="CorporateMinQuantity" class="form-control" id="CorporateMinQuantity">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="CorporatePrice">Corporate Price</label>
                                                        <input type="text" name="CorporatePrice" class="form-control number-with-decimals" id="CorporatePrice">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="ForIsCustomizedProduct" style="display: none;">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="BoxIDs">Boxes</label>
                                                        <select name="BoxIDs[]" class="form-control" id="BoxIDs" multiple>
                                                            <?php foreach ($boxes as $box) { ?>
                                                                <option value="<?php echo $box->BoxID; ?>"><?php echo $box->Title; ?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <input type="file" name="Image[]" multiple="multiple">
                                                        <p><?php echo lang('cannot_upload_more_then'); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group text-right m-b-0">
                                                <button class="btn btn-primary waves-effect waves-light submit" type="submit">
                                                    <?php echo lang('submit');?>
                                                </button>
                                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                                    <button type="button" class="submit btn btn-default waves-effect m-l-5">
                                                        <?php echo lang('back');?>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $('.select2').select2();
</script>
<script type="text/javascript">
     var inside_count = 0; 
     var kilo_count = 0; 
    $(document).ready(function () {
       $('#kilo_field').hide();
                 
       $('#CategoryID').on('change',function(){
           
            var CategoryID = $(this).val();
            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/category/getSubCategory',
                    data: {
                        'CategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {
                        $('#SubCategoryID').html(result.html);
                        $('#SubCategoryID').selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
       $('#PriceType').on('change',function(){
        var value = $(this).val();
        if(value == 'kg'){
            //$('.minimum_kg_quantity').show();
            $('.peice_field').hide();
            $('.minimum_purchasable').hide();
            $('#MinimumOrderQuantity').val(100);
            $('#min_order_label').html('Grams');
            $('#add_whats_inside_fields').hide();
            $('#kilo_field').show();
            $('.weight_field').hide();

            //$('.kilo_field').click();
        }else{
            //$('.minimum_kg_quantity').hide();
            $('.peice_field').show();
            $('.minimum_purchasable').show();
            $('#MinimumOrderQuantity').val(1);
            $('#min_order_label').html('Pcs');
            $('#add_whats_inside_fields').show();
            $('#kilo_field').hide();
            $('.weight_field').show();
             
        }
       });


        $(".kilo_field").on('click', function () {
           
            $kilo_fields = `<div class="row" id="kilo_field`+kilo_count+`">
            <div class="col-md-1">
                                <i class="fa fa-times delete_inside_added_row" onclick="removeRowkilo(`+kilo_count+`)" style="margin-top:30px;cursor:pointer;" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <div class="row">
                                        <div class="col-md-4">                    
                                            <label class="control-label">Select Package</label>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="form-control respnsve-width-packages " name="PackagesID[]" >
                                           <option value="0">-- Select Package --</option>
                                           option
                                           <?php
                                           foreach($packages as $k=>$v){
                                           echo "<option value=".$v->PackagesID.">".$v->Title."</option>";
        
                                           }
        
                                           ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group label-floating">
                                    <div class="row">
                                        <div class="col-md-6">                    
                                            <label class="control-label">Minimum package</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width" name="MinimumPackage[]" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group label-floating">
                                    <div class="row">
                                        <div class="col-md-6">                    
                                            <label class="control-label">Maximum package</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width" name="MaximumPackage[]" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group label-floating">
                                    <div class="row">
                                        <div class="col-md-6">                    
                                            <label class="control-label per_pc_weight_in_gm">Per piece weight in grams</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width" name="PerPiecePrice[]" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group label-floating">
                                    <div class="row">
                                        <div class="col-md-6">                    
                                            <label class="control-label">Price per 1 Gram</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" min="0" class="form-control number-with-decimals add-packages-field-width" name="PerGramPrice[]" value="0" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
               `;


            $("#kilo_field").append($kilo_fields);
            kilo_count++;
        

        });    
       
       
    });
function removeRowkilo(id){
    $('#kilo_field'+id).remove();
}
$(".submit").click(function () {
    var price_type = $("#PriceType option:selected").val();
    if(price_type == 'kg'){
        set_kg_display_price();
    }
});
function set_kg_display_price(){
        var min = $( ".min_package" ).val();
        var max = $( ".max_package" ).val();
        var weight = $( ".PackagesID option:selected" ).data("weight");
        var piece_weight = $( ".gram_weight" ).val();
        var gram_price = $( ".gram_price" ).val();
        var max_val = max;
        min = min/weight;
        max = max/weight;
        if(min < 1){
            if(weight < max_val){
                min = 1;    
            }else{
                min=0;
            }
        }
        var unit_price = 0;
        unit_price = (parseFloat(gram_price)/parseFloat(piece_weight))*parseFloat(weight);
        $("#Price").val(unit_price);
        return unit_price;
}
</script>