<div class="content">
    <div class="container-fluid">
        <?php if($this->session->userdata['admin']['RoleID'] == 1){ ?>
        <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Import Products List</h4>
                            <div class="material-datatables" style="margin-left: 450px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="<?php echo base_url(); ?>cms/product/importCsv" method="post"
                                                      onsubmit="return false;" class="form_data"
                                                      enctype="multipart/form-data"
                                                      data-parsley-validate novalidate>
                                        
                                            
                                                
                                                       
                                            <div class="row">
                                                        <div class="col-md-12">

                                                    <label>Upload Products CSV</label>
                                                    <input type="file" name="Csv" id="Csv">
                                                </div>
                                                </div>

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <label>Upload Products Images as Zip File</label>
                                                    <input type="file" name="file" id="file">
                                                </div>
                                                </div>


                                            
                                            <div class="">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                    Import
                                                </button>
                                            </div>
                                        
                                    </form>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="<?php echo base_url('assets/product_sample.csv');?>" target="_blank">Download Sample Format</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
        </div>
    <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName . 's'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/' . $ControllerName . '/add'); ?>">
                                <button type="button"
                                        class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_' . $ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th>#</th>
                                    <th><?php echo lang('title'); ?></th>
                                    <th>SKU</th>
                                    <th>Purchases</th>
                                    <th>Active</th>
                                    <th>Featured</th>
                                    <th>Customized</th>
                                    <th>Out of stock</th>
                                    <th>Corporate</th>
                                    <th>Rating</th>
                                    <th>Image</th>


                                    <?php if (checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    $i = 1;
                                    foreach ($results as $value) { ?>
                                        <tr id="<?php echo $value->ProductID; ?>">

                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->SKU; ?></td>
                                            <td><?php echo $value->PurchaseCount; ?></td>
                                            <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                            <td><?php echo($value->IsFeatured ? lang('yes') : lang('no')); ?></td>
                                            <td><?php echo($value->IsCustomizedProduct ? lang('yes') : lang('no')); ?></td>
                                            <td><?php echo($value->OutOfStock ? lang('yes') : lang('no')); ?></td>
                                            <td><?php echo($value->IsCorporateProduct ? lang('yes') : lang('no')); ?></td>
                                            <td><?php echo (productAverageRating($value->ProductID) > 0 ? productAverageRating($value->ProductID).'/5' : 'N/A'); ?></td>
                                            <td>
                                                <a href="<?php echo base_url(get_images($value->ProductID, 'product', false)); ?>"
                                                   target="_blank">
                                                    <img src="<?php echo base_url(get_images($value->ProductID, 'product', false)); ?>"
                                                         style="height:70px;width:70px;">
                                                </a>
                                            </td>

                                            <?php if (checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $value->ProductID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons" title="Edit">edit</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (checkUserRightAccess(62, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                        <a href="javascript:void(0);"
                                                           onclick="deleteRecord('<?php echo $value->ProductID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                           class="btn btn-simple btn-danger btn-icon remove"><i
                                                                    class="material-icons" title="Delete">close</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>
                                                    <a href="javascript:void(0);" class="store_availability" data-product-id= "<?php echo $value->ProductID;?>" data-product-title="<?php echo $value->Title; ?>">Add Quantity
                                                           
                                                        </a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $i++;
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Availability</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="model_body">
        
      </div>
      
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>