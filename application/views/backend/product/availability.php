<form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/saveProductAvailability" method="post" onsubmit="return false;" class="" id="availability_form" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="ProductID" value="<?php echo $ProductID; ?>">

<?php if($stores){ 
    foreach($stores as $key => $store){
        $quantity = 0;
        $checkbox = '';

        if(array_key_exists($store['StoreID'],$products_availability)){
            $quantity = $products_availability[$store['StoreID']];
            $checkbox = 'checked';
        }

     ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <input name="StoreID[]" value="<?php echo $store['StoreID'];?>" type="checkbox" id="Store<?php echo $store['StoreID'];?>" <?php echo $checkbox; ?>/>
                                        <label for="Store<?php echo $store['StoreID'];?>">
                                            <?php echo $store['Title']; ?>
                                        </label>


                                </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Quantity<?php echo $store['StoreID'];?>" class="col-form-label"><?= ($product_type == 'kg')? 'Available Quantity(Grams)' : 'Available Quantity'?>:</label>
                                        <input type="text" class="form-control" name="<?= ($product_type == 'kg')? 'GramQuantity[]' : 'Quantity[]'?>" id="Quantity<?php echo $store['StoreID'];?>" value="<?php echo $quantity; ?>">
                                    </div>
                                </div>

                            </div>
                        <?php } } ?>
                            <div class="form-group text-right m-b-0">
                                <input type="hidden" name="product_type" value="<?= @$product_type?>">
                                <button class="btn btn-primary waves-effect waves-light" type="submit" >
                                    <?php echo lang('submit');?>
                                </button>
                                
                                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                               
                            </div>


<script type="text/javascript">

$(document).ready(function () {
    $("#availability_form").submit(function (e) {
        e.preventDefault();


        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });


    if ($('.jFiler-input-text').length > 0) {

        $('.jFiler-input-text').hide();
    }


});

</script>