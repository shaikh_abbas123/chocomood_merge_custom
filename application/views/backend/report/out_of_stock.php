<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Out Of Stock</h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo lang('title'); ?></th>
                                    <th>SKU</th>
                                    <th>Purchases</th>
                                    <th>Rating</th>
                                    <th>Image</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    $i = 1;
                                    foreach ($results as $value) { ?>
                                        <tr id="<?php echo $value->ProductID; ?>">

                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->SKU; ?></td>
                                            <td><?php echo $value->PurchaseCount; ?></td>
                                            <td><?php echo (productAverageRating($value->ProductID) > 0 ? productAverageRating($value->ProductID).'/5' : 'N/A'); ?></td>
                                            <td>
                                                <a href="<?php echo base_url(get_images($value->ProductID, 'product', false)); ?>"
                                                   target="_blank">
                                                    <img src="<?php echo base_url(get_images($value->ProductID, 'product', false)); ?>"
                                                         style="height:70px;width:70px;">
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>