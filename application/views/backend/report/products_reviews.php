<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Products Reviews</h4>
                        <div class="toolbar">
                            
                        </div>
                        <div class="material-datatables respnsve-tbl">
                            <table id="" class="prod-review-tbl search_fields datatables_csv table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th class="prod-review-th1">ID</th>
                                    <th class="prod-review-th2"><?php echo lang('title');?></th>
                                    <th class="prod-review-th3">Reviews</th>
                                    <th class="prod-review-th4">Average</th>
                                    <th class="prod-review-th5">Action</th>


                                    

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){
                                        $avg_rating = productAverageRating($value->ProductID);
                                     ?>
                                        <tr>
                                            <td class="prod-review-td1"><?php echo $value->ProductID; ?></td>
                                            <td class="prod-review-td2"><?php echo $value->Title; ?></td>
                                            <td class="prod-review-td3"><?php echo $value->TotalReviews; ?></td>
                                            <td class="prod-review-td4"><?php echo $avg_rating; ?></td>                           
                                            <td class="prod-review-td5"><a href="<?php echo base_url('cms/report/product_reviews/'.$value->ProductID);?>">Show Review</a></td>                           
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<style>
.prod-review-th1{min-width: 180px;}
.prod-review-th2{min-width: 180px;}
.prod-review-th3{min-width: 180px;}
.prod-review-th4{min-width: 180px;}
.prod-review-th5{min-width: 180px;}

.prod-review-td1{min-width: 180px;}
.prod-review-td2{min-width: 180px;}
.prod-review-td3{min-width: 180px;}
.prod-review-td4{min-width: 180px;}
.prod-review-td5{min-width: 180px;}

.respnsve-tbl {
    overflow-x:auto; 
}
table.prod-review-tbl > thead > tr > th input {
    min-width: 150px!important;
}
</style>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>