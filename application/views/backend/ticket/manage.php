<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Tickets</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="active nav-pills-warning">
                                    <a href="#ongoing" data-toggle="tab" aria-expanded="true">Ongoing</a>
                                </li>
                                <li>
                                    <a href="#closed" data-toggle="tab" aria-expanded="false">Closed</a>
                                </li>
                                <?php if ($this->session->userdata['admin']['RoleID'] == 1) { ?>
                                    <li class="">
                                        <a href="#reopened" data-toggle="tab" aria-expanded="false">Re-Opened</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="ongoing">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>User Email</th>
                                    <th>Ticket #</th>
                                    <th>Ticket submitted on</th>
                                    <th>Order #</th>
                                    <th>Order Status</th>
                                    <th>Order Amount</th>
                                    <?php if (checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($ongoing_tickets) {
                                    foreach ($ongoing_tickets as $value) {
                                        $status = $value->Status;
                                        if ($status == 1) {
                                            $class = "btn btn-sm";
                                        } else if ($status == 2) {
                                            $class = "btn btn-primary btn-sm";
                                        } else if ($status == 3) {
                                            $class = "btn btn-warning btn-sm";
                                        }else if ($status == 4) {
                                            $class = "btn btn-success btn-sm";
                                        } else if ($status == 5) {
                                            $class = "btn btn-danger btn-sm";
                                        }
                                        ?>
                                        <tr id="<?php echo $value->TicketID; ?>">
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->Email; ?></td>
                                            <td><?php echo $value->TicketNumber; ?></td>
                                            <td><?php echo date('d-m-Y h:i:s A', strtotime($value->TicketCreatedAt)); ?></td>
                                            <td><?php echo $value->OrderNumber; ?></td>
                                            <td>
                                                <button class="<?php echo @$class; ?>">
                                                    <?php echo $value->OrderStatusEn; ?>
                                                    <div class="ripple-container"></div>
                                                </button>
                                            </td>
                                            <td><?php echo $value->TotalAmount; ?> SAR</td>
                                            <?php if (checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php /*if(checkUserRightAccess(68,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?><!--
                                                    <a href="<?php /*echo base_url('cms/ticket/edit/'.$value->TicketID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php /*} */ ?>
                                                <?php /*if(checkUserRightAccess(68,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $value->TicketID;*/ ?>','cms/ticket/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                --><?php /*} */ ?>
                                                    <a href="<?php echo base_url('cms/ticket/view/' . $value->TicketID); ?>"
                                                       class="btn btn-simple btn-warning btn-icon edit"><i
                                                                class="material-icons"
                                                                title="Click to view ticket details">assignment</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                            <div class="material-datatables tab-pane" id="closed">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>User Email</th>
                                        <th>Ticket #</th>
                                        <th>Ticket submitted on</th>
                                        <th>Order #</th>
                                        <th>Order Status</th>
                                        <th>Order Amount</th>
                                        <?php if (checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($closed_tickets) {
                                        foreach ($closed_tickets as $value) {
                                            $status = $value->Status;
                                            if ($status == 1) {
                                                $class = "btn btn-sm";
                                            } else if ($status == 2) {
                                                $class = "btn btn-primary btn-sm";
                                            } else if ($status == 3) {
                                                $class = "btn btn-warning btn-sm";
                                            }else if ($status == 4) {
                                                $class = "btn btn-success btn-sm";
                                            } else if ($status == 5) {
                                                $class = "btn btn-danger btn-sm";
                                            }
                                            ?>
                                            <tr id="<?php echo $value->TicketID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->TicketNumber; ?></td>
                                                <td><?php echo date('d-m-Y h:i:s A', strtotime($value->TicketCreatedAt)); ?></td>
                                                <td><?php echo $value->OrderNumber; ?></td>
                                                <td>
                                                    <button class="<?php echo @$class; ?>">
                                                        <?php echo $value->OrderStatusEn; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </td>
                                                <td><?php echo $value->TotalAmount; ?> SAR</td>
                                                <?php if (checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php /*if(checkUserRightAccess(68,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?><!--
                                                    <a href="<?php /*echo base_url('cms/ticket/edit/'.$value->TicketID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php /*} */ ?>
                                                <?php /*if(checkUserRightAccess(68,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $value->TicketID;*/ ?>','cms/ticket/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                --><?php /*} */ ?>
                                                        <a href="<?php echo base_url('cms/ticket/view/' . $value->TicketID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view ticket details">assignment</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <?php if ($this->session->userdata['admin']['RoleID'] == 1) { ?>
                            <div class="material-datatables tab-pane" id="reopened">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>User Email</th>
                                        <th>Ticket #</th>
                                        <th>Ticket submitted on</th>
                                        <th>Order #</th>
                                        <th>Order Status</th>
                                        <th>Order Amount</th>
                                        <?php if (checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($reopened_tickets) {
                                        foreach ($reopened_tickets as $value) {
                                            $status = $value->Status;
                                            if ($status == 1) {
                                                $class = "btn btn-sm";
                                            } else if ($status == 2) {
                                                $class = "btn btn-primary btn-sm";
                                            } else if ($status == 3) {
                                                $class = "btn btn-warning btn-sm";
                                            }else if ($status == 4) {
                                                $class = "btn btn-success btn-sm";
                                            } else if ($status == 5) {
                                                $class = "btn btn-danger btn-sm";
                                            }
                                            ?>
                                            <tr id="<?php echo $value->TicketID; ?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->TicketNumber; ?></td>
                                                <td><?php echo date('d-m-Y h:i:s A', strtotime($value->TicketCreatedAt)); ?></td>
                                                <td><?php echo $value->OrderNumber; ?></td>
                                                <td>
                                                    <button class="<?php echo $class; ?>">
                                                        <?php echo $value->OrderStatusEn; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </td>
                                                <td><?php echo $value->TotalAmount; ?> SAR</td>
                                                <?php if (checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(68, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td>
                                                        <?php /*if(checkUserRightAccess(68,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?><!--
                                                    <a href="<?php /*echo base_url('cms/ticket/edit/'.$value->TicketID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php /*} */ ?>
                                                <?php /*if(checkUserRightAccess(68,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $value->TicketID;*/ ?>','cms/ticket/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                --><?php /*} */ ?>
                                                        <a href="<?php echo base_url('cms/ticket/view/' . $value->TicketID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view ticket details">assignment</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <?php } ?>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable();
    });
    setTimeout(function () {
        window.location.reload();
    }, 60000);
</script>