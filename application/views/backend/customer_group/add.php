<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Add Customer Group</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="GroupType">Search Type</label>
                                    <select id="GroupType" class="selectpicker" data-style="select-with-transition"
                                            name="CustomerGroupType">
                                        <option value="Orders">Orders (Count Order)</option>
                                        <option value="Purchases">Purchases (Order Amount Sum)</option>
                                        <option value="AllUsers">All Users</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row users">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="MinimumValue">Minimum Value *</label>
                                    <input type="text" name="MinimumValue" required class="form-control"
                                           id="MinimumValue">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="MaximumValue">Maximum Value *</label>
                                    <input type="text" name="MaximumValue" required class="form-control"
                                           id="MaximumValue">
                                </div>
                            </div>
                        </div>
                        <div class="row users">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="FromDate">From Date</label>
                                    <input type="text" name="FromDate"
                                           class="form-control custom_datepicker" id="FromDate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="ToDate">To Date</label>
                                    <input type="text" name="ToDate" class="form-control custom_datepicker"
                                           id="ToDate">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right m-b-0">
                            <button type="button" class="btn btn-primary waves-effect waves-light"
                                    onclick="showRecords();">
                                Show Records
                            </button>
                            <a href="<?php echo base_url(); ?>cms/CustomerGroup">
                                <button type="button" class="btn btn-default waves-effect m-l-5">
                                    <?php echo lang('back'); ?>
                                </button>
                            </a>
                        </div>
                        <div id="showRecords"></div>
                        <div class="row">
                            <div class="col-md-4">
                                <a class="btn btn-success waves-effect waves-light create_customer_group_button"
                                   style="display: none;" onclick="openCustomerGroupTitlePopup();">
                                    Create Group
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<div id="CreateCustomerGroupTitleModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Customer Group</h4>
            </div>
            <div class="modal-body">
                <div class="form-group label-floating">
                    <label class="control-label" for="CustomerGroupTitle">Customer Group Title</label>
                    <input type="text" name="CustomerGroupTitle" required class="form-control"
                           id="CustomerGroupTitle">
                    <input type="hidden" id="CustomerIds">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="createCustomerGroup();">Create Group</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    
    $( document ).ready(function() {
        $('#GroupType').on('change',function(){
            if($( "#GroupType option:selected" ).val() == 'AllUsers'){
                $('.users').hide();
            }else{
                $('.users').show();
            }
        });
    });
</script>