<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $group_detail->CustomerGroupTitle; ?>
                            (<?php echo $group_detail->CustomerGroupType; ?>) - Group Members</h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/CustomerGroup/add'); ?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add
                                    Customer Group
                                </button>
                            </a>
                            <a href="<?php echo base_url('cms/CustomerGroup'); ?>">
                                <button type="button" class="btn btn-default waves-effect w-md waves-light m-b-5">
                                    Back
                                </button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User ID</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Count / Sum</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($group_members) {
                                    if ($group_detail->CustomerGroupType == 'Orders') {
                                        $unit = ' Order(s)';
                                    } elseif ($group_detail->CustomerGroupType == 'Purchases') {
                                        $unit = ' SAR';
                                    }elseif ($group_detail->CustomerGroupType == 'AllUsers') {
                                        $unit = ' Order(s)';
                                    }
                                    $i = 1;
                                    foreach ($group_members as $value) { ?>
                                        <tr id="<?php echo $value->UserID; ?>">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->UserID; ?></td>
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->Email; ?></td>
                                            <td><?php echo $value->Mobile; ?></td>
                                            <td><?php echo $value->count_sum . $unit; ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>