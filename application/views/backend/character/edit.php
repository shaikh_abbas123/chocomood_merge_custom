<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tab-content">
                                    <form action="<?= base_url() . 'cms/' . $ControllerName . '/action' ?>" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                                        <input type="hidden" name="form_type" value="update">
                                        <input type="hidden" name="<?= $TableKey ?>" value="<?= base64_encode($result[0]->$TableKey) ?>">
                                        <div class="form-group text-right m-b-0">
                                            <div class="row">
                                                

                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Title"><?php echo lang('character_type'); ?></label>
                                                        <select name="CharacterType" class="form-control    ">
                                                            <option value="1" <?= ($result[0]->CharacterType == 1)?'selected':''?>>Alphabet</option>
                                                            <option value="0" <?= ($result[0]->CharacterType == 0)?'selected':''?>>Special</option>
                                                        </select>
                                                    </div>
                                                </div>
                                             
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Title"><?php echo lang('price'); ?></label>
                                                        <input value="<?= $result[0]->price;?>" type="text" name="price" required  class="form-control number-with-decimals" id="Price">
                                                    </div> 
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                                        <img src="<?= base_url() . $result[0]->CharacterImage ?>" style="height:150px;width:150px">
                                                    </div>        
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="Image"></label>
                                                            <input type="file" id="Image" name="Image" title="Select Wrapping Image">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 checkRibbon-radios">
                                                    <div class="form-group label-floating text-left">
                                                        <div class="checkbox">
                                                            <label for="IsActive">
                                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" <?= ((isset($result[0]->IsActive) && $result[0]->IsActive == 1) ? ' checked' : '') ?> /> <?= lang('is_active') ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>


                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                <?= lang('submit') ?>
                                            </button>
                                            <a href="<?= base_url() . 'cms/' . $ControllerName ?>">
                                                <button type="button" class="btn btn-default waves-effect m-l-5">
                                                    <?= lang('back') ?>
                                                </button>
                                            </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".delete_image").on('click', function() {
            var id = $(this).attr('data-image-id');

            url = "cms/Character/DeleteImage";
            reload = "<?php echo base_url(); ?>cms/Character/edit/<?php echo $result[0]->CharacterID; ?>";
            deleteRecord(id, url, reload);

        });
    });
</script>