<style type="text/css">
    .user_field, .user_field_delivery{
        display: none;
    }
</style>
<?php
$option = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }
$option2 = '<option value="0">Select / Unselect All</option>';
if($result[0]->RoleID == 3){
    if(!empty($stores)){
        foreach($stores as $store){
            $selected = '';
            if(!empty($delivery_stores)){
                foreach($delivery_stores as $k=>$v){
                    if($v->store_id == $store->StoreID){
                        $selected = 'selected';
                    }
                }
            }
            $option2 .= '<option value="'.$store->StoreID.'" '.$selected.'>'.$store->Title.' </option>';
        }
    }  
}else if($result[0]->RoleID == 4){
    if(!empty($stores)){
        foreach($stores as $store){
            $selected = '';
            if(!empty($admin_stores)){
                foreach($admin_stores as $k=>$v){
                    if($v->store_id == $store->StoreID){
                        $selected = 'selected';
                    }
                }
            }
            $option2 .= '<option value="'.$store->StoreID.'" '.$selected.'>'.$store->Title.' </option>';
        }
    }  
}else{
    if(!empty($stores)){
        foreach($stores as $store){
            $option2 .= '<option value="'.$store->StoreID.'" '.((isset($result[0]->StoreID) && $result[0]->StoreID == $store->StoreID) ? 'selected' : '').'>'.$store->Title.' </option>';
        }
    }  
}
$option3 = '';
if(!empty($cities)){
    foreach($cities as $city){
        $option3 .= '<option value="'.$city->CityID.'" '.((isset($result[0]->CityID) && $result[0]->CityID == $city->CityID) ? 'selected' : '').'>'.$city->Title.' </option>';
    } } 
$option4 = ' <option value="0">Select / Unselect All</option>';
if($result[0]->RoleID == 3){
   $user_district = explode(',',$result[0]->DistrictID);   
 
if (!empty($districts)) {
    foreach ($districts as $district) {
        $option4 .= '<option value="' . $district->DistrictID . '" '.(in_array($district->DistrictID, $user_district) ? 'selected' : '').'>' . $district->Title . ' </option>';
    }
}  
}
      
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
         $common_fields5 = '';
        $common_fields_lng = '';
        if($key == 0){
         $common_fields = '<div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="selectpicker" data-style="select-with-transition" required name="RoleID">
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';
        $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Email">'.lang('email').'</label>
                                                                <input type="text" disabled name="Email" parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Mobile">Mobile</label>
                                                                <input type="text" name="Mobile" parsley-trigger="change" required  class="form-control" id="Mobile" value="'.((isset($result[0]->Mobile)) ? $result[0]->Mobile : '').'">
                                                            </div>
                                                        </div>
                                                        ';
        $common_fields3 = '<div class="row">
                               '.$common_fields2.'
                               <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Password">'.lang('password').' '.lang('min_length').'</label>
                                        <input type="password" name="Password"   class="form-control" id="password" >
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            $common_fields4 = '<div class="row '.((isset($result[$key]->RoleID) && $result[$key]->RoleID != 1) ? '' : 'user_field').'">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GetStore">'.lang('city').' *</label>
                                        <select id="GetStore" class="selectpicker" data-style="select-with-transition" required name="CityID">

                                            '.$option3.'
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="StoreID">'.lang('choose_store').'  *</label>
                                        <select id="StoreID" class="selectpicker" data-style="select-with-transition" required name="StoreID[]"  multiple data-size="8">

                                            '.$option2.'
                                        </select>
                                    </div>
                                </div>
                            </div>';

           $common_fields5 = '<div class="row '.($result[0]->RoleID == 3 ? '': 'user_field_delivery').'"><div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="DistrictID">'.lang('district').'</label>
                                        <select id="DistrictID" class="form-control selectpicker" required="" name="DistrictID[]" multiple data-size="8" >
                                            '.$option4.'
                                        </select>
                                    </div>
                                </div></div>';
            if($result[0]->RoleID != 1){
                $common_fields_lng =  '
                                                   <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Language">'.lang('language').' </label>
                                        <select class="selectpicker" data-style="select-with-transition" required name="PreferredLang">
                                            <option value="0" '.((@$result[$key]->PreferredLang == 0)? 'selected' : '').' >English</option>
                                            <option value="1" '.((@$result[$key]->PreferredLang == 1)? 'selected' : '') .'>Arabic</option>
                                        </select>
                                    </div>
                                </div>';
            }                                 
       
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                      <div class="row">
                                                     '.$common_fields.'
                                                     <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('name').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->FullName)) ? $result[$key]->FullName : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                      
                                                    
                                                   '.$common_fields3.'

                                                   '.$common_fields4.'


                                                   '.$common_fields5.'
                                                   '.$common_fields_lng.'
                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Languages</h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
   $(document).ready(function () {
        $("#RoleID").on('change',function(){
            if($(this).val() != 1){
                $('.user_field').show();
            }else{
               $('.user_field').hide(); 
            }
        });


         $('#GetStore').on('change',function(){
          
            var CityID = $(this).val();
            var RoleID = $('#RoleID').val();
            
            $('#StoreID').html('');

            if(CityID == '')
            {
                $('#StoreID').html('<option value=""><?php echo lang("choose_store");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/store/getStore',
                    data: {
                        'CityID': CityID,
                        'District' : true
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StoreID').html(result.html).selectpicker('refresh');
                        if(RoleID == 3){
                             $('#DistrictID').html(result.html_district).selectpicker('refresh');
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
     var c = 0; 
    $('#DistrictID').selectpicker().trigger('change');
    $('#DistrictID').selectpicker().change(function(){
        if(c != 0){
            toggleSelectAll($(this));
        }else{
            c++;
        }

    }).trigger('change');
    var d = 0;
    $('#StoreID').selectpicker().trigger('change');
    $('#StoreID').selectpicker().change(function(){
        if(d != 0){
            toggleSelectAll($(this));
        }else{
            d++;
        }

    }).trigger('change');

   });

</script>