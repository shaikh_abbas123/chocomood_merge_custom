<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4&libraries=places"></script>
<?php
$option = '';
if (!empty($cities)) {
    foreach ($cities as $city) {
        $option .= '<option value="' . $city->CityID . '">' . $city->Title . ' </option>';
    }
}
$option2 = '  <option value="0"> Select / Unselect All</option>';
if (!empty($districts)) {
    foreach ($districts as $district) {
        $option2 .= '<option value="' . $district->DistrictID . '">' . $district->Title . ' </option>';
    }
}

$payment_option = '  <option value="0"> Select / Unselect All</option>';
if (!empty($payment_methods)) {
    foreach ($payment_methods as $v) {
        $payment_option .= '<option value="' . $v->PaymentMethodID . '">' . $v->Title . ' </option>';
    }
}

$shipping_option = '  <option value="0"> Select / Unselect All</option>';
if (!empty($shipping_methods)) {
    foreach ($shipping_methods as $v) {
        $shipping_option .= '<option value="' . $v->ShippingMethodID . '">' . $v->Title . ' </option>';
    }
}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style>
   
    #map-canvas {
        height: 100%;
        width: 100%;
    }
   
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add') . ' ' . lang($ControllerName); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-sm-3 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-3 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="isCustomize">
                                                <input name="isCustomize" value="1" type="checkbox" id="isCustomize"
                                                       checked/> Is Customize
                                            </label>
                                        </div>
                                    </div>
                                </div> -->

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursSaturdayToThursdayFrom">Working Hours Saturday to Thursday From</label>
                                        <input type="text" name="WorkingHoursSaturdayToThursdayFrom" required class="form-control timepicker" id="WorkingHoursSaturdayToThursdayFrom">
                                        
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursSaturdayToThursdayTo">Working Hours Saturday to Thursday From</label>
                                        
                                        <input type="text" name="WorkingHoursSaturdayToThursdayTo" required class="form-control timepicker" id="WorkingHoursSaturdayToThursdayTo">
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursFridayFrom">Working Hours Friday From</label>
                                        <input type="text" name="WorkingHoursFridayFrom" required class="form-control timepicker" id="WorkingHoursFridayFrom">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursFridayTo">Working Hours Friday To</label>
                                        <input type="text" name="WorkingHoursFridayTo" required class="form-control timepicker" id="WorkingHoursFridayTo">
                                    </div>
                                </div>
                            </div> 
                             <div class="row">
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursSaturdayToThursdayFrom">Working Hours Saturday to Thursday From</label>
                                        <input type="text" name="WorkingHoursSaturdayToThursdayFrom1" required class="form-control timepicker" id="WorkingHoursSaturdayToThursdayFrom">
                                        
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursSaturdayToThursdayTo">Working Hours Saturday to Thursday From</label>
                                        
                                        <input type="text" name="WorkingHoursSaturdayToThursdayTo1" required class="form-control timepicker" id="WorkingHoursSaturdayToThursdayTo">
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursFridayFrom">Working Hours Friday From</label>
                                        <input type="text" name="WorkingHoursFridayFrom1" required class="form-control timepicker" id="WorkingHoursFridayFrom">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="WorkingHoursFridayTo">Working Hours Friday To</label>
                                        <input type="text" name="WorkingHoursFridayTo1" required class="form-control timepicker" id="WorkingHoursFridayTo">
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VatNo">Vat no</label>
                                        <textarea id="VatNo" name="VatNo" class="form-control" required></textarea>
                                    </div>
                                </div>
                                
                                <!--<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="WorkingHours">Working Hours</label>
                                        <textarea id="WorkingHours" name="WorkingHours" class="form-control"
                                                  required></textarea>
                                    </div>
                                </div>-->
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DeliveryTime">Delivery Time</label>
                                        <textarea id="DeliveryTime" name="DeliveryTime" class="form-control"
                                                  required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="CityID"><?php echo lang('choose_city'); ?></label>
                                        <select id="CityID" class="form-control" required="" name="CityID">
                                            <?php echo $option; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="DistrictID"><?php echo lang('district'); ?></label>
                                        <select id="DistrictID" class="form-control selectpicker" required="" name="DistrictID[]" multiple data-size="8">
                                            <?php echo $option2; ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="PaymentID"><?php echo lang('payment_method'); ?></label>
                                        <select id="PaymentID" class="form-control selectpicker" required="" name="PaymentID[]" multiple data-size="8">
                                            <?php echo $payment_option; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="ShippingID"><?php echo lang('shipping_method'); ?></label>
                                        <select id="ShippingID" class="form-control selectpicker" required="" name="ShippingID[]" multiple data-size="8">
                                            <?php echo $shipping_option; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="phone">Phone</label>
                                        <input type="text" name="phone" required class="form-control" id="phone">
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="Address">Address</label>
                                        <textarea  id="map-search" name="Address" class="form-control" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type='hidden' name='Latitude' id='lat' class="latitude">
                                    <input type='hidden' name='Longitude' id='lng' class="longitude">
                                    <input type='hidden'  class="reg-input-city">
                                    
                                    
                                     <div id="map-canvas" style="height: 400px;"></div>
                                   
                                </div>
                            </div>
                            <!--<input type='hidden' name='Latitude' id='lat'>
                            <input type='hidden' name='Longitude' id='lng'>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <a class="btn btn-primary waves-effect waves-light" data-toggle="modal"
                                           data-target="#mapModal">
                                            Select Location On Map
                                        </a>
                                        <br>
                                        <small>(Jeddah will be selected as location if not changed)</small>
                                    </div>
                                </div>
                            </div>-->


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<!-- notice modal -->
<!--<div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i>
                </button>
                <h5 class="modal-title" id="myModalLabel">Select location on map by dragging marker</h5>
            </div>
            <div class="modal-body">
                <div id="googleMap" style="height: 400px;"></div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('input.timepicker').timepicker({
            minTime: '9:00:00' // 11:45:00 AM,
        });
    });
</script>
<!-- end notice modal -->
<script>
    function initialize() {
        var mapOptions, map, marker, searchBox, city,
        infoWindow = '',
        addressEl = document.querySelector( '#map-search' ),
        latEl = document.querySelector( '.latitude' ),
        longEl = document.querySelector( '.longitude' ),
        element = document.getElementById( 'map-canvas' );
        city = document.querySelector( '.reg-input-city' );
        mapOptions = {
            // How far the maps zooms in.
            zoom: 8,
            // Current Lat and Long position of the pin/
            center: new google.maps.LatLng( 21.484716, 39.189606 ),
            // center : {
            //  lat: -34.397,
            //  lng: 150.644
            // },
            disableDefaultUI: false, // Disables the controls like zoom control on the map if set to true
            scrollWheel: true, // If set to false disables the scrolling on the map.
            draggable: true, // If set to false , you cannot move the map around.
            // mapTypeId: google.maps.MapTypeId.HYBRID, // If set to HYBRID its between sat and ROADMAP, Can be set to SATELLITE as well.
            // maxZoom: 11, // Wont allow you to zoom more than this
            // minZoom: 9  // Wont allow you to go more up.
            styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "color": "#50456d"
            },
            {
                "saturation": "29"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b31f1f"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dfd1ae"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#f00000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "color": "#1a1526"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ff0f0f"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "color": "#892020"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-84"
            },
            {
                "lightness": "85"
            },
            {
                "gamma": "0.00"
            },
            {
                "weight": "1"
            },
            {
                "color": "#50456d"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f6c866"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
        };
    /**
     * Creates the map using google function google.maps.Map() by passing the id of canvas and
     * mapOptions object that we just created above as its parameters.
     *
     */
    // Create an object map with the constructor function Map()
        map = new google.maps.Map( element, mapOptions ); // Till this like of code it loads up the map.
    /**
     * Creates the marker on the map
     *
     */
        marker = new google.maps.Marker({
            position: mapOptions.center,
             icon: '<?php echo base_url(); ?>assets/frontend/images/pin.svg',
            label: {text: '1' , color: 'white'},
            map: map,
            draggable: true
        });
    /**
     * Creates a search box
     */
         searchBox = new google.maps.places.SearchBox( addressEl );
    /**
     * When the place is changed on search box, it takes the marker to the searched location.
     */
    google.maps.event.addListener( searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces(),
            bounds = new google.maps.LatLngBounds(),
            i, place, lat, long, resultArray,
            addresss = places[0].formatted_address;
        for( i = 0; place = places[i]; i++ ) {
            bounds.extend( place.geometry.location );
            marker.setPosition( place.geometry.location );  // Set marker position new.
        }
        map.fitBounds( bounds );  // Fit to the bound
        map.setZoom( 15 ); // This function sets the zoom to 15, meaning zooms to level 15.
        // console.log( map.getZoom() );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        latEl.value = lat;
        longEl.value = long;
        resultArray =  places[0].address_components;
        // Get the city and set the city input value to the one selected
        for( var i = 0; i < resultArray.length; i++ ) {
            if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                citi = resultArray[ i ].long_name;
                city.value = citi;
            }
        }
        // Closes the previous info window if it already exists
        if ( infoWindow ) {
            infoWindow.close();
        }
        /**
         * Creates the info Window at the top of the marker
         */
        infoWindow = new google.maps.InfoWindow({
            content: addresss
        });
        infoWindow.open( map, marker );
    } );
    /**
     * Finds the new position of the marker when the marker is dragged.
     */
    google.maps.event.addListener( marker, "dragend", function ( event ) {
        var lat, long, address, resultArray, citi;
        console.log( 'i am dragged' );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { latLng: marker.getPosition() }, function ( result, status ) {
            if ( 'OK' === status ) {  // This line can also be written like if ( status == google.maps.GeocoderStatus.OK ) {
                address = result[0].formatted_address;
                resultArray =  result[0].address_components;
                // Get the city and set the city input value to the one selected
                for( var i = 0; i < resultArray.length; i++ ) {
                    if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                        citi = resultArray[ i ].long_name;
                        console.log( citi );
                        city.value = citi;
                    }
                }
                addressEl.value = address;
                latEl.value = lat;
                longEl.value = long;
            } else {
                console.log( 'Geocode was not successful for the following reason: ' + status );
            }
            // Closes the previous info window if it already exists
            if ( infoWindow ) {
                infoWindow.close();
            }
            /**
             * Creates the info Window at the top of the marker
             */
            infoWindow = new google.maps.InfoWindow({
                content: address
            });
            infoWindow.open( map, marker );
        } );
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    /*var my_lat = 21.484716;
    var my_lng = 39.189606;

    function initialize() {
        var myLatlng = new google.maps.LatLng(my_lat, my_lng);
        var mapProp = {
            center: myLatlng,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Drag me!',
            draggable: true
        });
        document.getElementById('lat').value = my_lat;
        document.getElementById('lng').value = my_lng;
        // marker drag event
        google.maps.event.addListener(marker, 'drag', function (event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
        });

        //marker drag event end
        google.maps.event.addListener(marker, 'dragend', function (event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
            // alert("lat=>" + event.latLng.lat());
            // alert("long=>" + event.latLng.lng());
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);*/

     $(document).ready(function () {
        

         $('#CityID').on('change',function(){
          
            var CityID = $(this).val();
            $('#DistrictID').html('');

            if(CityID == '')
            {
                $('#DistrictID').html('<option value=""><?php echo lang("district");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "GET",
                    url: base_url + 'cms/district/getDistrictsForCity',
                    data: {
                        'CityID': CityID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#DistrictID').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
    $('#DistrictID').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
    $('#ShippingID').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
    $('#PaymentID').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
        
   });



</script>