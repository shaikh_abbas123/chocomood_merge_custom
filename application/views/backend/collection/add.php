<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Url Link</label>
                                        <input type="text" name="Link" required  class="form-control" id="Link">
                                    </div>
                                </div>
                                
                            </div>
                            
                            <!--<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description"><?php echo lang('description'); ?></label>
                                        <textarea class="form-control" name="Description" id="Description" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Ingredients"><?php echo lang('ingredients'); ?></label>
                                        <textarea class="form-control" name="Ingredients" id="Ingredients" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Specifications">Specifications</label>
                                        <textarea class="form-control summernote" name="Specifications" id="Specifications" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Tags"><?php echo lang('tags'); ?></label>
                                        <input type="text" name="Tags" required  class="form-control" id="Tags">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Keywords"><?php echo lang('keywords'); ?></label>
                                        <input type="text" name="Keywords" required  class="form-control" id="Keywords">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MetaTages"><?php echo lang('meta_tages'); ?></label>
                                        <input type="text" name="MetaTags" required  class="form-control" id="MetaTages">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MetaKeywords"><?php echo lang('meta_keywords'); ?></label>
                                        <input type="text" name="MetaKeywords" required  class="form-control" id="MetaKeywords">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MetaDescription"><?php echo lang('meta_description'); ?></label>
                                        <textarea class="form-control" name="MetaDescription" id="MetaDescription" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>-->

                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsFeatured">
                                                <input name="IsFeatured" value="1" type="checkbox" id="IsFeatured"/> <?php echo lang('is_featured'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>-->
                                
                            </div>
                           <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="ProductID[]" class="form-control" id="ProductID" required multiple>
                                            <option value=""><?php echo 'Choose '.lang('products');?></option>
                                            <?php foreach ($products as $key => $value) { ?>
                                                <option value="<?php echo $value->ProductID; ?>"><?php echo $value->Title; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                </div>-->
                                <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Home Page Image</label>
                                    <div class="form-group">
                                        
                                        <label>&nbsp;</label> 
                                        <input type="file" name="HomeImage[]">
                                        <p>Dimission For 1st and 2nd (400*450) For 3rd (400*950) and For 4th(800*450)</p>
                                    </div>
                                </div>
                            </div>

                            <!--<div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" name="Image[]" multiple="multiple">
                                        <p><?php echo lang('cannot_upload_more_then'); ?></p>
                                    </div>
                                </div>
                            </div>-->



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
                   
       $('#CategoryID').on('change',function(){
           
            var CategoryID = $(this).val();

            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/category/getSubCategory',
                    data: {
                        'CategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#SubCategoryID').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
      

       

       
    });

    
</script>