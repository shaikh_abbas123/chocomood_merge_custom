<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Abandoned Carts</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="active nav-pills-warning">
                                    <a href="#user" data-toggle="tab" aria-expanded="true">By Users</a>
                                </li>
                                <li class="">
                                    <a href="#anonymous" data-toggle="tab" aria-expanded="false">By Anonymous</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="respnsve-tbl material-datatables tab-pane active" id="user">
                                <table id="" class="tbl-td-width search_fields datatables_csv table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th class="aband-cart-th1">Customer</th>
                                        <th class="aband-cart-th2">Email</th>
                                        <th class="aband-cart-th3">Products</th>
                                        <th class="aband-cart-th4">Quantity</th>
                                        <th class="aband-cart-th5">Subtotal</th>
                                        <th class="aband-cart-th6" class="notexport">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($abandoned_carts) {
                                        foreach ($abandoned_carts as $abandoned_cart) { ?>
                                            <tr>
                                                
                                                <td class="aband-cart-td1"><?php echo $abandoned_cart->FullName; ?></td>
                                                <td class="aband-cart-td2">
                                                    <a href="mailto:<?php echo $abandoned_cart->Email; ?>"><?php echo $abandoned_cart->Email; ?></a>
                                                </td>
                                                <td class="aband-cart-td3"><?php echo $abandoned_cart->CartProductsCount; ?></td>
                                                <td class="aband-cart-td4"><?php echo $abandoned_cart->CartQuantityCount; ?></td>
                                                <td class="aband-cart-td5"><?php echo $abandoned_cart->SubTotal; ?></td>
                                                <?php if (checkUserRightAccess(87, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(87, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <td class="aband-cart-td6">
                                                        <a href="javascript:void(0);"
                                                           class="btn btn-simple btn-warning btn-icon edit"
                                                           onclick="notifyUser('<?php echo $abandoned_cart->UserID; ?>');"><i
                                                                    class="material-icons"
                                                                    title="Click to notify this user via email">dvr</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="anonymous">
                                <table id="" class="search_fields datatables_csv2 table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>User Temp Key</th>
                                        <th>Total Products In Cart</th>
                                        <th>Total Quantity In Cart</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($abandoned_carts_anonymous) {
                                        foreach ($abandoned_carts_anonymous as $abandoned_cart) { ?>
                                            <tr>
                                                <td><?php echo $abandoned_cart->UserID; ?></td>
                                                <td><?php echo $abandoned_cart->CartProductsCount; ?></td>
                                                <td><?php echo $abandoned_cart->CartQuantityCount; ?></td>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<style>
    .aband-cart-th1{min-width: 180px;}
    .aband-cart-th2{min-width: 180px;}
    .aband-cart-th3{min-width: 180px;}
    .aband-cart-th4{min-width: 180px;}
    .aband-cart-th5{min-width: 180px;}
    .aband-cart-th6{min-width: 180px;}

    .aband-cart-td1{min-width: 180px;}
    .aband-cart-td2{min-width: 180px;}
    .aband-cart-td3{min-width: 180px;}
    .aband-cart-td4{min-width: 180px;}
    .aband-cart-td5{min-width: 180px;}
    .aband-cart-td6{min-width: 180px;}

    .respnsve-tbl {
        overflow-x:auto; 
    }
    table.tbl-td-width > thead > tr > th input {
        min-width: 150px!important;
    }
</style>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>

<script>
    $(document).ready(function () {
        $('table.datatable').DataTable();
    });

    function notifyUser(UserID) {
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + 'cms/abandonedCart/sendAbandonedCartNotificationToUser',
            data: {'UserID': UserID},
            dataType: "json",
            success: function (result) {
                hideCustomLoader();
                showSuccess(result.message);
            }
        });
    }
</script>