<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add') . ' '.$for ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                            <input type="hidden" name="for" value="<?= $for?>">

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxPrice">Box Price</label>
                                        <input type="text" name="BoxPrice" required class="form-control number-with-decimals" id="BoxPrice">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxSpace">Box Capacity
                                            <small>(No. of pieces box can have in it)</small>
                                        </label>
                                        <input type="number" name="BoxSpace" required class="form-control"
                                               id="BoxSpace">
                                    </div>
                                </div>
                                <?php if($for == 'Choco Box')
                                {
                                ?>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxCategory">Box Category</label>
                                        <select id="BoxCategory" class="selectpicker" 
                                                name="BoxCategory" >
                                            <option value=""><?php echo lang("choose_category");?></option>
                                            <?php
                                                foreach($box_categories as $k => $v){
                                            ?>
                                                     <option value="<?= @$v->BoxCategoryID; ?>"><?= @$v->Title; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxType">Box Category</label>
                                        <select id="SubCategoryID" class="selectpicker" name="BoxType" >
                                        <option value=""><?php echo lang("choose_category");?></option>
                                        <!-- <?php
                                                foreach($box_categories as $k => $v){
                                            ?>
                                                     <option value="<?= @$v->BoxCategoryID; ?>"><?= @$v->Title; ?></option>
                                            <?php } ?> -->
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <select name="PriceType" class="form-control" id="PriceType" required>
                                                           
                                        <option value="item"><?php echo lang('price_type_item');?></option>
                                        <option value="kg"><?php echo lang('price_type_kg');?></option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="no_of_row">Weight
                                            <small>(If price type kg, enter weight in grams)</small>
                                        </label>
                                        <input type="number" name="weight" required class="form-control" min="0" 
                                               id="weight">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="no_of_row">No of Rows 
                                            <!-- <small>(No. of pieces box can have in it)</small> -->
                                        </label>
                                        <input type="number" name="no_of_row" required class="form-control" min="0" 
                                               id="no_of_row">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="no_of_choclate_in_row">No of chocolate as per row sequences 
                                            <!-- <small>(No. of pieces box can have in it)</small> -->
                                        </label>
                                        <input type="number" name="no_of_choclate_in_row" required class="form-control" min="0" 
                                               id="no_of_choclate_in_row">
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="height">Box Height 
                                            <!-- <small>(No. of pieces box can have in it)</small> -->
                                        </label>
                                        <input type="text" name="height" required class="form-control" min="0" 
                                               id="height">
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="orderProcessing">Order Processing Time 
                                            <!-- <small>(No. of pieces box can have in it)</small> -->
                                        </label>
                                        <input type="text" name="orderProcessing" required class="form-control" 
                                               id="orderProcessing">
                                    </div>
                                </div>
                                

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MinOrder">Min Order  
                                            <small>(No. of boxes)</small>
                                        </label>
                                        <input type="number" name="MinOrder" required class="form-control" 
                                               id="MinOrder">
                                    </div>
                                </div>
                                 
                                 <!-- <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxSpace">Choclate Type  
                                             <small>(No. of pieces box can have in it)</small> 
                                        </label>
                                        <input type="text" name="box_height" required class="form-control" 
                                               id="box_height">
                                    </div>
                                </div>  -->
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxInside">Choclates</label>
                                        <select id="BoxInside" class="selectpicker" 
                                                name="BoxInside[]" multiple data-size="50" >
                                            <option value="0">Select / Unselect All</option>
                                            <?php
                                                foreach($products as $k => $v){
                                            ?>
                                                     <option value="<?= @$v->ProductID; ?>"><?= @$v->Title; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <?php } ?>
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Description</label>
                                        <textarea class="form-control summernote" name="Description"
                                                  id="Description" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    
                                        <label >Box Image (Outside)</label>
                                        <input type="file" id="BoxImage" name="BoxImage" title="Select Box Image Outside">
                                    
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    
                                        <label >Box Image (Inside)</label>
                                        <input type="file" id="BoxImageInside" name="BoxImageInside" title="Select Box Image Inside">
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName.($for == "Choco Box"?'/choco_box_box':''); ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
$(document).ready(function () {
    $('#BoxInside').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
     /*$('#BoxType').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');*/
      $('#BoxCategory').on('change',function(){
           
            var CategoryID = $(this).val();
            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/box_category/getSubCategory',
                    data: {
                        'BoxCategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {
                        $('#SubCategoryID').html(result.html);
                        $('#SubCategoryID').selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
});
</script>