<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($boxes_type)){ 
    foreach($boxes_type as $v){ 
        $option .= '<option value="'.$v->BoxTypeID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } 
}

if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        if ($key == 0) {

            $common_fields2 = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxPrice">Box Price</label>
                                        <input type="text" name="BoxPrice" required  class="form-control number-with-decimals" id="BoxPrice" value="' . $result[$key]->BoxPrice . '">
                                        <input type="hidden" name="for" value="'.$result[$key]->BoxFor.'">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxSpace">Box Capacity <small>(No. of pieces box can have in it)</small></label>
                                        <input type="number" name="BoxSpace" required  class="form-control" id="BoxSpace" value="' . $result[$key]->BoxSpace . '">
                                    </div>
                                </div>
                                ';
                                if($result[$key]->BoxFor == 'Choco Box')
                                {
                                    $common_fields2 .= '<div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxCategory">Box Category</label>
                                        <select id="BoxCategory" class="selectpicker" 
                                                name="BoxCategory" >
                                            <option value="">'. lang("choose_category").'</option>';
                                            
                                                foreach($box_categories as $k => $v){
                                                    $common_fields2 .= '<option value="'. @$v->BoxCategoryID.'"';
                                                    if($selected_box_cat_parent == $v->BoxCategoryID)
                                                    {
                                                        $common_fields2 .= ' selected';
                                                    }
                                                    $common_fields2 .= ' >'. @$v->Title.'</option>';
                                             } 
                                             $common_fields2 .='    </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxType">Box Type</label>
                                        <select id="SubCategoryID" class="selectpicker" name="BoxType" >
                                           ';
                                           foreach($box_sub_categories_all as $k => $v)
                                           {
                                            $common_fields2 .= '<option value="'. @$v->BoxCategoryID.'"';
                                            if($selected_box_cat_sub == $v->BoxCategoryID)
                                            {
                                                $common_fields2 .= ' selected';
                                            }
                                            $common_fields2 .= ' >'. @$v->Title.'</option>';
                                           }
                                           $common_fields2 .= '
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <select name="PriceType" class="form-control" id="PriceType" required>
                                        <option '.(($result[$key]->PriceType == 'item')?'selected':'').' value="item">'. lang('price_type_item').'</option>
                                        <option '.(($result[$key]->PriceType == 'kg')?'selected':'').' value="kg">'. lang('price_type_kg').'</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="no_of_row">Weight
                                            <small>(If price type kg, enter weight in grams)</small>
                                        </label>
                                        <input type="number" name="weight" required class="form-control" min="0" value="' . $result[$key]->weight . '" id="weight">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="no_of_row">No of Rows </label>
                                        <input type="number" name="no_of_row" required class="form-control" min="0" value="' . $result[$key]->no_of_row . '" id="no_of_row">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="no_of_choclate_in_row">No of chocolate as per row sequences </label>
                                        <input type="number" name="no_of_choclate_in_row" required class="form-control" min="0" value="' . $result[$key]->no_of_choclate_in_row . '" id="no_of_choclate_in_row">
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="height">Box Height </label>
                                        <input type="text" name="height" required class="form-control" value="' . $result[$key]->height . '" min="0" id="height">
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="orderProcessing">Order Processing Time </label>
                                        <input type="text" name="orderProcessing" required class="form-control" value="' . $result[$key]->orderProcessing . '" id="orderProcessing">
                                    </div>
                                </div>
                                

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MinOrder">Min Order  
                                            <small>(No. of boxes)</small>
                                        </label>
                                        <input type="number" name="MinOrder" required class="form-control" value="' . $result[$key]->MinOrder . '" id="MinOrder">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BoxInside">Choclates</label>
                                        <select id="BoxInside" class="selectpicker" name="BoxInside[]" multiple data-size="50" >
                                            <!-- <option value="0">Select / Unselect All</option> -->
                                        ';
                                            
                                            foreach ($products as $v) {
                                                $selected = '';
                                                foreach ($inside_products as $v2) 
                                                {
                                                    if($v2[0]->ProductID == $v->ProductID)
                                                    {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                $common_fields2 .= ' <option '.$selected.' value="'.@$v->ProductID.'">'. @$v->Title.'</option>';
                                            }
                                            $common_fields2 .= '</select>
                                    </div>
                                </div>
                                ';
                                }

                                $common_fields2 .= '
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>';

            $common_fields = '<div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="' . base_url() . $result[$key]->BoxImage . '" width="200" height="200">
                                    </div>   
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                            <img src="' . base_url() . $result[$key]->BoxImageInside . '" width="200" height="200">
                                    </div>      
                                            
                                </div>
                                        
                            <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                
                                                    <label >Box Image (Outside)</label>
                                                    <input type="file" id="BoxImage" name="BoxImage" title="Select Box Image">
                                                
                                            </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                   
                                                        <label >Box Image (Inside)</label>
                                                        <input type="file" id="BoxImageInside" name="BoxImageInside" title="Select Box Image">
                                                   
                                                </div>
                                            </div>';
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">
                                                    <input type="hidden" name="for" value="'.$result[0]->BoxFor.'">
                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                                <input type="hidden" name="Title_old" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description' . $key . '">Description</label>
                                                                <textarea class="form-control summernote" name="Description"
                                                                          id="Description' . $key . '" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                         ' . $common_fields2 . '
                                                    </div>
                                                    ' . $common_fields . '
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName .($result[0]->BoxFor == "Choco Box"?'/choco_box_box':''). '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
    // $('#BoxInside').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
     
      $('#BoxCategory').on('change',function(){
           
            var CategoryID = $(this).val();
            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/box_category/getSubCategory',
                    data: {
                        'BoxCategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {
                        $('#SubCategoryID').html(result.html);
                        $('#SubCategoryID').selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
});
</script>