<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
.form-group.label-floating label.control-label, .form-group.label-placeholder label.control-label {
    color: #bd9371;
    position: static;
    float: left;
    min-width: 160px;
}
.form-group.label-floating .form-control,
.select2-container {
    float: right;
    width: calc(100% - 160px) !important;
    border-color: #D2D2D2;
    border-style: solid;
    border-width: 1px 1px 1px;
    background-position: center bottom, center calc(100% + 1px);
    padding-left: 10px;
    padding-right: 10px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
    border: none !important;
    outline: 0;
}
.select2-container--default .select2-selection--multiple {
    background: transparent;
    border: 0;
    border-radius: 0;
}
.select2-container--default .select2-selection--multiple ul {
    padding:0;
}
.add_whats_inside + .row .form-group.label-floating .form-control {
    width: 100%;
    margin-top: 5px;
}
.form-group.label-floating .btn-group.bootstrap-select,
.form-group.label-floating .note-editor.note-frame {
    float: right;
    width: calc(100% - 165px);
}
.note-editor.note-frame {
    border: 1px solid #D2D2D2 !important;
}
.chocoCMSAccordion .card {
    margin: 15px 0;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.2);
    /* overflow: hidden; */
}
.chocoCMSAccordion .card .card-header {
    padding: 0 !important;
}
.chocoCMSAccordion .card .card-header h2 {
    margin: 0;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link {
    color: #50456d;
    background: transparent;
    border: 0;
    border-radius: 0;
    margin: 0;
    box-shadow: none;
    padding: 14px 20px;
    line-height: 1.5;
    min-height: inherit;
    font-size: 22px;
    font-weight: normal;
    display: block;
    width: 100%;
    text-align: inherit;
    text-decoration: none;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link:hover {
    background-color: #f7f4eb;
}
.chocoCMSAccordion .card .card-body {
    padding: 50px 35px;
    /* border-top: 2px solid #513c7e; */
}
.chocoCMSAccordion .card .card-header i.fa {
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    line-height: 12px;
    margin-top: 8px;
    margin-right: 4px;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn[aria-expanded="true"] i.fa {
    transform: rotateX(180deg);
}

.add-packages-field-width{
    min-width: 100% !important;
    position: relative;
    left: 12px;
}


@media (min-width: 1330px) and (max-width: 1440px){
    .respnsve-width-packages{
        min-width:100% !important;
        position: relative;
        left: 12px;
    }
    .per_pc_weight_in_gm{
        min-width:100px !important;
    }
     .add-packages-field-width {
        min-width: 100% !important;
        position: relative;
        left: 26px;
    }
}

@media (min-width: 992px) and (max-width: 1024px){
    .per_pc_weight_in_gm{
        min-width:75px !important;
    }
    .add-packages-field-width{
        min-width: 62px !important;
        position: relative;
        top: 55px;
        margin-bottom: 60px;
    }
    .respnsve-width-packages{
        min-width: 138px !important;
        position: relative;
        top: 55px;
        left: 15px;
        margin-bottom: 60px;
    }
    .respnsve-lbl-width-packages{
        min-width:80px !important;
    }
}

@media (min-width:1680px){
    .respnsve-width-packages{
        min-width:100%;
    }
    .per_pc_weight_in_gm{
        min-width:100px !important;
    }
    
}

</style>
<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $nutrition_dropdown ='';
        $tags_dropdown ='';
        if ($key == 0) {
            foreach ($nutritions as $nutrition) {
                $nutrition_dropdown .= '<option ' . ((!empty($product_nutritions_result) && in_array($nutrition->NutritionID, $product_nutritions_result)) ? 'selected' : '') . ' value="' . $nutrition->NutritionID . '" class="nutrition_' . $nutrition->NutritionID . '" data-quantity="' . (isset($product_nutritions_quanity[$nutrition->NutritionID]) ? $product_nutritions_quanity[$nutrition->NutritionID] : 0) . '">' . $nutrition->Title . '</option>';
            }
            foreach($tags as $tag){
                $tags_dropdown .= '<option ' . (in_array($tag->TagID,explode(',',$result[$key]->TagIDs )) ? 'selected="selected"' : '') . ' value="' . $tag->TagID . '">' . $tag->Title . '</option>';
            }
            
            $common_fields2 = '
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_weight">Price Per KG</label>
                                        <input type="text" name="price_per_kg" required class="form-control number-with-decimals" id="price_per_kg" value="' . $result[$key]->PricePerKG . '">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_length">Quantity Per KG</label>
                                        <input type="text" name="quantity_per_kg" id="quantity_per_kg" required class="form-control number-with-decimals"  value="' . $result[$key]->QuantityPerKG . '">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_weight">Price Per PCS</label>
                                        <input type="text" readonly name="price_per_pcs" id="price_per_pcs" required class="form-control number-with-decimals" value="' . $result[$key]->PricePerPcs . '">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_length">Order Processing Time</label>
                                        <input type="text" name="processing_time" required class="form-control" id="processing_time" value="' . $result[$key]->OrderProcessingTime . '">
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_weight">Individual Size</label>
                                        <input type="text" name="individual_size" required class="form-control " id="individual_size" value="' . $result[$key]->IndividualSize . '">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_length">Minimum Order</label>
                                        <input type="text" name="minimum_order" required class="form-control number-with-decimals" id="minimum_order" value="' . $result[$key]->MinimumOrder . '">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_length">Maximum Order</label>
                                        <input type="text" name="maximum_order" required class="form-control number-with-decimals" id="maximum_order" value="' . $result[$key]->MaximumOrder . '">
                                    </div>
                                </div>
                            </div>
                            
                                ';

            $common_fields = '<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <img src="'.base_url().$result[$key]->ShapeImage.'" style="height:200px;width:200px;">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group label-floating">
                            <label for="Image">Shape Custom Image, it should be 250 × 250 px</label>
                            <input type="file" id="Image" name="Image"  title="Select Shape Image, it should be 250 × 250 px">
                        </div>
                    </div>
                </div>
                
            <br>';
            $common_fields .= '<div class="row">';
            $shape_images = get_images($result[$key]->ShapeID, 'shape');
            if ($shape_images) {
                foreach ($shape_images as $shape_image) {
                    $common_fields .= '<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-trash delete_image" data-image-id="' . $shape_image->SiteImageID . '" aria-hidden="true"></i><img src="' . base_url() . $shape_image->ImageName . '" style="height:200px;width:200px;"></div>';
                }
            }                    
            $common_fields .='
                                          
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="ShapeImage">Shape Image, it should be 800 × 800 px</label>
                                        <input type="file" id="ShapeImage" name="ShapeImage[]" multiple="multiple" title="Select Shape Image">
                                                </div>
                                            </div>
                                        </div>';
            $common_fields3 = '
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Tags *</label>
                                    <select name="TagIDs[]" class="selectpicker select2" data-style="select-with-transition" id="TagID" required multiple>
                                        <option value="">'.lang('tags').'</option>
                                        '.$tags_dropdown.'
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">' . lang('select_nutritions') . '</label>
                                    <select  class="form-control" id="Sl_Chocolate" multiple required>
                                        
                                        ' . $nutrition_dropdown . '
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row eddie" id="multiple_fields">
                            ' . $product_nutrition_field . '
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="dimension_length">Length</label>
                                    <input type="text" name="dimension_length" required  class="form-control" id="dimension_length" value="' . $result[$key]->dimension_length . '">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="dimension_width">Width</label>
                                    <input type="text" name="dimension_width" required  class="form-control number-with-decimals" id="dimension_width" value="' . $result[$key]->dimension_width . '">
                                </div>
                            </div>
                        </div>
                        <div class="row">   
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="dimension_height">Height</label>
                                    <input type="text" name="dimension_height" required class="form-control number-with-decimals" id="dimension_height" value="' . $result[$key]->dimension_height . '">
                                </div>
                            </div>
                        </div>
                        <div class="row">    
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="dimension_weight">Weight</label>
                                    <input type="text" name="dimension_weight" required class="form-control number-with-decimals" id="dimension_weight" value="' . $result[$key]->dimension_weight . '">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="inner_dimension_length">Inner Length</label>
                                    <input type="text" name="inner_dimension_length" required class="form-control number-with-decimals" id="inner_dimension_length" value="' . $result[$key]->inner_dimension_length . '">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="inner_dimension_weight">Inner Weight</label>
                                    <input type="text" name="inner_dimension_weight" required class="form-control number-with-decimals" id="inner_dimension_weight" value="' . $result[$key]->inner_dimension_weight . '">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 checkRibbon-radios">
                                <div class="form-group label-floating">
                                    <div class="checkbox">
                                        <label for="IsActive">
                                            <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
            ';
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
        <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;"
        class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
        <input type="hidden" name="form_type" value="update">
        <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
        <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
        <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">
    
        <div class="accordion chocoCMSAccordion" id="addProdAccordion">
            <div class="card">
                <div class="card-header" id="headingOne'.$key.'">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne'.$key.'"
                            aria-expanded="false" aria-controls="collapseOne"><span><i class="fa fa-angle-down"
                                    aria-hidden="true"></i></span>Product Information</button>
                    </h2>
                </div>
                <div id="collapseOne'.$key.'" class="collapse" aria-labelledby="headingOne" data-parent="#addProdAccordion"
                    aria-expanded="true" style="">
                    <div class="card-body">
                        <div class="row">
    
                            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                    <input type="text" name="Title" parsley-trigger="change" required class="form-control"
                                        id="Title' . $key . '"
                                        value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                    <input type="hidden" name="Title_old"
                                        value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
    
                                </div>
                            </div>
                        </div>
                        
                            ' . $common_fields2 . '
                        
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo'.$key.'">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo'.$key.'" aria-expanded="false" aria-controls="collapseTwo"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Description</button>
                    </h2>
                </div>
                <div id="collapseTwo'.$key.'" class="collapse" aria-labelledby="headingTwo" data-parent="#addProdAccordion" aria-expanded="false" style="height: 0px;">
                    <div class="card-body">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="Ingredients">' . lang('ingredients') . '</label>
                                    <textarea class="form-control" name="Ingredients" id="Ingredients" style="height: 100px;">' . ((isset($result[$key]->Ingredients)) ? $result[$key]->Ingredients : '') . '</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="Specifications">Specifications</label>
                                    <textarea class="form-control summernote" name="Specifications" id="Specifications" style="height: 100px;">' . ((isset($result[$key]->Specifications)) ? $result[$key]->Specifications : '') . '</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="description' . $key . '">' . lang('description') .
                                        '</label>
                                    <textarea rows="5" name="description" required class="form-control"
                                        id="description' . $key . '">'.((isset($result[$key]->Description)) ? $result[$key]->Description : '').'</textarea>
                                    <input type="hidden" name="description_old"
                                        value="' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="Keywords">Keywords</label>
                                    <input type="text" name="Keywords" value="' . ((isset($result[$key]->Keywords)) ? $result[$key]->Keywords : '') . '" required  class="form-control" id="Keywords">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="MetaTages">Meta Tags</label>
                                    <input type="text" name="MetaTags" value="' . ((isset($result[$key]->MetaTags)) ? $result[$key]->MetaTags : '') . '" required  class="form-control" id="MetaTages">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="MetaKeywords">Meta Keywords</label>
                                    <input type="text" name="MetaKeywords" value="' . ((isset($result[$key]->MetaKeywords)) ? $result[$key]->MetaKeywords : '') . '" required  class="form-control" id="MetaKeywords">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="MetaDescription">Meta Description</label>
                                    <textarea class="form-control" name="MetaDescription" id="MetaDescription" style="height: 100px;">' . ((isset($result[$key]->MetaDescription)) ? $result[$key]->MetaDescription : '') . '</textarea>
                                </div>
                            </div>
                        </div>
                        
                        ' . $common_fields3. '
                        ' . $common_fields . '
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group text-right m-b-0">
            <button class="btn btn-primary waves-effect waves-light" type="submit">
                ' . lang('submit') . '
            </button>
            <a href="' . base_url() . 'cms/' . $ControllerName . '">
                <button type="button" class="btn btn-default waves-effect m-l-5">
                    ' . lang('back') . '
                </button>
            </a>
        </div>
    </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".delete_image").on('click', function () {
        var id = $(this).attr('data-image-id');

        url = "cms/product/deleteImage2";
        reload = "<?php echo base_url();?>cms/shape/edit/<?php echo $result[0]->ShapeID; ?>";
        deleteRecord(id, url, reload);

    });
    
    $("#price_per_kg, #quantity_per_kg").on("keyup change blur click", function(e) {
        var price_per_kg = $('#price_per_kg').val();
        var quantity_per_kg = $('#quantity_per_kg').val();
        if(price_per_kg > 0 || quantity_per_kg > 0)
        {
            $('#price_per_pcs').val((price_per_kg/quantity_per_kg).toFixed(2));
        }
    })
</script>