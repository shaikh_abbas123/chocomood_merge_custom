<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
.form-group.label-floating label.control-label, .form-group.label-placeholder label.control-label {
    color: #bd9371;
    position: static;
    float: left;
    min-width: 160px;
}
.form-group.label-floating .form-control,
.form-group.label-floating .select2-container {
    float: right;
    width: calc(100% - 160px) !important;
    border-color: #D2D2D2;
    border-style: solid;
    border-width: 1px 1px 1px;
    background-position: center bottom, center calc(100% + 1px);
    padding-left: 10px;
    padding-right: 10px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
    border: none !important;
    outline: 0;
}
.select2-container--default .select2-selection--multiple {
    background: transparent;
    border: 0;
    border-radius: 0;
}
.select2-container--default .select2-selection--multiple ul {
    padding:0;
}
.add_whats_inside + .row .form-group.label-floating .form-control {
    width: 100%;
    margin-top: 5px;
}
.form-group.label-floating .btn-group.bootstrap-select,
.form-group.label-floating .note-editor.note-frame {
    float: right;
    width: calc(100% - 165px);
}
.note-editor.note-frame {
    border: 1px solid #D2D2D2 !important;
}
.chocoCMSAccordion .card {
    margin: 15px 0;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.2);
    /* overflow: hidden; */
}
.chocoCMSAccordion .card .card-header {
    padding: 0 !important;
}
.chocoCMSAccordion .card .card-header h2 {
    margin: 0;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link {
    color: #50456d;
    background: transparent;
    border: 0;
    border-radius: 0;
    margin: 0;
    box-shadow: none;
    padding: 14px 20px;
    line-height: 1.5;
    min-height: inherit;
    font-size: 22px;
    font-weight: normal;
    display: block;
    width: 100%;
    text-align: inherit;
    text-decoration: none;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn.btn-link:hover {
    background-color: #f7f4eb;
}
.chocoCMSAccordion .card .card-body {
    padding: 50px 35px;
    /* border-top: 2px solid #513c7e; */
}
.chocoCMSAccordion .card .card-header i.fa {
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    line-height: 12px;
    margin-top: 8px;
    margin-right: 4px;
    transition-duration: 0.5s;
}
.chocoCMSAccordion .card .card-header button.btn[aria-expanded="true"] i.fa {
    transform: rotateX(180deg);
}
.add-packages-field-width{
    min-width: 70% !important;
}

@media (min-width: 1330px) and (max-width: 1440px){
    .respnsve-width-packages{
        min-width:100% !important;
    }
    .per_pc_weight_in_gm{
        min-width:100px !important;
    }
}

@media (min-width: 992px) and (max-width: 1024px){
    .per_pc_weight_in_gm{
        min-width:100px !important;
    }
    .add-packages-field-width{
        min-width:75px !important;
        /*position: relative;*/
        /*top: 40px;*/
        margin-bottom: 20px;
    }
    .respnsve-width-packages{
        min-width:160px !important;
        position: relative;
        top: 40px;
        margin-bottom: 20px;
    }
}

@media (min-width:1680px){
    .respnsve-width-packages{
        min-width:100%;
    }
}

input.upload-img {
    top: 20px !important;
}
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add') . ' ' . lang($ControllerName); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">

                            <div class="accordion chocoCMSAccordion" id="addProdAccordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Product Information</button>
                                    </h2>
                                    </div>
                                    <div id="collapseOne" class="collapse in" aria-labelledby="headingOne" data-parent="#addProdAccordion" aria-expanded="true">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Title"><?php echo lang('title'); ?> *</label>
                                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                                    </div>
                                                </div>  
                                            </div>  
                                            <div class="row">                                          
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_weight">Price Per KG</label>
                                                        <input type="text" name="price_per_kg" id="price_per_kg" required class="form-control number-with-decimals" >
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_length">Quantity Per KG</label>
                                                        <input type="text" name="quantity_per_kg" id="quantity_per_kg" required class="form-control number-with-decimals" >
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_weight">Price Per PCS</label>
                                                        <input type="text" readonly name="price_per_pcs" id="price_per_pcs" required class="form-control number-with-decimals" >
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_length">Order Processing Time</label>
                                                        <input type="text" name="processing_time" required class="form-control" >
                                                    </div>
                                                </div>
                                            </div>  
                                             
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_weight">Serving Size</label>
                                                        <input type="text" name="individual_size" required class="form-control " id="inner_dimension_weight">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_length">Minimum Order</label>
                                                        <input type="text" name="minimum_order" required class="form-control number-with-decimals" id="inner_dimension_length">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_length">Maximum Order</label>
                                                        <input type="text" name="maximum_order" required class="form-control number-with-decimals" id="inner_dimension_length">
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span><i class="fa fa-angle-down" aria-hidden="true"></i></span>Description</button>
                                    </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#addProdAccordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Ingredients"><?php echo lang('ingredients'); ?></label>
                                                        <textarea class="form-control" name="Ingredients" id="Ingredients" style="height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label"><?php echo lang('select_nutritions');?></label>
                                                        <select  class="form-control" id="Sl_Chocolate" multiple required>
                                                            <?php foreach ($nutritions as $key => $nutrition) { ?>
                                                                <option value="<?php echo $nutrition->NutritionID; ?>" class="nutrition_<?php echo $nutrition->NutritionID; ?>" data-quantity="0"><?php echo $nutrition->Title; ?></option>
                                                        <?php        
                                                                
                                                            }
                                                            ?>
                                                            
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="row" id="multiple_fields"></div>        
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Specifications">Specifications</label>
                                                        <textarea class="form-control summernote" name="Specifications" id="Specifications" style="height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Tags *</label>
                                                        <select name="TagIDs[]" class="selectpicker select2" data-style="select-with-transition" id="TagID" required multiple>
                                                            <option value=""><?php echo lang('tags');?></option>
                                                            <?php foreach ($tags as $key => $value) { ?>
                                                                <option value="<?php echo $value->TagID; ?>"><?php echo $value->Title; ?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="Keywords"><?php echo lang('keywords'); ?></label>
                                                        <input type="text" name="Keywords" required  class="form-control" id="Keywords">
                                                    </div>
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="MetaTages"><?php echo lang('meta_tages'); ?></label>
                                                        <input type="text" name="MetaTags" required  class="form-control" id="MetaTages">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="MetaKeywords"><?php echo lang('meta_keywords'); ?></label>
                                                        <input type="text" name="MetaKeywords" required  class="form-control" id="MetaKeywords">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="MetaDescription"><?php echo lang('meta_description'); ?></label>
                                                        <textarea class="form-control" name="MetaDescription" id="MetaDescription" style="height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="RibbonSku">Length
                                                        <!--   <small>(No. of pieces Ribbon can have in it)</small> -->
                                                        </label>
                                                        <input type="text" name="dimension_length" required class="form-control"
                                                            id="dimension_length">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="dimension_width">Width</label>
                                                        <input type="text" name="dimension_width" required class="form-control" id="dimension_width">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="dimension_height">Height</label>
                                                        <input type="text" name="dimension_height" required class="form-control" id="dimension_height">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="dimension_weight">Weight</label>
                                                        <input type="text" name="dimension_weight" required class="form-control" id="dimension_weight">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_length">Inner Length</label>
                                                        <input type="text" name="inner_dimension_length" required class="form-control" id="inner_dimension_length">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">  
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="inner_dimension_weight">Inner Weight</label>
                                                        <input type="text" name="inner_dimension_weight" required class="form-control" id="inner_dimension_weight">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label" for="description">Description</label>
                                                        <textarea id="description" rows="5" class="form-control" name="description" title="Add Description"></textarea>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <img id="uploadedImage" src="" style="display:none;width:100px;" >
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label for="Image">Shape Custom Image, it should be 250 × 250 px</label>
                                                        <input type="file" id="Image" name="Image"  title="Select Shape Image, it should be 250 × 250 px">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <!-- <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <img src="" id="uploadedImage_content_1" style="display:none;width:100px;">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label for="shape_content_image_1">Shape Name Image, it should be 250 × 250 px</label>
                                                        <input type="file" id="shape_content_image_1" name="shape_content_image_1"  title="Select Shape Name Image, it should be 250 × 250 px">
                                                    </div>
                                                </div> 
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <img src="" id="uploadedImage_content_2" style="display:none;width:100px;">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label for="shape_content_image_2">Shape Name & Logo Image, it should be 250 × 250 px</label>
                                                        <input type="file" id="shape_content_image_2" name="shape_content_image_2"  title="Select Shape Name & Logo Image, it should be 250 × 250 px">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <img src="" id="uploadedImage_content_3" style="display:none;width:100px;">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label for="shape_content_image_3">Shape Logo Image, it should be 250 × 250 px</label>
                                                        <input type="file" id="shape_content_image_3" name="shape_content_image_3"  title="Select Shape Logo Image, it should be 250 × 250 px">
                                                    </div>
                                                </div>
                                            </div>
                                            <br> -->
                                            <div class="row">
                                                <div class="gallery"></div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group label-floating">
                                                        <label for="ShapeImage">Shape Images, it should be 800 × 800 px</label>
                                                        <input type="file" id="ShapeImage" name="ShapeImage[]" multiple="multiple" title="Select Shape Image, it should be 800 × 800 px">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 checkRibbon-radios">
                                                    <div class="form-group label-floating">
                                                        <div class="checkbox">
                                                            <label for="IsActive">
                                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                                    checked/> <?php echo lang('is_active'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $("#price_per_kg, #quantity_per_kg").on("keyup change blur", function(e) {
        var price_per_kg = $('#price_per_kg').val();
        var quantity_per_kg = $('#quantity_per_kg').val();
        console.log(quantity_per_kg);
        if(price_per_kg > 0 && quantity_per_kg > 0 && price_per_kg != '' && quantity_per_kg != '')
        {
            $('#price_per_pcs').val((price_per_kg/quantity_per_kg).toFixed(2));
        }
    });
    document.getElementById('Image').addEventListener('change', function(){
        if (this.files[0] ) {
            var picture = new FileReader();
            picture.readAsDataURL(this.files[0]);
            picture.addEventListener('load', function(event) {
            document.getElementById('uploadedImage').setAttribute('src', event.target.result);
            document.getElementById('uploadedImage').style.display = 'block';
            });
        }
    });

    document.getElementById('shape_content_image_1').addEventListener('change', function(){
        if (this.files[0] ) {
            var picture = new FileReader();
            picture.readAsDataURL(this.files[0]);
            picture.addEventListener('load', function(event) {
            document.getElementById('uploadedImage_content_1').setAttribute('src', event.target.result);
            document.getElementById('uploadedImage_content_1').style.display = 'block';
            });
        }
    });

    document.getElementById('shape_content_image_2').addEventListener('change', function(){
        if (this.files[0] ) {
            var picture = new FileReader();
            picture.readAsDataURL(this.files[0]);
            picture.addEventListener('load', function(event) {
            document.getElementById('uploadedImage_content_2').setAttribute('src', event.target.result);
            document.getElementById('uploadedImage_content_2').style.display = 'block';
            });
        }
    });

    document.getElementById('shape_content_image_3').addEventListener('change', function(){
        if (this.files[0] ) {
            var picture = new FileReader();
            picture.readAsDataURL(this.files[0]);
            picture.addEventListener('load', function(event) {
            document.getElementById('uploadedImage_content_3').setAttribute('src', event.target.result);
            document.getElementById('uploadedImage_content_3').style.display = 'block';
            });
        }
    });

    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        

    };

    $('#ShapeImage').on('change', function() {
        var input =this;
        var placeToInsertImagePreview ='div.gallery';
        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="width:100px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    });
});
</script>