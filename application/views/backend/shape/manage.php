<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>
                                    <th>Width</th>
                                    <th>Height</th>
                                    <th>Weight</th>

                                    <th><?php echo lang('is_active');?></th>


                                    <?php if(checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->ShapeID;?>">

                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->dimension_width; ?></td>
                                            <td><?php echo $value->dimension_height; ?></td>
                                            <td><?php echo $value->dimension_weight; ?></td>


                                            <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->ShapeID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                               
                                                <?php if(checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <!-- <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->ShapeID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a> -->
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>