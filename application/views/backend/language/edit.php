<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="SystemLanguageID" value="<?php echo $result->SystemLanguageID;?>">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="SystemLanguageTitle"><?php echo lang('title'); ?><span class="text-danger">*</span></label>
                                        <input type="text" name="SystemLanguageTitle" parsley-trigger="change" required  class="form-control" id="SystemLanguageTitle" value="<?php echo $result->SystemLanguageTitle;?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ShortCode"><?php echo lang('short_code'); ?><span class="text-danger">*</span></label>
                                        <input type="text" parsley-trigger="change" required  class="form-control" id="ShortCode" value="<?php echo $result->ShortCode;?>" disabled>
                                    </div>
                                </div>

                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsDefault">
                                                <input name="IsDefault" value="1" type="checkbox" id="IsDefault" <?php echo ($result->IsDefault == 1 ? 'checked' : ''); ?>/> <?php echo lang('is_default'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" <?php echo ($result->IsActive == 1 ? 'checked' : ''); ?>/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>