<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $product_dropdown = '';
        $groups = '<option value="0" ' . ((isset($result[$key]->IsForAll) && $result[$key]->IsForAll == 1) ? 'selected' : '') . '>'.lang('ForAll').'</option>';
        if ($key == 0) {
           
            $collection_products = explode(',', $result[$key]->ProductID);
            $product_dropdown .= '<option value="0"> Select / Unselect All </option>';
            foreach ($products as $product) {
                $product_dropdown .= '<option ' . ((in_array($product->ProductID, $collection_products)) ? 'selected' : '') . ' value="' . $product->ProductID . '">' . $product->Title . '</option>';
            }

            foreach ($c_groups  as $value) { 

                     $groups .= '<option  ' . ((in_array($value->CustomerGroupID, $customer_groups)) ? 'selected="selected"' : '') . ' value="'.$value->CustomerGroupID.'">'.$value->CustomerGroupTitle.'</option>';
                             }

           
            $common_fields2 = '<div class="col-sm-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                ';



            $common_fields3 = '<div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="Discount">'.lang('Discount').'</label>
                                    <input type="text" name="Discount" class="form-control number-with-decimals"
                                           id="Discount" value="'.$result[$key]->Discount.'">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Choose '.lang('DiscountType').'</label>
                                        <select name="DiscountType" class="form-control" id="DiscountType" required>
                                            
                                                <option value="percentage" ' . ((isset($result[$key]->DiscountType) && $result[$key]->DiscountType == 'percentage') ? 'selected' : '') . '>'.lang('Percentage').'</option>
                                                <option value="per item" ' . ((isset($result[$key]->DiscountType) && $result[$key]->DiscountType == 'per item') ? 'selected' : '') . '>'.lang('PerItem').'</option>
                                           
                                        </select>
                                    </div>
                                </div>
                        </div><div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="FromDate">'.lang('FromDate').'</label>
                                    <input type="text" name="ValidFrom"
                                           class="form-control custom_datepicker" id="FromDate" value="'.$result[$key]->ValidFrom.'">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="ToDate">'.lang('ToDate').'</label>
                                    <input type="text" name="ValidTo" class="form-control custom_datepicker"
                                           id="ToDate" value="'.$result[$key]->ValidTo.'">
                                </div>
                            </div>
                        </div>';
            
            
            
            $common_fields5 = '<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Choose ' . lang('products') . '</label>
                                        <select name="ProductID[]" class="form-control selectpicker" id="ProductID" required multiple data-size="8" data-live-search="true" >
                                           
                                            ' . $product_dropdown . '
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Choose ' . lang('customer_groups') . '</label>
                                        <select name="CustomerGroupID[]" class="form-control selectpicker" id="CustomerGroupID" required multiple data-size="8" >
                                            '.$groups.'
                                            
                                        </select>
                                    </div>
                                </div>
                                
                            </div>';
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        ' . $common_fields2 . '
                                                        
                                                         
                                                    </div>
                                                    '.$common_fields3.'


                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">' . lang('description') . '</label>
                                                                <textarea class="form-control" name="Description" id="Description" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                        


                                                    
                                                    ' . $common_fields5 . '
                                                   
                                                    

                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
    var c = 0; 
    var j = 0; 
    $('#ProductID').selectpicker().trigger('change');
    $('#ProductID').selectpicker().change(function(){
        if(c != 0){
            toggleSelectAll($(this));
        }else{
            c++;
        }

    }).trigger('change');
    $('#CustomerGroupID').selectpicker().trigger('change');
    $('#CustomerGroupID').selectpicker().change(function(){
        if(j != 0){
            toggleSelectAll($(this));
        }else{
            j++;
        }

    }).trigger('change');
});
</script>