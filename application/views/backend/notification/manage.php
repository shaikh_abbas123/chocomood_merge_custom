<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Send Notification</h4>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/sendNotification"
                              method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="NotificationType">Notification Type</label>
                                        <select id="NotificationType" class="selectpicker" data-style="select-with-transition"
                                                name="NotificationType">
                                            <option value="Email">Email</option>
                                            <option value="SMS">SMS</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="UsersType">Send to</label>
                                        <select id="UsersType" class="selectpicker" data-style="select-with-transition"
                                                name="UsersType">
                                            <option value="Admins">Store Admin Users</option>
                                            <option value="Stores">Store Warehouse Users</option>
                                            <option value="Drivers">Driver Users</option>
                                            <option value="Customers">Customers</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required class="form-control" id="Title">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Message">Message</label>
                                        <textarea class="form-control summernote textarea" name="Message" id="Message"
                                                  style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    Send
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        Cancel
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>