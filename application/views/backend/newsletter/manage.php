<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Newsletter Subscribers <small>(These emails are directly being fetched from mailchimp)</small></h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Email</th>
                                    <?php /*if(checkUserRightAccess(79,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(79,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?><!--
                                            <th><?php /*echo lang('actions');*/?></th>
                                     --><?php /*} */?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo @$value->id;?>">
                                            <td style="padding: 15px !important;font-weight: bold;"><a style="color: black !important;" href="mailto:<?php echo $value->email_address; ?>"><?php echo $value->email_address; ?></a></td>
                                             <?php /*if(checkUserRightAccess(79,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(79,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?><!--
                                            <td>
                                                <?php /*if(checkUserRightAccess(79,$this->session->userdata['admin']['UserID'],'CanEdit')){*/?>
                                                    <a href="<?php /*echo base_url('cms/'.$ControllerName.'/edit/'.$value->NewsletterID);*/?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php /*} */?>
                                               
                                                <?php /*if(checkUserRightAccess(79,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $value->NewsletterID;*/?>','cms/<?php /*echo $ControllerName; */?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php /*} */?>
                                            </td>
                                            --><?php /*} */?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                                </tbody>
                               <!--  <tfoot>
                               
                                    <tr>
                                        <td>Total 515 entries</td>
                                    </tr>
                                </tfoot> -->
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
