<?php
$show_cancel_button = false;
$show_receipt_button = false;
$status = $order[0]->Status;
if ($status == 1) {
    $class = "btn btn-sm";
} else if ($status == 2) {
    $class = "btn btn-primary btn-sm";
} else if ($status == 3) {
    $class = "btn btn-warning btn-sm";
} else if ($status == 4) {
    $class = "btn btn-success btn-sm";
} else if ($status == 5) {
    $class = "btn btn-danger btn-sm";
}
?>
<style>
.coloredCssDdEd ul.dropdown-menu.inner li:nth-child(1) a {background: #ff9800;color: #fff;}
.coloredCssDdEd ul.dropdown-menu.inner li:nth-child(2) a {background: brown;color: #fff;}
.coloredCssDdEd ul.dropdown-menu.inner li:nth-child(3) a {background: blue;color: #fff;}
.coloredCssDdEd ul.dropdown-menu.inner li:nth-child(4) a {background: #4caf50;color: #fff;}
.coloredCssDdEd ul.dropdown-menu.inner li:nth-child(5) a {background: #f44336;color: #fff;}
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            Details For Order # <b><?php echo $order[0]->OrderNumber; ?></b>
                            &nbsp;&nbsp;<button class="<?php echo @$class; ?>">
                                <?php echo $order[0]->OrderStatusEn; ?>
                                <div class="ripple-container"></div>
                            </button>
                            <div class="pull-right">
                                <?php
                                if ($order[0]->HasTicket == 1) { ?>
                                    <a href="<?php echo base_url('cms/ticket/view') . '/' . $order[0]->TicketID; ?>">
                                        <button class="btn btn-primary btn-sm">
                                            View Ticket Details
                                            <div class="ripple-container"></div>
                                        </button>
                                    </a>
                                <?php }
                                ?>
                                <?php
                                if ($show_cancel_button) { ?>
                                    <button class="btn btn-danger btn-sm"
                                            onclick="cancelOrder(<?php echo $order[0]->OrderID; ?>, <?php echo $order[0]->UserID; ?>);">
                                        Cancel Order
                                        <div class="ripple-container"></div>
                                    </button>
                                <?php }
                                ?>
                                <?php
                                if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 4) { 
                                    if($order[0]->CollectFromStore == 0 && $order[0]->SemsaShippingAmount < 1){
                                    ?>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal" data-order-id="<?php echo $order[0]->OrderID; ?>" class="assign_order">
                                            <button <?= (@$order[0]->Status == 4)? 'disabled' : ''; ?> class="btn btn-warning btn-sm">
                                                Assign Order
                                                <div class="ripple-container"></div>
                                            </button>
                                        </a>
                                <?php } }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Customer Name</label>
                                        <h5>
                                            <a href="<?php echo base_url('cms/customer/edit') . '/' . $order[0]->UserID; ?>"
                                               target="_blank"><?php echo $order[0]->FullName; ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Mobile No.</label>
                                        <h5><?php echo($order[0]->Mobile != '' ? '<a href="tel:' . $order[0]->Mobile . '">' . $order[0]->Mobile . '</a>' : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Email</label>
                                        <h5><?php echo($order[0]->Email != '' ? '<a href="mailto:' . $order[0]->Email . '">' . $order[0]->Email . '</a>' : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">User City</label>
                                        <h5><?php echo($order[0]->UserCity != '' ? ucfirst($order[0]->UserCity) : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Branch</label>
                                        <h5><?php echo($order[0]->StoreTitle != '' ? ucfirst($order[0]->StoreTitle) : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Delivery District</label>
                                        <h5><?php echo($order[0]->DistrictTitle != '' ? ucfirst($order[0]->DistrictTitle) : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Coupon Code Used</label>
                                        <h5><?php echo($order[0]->CouponCodeUsed != '' ? $order[0]->CouponCodeUsed : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Coupon Discount %</label>
                                        <h5><?php echo($order[0]->CouponCodeDiscountPercentage > 0 ? $order[0]->CouponCodeDiscountPercentage : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?php if ($order[0]->isCustomize == 0){?>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Discount Availed</label>
                                        <h5><?php echo($order[0]->DiscountAvailed > 0 ? $order[0]->DiscountAvailed . ' SAR' : 'N/A') ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($order[0]->CollectFromStore == 0){?>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Total Shipping Charges</label>
                                        <h5><?php echo ($order[0]->TotalShippingCharges < 1)? freeShippingTitle() : number_format($order[0]->TotalShippingCharges, 2).'  SAR'; ?></h5>
                                    </div>
                                </div>
                            </div>
                             <?php } ?>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Total Tax Amount</label>
                                        <h5><?php echo number_format($order[0]->TotalTaxAmount, 2); ?> SAR</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Total Amount</label>
                                        <h5><?php echo number_format($order[0]->TotalAmount, 2); ?> SAR</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Method</label>
                                        <h5>
                                        <?php
                                            if (isset($order[0]->CollectFromStore) && $order[0]->CollectFromStore == 1) { 
                                                 echo "Payment on Store";
                                            }else{
                                                 echo $order[0]->PaymentMethod;
                                            } ?>
                                        <?php ?>
                                                
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Shipment Method</label>
                                        <h5><?php 
                                            $is_semsa =  0;
                                            $is_collect_from_store = 0;
                                            if($order[0]->SemsaShippingAmount && $order[0]->SemsaShippingAmount > 0){
                                                echo 'SMSA';
                                                $is_semsa = 1;
                                            }else{
                                                if(isset($order[0]->CollectFromStore) && $order[0]->CollectFromStore > 0){
                                                    //echo "Pickup for Store";
                                                    echo "Collect From Store";
                                                    $is_collect_from_store =1;
                                                }else{
                                                    echo $order[0]->ShipmentMethodTitle;
                                                }
                                            }
                                         ?></h5>
                                         <input type="hidden" id="is_semsa" value="<?= @$is_semsa ?>">
                                         <input type="hidden" id="is_collect_from_store" value="<?= @$is_collect_from_store ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Order Placed At</label>
                                        <h5><?php echo date('d-m-Y h:i:s A', strtotime($order[0]->CreatedAt)); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?php if(@$is_semsa != 1 && $order[0]->CollectFromStore == 0){ ?>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Estimated Delivery</label>
                                        <?php
                                            $site_setting = site_settings();
                                            $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                        ?>
                                        <h5><?php echo date('d-m-Y', strtotime($days_to_deliver, strtotime($order[0]->CreatedAt))); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <?php
                            if (isset($order[0]->CollectFromStore) && $order[0]->CollectFromStore == 1) { ?>
                                <h4>
                                
                                </h4>
                            <?php } else { ?>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Delivery Address</label>
                                            <h5>
                                                <span>Recipient Name: <?php echo $order[0]->RecipientName; ?></span><br>
                                                <span>Recipient Mobile No: <?php echo $order[0]->MobileNo; ?></span><br>
                                                <span>Email: <?php echo $order[0]->Email; ?></span><br>
                                                <span>City: <?php echo $order[0]->AddressCity; ?></span><br>
                                                <span>District: <?php echo $order[0]->AddressDistrict; ?></span><br>
                                                <span>Building No: <?php echo $order[0]->BuildingNo; ?></span><br>
                                                <span>Street: <?php echo $order[0]->Street; ?></span><br>
                                                <span>P.O Box: <?php echo $order[0]->POBox; ?></span><br>
                                                <span>Zip Code: <?php echo $order[0]->ZipCode; ?></span><br>
                                                <span>See location on map: <a
                                                            href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $order[0]->Latitude; ?>,<?php echo $order[0]->Longitude; ?>"
                                                            target="_blank">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHbVVHxTASKeTMj0H-JFtHN138p-i6Rx-UdC0VQ2l17yJcaRFVCQ"
                                                 height="25" width="25"
                                                 style="height: 40px !important;width: 40px !important;">
                                        </a></span>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-md-6 col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Payment Collection Address</label>
                                            <?php
                                            if ($order[0]->AddressID == $order[0]->AddressIDForPaymentCollection) { ?>
                                                <h4>Same as shipping address</h4>
                                            <?php } else { ?>
                                                <h5>
                                                    <span>Recipient Name: <?php echo $payment_address[0]->RecipientName; ?></span><br>
                                                    <span>Recipient Mobile No: <?php echo $payment_address[0]->MobileNo; ?></span><br>
                                                    <span>Email: <?php echo $payment_address[0]->Email; ?></span><br>
                                                    <span>City: <?php echo $payment_address[0]->CityTitle; ?></span><br>
                                                    <span>District: <?php echo $payment_address[0]->DistrictTitle; ?></span><br>
                                                    <span>Building No: <?php echo $payment_address[0]->BuildingNo; ?></span><br>
                                                    <span>Street: <?php echo $payment_address[0]->Street; ?></span><br>
                                                    <span>P.O Box: <?php echo $payment_address[0]->POBox; ?></span><br>
                                                    <span>Zip Code: <?php echo $payment_address[0]->ZipCode; ?></span><br>
                                                    <span>See location on map: <a
                                                                href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $payment_address[0]->Latitude; ?>,<?php echo $payment_address[0]->Longitude; ?>"
                                                                target="_blank">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHbVVHxTASKeTMj0H-JFtHN138p-i6Rx-UdC0VQ2l17yJcaRFVCQ"
                                                 height="25" width="25"
                                                 style="height: 40px !important;width: 40px !important;">
                                        </a></span>
                                                </h5>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>-->
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
               
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Extra Charges</h5>
                    </div>
                    <div class="card-content">
                        <div class="material-datatables">
                            <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Factor</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($order_extra_charges) {
                                    foreach ($order_extra_charges as $extra_charge) { ?>
                                        <tr>
                                            <td><?php echo $extra_charge->Title; ?></td>
                                            <td><?php echo $extra_charge->Factor; ?></td>
                                            <td><?php echo number_format($extra_charge->Amount, 2); ?> SAR</td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Assigned To</h5>
                    </div>
                    <div class="card-content">
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label" for="AssignedDriverName">Assigned Driver
                                            Name</label>
                                        <?php echo($order[0]->AssignedDriverName != '' ? ucfirst($order[0]->AssignedDriverName) : 'N/A'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label" for="AssignedDriverEmail">Assigned Driver
                                            Email</label>
                                        <?php echo($order[0]->AssignedDriverEmail != '' ? '<a href="mailto:' . $order[0]->AssignedDriverEmail . '">' . $order[0]->AssignedDriverEmail . '</a>' : 'N/A'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label" for="AssignedDriverMobile">Assigned Driver
                                            Mobile</label>
                                        <?php echo($order[0]->AssignedDriverMobile != '' ? '<a href="tel:' . $order[0]->AssignedDriverMobile . '">' . $order[0]->AssignedDriverMobile . '</a>' : 'N/A'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Change status</h5>
                    </div>
                    <div class="card-content">
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label" for="Status">Status</label>
                                        <select class="selectpicker coloredCssDdEd" <?= (@$order[0]->Status == 4)? 'disabled' : ''; ?> id="Status" data-style="select-with-transition">
                                            <?php foreach ($order_statuses as $order_status) { 
                                                    if($this->session->userdata['admin']['RoleID'] == 3 &&$order_status->OrderStatusID == 5){
                                                    }else{
                                                ?>
                                                <option value="<?php echo $order_status->OrderStatusID; ?>" <?php echo($order_status->OrderStatusID == $order[0]->Status ? 'selected disabled' : ''); ?>><?php echo $order_status->OrderStatusEn; ?></option>
                                            <?php    } 
                                                  }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="OrderID" value="<?php echo $order[0]->OrderID; ?>">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <button class="btn btn-primary waves-effect waves-light pull-right"
                                            type="button"
                                            <?= (@$order[0]->Status == 4)? 'disabled' : ''; ?>
                                            onclick="changeOrderStatus();">
                                        <?php echo lang('submit'); ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Ordered Items</h5>
                    </div>
                    <div class="card-content">
                        <div class="material-datatables">
                            <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>Product Image</th>
                                    <th>Product Title</th>
                                    <th>SKU</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($order_items) {
                                    foreach ($order_items as $order_item) { ?>
                                        <tr>
                                            <td>
                                        <?php
                                        if ($order_item->ItemType == 'Product') { ?>
                                                <a href="<?php echo base_url() . '/' . get_images($order_item->ProductID, 'product', false); ?>" target="_blank">
                                                    <img src="<?php echo base_url() . '/' . get_images($order_item->ProductID, 'product', false); ?>" style="width: 70px;height: 70px;">
                                                </a>
                                        <?php } elseif ($order_item->ItemType == 'Choco Shape') { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "cms/Orders/getChocoboxDetail"
                                                       data-id="<?php echo $order[0]->OrderID; ?>"
                                                       data-box_id="<?php echo $order_item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $order_item->ItemType; ?>"
                                                       data-ribbon="<?php echo $order_item->Ribbon; ?>">
                                                    <img style="width: 70px;height: 70px;" src="<?php echo front_assets("images/" . $order_item->ItemType . ".png"); ?>">
                                                    
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "cms/Orders/getChocoboxDetail"
                                                       data-id="<?php echo $order[0]->OrderID; ?>"
                                                       data-box_id="<?php echo $order_item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $order_item->ItemType; ?>"
                                                       data-ribbon="<?php echo $order_item->Ribbon; ?>">
                                                        <img style="width: 70px;height: 70px;" src="<?php echo front_assets("images/" . $order_item->ItemType . ".png"); ?>">
                                                        
                                                        
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($order_item->ItemType == 'Product') { ?>
                                                    <a href="<?php echo base_url('cms/product/edit') . '/' . $order_item->ProductID; ?>"
                                                       target="_blank"><?php echo $order_item->Title; ?></a>
                                                <?php } elseif ($order_item->ItemType == 'Choco Shape') { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "cms/Orders/getChocoboxDetail"
                                                       data-id="<?php echo $order[0]->OrderID; ?>"
                                                       data-box_id="<?php echo $order_item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $order_item->ItemType; ?>"
                                                       data-ribbon="<?php echo $order_item->Ribbon; ?>">
                                                    
                                                    <h5>Choco Shape</h5>
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "cms/Orders/getChocoboxDetail"
                                                       data-id="<?php echo $order[0]->OrderID; ?>"
                                                       data-box_id="<?php echo $order_item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $order_item->ItemType; ?>"
                                                       data-ribbon="<?php echo $order_item->Ribbon; ?>">
                                                        
                                                        <p><?php echo $order_item->ItemType; ?>
                                                        <?php if($order_item->ItemType == 'Choco Box') { ?>
                                                        (<?= (@$order_item->Ribbon != 1)?'Printable':'Unprintable'?>)
                                                        </p>
                                                        <?php }?>
                                                    </a>
                                                <?php }
                                                ?>
                                            </td>
                                            <td><?php echo $order_item->SKU; ?></td>
                                            <td>
                                                <?php
                                                     $IsOnOffer = false;
                                                     $Price = $order_item->Amount;
                                                     $DiscountType = $order_item->DiscountType;
                                                     $DiscountFactor = $order_item->Discount;
                                                     if ($DiscountType == 'percentage') {
                                                         $IsOnOffer = true;
                                                         $Discount = ($DiscountFactor / 100) * $Price;
                                                         if ($Discount > $Price) {
                                                             $ProductDiscountedPrice = 0;
                                                         } else {
                                                             $ProductDiscountedPrice = $Price - $Discount;
                                                         }
                                                     } elseif ($DiscountType == 'per item') {
                                                         $IsOnOffer = true;
                                                         $Discount = $DiscountFactor;
                                                         if ($Discount > $Price) {
                                                             $ProductDiscountedPrice = 0;
                                                         } else {
                                                             $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                         }
                                                     } else {
                                                         $Discount = 0;
                                                         if ($Discount > $Price) {
                                                             $ProductDiscountedPrice = 0;
                                                         } else {
                                                             $ProductDiscountedPrice = $Price;
                                                         }
                                                     }
                                                     if ($order_item->PriceType == 'kg') {
                                                         $product_packages = get_product_packages($order_item->ProductID, $language);
                                                        if($product_packages){
                                                         foreach (@$product_packages as $k => $v) {
                                                             if ($order_item->package_id == $v['PackagesProductID']) {
                                                                 echo $v['Title'].' x '. $order_item->Quantity;
                                                             }
                                                         }
                                                        }else{
                                                            echo $item->Quantity;
                                                        }
                                                     }
                                                     else
                                                     {
                                                         if($order_item->ProductID == 0)
                                                         {
                                                            if($order_item->ItemType == 'Choco Shape') 
                                                            {
                                                                echo $order_item->Quantity.' Shape';
                                                            }
                                                            else
                                                            {
                                                                echo $order_item->Quantity.' Box';
                                                            }
                                                            
                                                         }else
                                                         {
                                                            echo $order_item->Quantity.($order_item->IsCorporateItem ? ' kg' : 'pcs');
                                                         }
                                                        
                                                     }
                                                ?>
                                               <!--  <?php echo $order_item->Quantity; ?><?php echo ($order_item->IsCorporateItem ? ' kg' : 'pcs'); ?> -->
                                                    
                                            </td>
                                            <td>
                                                    <?php 
                                                        if($IsOnOffer)
                                                        {
                                                            ?>
                                                                <span style="text-decoration: line-through;"><?php echo number_format($order_item->Amount, 2); ?></span> <?= number_format($ProductDiscountedPrice, 2) ?> SAR
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <?php echo number_format($order_item->Amount, 2); ?> SAR
                                                            <?php
                                                        }   
                                                    ?>
                                            </td>
                                            <td>
                                            <?php 
                                                        if($IsOnOffer)
                                                        {
                                                            ?>
                                                                <?= number_format($ProductDiscountedPrice*$order_item->Quantity, 2) ?> SAR
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <?php echo number_format($order_item->Amount*$order_item->Quantity, 2); ?> SAR
                                                            <?php
                                                        }   
                                                    ?>
                                                SAR
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Offer Detail Message Modal -->
<div id="ChocoboxDetailModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="ChocoboxDetailModalDescription">
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" id="response_data">
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $('.assign_order').on('click', function () {
            var OrderID = $(this).attr('data-order-id');
            $('#response_data').html('');
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/get_drivers',
                data: {
                    'OrderID': OrderID
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {
                    $('#response_data').html(result.html);
                    $('#exampleModal').modal('show');
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        });
    });
</script>
<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });
    var channel = pusher.subscribe('Chocomood_Order_Channel');
    channel.bind('Chocomood_Order_Event', function (data) {
        console.log(JSON.stringify(data));
        playAudio();
        setTimeout(function () {
            window.location.reload();
        }, 2000);
    });
    function playAudio() {
        var audiotag = document.getElementById('audiotag');
        audiotag.play();
    }
</script>
<script>
    $(document).on('click', '.chocobox_detail', function () {
        var url = $(this).data('url');
        var tempOrderID = $(this).data('id');
        var pids = $(this).data('pids');
        var box_type = $(this).data('box_type');
        var box_id = $(this).data('box_id');
        var ribbon = $(this).data('ribbon');
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + url,
            data: {'id':tempOrderID,'ProductIDs': pids, 'box_type': box_type, 'box_id': box_id, 'ribbon': ribbon},
            success: function (result) {
                hideCustomLoader();
                $('#ChocoboxDetailModalDescription').html(result);
                $('#ChocoboxDetailModal').modal('show');
            }
        });
    });
</script>