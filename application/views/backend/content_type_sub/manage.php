<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.''); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/'.$add);?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Content <?php echo lang('title');?></th>
                                    <th>Category <?php echo lang('title');?></th>
                                    <th> Image</th>
                                    <th><?php echo lang('is_active');?></th>


                                    <?php if(checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanEdit') ){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value['ContentTypeSubID'];?>">

                                        <td><?php echo $value['content_type_title']; ?></td>
                                            <td><?php echo $value['Title']; ?></td>
                                            
                                            <td>
                                            <img src="<?php echo base_url(get_images($value['ContentTypeSubID'] , 'content_type_sub_image', false)); ?>"
                                                         style="height:70px;width:70px;">
                                            </td>

                                            <td><?php echo ($value['IsActive'] ? lang('yes') : lang('no')); ?></td>

                                            <td>
                                                <?php if(checkUserRightAccess(86,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value['ContentTypeSubID']);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            </td>
                                            
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>