<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add') . ' ' .$for.' '. lang($ControllerName); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                            <input type="hidden" name="for" value="<?= $for?>">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required class="form-control" id="Title">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Price"><?php echo lang('price'); ?></label>
                                        <input type="text" name="Price" required class="form-control" id="Price">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="quantity">Content Type</label>
                                        <select name="ContentTypeID" class="form-control" id="ContentTypeID" >
                                            <?php foreach ($content_type as $v) { ?>
                                            <?php
                                                    if($for == 'Choco Box' && $v->ContentTypeID == 3)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $v->ContentTypeID; ?>"><?php echo $v->Title; ?></option>
                                                        <?php
                                                    }
                                                    if($for == 'Choco Shape')
                                                    {
                                                        ?>
                                                        <option value="<?php echo $v->ContentTypeID; ?>"><?php echo $v->Title; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                

                                <div class="col-md-6 checkRibbon-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <input type="file" name="Image[]" accept="image/png"  multiple="multiple">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                            
                            


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?= ($for == 'Choco Shape')? base_url().'cms/'.$ControllerName.'/choco_shape_content_sub': base_url().'cms/'.$ControllerName?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>