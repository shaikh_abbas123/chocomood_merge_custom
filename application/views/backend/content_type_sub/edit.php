<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        if ($key == 0) {

            $common_fields2 = '
                                
                                
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Price">'.lang('price').'</label>
                                        <input type="text" name="Price" required class="form-control" value="'.$result[$key]->Price.'" id="Price">
                                    </div>
                                </div>
                                <div class="col-sm-4 checkRibbon-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="quantity">Content Type</label>
                                        <select name="ContentTypeID" class="form-control" id="ContentTypeID" >';
                                                            foreach ($content_type as $v) { 
                                                                $common_fields2 .= '<option ';
                                                                $common_fields2.= ($v->ContentTypeID == $result[$key]->ContentTypeID )?'selected ':' ';
                                                                $common_fields2.=  ' value="'.$v->ContentTypeID.'">'. $v->Title.'</option>';
                                                            }
                                                            $common_fields2 .='</select>
                                    </div>
                                </div>
                                ';

            $common_fields = '<div class="row">';
            $images = get_images($result[$key]->ContentTypeSubID, 'content_type_sub_image');
            // print_rm($product_images);
            if ($images) {
                foreach ($images as $product_image) {
                    $common_fields .= '
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <i class="fa fa-trash delete_image" data-image-id="' . $product_image->SiteImageID . '" aria-hidden="true"></i>
                        <img src="' . base_url() . $product_image->ImageName . '" style="height:200px;width:200px;">
                    </div>';
                }
            }
            $common_fields .= '<div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="form-group">

                        <label>Image</label>
                        <input type="file" name="Image[]" accept="image/png" multiple="multiple">
                        <p class="content_type_sub_image_instruction">' . lang('cannot_upload_more_then') . '</p>
                    </div>
            </div>';
            $common_fields .= '</div>';
            
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';
        $back_url = base_url() . 'cms/' . $ControllerName;
        if($result[0]->ContentTypeSubFor == 'Choco Shape')
        {
            $back_url = base_url() . 'cms/' . $ControllerName.'/choco_shape_content_sub';
        }
        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                                <input type="hidden" name="Title_old" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        
                                                         ' . $common_fields2 . '
                                                    </div>
                                                    ' . $common_fields . '
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' .$back_url.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
    $(document).ready(function() {
        $(".delete_image").on('click', function() {
            var id = $(this).attr('data-image-id');

            url = "cms/Content_type_sub/deleteImage2";
            reload = "<?php echo base_url(); ?>cms/<?= $ControllerName?>/edit/<?php echo $result[0]->ContentTypeSubID; ?>";
            deleteRecord(id, url, reload);

        });
    });
</script>