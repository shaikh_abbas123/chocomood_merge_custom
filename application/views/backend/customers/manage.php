<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName); ?></h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="active nav-pills-warning">
                                    <a href="#AllCustomers" data-toggle="tab" aria-expanded="true">All Customers</a>
                                </li>
                                <li>
                                    <a href="#Online" data-toggle="tab" aria-expanded="true">Now Online</a>
                                </li>
                                <!--<li>
                                    <a href="#Website" data-toggle="tab" aria-expanded="true">Registered via Website</a>
                                </li>-->
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="AllCustomers">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>City</th>
                                    <?php if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <th><?php echo lang('actions'); ?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($all_customers){
                                    foreach($all_customers as $value){ ?>
                                        <tr id="<?php echo $value->UserID;?>">
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->Email; ?></td>
                                            <td><?php echo $value->Mobile; ?></td>
                                            <td><?php echo $value->CityTitle; ?></td>
                                             <?php if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){ ?>
                                            <td>
                                                <?php if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){ 
                                                        if($value->IsActive != 0){
                                                    ?>
                                                    <a href="javascript:void(0);" onclick="suspendRecord('<?php echo $value->UserID; ?>','cms/account/suspendUser','/cms/<?php echo $ControllerName; ?>/')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Suspend">block</i><div class="ripple-container"></div></a>
                                                <?php }else if($value->IsActive == 0){ ?>
                                                     <a href="javascript:void(0);" onclick="suspendRecord('<?php echo $value->UserID; ?>','cms/account/unsuspendUser','/cms/<?php echo $ControllerName; ?>/')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Unsuspend">remove_circle</i><div class="ripple-container"></div></a>
                                                <?php }
                                                } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                            <div class="material-datatables tab-pane" id="Online">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th>City</th>
                                        <?php /*if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?><!--
                                            <th><?php /*echo lang('actions');*/?></th>
                                     --><?php /*} */?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($online_customers){
                                        foreach($online_customers as $value){ ?>
                                            <tr id="<?php echo $value->UserID;?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->Mobile; ?></td>
                                                <td><?php echo $value->CityTitle; ?></td>
                                                <?php /*if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?><!--
                                            <td>
                                                <?php /*if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $value->UserID;*/?>','cms/<?php /*echo $ControllerName; */?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php /*} */?>
                                            </td>
                                            --><?php /*} */?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <!--<div class="material-datatables tab-pane" id="Website">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th>City</th>
                                        <?php /*if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?><!--
                                            <th><?php /*echo lang('actions');*/?></th>
                                     --><?php /*} */?>

                                   <!-- </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($all_customers){
                                        foreach($all_customers as $value){ ?>
                                            <tr id="<?php echo $value->UserID;?>">
                                                <td><?php echo $value->FullName; ?></td>
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->Mobile; ?></td>
                                                <td><?php echo $value->CityTitle; ?></td>
                                                <?php /*if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?><!--
                                            <td>
                                                <?php /*if(checkUserRightAccess(71,$this->session->userdata['admin']['UserID'],'CanDelete')){*/?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $value->UserID;*/?>','cms/<?php /*echo $ControllerName; */?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php /*} */?>
                                            </td>
                                            --><?php /*} */?>
                                            <!--</tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>-->
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": true
        });
    });
</script>