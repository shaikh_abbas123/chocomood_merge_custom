<?php

/*
 * English language
 */

//General 

$lang['some_thing_went_wrong'] = 'There is something went wrong';
$lang['something_went_wrong'] = 'Something went wrong. Please try again later.';
$lang['save_successfully'] = 'Saved Successfully';
$lang['update_successfully'] = 'Updated Successfully';
$lang['deleted_successfully'] = 'Deleted Successfully';
$lang['suspended_successfully'] = 'Suspended Successfully';
$lang['you_should_update_data_seperately_for_each_language'] = 'You would need to update data separately for each language';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['actions'] = 'Action';
$lang['submit'] = 'Submit';
$lang['back'] = 'Back';
$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['are_you_sure'] = 'Are you sure you want to delete this?';
$lang['view'] = 'View';
$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['is_active'] = 'Is Active';
$lang['create_table'] = 'Create Table';
$lang['create_model'] = 'Create Model';
$lang['create_view'] = 'Create View';
$lang['create_controller'] = 'Create Controller';
$lang['logout'] = 'Logout';
$lang['please_add_role_first'] = 'Please add role first';
$lang['you_dont_have_its_access'] = 'You don\'t have its access';
$lang['SiteName'] = 'Site Name';
$lang['PhoneNumber'] = 'Phone Number';
$lang['Whatsapp'] = 'Whatsapp';
$lang['Skype'] = 'Skype';
$lang['Fax'] = 'Fax';
$lang['SiteLogo'] = 'Site Logo';
$lang['EditSiteSettings'] = 'Edit Site Settings';
$lang['FacebookUrl'] = 'Facebook Url';
$lang['GoogleUrl'] = 'Google Url';
$lang['LinkedInUrl'] = 'LinkedIn Url';
$lang['TwitterUrl'] = 'Twitter Url';
//end General

// Module Section
$lang['choose_parent_module'] = 'Choose Parent Module';
$lang['parent_module'] = 'Parent Module';
$lang['slug'] = 'Slug';
$lang['icon_class'] = 'Icon Class';

$lang['title'] = 'Title';
$lang['add_module'] = 'Add Module';
$lang['modules'] = 'Modules';
$lang['module'] = 'Module';
$lang['module_rights'] = 'Module Rights';

$lang['please_add_module_first'] = 'Please add module first';
$lang['rights'] = 'Rights';


// Roles Section

$lang['select_role'] = 'Select Role';
$lang['add_role'] = 'Add Role';
$lang['roles'] = 'Roles';
$lang['role'] = 'Role';
$lang['please_add_role_first'] = 'Please add role first';
$lang['choose_user_role'] = 'Choose user role';


// user

$lang['please_add_user_first'] = 'Please add user first';

$lang['name'] = 'Name';
$lang['user'] = 'User';
$lang['users'] = 'Users';
$lang['add_user'] = 'Add User';
$lang['select_user'] = 'Select User';

$lang['email'] = 'Email';
$lang['password'] = 'Password';
$lang['min_length'] = '(Min char 8)';
$lang['confirm_password'] = 'Confirm Password';


$lang['add_city'] = 'Add City';
$lang['citys'] = 'Cities';
$lang['city'] = 'City';
$lang['add_district'] = 'Add District';
$lang['districts'] = 'Districts';
$lang['district'] = 'District';
$lang['please_add_distric_first'] = 'Please add district first';
$lang['choose_district'] = 'Choose District';
$lang['add_email_template'] = 'Add Email Template';
$lang['email_templates'] = 'Email Templates';
$lang['email_template'] = 'Email Template';
$lang['Heading'] = 'Heading';
$lang['Description'] = 'Description';
$lang['Image'] = 'Image';
$lang['add_language'] = 'Add Language';
$lang['languages'] = 'Languages';
$lang['language'] = 'Language';
$lang['short_code'] = 'Short Code';

$lang['is_default'] = 'Is Default';
$lang['please_select_default_value_language_first'] = 'Please select another default value language first';
$lang['add_category'] = 'Add Category';
$lang['categorys'] = 'Categories';
$lang['category'] = 'Category';
$lang['add_child_category'] = 'Add Child Category';
$lang['child_catagories_of'] = 'Child Categories Of';
$lang['price'] = 'Price';
$lang['box_category'] = 'Box Category';

$lang['add_package'] = 'Add Package';
$lang['packages'] = 'Packages';
$lang['package'] = 'Package';

$lang['visit_per_year'] = 'Visit Per Year';
$lang['add_page'] = 'Add Page';
$lang['pages'] = 'Pages';
$lang['page'] = 'Page';
$lang['add_booking'] = 'Add Booking';
$lang['bookings'] = 'Bookings';
$lang['booking'] = 'Booking';
$lang['add_booking/OnTheWay'] = 'Add Booking/OnTheWay';
$lang['booking/OnTheWays'] = 'Booking/OnTheWays';
$lang['booking/OnTheWay'] = 'Booking/OnTheWay';
$lang['add_vehicle'] = 'Add Vehicle';
$lang['vehicles'] = 'Vehicles';
$lang['vehicle'] = 'Vehicle';
$lang['add_coupon'] = 'Add Gift Voucher';
$lang['coupons'] = 'Gift Vouchers';
$lang['coupon'] = 'Gift Voucher';
$lang['add_requests'] = 'Add Requests';
$lang['requestss'] = 'Requests';
$lang['requests'] = 'Requests';
$lang['add_store'] = 'Add Store';
$lang['stores'] = 'Stores';
$lang['store'] = 'Store';
$lang['choose_store'] = 'Select Store';
$lang['add_product'] = 'Add Product';
$lang['products'] = 'Products';
$lang['product'] = 'Product';
$lang['is_featured'] = 'Is Featured';
$lang['(out_of_stock)'] = 'Out Of Stock';
$lang['choose_category'] = 'Choose Category';
$lang['choose_sub_category'] = 'Choose Sub Category';
$lang['description'] = 'Description';
$lang['ingredients'] = 'Ingredients';
$lang['add_order'] = 'Add Order';
$lang['orders'] = 'Orders';
$lang['order'] = 'Order';
$lang['add_invoice'] = 'Add Invoice';
$lang['invoices'] = 'Invoices';
$lang['invoice'] = 'Invoice';
$lang['add_shipment'] = 'Add Shipment';
$lang['shipments'] = 'Shipments';
$lang['shipment'] = 'Shipment';
$lang['add_ticket'] = 'Add Ticket';
$lang['tickets'] = 'Tickets';
$lang['ticket'] = 'Ticket';
$lang['add_refund'] = 'Add Refund';
$lang['refunds'] = 'Refunds';
$lang['refund'] = 'Refund';
$lang['add_customers'] = 'Add Customers';
$lang['customerss'] = 'Customers';
$lang['customers'] = 'Customers';
$lang['add_report'] = 'Add Report';
$lang['reports'] = 'Reports';
$lang['report'] = 'Report';
$lang['add_collection'] = 'Add Home Grid Image';
$lang['collections'] = 'Home Grid Images';
$lang['collection'] = 'Home Grid Image';
$lang['add_nutrition'] = 'Add Nutrition';
$lang['nutritions'] = 'nutritions';
$lang['select_nutritions'] = 'Select Nutrition';
$lang['nutrition'] = 'Nutrition';
$lang['add_collection_image'] = 'Add Collection_image';
$lang['collection_images'] = 'Collection_images';
$lang['collection_image'] = 'Collection_image';
$lang['tags'] = 'Tags';
$lang['keywords'] = 'Keywords';
$lang['add_taxShippingCharges'] = 'Add TaxShippingCharges';
$lang['taxShippingChargess'] = 'Tax Shipping Charges';
$lang['meta_tages'] = 'Meta Title';
$lang['meta_keywords'] = 'Meta Keywords';
$lang['meta_description'] = 'Meta Description';
$lang['giving_your_review'] = 'Thank you for giving your review on this product.';
$lang['you_have_already_given_review'] = 'You have already reviewed this product.';
$lang['thank_you_for_rating'] = "Thank you for rating this product";
$lang['updated_rating'] = "Your rating is updated for this product";
$lang['you_have_already_rate_this_product'] = "You have already rated this product.";
$lang['thank_you_subscribe_our_news_letter'] = "Thank you for subscribing to our newsletter We will keep you updated with our latest happenings.";
$lang['you_have_enter_invalid_email'] = "You have entered an invalid email or this email is already subscribed to our newsletter.";
$lang['feed_back_submitted_submitted_successfully'] = "Your Feedback has been submitted successfully!";
$lang['only_pdf_doc_file_format'] = "Only PDF and Doc file format allowed. Please upload your CV in proper format.";
$lang['must_use_captcha'] = "You must use captcha to proceed :(";
$lang['please_choose_image'] = "Please choose image from 1 to 6 rang";
$lang['cannot_upload_more_then'] = "You can't upload more then 6 images.First Image will be cover image and make sure aspect ratio of images should be 1:1";
$lang['price_type'] = "Price Type";
$lang['price_type_item'] = "Per Item";
$lang['price_type_kg'] = "Per Kg";
$lang['quantity'] = "Quantity";
$lang['package'] = "Package";
$lang['add_to_basket'] = "Add To Basket";
$lang['buy_now'] = "Buy Now";
$lang['product_catalogue'] = "Product Catalogue";
$lang['home'] = "Home";
$lang['total'] = "Total";
$lang['shopping_basket'] = "Shopping Basket";
$lang['wishlist'] = "Wishlist";
$lang['you_can_not_set_more_than_four'] = "You can't set more than 4 collections to featured.";
$lang['load_more'] = "Load More";
$lang['my_account'] = "My Account";
$lang['my_profile'] = "My Profile";
$lang['my_orders'] = "My Orders";
$lang['about'] = "About";
$lang['customize'] = "Customize";
$lang['products'] = "Products";
$lang['corporate'] = "Corporate";
$lang['store'] = "Store";
$lang['contact_us'] = "Contact Us";
$lang['is_customize'] = 'Is Customized';
$lang['is_corporate'] = 'Is Corporate';
$lang['add_offer'] = 'Add Discount';
$lang['offers'] = 'Discounts';
$lang['offer'] = 'Discount';
$lang['customer_groups'] = 'Customer Groups';
$lang['FromDate'] = "From Date";
$lang['ToDate'] = "To Date";
$lang['add_customerGroup'] = 'Add CustomerGroup';
$lang['customerGroups'] = 'CustomerGroups';
$lang['customerGroup'] = 'CustomerGroup';
$lang['add_searchTag'] = 'Add SearchTag';
$lang['searchTags'] = 'SearchTags';
$lang['searchTag'] = 'SearchTag';
$lang['Discount'] = 'Discount';
$lang['DiscountType'] = 'Discount Type';
$lang['Percentage'] = 'Percentage';
$lang['PerItem'] = 'Per Item';
$lang['ForAll'] = 'For All';
$lang['offers_for_you'] = 'Offers For You';
$lang['registered_successfully_signup'] = 'Registered successfully. Redirecting you to profile section.';
$lang['email_password_incorrect'] = 'Email or password incorrect.';
$lang['loggedin_successfully'] = 'Logged in successfully.';
$lang['provide_verification_code'] = 'Please provide verification code sent in sms to you to verify your provided details.';
$lang['your_account_suspended'] = 'Your account is suspended. Please contact admin for further details & action.';
$lang['logged_out'] = 'Logged out successfully!';
$lang['wrong_old_password'] = 'Wrong old password tried.';
$lang['new_old_password_not_match'] = 'New password and confirm password didn\'t match.';
$lang['profile_updated'] = 'Profile updated successfully.';
$lang['password_reset_successfully'] = 'Your password is reset successfully. A random password is generated and sent to your email address. Please use this password for login then go to your profile and change it.';
$lang['user_not_found_with_email'] = 'No user found with this email.';
$lang['otp_sent'] = 'An OTP is sent to your provided mobile number. Please use that here to proceed.';
$lang['wrong_otp_inserted'] = 'The inserted OTP is incorrect.';
$lang['you_have_no_saved_address'] = "You don\'t have any address saved. Please add an address then proceed to your cart for checkout.";
$lang['address_saved_successfully'] = "Address saved successfully.";
$lang['address_set_as_default'] = "Address is changed as selected.";
$lang['address_set_for_payment'] = "Address set for payment collection successfully.";
$lang['cant_remove_address_order'] = "Can't remove this address as this is used for an order.";
$lang['address_removed'] = "Address removed successfully.";
$lang['added_to_basket'] = "Item Added to Shopping Basket!";
$lang['basket_updated'] = "Basket updated successfully.";
$lang['removed_from_basket'] = "Item removed from basket.";
$lang['removed_from_wishlist'] = "Item removed from your wishlist.";
$lang['added_to_wishlist'] = "Item added to your wishlist.";
$lang['need_to_login_wishlist'] = "You need to be logged in to add this to your wishlist.";
$lang['coupon_applied'] = "Hurray! Gift voucher applied successfully.";
$lang['coupon_expired'] = "Sorry! This Gift voucher has expired.";
$lang['coupon_invalid'] = "Sorry! This Gift voucher code is invalid.";
$lang['coupon_cleared'] = "Gift voucher cleared successfully.";
$lang['shipment_method_changed'] = "Shipment method changed successfully.";
$lang['payment_method_changed'] = "Payment method changed successfully.";
$lang['no_items_in_basket'] = "You have no items in your basket.";
$lang['thank_you_for_order'] = "Thank you for placing your order with us.";
$lang['order_cancelled'] = "Your order is cancelled successfully.";
$lang['out_of_stock'] = '(Out Of Stock)';
$lang['click_to_add_to_wishlist'] = 'Click to add to your wishlist';
$lang['click_to_add_to_cart'] = 'Click to add this to your cart';
$lang['ticket_already_generated'] = "A ticket is already generated against this booking.";
$lang['ticket_raised'] = "A ticket is raised against this booking.";
$lang['ticket_message_sent'] = "Message sent successfully.";
$lang['please_add_items_to_box'] = "Please add some items to your box first.";
$lang['add_box'] = 'Add Box';
$lang['boxs'] = 'Boxs';
$lang['box'] = 'Box';
$lang['email'] = 'Email Address';
$lang['password'] = 'Password';
$lang['full_name'] = 'Full Name';
$lang['mobile_no'] = 'Mobile No.';
$lang['send_otp_sms'] = 'Dear customer please use this code to verify your mobile number.';
$lang['your_cart_is_empty'] = 'Your cart is empty, please add items to cart first';
$lang['upload_allowed_file_type'] = "Please upload only allowed file type";
$lang['upload_shape_image'] = "Please upload a shape image to continue";
$lang['a_box_must_be_selected'] = 'A box must be selected to proceed';
$lang['sar'] = 'SAR';
$lang['box_packaging_detail'] = 'Box & Packaging';
$lang['box_title'] = 'Box Title:';
$lang['box_price'] = 'Box Price:';
$lang['box_capacity'] = 'Box Capacity:';
$lang['pieces'] = 'Pieces';
$lang['enlarge_image'] = 'Enlarge Image';
$lang['select_box_to_continue'] = 'Please select a box from below list to continue';
$lang['click_to_proceed_with_box'] = 'Click to proceed with this box';
$lang['featured'] = 'Featured';
$lang['products'] = 'Products';
$lang['view_all'] = 'View All';
$lang['your_order'] = 'your order';
$lang['newsletter_subscribe'] = 'Newsletter Subscribe';
$lang['email_placeholder'] = 'Enter your email here';
$lang['subscribe'] = 'Subscribe';
$lang['payments_supported_by'] = 'Payments Supported By';
$lang['copyright'] = 'Copyright';
$lang['chocomood'] = 'Chocomood';
$lang['all_rights_reserved'] = 'All rights reserved';
$lang['cookies_usage_alert'] = 'This website uses cookies to ensure you get the best experience on our website.';
$lang['learn_more'] = 'Learn more';
$lang['got_it'] = 'Got it!';
$lang['customize_now'] = 'Customize Now';
$lang['kilogram'] = 'kilogram';
$lang['of_high_quality_chocolate'] = 'of High Quality Chocolate';
$lang['only_shape_formats'] = '(only jpeg, jpg, png allowed)';
$lang['add_new_address'] = 'Add New Address';
$lang['recipient_name'] = 'Recipient Name';
$lang['please_select_city'] = 'Please select city';
$lang['please_select_city_first'] = 'Please select city first';
$lang['building_no'] = 'Building No.';
$lang['street'] = 'Street';
$lang['POBox'] = 'P.O Box';
$lang['zip_code'] = 'Zip Code';
$lang['google_pin_link'] = 'Google Pin Link';
$lang['jeddah_default_pin'] = '(Jeddah will be selected as location if not changed)';
// $lang['cancel'] = 'Cancel';
$lang['select_location_on_map'] = 'Select location on map by dragging marker';
$lang['done'] = 'Done';
$lang['forgot_password'] = 'Forgot Password';
$lang['enter_registered_email_here'] = 'Enter Your Registered Email Address';
$lang['request_password'] = 'Request Password';
$lang['dismiss'] = 'Dismiss';
$lang['continue_shopping'] = 'Continue Shopping';
$lang['You_do_not_have_any_order_yet'] = 'You donot have any order yet.';
$lang['proceed_to_checkout'] = 'Proceed to checkout';
$lang['proceed_to_checkout1'] = 'Proceed to checkout';
$lang['last'] = 'Last'; 
$lang['pcs'] = 'Pcs'; 
$lang['write_otp_here_and_verify'] = 'Write OTP here and verify';
$lang['verify_otp_here'] = 'Verify OTP';
$lang['my_information'] = 'My Information';
$lang['orders'] = 'Orders';
$lang['my_addresses'] = 'My Addresses';
$lang['wishlist_items'] = 'Wishlist Items';
$lang['password_protected'] = 'Password Protected';
$lang['old_password'] = 'Old Password';
$lang['new_password'] = 'New Password (Min Length: 6)';
$lang['confirm_new_password'] = 'Confirm Password (Min Length: 6)';
$lang['save_changes'] = 'Save Changes';
$lang['last_unsuccessful_login'] = 'Last unsuccessful login';
$lang['discard'] = 'Discard';
$lang['boxes'] = 'Boxes';
$lang['ribbons'] = 'Ribbons';
$lang['view_detail'] = 'View Detail';
$lang['login'] = 'Login';
$lang['forgot_password'] = 'Forgot Password?';
$lang['dont_have_account_register'] = 'Don\'t have an account? Register Now';
$lang['register'] = 'Register';
$lang['your_account'] = 'your account';
$lang['register_now'] = 'Register Now';
$lang['i_accept_the'] = 'I accept the';
$lang['terms_and_conditions'] = 'Terms and conditions';
$lang['filter'] = 'Filter';
$lang['count'] = 'Count';
$lang['sort'] = 'Sort';
$lang['bulk'] = 'Bulk';
$lang['announcements'] = 'Announcements';
$lang['feel_free_to'] = 'Feel free to';
$lang['write_us'] = 'Write Us';
$lang['toll_free'] = 'Toll Free';
$lang['subject'] = 'Subject';
$lang['feedback'] = 'Feedback';
$lang['career'] = 'Career';
$lang['company'] = 'Company';
$lang['department'] = 'Department';
$lang['your_message'] = 'Your Message';
$lang['upload_cv'] = 'Upload CV';
$lang['seasons'] = 'Seasons';
$lang['check_availability'] = 'Check availability';
$lang['add_customize']            = 'Add Customize';
$lang['customizes']            = 'Customizes';
$lang['customize']            = 'Personlise';
$lang['add_tag']            = 'Add Tag';
$lang['tags']            = 'Tags';
$lang['tag']            = 'Tag';
$lang['address_type']            = 'Address Type';
$lang['is_default']            = 'Is Default Address';
$lang['map_pin_required']            = 'Google pin is required';
$lang['make_default_address']            = 'Please make other address default first.';
$lang['edit_address']            = 'Edit Address';
$lang['primary']            = 'Primary Address';
$lang['secondary']            = 'Secondary Address';

$lang['pending_orders']            = 'Pending Orders';
$lang['packed_orders']            = 'Packed Orders';
$lang['dispatch_orders']            = 'Dispatched Orders';
$lang['delivered_orders']            = 'Delivered Orders';
$lang['cancelled_orders']            = 'Cancelled Orders';
$lang['send_otp']            = 'Send OTP';
$lang['write_mobile_number']            = 'Write Mobile Number';
$lang['available']            = 'Available';
$lang['not_available']            = 'Not Available';
$lang['rating']            = 'Rating';
$lang['product_per_page']            = 'Product per page';
$lang['Select_filter_to_sort_by']            = 'Select filter to sort by';
$lang['Sort_by']            = 'Sort by';
$lang['Purchased'] = 'Purchased';
$lang['Highest_to_lowest'] = 'Highest to lowest';
$lang['Lowest_to_Highest'] = 'Lowest to Highest';
$lang['Older_to_Newer'] = 'Older to Newer';
$lang['Newer_to_Older'] = 'Newer to Older';
$lang['no_product_available'] = 'No product available';
$lang['whats'] = 'what\'s';
$lang['inside'] = 'Inside';
$lang['resend_otp'] = 'Resend OTP';

$lang['add_faq']            = 'Add Faq';
$lang['faqs']            = 'Faqs';
$lang['faq']            = 'Faq';
$lang['delivery_store_id_selected']            = 'Delivery branch selected';
$lang['payment_page_redirect']            = 'You are redirecting to payment page.Please wait...';
$lang['already_amount_paid']            = 'Already paid';
$lang['privacy_policy'] = 'Privacy Policy';
$lang['return_policy'] = 'Return Policy';
$lang['faq'] = 'FAQ';
$lang['site_map'] = 'Site Map';
$lang['select'] = 'Select';
$lang['address'] = 'Address';
$lang['opening_hour'] = 'Opening Hour';
$lang['promo_discount'] = 'Promo Discount';
$lang['promo_discount_availed'] = 'Promo Discount Availed';
$lang['click_to_add_to_your_wishlist'] = 'Add to your wishlist';
$lang['click_to_add_this_to_your_cart'] = 'Add this to your cart';
$lang['check'] = 'Check';
$lang['availability'] = 'Availability';
$lang['results'] = 'Results';
$lang['specifications'] = 'Specifications';
$lang['nutrition_info'] = 'Nutrition facts';
$lang['kg'] = 'kilo gram';
$lang['Minimum_Qty'] = 'Minimum Qty';
$lang['SAR'] = 'SAR';
$lang['Out_Of_Stock'] = 'Out Of Stock';
$lang['Close'] = 'Close';
$lang['avg_rated_by'] = 'Avg rated by';
$lang['People'] = 'People';
$lang['what'] = 'What\'s';
$lang['InsideInside'] = 'Inside';
$lang['rate_the'] = 'Rate the';
$lang['Your_review'] = 'Your review';
$lang['Customer_Name'] = 'Customer Name';
$lang['Give_Your_Review'] = 'Write Your Review';
$lang['Rate_it'] = 'Rate it';
$lang['Other_Reviews'] = 'Other Reviews';
$lang['No_other_reviews_yet'] = 'No other reviews yet.';
$lang['No_reviews_yet'] = 'No reviews yet.';
$lang['ratings'] = 'Ratings';
$lang['Complete'] = 'Complete';
$lang['Serving_Size'] = 'Serving Size';
$lang['Nutrition_Facts'] = 'Nutrition data';
$lang['This_item_will_not'] = 'This item will not be added to cart as a corporate product now';
$lang['This_item_will_be'] = 'This item will be added to cart as a corporate product now';
$lang['Nutrition'] = 'Nutrition value';
$lang['Order_No'] = 'Order No';
$lang['Transaction_ID'] = 'Transaction ID';
$lang['Est_Delivery'] = 'Est. Delivery';
$lang['Total_Cost'] = 'Total Cost';
$lang['Paid_By'] = 'Paid By';
$lang['Invoice'] = 'Invoice';
$lang['Return_Order'] = 'Return Order';
$lang['Cancel_Order'] = 'Cancel Order';
$lang['Delivery_Address'] = 'Delivery Address';
$lang['Grand_Total'] = 'Grand Total';
$lang['Collect_From_Store'] = 'Collect From Store';
$lang['RecipientName'] = 'Recipient Name';
$lang['BuildingNo'] = 'Building No.';
$lang['StreetName'] = 'Street Name';
$lang['DistrictName'] = 'District Name';
$lang['CityName'] = 'City Name';
$lang['POBox'] = 'P.O.Box';
$lang['ZipCode'] = 'Zip Code';
$lang['MobileNumber'] = 'Mobile Number';
$lang['Complain'] = 'Help';
$lang['ComplainNo'] = 'Request No.';
$lang['ViewMessages'] = 'View Messages';
$lang['if_you_did'] = 'If you didn\'t receive the order, or have a query please write to us';
$lang['Raise_a_Complaint'] = 'Help request';
$lang['Ticket_Status'] = 'Ticket Status';
$lang['Submitted_on'] = 'Submitted on';
$lang['addresses'] = 'Addresses';
$lang['Name'] = 'Name';
$lang['Address'] = 'Address';
$lang['no_address_found'] = 'No addresses found. Please add a new address.';
$lang['Add_New_Address'] = 'Add New Address';
$lang['no_item_in_wishlist'] = 'No items found in your wishlist.';
$lang['removed_from_wishlist'] = 'Remove from wishlist.';
$lang['Procceed_to_Checkout'] = 'Proceed to Checkout';
$lang['Continue_Shopping'] = 'Continue Shopping';
$lang['total_bill'] = 'Total bill';
$lang['select'] = 'Select';
$lang['search_tags'] = 'Search Tags';
$lang['search'] = 'Search';
$lang['grams'] = 'g';
$lang['Manage your Account'] = 'Manage your Account';
$lang['Make is easier to check-out your cart. Always keep your information updated!'] = 'Make is easier to check-out your cart. Always keep your information updated!';
$lang['my_wishlist'] = 'My Wishlist';
$lang['View/Edit your Personal Information'] = 'View/Edit your Personal Information';
$lang['View all orders placed by you'] = 'View all orders placed by you';
$lang['Manage your shipping Addresses'] = 'Manage your shipping Addresses';
$lang['Manage all items you wish to buy later'] = 'Manage all items you wish to buy later';
$lang['Your Feedback has been submitted successfully!'] = 'Your Feedback has been submitted successfully!';
$lang['Out terms and conditions are updated and you should have a look at those'] = 'Out terms and conditions are updated and you should have a look at those';
$lang['View updated terms and conditions'] = 'View updated terms and conditions';

$lang['Enter Email'] = 'Enter Email Address';
$lang['Enter Password'] = 'Enter Password';
$lang['Enter Full Name'] = 'Enter Full Name';
$lang['Add more'] = 'Add More';
$lang['Enter Email Address'] = 'Enter Email Address';
$lang['Limit'] = 'Limit Error In Cart';
$lang['kindly_create_support_ticket_against_your_order_to_process'] = 'Kindly create support ticket against your order to process.';
$lang['unit_price'] = 'Unit Price';
$lang['delivery_policy'] = 'Delivery Policy';


// Manually added
$lang['work'] = 'Work';
$lang['please_select_district'] = 'Please Select District';
$lang['street'] = 'Street';
$lang['please_enter_your_address_below_or_move_the_cursor_to_your_location_from_the_map'] = 'Please enter your address below or move the cursor to your location from the map';
$lang['add_this_message_and_similar_icon_on_the_empty_state_for_this_area'] = 'Add this message and similar icon on the empty state for this area';
$lang['click_and_collect'] = 'Click & Collect';
$lang['order_this_product_now_and_collect_it_from_a_store_of_your_choice'] = 'Order this product now and collect it from a store of your choice';
$lang['standard_shipment'] = 'Standard Shipment';
$lang['fast_delivery'] = 'Fast Delivery';
$lang['delivered_in_business_days'] = ' Delivered in 3-5 Business days';
$lang['Click_here_for_order_details'] = 'Click here for order details';
$lang['Tracking_ID'] = 'Tracking ID';

$lang['free_delivery'] = 'Free Delivery';
$lang['free_delivery_start_from_250_sar'] = 'Free Delivery start from 250 sar';

$lang['return_policy'] = 'Return policy';
$lang['up_to_14_days_from_the_date_of_order'] = 'Up to 14 days from the date of order';
$lang['learn_more'] = 'Learn More.';
$lang['out_of_5_stars'] = 'Out of 5 stars';
$lang['star_1'] = 'Star 1';
$lang['star_2'] = 'Star 2';
$lang['star_3'] = 'Star 3';
$lang['star_4'] = 'Star 4';
$lang['star_5'] = 'Star 5';

$lang['gift_voucher_code'] = 'Gift voucher code';
$lang['va'] = 'Vat'; // asking for this
$lang['in_stock_online'] = 'In stock online';
$lang['including_tax'] = 'Including tax';
$lang['confirm_question']  = 'Confirm!';
$lang['are_you_sure_to_remove_this'] = ' Are you sure to remove this ? ';
$lang['cancel'] = ' Cancel';
$lang['confirm'] = ' Confirm';

$lang['start_browsing_our_products'] = ' Browse Our Products';
$lang['shipping_method'] = 'Shipping Method';
$lang['shipping'] = 'Shipping';
$lang['payment_method'] = 'Payment Method';
$lang['payment'] = 'Payment';
$lang['Write_Review'] = ' Write Review ';
$lang['Be_the_first_to_review'] = 'Be the first to review';
$lang['Review'] = 'Review';
$lang['Redeem'] = 'Redeem';
$lang['Shopping'] = 'Shopping';
$lang['Shipping_Address'] = 'Shipping Address';
$lang['Basket'] = 'Basket';
$lang['Order_Details'] = 'Order Details';
$lang['Deliver_here'] = 'Deliver here';
$lang['No_Shipping_Charges'] = 'No Shipping Charges <br>Required for Pickup';
$lang['Choose_your_branch'] = 'Choose your branch';
$lang['branch'] = 'Branch';
$lang['Select_City'] = 'Select City';
$lang['Select_Payment_Method'] = 'Select Payment Method';
$lang['Pay_Online'] = 'Pay Online';
$lang['cod'] = 'COD';
$lang['Transaction_Failed'] = 'Transaction Failed';
$lang['Shipping_method_required'] = 'Shipping method required';
$lang['Attention'] = 'Attention';
$lang['Some_of_the_Items_are_not_available_in_this_Branch'] = 'Some of the Items are not available in this Branch';
$lang['SR'] = 'SR';
$lang['Sold_out'] = 'Sold out';
$lang['Edit_Cart'] = 'Edit Cart';
$lang['Continue_to_Payment'] = 'Continue to Payment';
$lang['By_Clicking_continue'] = 'By Clicking continue, you will receive the proposed quantity and sold-out items will be discarded';
$lang['Please_select_brach_from_map'] = 'Please select brach from map';
$lang['Selected'] = 'Selected';
$lang['Your_Order_Has_been_Placed_Successfully'] = 'Your Order Has been Placed Successfully';
$lang['Print'] = 'Print';
$lang['Total_Tax_Paid'] = 'Total Tax Paid';
$lang['Grand_Total'] = 'Grand Total';
$lang['Continue'] = 'Continue';
$lang['checkout'] = 'Check Out';
$lang['show_all_city'] = 'Show All City';
$lang['Please_select_payment_method'] = 'Please select payment method';
$lang['only'] = 'only';
$lang['What_do_you_think_about_this_product'] = 'What do you think about this product?';
$lang['Continue_to_complete_the_order'] = 'Continue to complete the order';

// Hamza
$lang['Choose_closest_branch'] = 'Choose closest branch';
$lang['Complain_No'] = 'Complain No';
// $lang['Submitted'] = 'Submitted';
$lang['I_accept_the'] = 'I accept the';
$lang['terms_and_conditions'] = 'Terms and Conditions';
$lang['Branch_is_not_delivered_in_your_district'] = 'This Branch is not delivered in your district please select other branch';
$lang['Looks_like_there_are_no_items_in_your_basket_yet'] = 'Looks like there are no items in your basket yet';
$lang['The_order_doesnot_meet_maximum_purchase_limit'] = 'The order doesn’t meet maximum purchase limit';
$lang['The_order_doesnot_meet_minimum_purchase_limit'] = 'The order doesn’t meet minimum purchase limit';
$lang['order_id'] = 'Order ID';
$lang['order_total'] = 'Order Total';
$lang['on_going'] = 'OnGoing';
$lang['closed'] = 'Closed';
$lang['pending'] = 'Pending';
$lang['payment_methods'] = 'Payment Methods';
$lang['promo_applied'] = 'Promo applied';
$lang['clear'] = 'Clear';
// $lang['hurray_gift_voucher_applied_successfully_new'] = 'Hurray! Gift voucher applied successfully';
// $lang['gift_voucher_cleared_successfully_new'] = 'Gift voucher cleared successfully';
$lang['clear_this_coupon'] = 'Are you sure to clear this coupon';

$lang['order_assigned'] = 'An order is assigned to you with Order';
$lang['at_chocomood'] = 'At Chocomood';

$lang['dear'] = 'Dear';
$lang['status_assign_order'] = 'Status of your assigned Order';
$lang['changed_to'] = 'is changed to';

$lang['order_with_deliver'] = 'Your order with order';
$lang['thankyou_chocomood'] = 'is delivered. Thank you for choosing Chocomood ';

$lang['order_status_update'] = 'Status of your Order';

$lang['assigned_delivery_person'] = 'is assigned to the Delivery Person with OTP.';
$lang['provide_otp_to_assign_person'] = 'Please provide this OTP to the assigned person';
$lang['delivery_completion'] = 'for the delivery completion.';
$lang['sorry_we_could_not_find_any_result_that_matches_your_request'] = 'Sorry we could not find any result that matches  your request.';
$lang['Payment_at_Store'] = 'Payment at Store';
$lang['Add_an_Address'] = 'Add an Address';
$lang['Become_a_user_and_login_to_add_items_to_your_favourite_list'] = 'Become a user & login to add items to your favourite list';

$lang['fill_color'] = "Fill Color";
$lang['color'] = "Color";
$lang['add_fill_color'] = "Add Color";
$lang['filling'] = "Filling";
$lang['add_filling'] = 'Add Filling';
$lang['powder_color'] = "Powder Color";
$lang['add_powder_color'] = "Add Powder Color";

$lang['ribbons'] = 'Ribbons';
$lang['add_ribbon'] = 'Add Ribbon';
$lang['ribbon'] = 'Ribbon';
$lang['add_box_category'] = 'Add Box Category';
$lang['add_child_box_category'] = 'Add Child Box Category';
$lang['add_shape'] = 'Add Shape';
$lang['shape'] = 'Shape';
$lang['wrappings'] = 'Wrappings';
$lang['add_wrapping'] = 'Add Wrapping';
$lang['wrapping'] = 'Wrapping';
$lang['content_type'] = "Content Type";
$lang['add_content_type'] = "Add Content Type";
$lang['add_content_type_sub'] = 'Add Content Type Sub';
$lang['content_type_sub'] = 'Content Type Sub';
$lang['add_character'] = 'Add Character';
$lang['character_type'] = 'Character Type';
$lang['add_Fill_Color'] = 'Add Fill Color';
$lang['Fill_Color'] = 'Fill Color';
$lang['add_Powder_Color'] = 'Add Powder Color';
$lang['Powder_Color'] = 'Powder Color';
$lang['order_now_and_collect_from_store'] = 'Order now and collect from store';
$lang['Delivery'] = 'Delivery';
$lang['Order_now_and_your_order_will_be_delivered_in_two_to_five_days'] = 'Order now and your order will be delivered in 2-5 days';
$lang['collect_from_store'] = 'Collect from store';
$lang['Free_delivery_starts_from'] = 'Free delivery starts from ';
$lang['SAR_or_more'] = ' SAR or more';
$lang['a_shape_must_be_selected'] = 'A shape must be selected to proceed';
$lang['shape_title'] = 'Shape Title';
$lang['shape_order_processing_time'] = 'Order Processing Time';
$lang['shape_price_per_pcs'] = 'Price Per Piece';
$lang['shape_price_per_kg'] = 'Price Per Kg';
$lang['shape_individual_size'] = 'Individual Size';
$lang['shape_Description'] = 'Description';
$lang['reset'] = 'Reset';
$lang['auto_fill'] = "Auto Fill";
$lang['choco_box'] = "Choco Box";
$lang['choco_message'] = "Choco Message";
$lang['step_1'] = "Step 1:";
$lang['step_2'] = "Step 2:";
$lang['step_3'] = "Step 3:";
$lang['select_favourite_box'] = "Select your Favorite Box";
$lang['select_box'] = "SELECT A BOX TO CONTINUE";
$lang['customise_your_message'] = "Customize Your Message";
$lang['click_chocolate_letter_to_add'] = "Click Chocolate letters to add them to your box";
$lang['click_symbol_letter_to_add'] = "Click Symbols and Shapes to add them to your box";
$lang['get_current_location'] = "Get Current Location";
$lang['add_address'] = "Add Address";
$lang['back_to_cart'] = "Back to Cart";
$lang['order_summary'] = "Order Summary";
$lang['nex_t'] = "Next";
$lang['pre_vious'] = "Previous";
$lang['collect_from_store_no_shipping_req'] = "Collect From Store Selected No Shipping Required.";
$lang['click_on_this_icon_to_explore'] = "If this appears, click on icon to zoom in for more stores.";
$lang['brand'] = 'Brand';
$lang['brands'] = 'Brands';
$lang['categories'] = 'Categories';
$lang['add_brand'] = 'Add Brand';
$lang['pickup_location']= 'Select your pickup Location Branch';