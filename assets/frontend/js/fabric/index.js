const svgState = {}
var image_left = 55;
var image_top = 75;
var _i = 0;

var get_url = document.getElementById('custom_box');

const canvas = new fabric.Canvas('canvas', {
    width: 450,
    height: 550,

});
canvas.renderAll();

fabric.Image.fromURL(get_url.value, (img) => {
    img.scaleToHeight(100);
    img.scaleToWidth(450);
    canvas.backgroundImage = img
    canvas.renderAll()
})

const addImage = (canvas, str) => {
    var box_size = $('#customsgbox').data('box_size');
    if (_i < box_size) {
        fabric.Image.fromURL(str, img => {
            img.set({ top: image_top, left: image_left, selectable: false }); //  width: canvas.width, height: canvas.height, 
            img.scaleToHeight(65);
            img.scaleToWidth(65);
            canvas.add(img);
            canvas.requestRenderAll();
            image_left += 92;
            _i++;
            if (_i % 4 === 0) {
                image_top += 85;
                image_left = 55;
            }
        })
    }
}

const clearCanvas = (canvas, state) => {
    state.val = canvas.toSVG()
    canvas.getObjects().forEach((o) => {
        if (o !== canvas.backgroundImage) {
            canvas.remove(o)
        }
    })
    image_left = 55;
    image_top = 75;
    _i = 0;
}