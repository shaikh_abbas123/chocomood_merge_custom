

$(document).ready(function(){
    if(document.getElementById("customize-chocolate-box")!=null){
    var chocolateChoosed = [];
    var maxQuantity      = $('.chocolate-choosen').attr('data-max-quantity'); 
    var rowsCount      = $('.chocolate-choosen').attr('data-count-rows'); 
    var chocolateInRow     = $('.chocolate-choosen').attr('data-chocolate-count-in-rows').split("-");
    document.querySelector(".chocolate-choosen-container").innerHTML='';
    for(row=0;row<rowsCount;row++) {
        document.querySelector(".chocolate-choosen-container").innerHTML += `
            <div class="row chocolate-choosen-row"> </div>
        `;
        document.querySelectorAll(".chocolate-choosen-row")[row].innerHTML='';        
        for(cr= 0; cr<chocolateInRow[row];cr++){
            document.querySelectorAll(".chocolate-choosen-row")[row].innerHTML +=`
            <div class="col-2 chocolate-choosen-item p-1">
                <div class="chocolate-choosen-card">
                </div>
            </div>
        `
        }
        
    }
    var count = 0;

    $(`.chocolate-for-box input.quantity-count`).on('focus' ,(e)=> {
        count=0
        $(`.chocolate-for-box input.quantity-count`).each( num => {
            count=count+Number($(`.chocolate-for-box input.quantity-count:eq(${num})`).val());
        });
        console.log(count);
        
    })
    $(`.chocolate-for-box input.quantity-count`).on('blur' ,(e)=> {
        count=0
        $(`.chocolate-for-box input.quantity-count`).each( num => {
            count=count+Number($(`.chocolate-for-box input.quantity-count:eq(${num})`).val());
        });
        console.log(count);
    })
    $(`input.quantity-count`).on('keyup' , (e)=>{



        var $input = $(`input.quantity-count:eq(${$('input.quantity-count').index(e.target)})`);
        var chocolateInArray = chocolateChoosed.filter( chocolate => {
            return chocolate == e.originalEvent.path[2].querySelector("img").getAttribute("src") == true
        })
        var $inputValue = 0;
        if(count == maxQuantity){
            $input.val(0)
        }
        else if($input.val() < maxQuantity && count == 0) {
            $inputValue= $input.val();
        }else if ($input.val() < maxQuantity && count > 0 && count <= maxQuantity){
            if($input.val() > maxQuantity - count){
                $inputValue= $input.val(maxQuantity - count);
            } else {
                $inputValue= $input.val();
            }
            
        }else if ($input.val() > maxQuantity && count == 0 && $input.val() > 10){
            $inputValue = $input.val(maxQuantity)
            
        }else if ($input.val() >= maxQuantity && count > 0 && count <= maxQuantity && $input.val() > 10){
            $inputValue = $input.val(maxQuantity - count)
        } else if ($input.val() > maxQuantity && count > 0 && chocolateChoosed.length != maxQuantity){
            $inputValue = $input.val()
        }
        console.log($inputValue)



        if($input.val() > chocolateInArray.length) {
            for(ci=0;ci<$input.val() - chocolateInArray.length;ci++){
                chocolateChoosed.push(e.originalEvent.path[2].querySelector("img").getAttribute("src"));
            }
            chocolateChoosen();
        }else if($input.val() < chocolateInArray.length){
                
            for(ci=0;ci<chocolateInArray.length - $input.val();ci++){
                var index = chocolateChoosed.lastIndexOf(e.originalEvent.path[2].querySelector("img").getAttribute("src"));
                chocolateChoosed.splice(index,1);
            }
            chocolateChoosen();
            
        }
        
    })
    $('button.increase').click( (e) => {
        var $input = $(`input.quantity-count:eq(${$('button.increase').index(e.target)})`);

        
        if(e.originalEvent.path[2].className.indexOf('box-counter') == -1 && chocolateChoosed.length < maxQuantity){
            $input.val( +$input.val() + 1 );
            chocolateChoosed.push(e.originalEvent.path[2].querySelector("img").getAttribute("src"));
            chocolateChoosen();

        }else if(chocolateChoosed.length == maxQuantity){
            $('html, body').animate({
                scrollTop: $('#add-to-cart').offset().top
            }, 10, function(){
                window.location.hash = '#add-to-cart';
            });
        }else if(e.originalEvent.path[2].className.indexOf('box-counter') != -1){
            $input.val( +$input.val() + 1 );
        }
    })
    $('button.decrease').on('click', (e) => {
        var $input = $(`input.quantity-count:eq(${$('button.decrease').index(e.target)})`);
        if(e.originalEvent.path[2].className.indexOf('box-counter') != -1){
            if($input.val()> 1){
                
                $input.val( +$input.val() - 1 )
            }
        } else {
            if($input.val() > 0){
                
                $input.val( +$input.val() - 1 );
                var index = chocolateChoosed.lastIndexOf(e.originalEvent.path[2].querySelector("img").getAttribute("src"));
                chocolateChoosed.splice(index,1);
                chocolateChoosen()

            }
        }
        
        
    })

    function chocolateChoosen(){
        var item = document.getElementsByClassName('chocolate-choosen-card');
        for(c=0;c<item.length;c++){
            item[c].innerHTML = ``
        }
        for(c=0;c<chocolateChoosed.length;c++){
            item[c].innerHTML = `<img class="m-auto d-block" src="${chocolateChoosed[c]}" width="100%" alt="">`
        }
    }
    var boxHeight = document.getElementById('box').clientHeight; 
    var boxWidth = document.getElementById('box').clientWidth; 
    $('.chocolate-choosen-row').css({'height':boxHeight/5.8,'width':boxWidth/1.2});
    window.onresize = ()=> {
        boxHeight = document.getElementById('box').clientHeight; 
        boxWidth = document.getElementById('box').clientWidth; 
        $('.chocolate-choosen-row').css({'height':boxHeight/5.8,'width':boxWidth/1.2})
    }
    if(window.scrollY >= 800){
        document.getElementById("show-box-btn").style.display="block";
    }else {
        document.getElementById("show-box-btn").style.display="none";
    }
    window.onscroll = () => {
        if(window.scrollY >= 800){
            document.getElementById("show-box-btn").style.display="block";
        }else {
            document.getElementById("show-box-btn").style.display="none";
        }
    }
    $("a#show-box-btn").on('click', function(event) {
        if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 10, function(){
            window.location.hash = hash;
        });
        } 
    });
    }
    
    
  });