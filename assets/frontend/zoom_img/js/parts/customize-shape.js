$(document).ready( ()=>{
  if(document.getElementById("customize-chocolate-print")!=null || document.getElementById("customize-chocolate-shape")!=null){
        
    var contentType = document.getElementById("content-type"),
    drawing = document.getElementById("drawing"),
    contentTypeItems = contentType.getElementsByClassName("content-type-item"),
    drawingItems = drawing.getElementsByClassName("drawing-item"),
    colorsChecked = document.getElementsByClassName("color-item"),
    contentTypeView = document.getElementById("content-type-view"),
    contentTypeViewItem = contentTypeView.children[0].children,
    contentTypeViewImage = "",
    color = '#FD692B',
    powderColor = "",
    contentInput = document.getElementsByClassName(
      "customize-shape-item-content-view"
    ),
    contentOutput = document.getElementById("content-output");

    function getSrc() {
    if ($('.text-color-active input[type=radio]').val() == "none") {
      if (
        contentType
          .getElementsByClassName("content-type-active")[0]
          .querySelector("input:checked").value == "logo"
      ) {
        var logoDrawing = contentType.getElementsByClassName("content-type-active")[0].querySelectorAll("img")[1].dataset.mask
        contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
        contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="mask-container"><img class="mask-item" src="https://about.canva.com/wp-content/themes/canvaabout/img/proLandingPage/banner-wave-3.svg" width="100%" style="-webkit-mask-image:url(' + logoDrawing + ')!important;mask-image:url(' + logoDrawing + ')!important;background-color: rgb(253, 105, 43);"/></div>';
        contentTypeView.children[0].children[0].style.backgroundColor =
          color;
        // contentTypeView.children[0].children[0].children[1].style.backgroundColor = color;
      } else if (contentType
        .getElementsByClassName("content-type-active")[0]
        .querySelector("input:checked").value == "text") {
        var textType = document.querySelector(".text-type-active").querySelector("img").dataset.mask;
        contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
        contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="mask-container"><img class="mask-item" src="https://about.canva.com/wp-content/themes/canvaabout/img/proLandingPage/banner-wave-3.svg" width="100%" style="-webkit-mask-image:url(' + textType + ')!important;mask-image:url(' + textType + ')!important;background-color: rgb(253, 105, 43);"/></div>';
      } else {
        var drawingChoose = drawing.querySelector(".drawing-active").querySelector("img").dataset.mask;
        contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
        contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="mask-container"><img class="mask-item" src="https://about.canva.com/wp-content/themes/canvaabout/img/proLandingPage/banner-wave-3.svg" width="100%" style="-webkit-mask-image:url(' + drawingChoose + ')!important;mask-image:url(' + drawingChoose + ')!important;background-color: rgb(253, 105, 43);"/></div>';
      }

      contentTypeView.children[0].children[0].style.backgroundColor = color;
      contentTypeView.children[0].children[0].querySelector("img.mask-item").style.backgroundColor = color;

    } else {

      var powderColor = $('.text-color-active img').attr("data-mask");

      if (powderColor != undefined) {
        if (
          contentType
            .getElementsByClassName("content-type-active")[0]
            .querySelector("input:checked").value == "logo"
        ) {
          var logoDrawing = contentType.getElementsByClassName("content-type-active")[0].querySelectorAll("img")[1].dataset.mask
          contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
          contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="mask-container"><img class="mask-item" src="' + powderColor + '" width="100%" style="-webkit-mask-image:url(' + logoDrawing + ')!important;mask-image:url(' + logoDrawing + ')!important;background-color: rgb(253, 105, 43);"/></div>';
          contentTypeView.children[0].children[0].style.backgroundColor =
            color;
          // contentTypeView.children[0].children[0].children[1].style.backgroundColor = color;
        }
        else if (contentType
          .getElementsByClassName("content-type-active")[0]
          .querySelector("input:checked").value == "text") {
          var textType = document.querySelector(".text-type-active").querySelector("img").dataset.mask;
          contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
          contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="mask-container"><img class="mask-item" src="' + powderColor + '" width="100%" style="-webkit-mask-image:url(' + textType + ')!important;mask-image:url(' + textType + ')!important;background-color: rgb(253, 105, 43);"/></div>';
        } else {
          var drawingChoose = drawing.querySelector(".drawing-active").querySelector("img").dataset.mask;
          contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
          contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="mask-container"><img class="mask-item" src="' + powderColor + '" width="100%" style="-webkit-mask-image:url(' + drawingChoose + ')!important;mask-image:url(' + drawingChoose + ')!important;background-color: rgb(253, 105, 43);"/></div>';
        }

        contentTypeView.children[0].children[0].style.backgroundColor = color;
        contentTypeView.children[0].children[0].querySelector("img.mask-item").style.backgroundColor = color;
      } else {
        if (
          contentType
            .getElementsByClassName("content-type-active")[0]
            .querySelector("input:checked").value == "logo"
        ) {
          var logoDrawing = contentType.getElementsByClassName("content-type-active")[0].querySelectorAll("img")[1].dataset.mask
          contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
          contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="img-printer"><div class="blend-mode-item" style="background-image:url(' + logoDrawing + ')"></div></div>';
          contentTypeView.children[0].children[0].style.backgroundColor =
            color;
          // contentTypeView.children[0].children[0].children[1].style.backgroundColor = color;
        } else {
          var drawingChoose = drawing.querySelector(".drawing-active").querySelector("img").dataset.mask;
          contentTypeViewImage = document.getElementsByClassName("content-type-active")[0].children[0].src;
          contentTypeViewItem[0].innerHTML = '<img src="' + contentTypeViewImage + '" width="100%" />' + '<div class="img-printer"><div class="blend-mode-item" style="background-image:url(' + drawingChoose + ')"></div></div>';
        }

        contentTypeView.children[0].children[0].style.backgroundColor = color;
        contentTypeView.children[0].children[0].style.backgroundSize = "50% 50%";
        contentTypeView.children[0].children[0].style.backgroundPostion = "center";
        contentTypeView.children[0].children[0].style.backgroundPostion = "center";
        contentTypeView.children[0].children[0].style.backgroundRepeat = "no-repete";
        contentTypeView.children[0].children[0].style.backgroundBlendMode = "no-repete";
      }

    }
    }

    //content Type
    for (var i = 0; i < contentTypeItems.length; i++) {
    contentTypeItems[i].addEventListener("click", function () {
      var current = document.getElementsByClassName("content-type-active"),
        contentTypeItemsImg = this.querySelector("Img");
      current[0].className = current[0].className.replace(
        " content-type-active",
        ""
      );
      this.className += " content-type-active";
      getSrc();
      if (this.querySelector("input[type=radio]:checked").value == "text") {
        document.getElementById("text-type").style.display = "block"
      } else {
        if (document.getElementById("text-type")) {
          document.getElementById("text-type").style.display = "none"
        }

      }

      if (
        this.querySelector("input[type=radio]:checked").value == "text" ||
        this.querySelector("input[type=radio]:checked").value == "drawing"
      ) {
        document
          .getElementById("content-output")
          .setAttribute("type", "text");
      } else {
        document
          .getElementById("content-output")
          .setAttribute("type", "file");
      }

      if (this.querySelector("input[type=radio]:checked").value == "drawing") {
        drawing.style.display = "block";

        for (ci = 0; ci < contentInput.length; ci++) {
          contentInput[ci].style.backgroundImage = "unset";
        }
      } else {
        drawing.style.display = "none";
      }
    });
    }

    for (var di = 0; di < drawingItems.length; di++) {
    drawingItems[di].addEventListener("click", function () {
      var current = document.getElementsByClassName("drawing-active");

      current[0].className = current[0].className.replace(" drawing-active", "");
      this.className += " drawing-active";
      getSrc();
    });
    }


    // colors
    $("#colors").on("change", "input[type=radio]", function () {

    color = $('.checked input[type=radio]').val();
    drawingChoose = drawing.querySelector(".drawing-active").children[0]
      .children[0],
      drawingChooseActive = drawingChoose.cloneNode(true);
    getSrc();


    });

    for (var i = 0; i < colorsChecked.length; i++) {
    colorsChecked[i].addEventListener("click", function () {
      var checked = document.getElementsByClassName("checked");
      checked[0].className = checked[0].className.replace(" checked", "");
      this.className += " checked";
      // this.className = this.className.replace(" checked", "");
    });
    }



    // start fillings
    var addOns = document.getElementsByClassName("fillings-item");
    for (var i = 0; i < addOns.length; i++) {
    addOns[i].addEventListener("click", function () {
      var current = document.getElementsByClassName("fillings-active");

      current[0].className = current[0].className.replace(" fillings-active", "");
      this.className += " fillings-active";
    });
    }

    //  start wrappings

    var wrappings = document.getElementsByClassName("wrappings-item");
    for (var i = 0; i < wrappings.length; i++) {
    wrappings[i].addEventListener("click", function () {
      var current = document.getElementsByClassName("wrappings-active");

      current[0].className = current[0].className.replace(
        " wrappings-active",
        ""
      );
      this.className += " wrappings-active";
    });
    }


    //  start text type

    var textType = document.getElementsByClassName("text-type-item");
    for (var i = 0; i < textType.length; i++) {
    textType[i].addEventListener("click", function () {
      var current = document.getElementsByClassName("text-type-active");

      current[0].className = current[0].className.replace(
        " text-type-active",
        ""
      );
      this.className += " text-type-active";
      getSrc();
    });
    }


    //  start text color

    var textColor = document.getElementsByClassName("text-color-item");
    for (var i = 0; i < textColor.length; i++) {
    textColor[i].addEventListener("click", function () {

      var current = document.getElementsByClassName("text-color-active");

      current[0].className = current[0].className.replace(
        " text-color-active",
        ""
      );

      this.className += " text-color-active";
      getSrc();
    });
    }
  }
})


