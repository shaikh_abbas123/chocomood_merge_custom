$(document).ready( ()=>{
    if(document.getElementById("customize-box")!=null){
        function checkTemplateType(){
            // check template type == text
            if($(".template-type-active:eq(0) input:checked").val() == "text"){
                document.getElementById("text-templates").style.display='flex';
                
            }else {
                document.getElementById("text-templates").style.display='none';
            }
            // check template type == drawing
            if($(".template-type-active:eq(0) input:checked").val() == "drawing"){
                document.getElementById("drawing").style.display='flex';
                
            }else {
                document.getElementById("drawing").style.display='none';
            }
            // check template type == text || template type == text
            if($(".template-type-active:eq(0) input:checked").val() == "text" || $(".template-type-active:eq(0) input:checked").val() == "drawing"){
                $('.max-character:eq(0)').css('display','block');
                $("#content-output").attr('type','text')
            } else {
                $('.max-character:eq(0)').css('display','none');
                $("#content-output").attr('type','file');
                if($(".template-type-active:eq(0) input:checked").val() == "logo"){
                    $("#content-output:after").css('content','Add Your Logo');
                    $("#content-output").addClass('add-your-logo').removeClass('add-your-photo');;
                } else {
                    $("#content-output").addClass('add-your-photo').removeClass('add-your-logo');;
                }
                
            }
            // if type = none  
            if($(".template-type-active:eq(0) input:checked").val() == "none"){
                $("#content-output").css('display','none');
                $(".max-character").css('display','none');
                $('.template-type-choosen:eq(0)').remove();
            } else {
                $("#content-output").css('display','block');
                $(".max-character").css('display','block');
                templateTypeSrc();
            }
            
            addRiboon()
        }
        function templateTypeSrc(){
                var templateType = $('.template-type-active:eq(0) img').attr('src');
                if($(".template-type-active:eq(0) input:checked").val() == "text"){
                    templateType = $('.text-template-active:eq(0) img').attr('src')
                }else if($(".template-type-active:eq(0) input:checked").val() == "drawing"){
                    templateType = $('.drawing-active:eq(0) img').attr('src')
                }
                $('.template-type-choosen:eq(0)').remove();
                
                $('.view-item:eq(0)').append(`
                    <img class="template-type-choosen" src="${templateType}" style="position:absolute; width:80%; display:block;z-index:1;top:22px;left:23px;" />
                `);
        }
        function addRiboon(){
            if($('.ribbons-active:eq(0) input:checked').val() != 'none'){

                if($(".template-type-active:eq(0) input:checked").val() == "none"){
                    $('.template-type-choosen:eq(0)').remove();
                }                
                var riboon = $('.ribbons-active:eq(0) img').attr('src');
                $('.riboon-choosen:eq(0)').remove();
            
                $('.view-item:eq(0)').append(`
                    <img class="riboon-choosen" src="${riboon}" style="position:absolute; width:100%; display:block;z-index:2;top:0;left:0;" />
                `);
            }else {
                $('.riboon-choosen:eq(0)').remove();
            }
        }        // template type
        var templateType = document.getElementsByClassName("template-type-item");
        for (var i = 0; i < templateType.length; i++) {
            templateType[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("template-type-active");
                current[0].className = current[0].className.replace(
                " template-type-active",
                ""
                );
                this.className += " template-type-active";
                checkTemplateType();
            });
        }

        // start fillings
        var addOns = document.getElementsByClassName("fillings-item");
        for (var i = 0; i < addOns.length; i++) {
            addOns[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("fillings-active");
        
                current[0].className = current[0].className.replace(" fillings-active", "");
                this.className += " fillings-active";
            });
        }
        var drawing = document.getElementsByClassName("drawing-item");
        for (var i = 0; i < drawing.length; i++) {
            drawing[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("drawing-active");
        
                current[0].className = current[0].className.replace(
                " drawing-active",
                ""
                );
                this.className += " drawing-active";
                checkTemplateType();
            });
        }
        var textTemplate = document.getElementsByClassName("text-template-item");
        for (var i = 0; i < textTemplate.length; i++) {
            textTemplate[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("text-template-active");
        
                current[0].className = current[0].className.replace(
                " text-template-active",
                ""
                );
                this.className += " text-template-active";
                checkTemplateType();
            });
        }
        var ribbons = document.getElementsByClassName("ribbons-item");
        for (var i = 0; i < ribbons.length; i++) {
        ribbons[i].addEventListener("click", function () {
            var current = document.getElementsByClassName("ribbons-active");
    
            current[0].className = current[0].className.replace(
            " ribbons-active",
            ""
            );
            this.className += " ribbons-active";
            checkTemplateType()
        });
        }
        //  start wrappings
    
        var wrappings = document.getElementsByClassName("wrappings-item");
        for (var i = 0; i < wrappings.length; i++) {
        wrappings[i].addEventListener("click", function () {
            var current = document.getElementsByClassName("wrappings-active");
    
            current[0].className = current[0].className.replace(
            " wrappings-active",
            ""
            );
            this.className += " wrappings-active";
        });
        }
        
    
    
    
        
    }
  })
  
  
  